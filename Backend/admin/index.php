<?php 
include_once('includes/check.php'); 
if($_SESSION['category'] == "user")
{
	header('location: feedback.php');
}
include_once('includes/header.php');
unset($_SESSION['lib']); unset($_SESSION['newUserId']); unset($_SESSION['userFlag']); unset($_SESSION['lib_name']);
$database->query('SELECT id FROM web_registration');
$database->execute();
$first = $database->rowCount();

$database->query('SELECT id FROM registeredusers');
$database->execute();
$second = $database->rowCount();

$database->query('SELECT id FROM web_contacts');
$database->execute();
$third = $database->rowCount();

$database->query('SELECT id FROM web_gallery');
$database->execute();
$fourth = $database->rowCount();

$database->query('SELECT id FROM web_sponsors');
$database->execute();
$fifth = $database->rowCount();

$database->query('SELECT id FROM userfeedback');
$database->execute();
$sixth = $database->rowCount();
?>

<div class="container-fluid">
	<div class="row">

		<?php include_once('includes/menu.php'); ?>

		<div class="col-md-9">
			<div class="well well-sm content_area">Welcome Admin</div>
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Registered Users
					</div>
					<div class="panel-body"><?php echo $first; ?></div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						Subscriptions
					</div>
					<div class="panel-body"><?php echo $second; ?></div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-success">
					<div class="panel-heading">
						User Contacts
					</div>
					<div class="panel-body"><?php echo $third; ?></div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading">
						Gallery
					</div>
					<div class="panel-body"><?php echo $fourth; ?></div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-warning">
					<div class="panel-heading">
						Sponsors
					</div>
					<div class="panel-body"><?php echo $fifth; ?></div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-danger">
					<div class="panel-heading">
						User Feedbacks
					</div>
					<div class="panel-body"><?php echo $sixth; ?></div>
				</div>
			</div>

		</div>

	</div>
</div>

<?php include_once('includes/footer.php'); ?>