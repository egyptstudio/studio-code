<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php');
$_SESSION['lib'] = 26;
$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page']))
{
    $page  = $_GET["page"];
} else {
    $page=1;
}
$startFrom = ($page-1) * $recPerPage;
$database->query('SELECT * FROM faq_q, faq_cat WHERE faq_q.catPid = faq_cat.pid ORDER BY faq_q.qPid DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage);
$database->query('SELECT * FROM faq_q, faq_cat WHERE faq_q.catPid = faq_cat.pid ORDER BY faq_q.qPid DESC LIMIT :start, :num');
$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();
if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else
{
    $rowsGen = $database->rowCount();
}
if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>
<div class="container-fluid">
    <div class="row">
        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Questions</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_entry btn-primary">Add New Entry &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-left">
                                    <ul class="pagination">
                                        <li>
                                            <a href="questions.php?page=1" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                        <?php
                                        if($page == 1)
                                        {
                                            $prevPage = 1;
                                        } else
                                        {
                                            $prevPage = ($page - 1);
                                        }
                                        if($page == $total_pages)
                                        {
                                            $nextPage = $total_pages;
                                        } else
                                        {
                                            $nextPage = ($page + 1);
                                        }
                                        echo '<li><a href="questions.php?page='.$prevPage.'">&lt;</a></li>';
                                        echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                        echo '<li><a href="questions.php?page='.$nextPage.'">&gt;</a></li>';
                                        ?>
                                        <li>
                                            <a href="questions.php?page=<?php echo $total_pages ?>" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>

                            </div>

                            <div class="col-md-12">
                                <table class="table table-bordered userTable">
                                    <thead>
                                        <tr>
                                            <th>Language</th>
                                            <th>Category</th>
                                            <th>Question</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $database->resultset();
                                        foreach ($result as $row) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <span><?php echo $row['lang']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $row['catTxt']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $row['qTxt']; ?></span>
                                                </td>
                                                <td style="text-align:center">
                                                    <a href="javascript:;" class="btn btn-lang btn-primary" data-id="<?php echo $row['qPid']; ?>"><i class="fa fa-language"></i> Translation</a>
                                                    <a href="javascript:;" class="btn btn-edit btn-info" data-id="<?php echo $row['qPid']; ?>"><i class="fa fa-edit"></i> Edit</a>
                                                    <a href="javascript:;" class="del_entry btn btn-danger" data-id="<?php echo $row['qPid']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                                </td>

                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } else {
                                    echo "<h2 style='margin-left: 20px'>List of questions will be displayed here.</h2>";
                                } ?>

                            </div><!-- aj_sec -->
                        </div><!-- postSection -->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Entry</h4>
                </div>

                <form class="form-horizontal myform">

                    <div class="modal-body">
                     <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Category</h4></label>
                        <div class="col-sm-8">
                            <input type="hidden" value="en" class="getValue form-control" data-key="lang">
                            <select class="form-control getValue" data-key='catPid'>
                            <?php 
                            $database->query('SELECT catTxt, pid FROM faq_cat ORDER BY pid ASC');
                            $database->execute();
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                echo '<option value="'.$row['pid'].'">'.$row['catTxt'].'</option>';
                            } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Question</h4></label>
                        <div class="col-sm-8">
                            <input type="type" class="getValue required form-control" data-key="qTxt">
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save_entry">Save</button>
                </div>
            </form>

        </div>
    </div>
</div>
<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="aj_data"></div>
        </div>
    </div>
</div>
<?php
include_once ('../includes/footer.php');  ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;

        $(".add_entry").click(function(){
            $("#myModal").modal('show');
        });
        $(document).on('click', '.btn-lang', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("faq_data.php?command=getFaqQuesTranslation&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 300);
        });

        $(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("faq_data.php?command=getFaqQues&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 300);
        });

        $(document).on('click', '.save_entry', function(e){
            e.preventDefault();
            if (!$(".myform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'faq_q' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    if(data == 'correct')
                    {
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            hideLoader();
                            $("#myModal").modal('hide');
                            showNotification('success', 'Process done successfully!');
                            $(".myform")[0].reset();    
                        });
                    } else
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });

$(document).on('click', '.del_entry', function(){
    if(!confirm('Are you sure, you want to delete data?')) return false;
    db = "qPid";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'faq_q' + '&db=' + db + '&data=' + value + '&pageName=' + 'questions' + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                    hideLoader();    
                    showNotification('success', 'Process done successfully!');
                });
            } else
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});


$(document).on('click', '.edit_entry', function(e){
    e.preventDefault();
    if (!$(".myeditform").validate().form()) {
        return false;
    }
    var arrFields = $(".myeditform .getValue").map(function() {
        var $elm = $(this),
        childId = $elm.data('key');
        if (childId.length == 0) {
            alert("Error");
            return false;
        }
        return childId;
    }).get();
    var arrValues = $(".myeditform .getValue").map(function() {
        if ($(this).val().length == 0) {
            $(this).val('');
        }
        return this.value.escapeHTML();
    }).get();

    var key = "qPid";
    var value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'faq_q' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + key + '&value=' + value + '&command=' + 'update',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                    hideLoader();
                    $("#myEditModal").modal('hide');    
                    showNotification('success', 'Process done successfully!');
                });
                
            } else
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('click', '.add-lang', function(e){
    e.preventDefault();
    if (!$(".langform").validate().form()) {
        return false;
    }
    var arrFields = $(".langform .getValue").map(function() {
        var $elm = $(this),
        childId = $elm.data('key');
        if (childId.length == 0) {
            alert("Error");
            return false;
        }
        return childId;
    }).get();
    var arrValues = $(".langform .getValue").map(function() {
        if ($(this).val().length == 0) {
            $(this).val('');
        }
        return this.value.escapeHTML();
    }).get();
    showLoader();
    var id = $(".qPid").val();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'faq_qtrans' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'add',
        cache: false,
        success: function(data) {

            if(data == 'correct')
            {
                $(".translations").load("faq_data.php?command=getFaqQuesTranslation&id="+id+" .langtable", function(){
                    hideLoader(); 
                    showNotification('success', 'Process done successfully!');
                    $(".langform")[0].reset();
                });
            } else
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('click', '.del-lang', function(){
    if(!confirm('Are you sure, you want to delete data?')) return false;
    db = "qtransId";
    value = $(this).data('id');
    showLoader();
    var id = $(".qPid").val();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'faq_qtrans' + '&db=' + db + '&data=' + value + '&pageName=' + 'questions' + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                $(".translations").load("faq_data.php?command=getFaqQuesTranslation&id="+id+" .langtable", function(){
                    hideLoader(); 
                    showNotification('success', 'Process done successfully!');
                });
            } else
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

});
</script>