<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 
$_SESSION['userFlag'] = 1;


if(isset($_SESSION['newUserId']))
{
    $userid = $_SESSION['newUserId'];
} else if(!isset($_REQUEST['id']) || !is_numeric($_REQUEST['id']))
{
    $userid = $_SESSION['userId'];
} else 
{
    $_SESSION['userId'] = $_REQUEST['id'];
    $userid = $_SESSION['userId'];
}

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}

$startFrom = ($page-1) * $recPerPage;

$database->query('SELECT * FROM user WHERE userType = :type OR userType = :type2 ORDER BY userId DESC');
$database->bind(':type', 1);
$database->bind(':type2', 2);
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM user WHERE userType = :type OR userType = :type2 ORDER BY userId DESC LIMIT :start, :num');
$database->bind(':type', 1);
$database->bind(':type2', 2);
$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records> $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}
if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            <input type="hidden" value="<?php echo $userid ?>" id="userid">
            <div class="row">
                <div class="col-md-12">
                    <div style="right: auto; font-size: 20px; top: 8px;" class="head_opts">
                        <a href="add_user.php"><i class="fa fa-arrow-left"></i> Create New User</a>
                    </div>
                    <h4 class="heading">Following List</h4>
                    
                </div>
                
                <div class="photoOpts2">
                    <div class="chkboxOpts">
                        <label> <input id="selectall" type="checkbox"> Select / Unselect All</label>
                    </div>
                    <button class="btn btn-primary pull-right followSelectedUser">
                        Follow selected
                    </button>
                </div>
                
                <div class="postSection">
                <div class="aj_sec">
                    <nav class="siteNav">
                      <ul class="pagination">
                        <li>
                          <a href="following.php?page=1" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                          </a>
                        </li>
                        <?php /*for ($i=1; $i<=$total_pages; $i++) { 
                            if($i == $page)
                            {
                                echo "<li class='active'><a href='following.php?page=".$i."'>".$i."</a></li> "; 
                            } else 
                            {
                                echo "<li class=''><a href='following.php?page=".$i."'>".$i."</a></li> "; 
                            }
                        };*/ 
                        if($page == 1)
                        {
                            $prevPage = 1;
                        } else 
                        {
                            $prevPage = ($page - 1);
                        }

                        if($page == $total_pages)
                        {
                            $nextPage = $total_pages;
                        } else 
                        {
                            $nextPage = ($page + 1);
                        }
                        echo '<li><a href="following.php?page='.$prevPage.'">&lt;</a></li>';
                        echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                        echo '<li><a href="following.php?page='.$nextPage.'">&gt;</a></li>';
                        
                        ?>
                        <li>
                          <a href="following.php?page=<?php echo $total_pages ?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                          </a>
                        </li>
                      </ul>
                    </nav>

                
                <div class="col-md-12 aj_data">
                    <?php
                    $result = $database->resultset();
                    if( $database->rowCount() > 0 ) { 

                        foreach ($result as $row) {
                            $haystack = $row['profilePic'];
                                    $needle = 'http';

                                    if (strpos($haystack,$needle) !== false) {
                                        $imgPath = $haystack;
                                    } else 
                                    {
                                        $imgPath = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/profile/".$haystack;
                                    }
                    ?>
                    <div class="col-md-2 photosCol">
                        
                        <div class="photoDetail">
                            <label class="pull-left userSelect">
                                <input data-id="<?php echo $row['userId'] ?>" class="post_photo" type="checkbox"> Select
                            </label>
                            <img src="<?php echo $imgPath; ?>" class="img-responsive" alt="Player">
                            <div class="follow_btm">
                                <p class="uName"><?php echo $row['fullName']; ?></p>
                                <a href="#" data-id="<?php echo $row['userId'] ?>" class="followUser">Follow</a>
                            </div>
                        </div>
                    </div>
                    <?php } 
                } else {
                    echo "Following list will be displayed here.";
                }
                    ?>
                </div>
                </div><!-- aj_sec -->
                </div><!-- postSection -->

            </div>
                
            </div>
        </div>
    </div>
</div>


<?php
include_once ('includes/footer.php'); ?>
<script type="text/javascript" src="js/users.js"></script>