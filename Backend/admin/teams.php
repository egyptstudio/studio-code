<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 
$imgUrl = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/";
$_SESSION['lib'] = 2;
$_SESSION['lib_name'] = 'teams';
?>

<div class="container-fluid">
    <div class="row">

        <?php
include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            
            <div class="row">
                <div class="col-md-12">
                    <h4 class="heading">Teams</h4>
                    <div class="head_opts">
                        <a href="add_library.php" class="btn btn-primary">Add new Photo &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                    </div>
                </div>

                <div class="aj_sec">
                    <div class="col-md-12 aj_row">
                        <?php 
                        $database->query('SELECT * FROM thesuarus WHERE type =:type');
                        $database->bind(':type', 2);
                        $database->execute();
                        $result = $database->resultSet();
                        $count = $database->rowCount();
                        if($count > 0)
                        {
                        foreach ($result as $row) {
                            $thesuarusId = $row['thesuarusId'];
                            $database->query('SELECT spt.*, std.* FROM studiophotothesaurus spt, studiophoto std WHERE spt.studioPhotoId = std.studioPhotoId AND std.captureType = :captureType AND spt.thesuarusId = :id');
                            $database->bind(':id', $thesuarusId);
                            $database->bind(':captureType', 2);
                            $database->execute();
                            $eventRow = $database->single();
                            $countRow = $database->rowCount();
                        ?>
                        <div class="col-md-3">
                            <div class="photoDetail teamDetail">
                                <?php if($countRow > 0) {?>
                                <a href="photos.php?id=<?php echo $eventRow['thesuarusId']; ?>">
                                <?php } ?>
                                    <img src="<?php echo $imgUrl.$row['photoUrl']; ?>" class="img-responsive" alt="Player">
                                    <div class="team_btm">
                                        <p class="pull-left"><?php echo $row['name']; ?></p>
                                        <p class="pull-right"><?php echo $countRow; ?> Photos</p>
                                    </div>
                                <?php if($countRow > 0) {?>
                                </a>
                                <?php } ?>
                                <a href="#" data-id="<?php echo $row['thesuarusId']; ?>" data-src="<?php echo $imgUrl.$row['photoUrl']; ?>" class="changeThumb btn btn-info"><i class="fa fa-edit"></i></a>
                            </div>
                        </div>
                        <?php } } else 
                        {
                            echo "No photos Added yet. Click on Add new Photo button to add your first Photo.";
                            } ?>
                    </div>
                </div>
            </div>
                
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="thesId">
<input type="hidden" id="thesFile">
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data">
        </div>
    </div>
  </div>
</div>

<?php
include_once ('includes/footer.php'); ?>
<script type="text/javascript" src="js/players.js"></script>