<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 

$_SESSION['lib'] = 14;
$database->query('SELECT * FROM userfeedback ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">User Feedbacks</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_entry btn-primary">Send Mail &nbsp;&nbsp;&nbsp;<i class="fa fa-send"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>

                            <div class="col-md-12">

                                <table class="table table-bordered userTable">
                                    <thead>
                                        <tr>
                                            <th>Select <input type="checkbox" id="selectall"></th>
                                            <th>Email</th>
                                            <th>Name</th>
                                            <th>Title</th>
                                            <th>Platform</th>
                                            <th style="text-align:center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $database->resultset();
                                        foreach ($result as $row) {
                                            $email = $row['username'];
                                            $database->query('SELECT fullName FROM user WHERE email = :email');
                                            $database->bind(':email', $email);
                                            $database->execute();
                                            $name = $database->single();
                                            ?>
                                            <tr>
                                                <td>
                                                    <span><input class="email_checkbox" type="checkbox" value="<?php echo $row['username']; ?>"></span>
                                                </td>
                                                <td>
                                                    <span><a href="javascritp:;" class="btn-view" data-id="<?php echo $row['id']; ?>"><?php echo $row['username']; ?></a></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $name['fullName']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $row['title']; ?></span>
                                                </td>

                                                <td>
                                                    <span><?php echo $row['platform']; ?></span>
                                                </td>
                                                <td style="text-align:center">
                                                    <a href="javascript:;" class="btn btn-danger btn-delete" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                                </td>

                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } else { 
                                    echo "<h2 style='margin-left: 20px'>List of user feedback will be displayed here.</h2>"; 
                                } ?>

                            </div><!-- aj_sec -->
                        </div><!-- postSection -->

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="aj_data"></div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Send Email</h4>
        </div>
        <div class="modal-body">

            <form class="form-horizontal myform">
                <input type="hidden" class="emails">
                <div class="form-group">
                    <label for="l2" class="col-sm-2 control-label"><h4 class="tb_title">Message</h4></label>
                    <div class="col-sm-10">
                        <textarea name="editor1" class="editor1 message form-control"></textarea>
                    </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary sendMail">Send</button>
        </div>
    </div>
</div>
</div>


<?php
include_once ('includes/footer.php'); ?>
<script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;

        var roxyFileman = 'plugins/ckeditor/fileman/index.html'; 
        CKEDITOR.replace( 'editor1',{filebrowserBrowseUrl:roxyFileman,
            filebrowserImageBrowseUrl:roxyFileman+'?type=image',
            removeDialogTabs: 'link:upload;image:upload'}); 

        var idArray = [];
        $(document).on('click', ".email_checkbox", function(){
            if($(this).is(':checked'))
            {
              idArray.push($(this).val());
              $(".emails").val(idArray);
              
          } else 
          {
            var text = $(this).val();
            pos = getPosition(idArray, text);
            
            var remove = idArray[pos];
            idArray = jQuery.grep(idArray, function(value) {
              return value != remove;
          });

            var newVal = removeValue($(".emails").val(), remove);
            $(".emails").val(newVal);
        }
    });

        $('#selectall').click(function(event) { 
            idArray = [];
            if(this.checked) { 
                $('.email_checkbox').each(function() { 
                    this.checked = true;
                    idArray.push($(this).val());

                });
                $(".emails").val(idArray);
            }else{
                $('.email_checkbox').each(function() { 
                    this.checked = false; 
                });
                $(".emails").val(''); 
                var idArray = [];
            }
        });



        $(".add_entry").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.sendMail', function(){
            var emails = $(".emails").val();
            var textareaData = CKEDITOR.instances.editor1.getData();
            mesg = textareaData.escapeHTML();
            //var mesg = $(".message").val();
            if(emails == "")
            {
                showNotification("warning", "Please select atleast one email.");
                return false;
            }
            showLoader();
            $.ajax({
                type: 'POST',
                url: 'includes/processing.php',
                data: 'emails=' + emails + '&mesg=' + encodeURIComponent(mesg) + '&command=' + 'feedbackEmail',
                cache: false,
                success: function(data) {
                    //alert(data);
                    hideLoader();
                    if(data == 'correct')
                    {
                        $("#myModal").modal('hide');
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec");
                        CKEDITOR.instances.editor1.setData('');
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  

$(document).on('click', '.btn-view', function(){
    showLoader();
    var id = $(this).data('id');
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getFeedback&id="+id+"", function(){
            $("#myEditModal").modal('show');
            hideLoader();
        });
    }, 500);
});

$(document).on('click', '.arrow-left', function(){
    showLoader();
    var id = $(this).data('id');
    $(".aj_data").load("aj_data.php?command=getFeedback&id="+id+"", function(){
        hideLoader();
    });
});
$(document).on('click', '.arrow-right', function(){
    showLoader();
    var id = $(this).data('id');
    $(".aj_data").load("aj_data.php?command=getFeedback&id="+id+"", function(){
        hideLoader();
    });
});

$(document).on('click', '.btn-delete', function(){
    if(!confirm('Are you sure, you want to delete feedback?')) return false;
    db = "id";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: 'includes/shop_processing.php',
        data: 'table=' + 'userfeedback' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            hideLoader();
            if(data == 'correct')
            {
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec"); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

});

</script>