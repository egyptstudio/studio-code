<?php 
$db = new Database();
$_SESSION['lib'] = isset($_SESSION['lib']) ? $_SESSION['lib'] : ''; 
$url = "http://". $_SERVER['SERVER_NAME'] ."/barca_out/admin";

$db->query('SELECT * FROM admin WHERE id = :id');
$db->bind(":id", $log_id);
$db->execute();
$adminRow = $db->single();

$priArr = explode("-", $adminRow['privileges']);

if(!empty($_SESSION['lib']))
{
  if(!in_array($_SESSION['lib'], $priArr)) {
   echo '<script>window.location.href="http://barca.tawasoldev.com/barca_out/admin/login.php"</script>';  
 }
}

?>
<a href="javascript:;" class="open"><i class="fa fa-bars"></i></a>
<div class="col-md-3 mymenus">

  <div class="well well-sm">
    <a href="javascript:;" class="menuClose"><i class="fa fa-bars"></i></a>
    <ul class="nav nav-list">
      <li><label class="nav-header">Welcome <?php echo $name; ?></label></li>
      
      <?php if($log_id == 1) { ?>
      <li class="nav-divider"></li>
      <li><label class="tree-toggler nav-header">User Management</label>
        <ul id="sub" class="nav nav-list tree">
          <li><a <?php if($_SESSION['lib'] == 30) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/user/admins.php">Users Management</a></li>
        </ul>
      </li>
      <?php } ?>
      
      <?php if(count(array_intersect(array('1', '2'), $priArr)) > 0) { ?>
      <li class="nav-divider"></li>
      <li><label class="tree-toggler nav-header">Libraries</label>
        <ul id="sub" class="nav nav-list tree">
          <li><a <?php if(!in_array('1', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 1) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/players.php">Players</a></li>
          <li><a <?php if(!in_array('2', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 2) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/teams.php">Teams</a></li>
        </ul>
      </li>
      <?php } ?>

      <?php if(in_array('28', $priArr)) { ?>
      <li class="nav-divider"></li>
      <li><label class="tree-toggler nav-header">Wall Management</label>
        <ul id="sub" class="nav nav-list tree">
          <li><a <?php if(!in_array('28', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 28) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/wall/posts.php">Latest Posts</a></li>
        </ul>
      </li>
      <?php } ?>
      
      <?php if(count(array_intersect(array('3', '4', '5'), $priArr)) > 0) { ?>
      <li class="nav-divider"></li>
      <li><label class="tree-toggler nav-header">FCB Users</label>
        <ul id="sub" class="nav nav-list tree">
          <li><a <?php if(!in_array('3', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 3) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/users.php?id=3">FCB_DUMMY</a></li>
          <li><a <?php if(!in_array('4', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 4) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/users.php?id=4">FCB_APP</a></li>
          <li><a <?php if(!in_array('5', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 5) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/users.php?id=5">FCB_STD</a></li>
        </ul>
      </li>
      <?php } ?>

      <?php if(count(array_intersect(array('6', '7', '8', '10', '11', '24', '9', '14'), $priArr)) > 0) { ?>
      <li class="nav-divider"></li>
      <li><label class="tree-toggler nav-header">General Settings</label>
        <ul id="sub" class="nav nav-list tree">
          <li><a <?php if(!in_array('6', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 6) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/general/points_management.php">Points Management</a></li>
          <li><a <?php if(!in_array('7', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 7) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/general/contests.php">Contest Management</a></li>
          <li><a <?php if(!in_array('8', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 8) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/general/notifications.php">Notifications</a></li>
          <li><a <?php if(!in_array('10', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 10) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/moderation/posts.php">Moderation</a>
            <li><a <?php if(!in_array('11', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 11) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/shop/shop_items.php">Shop Items</a>
              <li><a <?php if(!in_array('24', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 24) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/storelinks.php">Store Links</a>
                <li><a <?php if(!in_array('29', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 29) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/general/generallinks.php">General Links</a>
                  <li><a <?php if(!in_array('9', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 9) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/general/help.php">Help Screens</a></li>
                  <li><a <?php if(!in_array('14', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 14) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/view_feedback.php">View Feedback</a></li>
                  <li><a <?php if(!in_array('31', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 31) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/general/countries.php">Countries</a></li>
                  <li><a <?php if(!in_array('32', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 32) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/general/cities.php">Cities</a></li>
                </ul>
              </li>
              <?php } ?>

              <?php if(count(array_intersect(array('12', '13'), $priArr)) > 0) { ?>
              <li class="nav-divider"></li>
              <li><label class="tree-toggler nav-header">Version and Server</label>
                <ul id="sub" class="nav nav-list tree">
                  <li><a <?php if(!in_array('12', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 12) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/version/version.php">Version Control</a></li>
                  <li><a <?php if(!in_array('13', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 13) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/version/server.php">Server Control</a></li>
                </ul>
              </li>
              <?php } ?>

              <?php if(count(array_intersect(array('25', '26', '27'), $priArr)) > 0) { ?>
              <li class="nav-divider"></li>
              <li><label class="tree-toggler nav-header">FAQ's</label>
                <ul id="sub" class="nav nav-list tree">
                  <li><a <?php if(!in_array('25', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 25) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/faq/categories.php">Categories</a></li>
                  <li><a <?php if(!in_array('26', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 26) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/faq/questions.php">Questions</a></li>
                  <li><a <?php if(!in_array('27', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 27) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/faq/answers.php">Answers</a></li>
                </ul>
              </li>
              <?php } ?>

              <?php if(count(array_intersect(array('15', '16', '17', '18', '19', '20', '21', '22', '23'), $priArr)) > 0) { ?>
              <li class="nav-divider"></li>
              <li><label class="tree-toggler nav-header">Website</label>
                <ul id="sub" class="nav nav-list tree">
                  <li><a <?php if(!in_array('15', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 15) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/subscriptions.php">Website Subscriptions</a></li>
                  <li><a <?php if(!in_array('16', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 16) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/configuration.php">Configurations</a></li>
                  <li><a <?php if(!in_array('17', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 17) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/about.php">About Content</a></li>
                  <li><a <?php if(!in_array('18', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 18) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/registrations.php">Registrations</a></li>
                  <li><a <?php if(!in_array('19', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 19) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/sponsors.php">Sponsors</a></li>
                  <li><a <?php if(!in_array('20', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 20) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/contacts.php">Contact Us</a></li>
                  <li><a <?php if(!in_array('21', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 21) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/gallery.php">Gallery</a></li>
                  <li><a <?php if(!in_array('21', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 33) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/tawasol_gallery.php">Tawasol Gallery</a></li>
                  <li><a <?php if(!in_array('22', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 22) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/platform.php">Platform</a></li>
                  <li><a <?php if(!in_array('23', $priArr)) { echo 'style="display:none"'; } if($_SESSION['lib'] == 23) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/website/os.php">Operating System</a></li>
                </ul>
              </li>
              <?php } ?>

            </ul>
          </div>
        </div>