<?php
error_reporting(E_ALL | E_WARNING | E_NOTICE);
ini_set('display_errors', TRUE);
include_once('functions.php');
include_once('class.phpmailer.php');
include_once('class.smtp.php');
$db = new Database();
$command = $_REQUEST['command'];

if ($command == 'sendNotification') {

    $date = date('Y-m-d', strtotime($_REQUEST['date']));
    $time = date('H:i:s', strtotime($_REQUEST['time']));
    $logincheck = $_REQUEST['logincheck'];
    $lastlogin = date('Y-m-d H:i:s', strtotime($_REQUEST['lastlogin']));
    $sendto = $_REQUEST['sendto'];
    $country = $_REQUEST['country'];
    $gender = $_REQUEST['gender'];
    $sendvia = $_REQUEST['sendvia'];
    $mesg = strip_tags($_REQUEST['mesg']);

    $db->query('SELECT * FROM user WHERE userId = 1221');
    $db->execute();
    $senderRow = $db->single();
    //echo $country;

    if($sendto == 'all')
    {
        if($country == 'all')
        {
            $db->query('SELECT pushnotificationtoken, devicetype, fullName, email, userId FROM user WHERE lastLoginDate '.$logincheck.' :lastlogin AND gender = :gender');
        } else {
            $db->query('SELECT pushnotificationtoken, devicetype, fullName, email, userId FROM user WHERE lastLoginDate '.$logincheck.' :lastlogin AND gender = :gender AND countryId = :countryId');
            $db->bind(":countryId", $country);
        }
        
    } else {
        if($country == 'all')
        {
            $db->query('SELECT pushnotificationtoken, devicetype, fullName, email, userId FROM user WHERE lastLoginDate '.$logincheck.' :lastlogin AND gender = :gender AND userId IN('.$sendto.')');
        } else {
            $db->query('SELECT pushnotificationtoken, devicetype, fullName, email, userId FROM user WHERE lastLoginDate '.$logincheck.' :lastlogin AND countryId = :countryId AND gender = :gender AND userId IN('.$sendto.')');
            $db->bind(":countryId", $country);
        }
        
    }

    $db->bind(":lastlogin", $lastlogin);
    $db->bind(":gender", $gender);
    $db->execute();
    //echo $db->rowCount();
    $result = $db->resultset();
    foreach ($result as $row) {
        /*$db->query('INSERT INTO messages (messageText, senderUserID, recieverUserID, type, sentAt) VALUES (:msgTxt, :senderID, :recieverID, 0, :datetime)');
        $db->bind(":msgTxt", $mesg);
        $db->bind(":senderID", 1221);
        $db->bind(":recieverID", $row['userId']);
        $db->bind(":datetime", $date." ".$time);
        $db->execute();
        $mesgID = $db->lastInsertId();
        */
        $token = trim($row['pushnotificationtoken']);
        $type = $row['devicetype'];
        $dateTime = strtotime($date." ".$time);
        
        if($sendvia == 'notification')
        {
            if ($type == 2) {
                push_iphone($token, $mesg, 7, null, $senderRow['fullName'], $senderRow['profilePic'], $senderRow['userId'], $dateTime, $row['userId']);
            } else {
                push_android($token, $mesg, 7, null, $senderRow['fullName'], $senderRow['profilePic'], $senderRow['userId'], $dateTime, $row['userId']);
            }
        } else if($sendvia == 'email')
        {
            sendEmail($row['email'], $mesg, $row['fullName'], 'Email');
        } else {
            if ($type == 2) {
                push_iphone($token, $mesg, 7, null, $senderRow['fullName'], $senderRow['profilePic'], $senderRow['userId'], $dateTime, $row['userId']);
            } else {
                push_android($token, $mesg, 7, null, $senderRow['fullName'], $senderRow['profilePic'], $senderRow['userId'], $dateTime, $row['userId']);
            }
            sendEmail($row['email'], $mesg, $row['fullName'], 'Email');
        }
    }
    
    $db->query('INSERT INTO send_notifications (sendDate, sendTime, sendTo, sendVia, message) VALUES (:edate, :etime, :sendto, :sendvia, :mesg)');
    $db->bind(":edate", $date);
    $db->bind(":etime", $time);
    $db->bind(":sendto", $sendto);
    $db->bind(":sendvia", $sendvia);
    $db->bind(":mesg", $mesg);
    if ($db->execute()) {
        echo 'correct';
    } else {
        echo 'incorrect';
    }

}


function push_android($deviceRegistrationId, $messageText, $type, $mesgID, $name, $img, $senderID, $sendAt, $recieverID) {
    $apiKey = 'AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs';
    $headers = array(
        "Content-Type:" . "application/json",
        "Authorization:" . "key=" . $apiKey
        );
    $data = array(
        'data' => array(
            'message' => $messageText,
            'type' => $type,
            'mID' => $mesgID,
            'sName' => $name,
            'sPic' => $img,
            'sID' => $senderID,
            'sAt' => $sendAt,
            'rID' => $recieverID
            ) ,
        'registration_ids' => array(
            $deviceRegistrationId
            )
        );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send");
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

    $response = curl_exec($ch);
    curl_close($ch);

        //echo $response;

}

function push_iphone($deviceToken, $message, $type, $mesgID, $name, $img, $senderID, $sendAt, $recieverID) {

    $passphrase = "tawasolit";
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'fcb.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
    $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        //$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)exit();// exit("Failed to connect: $err $errstr" . PHP_EOL);
        
        //echo 'Connected to APNS' . PHP_EOL;
        
        // Create the payload body
        //$body['aps'] = array('alert' => array('body' => $message, 'action-loc-key' => 'Bango App', ), 'badge' => 2, 'sound' => 'oven.caf', );
        
        $body['aps']['alert'] = array(
            //'action-loc-key' => 'view',
            'body' => $message
            );
        //$body['aps']['sound'] = 'default';
        $body['aps']['type'] = $type;
        $body['aps']['mID'] = $mesgID;
        $body['aps']['sName'] = $name;
        $body['aps']['sPic'] = $img;
        $body['aps']['sID'] = $senderID;
        $body['aps']['sAt'] = $sendAt;
        $body['aps']['rID'] = $recieverID;
        
        $body['badge'] = '1';
        
        // Encode the payload as JSON
        $payload = json_encode($body,JSON_UNESCAPED_UNICODE);
        
        //print_r($payload);
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        
        //if (!$result) echo 'Message not delivered' . PHP_EOL;
        //else echo 'Message successfully delivered' . PHP_EOL . '<br>';
        
        // Close the connection to the server
        fclose($fp);
    }

    function sendEmail($senderEmail, $mesg, $fullName, $subject)
    {
        $mail = new PHPMailer();
        $mail->IsSMTP(); 
            
        $mail->SMTPAuth   = true;         
        $mail->Host       = "smtp.gmail.com";
        $mail->SMTPSecure = 'tls'; 
        $mail->Port       = 587;
        $mail->Username   = "no-reply@fcbstudio.info";
        $mail->Password   = "T@W@$0lIT"; 

        $mail->AddReplyTo('no-reply@fcbstudio.info');
        $mail->SetFrom('no-reply@fcbstudio.info');
        $mail->AddAddress($senderEmail, $fullName);
        $mail->Subject = $subject;
        $mail->Body =  html_entity_decode($mesg);
        $mail->IsHTML(true);
        $mail->Send();
    }

    ?>