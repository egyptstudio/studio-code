$(function(){
	//Users
    var userArray = [];
    var idArray = [];
    $(document).on('click', '.user_checkbox', function() {
        
        if($(this).is(':checked'))
        {
            userArray.push($(this).val());
            $("#users").val(userArray);

            idArray.push($(this).data('id'));
            $("#users").attr('data-id', idArray);
        } else 
        {
            text = $(this).val();
            pos = getPosition(userArray, text);
        
            userArray = jQuery.grep(userArray, function(value) {
                return value != text;
            });
            
            var remove = idArray[pos];
            idArray = jQuery.grep(idArray, function(value) {
                return value != remove;
            });
            
            var newVal = removeValue($("#users").attr('data-id'), remove);
            $("#users").attr('data-id', newVal);
        }
    });
    $(document).on('click', '.select_user', function() {
        //$('input#players').tagsinput('removeAll');
        for (i = 0; i < userArray.length; i++) {
            $('input#users').tagsinput('add', userArray[i]);
        }
        $(".user_selection input").removeAttr('placeholder');
        $("#myModal").modal('hide');
    });

    $(document).on('click', '.cancel_user', function(){
        $("#users").attr('data-id', '');
        $('input#users').tagsinput('removeAll');
        userArray = [];
        idArray = [];
    });

    $(document).on('click', '.user_selection span.tag span', function(){
        var text = $(this).parent('span').text();
        var pos = getPosition(userArray, text);
       
        userArray = jQuery.grep(userArray, function(value) {
            return value != text;
        });
        var remove = idArray[pos];
        var newVal = removeValue($("#users").attr('data-id'), remove);
        $("#users").attr('data-id', newVal);
    });


    //Country
    var countryArray = [];
    var countryIdArr = [];
    $(document).on('click', '.country_checkbox', function() {
        
        if($(this).is(':checked'))
        {
            countryArray.push($(this).val());
            $("#country").val(countryArray);

            countryIdArr.push($(this).data('id'));
            $("#country").attr('data-id', countryIdArr);
        } else 
        {
            text = $(this).val();
            pos = getPosition(countryArray, text);
        
            countryArray = jQuery.grep(countryArray, function(value) {
                return value != text;
            });
            
            var remove = countryIdArr[pos];
            countryIdArr = jQuery.grep(countryIdArr, function(value) {
                return value != remove;
            });
            
            var newVal = removeValue($("#country").attr('data-id'), remove);
            $("#country").attr('data-id', newVal);
        }
    });
    $(document).on('click', '.select_country', function() {
        //$('input#players').tagsinput('removeAll');
        for (i = 0; i < countryArray.length; i++) {
            $('input#country').tagsinput('add', countryArray[i]);
        }
        $(".country_selection input").removeAttr('placeholder');
        $("#myModal").modal('hide');
    });

    $(document).on('click', '.cancel_country', function(){
        $("#country").attr('data-id', '');
        $('input#country').tagsinput('removeAll');
        countryArray = [];
        countryIdArr = [];
    });

    $(document).on('click', '.country_selection span.tag span', function(){
        var text = $(this).parent('span').text();
        var pos = getPosition(countryArray, text);
       
        countryArray = jQuery.grep(countryArray, function(value) {
            return value != text;
        });
        var remove = countryIdArr[pos];
        var newVal = removeValue($("#country").attr('data-id'), remove);
        $("#country").attr('data-id', newVal);
    });
});