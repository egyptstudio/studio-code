$(document).ready(function() {
    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    var fileName = loc.substring(loc.lastIndexOf('/') + 1);
    var topLoc = window.top.location.pathname;
    var qs = window.top.location.search;

    $('#selectall').click(function(event) { 
        if (this.checked) { 
            $('.post_photo').each(function() { 
                this.checked = true; 
            });
        } else {
            $('.post_photo').each(function() { 
                this.checked = false;
            });
        }
    });

    //Save user
    $(".save_user").click(function(event){
        event.preventDefault();
        if($(".ori_file").val() == "" || $(".fullName").val() == "" || $(".email").val() == "")
        {
            showNotification('error', 'Please fill all mandatory fields!');
            return false;
        }
        showLoader();
        var arrFields = $("#save_user_form .getValue").map(function() {
            var $elm = $(this),
                childId = $elm.data('key');

            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        var arrValues = $("#save_user_form .getValue").map(function() {
            /*if ($(this).val().length == 0) {
                $(this).val('--');
            }*/
            return this.value.escapeHTML();
        }).get();

        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'saveUserInfo',
            cache: false,
            success: function(data) {

                hideLoader();
                var arr = data.split('~');
                if(arr[0] == 'correct')
                {
                    $("#userid").val(arr[1]);
                    showNotification('success', 'User created successfully, Now you can add posts!');
                    $(".ajax-file-upload-statusbar").hide();
                    $("#save_user_form")[0].reset();

                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });

    });
    //Save user

    //Check Email
    $(".email").blur(function(){
        var email = $(".email").val();
        if(email == "")
        {
            return false;
        }
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'email=' + encodeURIComponent(email) + '&command=' + 'checkEmail',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                    $(".save_user").prop('disabled', false);
                } else 
                {
                    showNotification('error', 'Email already exisits!');
                    $(".save_user").prop('disabled', true);
                }
            }
        });
    });

    //CountryDDL
    $(".countryDDL").change(function(){
        var id = $(this).val();
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'id=' + encodeURIComponent(id) + '&command=' + 'getCities',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "")
                {
                    $(".cityData").html('<option value="">No City</option>');
                } else 
                {
                    $(".cityData").html(data);
                }
            }
        });
    });

    //Post and following check
    $(".check_btn").click(function(event){
        event.preventDefault();
        if($("#userid").val() == "")
        {
            showNotification('error', 'First save user basic info!');
        } else 
        {
            window.location.href = $(this).attr('href');
        }
    });

    //Delete Post
    $(".delPost").click(function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        var file = $(this).attr('data-file');
        if(!confirm("Are your sure you want to delete these post?")) return false;
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'id=' + id + '&file=' + file + '&userid=' + $("#userid").val() + '&command=' + 'delPost',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Post deleted successfully!");
                    $(".postSection").load('posts.php .aj_sec');
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });

    //Delete selected Post
    $(".delSelectedPost").click(function(event){
        event.preventDefault();
        if($(".post_photo:checked").val() == null)
        {
            showNotification('error', 'Please select post first!');
            return false;
        }
        if(!confirm("Are your sure you want to delete these posts?")) return false;
        var arrIds = $(".post_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        var arrFiles = $(".post_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('file');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();
        
        showLoader();

        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'userid=' + $("#userid").val() + '&ids=' + arrIds + '&files=' + arrFiles + '&command=' + 'delSelectedPost',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Post(s) deleted successfully!");
                    $(".postSection").load('posts.php .aj_sec');
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });

    //Follow user
    $(".followUser").click(function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        var arrFields = ['userId', 'fanId', 'isFollow'];
        var arrValues = [$("#userid").val(), id, 1];
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'table=' + 'userfans' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&pageName=' + '' + '&command=' + 'add',
            cache: false,
            success: function(data) {
                alert(data);
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Process done successfully!");
                    $(".postSection").load('following.php .aj_sec');
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });

    //Follow selected User
    $(".followSelectedUser").click(function(event){
        event.preventDefault();
        if($(".post_photo:checked").val() == null)
        {
            showNotification('error', 'Please select user first!');
            return false;
        }
        var arrIds = $(".post_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        showLoader();

        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'userid=' + $("#userid").val() + '&ids=' + arrIds + '&command=' + 'followSelectedUser',
            cache: false,
            success: function(data) {
                alert(data);
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Post(s) deleted successfully!");
                    $(".postSection").load('posts.php .aj_sec');
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });

}); // Main
function showLoader() {
    $("#overlay").show();
    $("#loader").show();
}

function hideLoader() {
    $("#overlay").hide();
    $("#loader").hide();
}

function showNotification(type, text) {
    if (type == 'error') {
        var notiClass = "alert-danger";
    } else if (type == 'success') {
        var notiClass = "alert-success";
    } else if (type == 'warning') {
        var notiClass = "alert-warning";
    } else if (type == 'info') {
        var notiClass = "alert-info";
    }
    $(".alert").addClass(notiClass);
    $(".alert strong").text(text);
    $(".alert").fadeIn().delay(3000).fadeOut();
}