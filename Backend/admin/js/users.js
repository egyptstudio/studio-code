$(document).ready(function() {
    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    var fileName = loc.substring(loc.lastIndexOf('/') + 1);
    var topLoc = window.top.location.pathname;
    var qs = window.top.location.search;

    $('#selectall').click(function(event) { 
        if (this.checked) { 
            $('.post_photo').each(function() { 
                this.checked = true; 
            });
        } else {
            $('.post_photo').each(function() { 
                this.checked = false;
            });
        }
    });

    //Save user
    $(".save_user").click(function(event){
        event.preventDefault();
        if($(".ori_file").val() == "" || $(".fullName").val() == "" || $(".email").val() == "")
        {
            showNotification('error', 'Please fill all mandatory fields!');
            return false;
        }
        showLoader();
        var arrFields = $("#save_user_form .getValue").map(function() {
            var $elm = $(this),
                childId = $elm.data('key');

            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        var arrValues = $("#save_user_form .getValue").map(function() {
            /*if ($(this).val().length == 0) {
                $(this).val('--');
            }*/
            return this.value.escapeHTML();
        }).get();

        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'saveUserInfo',
            cache: false,
            success: function(data) {

                hideLoader();
                var arr = data.split('~');
                if(arr[0] == 'correct')
                {
                    $("#userid").val(arr[1]);
                    showNotification('success', 'User created successfully, Now you can add posts!');
                    $(".ajax-file-upload-statusbar").hide();
                    $("#save_user_form")[0].reset();

                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });

    });
    //Save user

    //Check Email
    $(".email").blur(function(){
        var email = $(".email").val();
        if(email == "")
        {
            return false;
        }
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'email=' + encodeURIComponent(email) + '&command=' + 'checkEmail',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                    $(".save_user").prop('disabled', false);
                } else 
                {
                    showNotification('error', 'Email already exisits!');
                    $(".save_user").prop('disabled', true);
                }
            }
        });
    });

    //CountryDDL
    $(".countryDDL").change(function(){
        var id = $(this).val();
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'id=' + encodeURIComponent(id) + '&command=' + 'getCities',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "")
                {
                    $(".cityData").html('<option value="">No City</option>');
                } else 
                {
                    $(".cityData").html(data);
                }
            }
        });
    });

    //Post and following check
    $(".check_btn").click(function(event){
        event.preventDefault();
        if($("#userid").val() == "")
        {
            showNotification('error', 'First save user basic info!');
        } else 
        {
            window.location.href = $(this).attr('href');
        }
    });

    //Follow user
    $(".followUser").click(function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        var arrFields = ['userId', 'fanId', 'isFollow'];
        var arrValues = [$("#userid").val(), id, 1];
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'table=' + 'userfans' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&pageName=' + '' + '&command=' + 'add',
            cache: false,
            success: function(data) {
                alert(data);
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Process done successfully!");
                    $(".postSection").load('following.php .aj_sec');
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });

    //Follow selected User
    $(".followSelectedUser").click(function(event){
        event.preventDefault();
        if($(".post_photo:checked").val() == null)
        {
            showNotification('error', 'Please select user first!');
            return false;
        }
        var arrIds = $(".post_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        showLoader();

        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'userid=' + $("#userid").val() + '&ids=' + arrIds + '&command=' + 'followSelectedUser',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Post(s) deleted successfully!");
                    $(".postSection").load('following.php .aj_sec');
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });
    
    //unFollow User
    $(document).on('click', '.unfollowUser', function(){
        var id = $(this).attr('data-id');
        var arrFields = ['id'];
        var arrValues = [id];
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'table=' + 'userfans' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&pageName=' + '' + '&command=' + 'delete',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Process done successfully!");
                    $(".postSection").load('edit_following.php .aj_sec');
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });

    //unFollow selected user
    $(".unfollowSelectedUser").click(function(event){
        event.preventDefault();
        if($(".post_photo:checked").val() == null)
        {
            showNotification('error', 'Please select user first!');
            return false;
        }
        var arrIds = $(".post_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'ids=' + arrIds + '&command=' + 'unfollowSelectedUser',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Post(s) deleted successfully!");
                    $(".postSection").load('edit_following.php .aj_sec');
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });

    //Image upload
    $(".openUploader").click(function(event){
        event.preventDefault();
        if($(this).hasClass('showUploader'))
        {
            $(this).removeClass('showUploader');
            $(this).addClass('hideUploader');
            $(".player_img").fadeIn();
            $(".uploaders").hide();
            $(this).text('Change Picture');
        } else 
        {
            $(this).removeClass('hideUploader');
            $(this).addClass('showUploader');
            $(".player_img").hide();
            $(".uploaders").fadeIn();
            $(this).text('Cancel Upload');
        }
    });

    //edit user
    $(".edit_user").click(function(){
        var img1 = $(".old_ori_file").val(),
        oriImg = $(".ori_file").val();

        if( $(".fullName").val() == "")
        {
            showNotification('error', 'Please fill all mandatory fields.');
            return false;
        }

        if((img1 != oriImg && oriImg != ""))
        {
            command = "updateFile";
        } else
        {
            command = "update";
            oriImg = img1;
        }

        showLoader();
        var arrFields = $("#save_user_form .getValue").map(function() {
            var $elm = $(this),
                childId = $elm.data('key');

            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        var arrValues = $("#save_user_form .getValue").map(function() {
            return this.value.escapeHTML();
        }).get();

        arrFields.splice(0, 0, 'profilePic');
        arrValues.splice(0, 0, oriImg);

        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + 'userId' + '&value=' + $("#userid").val() + '&subCommand=' + command + '&command=' + 'editUserInfo',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                    showNotification('success', 'User updated successfully!');
                    $(".ajax-file-upload-statusbar").hide();
                    $(this).removeClass('showUploader');
                    $(this).addClass('hideUploader');
                    $(".player_img").fadeIn();
                    $(".uploaders").hide();
                    $(this).text('Change Picture');
                    $(".player_img").attr('src', 'https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/profile/'+oriImg);
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });

    });

}); // Main
function showLoader() {
    $("#overlay").show();
    $("#loader").show();
}

function hideLoader() {
    $("#overlay").hide();
    $("#loader").hide();
}

function showNotification(type, text) {
    if (type == 'error') {
        var notiClass = "alert-danger";
    } else if (type == 'success') {
        var notiClass = "alert-success";
    } else if (type == 'warning') {
        var notiClass = "alert-warning";
    } else if (type == 'info') {
        var notiClass = "alert-info";
    }
    $(".alert").addClass(notiClass);
    $(".alert strong").text(text);
    $(".alert").fadeIn().delay(3000).fadeOut();
}