<?php
include_once ('includes/check.php');
include_once ('includes/header.php');

if(!isset($_REQUEST['id']) || $_REQUEST['id'] == "" || !is_numeric($_REQUEST['id']))
{
   header('location: users.php?id='.$_SESSION['lib'].'');
}
$id = $_REQUEST['id'];
$database->query("SELECT * FROM user WHERE userId = :id");
$database->bind(":id", $id);
$database->execute();
$row = $database->single();

$countryId = $row['countryId'];
$database->query('SELECT countryId, countryName FROM country WHERE countryId = :id');
$database->bind(":id", $countryId);
$database->execute();
$countryName = $database->single();

$cityId = $row['cityId'];
$database->query('SELECT cityId, name_en FROM city WHERE cityId = :id');
$database->bind(":id", $cityId);
$database->execute();
$cityName = $database->single();
                                  

$haystack = $row['profilePic'];
$needle = 'http';
if (strpos($haystack,$needle) !== false) {
    $imgPath = $haystack;
} else {
    $imgPath = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/profile/".$haystack;
}

$database->query('SELECT userId FROM post WHERE userId = :id');
$database->bind(":id", $id);
$database->execute();
$postCount = $database->rowCount();

$database->query('SELECT userId FROM userfans WHERE userId = :id AND isFollow = 1');
$database->bind(":id", $id);
$database->execute();
$followCount = $database->rowCount();

?>
<style type="text/css">
    .fa-arrow-right:before
    {
        float: right;
        margin-left: 10px;
    }
</style>
<div class="container-fluid">
    <div class="row">

        <?php
include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">User Details</h4>
                        <div class="head_opts">
                            <a href="edit_user.php?id=<?php echo $id ?>" class="btn btn-default btn-black">Edit</button>
                            <a href="users.php?id=<?php echo $_SESSION["lib"] ?>" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <form class="form-horizontal myform" id="save_user_form">
                        <div class="form-group">
                            <label for="l1" class="col-sm-3 control-label"><h4 class="tb_title">Profile Picture</h4></label>
                            <div class="col-sm-9">
                                <img class="img-responsive" src="<?php echo $imgPath; ?>" alt="User">
                            </div>
                        </div>
                        
                        <div class="form-group border_top">
                            <label for="l4" class="col-sm-3 control-label"><h4 class="tb_title">Full Name</h4></label>
                            <div class="col-sm-8">
                                <?php echo $row['fullName']; ?>
                            </div>
                        </div>
                        <div class="form-group border_top">
                            <label for="l5" class="col-sm-3 control-label"><h4 class="tb_title">Email</h4></label>
                            <div class="col-sm-8">
                                <?php echo $row['email']; ?>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title">Country</h4></label>
                            <div class="col-sm-8">
                              <?php echo $countryName['countryName']; ?>
                            </div>
                        </div>
                        <div class="form-group border_top">
                            <label for="l7" class="col-sm-3 control-label"><h4 class="tb_title">City</h4></label>
                            <div class="col-sm-8">
                              <?php echo $cityName['name_en']; ?>
                            </div>
                        </div>
                        
                        <div class="form-group border_top">
                            <label for="l8" class="col-sm-3 control-label"><h4 class="tb_title">District</h4></label>
                            <div class="date col-sm-8">
                                <?php echo $row['district']; ?>
                            </div>
                        </div>
                        

                        <div class="form-group border_top">
                            <label for="l8" class="col-sm-3 control-label"><h4 class="tb_title">Birthdate</h4></label>
                            <div class="col-sm-8">
                                <?php echo date("l jS \of F Y", strtotime($row['dateOfBirth'])); ?>
                            </div>
                        </div>
                        
                        <div class="form-group border_top">
                            <label for="l10" class="col-sm-3 control-label"><h4 class="tb_title">Education</h4></label>
                            <div class="date col-sm-8">
                                <?php echo $row['education']; ?>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l10" class="col-sm-3 control-label"><h4 class="tb_title">About me</h4></label>
                            <div class="date col-sm-8">
                                <?php echo $row['aboutMe']; ?>
                            </div>
                        </div>
                        

                    </form>
                    <br><br>
                    <a class="userLink" href="posts.php?id=<?php echo $id; ?>"><span class="pull-left">User Posts</span> <i class="pull-right fa fa-arrow-right"><?php echo $postCount; ?></i></a>
                    <a class="userLink" href="edit_following.php?id=<?php echo $id; ?>"><span class="pull-left">Following List</span> <i class="pull-right fa fa-arrow-right"><?php echo $followCount; ?></i></a>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="aj_data">
      </div>
    </div>
  </div>
</div>

<?php
include_once ('includes/footer.php'); ?>
<script src="plugins/datepicker/moment.js"></script>
<script src="plugins/datepicker /bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/users.js"></script>
<script type="text/javascript">
$(function(){
    $('#datetimepicker1').datetimepicker({
        pickTime: false,
        format: 'dddd, MMMM Do, YYYY'
    });
    
    var settings1 = {
        url: "includes/processing.php",
        dragDrop: false,
        dragDropStr: "<span><b>Drag & Drop File</b></span>",
        multiple: false,
        fileName: "myfile",
        formData: {
            "command": "uploadData",
            "dir": "third_party/uploads/profile/",
            "thumb": "No"
        },
        allowedTypes: "jpg,png,jpeg",
        returnType: "json",
        showFileCounter: false,
        showDone: false,
        maxFileCount: 1,
        onSuccess: function(files, data, xhr) {
            //alert(data);
            //$(id_ori+" .ajax-file-upload form:first").remove();
            $(".ori_file").val(data);
        },
        onSubmit: function(files) {
            //alert(files);
            setTimeout(function() {
                $("#loader, #overlay").hide();
            }, 100);
        },
        showDelete: true,
        deleteCallback: function(data, pd) {
            for (var i = 0; i < data.length; i++) {
                $.post("includes/processing.php", {
                    op: "delete",
                    dir: "third_party/uploads/profile/",
                    command: "deleteData",
                    name: data[i]
                }, function(resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#ori_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
            }
            pd.statusbar.hide(); //You choice to hide/not.
        }
    }
    var uploadObj = $("#original").uploadFile(settings1);

});
</script>