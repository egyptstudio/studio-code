<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$_SESSION['lib'] = 23;

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}
$startFrom = ($page-1) * $recPerPage;
$database->query('SELECT * FROM platform, operatingsystems WHERE operatingsystems.platformId = platform.id ORDER BY operatingsystems.id DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM platform, operatingsystems WHERE operatingsystems.platformId = platform.id ORDER BY operatingsystems.id DESC LIMIT :start, :num');

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Shop Items</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_entry btn-primary">Add New Entry &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-left">
                                  <ul class="pagination">
                                    <li>
                                      <a href="os.php?page=1" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <?php
                                if($page == 1)
                                {
                                    $prevPage = 1;
                                } else 
                                {
                                    $prevPage = ($page - 1);
                                }

                                if($page == $total_pages)
                                {
                                    $nextPage = $total_pages;
                                } else 
                                {
                                    $nextPage = ($page + 1);
                                }
                                echo '<li><a href="os.php?page='.$prevPage.'">&lt;</a></li>';
                                echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                echo '<li><a href="os.php?page='.$nextPage.'">&gt;</a></li>';
                                ?>
                                <li>
                                  <a href="os.php?page=<?php echo $total_pages ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                </div>

                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Platform Name</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <span><?php echo $row['platform']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['os']; ?></span>
                                    </td>
                                    <td style="text-align:center">
                                        <a href="javascript:;" class="del_entry btn btn-danger" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                    </td>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } else { 
                        echo "<h2 style='margin-left: 20px'>List of OS will be displayed here.</h2>"; 
                    } ?>

                </div><!-- aj_sec -->
            </div><!-- postSection -->

        </div>

    </div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Add Entry</h4>
        </div>
        <div class="modal-body">

            <form class="form-horizontal myform">
                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Platform Name</h4></label>
                    <div class="col-sm-8">
                        <select class="getValue form-control" data-key="platformId">
                            <?Php $database->query("SELECT * FROM platform ORDER BY id DESC");
                            $database->execute();
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                 echo '<option value='.$row['id'].'>'.$row['platform'].'</option>';
                             } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">OS Name</h4></label>
                    <div class="col-sm-8">
                        <input type="text" value="" class="getValue form-control" data-key="os">
                    </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary save_entry">Save</button>
        </div>
    </div>
</div>
</div>


<?php
include_once ('../includes/footer.php');  ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(".add_entry").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.save_entry', function(){

            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'operatingsystems' + '&db=' + arrFields + '&data=' + arrValues + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        $("#myModal").modal('hide');
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec"); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  
$(document).on('click', '.del_entry', function(){
    if(!confirm('Are you sure, you want to delete data?')) return false;
    db = "id";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'operatingsystems' + '&db=' + db + '&data=' + value + '&pageName=' + 'shop_items' + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            hideLoader();
            if(data == 'correct')
            {
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec"); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

});

</script>