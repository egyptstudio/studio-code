<?php
include_once ('../includes/check.php');
include_once ('header.php'); 

$_SESSION['lib'] = 20;

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}
$startFrom = ($page-1) * $recPerPage;
$database->query('SELECT * FROM web_contacts ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM web_contacts ORDER BY id DESC LIMIT :start, :num');

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Contacts</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn exportToExcel btn-primary">Export to Excel &nbsp;&nbsp;&nbsp;<i class="fa fa-arrow-down"></i></a>
                        </div>
                    
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-left">
                                  <ul class="pagination">
                                    <li>
                                      <a href="contacts.php?page=1" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <?php
                                if($page == 1)
                                {
                                    $prevPage = 1;
                                } else 
                                {
                                    $prevPage = ($page - 1);
                                }

                                if($page == $total_pages)
                                {
                                    $nextPage = $total_pages;
                                } else 
                                {
                                    $nextPage = ($page + 1);
                                }
                                echo '<li><a href="contacts.php?page='.$prevPage.'">&lt;</a></li>';
                                echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                echo '<li><a href="contacts.php?page='.$nextPage.'">&gt;</a></li>';
                                ?>
                                <li>
                                  <a href="contacts.php?page=<?php echo $total_pages ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                </div>

                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date Time</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <span><?php echo $row['name']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['email']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo date("d-m-Y H:i:s", strtotime($row['datetime'])); ?></span>
                                    </td>
                                    <td style="text-align:center">
                                        <a href="javascript:;" class="view_entry btn btn-info" data-id="<?php echo $row['id']; ?>"><i class="fa fa-info"></i> View</a>
                                        <a href="javascript:;" class="del_entry btn btn-danger" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                    </td>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } else { 
                        echo "<h2 style='margin-left: 20px'>List of Contacts will be displayed here.</h2>"; 
                    } ?>

                </div><!-- aj_sec -->
            </div><!-- postSection -->

        </div>

    </div>
</div>
</div>
</div>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data"></div>
    </div>
</div>
</div>


<?php
include_once ('footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        

        $(document).on('click', '.view_entry', function(){
            showLoader();
            var id = $(this).data('id');
            
            setTimeout(function(){
                $(".aj_data").load("../aj_data.php?command=getContact&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 700);
        });
        $(".exportToExcel").click(function(){
            window.location.href = 'contact_csv.php';
        });
        
        $(document).on('click', '.del_entry', function(){
            if(!confirm('Are you sure, you want to delete data?')) return false;
            db = "id";
            value = $(this).data('id');
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'web_contacts' + '&db=' + db + '&data=' + value + '&pageName=' + 'shop_items' + '&command=' + 'delete',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec"); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });

    });
</script>