<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php');
$database->query('SELECT * FROM web_configuration');
$database->execute();
$row = $database->single();
$count = $database->rowCount();
$_SESSION['lib'] = 16;
?>

<div class="container-fluid">
    <div class="row">

        <?php
        include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">Configurations</h4>
                        <div class="head_opts">
                            <button class="btn btn-default btn-black update_entry">Save</button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form class="form-horizontal myform">

                            <div class="form-group">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Library Version</h4></label>
                                <div class="col-sm-6">
                                    <input type="text" id="version" class="getValue form-control" value="<?php echo $row['version'] ?>" data-key="version">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Play Store</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="purl" class="getValue form-control" value="<?php echo $row['playstore_url'] ?>" data-key="playstore_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Apple Store</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="aurl" class="getValue form-control" value="<?php echo $row['appstore_url']; ?>" data-key="appstore_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Facebook</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="fburl" class="form-control getValue" value="<?php echo $row['facebook_url']; ?>" data-key="facebook_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Twitter</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="twurl" class="form-control getValue" value="<?php echo $row['twitter_url']; ?>" data-key="twitter_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Instagram</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="insturl" class="form-control getValue" value="<?php echo $row['instagram_url']; ?>" data-key="instagram_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">YouTube</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="youurl" class="form-control getValue" value="<?php echo $row['youtube_url']; ?>" data-key="youtube_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Google +</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="gplusurl" class="form-control getValue" value="<?php echo $row['googleplus_url']; ?>" data-key="googleplus_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Pinterest</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="pinurl" class="form-control getValue" value="<?php echo $row['pinterest_url']; ?>" data-key="pinterest_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Blog</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="blogurl" class="form-control getValue" value="<?php echo $row['blog_url']; ?>" data-key="blog_url">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Facebook</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="fburl2" class="form-control getValue" value="<?php echo $row['facebook_url2']; ?>" data-key="facebook_url2">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Twitter</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="twurl2" class="form-control getValue" value="<?php echo $row['twitter_url2']; ?>" data-key="twitter_url2">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Instagram</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="insturl2" class="form-control getValue" value="<?php echo $row['instagram_url2']; ?>" data-key="instagram_url2">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">YouTube</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="youurl2" class="form-control getValue" value="<?php echo $row['youtube_url2']; ?>" data-key="youtube_url2">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Google +</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="gplusurl2" class="form-control getValue" value="<?php echo $row['googleplus_url2']; ?>" data-key="googleplus_url2">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Pinterest</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="pinurl2" class="form-control getValue" value="<?php echo $row['pinterest_url2']; ?>" data-key="pinterest_url2">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Blog</h4></label>
                                <div class="col-sm-6">
                                    <input type="url" id="blogurl2" class="form-control getValue" value="<?php echo $row['blog_url2']; ?>" data-key="blog_url2">
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once ('../includes/footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(document).on('click', '.update_entry', function(e){
            e.preventDefault();
            if (!$(".myform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            
            var key = "id";
            var value = '1';
            <?php 
            if($count == 0)
                { ?>
                    var command = 'add';
                    <?php } else { ?>
                        var command = 'update';
                        <?php }    
                        ?>
                        showLoader();
                        $.ajax({
                            type: 'POST',
                            url: '../includes/shop_processing.php',
                            data: 'table=' + 'web_configuration' + '&db=' + arrFields + '&data=' + arrValues + '&key=' + key + '&value=' + value + '&command=' + command,
                            cache: false,
                            success: function(data) {
                                hideLoader();
                                if(data == 'correct')
                                {
                                    showNotification('success', 'Process done successfully!');
                                } else 
                                {
                                    showNotification('error', 'There is some error, Please contact administration!');
                                }
                            }
                        });
                    });

});
</script>