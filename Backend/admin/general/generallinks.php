<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$_SESSION['lib'] = 29;
$database->query('SELECT * FROM general_links ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">General Links</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_version btn-primary">Add New Link &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>

                            <div class="col-md-12">

                                <table class="table table-bordered userTable">
                                    <thead>
                                        <tr>
                                            <th>Language</th>
                                            <th>Name</th>
                                            <th>Link</th>
                                            <th style="text-align:center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $database->resultset();
                                        foreach ($result as $row) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <span><?php echo $row['lang']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $row['name']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $row['link']; ?></span>
                                                </td>
                                                <td style="text-align:center">
                                                    <a href="javascript:;" class="btn btn-info btn-edit" data-id="<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i> Edit</a>
                                                    <a href="javascript:;" class="btn btn-danger btn-delete" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                                </td>

                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } else { 
                                    echo "<h2 style='margin-left: 20px'>List of General link URLs will be displayed here.</h2>"; 
                                } ?>

                            </div><!-- aj_sec -->
                        </div><!-- postSection -->

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Link</h4>
            </div>
            <form class="form-horizontal myform">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Language</h4></label>
                        <div class="col-sm-8">
                            <select class="getValue form-control required" name="f1" data-key="lang">
                                <option value="en">EN</option>
                                <option value="es">ES</option>
                                <option value="ca">CA</option>
                                <option value="ar">AR</option>
								<option value="fr">FR</option>
								<option value="pt">PT</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Name</h4></label>
                        <div class="col-sm-8">
                            <input type="text" class="getValue required form-control" name="f2" data-key="name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> URL</h4></label>
                        <div class="col-sm-8">
                            <input type="url" class="getValue required form-control" name="f3" data-key="link">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save_url">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data"></div>
    </div>
</div>
</div>

<?php 
include_once ('../includes/footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(".add_version").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("../aj_data.php?command=getGeneralLink&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 700);
        });

        $(document).on('click', '.save_url', function(e){
            e.preventDefault();
            if (!$(".myform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            showLoader();

            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'general_links' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                         hideLoader();
                         $("#myModal").modal('hide');
                         $(".myform")[0].reset();
                     }); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  
$(document).on('click', '.btn-delete', function(){
    if(!confirm('Are you sure, you want to delete link?')) return false;
    db = "id";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'general_links' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
        cache: false,
        success: function(data) {

            if(data == 'correct')
            {
                $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                    hideLoader();
                    showNotification('success', 'Process done successfully!');
                    
                });
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('click', '.edit_url', function(e){
    e.preventDefault();
    if (!$(".myeditform").validate().form()) {
        return false;
    }
    var arrFields = $(".myeditform .getValue").map(function() {
        var $elm = $(this),
        childId = $elm.data('key');
        if (childId.length == 0) {
            alert("Error");
            return false;
        }
        return childId;
    }).get();
    var arrValues = $(".myeditform .getValue").map(function() {
        if ($(this).val().length == 0) {
            $(this).val('');
        }
        return this.value.escapeHTML();
    }).get();

    var key = "id";
    var value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'general_links' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + key + '&value=' + value + '&command=' + 'update',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                    $("#myEditModal").modal('hide');
                    hideLoader();
                }); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});  

});

</script>