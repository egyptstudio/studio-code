<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$_SESSION['lib'] = 32;
?>

<link rel="stylesheet" type="text/css" href="../plugins/dt/dataTables.bootstrap.min.css">
<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Cities</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_version btn-primary">Add New City &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="col-md-12">


                        <table id="cities" cellspacing="0" width="100%" class="table table-bordered userTable">
                            <thead>
                                <tr>
                                    <th>cityId</th>
                                    <th>Country</th>
                                    <th>City Name</th>
                                    <th style="text-align:center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Add City</h4>
        </div>
        <form class="form-horizontal myform">
            <div class="modal-body">

                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Country</h4></label>
                    <div class="col-sm-8">
                        <select class="form-control getValue" data-key="countryId">
                        <?php 
                        $db->query('SELECT countryName, countryId FROM country');
                        $db->execute();
                        $result = $db->resultSet();
                        foreach ($result as $countryRow) {
                                echo '<option value="'.$countryRow['countryId'].'">'.$countryRow['countryName'].'</option> ';
                        }
                        ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Country Code</h4></label>
                    <div class="col-sm-8">
                        <input type="text" class="getValue required form-control" name="f3" data-key="name_en">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary save_city">Save</button>
            </div>
        </form>
    </div>
</div>
</div>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data"></div>
    </div>
</div>
</div>

<?php 
include_once ('../includes/footer.php'); ?>
<script src="../plugins/dt/jquery.dataTables.min.js"></script>
<script src="../plugins/dt/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(function(){

        $(document).ready(function() {
            var table = $('#cities').DataTable( {
                "dom": 'pfrt',
                "processing": true,
                "serverSide": true,
                "ajax": "cities_data.php",
                "order": [[ 0, "desc" ]],
                "pageLength": 20,
                "columnDefs": [
                { "searchable": false, "visible": false, "targets": 0},
                { "searchable": false, "class": "centerAlign",  "orderable": false, "targets": 3 }
                ],
            } );
        } );


        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(".add_version").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("cities_data.php?command=edit&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 700);
        });

        $(document).on('click', '.btn-lang', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("cities_data.php?command=translate&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 700);
        });

        $(document).on('click', '.save_country', function(e){
            e.preventDefault();
            if (!$(".myform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            showLoader();

            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'city' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        $("#cities").DataTable().ajax.reload(null, false);
                        hideLoader();
                        $("#myModal").modal('hide');
                        $(".myform")[0].reset();

                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  
$(document).on('click', '.btn-delete', function(){
    if(!confirm('Are you sure, you want to delete link?')) return false;
    db = "cityId";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'city' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
        cache: false,
        success: function(data) {

            if(data == 'correct')
            {
                    $("#cities").DataTable().ajax.reload(null, false);
                    hideLoader();
                    showNotification('success', 'Process done successfully!');
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('click', '.edit_city', function(e){
    e.preventDefault();
    if (!$(".myeditform").validate().form()) {
        return false;
    }
    var arrFields = $(".myeditform .getValue").map(function() {
        var $elm = $(this),
        childId = $elm.data('key');
        if (childId.length == 0) {
            alert("Error");
            return false;
        }
        return childId;
    }).get();
    var arrValues = $(".myeditform .getValue").map(function() {
        if ($(this).val().length == 0) {
            $(this).val('');
        }
        return this.value.escapeHTML();
    }).get();

    var key = "cityId";
    var value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'city' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + key + '&value=' + value + '&command=' + 'update',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                showNotification('success', 'Process done successfully!');
                $("#cities").DataTable().ajax.reload(null, false);
                $("#myEditModal").modal('hide');
                hideLoader();
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});  

});

</script>