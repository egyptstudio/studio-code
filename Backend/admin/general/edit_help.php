<?php
include_once ('../includes/check.php');
include_once ('header.php');
if(!isset($_REQUEST['id']) || $_REQUEST['id'] == "" || !is_numeric($_REQUEST['id']))
{
    header('location: help.php');
}

$id = $_REQUEST['id'];
$database->query('SELECT * FROM helpscreen WHERE screenId = :id');
$database->bind(":id", $id);
$database->execute();
$row = $database->single();
?>
<link rel="stylesheet" href="../plugins/datepicker/bootstrap-datetimepicker.min.css">
<div class="container-fluid">
    <div class="row">

        <?php
include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            <input type="hidden" id="screenId" value="<?php echo $id; ?>">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">Edit Screen</h4>
                        <div class="head_opts">
                            <button class="btn btn-default btn-black edit_help">Save</button>
                            <a href="help.php" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <form class="form-horizontal myform">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Are Mandatory fields</h4></label>
                        </div>
                        
                        <div class="form-group border_top">
                            <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span>Image</h4></label>
                            <div class="col-sm-7">
                                <div class="prizeImgSec">
                                    <img class="img-responsive" src="<?php echo $row['screenImageUrl']; ?>">
                                    <a href="#" class="openUploader btn btn-primary">Change Image</a>
                                    <input type="hidden" class="old_file1" value="<?php echo $row['screenImageUrl']; ?>">
                                </div>
                                <div class="prizeUploader">
                                    <div class="col-sm-12">
                                        <div class="original">
                                            <div id="s_image">Upload</div>
                                            <div class="uploaderStatus" id="simage_file"></div>
                                        </div>
                                        <input type="hidden" id="simage_fileName" class="getValue" data-key="screenImageUrl">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span>Image</h4></label>
                            <div class="col-sm-7">
                                <div class="prizeImgSec">
                                        <img class="img-responsive" src="<?php echo $row['screenIconUrl']; ?>">
                                        <a href="#" class="openUploader btn btn-primary">Change Image</a>
                                        <input type="hidden" class="old_file2" value="<?php echo $row['screenIconUrl']; ?>">
                                </div>
                                <div class="prizeUploader">
                                    <div class="col-sm-12">
                                        <div class="original">
                                            <div id="s_icon">Upload</div>
                                    <div class="uploaderStatus" id="sicon_file>"></div>
                                </div>
                                <input type="hidden" id="sicon_fileName" class="getValue" data-key="screenIconUrl">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span>Title</h4></label>
                            <div class="col-sm-8">
                                <input type="text" id="title" class="form-control getValue" data-key="screenTitle" value="<?php echo $row['screenTitle']; ?>">
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title">Language</h4></label>
                            <div class="col-sm-8">
                               <select class="form-control getValue" data-key="languageCode">
                                    <option <?php if($row['languageCode'] == 'ar') { echo 'selected'; } ?> value="ar">ar</option>
                                    <option <?php if($row['languageCode'] == 'en') { echo 'selected'; } ?> value="en">en</option>
                                    <option <?php if($row['languageCode'] == 'es') { echo 'selected'; } ?> value="es">es</option>
									<option <?php if($row['languageCode'] == 'ca') { echo 'selected'; } ?> value="ca">ca</option>
									<option <?php if($row['languageCode'] == 'fr') { echo 'selected'; } ?> value="fr">fr</option>
									<option <?php if($row['languageCode'] == 'pt') { echo 'selected'; } ?> value="pt">pt</option>
                               </select>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title">Description</h4></label>
                            <div class="col-sm-8">
                                <textarea class="form-control getValue" data-key="description"><?php echo $row['description']; ?></textarea>
                            </div>
                        </div>
                        

                    </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once ('footer.php'); ?>
<script type="text/javascript" src="../js/general.js"></script>
<script type="text/javascript">
    $(function(){

        
            var settings1 = {
                url: "../includes/general_processing.php",
                dragDrop: false,
                dragDropStr: "<span><b>Drag & Drop File</b></span>",
                multiple: false,
                fileName: "myfile",
                formData: {"command":"uploadData", "dir" : "third_party/uploads/help/", "thumb" : "No"},
                allowedTypes: "jpg,png,jpeg",
                returnType: "json",
                showFileCounter: false,
                showDone: false,
                maxFileCount:1,
                onSuccess: function (files, data, xhr) {
                     $("#simage_fileName").val('https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/help/'+data);
                },
                onSubmit:function(files)
                {
                    setTimeout(function(){
                    $("#loader, #overlay").hide();
                    }, 100);
                },
                showDelete: true,
                deleteCallback: function (data, pd) {
                    for (var i = 0; i < data.length; i++) {
                        $.post("../includes/general_processing.php", {
                                op: "delete",
                                dir: "third_party/uploads/help/",
                                command: "deleteData",
                                name: data[i]
                            },
                            function (resp, textStatus, jqXHR) {
                                $("#simage_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                                $("#simage_fileName").val('');
                            });
                    }
                    pd.statusbar.hide(); //You choice to hide/not.
                }
            }
           var uploadObj = $("#s_image").uploadFile(settings1);

           var settings2 = {
                url: "../includes/general_processing.php",
                dragDrop: false,
                dragDropStr: "<span><b>Drag & Drop File</b></span>",
                multiple: false,
                fileName: "myfile",
                formData: {"command":"uploadData", "dir" : "third_party/uploads/help/", "thumb" : "No"},
                allowedTypes: "jpg,png,jpeg",
                returnType: "json",
                showFileCounter: false,
                showDone: false,
                maxFileCount:1,
                onSuccess: function (files, data, xhr) {
                     $("#sicon_fileName").val('https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/help/'+data);
                },
                onSubmit:function(files)
                {
                    setTimeout(function(){
                    $("#loader, #overlay").hide();
                    }, 100);
                },
                showDelete: true,
                deleteCallback: function (data, pd) {
                    for (var i = 0; i < data.length; i++) {
                        $.post("../includes/general_processing.php", {
                                op: "delete",
                                dir: "third_party/uploads/help/",
                                command: "deleteData",
                                name: data[i]
                            },
                            function (resp, textStatus, jqXHR) {
                                $("#sicon_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                                $("#sicon_fileName").val('');
                            });
                    }
                    pd.statusbar.hide(); //You choice to hide/not.
                }
            }
           var uploadObj = $("#s_icon").uploadFile(settings2);
    });
</script>
