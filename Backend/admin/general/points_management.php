<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php');
$_SESSION['lib'] = 6;

$database->query('SELECT * FROM points');
$database->execute();
$points = $database->single();
?>
<style type="text/css">
    h4
    {
        font-size: 16px;
    }
</style>
<div class="container-fluid">
    <div class="row">

        <?php
include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">Points Management</h4>
                        <div class="head_opts">
                            <button class="btn btn-default btn-black save_points">Save</button>
                            <a href="points_management.php" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <form class="form-horizontal myform">
                        
                        
                        <div class="form-group">
                            <label for="f1" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f1"> Level 1 Registration (Welcome Points)</h4></label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="levelOneRegistration" value="<?php echo $points['levelOneRegistration']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f2" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f2"> Level 2 Registration</h4></label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="levelTwoRegistration" value="<?php echo $points['levelTwoRegistration']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f3" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f3"> Level 3 Registration</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="levelThreeRegistration" value="<?php echo $points['levelTwoRegistration']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f4" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f4"> Invite Friends (SMS - FCB Charged)</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="inviteFriendsSMS" value="<?php echo $points['inviteFriendsSMS']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f5" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f5"> Invite Friends (URL Click Through) </h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="inviteFriendsURLClickThrough" value="<?php echo $points['inviteFriendsURLClickThrough']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f6" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f6"> Post Image to wall (Private) </h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="postImageToWallPrivate" value="<?php echo $points['postImageToWallPrivate']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f7" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f7"> Post Image to wall (Friends) </h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="postImageToWallFriends" value="<?php echo $points['postImageToWallFriends']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f8" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f8"> Post Image to wall (Public) </h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="postImageToWallPublic" value="<?php echo $points['postImageToWallPublic']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f9" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f9"> Share Image to social media</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="shareImageSocialMedia" value="<?php echo $points['shareImageSocialMedia']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f10" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f10"> Dialy Points (Day 1)</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="dialyPointsDay1" value="<?php echo $points['dialyPointsDay1']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f11" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f11"> Dialy Points (Day 2)</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="dialyPointsDay2" value="<?php echo $points['dialyPointsDay2']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f12" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f12"> Dialy Points (Day 3)</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="dialyPointsDay3" value="<?php echo $points['dialyPointsDay3']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f13" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f13"> Dialy Points (Day 4)</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group"> 
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="dialyPointsDay4" value="<?php echo $points['dialyPointsDay4']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f14" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f14"> Dialy Points (Day 5)</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="dialyPointsDay5" value="<?php echo $points['dialyPointsDay5']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f15" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f15"> Use Image (Raw Material)</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="number" class="form-control xp_text" data-key="useImageNormal" placeholder="Enter Amount" aria-describedby="sizing-addon2" value="<?php echo $points['useImageNormal']; ?>">
                                  <input type="hidden" class="oldpoints" value="<?php echo $points['useImageNormal']; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f16" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f16"> Use Premium Image (Raw Material)</h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="number" class="form-control xp_text" data-key="useImagePremium" placeholder="Enter Amount" aria-describedby="sizing-addon2" value="<?php echo $points['useImagePremium']; ?>">
                                  <input type="hidden" class="oldpoints" value="<?php echo $points['useImagePremium']; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f17" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f17"> Repost my Post</h4></label>
                           
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="repostMyPostNormal" value="<?php echo $points['repostMyPostNormal']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f18" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f18"> Repost my Post (In Case Premium) </h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="repostMyPostPremium" value="<?php echo $points['repostMyPostPremium']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="f19" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f19"> Premium Subscription Package 1 (1 Month)</h4></label>
                           
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="premiumSubscriptionPackageOneMonth" value="<?php echo $points['premiumSubscriptionPackageOneMonth']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f20" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f20"> Premium Subscription Package 2 (3 Months) </h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="premiumSubscriptionPackageThreeMonth" value="<?php echo $points['premiumSubscriptionPackageThreeMonth']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f21" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f21"> Premium Subscription Package 3 (6 Months) </h4></label>
                           
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="premiumSubscriptionPackageSixMonth" value="<?php echo $points['premiumSubscriptionPackageSixMonth']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f22" class="col-sm-7 control-label"><h4 class="tb_title"><input type="checkbox" id="f22"> Premium Subscription Package 4 (1 Year) </h4></label>
                            
                            <div class="col-sm-5">
                                <div class="input-group">
                                  <span class="input-group-addon xp" id="sizing-addon2">XP</span>
                                  <input type="text" class="form-control xp_text" data-key="premiumSubscriptionPackageOneYear" value="<?php echo $points['premiumSubscriptionPackageOneYear']; ?>" placeholder="Enter Amount" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        
                    </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once ('../includes/footer.php'); ?>
<script type="text/javascript" src="../js/general.js"></script>
