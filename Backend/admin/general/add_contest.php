<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php');
$_SESSION['lib'] = 7;
?>
<link rel="stylesheet" href="../plugins/datepicker/bootstrap-datetimepicker.min.css">
<div class="container-fluid">
    <div class="row">

        <?php
include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">Add new Photo</h4>
                        <div class="head_opts">
                            <button class="btn btn-default btn-black save_contest">Save</button>
                            <a href="contests.php" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <form class="form-horizontal myform">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Are Mandatory fields</h4></label>
                        </div>
                        
                        <div class="form-group">
                            <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Contest Name</h4></label>
                            <div class="col-sm-6">
                                <input type="text" id="name" class="form-control getValue" data-key="name">
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l8" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Start Date</h4></label>
                            <div style="padding: 0 15px" class="input-group date col-sm-6" id="startDate">
                                <input type="text" id="startdate" name="date" class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l8" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Start Time</h4></label>
                            <div style="padding: 0 15px" class="input-group date col-sm-6" id="startTime">
                                <input type="text" id="starttime" name="date" class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l8" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>End Date</h4></label>
                            <div style="padding: 0 15px" class="input-group date col-sm-6" id="endDate">
                                <input type="text" id="enddate" name="date" class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l8" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>End Time</h4></label>
                            <div style="padding: 0 15px" class="input-group date col-sm-6" id="endTime">
                                <input type="text" id="endtime" name="date" class="form-control" />
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                            </div>
                        </div>

                        <?php for($i=1;$i<=10;$i++) { ?>
                        <div class="form-group border_top">
                            <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Prize <?php echo $i; ?> Name</h4></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control getPName">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title">Prize <?php echo $i; ?> Image</h4></label>
                            <div class="col-sm-6">
                                <div class="original">
                                    <div id="original<?php echo $i; ?>">Upload</div>
                                    <div class="uploaderStatus" id="ori_file<?php echo $i; ?>"></div>
                                </div>
                                <input type="hidden" id="fileName<?php echo $i; ?>" class="getPFile" value="">
                
                            </div>
                        </div>
                        <?php } ?>
                        
                    </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once ('../includes/footer.php'); ?>
<script src="../plugins/datepicker/moment.js"></script>
<script src="../plugins/datepicker /bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="../js/general.js"></script>
<script type="text/javascript">
    $(function(){

        $('#startDate').datetimepicker({
            pickTime: false,
            format: 'dddd, MMMM Do, YYYY',
            minDate: moment()
        });
        
        $('#startTime').datetimepicker({
            pickDate: false,
        });

        $('#endDate').datetimepicker({
            pickTime: false,
            format: 'dddd, MMMM Do, YYYY',
            minDate: moment()
        });
        
        $('#endTime').datetimepicker({
            pickDate: false,
        });
       
        $(document).on('change', '#startDate', function(){
            setTimeout(function(){
                d1 = $("#startDate input").val();
                d2 = $("#endDate input").val();
            if(d1 == d2)
            {
                showNotification('warning', 'Dates cannot be same!');
                $(".save_contest").prop('disabled', true);
            } else 
            {
                $(".save_contest").prop('disabled', false);
            }
            }, 1000);
        });
        $(document).on('change', '#endDate', function(){
            setTimeout(function(){
                d1 = $("#startDate input").val();
                d2 = $("#endDate input").val();
            if(d1 == d2)
            {
                showNotification('warning', 'Dates cannot be same!');
                $(".save_contest").prop('disabled', true);
            } else 
            {
                $(".save_contest").prop('disabled', false);
            }
            }, 1000);
        });
        
        for(i=1;i<=10;i++)
        {
            !function( i ){
            window["settings" + i] = {
                url: "../includes/general_processing.php",
                dragDrop: false,
                dragDropStr: "<span><b>Drag & Drop File</b></span>",
                multiple: false,
                fileName: "myfile",
                formData: {"command":"uploadData", "dir" : "third_party/uploads/prizes/", "thumb" : "No"},
                allowedTypes: "jpg,png,jpeg",
                returnType: "json",
                showFileCounter: false,
                showDone: false,
                maxFileCount:1,
                onSuccess: function (files, data, xhr) {
                     $("#fileName"+i+"").val("https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/prizes/" + data);
                     //alert($("#fileName1").val());
                     
                },
                onSubmit:function(files)
                {
                    setTimeout(function(){
                    $("#loader, #overlay").hide();
                    }, 100);
                },
                showDelete: true,
                deleteCallback: function (data, pd) {
                    for (var i = 0; i < data.length; i++) {
                        $.post("../includes/general_processing.php", {
                                op: "delete",
                                dir: "third_party/uploads/prizes/",
                                command: "deleteData",
                                name: data[i]
                            },
                            function (resp, textStatus, jqXHR) {
                                $("#ori_file" + i).append("<div>File Deleted</div>").delay('5000').fadeOut();
                                $("#fileName"+i+"").val('');
                            });
                    }
                    pd.statusbar.hide(); //You choice to hide/not.

                }
            }
            }( i );
           var uploadObj = $("#original"+i).uploadFile( window["settings" + i]);

        }

    });
</script>
