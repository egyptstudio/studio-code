<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 
$imgUrl = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/";
//$imgUrl = "/FCB/admin/data/lowResolution/";
if(!isset($_REQUEST['id']) || $_REQUEST['id'] == "" || !is_numeric($_REQUEST['id']))
{
    if($_SESSION['lib_name'] == 'players')
    {
        header('location: libraries.php');
    }
}

$thesId = $_SESSION['tid'];
$studioId = $_REQUEST['id'];

$database->query('SELECT thesuarusId, name FROM thesuarus WHERE thesuarusId = :id AND type = :type');
$database->bind(':id', $thesId);
$database->bind(':type', $_SESSION['lib']);
$database->execute();
$thesRow = $database->single();

$database->query('SELECT spt.*, the.* FROM studiophotothesaurus spt, thesuarus the WHERE spt.thesuarusId = the.thesuarusId AND spt.studioPhotoId = :id AND the.type = :type');
$database->bind(':id', $studioId);
$database->bind(':type', $_SESSION['lib']);
$database->execute();
$playerRow = $database->resultset();

foreach ($playerRow as $pRow) {
    $playerName[] = $pRow['name'];
    $playerId[] = $pRow['thesuarusId'];
}
$pNames = implode(',', $playerName);
$pId = implode(',', $playerId);


$database->query('SELECT * FROM studiophoto WHERE studioPhotoId = :id');
$database->bind(':id', $studioId);
$database->execute();
$studioRow = $database->single();

$database->query('SELECT spt.*, the.* FROM studiophotothesaurus spt, thesuarus the WHERE spt.thesuarusId = the.thesuarusId AND spt.studioPhotoId = :id AND the.type = :type');
$database->bind(':id', $studioId);
$database->bind(':type', 2);
$database->execute();
$eventRow = $database->single();
$eventCount = $database->rowCount();

$database->query('SELECT spt.*, the.* FROM studiophotothesaurus spt, thesuarus the WHERE spt.thesuarusId = the.thesuarusId AND spt.studioPhotoId = :id AND the.type = :type');
$database->bind(':id', $studioId);
$database->bind(':type', 3);
$database->execute();
$seasonRow = $database->single();
$seasonCount = $database->rowCount();

/*$database->query('SELECT spt.*, the.* FROM studiophotothesaurus spt, thesuarus the WHERE spt.thesuarusId = the.thesuarusId AND spt.studioPhotoId = :id AND the.type = :type');
$database->bind(':id', $studioId);
$database->bind(':type', 4);
$database->execute();
$tagRows = $database->resultset();*/

$database->query('SELECT spt.*, the.* FROM studiophotothesaurus spt, thesuarus the WHERE spt.thesuarusId = the.thesuarusId AND spt.studioPhotoId = :id AND the.type = :type');
$database->bind(':id', $studioId);
$database->bind(':type', 4);
$database->execute();
$tagRow = $database->resultset();

foreach ($tagRow as $tRow) {
    $tagName[] = $tRow['name'];
    $tagId[] = $tRow['thesuarusId'];
}
$tNames = implode(',', $tagName);
$tId = implode(',', $tagId);
?>

<div class="container-fluid">
    <div class="row">

        <?php
        include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
                <input type="hidden" value="<?php echo $studioId; ?>" id="studioId"></input>
                <input type="hidden" value="<?php echo $_SESSION['lib_name']; ?>" id="pageId"></input>
                <div class="row">
                    <div class="col-md-12">
                        <div style="right: auto; font-size: 20px; top: 8px;" class="head_opts">
                            <a href="photos.php?id=<?php echo $thesId; ?>"><i class="fa fa-arrow-left"></i> <?php echo $thesRow['name']; ?></a>
                        </div>
                        
                        <h4 class="heading">Edit Details</h4>
                        <div class="head_opts">
                            <a href="#" class="btn btn-default btn-black edit_player">Save</a>
                            <a href="photos.php?id=<?php echo $thesId; ?>" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form class="form-horizontal myform">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Are Mandatory fields</h4></label>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <img style="margin: 0 auto" src="<?php echo $imgUrl.$studioRow['photoUrl']; ?>" class="img-responsive pull-left player_img" alt="Player">
                                    <a href="#" class="openUploader btn btn-primary pull-left">Change Picture</a>
                                </div>
                            </div>

                            <div class="uploaders">
                                <div class="form-group">
                                    <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Orignal Raw Image</h4></label>
                                    <div class="col-sm-6">
                                        <div class="original">
                                            <div id="original">Upload</div>
                                            <div class="uploaderStatus" id="ori_file"></div>
                                        </div>
                                        <input type="hidden" class="old_ori_file" value="<?php echo $studioRow['originalRawPhotoUrl']; ?>">
                                        <input type="hidden" class="ori_file" value="">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="l2" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Processed image in high resolution</h4></label>
                                    <div class="col-sm-6">
                                        <div class="high">
                                            <div id="high">Upload</div>
                                            <div class="uploaderStatus" id="high_file"></div>
                                        </div>
                                        <input type="hidden" class="old_high_file" value="<?php echo $studioRow['processedPhotoHighResUrl']; ?>">
                                        <input type="hidden" class="high_file" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="l3" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Processed image in low mobile resolution</h4></label>
                                    <div class="col-sm-6">
                                        <div class="low">
                                            <div id="low">Upload</div>
                                            <div class="uploaderStatus" id="low_file"></div>
                                        </div>
                                        <input type="hidden" class="old_low_file" value="<?php echo $studioRow['photoUrl']; ?>">
                                        <input type="hidden" class="low_file" value="">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group border_top">
                                <label for="l4" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Photo Type</h4></label>
                                <div class="col-sm-8">
                                    <label class="pull-left"><input <?php if($studioRow['captureType'] == 1) { echo 'checked'; } ?> value="1" type="radio" name="category" class="form-control p_type" id="l4">Player</label>
                                    <label class="pull-right"><input <?php if($studioRow['captureType'] == 2) { echo 'checked'; } ?> value="2" type="radio" name="category" class="form-control p_type">Team</label>
                                </div>

                            </div>
                            <div class="form-group border_top">
                                <label for="l5" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Premium</h4></label>
                                <div class="col-sm-8">
                                    <label class="pull-left"><input <?php if($studioRow['isPremium'] == 1) { echo 'checked'; } ?> value="1" type="radio" name="type" class="form-control premium" id="l5">Yes</label>
                                    <label class="pull-right"><input <?php if($studioRow['isPremium'] == 0) { echo 'checked'; } ?> value="0" type="radio" name="type" class="form-control premium">No</label>
                                </div>

                            </div>

                            <div class="form-group border_top">
                                <label for="l5" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Premium</h4></label>
                                <div class="col-sm-8">
                                    <input type="text" value="<?php echo $studioRow['requiredPoints']; ?>" name="number" class="form-control points" id="l11">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Player Name</h4></label>
                                <div class="input-group col-sm-8 players_selection">
                                  <input data-role="tagsinput" value="<?php echo $pNames; ?>" data-id="<?php echo $pId; ?>" id="players" disabled="disabled" type="text" class="form-control" name="players" placeholder="Select Players" aria-describedby="basic-addon1">
                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-plus"></i></span>
                              </div>                           
                          </div>

                          <div class="form-group border_top">
                            <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Event</h4></label>
                            
                            <div class="input-group col-sm-8 event_selection">
                              <input data-role="tagsinput" value="<?php echo $eventRow['name']; ?>" data-id="<?php echo $eventRow['thesuarusId'] ?>" id="event" disabled="disabled" type="text" name="events" class="form-control" placeholder="Select Event" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="fa fa-plus"></i></span>
                          </div>

                      </div>
                      <div class="form-group border_top">
                        <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Season</h4></label>
                        <div class="input-group col-sm-8 season_selection">
                          <input data-role="tagsinput" value="<?php echo $seasonRow['name']; ?>" data-id="<?php echo $seasonRow['thesuarusId'] ?>" id="season" disabled="disabled" type="text" name="season" class="form-control" placeholder="Select Season" aria-describedby="basic-addon3">
                          <span class="input-group-addon" id="basic-addon3"><i class="fa fa-plus"></i></span>
                      </div>
                  </div>

                  <div class="form-group border_top">
                    <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Tag Name</h4></label>
                    <div class="input-group col-sm-8 tags_selection">
                      <input data-role="tagsinput" value="<?php echo $tNames; ?>" data-id="<?php echo $tId; ?>" id="tags" disabled="disabled" type="text" class="form-control" name="tags" placeholder="Select Tags" aria-describedby="basic-addon1">
                      <span class="input-group-addon" id="basic-addon4"><i class="fa fa-plus"></i></span>
                  </div> 
                  <div class="col-sm-3"></div>                       
                  <div class="col-sm-8 def_tags">  
                    <span class="tag label label-primary">Playername</span>
                    <span class="tag label label-primary">Event</span>
                    <span class="tag label label-primary">Season</span>
                </div>
            </div>



                        <!--<div class="form-group border_top" id="tags">
                            <label for="l10" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Tags</h4></label>
                            <div class="input-group col-sm-8">
                                <input data-role="tagsinput" id="tags_data" name="tags" type="text" class="form-control" placeholder="Write your tag here" aria-describedby="basic-addon4">
                                <div class="def_tags">  
                                    <span class="tag label label-primary">Playername</span>
                                    <span class="tag label label-primary">Event</span>
                                    <span class="tag label label-primary">Season</span>
                                    <?php foreach ($tagRows as $tag) {
                                        //echo '<span class="tag label label-primary">'.$tag['name'].'<a data-mainId="'.$tag['id'].'" data-id="'.$tag['thesuarusId'].'" class="removeTag" href="#"><i class="fa fa-close"></i></a></span>';
                                    } ?>
                                </div>
                            </div>
                        </div>-->

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="aj_data">
      </div>
  </div>
</div>
</div>

<?php
include_once ('includes/footer.php'); 
//echo $_SERVER['DOCUMENT_ROOT'] . $imgUrl;
?>
<script type="text/javascript" src="js/players.js"></script>
<script>
    $(document).ready(function(){
        var settings1 = {
            url: "includes/processing.php",
            dragDrop: false,
            dragDropStr: "<span><b>Drag & Drop File</b></span>",
            multiple: false,
            fileName: "myfile",
            formData: {"command":"uploadData", "dir" : "<?php echo $imgUrl; ?>", "thumb" : "No"},
            allowedTypes: "jpg,png,jpeg",
            returnType: "json",
            showFileCounter: false,
            showDone: false,
            maxFileCount:1,
            onSuccess: function (files, data, xhr) {
         //alert(data);
         //$(id_ori+" .ajax-file-upload form:first").remove();
         $(".ori_file").val(data);
     },
     onSubmit:function(files)
     {
        //alert(files);
        setTimeout(function(){
            $("#loader, #overlay").hide();
        }, 100);
    },
    showDelete: true,
    deleteCallback: function (data, pd) {
        for (var i = 0; i < data.length; i++) {
            $.post("includes/processing.php", {
                op: "delete",
                dir: "<?php echo $imgUrl; ?>",
                command: "deleteData",
                name: data[i]
            },
            function (resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#ori_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
        }
        pd.statusbar.hide(); //You choice to hide/not.

    }
}

var uploadObj = $("#original").uploadFile(settings1);


var settings2 = {
    url: "includes/processing.php",
    dragDrop: false,
    dragDropStr: "<span><b>Drag & Drop File</b></span>",
    multiple: false,
    fileName: "myfile",
    formData: {"command":"uploadData", "dir" : "<?php echo $imgUrl; ?>", "thumb" : "No"},
    allowedTypes: "jpg,png,jpeg",
    returnType: "json",
    showFileCounter: false,
    showDone: false,
    maxFileCount:1,
    onSuccess: function (files, data, xhr) {
         //alert(data);
         //$(id_ori+" .ajax-file-upload form:first").remove();
         $(".high_file").val(data);
     },
     onSubmit:function(files)
     {
        //alert(files);
        setTimeout(function(){
            $("#loader, #overlay").hide();
        }, 100);
    },
    showDelete: true,
    deleteCallback: function (data, pd) {
        for (var i = 0; i < data.length; i++) {
            $.post("includes/processing.php", {
                op: "delete",
                dir: "<?php echo $imgUrl; ?>",
                command: "deleteData",
                name: data[i]
            },
            function (resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#high_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
        }
        pd.statusbar.hide(); //You choice to hide/not.

    }
}

var uploadObj = $("#high").uploadFile(settings2);

var settings3 = {
    url: "includes/processing.php",
    dragDrop: false,
    dragDropStr: "<span><b>Drag & Drop File</b></span>",
    multiple: false,
    fileName: "myfile",
    formData: {"command":"uploadData", "dir" : "<?php echo $imgUrl; ?>", "thumb" : "No"},
    allowedTypes: "jpg,png,jpeg",
    returnType: "json",
    showFileCounter: false,
    showDone: false,
    maxFileCount:1,
    onSuccess: function (files, data, xhr) {
         //alert(data);
         //$(id_ori+" .ajax-file-upload form:first").remove();
         $(".low_file").val(data);
     },
     onSubmit:function(files)
     {
        //alert(files);
        setTimeout(function(){
            $("#loader, #overlay").hide();
        }, 100);
    },
    showDelete: true,
    deleteCallback: function (data, pd) {
        for (var i = 0; i < data.length; i++) {
            $.post("includes/processing.php", {
                op: "delete",
                dir: "<?php echo $imgUrl; ?>",
                command: "deleteData",
                name: data[i]
            },
            function (resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#low_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
        }
        pd.statusbar.hide(); //You choice to hide/not.

    }
}

var uploadObj = $("#low").uploadFile(settings3);


$("#basic-addon1").click(function(){
    showLoader();
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getPlayers", function(){
            $("#myModal").modal('show');
            hideLoader();
            var numItems = $('.players_selection span.tag span').length
            
            var id = $("#players").data('id');
            $('*[data-id="'+id+'"]').prop('checked', true);
            
        });
    }, 1000);
});

$("#basic-addon2").click(function(){
    showLoader();
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getEvents", function(){
            $("#myModal").modal('show');
            hideLoader();

            var numItems = $('.event_selection span.tag span').length
            
            var id = $("#event").data('id');
            $('*[data-id="'+id+'"]').prop('checked', true);
        });
    }, 1000);
});

$("#basic-addon3").click(function(){
    showLoader();
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getSeason", function(){
            $("#myModal").modal('show');
            hideLoader();

            var numItems = $('.season_selection span.tag span').length
            
            var id = $("#season").data('id');
            $('*[data-id="'+id+'"]').prop('checked', true);
        });
    }, 1000);
});

$("#basic-addon4").click(function(){
    showLoader();
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getTags", function(){
            $("#myModal").modal('show');
            hideLoader();

            var id = $("#tags").data('id');
            var values = id.split(',');
            //$('*[data-id="'+id+'"]').prop('checked', true);
            $('.tags_data [data-id="'+values.join('"],[data-id="')+'"]').prop('checked',true);
        });
    }, 1000);
});

});// main

</script>
