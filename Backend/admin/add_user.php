<?php
include_once ('includes/check.php');
include_once ('includes/header.php');
if(!isset($_SESSION['userFlag']))
{
    unset($_SESSION['newUserId']);
}
?>
<link rel="stylesheet" href="plugins/datepicker/bootstrap-datetimepicker.min.css">
<div class="container-fluid">
    <div class="row">

        <?php
include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            <input type="hidden" id="userid" value="<?php echo $_SESSION['newUserId']; ?>">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">Create New User</h4>
                        <div class="head_opts">
                            <button class="btn btn-default btn-black save_user">Save</button>
                            <a href="users.php?id=<?php echo $_SESSION['lib']; ?>" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <form class="form-horizontal myform" id="save_user_form">
                        <?php 
                        if($_SESSION['lib'] == 3)
                        {
                            echo '<input type="hidden" value="3" class="getValue" data-key="userType">';
                        } else if($_SESSION['lib'] == 5) {
                            echo '<input type="hidden" value="4" class="getValue" data-key="userType">';
                        } ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Are Mandatory fields</h4></label>
                        </div>
                        <div class="form-group">
                            <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Profile Picture</h4></label>
                            <div class="col-sm-6">
                                <div class="original">
                                    <div id="original">Upload</div>
                                    <div class="uploaderStatus" id="ori_file"></div>
                                </div>
                                <input type="hidden" class="ori_file getValue" data-key="profilePic" value="">
                                
                            </div>
                        </div>
                        
                        <div class="form-group border_top">
                            <label for="l4" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Full Name</h4></label>
                            <div class="col-sm-8">
                                <input type="text" name="fullname" placeholder="Full name" data-key="fullName" class="getValue fullname form-control">
                            </div>
                        </div>
                        <div class="form-group border_top">
                            <label for="l5" class="col-sm-3 control-label"><h4 class="tb_title">Email</h4></label>
                            <div class="col-sm-8">
                                <input type="email" name="email" placeholder="youremail@companyname.com" data-key="email" class="email getValue form-control">
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title">Country</h4></label>
                            <div class="col-sm-8">
                              <select class="getValue countryDDL form-control" data-key="countryId">
                                  <?php 
                                  $database->query('SELECT countryId, countryName FROM country ORDER BY countryId ASC');
                                  $database->execute();
                                  $result = $database->resultset();
                                  foreach ($result as $row) {
                                  echo '<option value="'.$row['countryId'].'">'.$row['countryName'].'</option>';
                                   } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group border_top">
                            <label for="l7" class="col-sm-3 control-label"><h4 class="tb_title">City</h4></label>
                            <div class="col-sm-8">
                              <select class="getValue cityData form-control" data-key="cityId">
                                  
                              </select>
                            </div>
                        </div>
                        
                        <div class="form-group border_top">
                            <label for="l8" class="col-sm-3 control-label"><h4 class="tb_title">District</h4></label>
                            <div class="date col-sm-8">
                                <input type="text" name="district" placeholder="District" class="form-control getValue" data-key="district" />
                            </div>
                        </div>
                        

                        <div class="form-group border_top">
                            <label for="l8" class="col-sm-3 control-label"><h4 class="tb_title">Birthdate</h4></label>
                            <div style="padding: 0 15px" class="input-group date col-sm-8" id="datetimepicker1">
                                <input type="text" id="date" name="date" class="form-control getValue" data-key="dateOfBirth" />
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                            </div>
                        </div>
                        
                        <div class="form-group border_top">
                            <label for="l10" class="col-sm-3 control-label"><h4 class="tb_title">Education</h4></label>
                            <div class="date col-sm-8">
                                <input type="text" name="education" placeholder="Education" class="form-control getValue" data-key="education" />
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l10" class="col-sm-3 control-label"><h4 class="tb_title">About me</h4></label>
                            <div class="date col-sm-8">
                                <textarea name="aboutme" class="form-control getValue" data-key="aboutMe" placeholder="About me"></textarea>
                            </div>
                        </div>
                        

                    </form>
                    <br><br>
                    <a class="userLink check_btn" href="posts.php"><span class="pull-left">User Posts</span> <i class="pull-right fa fa-arrow-right"></i></a>
                    <a class="userLink check_btn" href="add_follow.php"><span class="pull-left">Following List</span> <i class="pull-right fa fa-arrow-right"></i></a>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="aj_data">
      </div>
    </div>
  </div>
</div>

<?php
include_once ('includes/footer.php'); ?>
<script src="plugins/datepicker/moment.js"></script>
<script src="plugins/datepicker /bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/users.js"></script>
<script type="text/javascript">
$(function(){
    $('#datetimepicker1').datetimepicker({
        pickTime: false,
        format: 'dddd, MMMM Do, YYYY'
    });
    
    var settings1 = {
        url: "includes/processing.php",
        dragDrop: false,
        dragDropStr: "<span><b>Drag & Drop File</b></span>",
        multiple: false,
        fileName: "myfile",
        formData: {
            "command": "uploadData",
            "dir": "third_party/uploads/profile/",
            "thumb": "No"
        },
        allowedTypes: "jpg,png,jpeg",
        returnType: "json",
        showFileCounter: false,
        showDone: false,
        maxFileCount: 1,
        onSuccess: function(files, data, xhr) {
            //alert(data);
            //$(id_ori+" .ajax-file-upload form:first").remove();
            $(".ori_file").val(data);
        },
        onSubmit: function(files) {
            //alert(files);
            setTimeout(function() {
                $("#loader, #overlay").hide();
            }, 100);
        },
        showDelete: true,
        deleteCallback: function(data, pd) {
            for (var i = 0; i < data.length; i++) {
                $.post("includes/processing.php", {
                    op: "delete",
                    dir: "third_party/uploads/profile/",
                    command: "deleteData",
                    name: data[i]
                }, function(resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#ori_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
            }
            pd.statusbar.hide(); //You choice to hide/not.
        }
    }
    var uploadObj = $("#original").uploadFile(settings1);

});
</script>