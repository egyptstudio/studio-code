<?php include_once('includes/functions.php'); 
$database = new Database();
?>
<?php $pageName = basename($_SERVER['PHP_SELF']); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FCB Studio Panel</title>

    <!-- Bootstrap -->
    <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link href="plugins/upload/uploadfile.css" rel="stylesheet" type="text/css">
    
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <link href="plugins/banner/jquery.bxslider.css" rel="stylesheet">
  </head>
  <style type="text/css">
    #save_feedback input, #save_feedback select, #save_feedback textarea
    {
        border: 1px solid #8b1b3f
    }
    label.error
    {
        margin-bottom: 0;
    }
    .bx-wrapper
    {
        margin-bottom: 25px
    }
    .bx-wrapper .bx-viewport
    {
        box-shadow: none;
        border: 0;
        background: none;left: 0
    }
    .bx-wrapper .bx-pager.bx-default-pager a
    {
        background: url(images/banner-bullets.png) -76px 0 no-repeat;
        border-radius: 0;
        width: 55px;
        height: 9px;
    }
    .bx-wrapper .bx-pager.bx-default-pager a:hover, .bx-wrapper .bx-pager.bx-default-pager a.active
    {
        background: url(images/banner-bullets.png) 0px 0 no-repeat;
    }
    .bx-wrapper .bx-pager, .bx-wrapper .bx-controls-auto
    {
        bottom: 10px;
    }
    copyright
    {
        background: #8b1b3f;
        float: left;
        width: 100%;
    }
    .copyright
    {
        padding: 35px 3em 55px; 
        color: white;
    }
    .copyright a
    {
        color: white;
    }

</style>
<body style="overflow: hidden">

    <div class="alert alert-dismissible" role="alert">
      <button type="button" class="close" data-hide="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong></strong>
  </div>

  <div id="overlay"></div>
  <div id="loader">
      <img src="images/211.GIF" alt="Loading...">
  </div>

  <div style="background: #8b1b3f" class="container-fluid">
      <div class="row">
        <div class="col-md-12 header">
          <h1 style="color: white" class="pull-left"><img src="images/fcbstudio.png" alt="Logo" width="40"> FCB Studio Panel</h1>
          
      </div>
  </div>
</div>
<banner>
    <ul class="bxslider">
        <li><a target="_blank" href="http://track.webgains.com/click.html?wgcampaignid=170253&wgprogramid=6373&wgtar
            get=http://www.nike.com/gb/en_gb/c/football"><img src="images/banner.jpg" /></a></li>
            <li><a target="_blank" href="https://www.facebook.com/FCBstudioApp?fref=ts"><img src="images/banner2.jpg" /></a></li>
            <li><a target="_blank" href=""><img src="images/banner3.jpg" /></a></li>
        </ul>
    </banner>

    <div style="margin: 2em 0" class="container-fluid">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="form-login">
                    <h4>Write your feedback and help us improving FCB Studio App</h4>

                    <form class="form-horizontal myform" id="save_feedback">
                        <input type="hidden" class="getValue" data-key="username" value="<?php echo $_SESSION["log_name"]; ?>">
                        <div class="form-group border_top">
                            <label for="l4" class="col-sm-3 control-label"><h5 class="tb_title">Title</h5></label>
                            <div class="col-sm-8">
                                <input type="text" name="title" data-key="title" class="getValue required form-control">
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l6" class="col-sm-3 control-label"><h5 class="tb_title">Platform</h5></label>
                            <div class="col-sm-8">
                              <select class="getValue platformDDL form-control" data-key="platform">
                                  <option value="">Select</option>
                                  <?php $database->query('SELECT * FROM platform');
                                  $database->execute();
                                  $result = $database->resultset();
                                  foreach ($result as $row) {
                                    echo '<option data-id="'.$row['id'].'" value="'.$row['platform'].'">'.$row['platform'].'</option>';     
                                } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group border_top">
                        <label for="l4" class="col-sm-3 control-label"><h5 class="tb_title">Operating System</h5></label>
                        <div class="col-sm-8">
                            <select name="operatingsystem" data-key="os" class="getValue osDDL form-control">

                            </select>
                        </div>
                    </div>

                    <div class="form-group border_top">
                        <label for="l4" class="col-sm-3 control-label"><h5 class="tb_title">Your Message</h5></label>
                        <div class="col-sm-8">
                            <textarea name="message" data-key="message" class="getValue required form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group border_top">
                        <label for="l1" class="col-sm-3 control-label"><h5 class="tb_title">Screenshot</h5></label>
                        <div class="col-sm-8">
                            <div class="original">
                                <div id="original">Upload</div>
                                <div class="uploaderStatus" id="ori_file"></div>
                            </div>
                            <input type="hidden" class="ori_file getValue" data-key="screenshot" value="">

                        </div>
                    </div>
                    <div class="form-group">
                        <button style="background-color: #9d3f5d" class="pull-right btn btn-default btn-black save_feedback">Save</button>
                    </div>
                </form>


            </div>

        </div>

    </div>
</div>

<copyright>
    <div class="container">
        <div class="row">
            <div class="col-md-12 copyright">
            <p>TAWASOL &copy; <?php echo date("Y"); ?> &#8226; <a href="">Privacy Policy</a></p>
            </div>
        </div>
    </div>
</copyright>

<?php include_once('includes/footer.php'); ?>
<script src="plugins/banner/jquery.easing.1.3.js"></script>
<script src="plugins/banner/jquery.bxslider.js"></script>
<script type="text/javascript">
    $(function(){

        $('.bxslider').bxSlider({
            mode: 'horizontal',
            captions: true,
            controls: false,
            auto: true
        });

        var settings1 = {
            url: "includes/processing.php",
            dragDrop: false,
            dragDropStr: "<span><b>Drag & Drop File</b></span>",
            multiple: false,
            fileName: "myfile",
            formData: {
                "command": "uploadData",
                "dir": "third_party/uploads/feedback/",
                "thumb": "No"
            },
            allowedTypes: "jpg,png,jpeg",
            returnType: "json",
            showFileCounter: false,
            showDone: false,
            maxFileCount: 1,
            onSuccess: function(files, data, xhr) {
            //alert(data);
            //$(id_ori+" .ajax-file-upload form:first").remove();
            $(".ori_file").val(data);
        },
        onSubmit: function(files) {
            //alert(files);
            setTimeout(function() {
                $("#loader, #overlay").hide();
            }, 100);
        },
        showDelete: true,
        deleteCallback: function(data, pd) {
            for (var i = 0; i < data.length; i++) {
                $.post("includes/processing.php", {
                    op: "delete",
                    dir: "third_party/uploads/feedback/",
                    command: "deleteData",
                    name: data[i]
                }, function(resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#ori_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
            }
            pd.statusbar.hide(); //You choice to hide/not.
        }
    }
    var uploadObj = $("#original").uploadFile(settings1);

    $(document).on('click', '.save_feedback', function(event){
        event.preventDefault();

        if (!$("#save_feedback").validate().form()) {
            return false;
        }

        var arrFields = $(".myform .getValue").map(function() {
            var $elm = $(this),
            childId = $elm.data('key');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();
        var arrValues = $(".myform .getValue").map(function() {
            if ($(this).val().length == 0) {
                $(this).val('');
            }
            return this.value.escapeHTML();
        }).get();
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'table=' + 'userfeedback' + '&db=' + arrFields + '&data=' + arrValues + '&command=' + 'add',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {

                    showNotification('success', ':) FCB would like to thank you for sending your feedback!');
                    setTimeout(function(){
                        window.location.href= "feedback.php";
                    }, 2000);
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });
    }); 

$(".platformDDL").change(function(){
    showLoader();
    var id = $(".platformDDL :selected").attr('data-id');
    $.ajax({
        type: 'POST',
        url: 'includes/processing.php',
        data: 'id=' + id + '&command=' + 'getOS',
        cache: false,
        success: function(data) {
            //alert(data);
            hideLoader();
            $(".osDDL").html(data);
        }
    });
});

});
</script>