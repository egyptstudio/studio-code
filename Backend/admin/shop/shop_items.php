<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$_SESSION['lib'] = 11;

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}
$startFrom = ($page-1) * $recPerPage;
$database->query('SELECT * FROM inappproducts ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM inappproducts ORDER BY id DESC LIMIT :start, :num');

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Shop Items</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_entry btn-primary">Add New Entry &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-left">
                                  <ul class="pagination">
                                    <li>
                                      <a href="shop_items.php?page=1" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <?php
                                if($page == 1)
                                {
                                    $prevPage = 1;
                                } else 
                                {
                                    $prevPage = ($page - 1);
                                }

                                if($page == $total_pages)
                                {
                                    $nextPage = $total_pages;
                                } else 
                                {
                                    $nextPage = ($page + 1);
                                }
                                echo '<li><a href="shop_items.php?page='.$prevPage.'">&lt;</a></li>';
                                echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                echo '<li><a href="shop_items.php?page='.$nextPage.'">&gt;</a></li>';
                                ?>
                                <li>
                                  <a href="shop_items.php?page=<?php echo $total_pages ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                </div>

                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Device Type</th>
                                <th>InApp Id</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th>Language</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                $lang = $row['language'];
                                if(is_numeric($lang))
                                {
                                    $database->query('SELECT * FROM languages WHERE id = :langId');
                                    $database->bind(":langId", $lang);
                                    $database->execute();
                                    $langRow = $database->single();
                                    $langText = $langRow['name'];
                                } else {
                                    $langText = $lang;
                                }
                                ?>
                                <tr>
                                    <td>
                                        <span><?php echo $row['deviceType']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['inAppID']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['inAppType']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['price']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $langText; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['description']; ?></span>
                                    </td>
                                    <td style="text-align:center">
                                        <a href="javascript:;" class="btn btn-edit btn-primary" data-id="<?php echo $row['id']; ?>"><i class="fa fa-info"></i> Edit </a>
                                        <a href="javascript:;" class="del_entry btn btn-danger" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                    </td>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } else { 
                        echo "<h2 style='margin-left: 20px'>List of In App Products will be displayed here.</h2>"; 
                    } ?>

                </div><!-- aj_sec -->
            </div><!-- postSection -->

        </div>

    </div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Add Entry</h4>
        </div>
        <div class="modal-body">

            <form class="form-horizontal myform">
                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Device Type</h4></label>
                    <div class="col-sm-8">
                        <select class="getValue form-control" data-key="deviceType">
                            <option value="iOS">iOS</option>
                            <option value="Android">Android</option>
                        </select>    
                    </div>
                </div>

                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">InApp ID</h4></label>
                    <div class="col-sm-8">
                        <input type="text" value="" class="inappid getValue form-control" data-key="inAppID">
                    </div>
                </div>

        <!--<div class="controller">
        <div class="form-group uploader">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Image Link</h4></label>
            <div class="col-sm-8">
                <div class="original">
                    <div id="original">Upload</div>
                    <div class="uploaderStatus" id="ori_file"></div>
                </div>
                <input type="hidden" class="ori_file getValue" value="" data-key="imageLink">
            </div>
        </div>
    </div>-->

    <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Type</h4></label>
        <div class="col-sm-8">
            <select class="getValue form-control" data-key="inAppType">
                <option value="Poster">Poster</option>
                <option value="Premium Subscription">Premium Subscription</option>
                <option value="Points">Points</option>
            </select>  
        </div>
    </div>
    <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Price</h4></label>
        <div class="col-sm-8">
            <input type="number" value="" class="getValue form-control" data-key="price">
        </div>
    </div>
    
    <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Langauge</h4></label>
        <div class="col-sm-8">
            <select class="getValue form-control" data-key="language">
                <?php 
                $database->query("SELECT * FROM languages");
                $database->execute();
                $result = $database->resultset();
                foreach ($result as $langRow) {
                    echo '<option value="'.$langRow['id'].'">'.$langRow['name'].'</option>';
                }
                ?>
            </select>  
        </div>
    </div>

    <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Description</h4></label>
        <div class="col-sm-8">
            <textarea class="getValue form-control" data-key="description"></textarea>
        </div>
    </div>

</form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary save_entry">Save</button>
</div>
</div>
</div>
</div>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data"></div>
    </div>
</div>
</div>
<?php
include_once ('../includes/footer.php');  ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(".add_entry").click(function(){
            $("#myModal").modal('show');
            var random = makeid();
            $(".inappid").val(random);
        });


        $(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("../aj_data.php?command=getShopItem&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 700);
        });

        $(document).on('click', '.save_entry', function(){

            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'inappproducts' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        $(".controller").load(""+loc+qs+" .uploader", function(){
                            $("#original").uploadFile(settings1);
                        }); 
                        $("#myModal").modal('hide');
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec"); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
});  
$(document).on('click', '.del_entry', function(){
    if(!confirm('Are you sure, you want to delete data?')) return false;
    db = "id";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'inappproducts' + '&db=' + db + '&data=' + value + '&pageName=' + 'shop_items' + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            hideLoader();
            if(data == 'correct')
            {
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec"); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('click', '.edit_entry', function(e){
    e.preventDefault();
    if (!$(".myeditform").validate().form()) {
        return false;
    }
    var arrFields = $(".myeditform .getValue").map(function() {
        var $elm = $(this),
        childId = $elm.data('key');
        if (childId.length == 0) {
            alert("Error");
            return false;
        }
        return childId;
    }).get();
    var arrValues = $(".myeditform .getValue").map(function() {
        if ($(this).val().length == 0) {
            $(this).val('');
        }
        return this.value.escapeHTML();
    }).get();

    var key = "id";
    var value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'inappproducts' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + key + '&value=' + value + '&command=' + 'update',
        cache: false,
        success: function(data) {
            hideLoader();
            if(data == 'correct')
            {
                $("#myModal").modal('hide');
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec");
                $("#myEditModal").modal('hide'); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});  

});
function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 9; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
</script>