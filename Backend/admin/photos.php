<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 
$imgUrl = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/";

if(!isset($_REQUEST['id']) || $_REQUEST['id'] == "" || !is_numeric($_REQUEST['id']))
{
    if($_SESSION['lib_name'] == 'players')
    {
        header('location: players.php');
    }
}
$id = $_REQUEST['id'];
$_SESSION['tid'] = $id;

$state = isset($_REQUEST['state']) ? $_REQUEST['state'] : '1';
if(!is_numeric($state))
{
    $state = 1;
}
if($state==1)
{
    $isHidden = 'false';
    $text = "Hide";
} else 
{
    $isHidden = 'true';
    $text = "Show";
}

$database->query('SELECT thesuarusId, name FROM thesuarus WHERE thesuarusId = :id AND type = :type ORDER BY name ASC');
$database->bind(':id', $id);
$database->bind(':type', $_SESSION['lib']);
$database->execute();
$thesRow = $database->single();
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            
            <div class="row">
                <div class="col-md-12">
                    <div style="right: auto; font-size: 20px; top: 8px;" class="head_opts">
                        <a href="players.php"><i class="fa fa-arrow-left"></i> Players</a>
                    </div>
                    <h4 class="heading"><?php echo $thesRow['name'] ?></h4>
                    <div class="head_opts">
                        <a href="add_library.php" class="btn btn-primary">Add new Photo &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="photoOpts1">
                    <div class="btn-group" role="group" aria-label="...">
                      <a href="photos.php?id=<?php echo $id; ?>&state=1" class="btn btn-primary <?php if($state==1) {echo 'activePlayerMenu'; } ?>">Show</a>
                      <a href="photos.php?id=<?php echo $id; ?>&state=2" class="btn btn-primary <?php if($state==2) {echo 'activePlayerMenu'; } ?>">Hidden</a>
                    </div>
                </div>

                <div class="photoOpts2">
                    <div class="chkboxOpts">
                        <label> <input id="selectall" type="checkbox"> Select / Unselect All</label>
                    </div>
                    <button id="<?php echo $text ?>_selected" class="btn btn-primary pull-right">
                        <?php echo $text; ?> Selected
                    </button>
                </div>

                <div class="aj_sec">
                <div class="col-md-12 aj_data">
                    <?php 
                    if($_SESSION['lib'] == 1)
                    { 
                        $database->query('SELECT spt.*, sp.* FROM studiophotothesaurus spt, studiophoto sp WHERE spt.thesuarusId = :id AND sp.captureType = 1 AND spt.studioPhotoId = sp.studioPhotoId AND sp.isHidden = :state GROUP BY sp.studioPhotoId');
                    } else
                    {
                        $database->query('SELECT spt.*, sp.* FROM studiophotothesaurus spt, studiophoto sp WHERE spt.thesuarusId = :id AND sp.captureType = 2 AND spt.studioPhotoId = sp.studioPhotoId AND sp.isHidden = :state GROUP BY sp.studioPhotoId');
                    }
                    $database->bind(':id', $id);
                    $database->bind(':state', $isHidden);
                    $database->execute();
                    $result = $database->resultset();

                    if($database->rowCount() > 0)
                    {
                    foreach ($result as $row) {
                            $studioId = $row['studioPhotoId'];

                            $database->query('SELECT * FROM studiophotothesaurus WHERE studioPhotoId = :stid');
                            $database->bind(':stid', $studioId);
                            $database->execute();
                            $stuThesResult = $database->resultSet();
                    ?>
                    <div class="col-md-2 photosCol">
                        <label>
                            <input data-id="<?php echo $row['studioPhotoId'] ?>" class="player_photo" type="checkbox"> Select
                        </label>
                        <div class="photoDetail">
                            <a href=detail.php?id=<?php echo $studioId; ?>>
                            <img src="<?php echo $imgUrl.$row['photoUrl']; ?>" class="img-responsive" alt="Player">
                            <h4><?php echo $thesRow['name']; ?></h4>
                            <?php foreach ($stuThesResult as $stuThesRow) {
                                $thesuarusId = $stuThesRow['thesuarusId'];         
                                $database->query('SELECT name FROM thesuarus WHERE thesuarusId = :thesId AND type = :type');
                                $database->bind(':thesId', $thesuarusId);
                                $database->bind(':type', 3);
                                $database->execute();
                                $seasonRow = $database->single();
                                if($database->rowCount() > 0) { ?>
                            <p><?php echo $seasonRow['name']; ?></p>
                                <?php } }?>
                            </a>
                        </div>
                    </div>
                    <?php } 
                } else {
                    echo "No record found!";
                }

                    ?>
                </div>
                </div>

            </div>
                
            </div>
        </div>
    </div>
</div>


<?php
include_once ('includes/footer.php'); ?>
<script type="text/javascript" src="js/players.js"></script>