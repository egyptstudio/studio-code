<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 
$imgUrl = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/";
//$imgUrl = "/FCB/admin/data/lowResolution/";

if(!isset($_REQUEST['id']) || $_REQUEST['id'] == "" || !is_numeric($_REQUEST['id']))
{
    if($_SESSION['lib_name'] == 'players')
    {
        header('location: libraries.php');
    }
}

$thesId = $_SESSION['tid'];
$studioId = $_REQUEST['id'];

$database->query('SELECT thesuarusId, name FROM thesuarus WHERE thesuarusId = :id AND type = :type');
$database->bind(':id', $thesId);
$database->bind(':type', $_SESSION['lib']);
$database->execute();
$thesRow = $database->single();

$database->query('SELECT * FROM studiophoto WHERE studioPhotoId = :id');
$database->bind(':id', $studioId);
$database->execute();
$studioRow = $database->single();

$database->query('SELECT spt.*, the.* FROM studiophotothesaurus spt, thesuarus the WHERE spt.thesuarusId = the.thesuarusId AND spt.studioPhotoId = :id AND the.type = :type');
$database->bind(':id', $studioId);
$database->bind(':type', 2);
$database->execute();
$eventRow = $database->single();
$eventCount = $database->rowCount();

$database->query('SELECT spt.*, the.* FROM studiophotothesaurus spt, thesuarus the WHERE spt.thesuarusId = the.thesuarusId AND spt.studioPhotoId = :id AND the.type = :type');
$database->bind(':id', $studioId);
$database->bind(':type', 3);
$database->execute();
$seasonRow = $database->single();
$seasonCount = $database->rowCount();

$database->query('SELECT spt.*, the.* FROM studiophotothesaurus spt, thesuarus the WHERE spt.thesuarusId = the.thesuarusId AND spt.studioPhotoId = :id AND the.type = :type');
$database->bind(':id', $studioId);
$database->bind(':type', 4);
$database->execute();
$tagRows = $database->resultset();
?>

<div class="container-fluid">
    <div class="row">

        <?php
include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">
                    <div class="col-md-12">
                        <div style="right: auto; font-size: 20px; top: 8px;" class="head_opts">
                            <a href="photos.php?id=<?php echo $thesId; ?>"><i class="fa fa-arrow-left"></i> <?php echo $thesRow['name']; ?></a>
                        </div>
                        
                        <h4 class="heading">Details</h4>
                        <div class="head_opts">
                            <a href="edit_library.php?id=<?php echo $studioId; ?>" class="btn btn-default btn-black">Edit</a>
                            <a href="photos.php?id=<?php echo $thesId; ?>" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <form class="form-horizontal myform">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <img style="margin: 0 auto" src="<?php echo $imgUrl.$studioRow['photoUrl']; ?>" class="img-responsive" alt="Player">
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l4" class="col-sm-3 control-label"><h4 class="tb_title">Photo Type</h4></label>
                            <div class="col-sm-8">
                                <?php 
                                if($studioRow['isPremium'] == 1)
                                {
                                    echo "Premium";
                                } else 
                                {
                                    echo "Normal";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group border_top">
                            <label for="l5" class="col-sm-3 control-label"><h4 class="tb_title">Premium</h4></label>
                            <div class="col-sm-8">
                                <?php 
                                if($studioRow['isPremium'] == 1)
                                {
                                    echo "Yes";
                                } else 
                                {
                                    echo "No";
                                }
                                ?>
                            </div>
                        </div>

                        <div class="form-group border_top">
                            <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title">Player Name</h4></label>
                            <div class="col-sm-8">
                              <?php echo $thesRow['name']; ?>
                            </div>
                        </div>
                        
                        <div class="form-group border_top">
                            <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title">Event</h4></label>
                            <div class=" col-sm-8">
                            <?php if($eventCount > 0) { 
                                    echo $eventRow['name'];
                                } else
                                {
                                    echo "No event";
                                }
                            ?>
                            </div>
                        </div>
                        <div class="form-group border_top">
                            <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title">Season</h4></label>
                            <div class=" col-sm-8">
                            <?php if($seasonCount > 0) { 
                                    echo $seasonRow['name'];
                                } else
                                {
                                    echo "No Season";
                                }
                            ?>
                            </div>
                        </div>
                        <div class="form-group border_top" id="tags">
                            <label for="l10" class="col-sm-3 control-label"><h4 class="tb_title">Tags</h4></label>
                            <div class="input-group col-sm-8">
                                <div class="def_tags">  
                                    <span class="tag label label-primary">Playername</span>
                                    <span class="tag label label-primary">Event</span>
                                    <span class="tag label label-primary">Season</span>
                                    <?php foreach ($tagRows as $tag) {
                                        echo '<span class="tag label label-primary">'.$tag['name'].'</span>';
                                    } ?>
                                </div>
                            </div>
                            
                        </div>

                    </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="aj_data">
      </div>
    </div>
  </div>
</div>

<?php
include_once ('includes/footer.php'); 
//echo $_SERVER['DOCUMENT_ROOT'] . $oriImg;
?>