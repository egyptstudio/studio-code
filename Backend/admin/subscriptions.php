<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 

$_SESSION['lib'] = 15;

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}
$startFrom = ($page-1) * $recPerPage;
$database->query('SELECT * FROM registeredusers ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM registeredusers ORDER BY id DESC LIMIT :start, :num');

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Website Subscriptions</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_entry btn-primary">Send Mail &nbsp;&nbsp;&nbsp;<i class="fa fa-send"></i></a>
                            <a href="javascript:;" class="btn exportToExcel btn-primary">Export to Excel &nbsp;&nbsp;&nbsp;<i class="fa fa-arrow-down"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-left">
                                  <ul class="pagination">
                                    <li>
                                      <a href="subscriptions.php?page=1" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <?php
                                if($page == 1)
                                {
                                    $prevPage = 1;
                                } else 
                                {
                                    $prevPage = ($page - 1);
                                }

                                if($page == $total_pages)
                                {
                                    $nextPage = $total_pages;
                                } else 
                                {
                                    $nextPage = ($page + 1);
                                }
                                echo '<li><a href="subscriptions.php?page='.$prevPage.'">&lt;</a></li>';
                                echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                echo '<li><a href="subscriptions.php?page='.$nextPage.'">&gt;</a></li>';
                                ?>
                                <li>
                                  <a href="subscriptions.php?page=<?php echo $total_pages ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                </div>

                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Select <input type="checkbox" id="selectall"></th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <span><input class="email_checkbox" type="checkbox" value="<?php echo $row['email']; ?>"></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['email']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php if($row['status'] == 'true') { echo '<label class="label label-success">Mail Send</label>'; } else { echo '<label class="label label-danger">Mail not Send</label>'; } ?></span>
                                    </td>
                                    <td style="text-align:center">
                                        <a href="javascript:;" class="del_entry btn btn-danger" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                    </td>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } else { 
                        echo "<h2 style='margin-left: 20px'>List of In App Products will be displayed here.</h2>"; 
                    } ?>

                </div><!-- aj_sec -->
            </div><!-- postSection -->

        </div>

    </div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Send Email</h4>
        </div>
        <div class="modal-body">

            <form class="form-horizontal myform">
                <input type="hidden" class="emails">
                <div class="form-group">
                    <label for="l2" class="col-sm-2 control-label"><h4 class="tb_title">Message</h4></label>
                    <div class="col-sm-10">
                        <textarea name="editor1" class="editor1 message form-control"></textarea>
                    </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary sendMail">Send</button>
        </div>
    </div>
</div>
</div>


<?php
include_once ('includes/footer.php'); ?>
<script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        var roxyFileman = 'plugins/ckeditor/fileman/index.html'; 
        CKEDITOR.replace( 'editor1',{filebrowserBrowseUrl:roxyFileman,
            filebrowserImageBrowseUrl:roxyFileman+'?type=image',
            removeDialogTabs: 'link:upload;image:upload'}); 

        var idArray = [];
        $(document).on('click', ".email_checkbox", function(){
            if($(this).is(':checked'))
            {
              idArray.push($(this).val());
              $(".emails").val(idArray);
              
          } else 
          {
            var text = $(this).val();
            pos = getPosition(idArray, text);
            
            var remove = idArray[pos];
            idArray = jQuery.grep(idArray, function(value) {
              return value != remove;
          });

            var newVal = removeValue($(".emails").val(), remove);
            $(".emails").val(newVal);
        }
    });

        $('#selectall').click(function(event) { 
            idArray = [];
            if(this.checked) { 
                $('.email_checkbox').each(function() { 
                    this.checked = true;
                    idArray.push($(this).val());

                });
                $(".emails").val(idArray);
            }else{
                $('.email_checkbox').each(function() { 
                    this.checked = false; 
                });
                $(".emails").val(''); 
                var idArray = [];
            }
        });



                $(".add_entry").click(function(){
                    $("#myModal").modal('show');
                });

                $(".exportToExcel").click(function(){
                    window.location.href = 'website/subscription_csv.php?emails=' + $(".emails").val();
                })

                $(document).on('click', '.sendMail', function(){
                    var emails = $(".emails").val();
                    var textareaData = CKEDITOR.instances.editor1.getData();
                    mesg = textareaData.escapeHTML();
            //var mesg = $(".message").val();
            if(emails == "")
            {
                showNotification("warning", "Please select atleast one email.");
                return false;
            }
            showLoader();
            $.ajax({
                type: 'POST',
                url: 'includes/processing.php',
                data: 'emails=' + emails + '&mesg=' + encodeURIComponent(mesg) + '&command=' + 'subscriptionEmail',
                cache: false,
                success: function(data) {
                    //alert(data);
                    hideLoader();
                    if(data == 'correct')
                    {
                        $("#myModal").modal('hide');
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec");
                        CKEDITOR.instances.editor1.setData('');
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  
$(document).on('click', '.del_entry', function(){
    if(!confirm('Are you sure, you want to delete data?')) return false;
    db = "id";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: 'includes/shop_processing.php',
        data: 'table=' + 'registeredusers' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            hideLoader();
            if(data == 'correct')
            {
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec"); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});
});
</script>