<?php
class Store_model extends CI_Model{
	function __construct() {
		parent::__construct();
	}

	function getMyPoints($username)
	{
		//Get
		$this->db
			->select('userId')
			->from('user')
			->where('fullName', $username);
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->row_array();
			$userId = $row['userId'];
			$this->db
				->select('points')
				->from('userpoints')
				->where('userId', $userId);
			$query = $this->db->get();
			if ( $query->num_rows() > 0 )
			{
				$row = $query->row_array();
				$points = $row['points'];
				return $points;
			} else
			{
				return '0';
			}	
		} else 
		{
			return '';
		}
	}

	function pointsChangedForUser($userId, $pointsDelta, $deltaDesc)
	{

		//Insert into pointshistory
		$data = array(
		   'userId' => $userId ,
		   'pointsDelta' => $pointsDelta ,
		   'deltaDesc' => str_replace("%20", " ", $deltaDesc)
		);
		$this->db->insert('userpointshistory', $data); 
		$insertResult = $this->db->affected_rows();
		if($insertResult > 0)
		{
			$this->db
				->select('points')
				->from('userpoints')
				->where('userId', $userId);

			$query = $this->db->get();
			if ( $query->num_rows() > 0 )
			{
				//Update
				$row = $query->row_array();
				$oldPoints = $row['points'];
				
				$newPoints = 0;
				if(intval($pointsDelta) > 0)
				{
					$newPoints = ($oldPoints + $pointsDelta);
				} else 
				{
					$newArr = explode("-", $pointsDelta);
					$newPoints = $oldPoints - $newArr[1];
				}
				
				$updData = array('points' => $newPoints);
				
				$this->db
				    ->where('userId', $userId)
				    ->update('userpoints', $updData);
				return $this->db->affected_rows();
			} else 
			{
				//Insert
				$data = array(
				   'userId' => $userId ,
				   'points' => $pointsDelta ,
				);
				$this->db->insert('userpoints', $data); 
				return $this->db->affected_rows();
			}
		} else 
		{
			return 'error';
		}
	}

	function getMyPointsHistory($userId)
	{
		//Get
		$this->db
			->select('pointsDelta, deltaDesc, happenedOn')
			->from('userpointshistory')
			->where('userId', $userId)
			->order_by("happenedOn", "desc");
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			return $query->result_array();
		}
		return 0;
	}


	function getMyPosters($userId)
	{
		//Get
		$this->db
			->select('orderDate, status')
			->from('posterinquirers')
			->where('userId', $userId)
			->order_by("orderDate", "desc");
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			return $query->result_array();
		}
		return 0;
	}

	function subscripeInPremium($userId, $subscriptionDuration)
	{


		//Get
		/*$this->db
			->select('registrationDate')
			->from('user')
			->where('userId', $userId);
			
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->row_array();
		 
			$t1 = strtotime( $row['registrationDate'] );
			$t2 = strtotime("+".$subscriptionDuration." days");
			$diff = $t2 - $t1;
			$hours = $diff / ( 60 * 60 );
			$days = floor($diff / 86400);
			//For 30 days
			if($days >= 30 && $days <= 90)
			{
				updateUserPoints($userId, 1000);
			} else if($days >= 91 && $days <= 181)
			{
				updateUserPoints($userId, 3000);
			} 
			//
		}*/
		$now = date("Y-m-d");
		$after = date("Y-m-d", strtotime("+".$subscriptionDuration." days"));
		$updData = array('isPremium' => 1, 'premiumEnd' => $after);	

		$query = $this->db
		    		->where('userId', $userId)
		    		->update('user', $updData);
		//return $this->db->last_query();

		if($query)
		{
			if($subscriptionDuration == 30)
			{
				$this->updateUserPoints($userId, 1000);
			} else if($subscriptionDuration == 91)
			{
				$this->updateUserPoints($userId, 3000);
			} else if($subscriptionDuration == 182)
			{
				$this->updateUserPoints($userId, 6000);
			} else if($subscriptionDuration == 365)
			{
				$this->updateUserPoints($userId, 12000);
			}
			return $after;
		} else 
		{
			return 0;
		}
	}

	function updateUserPoints($userId, $points)
	{
		//Update User Table
		if($points == 1000)
		{
			$updUser = array('isBasic' => 1, 'isBronze' => 0, 'isSilver' => 0, 'isGold' => 0);
			$this->db
				 ->where('userId', $userId)
				 ->update('user', $updUser);
		} else if($points == 3000)
		{
			$updUser = array('isBasic' => 0, 'isBronze' => 1, 'isSilver' => 0, 'isGold' => 0);
			$this->db
				 ->where('userId', $userId)
				 ->update('user', $updUser);
		} else if($points == 6000)
		{
			$updUser = array('isBasic' => 0, 'isBronze' => 0, 'isSilver' => 1, 'isGold' => 0);
			$this->db
				 ->where('userId', $userId)
				 ->update('user', $updUser);
		} else if($points == 12000)
		{
			$updUser = array('isBasic' => 0, 'isBronze' => 0, 'isSilver' => 0, 'isGold' => 1);
			$this->db
				 ->where('userId', $userId)
				 ->update('user', $updUser);
		}
		//Update User Table
		
		$this->db
				->select('points')
				->from('userpoints')
				->where('userId', $userId);

			$query = $this->db->get();
			if ( $query->num_rows() > 0 )
			{
				//Update
				$row = $query->row_array();
				$oldPoints = $row['points'];

				$newPoints = ($oldPoints + $points);
				
				$updData = array('points' => $newPoints);
			
				$this->db
				    ->where('userId', $userId)
				    ->update('userpoints', $updData);
				return $this->db->affected_rows();
			} else 
			{
				//Insert
				$data = array(
				   'userId' => $userId ,
				   'points' => $points ,
				);
				$this->db->insert('userpoints', $data); 
				return $this->db->affected_rows();
			}
	}

	function myPremiumStatus($userId)
	{
		//Get
		$this->db
			->select('isPremium, premiumEnd')
			->from('user')
			->where('userId', $userId);
			
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			return $query->row_array();
		}
		return 0;	
	}

	function orderPoster($userId, $posterImage, $shipmentAddress)
	{
		//Get
		$this->db
			->select('fullName')
			->from('user')
			->where('userId', $userId);
			
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->row_array();
			$userName = $row['fullName'];

			$imgPath = $this->saveBase64Image($posterImage, $userName);
			//Insert into pointshistory
			$data = array(
			   'userId' => $userId ,
			   'shipmentDetails' => $shipmentAddress ,
			   'posterPath' => $imgPath,
			   'orderDate' => date("Y-m-d"),
			   'status' => 'Under review'
			);
			$this->db->insert('posterinquirers', $data); 
			$insertResult = $this->db->affected_rows();
			return $insertResult;
		}
		return -1;
	}

	function saveBase64Image($base64img, $name) 
	{
		define('UPLOAD_DIR', base_url() . 'assets/poster_images/'); //define folder path

        $img = $base64img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $success = file_put_contents('assets/poster_images/'. $name . "_" . time() . '.png', $data);
        return UPLOAD_DIR . $name . "_" . time() . '.jpg';
	}


	function loadInAppProducts($type)
	{
		//Get
		$this->db
			->select('*')
			->from('inappproducts')
			->where('deviceType', $type)
			->order_by("id", "asc");
			
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			return $query->result_array();
		} else 
		{
			return 0;
		}
	}

	function loadInAppProductsDesc($id, $lang)
	{
		//Get
		$this->db
			->select('*')
			->from('inappproductsdescription')
			->where('inAppProducts_id', $id)
			->where('localization', $lang);
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0 )
		{
			return $query->row_array();
		} else 
		{
			return 0;
		}	
	}

	function addPointsForUser($userId, $points)
	{
		$this->db
				->select('points')
				->from('userpoints')
				->where('userId', $userId);

		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			//Update
			$row = $query->row_array();
			$oldPoints = $row['points'];
				
			$newPoints = 0;
			
			$newPoints = ($oldPoints + $points);
			
			$updData = array('points' => $newPoints);
				
			$this->db
			    ->where('userId', $userId)
			    ->update('userpoints', $updData);
			return $this->db->affected_rows();
		} else 
		{
			//Insert
			$data = array(
			   'userId' => $userId ,
			   'points' => $points ,
			);
			$this->db->insert('userpoints', $data); 
			return $this->db->affected_rows();
		}	
	}

	
}
?>