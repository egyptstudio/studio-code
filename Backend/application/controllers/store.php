<?php 
Class Store extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('store_model');
	}

	function getMyPoints()
	{
		$json_data = $this->input->post('data');
		//$json_data ='{"username" : "osama", "lang" : "ar"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);

		if(empty($array_data->username) || count($array_count) != 2)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$username = $array_data->username;
		$lang = $array_data->lang;

		$result = $this->store_model->getMyPoints($username);
		if(!empty($result))
		{
			$data = array(
				"points" => intval($result)
			);
			$status = array(
				"status" => 2,
				"data" => $data
			);
		} else
		{
			$data[] = array();
			$status = array(
				"status" => 2,
				"data" => $data
			);
		}
		echo json_encode($status);
	}

	function pointsChangedForUser()
	{
		$json_data = $this->input->post('data');
		//$json_data ='{"userID" : "711", "pointsDelta" : "100", "deltaDesc" : "Osama Shared your picture", "lang" : "ar"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);
		//print_r($array_data);
		//echo count($array_count);
		if(empty($array_data->userID) || empty($array_data->pointsDelta) || empty($array_data->deltaDesc) || count($array_count) != 4)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$userID = $array_data->userID;
		$pointsDelta = $array_data->pointsDelta;
		$deltaDesc = $array_data->deltaDesc;
		$lang = $array_data->lang;


		$result = $this->store_model->pointsChangedForUser($userID, $pointsDelta, $deltaDesc);
		if($result == 1)
		{
			$data = array(
				"error"=> false,
				"errorMessage" => ""
			);
			$status = array(
				"status"=> 2,
				"data" => $data
			);	
		} else 
		{
			$status = array(
				"status"=> -2,
			);
		}
		echo json_encode($status);
	}

	function getMyPointsHistory()
	{

		$json_data = $this->input->post('data');
		//$json_data ='{"userID" : "1", "lang" : "ar"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);
		//print_r($array_data);
		//echo count($array_count);
		if(empty($array_data->userID) || count($array_count) != 2)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$userID = $array_data->userID;
		$lang = $array_data->lang;

		$result = $this->store_model->getMyPointsHistory($userID);
		if(is_array($result))
		{
			foreach($result as $row)
			{
				$data[] = array(
					'pointsDelta' => intval($row['pointsDelta']),
					'deltaDesc' => $row['deltaDesc'],
					'happenedOn' => strtotime($row['happenedOn'])
				);	
			}
			
			$status = array(
				'status' => 2,
				'data' => $data
			);
		} else if($result == 0)
		{
			$data[] = array();
			$status = array(
				"status"=> 2,
				"data" => $data
			);
		} else {
			$status = array(
				"status"=> -2,
			);
		}
		echo json_encode($status);
	}

	function getMyPosters()
	{
		$json_data = $this->input->post('data');
		//$json_data ='{"userID" : "1", "lang" : "ar"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);
		//print_r($array_data);
		//echo count($array_count);
		if(empty($array_data->userID) || count($array_count) != 2)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$userID = $array_data->userID;
		$lang = $array_data->lang;

		$result = $this->store_model->getMyPosters($userID);
		if(is_array($result))
		{
			foreach($result as $row)
			{
				$data[] = array(
					"orderDate" => strtotime($row['orderDate']),
					"status" => $row['status'],
				);	
			}
			$status = array(
				"status" => 2,
				"data" => $data
			);
			
		} else if($result == 0)
		{
			$data[] = array();
			$status = array(
				"status"=> 2,
				"data" => $data
			);
		} else {
			$status = array(
				"status"=> -2,
			);
		}
		echo json_encode($status);
	}


	function subscripeInPremium()
	{

		$json_data = $this->input->post('data');
		//$json_data ='{"userID" : "711", "subscriptionDuration" : "91", "lang" : "ar"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);
		//print_r($array_data);
		//echo count($array_count);
		if(empty($array_data->userID) || empty($array_data->subscriptionDuration) || count($array_count) != 3)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$userID = $array_data->userID;
		$subscriptionDuration = $array_data->subscriptionDuration;
		$lang = $array_data->lang;


		$result = $this->store_model->subscripeInPremium($userID, $subscriptionDuration);
		
		if(!empty($result))
		{
			$data = array(
				"error" => false,
				"errorMessage" => "",
				"Ending Date" => strtotime($result)
			);
			$status = array(
				"status" => 2,
				"data" => $data
			);
		} else 
		{
			$status = array(
				"status"=> -2,
			);
		}
		echo json_encode($status);
	}


	function myPremiumStatus()
	{
		$json_data = $this->input->post('data');
		//$json_data ='{"userID" : 711, "lang" : "en"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);
		//print_r($array_data);
		//echo count($array_count);
		if(empty($array_data->userID) || count($array_count) != 2)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$userID = $array_data->userID;
		$lang = $array_data->lang;

		$result = $this->store_model->myPremiumStatus($userID);

		if(is_array($result))
		{
			if($result['isPremium'] == 1)
			{
				$premium = true;
			} else 
			{
				$premium = false;
			}
			$data = array(
				"premium" => $premium,
				"premiumEnd" => strtotime($result['premiumEnd']),
			);	
			
			$status = array(
				"status" => 2,
				"data" => $data
			);
			
		} else if($result == 0)
		{
			$data[] = array();
			$status = array(
				"status"=> 2,
				"data" => $data
			);
		} else {
			$status = array(
				"status"=> -2,
			);
		}
		echo json_encode($status);
	}

	function orderPoster()
	{
		$json_data = $this->input->post('data');

		//$json_data ='{"userID" : "711", "posterImage" : "$image", "shipmentAddress" : "Some address will goes here",  "lang" : "ar"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);
		//print_r($array_data);
		//echo count($array_count);
		if(empty($array_data->userID) || empty($array_data->posterImage) || empty($array_data->shipmentAddress) || count($array_count) != 4)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$userID = $array_data->userID;
		$posterImage = $array_data->posterImage;
		$shipmentAddress = $array_data->shipmentAddress;
		$lang = $array_data->lang;

		$result = $this->store_model->orderPoster($userID, $posterImage, $shipmentAddress);
		
		if($result == 1)
		{
			$data = array(
				"error" => false,
				"errorMessage" => "",
			);
			$status = array(
				"status"=> 2,
				"data" => $data
			);

		} else if($result == -1)
		{
			$data[] = array();
			$status = array(
				"status"=> 2,
				"data" => $data
			);
		}
		 else 
		{
			$status = array(
				"status"=> -2,
			);
		}
		echo json_encode($status);
	}

	function loadInAppProductIds()
	{
		$json_data = $this->input->post('data');
		//$json_data ='{"deviceType" : "Android", "lang" : "en"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);
		//print_r($array_data);
		//echo count($array_count);
		if(empty($array_data->deviceType) || count($array_count) != 2)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$deviceType = $array_data->deviceType;
		$lang = $array_data->lang;

		$result = $this->store_model->loadInAppProducts($deviceType);
		if(is_array($result))
		{
			foreach ($result as $row) {
				if($row['inAppType'] == 'Points')
				{
					$descResult = $this->store_model->loadInAppProductsDesc($row['id'], $lang);
					$points[] = array('inAppId' => $row['inAppID'], 'price' => floatval($row['price']), 'description' => $row['description']);
				} else 
				{
					$descResult = $this->store_model->loadInAppProductsDesc($row['id'], $lang);
					$premium[] = array('inAppId' => $row['inAppID'], 'price' => floatval($row['price']), 'description' => $row['description']);
				}
			}
			
			$data[] = array(
				'Points' => $points,
				'Premium' => $premium
			);
			$status = array(
				"status" => 2,
				"data" => $data
			);
			
		} else if($result == 0)
		{
			$data[] = array();
			$status = array(
				"status"=> 2,
				"data" => $data
			);
		} else {
			$status = array(
				"status"=> -2,
			);
		}
		echo json_encode($status);
	}

	function addPointsForUser()
	{
		$json_data = $this->input->post('data');
		//$json_data ='{"userID" : "711", "pointsBought" : "10", "lang" : "ar"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);
		if(empty($array_data->userID) || empty($array_data->pointsBought) || count($array_count) != 3)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$userID = $array_data->userID;
		$pointsBought = $array_data->pointsBought;
		$lang = $array_data->lang;

		$result = $this->store_model->addPointsForUser($userID, $pointsBought);
		if($result == 1)
		{
			$data[] = array();
			$status = array(
				"status"=> 2,
				"data" => $data
			);
		} else {
			$status = array(
				"status"=> -2,
			);
		}
		echo json_encode($status);

	}

}// Main Class
?>