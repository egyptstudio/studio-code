<?php 
include_once('functions.php'); 
include_once('class.phpmailer.php');
include_once('class.smtp.php');

$command = $_REQUEST['command'];
$database = new Database();

if($command == 'login')
{
	$fields = explode(',', $_POST['db']);
	$values = explode(',', $_POST['data']);

	$values[1] = md5($values[1]);

	$umk = new umkPlugin("admin", $fields, $values);
	$umk->login("id");
} else if($command == 'add')
{
	$fields = explode(',', $_POST['db']);
	$values = explode(',', $_POST['data']);

	$umk = new umkPlugin($_POST['table'], $fields, $values);
	$umk->insert();

    if($_POST['pageName'] == 'add_library')
    {
        echo "~".$database->lastInsertId();
    }
} else if($command == 'delete')
{
    $fields = $_POST['db'];
    $values = $_POST['data'];

    $umk = new umkPlugin($_POST['table'], $fields, $values);
    $umk->delete();

    /*if($pageName == 'deleteTags')
    {
        $umk = new umkPlugin('studiophotothesaurus', $fields, $values);
        $umk->delete();
    }*/
} else if($command == 'update')
{
    $fields = explode(',', $_POST['db']);
    $values = explode(',', $_POST['data']);

    $key = $_POST['key'];
    $value = $_POST['value'];

    $umk = new umkPlugin($_POST['table'], $fields, $values);
    $umk->update("WHERE $key = '$value'");    
}
// Customs
else if($command == 'checkTag')
{
    $tag = $_POST['tag'];
    $database->query('SELECT name, type FROM thesuarus WHERE name = :tag AND type = 4');
    
    $database->bind(':tag', $tag);
    $database->execute();
    if($database->rowCount() > 0)
    {
        echo "incorrect";
    } else 
    {
        /*$database->query('INSERT INTO thesuarus (name, type) VALUES (:name, :type)');
        $database->bind(":name", $tag);
        $database->bind(":type", 4);
        $database->execute();*/

        echo "correct";
    }
} else if($command == 'savePlayer')
{
    $database->query('INSERT INTO studiophoto (photoUrl, captureType, isPremium, isHidden, originalRawPhotoUrl, processedPhotoHighResUrl) VALUES (:low, :type, :premium, :hidden, :ori, :high)');
    $database->bind(":low", $_POST['lowImg']);
    $database->bind(":type", $_POST['type']);
    $database->bind(":premium", $_POST['premium']);
    $database->bind(":hidden", 'false');
    $database->bind(":ori", $_POST['oriImg']);
    $database->bind(":high", $_POST['highImg']);
    $database->execute();
    if($database->rowCount() > 0)
    {
        $studioId = $database->lastInsertId();
        $players = explode(",", $_POST['players']);
        
        for($i=0; $i<count($players); $i++)
        {
            $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
            $database->bind(":studioId", $studioId);
            $database->bind(":thesId", $players[$i]);
            $database->execute();
        }

        $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
        $database->bind(":studioId", $studioId);
        $database->bind(":thesId", $_POST['playerEvent']);
        $database->execute();

        $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
        $database->bind(":studioId", $studioId);
        $database->bind(":thesId", $_POST['season']);
        $database->execute();
        
        $tags = explode(",", $_POST['tags']);
        for($i=0; $i<count($tags); $i++)
        {
            $database->query('INSERT INTO thesuarus (name, type) VALUES (:name, :type)');
            $database->bind(":name", $tags[$i]);
            $database->bind(":type", 4);
            $database->execute();
            if($database->rowCount() > 0)
            {
                $tagId = $database->lastInsertId();

                $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
                $database->bind(":studioId", $studioId);
                $database->bind(":thesId", $tagId);
                $database->execute();
            }
        }

        //Send Notification

        $database->query('INSERT INTO notification (notificationType, notificationtime) VALUES (:type, :time)');
        $database->bind(":type", 8);
        $database->bind(":time", date("Y-m-d H:i:s"));
        if ($database->execute()) {
        //Apple Push noti settings
            $passphrase = '';
            $ctx        = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'fcb.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        //Apple Push noti settings
            
            $database->query('SELECT fullName, email, pushnotificationtoken, devicetype FROM user WHERE blocked = :block ORDER BY fullName ASC');
                //$database->bind(':type', 1);
                //$database->bind(':type2', 2);
            $database->bind(':block', 0);
            $database->execute();
            $result = $database->resultset();
            //Send Noti
            //echo $database->rowCount();
            
            foreach ($result as $userRow) {
                //Android
                if ($userRow['devicetype'] == 1) {
                    if (!empty($userRow['pushnotificationtoken'])) {
                        $apiKey = "AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs";
                        
                        $registrationIDs = array(
                            $userRow['pushnotificationtoken']
                            );
                        
                        $message = 'Studio Library Modified';
                        
                        $url = 'https://android.googleapis.com/gcm/send';
                        
                        $fields = array(
                            'registration_ids' => $registrationIDs,
                            'data' => array(
                                "message" => $message
                                )
                            );
                        
                        $headers = array(
                            'Authorization: key=' . $apiKey,
                            'Content-Type: application/json'
                            );
                        
                        $ch = curl_init();
                        
                        curl_setopt($ch, CURLOPT_URL, $url);
                        
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                        
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        
                        $result = curl_exec($ch);
                        if ($result === FALSE) {
                            die('Curl failed: ' . curl_error($ch));
                        }
                        
                        curl_close($ch);
                        //var_dump($result);
                    }
                } else {
                    if (!empty($userRow['pushnotificationtoken'])) {
                        $deviceToken = trim($userRow['pushnotificationtoken']);
                        
                        $message = 'Studio Library Modified';
                        
                        if (!$fp)
                            exit("Failed to connect: $err $errstr" . PHP_EOL);
                        
                        //echo 'Connected to APNS' . PHP_EOL;
                        
                        $body['aps'] = array(
                            'badge' => +1,
                            'alert' => $message,
                            'sound' => 'default'
                            );
                        
                        $payload = json_encode($body);
                        
                        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
                        
                        $result = fwrite($fp, $msg, strlen($msg));
                        
                        
                        /*if (!$result)
                        echo    $noti_msg = 'Message not delivered';
                        else
                        echo    $noti_msg = 'Message successfully delivered ' . $message;
                        */
                    }
                }
            }
            //Apple Push noti
            @socket_close($fp);
            fclose($fp);
            //Apple Push noti
        } //Notification
        // Send notification
        echo "correct";
    } else 
    {
        echo "incorrect";
    }
} else if($command == "editPlayer")
{
    $subCommand = $_POST['subCommand'];
    
    if($subCommand == 'updateFile')
    {
        $database->query('SELECT photoUrl, originalRawPhotoUrl, processedPhotoHighResUrl FROM studiophoto WHERE studioPhotoId = : id');
        $database->bind(":id", $_POST['id']);
        $database->execute();
        $row = $database->single();

        if (!class_exists('S3'))require_once('S3.php');
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);

        if($row['photoUrl'] != $_POST['lowImg'] && !empty($row['photoUrl']))
        {
            $output_dir = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/";
            $fileName = $row['photoUrl'];
            
            if ($s3->deleteObject("barca", "third_party/uploads/".$fileName)) {
                echo "Deleted File " . $fileName . "<br>";
            }
        }
        if($row['originalRawPhotoUrl'] != $_POST['oriImg'] && !empty($row['originalRawPhotoUrl']))
        {
            $output_dir = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/";
            $fileName = $row['originalRawPhotoUrl'];
            if ($s3->deleteObject("barca", "third_party/uploads/".$fileName)) {
                echo "Deleted File " . $fileName . "<br>";
            }
        }
        if($row['processedPhotoHighResUrl'] != $_POST['highImg'] && !empty($row['processedPhotoHighResUrl']))
        {
            $output_dir = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/";
            $fileName = $row['processedPhotoHighResUrl'];
            if ($s3->deleteObject("barca", "third_party/uploads/".$fileName)) {
                echo "Deleted File " . $fileName . "<br>";
            }
        }
    }
    $database->query('UPDATE studiophoto SET photoUrl = :low, captureType = :type, isPremium = :premium, originalRawPhotoUrl = :ori, processedPhotoHighResUrl = :high WHERE studioPhotoId = :id');
    $database->bind(":low", $_POST['lowImg']);
    $database->bind(":type", $_POST['type']);
    $database->bind(":premium", $_POST['premium']);
    $database->bind(":ori", $_POST['oriImg']);
    $database->bind(":high", $_POST['highImg']);
    $database->bind(":id", $_POST['id']);
    $database->execute();
    //$database->debugDumpParams();
    if($database->execute())
    {
        $studioId = $_POST['id'];
        $players = explode(",", $_POST['players']);

        $database->query('DELETE FROM studiophotothesaurus WHERE studioPhotoId = :studioId');
        $database->bind(":studioId", $studioId);
        
        $database->execute();
        
        for($i=0; $i<count($players); $i++)
        {
            $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
            $database->bind(":studioId", $studioId);
            $database->bind(":thesId", $players[$i]);
            $database->execute();
        }

        $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
        $database->bind(":studioId", $studioId);
        $database->bind(":thesId", $_POST['playerEvent']);
        $database->execute();

        $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
        $database->bind(":studioId", $studioId);
        $database->bind(":thesId", $_POST['season']);
        $database->execute();
        
        if($_POST['notDel'] != "")
        {
            $not = explode(",", $_POST['notDel']);
            for($j=0;$j<count($not); $j++)
            {
                $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
                $database->bind(":studioId", $studioId);
                $database->bind(":thesId", $not[$j]);
                $database->execute();
            }
        }

        if($_POST['tags'] != "")
        {
            $tags = explode(",", $_POST['tags']);
            
            for($i=0; $i<count($tags); $i++)
            {
                /*$database->query("INSERT INTO thesuarus (name, type)
                SELECT * FROM (SELECT :name, :type) AS tmp
                WHERE NOT EXISTS (
                    SELECT name, type FROM thesuarus WHERE name=:name and type = :type
                    );");*/

$database->query('INSERT INTO thesuarus (name, type) VALUES (:name, :type)');
$database->bind(":name", $tags[$i]);
$database->bind(":type", 4);
$database->execute();
if($database->rowCount() > 0)
{
    $tagId = $database->lastInsertId();

    $database->query('INSERT INTO studiophotothesaurus (studioPhotoId, thesuarusId) VALUES (:studioId, :thesId)');
    $database->bind(":studioId", $studioId);
    $database->bind(":thesId", $tagId);
    $database->execute();
}
}
}


        //Send Notification

$database->query('INSERT INTO notification (notificationType, notificationtime) VALUES (:type, :time)');
$database->bind(":type", 8);
$database->bind(":time", date("Y-m-d H:i:s"));
if ($database->execute()) {
        //Apple Push noti settings
    $passphrase = '';
    $ctx        = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'fcb.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

    $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        //Apple Push noti settings

    $database->query('SELECT fullName, email, pushnotificationtoken, devicetype FROM user WHERE blocked = :block ORDER BY fullName ASC');
                //$database->bind(':type', 1);
                //$database->bind(':type2', 2);
    $database->bind(':block', 0);
    $database->execute();
    $result = $database->resultset();
            //Send Noti
            //echo $database->rowCount();

    foreach ($result as $userRow) {
                //Android
        if ($userRow['devicetype'] == 1) {
            if (!empty($userRow['pushnotificationtoken'])) {
                $apiKey = "AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs";

                $registrationIDs = array(
                    $userRow['pushnotificationtoken']
                    );

                $message = 'Studio Library Modified';

                $url = 'https://android.googleapis.com/gcm/send';

                $fields = array(
                    'registration_ids' => $registrationIDs,
                    'data' => array(
                        "message" => $message
                        )
                    );

                $headers = array(
                    'Authorization: key=' . $apiKey,
                    'Content-Type: application/json'
                    );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);

                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $result = curl_exec($ch);
                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }

                curl_close($ch);
                        //var_dump($result);
            }
        } else {
            if (!empty($userRow['pushnotificationtoken'])) {
                $deviceToken = trim($userRow['pushnotificationtoken']);

                $message = 'Studio Library Modified';

                if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);

                        //echo 'Connected to APNS' . PHP_EOL;

                $body['aps'] = array(
                    'badge' => +1,
                    'alert' => $message,
                    'sound' => 'default'
                    );

                $payload = json_encode($body);

                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                $result = fwrite($fp, $msg, strlen($msg));


                        /*if (!$result)
                        echo    $noti_msg = 'Message not delivered';
                        else
                        echo    $noti_msg = 'Message successfully delivered ' . $message;
                        */
                    }
                }
            }
            //Apple Push noti
            @socket_close($fp);
            fclose($fp);
            //Apple Push noti
        } //Notification
        // Send notification

        
        echo "correct";
    } else 
    {
        echo "incorrect";
    }

} else if($command == 'isHiddenFalse')
{
    $ids = explode(",", $_POST['ids']);
    
    for($i=0; $i<count($ids); $i++)
    {
        $database->query('UPDATE studiophoto SET isHidden = :value WHERE studioPhotoId = :id');
        $database->bind(":id", $ids[$i]);
        $database->bind(":value", 'false');
        $database->execute();
    }
    echo "correct";
} else if($command == 'isHiddenTrue')
{
    $ids = explode(",", $_POST['ids']);
    for($i=0; $i<count($ids); $i++)
    {
        $database->query('UPDATE studiophoto SET isHidden = :value WHERE studioPhotoId = :id');
        $database->bind(":id", $ids[$i]);
        $database->bind(":value", 'true');
        $database->execute();
    }

    //Send Notification

    $database->query('INSERT INTO notification (notificationType, notificationtime) VALUES (:type, :time)');
    $database->bind(":type", 8);
    $database->bind(":time", date("Y-m-d H:i:s"));
    if ($database->execute()) {
        //Apple Push noti settings
        $passphrase = '';
        $ctx        = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'fcb.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        
        //Apple Push noti settings

        $database->query('SELECT fullName, email, pushnotificationtoken, devicetype FROM user WHERE blocked = :block ORDER BY fullName ASC');
                //$database->bind(':type', 1);
                //$database->bind(':type2', 2);
        $database->bind(':block', 0);
        $database->execute();
        $result = $database->resultset();
            //Send Noti
            //echo $database->rowCount();

        foreach ($result as $userRow) {
                //Android
            if ($userRow['devicetype'] == 1) {
                if (!empty($userRow['pushnotificationtoken'])) {
                    $apiKey = "AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs";

                    $registrationIDs = array(
                        $userRow['pushnotificationtoken']
                        );

                    $message = 'Studio Library Modified';

                    $url = 'https://android.googleapis.com/gcm/send';

                    $fields = array(
                        'registration_ids' => $registrationIDs,
                        'data' => array(
                            "message" => $message
                            )
                        );

                    $headers = array(
                        'Authorization: key=' . $apiKey,
                        'Content-Type: application/json'
                        );

                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $url);

                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    $result = curl_exec($ch);
                    if ($result === FALSE) {
                        die('Curl failed: ' . curl_error($ch));
                    }

                    curl_close($ch);
                        //var_dump($result);
                }
            } else {
                if (!empty($userRow['pushnotificationtoken'])) {
                    $deviceToken = trim($userRow['pushnotificationtoken']);

                    $message = 'Studio Library Modified';

                    if (!$fp)
                        exit("Failed to connect: $err $errstr" . PHP_EOL);

                        //echo 'Connected to APNS' . PHP_EOL;

                    $body['aps'] = array(
                        'badge' => +1,
                        'alert' => $message,
                        'sound' => 'default'
                        );

                    $payload = json_encode($body);

                    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                    $result = fwrite($fp, $msg, strlen($msg));


                        /*if (!$result)
                        echo    $noti_msg = 'Message not delivered';
                        else
                        echo    $noti_msg = 'Message successfully delivered ' . $message;
                        */
                    }
                }
            }
            //Apple Push noti
            @socket_close($fp);
            fclose($fp);
            //Apple Push noti
        } //Notification
        // Send notification


        echo "correct";
    } else if($command == 'getCities')
    {
        $id = $_POST['id'];
        $database->query('SELECT cityId, countryId, name_en FROM city WHERE countryId = :id');
        $database->bind(':id', $id);
        $database->execute();
        $result = $database->resultset();
        foreach ($result as $row) {
            echo '<option value="'.$row['cityId'].'">'.$row['name_en'].'</option>';
        }
    } else if($command == 'saveUserInfo')
    {
        unset($_SESSION['newUserId']);
        $fields = explode(',', $_POST['db']);
        $values = explode(',', $_POST['data']);

        $values[6] = date('Y-m-d', strtotime(html_entity_decode($values[6])));

        $umk = new umkPlugin('user', $fields, $values);
        $umk->insert();

        $_SESSION['newUserId'] = $database->lastInsertId();
        echo "~".$_SESSION['newUserId'];
    } else if($command == 'delPost')
    {
        $output_dir = 'third_party/uploads/users/'.$_POST['userid']."/";
        $fileName = $_POST['file'];
        if (!class_exists('S3'))require_once('S3.php');
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);

        if ($s3->deleteObject("barca", $output_dir.$fileName)) {
            $database->query("DELETE FROM post WHERE postId = :id");
            $database->bind(':id', $_POST['id']);
            if($database->execute())
            {
                echo "correct";
            } else 
            {
                echo "incorrect";
            }
        }
    } else if($command == 'delSelectedPost')
    {
        $output_dir = 'third_party/uploads/users/'.$_POST['userid']."/";
        $fileArray = explode(',', $_POST['files']);
        $idArray = explode(',', $_POST['ids']);

        if (!class_exists('S3'))require_once('S3.php');
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);
        for($i=0;$i<count($idArray);$i++)
        {
            $fileName = $fileArray[$i];
            if ($s3->deleteObject("barca", $output_dir.$fileName)) {
                $database->query("DELETE FROM post WHERE postId = :id");
                $database->bind(':id', $idArray[$i]);
                $database->execute();
            }
        }
        echo "correct";
    } else if($command == "uploadPost")
    {
        $output_dir   = $_POST['dir'];
        $fileName = str_replace(" ", "", $_FILES["myfile"]["name"]);

        if (!class_exists('S3'))require_once('S3.php');
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);
        $extension = end(explode('.', $fileName));
        $updated_name = time().'.'.$extension;

        if (!file_exists($output_dir)) {
           $s3->putObject(array( 
            'Bucket' => "barca",
            'Key'    => $output_dir,
            'Body'   => "",
            'ACL'    => 'public-read'
            ));
           if ($s3->putObjectFile($_FILES["myfile"]["tmp_name"], "barca", $output_dir.$updated_name, S3::ACL_PUBLIC_READ))
           {
            $ret[] = $updated_name;
        } else
        {
            $ret[] = "error";
        }
        
    } else {
        if ($s3->putObjectFile($_FILES["myfile"]["tmp_name"], "barca", $output_dir.$updated_name, S3::ACL_PUBLIC_READ))
        {
            $ret[] = $updated_name;
        } else
        {
            $ret[] = "error";
        }
    }
    echo json_encode($ret);
} else if($command == 'checkEmail')
{
    $database->query("SELECT email FROM user WHERE email = :email");
    $database->bind(":email", $_POST['email']);
    $database->execute();
    if($database->rowCount() > 0)
    {
        echo "incorrect";
    } else 
    {
        echo "correct";
    }
} else if($command == 'changeUserState')
{
    $idArray = explode(',', $_POST['ids']);
    $state = $_POST['state'];
    for($i=0;$i<count($idArray);$i++)
    {
        $database->query("UPDATE user SET blocked = '$state' WHERE userID = '$idArray[$i]'");
        $database->execute();
    }
    echo "correct";
} else if($command == 'followSelectedUser')
{
    $idArray = explode(',', $_POST['ids']);
    $userid = $_POST['userid'];

    for($i=0;$i<count($idArray);$i++)
    {
        $database->query("INSERT INTO userfans (userId, fanId, isFollow) VALUES (:uid, '$idArray[$i]', '1')");
        $database->bind(":uid", $userid);
        $database->execute();
    }
    echo "correct";
} else if($command == 'unfollowSelectedUser')
{
    $idArray = explode(',', $_POST['ids']);
    for($i=0;$i<count($idArray);$i++)
    {
        $database->query("DELETE FROM userfans WHERE id = :id");
        $database->bind(":id", $idArray[$i]);
        $database->execute();
    }
    echo "correct";
} else if($command == 'editUserInfo')
{
    $fields = explode(',', $_POST['db']);
    $values = explode(',', $_POST['data']);

    $values[6] = date('Y-m-d', strtotime(html_entity_decode($values[6])));

    if($_POST['subCommand'] == 'updateFile')
    {
        $database->query('SELECT profilePic FROM user WHERE userId = :id');
        $database->bind(":id", $_POST['value']);
        $database->execute();
        $row = $database->single();

        if (!class_exists('S3'))require_once('S3.php');
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);


        if($row['profilePic'] != $values[0] && !empty($row['profilePic']))
        {
            $haystack = $row['profilePic'];
            $needle = 'http';
            if (strpos($haystack,$needle) !== false) {
                $imgPath = $haystack;
            } else {
                $imgPath = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/profile/".$haystack;
            }
            $fileName = $imgPath;
            if ($s3->deleteObject("barca", $fileName)) {
                //echo "Deleted File " . $fileName . "<br>";
            }
        }
    }

    $key = $_POST['key'];
    $value = $_POST['value'];
    
    $umk = new umkPlugin('user', $fields, $values);
    $umk->update("WHERE $key = '$value'");   
} else if($command == 'updateThumb')
{
    $id = $_POST['id'];
    $file = $_POST['file'];
    $oldFile = $_POST['old'];
    if (!class_exists('S3'))require_once('S3.php');
    if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
    if (!defined('awsSecretKey')) define('awsSecretKey', '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');
    $s3 = new S3(awsAccessKey, awsSecretKey);
    if ($s3->deleteObject("barca", $oldFile)) {
      $database->query('UPDATE thesuarus SET photoUrl = :newfile WHERE thesuarusId = :id');
      $database->bind(":newfile", $file);
      $database->bind(":id", $id);
      if($database->execute())
      {
        echo 'correct';
    } else 
    {
        echo 'incorrect';
    }
}
} else if($command == 'keepPost')
{
    $id = $_POST['id'];
    $database->query('UPDATE post SET hidden = 0, reportCount = 0 WHERE postId = :id');
    $database->bind(":id", $id);
    if($database->execute())
    {
        echo 'correct';
    } else 
    {
        echo 'incorrect';
    }
} else if($command == 'hidePost')
{
    $id = $_POST['id'];
    $database->query('UPDATE post SET hidden = 1 WHERE postId = :id');
    $database->bind(":id", $id);
    if($database->execute())
    {
        $database->query('SELECT userId FROM post WHERE postId = :id');
        $database->bind(":id", $id);
        $database->execute();
        $row = $database->single();

        $userId = $row['userId'];
        $database->query('SELECT fullName, email FROM user WHERE userId = :id');
        $database->bind(":id", $userId);
        $database->execute();
        $userRow = $database->single();
        

        if(!empty($userRow['email']))
        {

            $mail = new PHPMailer();
            $mail->IsSMTP(); 
            //$mail->Host       = "smtp.zoho.com";
            //$mail->SMTPDebug  = 1;           
            // 1 = errors and messages
            // 2 = messages only
            $mail->SMTPAuth   = true;         
            $mail->Host       = "smtp.zoho.com";
            $mail->SMTPSecure = 'ssl'; 
            $mail->Port       = 465;
            $mail->Username   = "no-reply@fcbstudio.mobi";
            $mail->Password   = "T@W@$0lIT"; 

            $mail->AddReplyTo('no-reply@fcbstudio.mobi');
            $mail->SetFrom('no-reply@fcbstudio.mobi');
            $mail->AddAddress($userRow['email'], $userRow['fullName']);
            $mail->Subject    = 'FCB Reported Video';
            $mail->Body =  html_entity_decode('Hi, <br><br>Your post deleted because of user reports.<br><br> Thanks.');
            $mail->IsHTML(true);
            $mail->Send();
        }
        echo 'correct';
    } else 
    {
        echo 'incorrect';
    }
} else if($command == 'getOS')
{
    $id = $_POST['id'];
    $database->query('SELECT * FROM operatingsystems WHERE platformId = :id');
    $database->bind(':id', $id);
    $database->execute();
    $result = $database->resultset();
    foreach ($result as $row) {
        echo '<option value="'.$row['os'].'">'.$row['os'].'</option>';
    }
} else if($command == 'feedbackEmail')
{
    $emails = $_POST['emails'];
    $emails = explode(",", $emails);
    
    for($i=0; $i<count($emails); $i++)
    {
        if(!empty($emails[$i]))
        {
            $database->query('UPDATE registeredusers SET status = :status WHERE email = :emailId');
            $database->bind(":emailId", $emails[$i]);
            $database->bind(":status", "true");
            $database->execute();
            
            $mail = new PHPMailer();
            $mail->IsSMTP(); 
            //$mail->Host       = "smtp.zoho.com";
            //$mail->SMTPDebug  = 1;           
            // 1 = errors and messages
            // 2 = messages only
            $mail->SMTPAuth   = true;         
            $mail->Host       = "smtp.zoho.com";
            $mail->SMTPSecure = 'ssl'; 
            $mail->Port       = 465;
            $mail->Username   = "no-reply@fcbstudio.mobi";
            $mail->Password   = "T@W@$0lIT"; 

            $mail->AddReplyTo('no-reply@fcbstudio.mobi');
            $mail->SetFrom('no-reply@fcbstudio.mobi');
            $mail->AddAddress($emails[$i]);
            $mail->Subject    = 'FCB Studio Feedback';
            $mail->Body =  html_entity_decode($_POST['mesg']);
            $mail->IsHTML(true);
            if(!$mail->Send())
            {
                    //echo $mail->ErrorInfo;
            } else 
            {
                    //echo "YES";
            }
        }
}
echo 'correct';
} else if($command == "hidePosts")
{
    $id = $_POST['id'];
    $ids = explode(',', $id);
    for($i=0; $i<count($ids);$i++)
    {
        $database->query('UPDATE post SET hidden = 1 WHERE postId = :id');
        $database->bind(":id", $ids[$i]);
        $database->execute();
    }
    echo 'correct';
} else if($command == 'showPosts')
{
    $id = $_POST['id'];
    $ids = explode(',', $id);
    for($i=0; $i<count($ids);$i++)
    {
        $database->query('UPDATE post SET hidden = 0 WHERE postId = :id');
        $database->bind(":id", $ids[$i]);
        $database->execute();
    }
    echo 'correct';
}


// File Processing
if ($command == "uploadData") {

    $output_dir   = $_POST['dir'];
    $thumb        = $_POST['thumb'];
    $random_digit = time();
    
    if (isset($_FILES["myfile"])) {
        $ret = array();
        
        $error = $_FILES["myfile"]["error"];
        //You need to handle  both cases
        //If Any browser does not support serializing of multiple files using FormData() 
        if (!is_array($_FILES["myfile"]["name"])) //single file
        {
            //$fileName = $random_digit . "_" . str_replace(" ", "", $_FILES["myfile"]["name"]);
            $fileName = str_replace(" ", "", $_FILES["myfile"]["name"]);
            $path      = $output_dir . $fileName;
            $thumbPath = str_replace("uploads", "thumbs", $output_dir) . $fileName;
            
            if (!class_exists('S3'))require_once('S3.php');
            if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
            if (!defined('awsSecretKey')) define('awsSecretKey', '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');
            $s3 = new S3(awsAccessKey, awsSecretKey);
            $extension = end(explode('.', $fileName));
            $updated_name = time().'.'.$extension;
            if ($s3->putObjectFile($_FILES["myfile"]["tmp_name"], "barca", $output_dir.$updated_name, S3::ACL_PUBLIC_READ))
            {
                $ret[] = $updated_name;
            } else
            {
                $ret[] = "error";
            }
            
            /*if (move_uploaded_file($_FILES["myfile"]["tmp_name"], $path)) {
                if ($thumb == "Yes") {
                    makeThumb($path, "100", $thumbPath, $fileName, "false");
                }
                $ret[] = $fileName;
            } else {
                $ret[] = "error";
            }*/
        } else //Multiple files, file[]
        {
            $fileCount = count($_FILES["myfile"]["name"]);
            for ($i = 0; $i < $fileCount; $i++) {
                $fileName = $_FILES["myfile"]["name"][$i];
                move_uploaded_file($_FILES["myfile"]["tmp_name"][$i], $output_dir . $fileName);
                $ret[] = $fileName;
            }
            
        }
        echo json_encode($ret);
    }
} else if ($command == "deleteData") {
    $output_dir = $_POST['dir'];
    if (isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name'])) {
        $fileName = $_POST['name'];
        $filePath = $output_dir . $fileName;
        
        if (!class_exists('S3'))require_once('S3.php');
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);

        if ($s3->deleteObject("barca", $output_dir.$fileName)) {
            echo "Deleted File " . $fileName . "<br>";
        }

        /*if (file_exists($filePath)) {
            unlink($filePath);
            //unlink(str_replace("uploads", "thumbs", $filePath));
        }
        echo "Deleted File " . $fileName . "<br>";
        */
    }
}
?>