
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo $rootPath; ?>js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $rootPath; ?>plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo $rootPath; ?>js/jquery.validate.js"></script>

    <script src="<?php echo $rootPath; ?>plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <script src="<?php echo $rootPath; ?>plugins/upload/jquery.uploadfile.min.js"></script>

    <script src="<?php echo $rootPath; ?>js/scripts.js"></script>
  </body>
</html>