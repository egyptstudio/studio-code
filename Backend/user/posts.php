<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$_SESSION['lib'] = 22;

$state = isset($_REQUEST['state']) ? $_REQUEST['state'] : '0';

if(!is_numeric($state))
{
    $state = 0;
    
} 
if($state == 0)
{
    $text = 'Hide';
}
else {
    $text = 'Show';
}

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}
$startFrom = ($page-1) * $recPerPage;
$database->query('SELECT u.fullName, p.userId, p.postId, p.creationTime, p.postPhotoUrl, p.likeCount, p.reportCount, p.hidden FROM post p, user u WHERE p.hidden = :state AND u.userId = p.userId ORDER BY p.creationTime DESC');
$database->bind(':state', $state);       
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT u.fullName, p.userId, p.postId, p.creationTime, p.postPhotoUrl, p.likeCount, p.reportCount, p.hidden FROM post p, user u WHERE p.hidden = :state AND u.userId = p.userId ORDER BY p.creationTime DESC LIMIT :start, :num');
$database->bind(':state', $state);

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Latest Posts</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn btn-<?php echo $text; ?> btn-primary"><?php echo $text; ?> Selected</a>
                        </div>
                    </div>

                    <div class="photoOpts1" style="border: 0;width: 70%;">
                        <div class="btn-group" role="group" aria-label="...">
                          <a href="posts.php?page=<?php echo $page; ?>&state=0" class="btn btn-primary <?php if($state==0) {echo 'activePlayerMenu'; } ?>">Posts</a>
                          <a href="posts.php?page=<?php echo $page; ?>&state=1" class="btn btn-primary <?php if($state==1) {echo 'activePlayerMenu'; } ?>">Hidden Posts</a>
                      </div>
                  </div>


                  <div class="postSection">
                    <div class="aj_sec">
                        <?php if($total_records > 0) { ?>
                        <div class="opts3" style="margin-top: -52px;">
                            <nav class="siteNav pull-left">
                              <ul class="pagination">
                                <li>
                                  <a href="posts.php?page=1&state=<?php echo $state; ?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <?php
                            if($page == 1)
                            {
                                $prevPage = 1;
                            } else 
                            {
                                $prevPage = ($page - 1);
                            }

                            if($page == $total_pages)
                            {
                                $nextPage = $total_pages;
                            } else 
                            {
                                $nextPage = ($page + 1);
                            }
                            echo '<li><a href="posts.php?page='.$prevPage.'&state='.$state.'">&lt;</a></li>';
                            echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                            echo '<li><a href="posts.php?page='.$nextPage.'&state='.$state.'">&gt;</a></li>';
                            ?>
                            <li>
                              <a href="posts.php?page=<?php echo $total_pages ?>&state=<?php echo $state; ?>" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="col-md-12">

                <table class="table table-bordered userTable">
                    <thead>
                        <tr>
                            <th>Select <input type="checkbox" id="selectall"></th>
                            <th>Image</th>
                            <th>Username</th>
                            <th>Post Status</th>
                            <th>Number of Likes</th>
                            <th>Number of Reports</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $result = $database->resultset();
                        $database->query('SELECT startDateTime, endDateTime FROM contest WHERE status = :c');
                        $database->bind(":c", 2);
                        $database->execute();
                        $contestRow = $database->single();

                        foreach ($result as $row) {
                            if(($row['creationTime'] >= $contestRow['startDateTime']) && ($row['creationTime'] <= $contestRow['endDateTime']))
                            {
                                $postStatus = 'In Contest';
                            } else 
                            {
                                $postStatus = 'Out of Contest';
                            }
                            ?>
                            <tr>
                                <td>
                                    <span><input class="checkbox" type="checkbox" value="<?php echo $row['postId']; ?>"></span>
                                </td>
                                <td>
                                    <img width="100" src="<?php echo 'https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/users/'.$row['userId'].'/'.$row['postPhotoUrl']; ?>" alt="Post">
                                </td>
                                <td>
                                    <span class="fullname"><?php echo $row['fullName']; ?></span>
                                </td>
                                <td>
                                    <span class="pstatus"><?php echo $postStatus; ?></span>
                                </td>
                                <td>
                                    <span class="plikes"><?php echo $row['likeCount']; ?></span>
                                </td>
                                <td>
                                    <span class="rcount"><?php echo $row['reportCount']; ?></span>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php } else { 
                    echo "<h2 style='margin-left: 20px'>List of posts will be displayed here.</h2>"; 
                } ?>
                
                <div class="opts3">
                    <nav style="margin-bottom: 0px;" class="siteNav pull-left">
                      <ul style="margin-bottom: 0px;" class="pagination">
                        <li>
                          <a href="posts.php?page=1&state=<?php echo $state; ?>" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <?php
                    if($page == 1)
                    {
                        $prevPage = 1;
                    } else 
                    {
                        $prevPage = ($page - 1);
                    }

                    if($page == $total_pages)
                    {
                        $nextPage = $total_pages;
                    } else 
                    {
                        $nextPage = ($page + 1);
                    }
                    echo '<li><a href="posts.php?page='.$prevPage.'&state='.$state.'">&lt;</a></li>';
                    echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                    echo '<li><a href="posts.php?page='.$nextPage.'&state='.$state.'">&gt;</a></li>';
                    ?>
                    <li>
                      <a href="posts.php?page=<?php echo $total_pages ?>&state=<?php echo $state; ?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

</div><!-- aj_sec -->
</div><!-- postSection -->

</div>

</div>
</div>
</div>
</div>
<input type="hidden" value="" id="ids">
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title userName" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">
            <img width="200" src="" class="postPhotoHolder img-responsive" style="margin: 0 auto" alt="Image">
            <form style="margin-top: 10px; padding-top: 15px; border-top: 1px solid #ccc;" class="form-horizontal myform">
                <div class="form-group">
                    <label for="l2" class="col-sm-6 control-label"><h4 class="tb_title">Photo Status</h4></label>
                    <div class="col-sm-6">
                        <p class="postStatus"></p>    
                    </div>
                </div>

                <div class="form-group">
                    <label for="l2" class="col-sm-6 control-label"><h4 class="tb_title">Number of likes</h4></label>
                    <div class="col-sm-6">
                        <p class="likes"></p>    
                    </div>
                </div>

                <div class="form-group">
                    <label for="l2" class="col-sm-6 control-label"><h4 class="tb_title">Number of Reports</h4></label>
                    <div class="col-sm-6">
                        <p class="reports"></p>    
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" data-id="" class="btn btn-success btn-keep">Keep</button>
            <button type="button" data-id="" class="btn btn-danger btn-hide">Hide</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>


<?php
include_once ('../includes/footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        var idArray = [];
        /*$(document).on('click', '.openPop', function(){
            $(".userName").text($(this).parents('tr').find('span.fullname').text());
            $(".postStatus").text($(this).parents('tr').find('span.pstatus').text());
            $(".likes").text($(this).parents('tr').find('span.plikes').text());
            $(".reports").text($(this).parents('tr').find('span.rcount').text());
            $(".postPhotoHolder").attr('src', $(this).find('img').attr('src'));
            $(".btn-keep").attr('data-id', $(this).attr('data-id'));
            $(".btn-hide").attr('data-id', $(this).attr('data-id'));
            $("#myModal").modal('show');
        });*/


$(document).on('click', ".checkbox", function(){
            //if($(this).is(':checked'))
            if(this.checked)
            {
              idArray.push($(this).val());
              $("#ids").val(idArray);
              
          } else 
          {
            var text = $(this).val();
            pos = getPosition(idArray, text);

            var remove = idArray[pos];
            idArray = jQuery.grep(idArray, function(value) {
              return value != remove;
          });

            var newVal = removeValue($("#ids").val(), remove);
            $("#ids").val(newVal);
        }
    });

$('#selectall').click(function(event) { 
    if(this.checked) { 
        $('.checkbox').each(function() {
            this.checked = true;
            idArray.push($(this).val());
        });
        $("#ids").val(idArray);
    }else{
        $('.checkbox').each(function() { 
            this.checked = false; 
        });
        $("#ids").val(''); 
        idArray = [];
    }
});


$(".btn-Hide").click(function(){
    if($("#ids").val() == "")
    {
        showNotification('warning', 'Please select atleast 1 post!');
        return false;
    }
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/processing.php',
        data: 'id=' + $("#ids").val() + '&command=' + 'hidePosts',
        cache: false,
        success: function(data) {
            hideLoader();
            if(data == 'correct')
            {
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec"); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(".btn-Show").click(function(){
    if($("#ids").val() == "")
    {
        showNotification('warning', 'Please select atleast 1 post!');
        return false;
    }
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/processing.php',
        data: 'id=' + $("#ids").val() + '&command=' + 'showPosts',
        cache: false,
        success: function(data) {
            hideLoader();
            if(data == 'correct')
            {   
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec"); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

});
</script>