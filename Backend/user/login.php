<?php include_once('includes/header.php'); 
if(isset($_SESSION['log_id']))
{
	session_unset();
	session_destroy();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-offset-4 col-md-3 vertical-center">
            <div class="form-login">
                <h3>Please sign in</h3>
            <form id="login_form">
                <input type="text" name="userName" class="form-control input-sm chat-input getValue" required data-key="username" placeholder="Username" />
                </br>
                <input type="password" name="userPassword" class="form-control input-sm chat-input getValue" required data-key="password" placeholder="Password" />
                </br>
                <div class="wrapper">
                    <span class="group-btn">
                    <button type="submit" class="btn btn-primary btn-md" id="login">
                    	Login <i class="fa fa-sign-in"></i>
                    </button>
                    <!--<a href="#" class="btn btn-primary btn-md" id="login">login <i class="fa fa-sign-in"></i></a>-->
                    </span>
                </div>
            </form>
            </div>

        </div>

    </div>
</div>
<?php include_once('includes/footer.php'); ?>