package com.etiennelawlor.quickreturn.library.listeners;

/**
 * 
 * @author Basyouni
 * a listener to listen to view state change
 *
 */
public interface OnViewStateChanged {

	void viewDisappeared();//view disappeared 
	void viewAppeared();//view appeared
	
}
