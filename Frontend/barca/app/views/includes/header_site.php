<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<title>Choose business type</title>
<!-- Bootstrap core CSS -->
<link href="<?=base_url()?>includes/css_site/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url()?>includes/css_site/jquery.steps.css">
<link href="<?=base_url()?>includes/css_site/style.css" rel="stylesheet">
<link href="<?=base_url()?>includes/css_site/owl.carousel.css" rel="stylesheet">
<link href="<?=base_url()?>includes/css_site/bootstrap-colorpicker.css" rel="stylesheet">
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="<?=base_url()?>includes/js_site/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="<?=base_url()?>includes/js_site/ie-emulation-modes-warning.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?=base_url()?>includes/js_site/ie10-viewport-bug-workaround.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>includes/css_site/style_tabs.css"/>


</head>

<body>

<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container"> 
  <a class="navbar-brand" href="#"><img src="<?=base_url()?>includes/img_site/goapp-logo.png" alt="go-app-logo"></a> 
  <a role="button" href="#" class="btn btn-primary pull-right chat_btn"><i class="glyphicon glyphicon-comment"></i> Live Chat</a>
  </div>
</nav>
<div class="container wrap">
  
<!--<div class="nex_prev">
  <h4 class="text-center">Select Your Business</h4>
 </div> --> 
 
<br/> 


<div class="content">
 

