<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $title;?></title>
<link href="<?php echo base_url();?>includes/style.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>includes/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>

<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>



<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<script src="<?php echo base_url();?>assets/grocery_crud/js/jquery-1.10.2.min.js" type="text/javascript"></script>



	

<script type="text/javascript">
	$(document).ready(function(){
 		
 $('.user').click(function(){
 	
 	var serial = $(this).attr("id");
 	$('.user').css('background-color','transparent');
 	$(this).css('background-color','blue');
 	$.ajax({
    type: "POST",
    url: "<?= base_url()?>index.php/advisors/advisor/chat",
    data: { serial_number: serial}
    }).done(function( msg ) {
     //alert( "Data Saved: " + msg );
     $("#chatDiv").html(msg);
     $('#message_text').scrollTop($('#message_text')[0].scrollHeight);
	});
 	
 });
 
});
</script>

<style>
	tfoot {
		display:none;
	}
	table.dataTable tr.odd{
		background-color: #F7F7FA;
	}
	ul.MenuBarHorizontal ul li{
		width: 11em;
	}
	div.DTTT_container,.datatables-add-button{
		display: none;
	}
	.users{
		width:150px;
		height:500px;
		background-color: #ccc;
		overflow-y: scroll;
		text-align: center;
		float:left;
		
	}
	.user{
		width:150px;
		margin-top: 10px;
		cursor: pointer;
		
	}
	#message_text{
		width: 700px;
		background-color:#eee;
		height:350px;
		overflow-y: scroll;
		
	}
</style>

<link href="<?php echo base_url();?>includes/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />


</head>

<body onload="MM_preloadImages('<?php echo base_url();?>includes/images/log-in-hover_15.png','images/search002.jpg')">
	<div class="pannr-main2"></div>
	
	<div class="container-body">
		<div class="body-main1"></div>
		<div class="top-org" align="center"><?= $title;?></div>
		
		
		<div class="menu-in" align="center">

 <ul id="MenuBar1" class="MenuBarHorizontal" style="float: left;width: 900px;margin-left: 50px;">
 	    <li><a href="<?php echo site_url('advisors/login/logout')?>">Logout</a></li>

 </ul>
 	
</div>
	</div>
	
	<div class="container-body">
		<div class="fields1">