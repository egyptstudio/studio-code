<!DOCTYPE html>
<html>

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<title>FCB Studio | Login</title>
	<script src="<?php echo base_url();?>includes/jquery-1.8.0.js" type="text/javascript"></script>
	
	<link href="<?php echo base_url();?>includes/style.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<script type="text/javascript">
	$(document).on("keyup", function(event) {
  if (event.which == 13 && ($("#password").is(":focus") || $("#username").is(":focus") || ($('#password').val() != "" && $('#username').val() != "") && $('#username').val() != "")) { 
   $('#btn').click();
  } 
 });
</script>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>

<script type="text/javascript">
	function submit_form() {
		//$('#submit_form1').submit();
		//document.getElementById('submit_form1').submit();
	}
</script>

</head>

<body onload="MM_preloadImages('images/log-in-hover_15.png')">

	<div class="logo" align="center"><img src="<?php echo base_url();?>includes/images/logo2.png"  width="530" height="217" /></div>


	<div class="user-pass">
		<div class="right-log"></div>

		<div class="text-field">

			<div id="login_form1" class="login-header">Login</div>

			<form id="submit_form1" name="submit_form1" method="POST" action="<?php echo base_url()?>index.php/login/validate_credentials">
			<div class="user-header">
				<div class="username">User Name</div>
				<div class="text-field-user">
					<input class="inputForm" id="username" name="username" type="text"/>
				</div>
			</div>
			
			<div class="password-header">
				<div class="username">Password</div>
				<div class="text-field-user">
					<input class="inputForm" id="password" name="password" type="password"/>
				</div>
			</div>
			<div align="center">
					<span style="color: #FFFFFF;text-align: center;font-weight: bold"  class='erro' ><?=$error?></span>
					</div>	

			<div class="button-login">
				<a href="#" style="border: none" id="btn" onclick="submit_form();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','<?php echo base_url();?>includes/images/log-in-hover_15.png',1)">
					<img style="border: none" src="<?php echo base_url();?>includes/images/log-in_15.png" alt="login" width="75" height="30" id="Image1" />
				</a>
					
			</div>
			
		
			
			</form>

		</div>

		<div class="left-log"></div>
		
	</div>

</body>
</html>

