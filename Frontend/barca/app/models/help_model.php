<?php
class Help_model extends CI_Model{
	function __construct() {
		parent::__construct();
	}

	//Functions
	function getHelpScreen($lang)
	{
		//Get
		$this->db
			->select('*')
			->from('helpscreen')
			->where('languageCode', $lang);
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			return $query->result_array();
		}
		return 0;
	}
}
?>