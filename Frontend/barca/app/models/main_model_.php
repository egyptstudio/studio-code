<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Main_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function getData($table,$fields='',$where='',$order_by='',$order='',$limit='',$start='')
	{
		if($fields!='' && $fields!='*')
			$this->db->select($fields);
		if($where!='')
		{
			if(is_array($where))
				$this->db->where($where);
			else
				$this->db->where($where, NULL, FALSE);
		}
		if($order_by!='' && $order!='')
			$this->db->order_by($order_by, $order);
		if($limit != '' && $start != '')
			$this->db->limit($limit, $start);
		$query = $this->db->get($table);
		$res = $query->result();
		if(count($res)>0)
			return $res;
		return FALSE;
	}
	
	public function getBackendData($table,$fields='',$where='',$order_by='',$order='',$limit='',$start='',$additional_params='')
	{
		if($fields!='' && $fields!='*')
			$this->db->select($fields, FALSE);
		if($where!='')
		{
			if(!is_array($where))
			{
				if(strpos($where, "(")!==0)
					$this->db->where($where, NULL, FALSE);
				else
					$this->db->where($where);
			}
			else
				$this->db->where($where);
		}
		if($order_by!='' && $order!='')
			$this->db->order_by($order_by, $order);
		if($limit !== '' && $start !== '')
			$this->db->limit($limit, $start);
		if($additional_params!='')
			foreach($additional_params as $k=>$v)
			{
				$k=str_replace("2", "", $k);
				$k=str_replace("3", "", $k);
				$k=str_replace("4", "", $k);
				if(count($v)==3)
					$this->db->$k($v[0],$v[1],$v[2]);
				elseif(count($v)==2)
					$this->db->$k($v[0],$v[1]);
				elseif(count($v)==1)
					$this->db->$k($v[0]);
			}
		
		$query = $this->db->get($table);
		$res = $query->result();
		if(count($res))
			return $res;
		return false;
	}

	public function count($table,$where='',$additional_params='')
	{
		if($where!='')
			$this->db->where($where);
		if($additional_params!='')
			foreach($additional_params as $k=>$v)
			{
				$k=str_replace("2", "", $k);
				$k=str_replace("3", "", $k);
				$k=str_replace("4", "", $k);
				if(count($v)==3)
					$this->db->$k($v[0],$v[1],$v[2]);
				elseif(count($v)==2)
					$this->db->$k($v[0],$v[1]);
				elseif(count($v)==1)
					$this->db->$k($v[0]);
			}
		return $this->db->count_all_results($table);
	}
	
	public function insert($table,$insertArr) {
		$this -> db -> insert($table, $insertArr);
		return $this -> db -> insert_id();
	}
	
	public function update($table,$updatedArr,$where) {
		$this->db->where($where);
		return $this->db->update($table, $updatedArr);
	}
	
	public function increment($table,$field,$where) {
		$this->db->query("update `".$table."` set `".$field."`=(`".$field."`+1) WHERE ".$where);
		return true;
	}
	
	public function delete($table,$where)
	{
		$this->db->delete($table,$where);
	}
	
	public function getNotifications()
	{
		$pushNotificationPeriod=0;
		
		$send_minute=(intval(date('i'))-$pushNotificationPeriod);
		$this->db->where("(`last_sent` IS NULL or `last_sent`!='".date('Y-m-d')."') and Time(`notification_date`)='".date('H:i:00')."' and ((`notification_date`='".date('Y-m-d H:i:00')."' and `notification_type`='1') or `notification_type`='2' or (`".strtolower(date('l'))."`='1' and `notification_type`='3'))", NULL, FALSE);
		$query = $this->db->get('notification');
		$res = $query->result();
		if(count($res))
			return $res;
		return false;
	}
	

	
	
	
	
}
?>