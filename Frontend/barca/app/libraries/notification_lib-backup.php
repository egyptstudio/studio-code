<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Notification_lib {

	function push_android($deviceRegistrationId, $messageText , $type )
	{
		$apiKey= 'AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs';
		$headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $apiKey);
		$data = array(
		'data' => array('message' => $messageText,'type'=>$type),
		'registration_ids' => array($deviceRegistrationId)
		);
		 
		$ch = curl_init();
		 
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send" );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
		 
		$response = curl_exec($ch);
		curl_close($ch);
		 
		//echo $response;
	}

	public function push_iphone($deviceToken, $message, $type) {

		// Put your device token here (without spaces):
		//$deviceToken = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
		// Put your private key's passphrase here:
		//$passphrase = 'xxxxxxx';
		// Put your alert message here:
		//$message = 'A push notification has been sent!';

		////////////////////////////////////////////////////////////////////////////////
		//$passphrase = "manal";
		$passphrase = "12345678";
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', FCPATH . 'third_party/fcb.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		echo 'Connected to APNS' . PHP_EOL;

		// Create the payload body
		//$body['aps'] = array('alert' => array('body' => $message, 'action-loc-key' => 'Bango App', ), 'badge' => 2, 'sound' => 'oven.caf', );

		$body['aps']['alert'] = array('action-loc-key' => 'view', 'body' => $message);
		$body['aps']['sound'] = 'default';
		$body['aps']['type'] = $type;
/*		messageRemoteID 
messageText
senderName
 senderPicture 
senderUserID
sentAt*/
		 
		$body['badge'] = '1';

		// Encode the payload as JSON
		$payload = json_encode($body);

		//print_r($payload);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered' . PHP_EOL.'<br>';

		// Close the connection to the server
		fclose($fp);
	}
}
?>