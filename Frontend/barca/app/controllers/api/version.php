<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Version extends CI_Controller {

	function __construct() {
		parent::__construct();

		/* Standard Libraries */
		//$this->load->database();
		/* ------------------ */

	}

	public function getBaseURL() {
		
		file_put_contents('testindex.txt', '
'.date('Y-m-d H:i:s').' # '.$_SERVER['REQUEST_URI'].' -- '.$_POST['data'], FILE_APPEND);

		$json_data = $this -> input -> post('data');
		 //$json_data ='{"userID" : "1221","fanID":55 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$versionNumber = ($arr_data -> versionNumber)?:'1.0.60';
		$deviceType = intval($arr_data -> deviceType)?:'1';

		if (empty($versionNumber) || empty($deviceType)) {

			print_r(json_encode(array('status' => -1)));

		} else {
			$additionalParams['join'] = array('fcbservers', 'fcbservers.serverId=fcbversion.serverId');
			$version = $this -> main_model -> getBackendData('fcbversion', 'fcbversion.versionNumber,fcbversion.updateStatus,fcbversion.serverId,baseURL', array('versionNumber' => $versionNumber, 'deviceType' => $deviceType), '', '', '', '', $additionalParams);
			if ($version) {
				print_r(json_encode(array('status' => 2, 'data' => array('updateStatus' => $version[0] -> updateStatus, 'baseURL' => $version[0] -> baseURL))));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
		
				file_put_contents('testindex.txt', json_encode(array('status' => 2, 'data' => array('updateStatus' => $version[0] -> updateStatus, 'baseURL' => $version[0] -> baseURL))), FILE_APPEND);

	}
	
}
