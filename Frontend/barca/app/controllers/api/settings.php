<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class settings extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function changePassword() {

		$json_data = $this -> input -> post('data');
		// $json_data ='{"currentPassword":"1","oldPassword":"654564","userId":"711"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$currentPassword = ($arr_data -> currentPassword);
		$oldPassword = ($arr_data -> oldPassword);

		if (intval($userId) < 1 || empty($currentPassword) || empty($oldPassword))
			print_r(json_encode(array('status' => -1)));
		else {
			$checkuser = $this -> main_model -> getData('user', 'userId,password', array('password' => md5($oldPassword), 'userId' => $userId));
			if ($checkuser) {
				$upd = $this -> main_model -> update('user', array('password' => md5($currentPassword)), array('userId' => $userId));
				if ($upd)
					print_r(json_encode(array('status' => 1)));
				else
					print_r(json_encode(array('status' => -2)));

			} else
				print_r(json_encode(array('status' => -3, 'ruleName' => 'passwordMismatch', 'errorMessage' => 'old password is not correct, please provide a correct password')));

		}
		$this->db->close();
	}

	public function sendContactUs() {

		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"0","filterBy":"1","seasons":[200,202]}';
		$arr_data = json_decode($json_data);

		$email = ($arr_data -> email);
		$messageTitle = ($arr_data -> messageTitle);
		$messageText = ($arr_data -> messageText);

		if (empty($email) || empty($messageText) || empty($messageTitle))
			print_r(json_encode(array('status' => -1)));
		else {
			$ins = $this -> main_model -> insert('contactus', array('email' => $email, 'messageTitle' => $email, 'messageText' => $email));
			if ($ins)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -2)));
		}
		$this->db->close();
	}

	public function info_links() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"0","filterBy":"1","seasons":[200,202]}';
		$arr_data = json_decode($json_data);

		$lang = ($arr_data -> lang);
		$page = ($arr_data -> page);

		if (empty($lang) || empty($page) || ($page != "terms" && $page != "privacy" && $page != "about_fcb" && $page != "about_tawasol"))
			print_r(json_encode(array('status' => -1)));
		else {
			$base_url = "https://www.fcbstudio.mobi/info/" . $lang . "/" . $page . ".html";
			print_r(json_encode(array('status' => 2, 'data' => array("link" => $base_url))));
		}
		$this->db->close();
	}

	public function getBlockedUsers() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"0","filterBy":"1","seasons":[200,202]}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);

		if (empty($userId))
			print_r(json_encode(array('status' => -1)));
		else {
			$additionalParams['join'] = array('user as u', 'ub.fanId=u.userId', 'left');
			$blocked_users = $this -> main_model -> getBackendData('userblock as ub', 'ub.userId,ub.fanId,u.fullName as username', array('ub.userId' => $userId), '', '', 200, 0, $additionalParams);
			if ($blocked_users) {
				foreach ($blocked_users as $user) {
					unset($user -> userId);
					$user -> fanId = intval($user -> fanId);
					$user -> username = strval($user -> username);
					$data[] = $user;
				}
				print_r(json_encode(array('status' => 2, 'data' => $data)));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	public function unblockUser() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"0","filterBy":"1","seasons":[200,202]}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$fanId = intval($arr_data -> fanId);

		if (empty($userId) || empty($fanId))
			print_r(json_encode(array('status' => -1)));
		else {
			$del = $this -> main_model -> delete('userblock', array('userId' => $userId, 'fanId' => $fanId));

			print_r(json_encode(array('status' => 1)));

		}
		$this->db->close();
	}

	public function sendInviteInvitationText() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"0","filterBy":"1","seasons":[200,202]}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$invitationText = strval($arr_data -> invitationText);

		if (empty($userId) || empty($invitationText))
			print_r(json_encode(array('status' => -1)));
		else {
			$upd = $this -> main_model -> update('user', array('SMSInviteText' => $invitationText), array('userId' => $userId));
			if ($upd)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -2)));

		}
		$this->db->close();
	}

	public function filterUserContacts() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"0","filterBy":"1","seasons":[200,202]}';
		//$json_data = '{"pageNo":"1","userId":"711","UserContact":[{"userId":0,"email":"tttdft@ttt.com","phone":"154545"},{"userId":0,"email":"ee22@nn.cc","phone":"1545450"}]}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$pageNo = strval($arr_data -> pageNo);
		$UserContact = ($arr_data -> UserContact);

		if (empty($userId))
			print_r(json_encode(array('status' => -1)));
		else {
			foreach ($UserContact as $contact) {
				$emails[] = $contact -> email;
				$phones[] = $contact -> phone;
			}

			$additionalParams['where_in'] = array('email', $emails);
			$additionalParams['or_where_in'] = array('phone', $phones);

			$users = $this -> main_model -> getBackendData('user', "userId,email,phone", "", "userId", "desc", 50, 0, $additionalParams);
			if ($users) {
				foreach ($users as $user) {
					$user -> userId = intval($user -> userId);
					$user -> phone = strval($user -> phone);
					$user -> email = strval($user -> email);
					$data[] = $user;
				}
				print_r(json_encode(array('status' => 2, 'data' => $data)));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	public function getLinks() {
		$json_data = $this -> input -> post('data');
		//$json_data = '{"name":"test", "lang":"en"}';
		$arr_data = json_decode($json_data);

		$name = strval($arr_data -> name);
		$lang = strval($arr_data -> lang);
		
		if (empty($name))
			print_r(json_encode(array('status' => -1)));
		else {
			
			$result = $this -> main_model -> getData('general_links', "*", array('name' => $name, 'lang' => $lang ));
			if ($result) {
				print_r(json_encode(array('status' => 2, 'data' => $result)));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	// private function

}
