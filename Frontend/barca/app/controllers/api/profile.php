<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function updateUserProfile() {
		//file_put_contents('test35.txt', serialize($_POST));
		$json_data = $this -> input -> post('data');
		//$json_data ='{"lang":"en","user":{"fullName" : "osama","profilePic":"101010.png","userId":"711","countryId":"1","cityId":"2","email":2 ,"phone":1545646465,"dateOfBirth":"01-04-1981","gender":"2","district":"asdasd","status":"asdsad","aboutMe":"asdasd","relationship":"1","religion":"aaa","education":"aaaaaa","job":"aaaaa","company":"aaaaa","income":"1","receiveFriendRequest":"1","viewPersonalInfo":"1","viewContactsInfo":"1","receivePushNotification":"1"}}';
		$user_data = json_decode($json_data);
		$arr_data = $user_data -> user;
		$userId = intval($arr_data -> userId);
		$fullName = strval($arr_data -> fullName);
		$lang = strval($user_data -> lang);
		$profilePic = strval($arr_data -> profilePic);
		$countryId = intval($arr_data -> countryId);
		$cityId = intval($arr_data -> cityId);

		$countryName = strval($arr_data -> countryName);
		$cityName = strval($arr_data -> cityName);

		$phone = strval($arr_data -> phone);
		$phoneCode = strval($arr_data -> phoneCode);
		$email = strval($arr_data -> email);
		$phone = strval($arr_data -> phone);
		$dateOfBirth = ($arr_data -> dateOfBirth);
		$gender = intval($arr_data -> gender);
		$district = strval($arr_data -> district);
		$aboutMe = strval($arr_data -> aboutMe);

		$relationship = intval($arr_data -> relationship);
		$religion = strval($arr_data -> religion);
		$education = intval($arr_data -> education);
		$job = intval($arr_data -> job);
		$jobRole = intval($arr_data -> jobRole);
		$company = strval($arr_data -> company);
		$income = strval($arr_data -> income);
		$status = strval($arr_data -> status);
		$countryCode = strval($arr_data -> countryCode);

		$receiveFriendRequest = ($arr_data -> receiveFriendRequest);
		$viewPersonalInfo = ($arr_data -> viewPersonalInfo);
		$viewContactsInfo = ($arr_data -> viewContactsInfo);
		$receivePushNotification = ($arr_data -> receivePushNotification);
		$image = $_FILES['photo'];

		if (!empty($userId) && !empty($lang) && !empty($email)) {

			$this->checkUserFields($userId);

			$emailWhere = "userId != ".$userId." AND email = '".$email."'";
			$emailResult = $this -> main_model -> getData('user', 'email, userId', $emailWhere);
			if($emailResult)
			{
				print_r(json_encode(array('status' => -2)));
				return false;
			}
			
			if(!empty($countryName))
			{
				$countryWhere = "countryName = '".$countryName."'";
				$countryResult = $this -> main_model -> getData('country', 'countryId', $countryWhere); 
				
				if($countryResult)
				{
					$countryId = $countryResult[0]->countryId;
				} else 
				{
					$countryId = $countryId;
				}
			}

			if(!empty($cityName))
			{
				$cityWhere = "name_en = '".$cityName."'";
				$cityResult = $this -> main_model -> getData('city', 'cityId', $cityWhere); 
				if($cityResult)
				{
					$cityId = $cityResult[0]->cityId;
				} else 
				{
					$cityId = $cityId;
				}
			}

			if(!empty($phone))
			{
				$phoneWhere = "app_phone_no = '".$phone."'";
				$phoneResult = $this -> main_model -> getData('phone_ads', 'phone_id, app_phone_no, status', $phoneWhere); 
				if($phoneResult)
				{
					if($phoneResult[0]->status == 0)
					{

						$phoneId = $phoneResult[0]->phone_id;
						$upd = $this -> main_model -> update('phone_ads', array('status' => 1), array('phone_id' => $phoneId));	
						$this -> main_model -> incrementByValue('user', 'credit', 'userId=' . $userId, 200);
					}
				}
			}
			
			$user = $this -> main_model -> getData('user', '*', array('userId' => $userId));

			if ($user) {

				$this -> calc_points($arr_data);

				$update_profile_pic = 0;
				if (is_uploaded_file($image['tmp_name'])) {
					if (isset($image) && $image['error'] == 0) {
						require_once ('S3_config.php');
						if (!class_exists('S3'))
							require_once ('S3.php');
						$s3 = new S3($awsAccessKey, $awsSecretKey);

						$extension = end(explode('.', $image['name']));
						$updated_name = time() . '.' . $extension;
						if ($s3 -> putObjectFile($image['tmp_name'], $bucket, $uploads_dir_path.'/profile/' . $updated_name, S3::ACL_PUBLIC_READ)) {
							$imgname = $updated_name;
							$update_profile_pic = 1;
						} else {
							print_r(json_encode(array('status' => -2)));
							$this->db->close();
							exit ;
						}

					} else {
						print_r(json_encode(array('status' => -2)));
						$this->db->close();
						exit ;
					}
				} else if (!empty($profilePic)) {
					if ($user[0] -> profilePic != $profilePic) {
						$imgname = $profilePic;
						$update_profile_pic = 1;
					} else
					$imgname = $profilePic;

				}

				// check for country code

				if (!empty($countryCode) && empty($countryId) && empty($cityId)) {
					$additional_params['join'] = array('city', 'city.countryId=country.countryId');
					$country = $this -> main_model -> getBackendData('country', 'country.countryId,country.countryCode,city.cityId', array('countryCode' => ($countryCode)), '', '', 1, 0, $additional_params);
					if ($country) {

						$countryId = $country[0] -> countryId;
						$cityId = $country[0] -> cityId;

					}

				}

				if ($imgname == -1) {

					$upd = $this -> main_model -> update('user', array('fullName' => $fullName, 'countryId' => $countryId, 'cityId' => $cityId, 'phone' => $phone, 'phoneCode' => $phoneCode, 'dateOfBirth' => date('Y-m-d', $dateOfBirth), 'gender' => $gender, 'district' => $district, 'aboutMe' => $aboutMe, 'relationship' => $relationship, 'religion' => $religion, 'education' => $education, 'job' => $job, 'jobRole' => $jobRole, 'company' => $company, 'income' => $income, 'status' => $status, 'receiveFriendRequest' => $receiveFriendRequest, 'viewContactsInfo' => $viewContactsInfo, 'viewPersonalInfo' => $viewPersonalInfo, 'receivePushNotification' => $receivePushNotification, 'email' => $email, 'phone' => $phone), array('userId' => $userId));
				} else
				$upd = $this -> main_model -> update('user', array('fullName' => $fullName, 'profilePic' => $imgname, 'countryId' => $countryId, 'cityId' => $cityId, 'phone' => $phone, 'phoneCode' => $phoneCode, 'dateOfBirth' => date('Y-m-d', $dateOfBirth), 'gender' => $gender, 'district' => $district, 'aboutMe' => $aboutMe, 'relationship' => "$relationship", 'religion' => $religion, 'education' => $education, 'job' => $job, 'jobRole' => $jobRole, 'company' => $company, 'income' => $income, 'status' => $status, 'receiveFriendRequest' => "$receiveFriendRequest", 'viewContactsInfo' => $viewContactsInfo, 'viewPersonalInfo' => "$viewPersonalInfo", 'receivePushNotification' => "$receivePushNotification", 'email' => $email, 'phone' => $phone), array('userId' => $userId));

				// $upd_credit = $this->main_model->incrementByValue('user','credit','userId = '.$userId,$points);
				$this -> main_model -> insert('notification', array('senderId' => $userId, 'receiverId' => 0, 'postId' => 0, 'notificationType' => 5, 'notificationtime' => date("Y-m-d H:i:s")));

				if ($upd) {
					$where = "userId = " . $userId;
					$this -> getuser($where, '', '', $lang);
				} else
				print_r(json_encode(array('status' => -2)));

			} else
			print_r(json_encode(array('status' => -1)));
		} else {
			print_r(json_encode(array('status' => -1)));
		}
		$this->db->close();
	}

	public function checkUserFields($userId)
	{
		$flag = array();
		$result = $this->main_model->getData('user', '*', array("userId" => $userId));
		//$fields = $this->db->list_fields('user');
		
		if(!empty($result[0]->phone))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->dateOfBirth))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->gender))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->countryId))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->cityId))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->district))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->aboutMe))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->relationship))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->education))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->job))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->jobRole))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->company))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->income))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
		if(!empty($result[0]->status))
		{
			$flag[] = 'go';
		}else
		{
			$flag[] = 'stop';
		}
		
	
		//count($fields);
		/*for($i=0; $i<1; $i++)
		{
			if( !empty($result[0]->{$fields[$i]}) || $result[0]->{$fields[$i]} != "" )
			{
				
				$flag[] = 'go';
			} else 
			{
				
				$flag[] = 'stop';
			}
			
		}*/
		if(!in_array("stop", $flag))
		{
			//$points = $result[0]->credit + 20;
			//$this -> main_model -> incrementByValue('user', 'credit', 'userId=' . $userId, $points);
		} else 
		{

		}
	}

	public function getSuggestionsFans() {
		$json_data = $this -> input -> post('data');

		//$json_data ='{"keyword" : "mo","sortedBy":2 ,"favoritesFilter":0}';
		$arr_data = json_decode($json_data);

		$keyword = ($arr_data -> keyword);

		if (strlen($keyword) > 2) {

			$select = 'countryId , cityId , fullname as fanName , userId as fanID  , profilePic as fanPicture  ';

			if (!empty($keyword))
				$where = '  fullname like "' . $keyword . '%" ';

			$order = "noOfPics desc";

			$sql = "select " . $select . " from user where " . $where . "  order by " . $order . " limit 0,50";
			$query = $this -> db -> query($sql);
			$friends = $query -> result();

			$data = array();

			if ($friends) {
				foreach ($friends as $friend) {
					$friend -> fanID = intval($friend -> fanID);

					$friend -> fanPicture = $this -> userPic($friend -> fanPicture);

					unset($friend -> cityId);
					unset($friend -> countryId);
					$data[] = $friend;
				}
			}

			print_r(json_encode(array("status" => 2, "data" => array(" nextStartingLimit" => count($friends), "data" => $data))));

		} else {
			print_r(json_encode(array('status' => -1)));
		}
		$this->db->close();
	}

	public function verifyUserEmail() {
		$json_data = $this -> input -> post('data');

		//$json_data ='{"keyword" : "mo","sortedBy":2 ,"favoritesFilter":0}';
		$arr_data = json_decode($json_data);

		$userId = ($arr_data -> userId);
		$email = ($arr_data -> email);
		if (empty($email) || empty($userId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$check = $this -> main_model -> getData('user', 'userId,email', array('userId' => $userId, 'email' => $email));
			if ($check) {
				$this -> load -> helper('string');
				$code = random_string('alnum', 20);
				$user_code = $this -> main_model -> insert('verificationcode', array('code' => $code, 'userId' => $userId));
				if ($user_code) {
					$encrypted_string = $code;
					$this -> load -> library('email');

					$config = array();

					$config['protocol'] = "smtp";
					$config['smtp_host'] = "smtp.gmail.com";
					$config['smtp_port'] = "587";
					$config['smtp_crypto'] = 'tls';
					$config['smtp_timeout'] = '7';
					$config['smtp_user'] = 'no-reply@fcbstudio.info';
					$config['smtp_pass'] = "T@W@$0lIT";
					$config['mailtype'] = 'html';
					$config['charset'] = 'utf-8';
					$config['newline'] = "\r\n";
					$config['wordwrap'] = TRUE;
					$this -> email -> initialize($config);
					//$this->email->from('noreply@MoraselMobileApp.com', 'Morasel Mobile Application');
					$this -> email -> from('no-reply@fcbstudio.info', 'FCB Studio');
					$this -> email -> to($email);

					$this -> email -> subject('FCB Studio - Email Verification');
					$msg = '<html><body>
					Hey FCB Studio Fan,<br>
					Thanks for using FCB Studio, you are almost done.<br>
					<br>
					Complete the process by clicking the link below:
					http://barca.tawasoldev.com/barca/index.php/user/verifyemail/' . $encrypted_string . '


					<br><br>

					Regards,<br>
					FCB Team.<br></body></html>';
					$this -> email -> message($msg);

					$status = $this -> email -> send();
					//  echo $this->email->print_debugger();
					print_r(json_encode(array('status' => 1)));
				} else
				print_r(json_encode(array('status' => -2)));

			} else
			print_r(json_encode(array('status' => -1)));
		}
		$this->db->close();
	}

	public function verifyUserPhone() {
		$json_data = $this -> input -> post('data');

		//$json_data ='{"keyword" : "mo","sortedBy":2 ,"favoritesFilter":0}';
		$arr_data = json_decode($json_data);

		$userId = ($arr_data -> userId);
		$phone = ($arr_data -> phone);
		if (empty($phone) || empty($userId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$check = $this -> main_model -> getData('user', 'userId,phone', array('userId' => $userId, 'phone' => $phone));
			if ($check) {
				$this -> load -> helper('string');
				$code = random_string('alnum', 8);
				$user_code = $this -> main_model -> insert('verificationcode', array('code' => $code, 'userId' => $userId));
				if ($user_code) {
					$msg = "The Verification code is : " . $code;
					$id = $this -> send_sms($phone, $msg);
					if ($id == 1)

						print_r(json_encode(array('status' => 1)));
					else
						print_r(json_encode(array('status' => -2)));
				} else
				print_r(json_encode(array('status' => -2)));

			} else
			print_r(json_encode(array('status' => -1)));
		}
		$this->db->close();
	}

	public function sendSMSverificationCode() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = ($arr_data -> userId);
		$code = ($arr_data -> code);
		if (empty($code) || empty($userId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$user = $this -> main_model -> getData('verificationcode', '*', array('userId' => $userId, 'code' => $code));
			if ($user) {
				$upd = $this -> main_model -> update('user', array('phoneverified' => '1'), array('userId' => $userId));
				if ($upd) {
					$this -> send_notification($userId, 'phone verified', 3);
					print_r(json_encode(array('status' => 1)));
				} else
				print_r(json_encode(array('status' => -3, 'ruleName' => 'errorSendMessage', 'errorMessage' => 'Failure to send SMS message for this number')));

			} else
			print_r(json_encode(array('status' => -3, 'ruleName' => 'errorCode', 'errorMessage' => 'phone verification code not match')));

		}
		$this->db->close();
	}

	// private function

	private function getuser($where, $devicetype = '', $devicetoken = '', $lang) {
		$additionalParams['join'] = array('country', 'country.countryId = user.countryId', 'left');
		$additionalParams['join2'] = array('city', 'city.cityId = user.cityId', 'left');
		$user_login = $this -> main_model -> getBackendData('user', 'user.*,country.countryName as countryName,city.name_' . $lang . ' as cityName', $where, '', '', '', '', $additionalParams);
		if ($user_login) {
			$points = $this -> main_model -> getData('points', 'levelOneRegistration,levelTwoRegistration,levelThreeRegistration');

			if (!empty($devicetype) && !empty($devicetoken)) {
				if ($devicetoken != $user_login[0] -> pushnotificationtoken) {
					$this -> main_model -> update('user', array('pushnotificationtoken' => $devicetoken, 'devicetype' => $devicetype), array('userId' => $user_login[0] -> userId));
				}
			}
			// contact api for ejapard servser chat

			if (strpos($user_login[0] -> profilePic, 'http') !== FALSE)
				$user_login[0] -> profilePic = $user_login[0] -> profilePic;
			else {

				$user_login[0] -> profilePic = base_url() . 'third_party/uploads/profile/' . $user_login[0] -> profilePic;

			}
			if (empty($user_login[0] -> profilePic))
				$user_login[0] -> profilePic = '';

			//unset($user_login[0]->password);
			//unset($user_login[0]->isOnline);
			//unset($user_login[0]->lastLoginDate);

			if (empty($user_login[0] -> fullName))
				$user_login[0] -> fullName = '';

			$user_login[0] -> password = '';
			if (empty($user_login[0] -> dateOfBirth))
				$user_login[0] -> dateOfBirth = '';
			if (empty($user_login[0] -> gender))
				$user_login[0] -> gender = 0;
			if (empty($user_login[0] -> countryId))
				$user_login[0] -> countryId = 0;
			if (empty($user_login[0] -> lastLoginDate))
				$user_login[0] -> lastLoginDate = '';
			if (empty($user_login[0] -> noOfPics))
				$user_login[0] -> noOfPics = 0;
			if (empty($user_login[0] -> userType))
				$user_login[0] -> userType = 0;

			$user_login[0] -> registrationLevelOnePoints = intval($points[0] -> levelOneRegistration);
			$user_login[0] -> registrationLevelTwoPoints = intval($points[0] -> levelTwoRegistration);
			$user_login[0] -> registrationLevelThreePoints = intval($points[0] -> levelThreeRegistration);

			// $user_login[0]->phoneverified=intval ($user_login[0]->phoneverified);
			//$user_login[0]->emailvreified=intval ($user_login[0]->emailvreified);

			unset($user_login[0] -> phoneverified);
			unset($user_login[0] -> emailvreified);
			$user_login[0] -> phoneVerified = 1;
			$user_login[0] -> emailVerified = 1;

			if (empty($user_login[0] -> brief))
				$user_login[0] -> brief = "";

			if (empty($user_login[0] -> cityId))
				$user_login[0] -> cityId = "";

			if (empty($user_login[0] -> socialMediaId))
				$user_login[0] -> socialMediaId = '';
			if (empty($user_login[0] -> email))
				$user_login[0] -> email = '';

			if (empty($user_login[0] -> district))
				$user_login[0] -> district = '';

			if (empty($user_login[0] -> phone))
				$user_login[0] -> phone = '';

			if (empty($user_login[0] -> countryName))
				$user_login[0] -> countryName = '';

			if (empty($user_login[0] -> countryName))
				$user_login[0] -> countryName = '';

			if (empty($user_login[0] -> cityName))
				$user_login[0] -> cityName = '';

			$user_login[0] -> userType = intval($user_login[0] -> userType);
			$user_login[0] -> userId = intval($user_login[0] -> userId);
			$user_login[0] -> noOfPics = intval($user_login[0] -> noOfPics);
			$user_login[0] -> countryId = intval($user_login[0] -> countryId);
			$user_login[0] -> cityId = intval($user_login[0] -> cityId);
			$user_login[0] -> credit = intval($user_login[0] -> credit);
			$user_login[0] -> isPremium = intval($user_login[0] -> isPremium);
			$user_login[0] -> gender = intval($user_login[0] -> gender);
			$user_login[0] -> isOnline = intval($user_login[0] -> isOnline);
			$user_login[0] -> socialMediaId = ($user_login[0] -> socialMediaId);
			$user_login[0] -> userType = intval($user_login[0] -> userType);
			$user_login[0] -> phoneverified = intval($user_login[0] -> phoneverified);
			$user_login[0] -> emailvreified = intval($user_login[0] -> emailvreified);
			$user_login[0] -> emailvreified = intval($user_login[0] -> emailvreified);

			$user_login[0] -> devicetype = intval($user_login[0] -> devicetype);
			$user_login[0] -> relationship = intval($user_login[0] -> relationship);
			$user_login[0] -> income = intval($user_login[0] -> income);
			$user_login[0] -> receiveFriendRequest = intval($user_login[0] -> receiveFriendRequest);
			$user_login[0] -> viewPersonalInfo = intval($user_login[0] -> viewPersonalInfo);
			$user_login[0] -> viewContactsInfo = intval($user_login[0] -> viewContactsInfo);
			$user_login[0] -> receivePushNotification = intval($user_login[0] -> receivePushNotification);

			$user_login[0] -> registrationDate = strtotime($user_login[0] -> registrationDate);
			$user_login[0] -> lastLoginDate = strtotime($user_login[0] -> lastLoginDate);
			$user_login[0] -> dateOfBirth = strtotime($user_login[0] -> dateOfBirth);

			if (!$user_login[0] -> dateOfBirth)

				$user_login[0] -> dateOfBirth = 0;

			if (!$user_login[0] -> lastLoginDate)

				$user_login[0] -> lastLoginDate = 0;
			//$countryName = $user_login[0]->countryName;
			unset($user_login[0] -> SMSInviteText);

			$user_login[0] -> followingCount = $this -> main_model -> count('userfans', 'userId = ' . $user_login[0] -> userId . ' AND isFollow = 1');
			$user_login[0] -> followersCount = $this -> main_model -> count('userfans', 'fanId = ' . $user_login[0] -> userId . ' AND isFollow = 1');
			$user_login[0] -> allowLocation = 1;
			$update = $this -> main_model -> update('user', array('lastLoginDate' => date('Y-m-d H:i:s'), 'isOnline' => '1'), array('userId' => $user_login[0] -> userId));
			if ($update)
				//print_r(json_encode(array('status'=>2,'data'=>array("user"=>$user_login[0],"allowLocation"=>1,'countryName'=>$countryName,'followingCount'=>$followingCount,"followersCount"=>$followersCount))));
				print_r(json_encode(array('status' => 2, 'data' => $user_login[0])));
			else
				print_r(json_encode(array('status' => -1, 'data' => $user_login[0])));

		} else
		print_r(json_encode(array('status' => -3, 'ruleName' => 'loginFailed', 'errorMessage' => 'User name or Password is wrong')));

	}

	private function check_friend($userID, $fanID) {
		$where = '(userId = ' . $userID . ' AND fanId = ' . $fanID . ' AND isFriend = 1 ) OR (userId = ' . $fanID . ' AND fanId = ' . $userID . ' AND isFriend = 1 )';
		$is_friends = $this -> main_model -> getData('userfans', '*', $where);
		if ($is_friends)
			return TRUE;
		else
			return FALSE;
	}

	private function getUserFriends($userID = 0) {
		$sql = "(select fanId  from userfans where userId =" . $userID . " AND isFriend = 1 ) union (select userId from userfans where fanId = " . $userID . " AND isFriend = 1) ";

		$query = $this -> db -> query($sql);
		$res = $query -> result_array();
		if (count($res) > 0) {
			foreach ($res as $friend) {
				$ids[] = $friend['fanId'];

			}
			return $ids;
		}
		return FALSE;
	}

	private function send_notification($userID, $msg, $type) {
		$where = 'userId = ' . $userID;
		$user = $this -> main_model -> getData('user', 'userId, pushnotificationtoken , devicetype', $where);
		if ($user) {
			$device_type = $user[0] -> devicetype;
			$device_token = $user[0] -> pushnotificationtoken;
			$this -> load -> library('notification_lib');
			if ($device_type == 2) {
				$this -> notification_lib -> push_iphone($device_token, $msg, $type, "", "", "", "", "", $userID);
			} else if ($device_type == 1) {
				$this -> notification_lib -> push_android($device_token, $msg, $type, "", "", "", "", "", $userID);
			}

		}
	}

	private function userPic($url) {
		if (strpos($url, 'http') !== FALSE)
			return $url;
		else {
			if (empty($url))
				return '';
			else
				return base_url() . 'third_party/uploads/profile/' . $url;
		}

	}

	public function test_push($token) {
		$this -> load -> library('notification_lib');
		$this -> notification_lib -> push_android($token, "hi moarbe3 how are you");
	}

	private function calc_points($arr) {

		$userId = $arr -> userId;
		$district = $arr -> district;
		// echo $userId ;
		$level_points = $this -> main_model -> getData('points', 'levelOneRegistration,levelTwoRegistration,levelThreeRegistration');
		$level_one = $level_points[0] -> levelOneRegistration;
		$level_two = $level_points[0] -> levelTwoRegistration;
		$level_three = $level_points[0] -> levelThreeRegistration;
		$user = $this -> main_model -> getData('user', '*', array('userId' => $userId));
		$keywords = explode(',', $user[0] -> keywords);
		
		

		if ($user) {
			$points = 0;

			if (empty($user[0] -> countryId) && !empty($arr -> countryId) && !in_array('countryId', $keywords)) {
				$points += $level_two;
				$data_keywords[] = "countryId";

			}

			if (empty($user[0] -> cityId) && !empty($arr -> cityId) && !in_array('cityId', $keywords)) {
				$points += $level_two;
				$data_keywords[] = "cityId";
			}

			if (!empty($arr -> dateOfBirth) && !in_array('dateOfBirth', $keywords)) {

				$points += $level_two;
				$data_keywords[] = "dateOfBirth";
			}

			if (empty($user[0] -> district) && !empty($district) && !in_array('district', $keywords)) {
				$points += $level_two;
				$data_keywords[] = "district";

			}

			if (empty($user[0] -> phone) && !empty($arr -> phone) && !in_array('phone', $keywords)) {
				$points += $level_two;
				$data_keywords[] = "phone";
			}

			if (empty($user[0] -> gender) && !empty($arr -> gender) && !in_array('gender', $keywords)) {
				$points += $level_two;
				$data_keywords[] = "gender";
			}

			if (!empty($arr -> status) && !in_array('status', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "status";
			}

			if (empty($user[0] -> aboutMe) && !empty($arr -> aboutMe) && !in_array('aboutMe', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "aboutMe";
			}

			if (empty($user[0] -> relationship) && !empty($arr -> relationship) && !in_array('relationship', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "relationship";
			}

			if (empty($user[0] -> religion) && !empty($arr -> religion) && !in_array('religion', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "religion";
			}

			if (empty($user[0] -> education) && !empty($arr -> education) && !in_array('education', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "education";
			}

			if (empty($user[0] -> job) && !empty($arr -> job) && !in_array('job', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "job";
			}
			if (empty($user[0] -> jobRole) && !empty($arr -> jobRole) && !in_array('jobRole', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "jobRole";
			}

			if (empty($user[0] -> company) && !empty($arr -> company) && !in_array('company', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "company";
			}

			if (empty($user[0] -> income) && !empty($arr -> income) && !in_array('income', $keywords)) {
				$points += $level_three;
				$data_keywords[] = "income";
			}
			if (!empty($data_keywords)) {
				$fianlarr = array_merge($data_keywords, $keywords);

			} else
			$fianlarr = $keywords;
			
			$profileNo = count($keywords);
			
			$keywordstr = implode(',', $fianlarr);
			$final_point = $points + $user[0] -> credit;
			
			if($profileNo == 17)
			{
				$final_point = $final_point + 20;
			}
			//  echo $final_point . " k = ".$keywordstr. "  uid =".$userId;
			$this -> main_model -> update('user', array('credit' => $final_point, 'keywords' => $keywordstr), array('userId' => $userId));
			
			$arr = array(
				'TransTypeID' => 'AP_05',
				'UserID' => $userId,
				'TransDate' => date('Y-m-d'),
				'TransTime' => date('H:i:s'),
				'points' => ($final_point - $user[0] -> credit)
			);
			$this -> main_model -> insert('TransactionLog',$arr);
		}
	}

	function send_sms($phone, $msg) {
		$url = 'http://api.clickatell.com/http/sendmsg?user=FCB_Tawasol&password=KBZKLMZFLBXILQ&api_id=3518569&to=' . $phone . '&text=' . $msg;
		$id = file_get_contents($url);
		return $id;
	}

}
