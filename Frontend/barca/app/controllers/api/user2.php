<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		
	}

	public function sendInviteSMS() {
		$json_data = $this -> input -> post('data');

		//$json_data = '{"email":"test@tawasolit.com","password":"osama123","fullName":"ehab","profilePicURL":"","phone":"","country":"egy","city":"ss","district":"","birthday":"2014-8-9","gender":"0"}';

		$arr_data = json_decode($json_data);
		$userId = $arr_data -> userId;
		$phoneNumbers = $arr_data -> phoneNumbers;
		$invitationText = $arr_data -> invitationText;

		if (empty($userId) || empty($invitationText))
			print_r(json_encode(array('status' => -1)));
		else {
			$updated = $this -> main_model -> update('user', array('SMSInviteText' => $invitationText), array('userId' => $userId));
			if ($updated)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -1)));
		}
		$this->db->close();
	}

	public function getUserResgistrationLevel() {
		$json_data = $this -> input -> post('data');

		//$json_data = '{"email":"test@tawasolit.com","password":"osama123","fullName":"ehab","profilePicURL":"","phone":"","country":"egy","city":"ss","district":"","birthday":"2014-8-9","gender":"0"}';

		$arr_data = json_decode($json_data);
		$userId = $arr_data -> userId;

		if (empty($userId))
			print_r(json_encode(array('status' => -1)));
		else {
			$user = $this -> main_model -> getData('user', '*', array('userId' => $userId));
			if ($user) {
				if ($user[0] -> emailverified == 0 || empty($user[0] -> email) || empty($user[0] -> phone) || empty($user[0] -> countryId) || empty($user[0] -> cityId) || empty($user[0] -> district) || empty($user[0] -> gender) || empty($user[0] -> dateOfBirth)) {
					print_r(json_encode(array('status' => 1)));
				} else
				print_r(json_encode(array('status' => 2)));
			} else {
				print_r(json_encode(array('status' => -2)));
			}
		}
		$this->db->close();
	}

	public function login() {

		//file_put_contents('test50.txt', serialize($_POST));
		$json_data = $this -> input -> post('data');

		//$json_data = '{"email":"test@tawasolit.com","password":"osama123","fullName":"ehab","profilePicURL":"","phone":"","country":"egy","city":"ss","district":"","birthday":"2014-8-9","gender":"0"}';

		$arr_data = json_decode($json_data);
		$email = $arr_data -> email;
		$password = $arr_data -> password;
		$deviceToken = $arr_data -> deviceToken;
		$deviceType = $arr_data -> deviceType;
		$lang = $arr_data -> lang;
		if (empty($email) || empty($password))
			print_r(json_encode(array('status' => -1)));
		else {
			$where = array('email' => $email, 'password' => md5($password));

			$nowDate = date('Y-m-d');
			$result = $this->main_model->getData('user','userId,pushnotificationtoken, deviceType, last_points_update, credit', $where);
			$result[0]->last_points_update;
			$userPoints = $result[0]->credit;
			
			if($result)
			{
				$this -> main_model -> update('user', array('pushnotificationtoken' => ''), array('pushnotificationtoken' => $deviceToken));

				if($result[0]->last_points_update == $nowDate)
				{
					$isActive = $this -> main_model -> update('user', array('isOnline' => 1), $where);
					
				} else 
				{
					$points = $this->main_model->getData('points','dialyPointsDay1');
					$newPoints = $userPoints + $points[0]->dialyPointsDay1;
					$msg = "Welcome Back ".$points[0]->dialyPointsDay1."xp added";
					$isActive = $this -> main_model -> update('user', array('isOnline' => 1, 'credit' => $newPoints, 'last_points_update' => $nowDate), $where);
					if($isActive)
					{
						$arr = array(
							'TransTypeID' => 'AP_01',
							'UserID' => $result[0]->userId,
							'DeviceType' => $deviceType,
							'DeviceID' => $deviceToken,
							'TransDate' => date('Y-m-d'),
							'TransTime' => date('H:i:s'),
							'points' => $points[0]->dialyPointsDay1
						);
						$this -> main_model -> insert('TransactionLog',$arr);
						
						$this->load->library('notification_lib');
						if ($deviceType == 2) {
							$this->notification_lib->push_iphone($deviceToken, $msg, "9", "", "", "", "", "", "");
						} 
						else if ($deviceType == 1) {
							$this->notification_lib->push_android($deviceToken, $msg, "9", "", "", "", "", "", "");
						}
					//print_r(json_encode(array('status' => 2, 'credit' => $newPoints)));
					} else {
					//print_r(json_encode(array('status' => -2)));
					}
				}
			}

			$this -> getuser($where, $deviceType, $deviceToken, $lang);
		}
		$this->db->close();
	}

	// DB change profilepic to text . change email to be null .change socialMediaID to socialMediaId . points to DB
	/*
	 public function loginFacebook()
	 {
	 $this->login_social('facebook',2);

	 }
	 */
	 public function loginSocialMedia() {
	 	$json_data = $this -> input -> post('data');

		//  $json_data = '{"socialMediaId":"654646955554","email":"","fullName":"ehab","profilePicURL":"","phone":"","country":"egy","city":"ss","district":"","birthday":"2014-8-9","gender":"0"}';
	 	$arr_data = json_decode($json_data);

	 	$socialMediaId = $arr_data -> socialMediaId;
	 	$email = $arr_data -> email;
	 	$fullName = $arr_data -> fullName;
	 	$profilePicURL = $arr_data -> profilePicURL;
	 	$phone = $arr_data -> phone;
	 	$country = $arr_data -> country;
	 	$city = $arr_data -> city;
	 	$district = $arr_data -> district;
	 	$birthday = $arr_data -> birthday;
	 	$gender = $arr_data -> gender;
	 	$lang = $arr_data -> lang;
	 	$deviceToken = $arr_data -> deviceToken;
	 	$deviceType = $arr_data -> deviceType;

	 	if (empty($socialMediaId) || empty($fullName))
	 		print_r(json_encode(array('status' => -1)));
	 	else {
	 		$user_login = $this -> main_model -> getData('user', 'userId , credit , last_points_update', array('socialMediaId' => $socialMediaId));
	 		if ($user_login) {
	 			$where = array('socialMediaId' => $socialMediaId);
				$userPoints = $user_login[0]->credit;
				
				$nowDate = date('Y-m-d');
				if($user_login[0]->last_points_update == $nowDate)
				{
					$isActive = $this -> main_model -> update('user', array('isOnline' => 1), $where);
					
				} 
				else 
				{
					$points = $this->main_model->getData('points','dialyPointsDay1');

					$newPoints = $userPoints + $points[0]->dialyPointsDay1;
					$msg = "Welcome Back ".$points[0]->dialyPointsDay1."xp added";
					$isActive = $this -> main_model -> update('user', array('isOnline' => 1, 'credit' => $newPoints, 'last_points_update' => $nowDate), $where);
					if($isActive)
					{
						$this->load->library('notification_lib');
						if ($deviceType == 2) {
							$this->notification_lib->push_iphone($deviceToken, $msg, "9", "", "", "", "", "", "");
						} 
						else if ($deviceType == 1) {
							$this->notification_lib->push_android($deviceToken, $msg, "9", "", "", "", "", "", "");
						}
					//print_r(json_encode(array('status' => 2, 'credit' => $newPoints)));
					} else {
					//print_r(json_encode(array('status' => -2)));
					}
				}
	 			$this -> getuser($where, $deviceType, $deviceToken, $lang);
	 		} else {
	 			if (empty($email))
	 				print_r(json_encode(array('status' => 1)));
	 			else {
	 				if (!empty($country)) {
	 					$where_country = 'countryName like "%' . trim($country) . '%" ';
	 					$getCountryId = $this -> main_model -> getData('country', '*', $where_country);
	 					if ($getCountryId)
	 						$countryId = $getCountryId[0] -> countryId;
	 					else
	 						$countryId = 0;
	 				} else
	 				$countryId = 0;

					///////////city
	 				if (!empty($city)) {
	 					$where_city = 'name_en like "%' . trim($city) . '%" ';
	 					$getCityId = $this -> main_model -> getData('city', '*', $where_city);
	 					if ($getCityId)
	 						$cityId = $getCityId[0] -> cityId;
	 					else
	 						$cityId = 0;
	 				} else {
	 					$cityId = 0;
	 				}

					/// //////// profile Pic
	 				if (empty($profilePicURL)) {
	 					$getrandomPhoto = $this -> main_model -> getData('thesuarus', '*', array('type' => 1));
	 					if ($getrandomPhoto)
	 						$profilePicURL = $getrandomPhoto[0] -> photoUrl;
	 					else
	 						$profilePicURL = '';
	 				}

	 				$insert = $this -> main_model -> insert('user', array('fullName' => $fullName, 'dateOfBirth' => date("Y-m-d H:i:s", $birthday), 'email' => $email, 'socialMediaId' => $socialMediaId, 'profilePic' => $profilePicURL, 'userType' => 2, 'phone' => $phone, 'gender' => $gender, 'district' => $district, 'countryId' => $countryId, 'cityId' => $cityId, "status" => "I'm using FCB Studio"));
	 				if ($insert) {

						// increase points for first level
	 					$pointArr = $this -> main_model -> getData('points', 'levelOneRegistration');
	 					$points = $pointArr[0] -> levelOneRegistration;
	 					$this -> main_model -> incrementByValue('user', 'credit', 'userId=' . $insert, $points);

						// end of points
	 					$where = array('userId' => $insert);
	 					$this -> getuser($where, $deviceType, $deviceToken, $lang);
	 				} else
	 				print_r(json_encode(array('status' => -2)));
	 			}
	 		}
	 	}
	 	$this->db->close();
	 }

	/*
	 public function loginInstagram()
	 {
	 $this->login_social('instagram',3);
	 }

	 public function loginTwitter()
	 {
	 $this->login_social('twitter',4);

	 }

	 public function loginGooglePlus()
	 {
	 $this->login_social('GooglePlus',5);

	 }
	 */

	 
	 public function sendVerificationCode() {
	 	$this -> load -> helper('string');
	 	$json_data = $this -> input -> post('data');
	 	$arr_data = json_decode($json_data);
	 	$email = $arr_data -> email;
		//$email = 'ehab.adel3@gmail.com';
	 	if (!empty($email)) {
	 		$check_email = $this -> main_model -> getData('user', 'userId,email', array('email' => $email));
	 		if ($check_email) {
	 			$code = random_string('alnum', 12);
	 			$user_code = $this -> main_model -> insert('user_codes', array('code' => $code, 'userId' => $check_email[0] -> userId));
	 			if ($user_code) {
	 				$encrypted_string = $code;
	 				$this -> load -> library('email');

	 				$config = array();

	 				$config['protocol'] = "smtp";
	 				$config['smtp_host'] = "smtp.gmail.com";
	 				$config['smtp_port'] = "587";
	 				$config['smtp_crypto'] = 'tls';
	 				$config['smtp_timeout'] = '7';
	 				$config['smtp_user'] = 'no-reply@fcbstudio.info';
	 				$config['smtp_pass'] = "T@W@$0lIT";
	 				$config['mailtype'] = 'html';
	 				$config['charset'] = 'utf-8';
	 				$config['newline'] = "\r\n";
	 				$config['wordwrap'] = TRUE;
	 				$this -> email -> initialize($config);
					//$this->email->from('noreply@MoraselMobileApp.com', 'Morasel Mobile Application');
	 				$this -> email -> from('no-reply@fcbstudio.info', 'FCB Studio');
	 				$this -> email -> to($email);

	 				$this -> email -> subject('FCB Studio - Reset password');
	 				$msg = '<html><body>
	 				Hey FCB Studio Fan,<br>
	 				Thanks for using FCB Studio, you are almost done.<br>
	 				<br>
	 				Your verification code is :<br>
	 				' . $encrypted_string . '

	 				<br><br>

	 				Regards,<br>
	 				FCB Team.<br></body></html>';
	 				$this -> email -> message($msg);

	 				$this -> email -> send();

	 				print_r(json_encode(array('status' => 2, 'data' => array('userId' => $check_email[0] -> userId))));
	 			} else
	 			print_r(json_encode(array('status' => -2)));
	 		} else
	 		print_r(json_encode(array('status' => -3, 'ruleName' => 'emailNotExist', 'errorMessage' => 'email Not Exist')));

	 	} else
	 	print_r(json_encode(array('status' => -1)));
	 	$this->db->close();
	 }

	 public function changePassword() {
	 	$json_data = $this -> input -> post('data');
	 	$arr_data = json_decode($json_data);
	 	$code = $arr_data -> verificationCode;
	 	$password = $arr_data -> newPassword;
	 	if (empty($code) || empty($password)) {
	 		print_r(json_encode(array('status' => -1)));
	 	} else {
	 		$get_code = $this -> main_model -> getData('user_codes', '*', array('code' => $code));
	 		if ($get_code) {
	 			$update_password = $this -> main_model -> update('user', array('password' => md5($password)), array('userId' => $get_code[0] -> userId));
	 			if ($update_password)
	 				print_r(json_encode(array('status' => 1)));
	 			else
	 				print_r(json_encode(array('status' => -2)));
	 		} else
	 		print_r(json_encode(array('status' => -3, 'ruleName' => 'verificationCodeNotMatch', 'errorMessage' => 'The verification code is not correct')));
	 	}
	 	$this->db->close();
	 }

	 public function registerUser() {

		//file_put_contents('test32.txt', serialize($_POST));
	 	$json_data = $this -> input -> post('data');

	 	$arr_data = json_decode($json_data);
	 	$fullName = $arr_data -> fullname;
	 	$password = $arr_data -> password;
	 	$email = $arr_data -> email;
	 	$profilePic = $arr_data -> photoURL;
	 	$deviceToken = $arr_data -> deviceToken;
	 	$deviceType = $arr_data -> deviceType;
	 	$lang = $arr_data -> lang;
	 	if (empty($fullName) || empty($password) || empty($email))
	 		print_r(json_encode(array('status' => -1)));
	 	else {
	 		if (empty($profilePic)) {
	 			$check_user = $this -> main_model -> getData('user', '*', array('email' => $email));
	 			if ($check_user)
	 				print_r(json_encode(array('status' => -3, 'ruleName' => 'UserExists', 'errorMessage' => 'This email already used before, try login or forgot password')));
	 			else {
	 				if (is_uploaded_file($_FILES['photo']['tmp_name'])) {

	 					$image = $_FILES['photo'];
	 					if (isset($image) && $image['error'] == 0) {
	 						require_once ('S3_config.php');
	 						if (!class_exists('S3'))
	 							require_once ('S3.php');
	 						$s3 = new S3($awsAccessKey, $awsSecretKey);

	 						$extension = end(explode('.', $image['name']));
	 						$updated_name = time() . '.' . $extension;

	 						if ($s3 -> putObjectFile($_FILES['photo']['tmp_name'], $bucket, $uploads_dir_path.'/profile/' . $updated_name, S3::ACL_PUBLIC_READ)) {

	 							$user = $this -> main_model -> insert('user', array('fullName' => $fullName, 'password' => md5($password), 'email' => $email, 'userType' => '1', 'profilePic' => $updated_name, 'status' => "I'm using FCB Studio"));
	 							if ($user) {

	 								$pointArr = $this -> main_model -> getData('points', 'levelOneRegistration,levelTwoRegistration');

	 								$points = $pointArr[0] -> levelOneRegistration + $pointArr[0] -> levelTwoRegistration;
	 								$this -> main_model -> incrementByValue('user', 'credit', 'userId=' . $user, $points);
									
									$arr = array(
										'TransTypeID' => 'AP_02',
										'UserID' => $user,
										'DeviceType' => $deviceType,
										'DeviceID' => $deviceToken,
										'TransDate' => date('Y-m-d'),
										'TransTime' => date('H:i:s'),
										'points' => $points
									);
									$this -> main_model -> insert('TransactionLog',$arr);
									
	 								$where_user = array("userId" => $user);

	 								$result = $this->verifyUserEmail($user, $email);

	 								$this -> getuser($where_user, $deviceType, $deviceToken, $lang);
	 							} else
	 							print_r(json_encode(array('status' => -2)));
	 						} else {
	 							print_r(json_encode(array('status' => -2)));
	 						}
	 					} else {
	 						print_r(json_encode(array('status' => -2)));
	 					}
	 				} else {

						//print_r(json_encode(array('status'=>-1)));

	 				}
	 			}
	 		} else {
	 			$check_user = $this -> main_model -> getData('user', '*', array('email' => $email));
	 			if ($check_user)
	 				print_r(json_encode(array('status' => -3, 'ruleName' => 'UserExists', 'errorMessage' => 'This email aleady used before, try login or forgot password')));
	 			else {

	 				$user = $this -> main_model -> insert('user', array('fullName' => $fullName, 'password' => md5($password), 'email' => $email, 'userType' => '1', 'profilePic' => $profilePic, 'status' => "I'm using FCB Studio"));
	 				if ($user) {
	 					$pointArr = $this -> main_model -> getData('points', 'levelOneRegistration');

	 					$points = $pointArr[0] -> levelOneRegistration;
	 					$this -> main_model -> incrementByValue('user', 'credit', 'userId=' . $user, $points);
						
						$arr = array(
							'TransTypeID' => 'AP_02',
							'UserID' => $user,
							'DeviceType' => $deviceType,
							'DeviceID' => $deviceToken,
							'TransDate' => date('Y-m-d'),
							'TransTime' => date('H:i:s'),
							'points' => $points
						);
						$this -> main_model -> insert('TransactionLog',$arr);
						
	 					$where_user = array("userId" => $user);
	 					$result = $this->verifyUserEmail($user, $email);
	 					$this -> getuser($where_user, $deviceType, $deviceToken, $lang);
	 				}

					// print_r(json_encode(array('status' => 2,'data'=>array("userId"=>$user))));
	 				else
	 					print_r(json_encode(array('status' => -2)));
	 			}
	 		}
	 	}
	 	$this->db->close();
	 }


	 public function verifyUserEmail($userId, $email) {
		//$json_data = $this -> input -> post('data');

		//$json_data ='{"keyword" : "mo","sortedBy":2 ,"favoritesFilter":0}';
		//$arr_data = json_decode($json_data);

	 	$userId = $userId;
	 	$email = $email;
	 	if (empty($email) || empty($userId)) {
	 		print_r(json_encode(array('status' => -1)));
	 	} else {
	 		$check = $this -> main_model -> getData('user', 'userId,email', array('userId' => $userId, 'email' => $email));
	 		if ($check) {
	 			$this -> load -> helper('string');
	 			$code = random_string('alnum', 20);
	 			$user_code = $this -> main_model -> insert('verificationcode', array('code' => $code, 'userId' => $userId));
	 			if ($user_code) {
	 				$encrypted_string = $code;
	 				$this -> load -> library('email');

	 				$config = array();

	 				$config['protocol'] = "smtp";
	 				$config['smtp_host'] = "smtp.gmail.com";
	 				$config['smtp_port'] = "587";
	 				$config['smtp_crypto'] = 'tls';
	 				$config['smtp_timeout'] = '7';
	 				$config['smtp_user'] = 'no-reply@fcbstudio.info';
	 				$config['smtp_pass'] = "T@W@$0lIT";
	 				$config['mailtype'] = 'html';
	 				$config['charset'] = 'utf-8';
	 				$config['newline'] = "\r\n";
	 				$config['wordwrap'] = TRUE;
	 				$this -> email -> initialize($config);
					//$this->email->from('noreply@MoraselMobileApp.com', 'Morasel Mobile Application');
	 				$this -> email -> from('no-reply@fcbstudio.info', 'FCB Studio');
	 				$this -> email -> to($email);

	 				$this -> email -> subject('FCB Studio - Email Verification');
	 				$msg = '<html><body>
	 				Hey FCB Studio Fan,<br>
	 				Thanks for using FCB Studio, you are almost done.<br>
	 				<br>
	 				Complete the process by clicking the link below:
	 				http://barca.tawasoldev.com/barca/index.php/user/verifyemail/' . $encrypted_string . '


	 				<br><br>

	 				Regards,<br>
	 				FCB Team.<br></body></html>';
	 				$this -> email -> message($msg);

	 				$status = $this -> email -> send();
					//  echo $this->email->print_debugger();
					//print_r(json_encode(array('status' => 1)));
	 			} else {
					//print_r(json_encode(array('status' => -2)));
	 			}

	 		} else
	 		print_r(json_encode(array('status' => -1)));
	 	}
	 	$this->db->close();
	 }

	 public function getFCBPhotos() {
	 	$photos = $this -> main_model -> getData('thesuarus', 'thesuarusId as id , photoUrl as url', array('type' => '1', 'is_hidden' => '0'), '', '', 50, 0);
	 	if ($photos) {
	 		foreach ($photos as $photo) {
	 			$photo -> url = base_url() . 'third_party/uploads/' . $photo -> url;
	 			$photo -> photoName = $photo -> url;
	 			$data[] = $photo;
	 		}
	 		print_r(json_encode(array('status' => 2, 'data' => $data)));
	 	} else {
	 		print_r(json_encode(array('status' => 2, 'data' => array())));
	 	}
	 	$this->db->close();
	 }

	 public function getCountryCities() {

		//file_put_contents('test.txt', serialize($_POST));
	 	$json_data = $this -> input -> post('data');

		//$json_data = ' {  "postId" : 0,  "postPhotoCaption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0 , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  } , {    "tagId" : "0",    "tagName" : "tag 2"  }] } ';
	 	$arr_data = json_decode($json_data);

	 	$countryId = $arr_data -> countryId;

	 	$lang = $arr_data -> lang;
	 	if (empty($lang) || empty($countryId))
	 		print_r(json_encode(array('status' => -1)));
	 	else {
	 		$select = 'cityId , name_' . $lang . ' as cityName';
	 		$cities = $this -> main_model -> getData('city', $select, array('countryId' => $countryId) , 'name_en', 'ASC');
	 		if ($cities) {
	 			print_r(json_encode(array('status' => 2, "data" => $cities)));
	 		} else
	 		print_r(json_encode(array('status' => 2, "data" => array())));
	 	}
	 	$this->db->close();
	 }

	 public function getUserByID() {
	 	$json_data = $this -> input -> post('data');

		//  $json_data = ' {  "lang" : "en",  "postPhotoCaption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0 , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  } , {    "tagId" : "0",    "tagName" : "tag 2"  }] } ';
	 	$arr_data = json_decode($json_data);

	 	$userId = $arr_data -> userId;
	 	$lang = $arr_data -> lang;

	 	if (empty($userId))
	 		print_r(json_encode(array('status' => -1)));
	 	else {
	 		$where = "user.userId = " . $userId;
	 		$this -> getuser($where, '', '', $lang);
	 	}
	 	$this->db->close();
	 }

	 public function getCountryList() {

		//file_put_contents('test.txt', serialize($_POST));
	 	$json_data = $this -> input -> post('data');

		//  $json_data = ' {  "lang" : "en",  "postPhotoCaption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0 , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  } , {    "tagId" : "0",    "tagName" : "tag 2"  }] } ';
	 	$arr_data = json_decode($json_data);

	 	$lang = $arr_data -> lang;

	 	if (empty($lang))
	 		print_r(json_encode(array('status' => -1)));
	 	else {
	 		if ($lang == 'en')
	 			$select = 'countryId, phone_code, countryName as countryName';
	 		else
	 			$select = 'countryId , phone_code, name_' . $lang . ' as countryName';
	 		$countries = $this -> main_model -> getData('country', $select, '', 'countryName', 'ASC');
	 		if ($countries) {
	 			print_r(json_encode(array('status' => 2, "data" => $countries)));
	 		} else
	 		print_r(json_encode(array('status' => 2, "data" => array())));
	 	}
	 	$this->db->close();
	 }

	/*
	 public function uploadUserPhoto()
	 {
	 //file_put_contents('test.txt', serialize($_POST));
	 $json_data = $this->input->post('data');

	 $arr_data = json_decode($json_data);
	 $userId = $arr_data->userId;

	 if(empty($userId))
	 print_r(json_encode(array('status'=>-1)));
	 else
	 {
	 if(is_uploaded_file($_FILES['photo']['tmp_name']) )
	 {
	 $image = $_FILES['photo'];
	 if(isset($image) && $image['error'] == 0)
	 {
	 if (!class_exists('S3'))require_once('S3.php');
	 if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
	 if (!defined('awsSecretKey')) define('awsSecretKey', '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');
	 $s3 = new S3(awsAccessKey, awsSecretKey);
	 $extension = end(explode('.', $image['name']));
	 $updated_name = time().'.'.$extension;

	 if ($s3->putObjectFile($_FILES['photo']['tmp_name'], "barca", 'third_party/uploads/profile/'.$updated_name, S3::ACL_PUBLIC_READ))

	 {
	 $user = $this->main_model->update('user',array('profilePic'=>$updated_name),array('userId'=>$userId));
	 if($user)
	 print_r(json_encode(array('status' => 1)));
	 else
	 print_r(json_encode(array('status' => -2)));
	 }
	 else
	 {
	 print_r(json_encode(array('status' => -2)));

	 }

	 }
	 else {
	 print_r(json_encode(array('status'=>-2)));
	 }
	 }
	 else {
	 print_r(json_encode(array('status'=>-1)));
	 }

	 }

	 }
	 */

	/*
	 private function login_social($type,$userType)
	 {
	 $json_data = $this->input->post('data');
	 $arr_data = json_decode($json_data);

	 $socialMediaId = $arr_data->socialMediaId;
	 if($type =='instgram')
	 {
	 $fullName = $arr_data->userName;
	 $email = '';
	 }

	 else
	 {
	 $email = $arr_data->email;
	 $fullName = $arr_data->fullName;
	 }

	 $profilePicURL = $arr_data->profilePicURL;

	 if( empty($socialMediaId) || empty($profilePicURL) || empty($fullName) )
	 print_r(json_encode(array('status'=>-1)));
	 else
	 {
	 $user_login = $this->main_model->getData('user','userId',array('socialMediaId'=>$socialMediaId));
	 if($user_login)
	 {
	 $where = array('socialMediaId'=>$socialMediaId);
	 $this->getuser($where);
	 }
	 else
	 {
	 $insert = $this->main_model->insert('user',array('fullName'=>$fullName,'email'=>$email,'socialMediaID'=>$socialMediaID,'profilePic'=>$profilePicURL,'userType'=>$userType));
	 if($insert)
	 {
	 $where = array('userId'=>$insert);
	 $this->getuser($where);

	 }

	 else
	 print_r(json_encode(array('status'=>-2)));
	 }
	 //print_r(json_encode(array('status'=>-3,'ruleName'=>'loginFailed','errorMessage'=>'User name or Password is wrong')));

	 }

	 }
	 */
	 public function getuser($where, $devicetype = '', $devicetoken = '', $lang = 'en') {
	 	$additionalParams['join'] = array('country', 'country.countryId = user.countryId', 'left');
	 	$additionalParams['join2'] = array('city', 'city.cityId = user.cityId', 'left');
	 	if($lang == 'en')
	 	{
	 		$countryField = 'countryName';
	 	} else 
	 	{
	 		$countryField = 'name_'.$lang;
	 	}
	 	$user_login = $this -> main_model -> getBackendData('user', 'user.*,country.'.$countryField.' as countryName,city.name_' . $lang . ' as cityName', $where, '', '', '', '', $additionalParams);
	 	if ($user_login) {
	 		$points = $this -> main_model -> getData('points', 'levelOneRegistration,levelTwoRegistration,levelThreeRegistration');

	 		if (!empty($devicetype) && !empty($devicetoken)) {
	 			if ($devicetoken != $user_login[0] -> pushnotificationtoken) {
	 				$this -> main_model -> update('user', array('pushnotificationtoken' => $devicetoken, 'devicetype' => $devicetype), array('userId' => $user_login[0] -> userId));
	 			}
	 		}

			// contact api for ejapard servser chat

	 		if (strpos($user_login[0] -> profilePic, 'http') !== FALSE)
	 			$user_login[0] -> profilePic = $user_login[0] -> profilePic;
	 		else {

	 			$user_login[0] -> profilePic = base_url() . 'third_party/uploads/profile/' . $user_login[0] -> profilePic;
	 		}
	 		if (empty($user_login[0] -> profilePic))
	 			$user_login[0] -> profilePic = '';

			//unset($user_login[0]->password);
			//unset($user_login[0]->isOnline);
			//unset($user_login[0]->lastLoginDate);

	 		if (empty($user_login[0] -> fullName))
	 			$user_login[0] -> fullName = '';

	 		$user_login[0] -> password = '';
	 		if (empty($user_login[0] -> dateOfBirth))
	 			$user_login[0] -> dateOfBirth = '';
	 		if (empty($user_login[0] -> gender))
	 			$user_login[0] -> gender = 0;
	 		if (empty($user_login[0] -> countryId))
	 			$user_login[0] -> countryId = 0;
	 		if (empty($user_login[0] -> lastLoginDate))
	 			$user_login[0] -> lastLoginDate = '';
	 		if (empty($user_login[0] -> noOfPics))
	 			$user_login[0] -> noOfPics = 0;
	 		if (empty($user_login[0] -> userType))
	 			$user_login[0] -> userType = 0;

	 		if (empty($user_login[0] -> brief))
	 			$user_login[0] -> brief = "";

	 		if (empty($user_login[0] -> cityId))
	 			$user_login[0] -> cityId = "";

	 		if (empty($user_login[0] -> socialMediaId))
	 			$user_login[0] -> socialMediaId = '';
	 		if (empty($user_login[0] -> email))
	 			$user_login[0] -> email = '';

	 		if (empty($user_login[0] -> district))
	 			$user_login[0] -> district = '';

	 		if (empty($user_login[0] -> phone))
	 			$user_login[0] -> phone = '';

	 		if (empty($user_login[0] -> countryName))
	 			$user_login[0] -> countryName = '';

	 		$user_login[0] -> userType = intval($user_login[0] -> userType);
	 		$user_login[0] -> userId = intval($user_login[0] -> userId);
	 		$user_login[0] -> noOfPics = intval($user_login[0] -> noOfPics);
	 		$user_login[0] -> countryId = intval($user_login[0] -> countryId);
	 		$user_login[0] -> cityId = intval($user_login[0] -> cityId);
	 		$user_login[0] -> credit = intval($user_login[0] -> credit);
	 		$user_login[0] -> isPremium = intval($user_login[0] -> isPremium);
	 		$user_login[0] -> gender = intval($user_login[0] -> gender);
	 		$user_login[0] -> isOnline = intval($user_login[0] -> isOnline);
	 		$user_login[0] -> socialMediaId = ($user_login[0] -> socialMediaId);
	 		$user_login[0] -> userType = intval($user_login[0] -> userType);

	 		$user_login[0] -> registrationLevelOnePoints = intval($points[0] -> levelOneRegistration);
	 		$user_login[0] -> registrationLevelTwoPoints = intval($points[0] -> levelTwoRegistration);
	 		$user_login[0] -> registrationLevelThreePoints = intval($points[0] -> levelThreeRegistration);

			// $user_login[0]->phoneverified=intval ($user_login[0]->phoneverified);
			//$user_login[0]->emailvreified=intval ($user_login[0]->emailvreified);

	 		unset($user_login[0] -> phoneverified);
	 		unset($user_login[0] -> emailvreified);
	 		$user_login[0] -> phoneVerified = intval($user_login[0] -> phoneverified);
	 		$user_login[0] -> emailVerified = intval($user_login[0] -> emailverified);

	 		$user_login[0] -> devicetype = intval($user_login[0] -> devicetype);
	 		$user_login[0] -> relationship = intval($user_login[0] -> relationship);
	 		$user_login[0] -> income = intval($user_login[0] -> income);
			$user_login[0] -> jobRole = intval($user_login[0] -> jobRole);
	 		$user_login[0] -> receiveFriendRequest = intval($user_login[0] -> receiveFriendRequest);
	 		$user_login[0] -> viewPersonalInfo = intval($user_login[0] -> viewPersonalInfo);
	 		$user_login[0] -> viewContactsInfo = intval($user_login[0] -> viewContactsInfo);
	 		$user_login[0] -> receivePushNotification = intval($user_login[0] -> receivePushNotification);

	 		$user_login[0] -> registrationDate = strtotime($user_login[0] -> registrationDate);
	 		$user_login[0] -> lastLoginDate = strtotime($user_login[0] -> lastLoginDate);
	 		$user_login[0] -> dateOfBirth = strtotime($user_login[0] -> dateOfBirth);

	 		if (!$user_login[0] -> dateOfBirth)
	 			$user_login[0] -> dateOfBirth = 0;

	 		if (!$user_login[0] -> lastLoginDate)
	 			$user_login[0] -> lastLoginDate = 0;

			//$countryName = $user_login[0]->countryName;
	 		unset($user_login[0] -> SMSInviteText);

	 		$user_login[0] -> followingCount = $this -> main_model -> count('userfans', 'userId = ' . $user_login[0] -> userId . ' AND isFollow = 1');
	 		$user_login[0] -> followersCount = $this -> main_model -> count('userfans', 'fanId = ' . $user_login[0] -> userId . ' AND isFollow = 1');
	 		$user_login[0] -> allowLocation = 1;
	 		$update = $this -> main_model -> update('user', array('lastLoginDate' => date('Y-m-d H:i:s'), 'isOnline' => '1'), array('userId' => $user_login[0] -> userId));
	 		if ($update)
				//print_r(json_encode(array('status'=>2,'data'=>array("user"=>$user_login[0],"allowLocation"=>1,'countryName'=>$countryName,'followingCount'=>$followingCount,"followersCount"=>$followersCount))));
	 			print_r(json_encode(array('status' => 2, 'data' => $user_login[0])));
	 		else
	 			print_r(json_encode(array('status' => -1, 'data' => $user_login[0])));
	 	} else
	 	print_r(json_encode(array('status' => -3, 'ruleName' => 'loginFailed', 'errorMessage' => 'User name or Password is wrong')));
	 	$this->db->close();
	 }

	 public function logout() {
		 
	 	$json_data = $this -> input -> post('data');
	 	$arr_data = json_decode($json_data);

	 	$userId = intval($arr_data -> userID);

	 	if (empty($userId)) {

	 		print_r(json_encode(array('status' => -1)));
	 		$this->db->close();
	 		exit ;
	 	}
	 	$update = $this -> main_model -> update('user', array('pushnotificationtoken' => '', 'isOnline' => 0), array('userId' => $userId));
	 	if ($update)
	 		print_r(json_encode(array('status' => 1)));
	 	else
	 		print_r(json_encode(array('status' => -2)));
	 	$this->db->close();
	 }

	 public function updateToken() {
	 	$json_data = $this -> input -> post('data');
		//$json_data = '{"userId":"711", "token" : "123"}';
	 	$arr_data = json_decode($json_data);

	 	$userId = intval($arr_data -> userID);
	 	$token = $arr_data -> token;
		$deviceType = $arr_data -> deviceType;
		
	 	if (empty($userId) && empty($token)) {

	 		print_r(json_encode(array('status' => -1)));
	 		$this->db->close();
	 		exit ;
	 	}
	 	$this -> main_model -> update('user', array('pushnotificationtoken' => ''), array('pushnotificationtoken' => $token));
		
		if(!$deviceType)
		{
			$deviceType = '1';
			if(strlen($token)<70)$deviceType = '2';
		}
		
	 	$update = $this -> main_model -> update('user', array('pushnotificationtoken' => $token, 'devicetype' => $deviceType), array('userId' => $userId));
	 	if ($update)
	 		print_r(json_encode(array('status' => 1)));
	 	else
	 		print_r(json_encode(array('status' => -2)));
	 	$this->db->close();
	 }

	 public function pullMessage() {
	 	$json_data = $this -> input -> post('data');
		//$json_data = '{"userID":"711", "messageID" : "200"}';
	 	$arr_data = json_decode($json_data);

	 	$userId = intval($arr_data -> userID);
	 	$mesgId = intval($arr_data -> messageID);
	 	$fanId = intval($arr_data -> fanID);

	 	if (empty($userId) || empty($fanId)) {
	 		print_r(json_encode(array('status' => -1)));
	 		$this->db->close();
	 		exit ;
	 	}
	 	$messages = $this -> main_model -> getData('messages', "*", "senderUserID = '" . $fanId . "' AND recieverUserID = '" . $userId . "' AND messageID > '" . $mesgId . "'", "messageID DESC");

	 	if ($messages) {
	 		foreach ($messages as $row) {
	 			$where = 'userId = ' . $row -> senderUserID;
	 			$user2 = $this -> main_model -> getData('user', 'userId, fullName, profilePic, pushnotificationtoken , devicetype', $where);

	 			$haystack = $user2[0] -> profilePic;
	 			$needle = 'http';

	 			if (strpos($haystack, $needle) !== false) {
	 				$imgPath = $haystack;
	 			} else {
	 				$imgPath = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/profiles/" . $haystack;
	 			}
	 			$name = $user2[0] -> fullName;

	 			$data[] = array('messageID' => $row -> messageID, 'messageText' => $row -> messageText, 'senderUserID' => $row -> senderUserID, 'senderName' => $name, 'senderPic' => $imgPath, 'recieverUserID' => $row -> recieverUserID, 'sentAt' => strtotime($row -> sentAt), 'type' => $row -> type);
	 		}
	 		print_r(json_encode(array('status' => 2, 'data' => $data)));
	 	} else {
	 		print_r(json_encode(array('status' => -2)));
	 	}
	 	$this->db->close();
	 }

	 public function generateInviteToken() {
	 	$json_data = $this -> input -> post('data');
		//$json_data = '{"userID":"711", "messageText" : "Test"}';
	 	$arr_data = json_decode($json_data);

	 	$userId = intval($arr_data -> userID);
	 	$message = $arr_data -> messageText;
		if (empty($message)) {
	 		$message = "Hello darling,Please check this app. to see my pictures with FC Barcelona Players, and you can take pictures with them also.";
	 	}
	 	if (empty($userId)) {
	 		print_r(json_encode(array('status' => -1)));
	 		$this->db->close();
	 		exit ;
	 	}

	 	$userIdHash = md5($userId);
	 	$storeHash = $this -> main_model -> insert('invitationToken', array('userId' => $userId, 'messageText' => $message));
	 	if ($storeHash) {
	 		$invitationId = $storeHash;
	 		$binaryNumber = decbin(ord($invitationId));
	 		$concatStr = $userIdHash . $binaryNumber . time();
	 		$base64Str = base64_encode($concatStr);
	 		$storeToken = $this -> main_model -> update('invitationToken', array('invitationToken' => $base64Str), array('invitationID' => $storeHash));
	 		if ($storeToken) {
				$pointArr = $this -> main_model -> getData('points', 'inviteFriendsURLClickThrough');
	 			$points = $pointArr[0] -> inviteFriendsURLClickThrough;
				$this -> main_model -> incrementByValue('user', 'credit', 'userId=' . $userId, $points);
				
				$arr = array(
					'TransTypeID' => 'AP_03',
					'UserID' => $userId,
					'TransDate' => date('Y-m-d'),
					'TransTime' => date('H:i:s'),
					'points' => $points
				);
				$this -> main_model -> insert('TransactionLog',$arr);
				print_r(json_encode(array('status' => 2, 'data' => "https://www.fcbstudio.mobi/invite.php?invtoken=" . $base64Str)));
	 		} else {
	 			print_r(json_encode(array('status' => -2, )));
	 		}
	 	} else {
	 		print_r(json_encode(array('status' => -2)));
	 	}
	 	$this->db->close();
	 }

	 public function userIsActive() {
	 	$json_data = $this -> input -> post('data');
		//$json_data = '{"userID":"711", "lang" : "en"}';
	 	$arr_data = json_decode($json_data);

	 	$userId = intval($arr_data -> userID);
	 	$language = $arr_data -> lang;

	 	if (empty($userId)) {
	 		print_r(json_encode(array('status' => -1)));
	 		$this->db->close();
	 		exit ;
	 	}

	 	$nowDate = date('Y-m-d');
	 	$result = $this->main_model->getData('user','pushnotificationtoken, deviceType, last_points_update, credit', array('userId' => $userId));
	 	$result[0]->last_points_update;
	 	$userPoints = $result[0]->credit;
	 	$device_token = $result[0]->pushnotificationtoken;
	 	$device_type = $result[0]->deviceType;

	 	if($result[0]->last_points_update == $nowDate)
	 	{
	 		$isActive = $this -> main_model -> update('user', array('isOnline' => 1), array('userId' => $userId));
	 		if($isActive)
	 		{
	 			print_r(json_encode(array('status' => 2, 'credit' => $userPoints)));
	 		} else {
	 			print_r(json_encode(array('status' => 2, 'credit' => $userPoints)));
	 		}
	 	} else 
	 	{
	 		$points = $this->main_model->getData('points','dialyPointsDay1');

	 		$newPoints = $userPoints + $points[0]->dialyPointsDay1;
	 		$msg = "Welcome Back. ".$points[0]->dialyPointsDay1."xp added";
	 		$isActive = $this -> main_model -> update('user', array('isOnline' => 1, 'credit' => $newPoints, 'last_points_update' => $nowDate), array('userId' => $userId));
	 		if($isActive)
	 		{
	 			$this->load->library('notification_lib');
	 			if ($device_type == 2) {
	 				$this->notification_lib->push_iphone($device_token, $msg, "9", "", "", "", "", "", "");
	 			} 
	 			else if ($device_type == 1) {
	 				$this->notification_lib->push_android($device_token, $msg, "9", "", "", "", "", "", "");
	 			}
	 			print_r(json_encode(array('status' => 2, 'credit' => $newPoints)));
	 		} else {
	 			print_r(json_encode(array('status' => 2, 'credit' => $userPoints)));
	 		}
	 	}

	 	$this->db->close();
		file_put_contents('testindex4.txt', json_encode(array('status' => 2, 'credit' => $newPoints), FILE_APPEND);
		file_put_contents('testindex4.txt', json_encode(array('status' => 2, 'credit' => $userPoints)), FILE_APPEND);
	 }

	 public function userIsNotActive()
	 {
	 	$json_data = $this -> input -> post('data');
		//$json_data = '{"userID":"711", "lang" : "en"}';
	 	$arr_data = json_decode($json_data);

	 	$userId = intval($arr_data -> userID);

	 	if (empty($userId)) {
	 		print_r(json_encode(array('status' => -1)));
	 		$this->db->close();
	 		exit ;
	 	}
	 	$isActive = $this -> main_model -> update('user', array('isOnline' => 2), array('userId' => $userId));
	 	if($isActive)
	 	{
	 		print_r(json_encode(array('status' => 2)));
	 	} else 
	 	{
	 		print_r(json_encode(array('status' => -2)));
	 	}
	 	$this->db->close();
	 }
	
	 public function testMail() {
	 	$this -> load -> library('email');

	 	$config = array();

	 	$config['protocol'] = "smtp";
	 	$config['smtp_host'] = "localhost";
	 	$config['smtp_port'] = "25";
	 	$config['smtp_timeout'] = '7';
	 	$config['smtp_user'] = '';
	 	$config['smtp_pass'] = '';
	 	$config['mailtype'] = 'html';
	 	$config['smtp_crypto'] = 'tls';
	 	$config['charset'] = 'utf-8';
	 	$config['newline'] = "\r\n";
	 	$config['wordwrap'] = TRUE;
	 	$this -> email -> initialize($config);
		//$this->email->from('noreply@MoraselMobileApp.com', 'Morasel Mobile Application');
	 	$this -> email -> from('no-reply@fcbstudio.mobi', 'FCB Studio');
	 	$this -> email -> to($email);

	 	$this -> email -> subject('FCB Studio - Email Verification');
	 	$msg = '<html><body>
	 	Hey FCB Studio Fan,<br>
	 	Thanks for using FCB Studio, you are almost done.<br>
	 	<br>
	 	Complete the process by clicking the link below:
	 	http://barca.tawasoldev.com/barca/index.php/user/verifyemail/asdfs


	 	<br><br>

	 	Regards,<br>
	 	FCB Team.<br></body></html>';
	 	$this -> email -> message($msg);

	 	$status = $this -> email -> send();
	 }	
	}
