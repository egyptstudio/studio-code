<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Fans extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function loadFans() {
		//$this->output->enable_profiler(TRUE);
		$json_data = $this -> input -> post('data');

		//$json_data ='{"userID" : "1731","followingFilter" : "0","countryFilter" : [], "favoritesFilter" : "2", "onlyPremuim" : false,"startingLimit" : "0","sortedBy" : 1, "onlineOfflineFilter" : "3","keyword" : "","maleFemaleFilter" : "3"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$maleFemaleFilter = intval($arr_data -> maleFemaleFilter);
		$startingLimit = intval($arr_data -> startingLimit);
 
		$onlineOfflineFilter = intval($arr_data -> onlineOfflineFilter);
		$favoritesFilter = intval($arr_data -> favoritesFilter);
		$followingFilter = intval($arr_data -> followingFilter);
		$countryFilter = ($arr_data -> countryFilter);
		$onlyPremuim = intval($arr_data -> onlyPremuim);
		$sortedBy = intval($arr_data -> sortedBy);
		$keyword = ($arr_data -> keyword);

		if (empty($userId))
			$userId = 0;

		if ($sortedBy == 1) {
			$user = $this -> main_model -> getData('user', 'district,cityId,countryId', array('userId' => $userId));
			if ($user) {
				$cityId = intval($user[0] -> cityId);
				$countryId = intval($user[0] -> countryId);
				$district = $user[0] -> district;
			} else
				$cityId = $countryId = 0;
				$district = 'Nasr city';
		}

		$select = 'fullname as fanName , blocked as hidden, user.userId as fanID , isOnline as fanOnline , isPremium , profilePic as fanPicture ,  noOfPics as fanPicNumber  ,userfans.isFollow as isFollowed,userfans.isFavourite as isFavorite,userfans.isFriend';

		if ($maleFemaleFilter == 1)
			$where = 'gender = 1 ';
		elseif ($maleFemaleFilter == 2)
			$where = 'gender = 2 ';
		else
			$where = '1 ';

		if ($onlineOfflineFilter == 1)
			$where .= ' AND isOnline = 1 ';
		elseif ($onlineOfflineFilter == 2)
			$where .= ' AND isOnline = 0 ';
		elseif ($onlineOfflineFilter == 3)
			$where .= ' AND isOnline != 3 ';

		if ($onlyPremuim == 1)
			$where .= ' AND isPremium = 1 ';

		if (!empty($keyword))
			$where .= ' AND fullname like "%' . $keyword . '%" ';

		if ($sortedBy == 1)
			$order = "field (countryId,$countryId) desc,field (cityid,$cityId) desc,field (district,'$district') desc,registrationDate DESC";
		elseif ($sortedBy == 2)
			$order = 'noOfPics desc';

		if (!empty($countryFilter))
			$where .= ' AND countryId in (' . join(',', $countryFilter) . ')';

		if ($favoritesFilter == 1)
			$where .= ' AND userfans.isFavourite=1';
		if ($followingFilter == 1){
			$where .= ' AND userfans.isFollow=1';
		}else if ($followingFilter == 2) {
			$follow = $this -> main_model -> getData('userfans', 'userId,fanId,isFollow', array('isFollow' => 1, 'fanId' => $userId));

			if ($follow) {

				foreach ($follow as $fan) {
					$fanids[] = $fan -> userId;
				}

				$where .= ' AND user.userId in (' . join(',', $fanids) . ')';
			} else
				$where .= ' AND user.userId in (0)';
		} else if ($followingFilter == 3) {
			$follow = $this -> main_model -> getData('userfans', 'userId,fanId,isFollow', array('isFollow' => 1, 'userId' => $userId));

			$following = $this -> main_model -> getData('userfans', 'userId,fanId,isFollow', array('isFollow' => 1, 'fanId' => $userId));

			if ($follow) {

				foreach ($follow as $fan) {
					$fanids[] = $fan -> fanId;
				}
				//$where .= ' AND userId in (' . join(',', $fanids) . ')';
			} //else
				//$where .= ' AND userID in (0)';
			if ($following) {

				foreach ($following as $fan) {
					$fanids[] = $fan -> userId;
				}
				//$where .= ' AND userId in (' . join(',', $fanids) . ')';
			} //else
				//$where .= ' AND userID in (0)';
			
			if($fanids)
				$where .= ' AND user.userId in (' . join(',', $fanids) . ')';
			else
				$where .= ' AND user.userId in (0)';
		}
		$where .= ' AND user.blocked = 0 AND user.userId != ' . $userId;

		$where .= " AND user.userId not in  ( select fanId from userblock where userId = $userId )    AND user.userId not in ( select userId from userblock where fanId = $userId )";

		$sql = "select " . $select . " from user left join userfans on (userfans.userId = " . $userId . " AND userfans.fanId = user.userId) where " . $where . "  order by " . $order . " limit $startingLimit,50";
		$query = $this -> db -> query($sql);
		$friends = $query -> result();

		$data = array();

		if ($friends) {
			foreach ($friends as $friend) {
				$friend -> fanID = intval($friend -> fanID);
				$friend -> fanOnline = intval($friend -> fanOnline);
				$friend -> isPremium = intval($friend -> isPremium);
				$friend -> fanPicture = $this -> userPic($friend -> fanPicture);
				$friend -> isFollowed = intval($friend -> isFollowed);
				$friend -> isFavorite = intval($friend -> isFavorite);
				$friend -> fanPicNumber = intval($friend -> fanPicNumber);
				$friend -> isFriend = intval($friend -> isFriend);
				$data[] = $friend;
			}
		}
		echo json_encode(array('status' => 2, 'data' => array('nextStartingLimit' => count($friends), 'data' => $data)));
		$this -> db -> close();
	}

	public function getSuggestionsFans() {
		//$this->output->enable_profiler(TRUE);
		$json_data = $this -> input -> post('data');
		//$json_data ='{"keyword" : "U00","sortedBy":2 ,"favoritesFilter":0}';
		$arr_data = json_decode($json_data);

		$keyword = ($arr_data -> keyword);

		if (strlen($keyword) > 2) {
			$select = 'fullname as fanName,userId as fanID,profilePic as fanPicture';
			$where = 'fullname like "' . $keyword . '%"';
			$order = 'noOfPics desc';
			$sql = 'select ' . $select . ' from user where ' . $where . ' order by ' . $order . ' limit 0,50';
			$query = $this -> db -> query($sql);
			$friends = $query -> result();
			$data = array();
			if ($friends) {
				foreach ($friends as $friend) {
					$friend -> fanID = intval($friend -> fanID);
					$friend -> fanPicture = $this -> userPic($friend -> fanPicture);

					$data[] = $friend;
				}
			}
			print_r(json_encode(array("status" => 2, "data" => array(" nextStartingLimit" => count($friends), "data" => $data))));
		} else {
			print_r(json_encode(array('status' => -1)));
		}
		$this -> db -> close();
	}

	public function loadFollowersForFan() {
		//$this -> output -> enable_profiler(TRUE);
		$json_data = $this -> input -> post('data');
		//$json_data = '{"userID" : "1731","fanID":1707 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$startingLimit = intval($arr_data -> startingLimit);
		$fanID = intval($arr_data -> fanID);

		if (empty($userId) || empty($fanID)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$select = 'user.fullname as fanName , user.userId as fanID , user.isOnline as fanOnline , user.isPremium , user.profilePic as fanPicture ,  noOfPics as fanPicNumber  ,(select isFollow from userfans where userId = ' . $userId . ' AND fanId = fanID limit 1  ) as isFollowed ,  (select isFavourite from userfans where userId = ' . $userId . ' AND fanId = fanID limit 1 ) as isFavorite,  (select isFriend from userfans where userId = ' . $userId . ' AND fanId = fanID limit 1 ) as isFriend';
			$sql = 'select ' . $select . ' from userfans left join user on (userfans.userId = user.userId) where userfans.fanId = ' . $fanID . ' and userfans.isFollow=1 order by userfans.userId limit ' . $startingLimit . ',50 ';
			$query = $this -> db -> query($sql);
			$friends = $query -> result();
			$data = array();
			if ($friends) {
				foreach ($friends as $friend) {
					$check = $this -> main_model -> getData('userblock', "*", 'userId = "' . $userId . '" AND fanId = "' . $friend -> fanID . '"');
					if ($check) {
					} else {
						$friend -> fanID = intval($friend -> fanID);
						$friend -> fanOnline = intval($friend -> fanOnline);
						$friend -> isPremium = intval($friend -> isPremium);
						$friend -> fanPicture = $this -> userPic($friend -> fanPicture);
						$friend -> isFollowed = intval($friend -> isFollowed);
						$friend -> isFavorite = intval($friend -> isFavorite);
						$friend -> fanPicNumber = intval($friend -> fanPicNumber);
						$friend -> isFriend = intval($friend -> isFriend);
						$data[] = $friend;
					}
				}
			}
			print_r(json_encode(array("status" => 2, "data" => array(" nextStartingLimit" => count($friends), "data" => $data))));
		}
		$this -> db -> close();
	}

	public function loadFollowingForFan() {
		//$this->output->enable_profiler(TRUE);
		$json_data = $this -> input -> post('data');
		//$json_data ='{"userID" : "1731","fanID":1707 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$startingLimit = intval($arr_data -> startingLimit);
		$fanID = intval($arr_data -> fanID);

		if (empty($userId) || empty($fanID)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$select = 'user.fullname as fanName , user.userId as fanID , user.isOnline as fanOnline , user.isPremium , user.profilePic as fanPicture ,  noOfPics as fanPicNumber  ,(select isFollow from userfans where userId = ' . $userId . ' AND fanId = fanID limit 1  ) as isFollowed ,  (select isFavourite from userfans where userId = ' . $userId . ' AND fanId = fanID limit 1 ) as isFavorite,  (select isFriend from userfans where userId = ' . $userId . ' AND fanId = fanID limit 1 ) as isFriend';
			$sql = 'select ' . $select . ' from userfans left join user on (userfans.fanId = user.userId) where userfans.userId = ' . $fanID . ' and userfans.isFollow=1 order by userfans.userId limit ' . $startingLimit . ',50 ';
			$query = $this -> db -> query($sql);
			$friends = $query -> result();
			$data = array();
			if ($friends) {
				foreach ($friends as $friend) {
					$check = $this -> main_model -> getData('userblock', "*", 'userId = "' . $userId . '" AND fanId = "' . $friend -> fanID . '"');
					if ($check) {
					} else {
						$friend -> fanID = intval($friend -> fanID);
						$friend -> fanOnline = intval($friend -> fanOnline);
						$friend -> isPremium = intval($friend -> isPremium);
						$friend -> fanPicture = $this -> userPic($friend -> fanPicture);
						$friend -> isFollowed = intval($friend -> isFollowed);
						$friend -> isFavorite = intval($friend -> isFavorite);
						$friend -> fanPicNumber = intval($friend -> fanPicNumber);
						$friend -> isFriend = intval($friend -> isFriend);
						$data[] = $friend;
					}
				}
			}
			print_r(json_encode(array("status" => 2, "data" => array(" nextStartingLimit" => count($friends), "data" => $data))));
		}
		$this -> db -> close();
	}

	public function loadPremuimBar() {
		$sql = "select userId as fanID,profilePic as fanPicture from user where isPremium=1 AND blocked = 0 order By RAND() limit 8";
		$query = $this -> db -> query($sql);
		$premiums = $query -> result();
		$data = array();
		if ($premiums) {
			foreach ($premiums as $premium) {
				$premium -> fanPicture = $this -> userPic($premium -> fanPicture);
				$data[] = $premium;
			}
		}
		print_r(json_encode(array("status" => 2, "data" => $data)));
		$this -> db -> close();
	}

	public function loadContacts() {
		//$this->output->enable_profiler(TRUE);
		$json_data = $this -> input -> post('data');
		//$json_data ='{"userID" : "1731","fanID":1707 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);

		if (empty($userId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$select = 'userOneID , userTwoID , fullname as fanName ,user.  userId as fanID , isOnline as fanOnline , isPremium , profilePic as fanPicture ,  noOfPics as fanPicNumber  ,(select isFollow from userfans where userId = ' . $userId . ' AND fanId = fanID limit 1  ) as isFollowed ,  (select isFavourite from userfans where userId = ' . $userId . ' AND fanId = fanID limit 1 ) as isFavorite';

			$additionalparams['join'] = array('user', 'user.userId=usercontacts.userTwoID', 'left');
			$users = $this -> main_model -> getBackendData('usercontacts', $select, array('usrerOneID' => $userId), '', '', '', '', $additionalparams);
			if (!$users)
				print_r(json_encode(array("status" => 2, "data" => array())));
			else {
				foreach ($users as $friend) {
					$friend -> fanID = intval($friend -> fanID);
					$friend -> fanOnline = intval($friend -> fanOnline);
					$friend -> isPremium = intval($friend -> isPremium);
					$friend -> fanPicture = $this -> userPic($friend -> fanPicture);
					$friend -> isFollowed = intval($friend -> isFollowed);
					$friend -> isFavorite = intval($friend -> isFavorite);
					$friend -> fanPicNumber = intval($friend -> fanPicNumber);
					$friend -> isFriend = intval($this -> check_friend($userId, $friend -> fanID));

					$data[] = $friend;
				}
				print_r(json_encode(array("status" => 2, "data" => $data)));
			}
		}
		$this -> db -> close();
	}

	public function changeFavorite() {
		$json_data = $this -> input -> post('data');
		//  $json_data ='{"userID" : "817","fanID":55 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$favorite = intval($arr_data -> favorite);
		$fanID = intval($arr_data -> fanID);

		if (empty($userId) || empty($fanID) || $favorite > 1) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$user = $this -> main_model -> getData('userfans', 'id', array('userId' => $userId, 'fanId' => $fanID));
			if ($user)
				$upd = $this -> main_model -> update('userfans', array('isFavourite' => $favorite), array('userId' => $userId, 'fanId' => $fanID));
			else
				$upd = $this -> main_model -> insert('userfans', array('userId' => $userId, 'fanId' => $fanID, 'isFavourite' => $favorite));
			if ($upd) {
				/*
				 if($favorite == 1)
				 {
				 $this->main_model->increment('user','favorites',"userId =".$userId);
				 }
				 else
				 {
				 $this->main_model->decrement('user','favorites',"userId =".$userId);
				 }
				 */
				print_r(json_encode(array('status' => 1)));
			} else
				print_r(json_encode(array('status' => -2)));
		}
		$this -> db -> close();
	}

	public function changeFollow() {
		$json_data = $this -> input -> post('data');
		//  $json_data ='{"userID" : "817","fanID":55 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$follow = intval($arr_data -> follow);
		$fanID = intval($arr_data -> fanID);

		if (empty($userId) || empty($fanID) || $follow > 1) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$user = $this -> main_model -> getData('userfans', 'id', array('userId' => $userId, 'fanId' => $fanID));
			if ($user)
				$upd = $this -> main_model -> update('userfans', array('isFollow' => $follow), array('userId' => $userId, 'fanId' => $fanID));
			else			
				$upd = $this -> main_model -> insert('userfans', array('userId' => $userId, 'fanId' => $fanID, 'isFollow' => $follow));
			

			if ($upd) {
				
				 if($follow == 1)
				 {
				 $this->main_model->insert('notification',array('senderId'=>$userId,'receiverId'=>$fanID,'postId'=>0,'notificationType'=>4,'notificationtime'=>date("Y-m-d H:i:s")));
				 $this->main_model->increment('user','following',"userId =".$userId);
				 $this->main_model->increment('user','followers',"userId =".$fanID);

				 }
				 else
				 {
				 $this->main_model->decrement('user','following',"userId =".$userId);
				 $this->main_model->decrement('user','followers',"userId =".$fanID);
				 }
				 $this->main_model->SendNotification($fanID, 'Some one followed you.', 10);
				print_r(json_encode(array('status' => 1)));
			} else
				print_r(json_encode(array('status' => -2)));
		}
		$this -> db -> close();
	}

	public function loadFriendRequests() {
		//$this->output->enable_profiler(TRUE);
		$json_data = $this -> input -> post('data');
		//$json_data ='{"userID" : "1707","fanID":1737 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$startlimit = intval($arr_data -> startlimit);
		
		if ($userId < 1)
			print_r(json_encode(array('status' => -1)));
		else {
			if (empty($startlimit))
				$startlimit = 0;
			$select = 'fullname as fanName , user.userId as fanID , isOnline as fanOnline , isPremium , profilePic as fanPicture ,  noOfPics as fanPicNumber  ,isFollow as isFollowed ,   isFavourite  as isFavorite';

			$order = "uf.id desc";
			$additionalParams['join'] = array('user', 'user.userId=uf.userId', 'left');
			$users = $this -> main_model -> getBackendData('userfans as uf', $select, array('fanId' => $userId, 'friendRequest' => 1, 'isFriend' => 0, 'isBlocked' => 0), $order, '', 50, $startlimit, $additionalParams);
			$data = array();
			if ($users) {
				foreach ($users as $friend) {
					$friend -> fanID = intval($friend -> fanID);
					$friend -> fanOnline = intval($friend -> fanOnline);
					$friend -> isPremium = intval($friend -> isPremium);
					$friend -> fanPicture = $this -> userPic($friend -> fanPicture);
					$friend -> isFollowed = intval($friend -> isFollowed);
					$friend -> isFavorite = intval($friend -> isFavorite);
					$friend -> fanPicNumber = intval($friend -> fanPicNumber);

					$data[] = $friend;
				}
			}
			print_r(json_encode(array("status" => 2, "data" => $data)));
		}
		$this -> db -> close();
	}

	public function declineFriendRequests() {
		$json_data = $this -> input -> post('data');
		//  $json_data ='{"userID" : "817","fanID":55 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$fanID = intval($arr_data -> fanID);
		if ($userId < 1 || $fanID < 1)
			print_r(json_encode(array('status' => -1)));
		else {
			$upd = $this -> main_model -> update('userfans', array('friendRequest' => 0), array('userId' => $userId, 'fanId' => $fanID));
			if ($upd)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -2)));
		}
		$this -> db -> close();
	}

	public function changeFriend() {
		$json_data = $this -> input -> post('data');
		//  $json_data ='{"userID" : "817","fanID":55 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$fanID = intval($arr_data -> fanID);
		$friend = intval($arr_data -> friend);
		if ($userId < 1 || $fanID < 1)
			print_r(json_encode(array('status' => -1)));
		else {
			if ($friend == 1) {
				$where = "(userId = " . $userId . " AND fanId = " . $fanID . " ) OR (userId = " . $fanID . " AND fanId = " . $userId . " ) ";
				$check = $this -> main_model -> getData('userfans', '*', $where);
				if ($check) {
					$where_request_before = "(userId = " . $fanID . " AND fanId = " . $userId . " AND friendRequest = 1 AND isFriend = 0 AND isBlocked = 0)";
					$check_if_has_request = $this -> main_model -> getData('userfans', '*', $where_request_before);
					if ($check_if_has_request) {
						$this -> main_model -> update('userfans', array('isFriend' => 1, 'friendRequest' => 0), array('userId' => $userId, 'fanId' => $fanID));
						$this -> main_model -> update('userfans', array('isFriend' => 1, 'friendRequest' => 0), array('userId' => $fanID, 'fanId' => $userId));
						$this -> main_model -> insert('notification', array('senderId' => $userId, 'receiverId' => $fanID, 'postId' => 0, 'notificationType' => 7, 'notificationtime' => date("Y-m-d H:i:s")));
						//$this -> main_model -> insert('notification', array('senderId' => $fanID, 'receiverId' => $userId, 'postId' => 0, 'notificationType' => 7, 'notificationtime' => date("Y-m-d H:i:s")));
						$where_to_delete = "(senderId = " . $fanID . " AND receiverId = " . $userId . " AND notificationType = 3) or (senderId = " . $userId . " AND receiverId = " . $fanID . " AND notificationType = 3) ";
						$this -> main_model -> delete('notification', $where_to_delete);
						// $this->main_model->increment('user','friends',"userId =".$userId);
						//  $this->main_model->increment('user','friends',"userId =".$fanID);

						//  $this->main_model->decrement('user','friendRequests',"userId =".$userId);
						//  $this->main_model->decrement('user','friendRequests',"userId =".$fanID);

						//print_r(json_encode(array('status' => 2)));
					} else {
						// $this->main_model->increment('user','friendRequests',"userId =".$fanID);
						$this -> main_model -> insert('notification', array('senderId' => $userId, 'receiverId' => $fanID, 'postId' => 0, 'notificationType' => 3, 'notificationtime' => date("Y-m-d H:i:s")));
						$this -> main_model -> update('userfans', array('friendRequest' => 1), array('userId' => $userId, 'fanId' => $fanID));
					}
				} else {
					// $this->main_model->increment('user','friendRequests',"userId =".$fanID);+
					$this -> main_model -> insert('userfans', array('userId' => $userId, 'fanId' => $fanID, 'friendRequest' => 1));
					$this -> main_model -> insert('notification', array('senderId' => $userId, 'receiverId' => $fanID, 'postId' => 0, 'notificationType' => 3, 'notificationtime' => date("Y-m-d H:i:s")));
				}
			} elseif ($friend == 0) {
				$this -> main_model -> update('userfans', array('isFriend' => 0, 'friendRequest' => 0), array('userId' => $userId, 'fanId' => $fanID));
				$this -> main_model -> update('userfans', array('isFriend' => 0, 'friendRequest' => 0), array('userId' => $fanID, 'fanId' => $userId));
			}
			$this->main_model->SendNotification($fanID, 'Some one sent you a friend request.', 10);
			print_r(json_encode(array('status' => 1)));
		}
		$this -> db -> close();
	}

	public function changeBlock() {
		$json_data = $this -> input -> post('data');
		//  $json_data ='{"userID" : "817","fanID":55 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$fanID = intval($arr_data -> fanID);
		$block = intval($arr_data -> block);
		if ($userId < 1 || $fanID < 1)
			print_r(json_encode(array('status' => -1)));
		else {
			$ins = $this -> main_model -> insert('userblock', array('userId' => $userId, 'fanId' => $fanID));
			if ($ins) {
				$where = "( userId = " . $userId . " AND fanId =" . $fanID . " ) OR (fanId =" . $userId . " AND userId = " . $fanID . " )";
				$this -> main_model -> delete('userfans', $where);
				print_r(json_encode(array('status' => 1)));
			}
			/*
			 $upd = $this->main_model->update('userfans',array('isBlocked'=>$block),array('userId'=>$userId,'fanId'=>$fanID));
			 if($upd)
			 print_r(json_encode(array('status'=>1)));
			 else
			 print_r(json_encode(array('status'=>-2)));
			 */

		}
		$this -> db -> close();
	}

	public function getPublicProfileFan() {
		//$this->output->enable_profiler(TRUE);
		$json_data = $this -> input -> post('data');
		//$json_data ='{"userID" : "789","fanID":1707 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userID);
		$fanID = intval($arr_data -> fanID);

		if ($fanID < 1)
			print_r(json_encode(array('status' => -1)));
		else {
			$where = "(post.hidden=0 and post.userId = " . $fanID . " AND post.originalPostId = 0 AND ( post.privacy=1 or post.privacy=2 or (post.privacy='3' and (select isFriend from userfans where userfans.userId=" . $fanID . " AND userfans.fanId=" . $userId . " )=1)))";

			$counts = $this -> main_model -> count('post', $where);
			$update = $this -> main_model -> update('user', array("noOfPics" => $counts), array("userId" => $fanID));

			if ($userId < 1)
				$userId = 0;
			$select = 'aboutMe, relationship,religion,education,job,jobRole,company,income,status,email,phone, phoneCode,viewPersonalInfo,viewContactsInfo,country.countryName as fanCountry ,city.name_en as fanCity,district as fanDistrict , brief as fanBrief ,  fullname as fanName , user.userId as fanID , isOnline as fanOnline , isPremium , profilePic as fanPicture ,  noOfPics as fanPicNumber  , userfans.isFollow as isFollowed, userfans.isFavourite as isFavorite, userfans.isBlocked,userfans.isFriend, (select count(id) from userfans where fanId = ' . $fanID . ' AND isFollow = 1   ) as fanFollowers , (select count(id) from userfans where userId = ' . $fanID . ' AND isFollow = 1   ) as fanFollows';

			$additionalParams['join'] = array('country', 'country.countryId=user.countryId', 'left');
			$additionalParams['join2'] = array('city', 'city.cityId=user.cityId', 'left');
			$additionalParams['join3'] = array('userfans', 'userfans.fanId=user.userId and userfans.userId = ' . $userId, 'left');
			$user = $this -> main_model -> getBackendData('user', $select, array('user.userId' => $fanID), '', '', '', '', $additionalParams);
			if ($user) {
				$user[0] -> fanID = intval($user[0] -> fanID);
				$user[0] -> fanOnline = intval($user[0] -> fanOnline);
				$user[0] -> isPremium = intval($user[0] -> isPremium);
				$user[0] -> fanPicture = $this -> userPic($user[0] -> fanPicture);
				$user[0] -> isFollowed = intval($user[0] -> isFollowed);
				$user[0] -> isBlocked = intval($user[0] -> isBlocked);
				$user[0] -> isFavorite = intval($user[0] -> isFavorite);
				$user[0] -> fanPicNumber = intval($user[0] -> fanPicNumber);
				$user[0] -> isFriend = intval($user[0] -> isFriend);
				$user[0] -> fanFollows = intval($user[0] -> fanFollows);
				$user[0] -> fanFollowers = intval($user[0] -> fanFollowers);

				$user[0] -> fanBrief = strval($user[0] -> fanBrief);
				$user[0] -> fanDistrict = strval($user[0] -> fanDistrict);

				$user[0] -> fanPicture = strval($user[0] -> fanPicture);
				$user[0] -> fanCountry = strval($user[0] -> fanCountry);

				$user[0] -> fanCity = strval($user[0] -> fanCity);
				$user[0] -> fanName = strval($user[0] -> fanName);

				$user[0] -> fanBrief = strval($user[0] -> fanBrief);
				$user[0] -> fanDistrict = strval($user[0] -> fanDistrict);

				if ($user[0] -> viewPersonalInfo == 1 || ($user[0] -> viewPersonalInfo == 2 && $user[0] -> isFriend == 1)) {
					$info[] = array("title" => "about me", "value" => strval($user[0] -> aboutMe));					
					$info[] = array("title" => "education", "value" => strval($user[0] -> education));
					$info[] = array("title" => "Profession", "value" => strval($user[0] -> job));
					$info[] = array("title" => "job role", "value" => strval($user[0] -> jobRole));
					$info[] = array("title" => "company", "value" => strval($user[0] -> company));
					$info[] = array("title" => "status", "value" => strval($user[0] -> status));
					$info[] = array("title" => "income", "value" => $user[0] -> income);
					$info[] = array("title" => "relationship", "value" => $user[0] -> relationship);
				}

				if ($user[0] -> viewContactsInfo == 1 || ($user[0] -> viewContactsInfo == 2 && $user[0] -> isFriend == 1)) {
					$info[] = array("title" => "email", "value" => strval($user[0] -> email));
					$info[] = array("title" => "phone", "value" => strval($user[0] -> phoneCode) + strval($user[0] -> phone));
					

				}
				if (empty($info))
					$info = array();
				$user[0] -> moreInfo = $info;

				unset($user[0] -> aboutMe);
				unset($user[0] -> relationship);
				
				unset($user[0] -> education);
				unset($user[0] -> job);
				unset($user[0] -> jobRole);
				unset($user[0] -> company);
				unset($user[0] -> income);
				unset($user[0] -> status);
				unset($user[0] -> email);
				unset($user[0] -> phone);
				unset($user[0] -> phoneCode);
				unset($user[0] -> viewContactsInfo);
				unset($user[0] -> viewPersonalInfo);
				/*
				 echo"<pre>";
				 print_r($user[0]);
				 echo"</pre>";

				 */
				print_r(json_encode(array("status" => 2, "data" => $user[0])));

			} else
				print_r(json_encode(array("status" => 2, "data" => array())));
		}
		$this -> db -> close();
	}

	public function whoInstalledApp() {
		$json_data = $this -> input -> post('data');
		//   $json_data ='{"userID" : "817","contacts":[{"name":"contact 1 ","phones":"5465465","emails":"aaa@sss.ss"},{"name":"contact 3 ","phones":"88888","emails":"aaa@sss.ss"},{"name":"contact 2 ","phones":"111111","emails":"aaa@sss.ss"}] }';
		$arr_data = json_decode($json_data);

		$phones = $arr_data -> contacts;
		$userId = $arr_data -> userID;
		if (!empty($phones) && !empty($userId)) {
			//$del = $this->main_model->delete('usercontacts',array('userOneID'=>$userId));
			foreach ($phones as $phone) {

				/*
				 $where = "phoen like '".$phone->phones."%'";
				 $check = $this->main_model->getData(user,'userId,phone',$where);
				 if($check)
				 $this->main_model->insert('userContacts',array('userOneID'=>$userId,'userTwoID'=>$check[0]->userId,'installed'=>1));
				 else
				 $this->main_model->insert('userContacts',array('userOneID'=>$userId,'userTwoID'=>$phone->contactName,'installed'=>0));
				 */
				$data[] = array('name' => $phone -> name);
			}
			print_r(json_encode(array('status' => 2, 'data' => array("installed" => array(), "nonInstalled" => $data))));
		} else
			print_r(json_encode(array('status' => -1)));
	}

	public function inviteToApp() {
		$json_data = $this -> input -> post('data');
		//  $json_data ='{"userID" : "817","fanID":55 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$phones = $arr_data -> contacts;
		$msg = $arr_data -> message;
		if (!empty($phones) && !empty($msg)) {
			foreach ($phones as $phone) {
				if ($phone -> phones)
					$this -> sendSMS($msg, $phone -> phones);
				else
					$this -> sendMail($msg, $phone -> email);
			}
			print_r(json_encode(array('status' => 1)));
		} else
			print_r(json_encode(array('status' => -1)));
	}

	public function loadFollowingSuggestion() {
		//$this->output->enable_profiler(TRUE);
		$json_data = $this -> input -> post('data');
		//$json_data ='{"userID" : "1731","fanID":1707 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$nextStartingLimit = intval($arr_data -> nextStartingLimit);
		if (empty($nextStartingLimit))
			$nextStartingLimit = 0;
		$order = "noOfPics desc";
		$select = ' fullname as fanName , userId as fanID , isOnline as fanOnline , isPremium , profilePic as fanPicture ,  noOfPics as fanPicNumber ';
		$sql = "select " . $select . " from user   order by " . $order . " limit $nextStartingLimit,50";
		$query = $this -> db -> query($sql);
		$friends = $query -> result();
		$data = array();
		if ($friends) {
			foreach ($friends as $friend) {
				$friend -> fanID = intval($friend -> fanID);
				$friend -> fanOnline = intval($friend -> fanOnline);
				$friend -> isPremium = intval($friend -> isPremium);
				$friend -> fanPicture = $this -> userPic($friend -> fanPicture);
				$friend -> isFollowed = intval(0);
				$friend -> isFavorite = intval(0);
				$friend -> fanPicNumber = intval($friend -> fanPicNumber);
				$friend -> isFriend = intval(0);

				$data[] = $friend;
			}
		}

		print_r(json_encode(array("status" => 2, "data" => array(" nextStartingLimit" => count($friends), "data" => $data))));
		$this -> db -> close();
	}

	// private function

	private function check_friend($userID, $fanID) {
		$where = '(userId = ' . $userID . ' AND fanId = ' . $fanID . ' AND isFriend = 1 ) OR (userId = ' . $fanID . ' AND fanId = ' . $userID . ' AND isFriend = 1 )';
		$is_friends = $this -> main_model -> getData('userfans', 'id', $where);
		if ($is_friends)
			return TRUE;
		else
			return FALSE;
	}

	private function getUserFriends($userID = 0) {
		$sql = "(select fanId  from userfans where userId =" . $userID . " AND isFriend = 1 ) union (select userId from userfans where fanId = " . $userID . " AND isFriend = 1) ";

		$query = $this -> db -> query($sql);
		$res = $query -> result_array();
		if (count($res) > 0) {
			foreach ($res as $friend) {
				$ids[] = $friend['fanId'];

			}
			return $ids;
		}
		return FALSE;
	}

	private function send_notification($userID, $msg) {
		$where = 'userId = ' . $userID;
		$user = $this -> main_model -> getData('user', 'userId, pushnotificationtoken , devicetype', $where);
		if ($user) {
			$device_type = $user[0] -> devicetype;
			$device_token = $user[0] -> pushnotificationtoken;
			$this -> load -> library('notification_lib');
			if ($device_type == 'ios') {
				$this -> notification_lib -> push_iphone($device_token, $msg, "", "", "", "", "", "", $userID);
			} else if ($device_type == 'android') {
				$this -> notification_lib -> push_android($device_token, $msg, "", "", "", "", "", "", $userID);
			}

		}
	}

	private function userPic($url) {
		if (strpos($url, 'http') !== FALSE)
			return $url;
		else {
			if (empty($url))
				return '';
			else
				return base_url() . 'third_party/uploads/profile/' . $url;
		}

	}

	public function test_push($token) {
		$this -> load -> library('notification_lib');
		$this -> notification_lib -> push_android($token, "hi moarbe3 how are you");
	}

}
