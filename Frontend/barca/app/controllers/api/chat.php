<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Chat extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	/*
	 public function loadFriends_old()
	 {
	 $json_data = $this->input->post('data');
	 $json_data = '{"userID":"817","filter":"city","startingLimit":0}';
	 $arr_data = json_decode($json_data);
	 $userId = intval($arr_data->userID);
	 $filter = $arr_data->filter;
	 $startingLimit = intval($arr_data->startingLimit);

	 if(!empty($filter) && !empty($userId))
	 {

	 $user = $this->main_model->getData('user','userId,cityId,countryId',array('userId'=>$userId));
	 $cityId = intval($user[0]->cityId);
	 $countryId =intval($user[0]->countryId);

	 $friends_count = $this->get_no_of_friends($userId,$filter,$startingLimit,$cityId,$countryId);
	 if($friends_count==0)
	 {
	 if($filter == 'city')
	 {

	 $filter = 'country';
	 $startingLimit = 0 ;
	 $where = "cityId != ".$cityId.' AND countryId = '.$countryId;
	 $friends_count = $this->get_no_of_friends($userId,$filter,$startingLimit,$cityId,$countryId);
	 if($friends_count == 0)
	 {

	 $filter = 'other';
	 $startingLimit = 0 ;
	 $where = "cityId != ".$cityId.' AND countryId != '.$countryId;
	 $friends_count = $this->get_no_of_friends($userId,$filter,$startingLimit,$cityId,$countryId);

	 $nextLimit =intval($startingLimit)+ $friends_count;

	 $nextfilter = "otdddher";

	 }

	 }

	 if($filter == 'country')
	 {

	 $filter = 'other';
	 $startingLimit = 0 ;
	 $where = "cityId != ".$cityId.' AND countryId != '.$countryId;
	 $friends_count = $this->get_no_of_friends($userId,$filter,$startingLimit,$cityId,$countryId);
	 $nextLimit =intval($startingLimit)+ $friends_count;
	 $nextfilter = "other";

	 }
	 if($filter == 'other')
	 {

	 $where = "cityId != ".$cityId.' AND countryId != '.$countryId;
	 $nextLimit =intval($startingLimit);
	 $nextfilter = "other";

	 }

	 }

	 else
	 {
	 if($filter == 'other')
	 {

	 $where = "cityId != ".$cityId.' AND countryId != '.$countryId;
	 $nextLimit =intval($startingLimit)+$friends_count;
	 $nextfilter = "other";

	 }
	 }

	 $select = 'countryId , cityId , fullname as friendName , userId as friendID , isOnline as friendOnline , isPremium , profilePic as friendPicture ,  noOfPics as friendPicNumber  ,(select isFollow from userfans where userId = '.$userId.' AND fanId = userId  ) as isFollowed ,  (select isFavourite from userfans where userId = '.$userId.' AND fanId = userId  ) as isFavorite';
	 $getfriendsIds = $this->getUserFriends($userId);

	 $additional_params['where_in'] = array('userId',$getfriendsIds) ;
	 echo $where;

	 $friends = $this->main_model->getBackendData('user', $select , $where , '' , '' , 50 , $startingLimit , $additional_params);
	 $data = array();
	 if($friends){
	 foreach ($friends as $friend) {
	 $friend -> friendID = intval($friend -> friendID );
	 $friend -> friendOnline = intval($friend -> friendOnline );
	 $friend -> isPremium = intval($friend -> isPremium );
	 $friend -> friendPicNumber = intval($friend -> friendPicNumber );
	 $friend -> isFollowed = intval($friend -> isFollowed );
	 $friend -> isFavorite = intval($friend -> isFavorite );
	 $friend->friendPicture = $this->userPic($friend->friendPicture);
	 $data[] = $friend;
	 }
	 }

	 print_r(json_encode(array("nextFilter"=>$nextfilter,"nextStartingLimit"=>$nextLimit,"data"=>$data)));

	 }
	 else
	 {
	 print_r(json_encode(array('status'=>-1)));
	 }
	 }

	 */
	public function loadFriends() {
		$json_data = $this -> input -> post('data');

		//$json_data = '{"userID":"711","filter":"city","startingLimit":0}';
		//$json_data ='{"userID" : "711","filter" : "city","startingLimit" : "0"}';
		$arr_data = json_decode($json_data);
		$userId = intval($arr_data -> userID);
		$filter = $arr_data -> filter;
		$startingLimit = intval($arr_data -> startingLimit);

		if (!empty($userId)) {

			$user = $this -> main_model -> getData('user', 'userId,cityId,countryId', array('userId' => $userId));
			$cityId = intval($user[0] -> cityId);
			$countryId = intval($user[0] -> countryId);

			$select = 'countryId , cityId , fullname as friendName , userId as friendID , isOnline as friendOnline , isPremium , profilePic as friendPicture ,  noOfPics as friendPicNumber  ,(select isFollow from userfans where userId = ' . $userId . ' AND fanId = friendID limit 1  ) as isFollowed ,  (select isFavourite from userfans where userId = ' . $userId . ' AND fanId = friendID limit 1 ) as isFavorite';
			$getfriendsIds = $this -> getUserFriends($userId);
			if ($getfriendsIds)
				$ids = join(',', $getfriendsIds);
			else
				$ids = 0;
			$sql = "select " . $select . " from user where userId in ($ids)  order by field (cityid,$cityId) desc,field (countryId,$countryId) desc limit $startingLimit,50";
			$query = $this -> db -> query($sql);
			$friends = $query -> result();

			$data = array();
			$friendrequest = $this -> main_model -> count('userfans', 'fanId = ' . $userId . ' AND friendRequest = 1 AND isFriend = 0');

			if ($friends) {
				foreach ($friends as $friend) {
					$friend -> friendID = intval($friend -> friendID);
					$friend -> friendOnline = intval($friend -> friendOnline);
					$friend -> isPremium = intval($friend -> isPremium);
					$friend -> friendPicNumber = intval($friend -> friendPicNumber);
					$friend -> isFollowed = intval($friend -> isFollowed);
					$friend -> isFavorite = intval($friend -> isFavorite);
					$friend -> friendPicture = $this -> userPic($friend -> friendPicture);
					unset($friend -> cityId);
					unset($friend -> countryId);
					$data[] = $friend;
				}
			}

			print_r(json_encode(array("status" => 2, "data" => array(" friendRequests" => intval($friendrequest), "data" => $data))));
		} else {
			print_r(json_encode(array('status' => -1)));
		}
	}

	public function loadMessage() {
		$json_data = $this -> input -> post('data');

		//  $json_data = '{"myID":"711","lastTimeStampChecked":"1421068949"}';
		$arr_data = json_decode($json_data);
		$myID = intval($arr_data -> myID);
		$lastTimeStampChecked = $arr_data -> lastTimeStampChecked;

		if (!empty($myID)) {
			$select = 'm.messageID as messageRemoteID,m.type  , m.senderUserID as senderUserID, m.messageText , m.sentAt , u.isPremium , u.fullname as senderName , u.isOnline as senderOnline , u.profilePic   as senderPicture , (select isFavourite from userfans where userId = ' . $myID . ' AND fanId = senderUserID limit 1 )  as isFavorite , (select isFollow from userfans where userId = ' . $myID . ' AND fanId = senderUserID limit 1 )  as isFollowed';
			if (!empty($lastTimeStampChecked))
				$where = 'mr.userID = ' . $myID . ' AND m.sentAt > "' . date('Y-m-d H:i:s', $lastTimeStampChecked) . '"';
			else
				$where = 'mr.userID = ' . $myID;
			$additional_params['join'] = array('messagerecivers as mr', 'mr.messageID = m.messageID', 'left');
			$additional_params['join2'] = array('user as u', 'm.senderUserID = u.userId', 'left');
			$messages = $this -> main_model -> getBackendData('messages as m', $select, $where, 'm.messageID', 'ASC', '', '', $additional_params);
			if ($messages) {
				foreach ($messages as $message) {
					$message -> messageRemoteID = intval($message -> messageRemoteID);
					$message -> senderUserID = intval($message -> senderUserID);
					$message -> isPremium = intval($message -> isPremium);
					$message -> senderOnline = intval($message -> senderOnline);
					$message -> isFavorite = intval($message -> isFavorite);
					$message -> isFollowed = intval($message -> isFollowed);
					$message -> sentAt = strtotime($message -> sentAt);

					if (strpos($message -> senderPicture, 'http') !== FALSE)
						$message -> senderPicture = $message -> senderPicture;
					else {
						$message -> senderPicture = base_url() . 'third_party/uploads/profile/' . $message -> senderPicture;
					}
					if (empty($message -> senderPicture))
						$message -> senderPicture = '';

					$data[] = $message;

					//if($message->type == 1)
					//$this->main_model->delete('messages',array('messageID'=>$message->messageRemoteID));

				}
				print_r(json_encode(array('status' => 2, 'data' => $data)));
			} else {
				print_r(json_encode(array('status' => 2, 'data' => array())));
			}
		} else {
			print_r(json_encode(array('status' => -1)));
		}
		$this->db->close();
	}

	public function sendMessage() {
		$json_data = $this -> input -> post('data');

		//$json_data = '{"myID":"1437","fanID":"1568","messageText":"Bbv"}';
		$arr_data = json_decode($json_data);
		$myID = intval($arr_data -> myID);
		$fanID = intval($arr_data -> fanID);
		$messageText = $arr_data -> messageText;

		if (empty($myID) && empty($fanID) && empty($messageText)) {
			print_r(json_encode(array('status' => -1)));
			$this->db->close();
			exit ;
		}
		$is_friend = $this -> check_friend($myID, $fanID);
		$storeTime = date('Y-m-d H:i:s');
		if (!$is_friend) {

			//$where = 'senderUserID = ' . $myID . ' AND userID = ' . $fanID;

			//$where = "";
			//$additional_params['join'] = array('messagerecivers as mr', 'mr.messageID = m.messageID', 'left');
			//$message_no = $this -> main_model -> getBackendData('messages as m', 'm.messageID , m.senderUserID , mr.userID ', $where, 'm.messageID', 'ASC', '', '', $additional_params);
			$insert_msg = $this -> main_model -> insert('messages', array('messageText' => $messageText, 'senderUserID' => $myID, 'recieverUserID' => $fanID, 'type' => 0, 'sentAt' => $storeTime));
			$this -> main_model -> insert('messagerecivers', array('messageID' => $insert_msg, 'userID' => $fanID));

			$this -> send_notification($fanID, $myID, $insert_msg, $messageText, $storeTime);
			print_r(json_encode(array('status' => 2, "data" => array('storedMessageID' => $insert_msg, 'storedTime' => strtotime($storeTime)))));

		}

		// if is friend for us
		else {

			$insert_msg = $this -> main_model -> insert('messages', array('messageText' => $messageText, 'senderUserID' => $myID, 'recieverUserID' => $fanID, 'type' => 0, 'sentAt' => $storeTime));
			$this -> main_model -> insert('messagerecivers', array('messageID' => $insert_msg, 'userID' => $fanID));
			print_r(json_encode(array('status' => 2, "data" => array('storedMessageID' => $insert_msg, 'storedTime' => strtotime($storeTime)))));

			$this -> send_notification($fanID, $myID, $insert_msg, $messageText, $storeTime);
		}
		$this->db->close();
	}

	private function send_notification($userID, $senderID, $messageID, $msg, $time) {
		$where = 'userId = ' . $userID;
		$user = $this -> main_model -> getData('user', 'userId, pushnotificationtoken , devicetype', $where);
		if ($user) {

			$where = 'userId = ' . $senderID;
			$user2 = $this -> main_model -> getData('user', 'userId, fullName, profilePic, pushnotificationtoken , devicetype', $where);

			$device_type = $user[0] -> devicetype;
			$device_token = $user[0] -> pushnotificationtoken;

			$haystack = $user2[0] -> profilePic;
			$needle = 'http';

			if (strpos($haystack, $needle) !== false) {
				$imgPath = $haystack;
			} else {
				$imgPath = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/profiles/" . $haystack;
			}
			$name = $user2[0] -> fullName;
			$sendAt = strtotime($time);

			$this -> load -> library('notification_lib');
			if ($device_type == 2) {
				$this -> notification_lib -> push_iphone($device_token, $msg, 1, $messageID, $name, $imgPath, $senderID, $sendAt, $userID);
			} else if ($device_type == 1) {
				$this -> notification_lib -> push_android($device_token, $msg, 1, $messageID, $name, $imgPath, $senderID, $sendAt, $userID);
			}
		}
	}

	private function check_friend($userID, $fanID) {
		$where = '(userId = ' . $userID . ' AND fanId = ' . $fanID . ' AND isFriend = 1 ) OR (userId = ' . $fanID . ' AND fanId = ' . $userID . ' AND isFriend = 1 )';
		$is_friends = $this -> main_model -> getData('userfans', '*', $where);
		if ($is_friends)
			return TRUE;
		else
			return FALSE;
	}

	//End of Send Message

	public function deleteMessage() {
		$json_data = $this -> input -> post('data');

		//  $json_data = '{"userId":"1","pageNo":"4"}';
		$arr_data = json_decode($json_data);
		$messageID = intval($arr_data -> messageID);

		if (!empty($messageID)) {
			$this -> main_model -> delete('messages', array('messageID' => $messageID));
			$this -> main_model -> delete('messagerecivers', array('messageID' => $messageID));
			print_r(json_encode(array('status' => 1)));
		} else
			print_r(json_encode(array('status' => -1)));
		$this->db->close();
	}

	public function changeStatus() {
		$json_data = $this -> input -> post('data');

		//  $json_data = '{"userId":"1","pageNo":"4"}';
		$arr_data = json_decode($json_data);
		$myID = intval($arr_data -> myID);
		$online = intval($arr_data -> online);

		if (!empty($myID)) {
			$update = $this -> main_model -> update('user', array('isOnline' => $online), array('userId' => $myID));
			if ($update)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -2)));
		} else
			print_r(json_encode(array('status' => -1)));
		$this->db->close();
	}

	public function loadChatContacts() {
		$json_data = $this -> input -> post('data');

		//  $json_data ='{"userID" : "817","fanID":55 ,"startingLimit":0}';
		$arr_data = json_decode($json_data);

		$nextStartingLimit = intval($arr_data -> nextStartingLimit);
		$userId = intval($arr_data -> userId);

		if (empty($userId)) {

			print_r(json_encode(array('status' => -1)));
			$this->db->close();
			exit ;
		}

		$order = "noOfPics desc";
		$select = ' fullname as fanName , userId as fanID , isOnline as fanOnline , isPremium , profilePic as fanPicture ,  noOfPics as fanPicNumber  ,(select isFollow from userfans where userId = ' . $userId . ' AND userfans.fanId = user.userId limit 1  ) as isFollowed ,  (select isFavourite from userfans where userId = ' . $userId . ' AND fanId = user.userId  limit 1 ) as isFavorite';
		$sql = "select " . $select . " from user   order by " . $order . " limit 0,50";
		$query = $this -> db -> query($sql);
		$friends = $query -> result();

		$data = array();

		if ($friends) {
			foreach ($friends as $friend) {
				$friend -> fanID = intval($friend -> fanID);
				$friend -> fanOnline = intval($friend -> fanOnline);
				$friend -> isPremium = intval($friend -> isPremium);
				$friend -> fanPicture = $this -> userPic($friend -> fanPicture);
				$friend -> isFollowed = intval($friend -> isFollowed);
				$friend -> isFavorite = intval($friend -> isFollowed);
				$friend -> fanPicNumber = intval($friend -> fanPicNumber);
				$friend -> isFriend = intval($friend -> isFriend);
				$friend -> contactName = "contact" . $friend -> fanID;

				$data[] = $friend;
			}
		}

		print_r(json_encode(array("status" => 2, "data" => $data)));
		$this->db->close();
	}

	// private function

	private function getUserFriends($userID = 0) {
		$sql = "(select fanId  from userfans where userId =" . $userID . " AND isFriend = 1 ) union (select userId from userfans where fanId = " . $userID . " AND isFriend = 1) ";

		$query = $this -> db -> query($sql);
		$res = $query -> result_array();
		if (count($res) > 0) {
			foreach ($res as $friend) {
				$ids[] = $friend['fanId'];
			}
			return $ids;
		}
		return FALSE;
	}

	private function userPic($url) {
		if (strpos($url, 'http') !== FALSE)
			return $url;
		else {
			if (empty($url))
				return '';
			else
				return base_url() . 'third_party/uploads/profile/' . $url;
		}
	}

	public function test_push($token) {
		$this -> load -> library('notification_lib');
		$this -> notification_lib -> push_android($token, "hi moarbe3 how are you");
		$this->db->close();
	}
	private function check_For_Block($userID, $fanID) {
		$where = '(userId = ' . $userID . ' AND fanId = ' . $fanID . ' AND isBlocked = 0) OR (userId = ' . $fanID . ' AND fanId = ' . $userID . ' AND isBlocked = 0 )';
		$is_friends = $this -> main_model -> getData('userfans', '*', $where);
		if ($is_friends)
			return TRUE;
		else
			return FALSE;
	}
	
	/*
	 private function get_no_of_friends($userId,$filter,$startlimit,$cityId,$countryId)
	 {

	 if($filter == 'city')
	 $where = "cityId = ".$cityId;
	 else if($filter=='country')
	 $where = "cityId != ".$cityId.' AND countryId = '.$countryId;
	 else
	 $where = "cityId != ".$cityId.' AND countryId != '.$countryId;

	 $select = ' userId as friendID ,cityId ,countryId' ;
	 $getfriendsIds = $this->getUserFriends($userId);

	 $additional_params['where_in'] = array('userId',$getfriendsIds) ;

	 $friends = $this->main_model->getBackendData('user', $select , $where , '' , '' , 50 , $startlimit , $additional_params);
	 if($friends)
	 return count($friends);
	 else
	 return 0 ;

	 }
	 */
}
