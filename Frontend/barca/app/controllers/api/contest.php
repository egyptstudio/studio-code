<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Contest extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function GetContestPrizes() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$lang = $arr_data -> lang;
		if(empty($lang))
		{
			print_r(json_encode(array('status' => -1)));
			return false;
		}
		$additionalParams['join'] = array('contest as c', 'c.contestId = cp.contestId', 'left');
		$prizes = $this -> main_model -> getBackendData('contestPrize as cp', 'cp.*,c.name as contestName , c.status', array('c.status' => 2), '', '', '', '', $additionalParams);
		if ($prizes) {
			foreach ($prizes as $prize) {
				if($lang != 'en')
				{
					$id = intval($prize -> contestPrizeId);
					$where = "prizeId = ".$id." AND lang = '".$lang."'";
					$prizeTrans = $this->main_model->getData('contest_prize_trans','*', $where);
				}
				unset($prize -> status);
				unset($prize -> winnerPostId);
				$prize -> contestId = intval($prize -> contestId);
				$prize -> prizeRank = intval($prize -> prizeRank);
				$prize -> contestPrizeId = intval($prize -> contestPrizeId);
				if($lang == 'en') {
					$prize -> prizeText = strval($prize -> prizeText);
				} else {
					$prize -> prizeText = strval($prizeTrans[0] -> prizeTransText);
				}
				$prize -> prizeImageUrl = strval($prize -> prizeImageUrl);

				$data[] = $prize;
			}
			print_r(json_encode(array('status' => 2, "data" => $data)));

		} else
		print_r(json_encode(array('status' => -3, 'ruleName' => 'noRunningContest', 'errorMessage' => 'There is no running contest right now')));
		$this->db->close();
	}

	public function GetContestRules() {
		//$lang = strtoupper($lang);
		print_r(json_encode(array('status' => 2, "data" => array("link" => "https://www.fcbstudio.mobi/info/".$lang."/contest.html"))));
		$this->db->close();
		
	}

	public function GetContest() {
		$json_data = $this -> input -> post('data');
		//$json_data = '{"userID":"711","filter":"city","startingLimit":0}';
		//$json_data ='{"userID" : "711","filter" : "city","startingLimit" : "0"}';
		$arr_data = json_decode($json_data);
		$userId = intval($arr_data -> userId);
		$lang = $arr_data -> lang;
		if (empty($userId))
			$userId = 0;
		$contest = $this -> main_model -> getData('contest', '*', '', 'contestId', 'desc');

		if ($contest) {

			$additionalParams['join'] = array('post p', 'p.postId=cp.winnerPostId', 'left');
			$prizes = $this -> main_model -> getBackendData('contestPrize as cp', 'cp.*,p.userId as winnerId', array('contestId' => $contest[0] -> contestId), '', '', '', '', $additionalParams);
			if ($prizes) {
				foreach ($prizes as $prize) {
					if($lang != 'en')
					{
						$id = intval($prize -> contestPrizeId);
						$where = "prizeId = ".$id." AND lang = '".$lang."'";
						$prizeTrans = $this->main_model->getData('contest_prize_trans','*', $where);
					}
					unset($prize -> winnerPostId);
					$prize -> prizeRank = intval($prize -> prizeRank);
					$prize -> contestPrizeId = intval($prize -> contestPrizeId);
					$prize -> winnerId = intval($prize -> winnerId);
					if($lang == 'en') {
						$prize -> prizeText = strval($prize -> prizeText);
					} else {
						$prize -> prizeText = strval($prizeTrans[0] -> prizeTransText);
					}
					$prize -> prizeImageUrl = strval($prize -> prizeImageUrl);

					$data_prizes[] = $prize;
				}
			} else
			$data_prizes = array();

			$contest[0] -> contestId = intval($contest[0] -> contestId);
			$contest[0] -> status = intval($contest[0] -> status);
			$contest[0] -> name = strval($contest[0] -> name);

			$contest[0] -> startDate = strtotime($contest[0] -> startDateTime);
			$contest[0] -> endDate = strtotime($contest[0] -> endDateTime);

			$contestdata = array('contestId' => $contest[0] -> contestId, 'name' => $contest[0] -> name, 'startDate' => $contest[0] -> startDate, 'endDate' => $contest[0] -> endDate, 'status' => $contest[0] -> status);
			if ($contest[0] -> status != 4) {
				$data_winners = array();
			} else if ($contest[0] -> status == 4) {
				$additionalParams1['join'] = array('post p', 'p.postId=cp.winnerPostId', '');
				$additionalParams1['join2'] = array('user u', 'p.userId=u.userId', '');

				$winners = $this -> main_model -> getBackendData('contestPrize cp', 'cp.winnerPostId,p.userId as winnerId,u.fullname as winnerName , u.isPremium , u.isOnline,u.profilePic as profilePicUrl ,u.noOfPics , (select isFollow from userfans where userId = ' . $userId . ' AND userfans.fanId = p.userId limit 1  ) as isFollowed ,  (select isFavourite from userfans where userId = ' . $userId . ' AND fanId = p.userId  limit 1 ) as isFavorite ', array('cp.contestId' => $contest[0] -> contestId), 'prizeRank', 'asc', '', '', $additionalParams1);
				if ($winners) {
					foreach ($winners as $winner) {
						unset($winner -> winnerPostId);
						$winner -> winnerId = intval($winner -> winnerId);
						$winner -> isPremium = intval($winner -> isPremium);
						$winner -> isOnline = intval($winner -> isOnline);
						$winner -> noOfPics = intval($winner -> noOfPics);
						$winner -> isFollowed = intval($winner -> isFollowed);
						$winner -> isFavorite = intval($winner -> isFavorite);
						$winner -> profilePicUrl = $this -> userPic($winner -> profilePicUrl);
						$winner -> isFriend = intval($this -> check_friend($userId, $winner -> winnerId));

						$data_winners[] = $winner;
					}

				} else
				$data_winners = array();
			}
			if($lang == 'en')
			{
				$contestRules = "https://www.fcbstudio.mobi/info/en/Contest.html";
			} else
			{
				//$lang = strtoupper($lang);
				$contestRules = "https://www.fcbstudio.mobi/info/".$lang."/Contest.html";
			}

			print_r(json_encode(array('status' => 2, "data" => array('Contest' => $contestdata, 'ContestPrize' => $data_prizes, 'ContestWinner' => $data_winners, 'contestRules' => $contestRules))));

		} else

		print_r(json_encode(array('status' => -3, 'ruleName' => 'noContest', 'errorMessage' => 'There is no running contest')));
		$this->db->close();
	}

	private function userPic($url) {
		if (strpos($url, 'http') !== FALSE)
			return $url;
		else {
			if (empty($url))
				return '';
			else
				return base_url() . 'third_party/uploads/profile/' . $url;
		}

	}

	private function check_friend($userID, $fanID) {
		$where = '(userId = ' . $userID . ' AND fanId = ' . $fanID . ' AND isFriend = 1 ) OR (userId = ' . $fanID . ' AND fanId = ' . $userID . ' AND isFriend = 1 )';
		$is_friends = $this -> main_model -> getData('userfans', '*', $where);
		if ($is_friends)
			return TRUE;
		else
			return FALSE;
	}

}
