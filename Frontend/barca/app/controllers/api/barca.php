<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Barca extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function asd22() {
		echo "sssss";
		$email = "ehab.adel3@gmail.com";
		$this -> load -> library('email');

		$config = array();

		$config['protocol'] = "smtp";
		$config['smtp_host'] = "localhost";
		$config['smtp_port'] = "25";
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['wordwrap'] = TRUE;
		$this -> email -> initialize($config);
		//$this->email->from('noreply@MoraselMobileApp.com', 'Morasel Mobile Application');
		$this -> email -> from('info@tawasolit.com', 'Tawasol');
		$this -> email -> to($email);

		$this -> email -> subject('Barcelona - Reset password');
		$this -> email -> message('<html><body><div style="text-align:left;">Dear Sir, <br /> The verification code is : ' . $encrypted_string . '  </div></body></html>');

		$this -> email -> send();

	}

	public function getCountriesList() {
		$countries = $this -> main_model -> getData('country', '*');
		if ($countries)
			print_r(json_encode(array('status' => 2, 'data' => $countries)));
		else
			print_r(json_encode(array('status' => 2, 'data' => array())));
		$this->db->close();
	}

	#############################################################################
	/* ====================== User MGMT ==================================== */
	#############################################################################

	public function register() {
		//file_put_contents('test.txt', serialize($_POST));
		$json_data = $this -> input -> post('data');

		$arr_data = json_decode($json_data);
		$fullName = $arr_data -> fullName;
		$password = $arr_data -> password;
		$email = $arr_data -> email;
		/*
		 $password="asdasd";
		 $email="ehab@ehab.com";
		 $fullName="ehab adel";
		 //$image = $_FILES['img'];

		 $username="ddd";
		 $password="vvv";
		 $email="ddd";
		 $dateOfBirth="xxx";
		 $gender="v";
		 $countryId="5";
		 */
		/*
		 $fullName = 'ehab';
		 $password = '123456';
		 $email = 'ehab@tw.com';
		 */
		if (empty($fullName) || empty($password) || empty($email))
			print_r(json_encode(array('status' => -1)));
		else {
			$check_user = $this -> main_model -> getData('user', 'userId', array('email' => $email));
			if ($check_user)
				print_r(json_encode(array('status' => -3, 'ruleName' => 'UserExists', 'errorMessage' => 'Email already exists!”')));
			
else {

				if (is_uploaded_file($_FILES['img']['tmp_name'])) {
					$image = $_FILES['img'];
					$extension = end(explode('.', $image['name']));
					$updated_name = time() . '.' . $extension;

					$doUpload = @move_uploaded_file($image["tmp_name"], FCPATH . "third_party/uploads/profile/" . $updated_name);

					if (!$doUpload)
						print_r(json_encode(array('status' => -2)));
					else {
						//$user = $this->main_model->update('user',array('profilePic'=>$updated_name),array('userId'=>$userId));
						$user = $this -> main_model -> insert('user', array('fullName' => $fullName, 'profilePic' => $updated_name, 'password' => md5($password), 'email' => $email, 'userType' => '1'));
						if ($user)
							print_r(json_encode(array('status' => 1)));
						else
							print_r(json_encode(array('status' => -2)));
					}
				} else {

					$user = $this -> main_model -> insert('user', array('fullName' => $fullName, 'password' => md5($password), 'email' => $email, 'userType' => '1'));
					if ($user)
						print_r(json_encode(array('status' => 1)));
					else
						print_r(json_encode(array('status' => -2)));
				}
			}
		}
		$this->db->close();
	}

	public function login() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$email = $arr_data -> email;
		$password = $arr_data -> password;
		/*
		 $password="qqqq";
		 $email="ttt@ttt.com";
		 */
		if (empty($email) || empty($password))
			print_r(json_encode(array('status' => -1)));
		else {
			$additionalparam['join'] = array('country', 'country.countryId=user.countryId', 'left');
			$user_login = $this -> main_model -> getBackendData('user', 'user.*,country.countryName as countryText', array('email' => $email, 'password' => md5($password)), '', '', '', '', $additionalparam);
			if ($user_login) {
				// contact api for ejapard servser chat
				if ($user_login[0] -> profilePic == '')
					unset($user_login[0] -> profilePic);
				else
				{
					if (strpos($user_login[0] -> profilePic, 'http') === FALSE)
						$user_login[0] -> profilePic = base_url() . 'third_party/uploads/profile/' . $user_login[0] -> profilePic;
				}
				unset($user_login[0] -> password);
				//unset($user_login[0]->isOnline);
				//unset($user_login[0]->lastLoginDate);

				if (empty($user_login[0] -> fullName))
					$user_login[0] -> fullName = '';
				if (empty($user_login[0] -> dateOfBirth))
					$user_login[0] -> dateOfBirth = '';
				if (empty($user_login[0] -> gender))
					$user_login[0] -> gender = 0;
				if (empty($user_login[0] -> countryId))
					$user_login[0] -> countryId = 0;
				if (empty($user_login[0] -> lastLoginDate))
					$user_login[0] -> lastLoginDate = '';
				if (empty($user_login[0] -> noOfPics))
					$user_login[0] -> noOfPics = 0;
				if (empty($user_login[0] -> userType))
					$user_login[0] -> userType = 0;
				if (empty($user_login[0] -> brief))
					$user_login[0] -> brief = '';

				if (empty($user_login[0] -> socialMediaID))
					$user_login[0] -> socialMediaID = '';

				$user_login[0] -> userType = intval($user_login[0] -> userType);
				$user_login[0] -> noOfPics = intval($user_login[0] -> noOfPics);
				$user_login[0] -> countryId = intval($user_login[0] -> countryId);
				$user_login[0] -> gender = intval($user_login[0] -> gender);
				$user_login[0] -> isPremium = intval($user_login[0] -> isPremium);
				$user_login[0] -> isOnline = intval($user_login[0] -> isOnline);
				$user_login[0] -> latitude = floatval($user_login[0] -> latitude);
				$user_login[0] -> longitude = floatval($user_login[0] -> longitude);

				// update for returned counters

				$followingNo = $this -> main_model -> count('userfans', array('userId' => $user_login[0] -> userId, 'isFollow' => "1"));

				$followersNo = $this -> main_model -> count('userfans', array('fanId' => $user_login[0] -> userId, 'isFollow' => "1"));
				$user_login[0] -> followingNo = ($followingNo);
				$user_login[0] -> followersNo = ($followersNo);

				$update = $this -> main_model -> update('user', array('lastLoginDate' => date('Y-m-d H:i:s'), 'isOnline' => '1'), array('userId' => $user_login[0] -> userId));
				if ($update)
					//print_r(json_encode(array('status'=>2,'data'=>array('user'=>$user_login[0],'followingNo'=>$followingNo,'followersNo'=>$followersNo))));
					print_r(json_encode(array('status' => 2, 'data' => $user_login[0])));
				
else
					print_r(json_encode(array('status' => -1, 'data' => $user_login[0])));

			} else
				print_r(json_encode(array('status' => -3, 'ruleName' => 'loginFailed', 'errorMessage' => 'User name or Password is wrong')));
		}
	}

	public function login_facebook() {
		$this -> _login_social();
	}

	public function login_instagram() {
		$this -> _login_social();
	}

	private function _login_social() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$email = $arr_data -> email;
		$socialMediaId = $arr_data -> socialMediaId;
		$fullName = $arr_data -> fullName;

		//$email='asdxzzavsd@nbnb.ccc';
		if (empty($email))
			print_r(json_encode(array('status' => -1)));
		else {
			$user_login = $this -> main_model -> getData('user', '*', array('email' => $email));
			if ($user_login) {
				if (empty($user_login[0] -> profilePic))
					unset($user_login[0] -> profilePic);
				else
				{
					if (strpos($user_login[0] -> profilePic, 'http') === FALSE)
						$user_login[0] -> profilePic = base_url() . 'third_party/uploads/profile/' . $user_login[0] -> profilePic;
				}
				unset($user_login[0] -> password);
				//unset($user_login[0]->isOnline);
				//unset($user_login[0]->lastLoginDate);

				if (empty($user_login[0] -> fullName))
					$user_login[0] -> fullName = '';
				if (empty($user_login[0] -> dateOfBirth))
					$user_login[0] -> dateOfBirth = '';
				if (empty($user_login[0] -> gender))
					$user_login[0] -> gender = 0;
				if (empty($user_login[0] -> countryId))
					$user_login[0] -> countryId = 0;
				if (empty($user_login[0] -> lastLoginDate))
					$user_login[0] -> lastLoginDate = '';
				if (empty($user_login[0] -> noOfPics))
					$user_login[0] -> noOfPics = 0;
				if (empty($user_login[0] -> userType))
					$user_login[0] -> userType = '';
				if (empty($user_login[0] -> brief))
					$user_login[0] -> brief = '';
				if (empty($user_login[0] -> socialMediaID))
					$user_login[0] -> socialMediaID = '';

				$user_login[0] -> userType = intval($user_login[0] -> userType);
				$user_login[0] -> noOfPics = intval($user_login[0] -> noOfPics);
				$user_login[0] -> countryId = intval($user_login[0] -> countryId);
				$user_login[0] -> gender = intval($user_login[0] -> gender);
				$user_login[0] -> isOnline = intval($user_login[0] -> isOnline);
				$user_login[0] -> isPremium = intval($user_login[0] -> isPremium);
				$user_login[0] -> latitude = floatval($user_login[0] -> latitude);
				$user_login[0] -> longitude = floatval($user_login[0] -> longitude);

				$followingNo = $this -> main_model -> count('userfans', array('userId' => $user_login[0] -> userId, 'isFollow' => "1"));
				$followersNo = $this -> main_model -> count('userfans', array('fanId' => $user_login[0] -> userId, 'isFollow' => "1"));
				$user_login[0] -> followingNo = ($followingNo);
				$user_login[0] -> followersNo = ($followersNo);
				$update = $this -> main_model -> update('user', array('lastLoginDate' => date('Y-m-d H:i:s'), 'isOnline' => '1'), array('userId' => $user_login[0] -> userId));
				if ($update)
					print_r(json_encode(array('status' => 2, 'data' => $user_login[0])));
				else
					print_r(json_encode(array('status' => -1, 'data' => $user_login[0])));
			} else {
				$inserted_arr = array('lastLoginDate' => date('Y-m-d H:i:s'), 'isOnline' => '1');
				if (!empty($fullName))
					$inserted_arr['fullName'] = $fullName;
				if (!empty($email))
					$inserted_arr['email'] = $email;
				if (!empty($socialMediaID))
					$inserted_arr['socialMediaID'] = $socialMediaID;

				$insert = $this -> main_model -> insert('user', $inserted_arr);
				if ($insert) {
					$user_data = $this -> main_model -> getData('user', '', array('userId' => $insert));
					if ($user_data) {
						if (empty($user_data[0] -> profilePic))
							unset($user_data[0] -> profilePic);
						else
						{
							if (strpos($user_data[0] -> profilePic, 'http') === FALSE)
								$user_data[0] -> profilePic = base_url() . 'third_party/uploads/profile/' . $user_data[0] -> profilePic;
						}
						unset($user_data[0] -> password);

						if (empty($user_data[0] -> fullName))
							$user_data[0] -> fullName = '';
						if (empty($user_data[0] -> dateOfBirth))
							$user_data[0] -> dateOfBirth = '';
						if (empty($user_data[0] -> gender))
							$user_data[0] -> gender = 0;
						if (empty($user_data[0] -> countryId))
							$user_data[0] -> countryId = 0;
						if (empty($user_data[0] -> lastLoginDate))
							$user_data[0] -> lastLoginDate = '';
						if (empty($user_data[0] -> noOfPics))
							$user_data[0] -> noOfPics = 0;
						if (empty($user_data[0] -> userType))
							$user_data[0] -> userType = 0;
						if (empty($user_data[0] -> socialMediaID))
							$user_data[0] -> socialMediaID = '';
						if (empty($user_data[0] -> brief))
							$user_data[0] -> brief = '';

						$user_data[0] -> userType = intval($user_data[0] -> userType);
						$user_data[0] -> noOfPics = intval($user_data[0] -> noOfPics);
						$user_data[0] -> countryId = intval($user_data[0] -> countryId);
						$user_data[0] -> gender = intval($user_data[0] -> gender);
						$user_data[0] -> isOnline = 1;

						$followingNo = $this -> main_model -> count('userfans', array('userId' => $user_login[0] -> userId, 'isFollow' => "1"));
						$followersNo = $this -> main_model -> count('userfans', array('fanId' => $user_login[0] -> userId, 'isFollow' => "1"));
						$user_login[0] -> followingNo = ($followingNo);
						$user_login[0] -> followersNo = ($followersNo);
						print_r(json_encode(array('status' => 2, 'data' => $user_data[0])));
					}
				} else
					print_r(json_encode(array('status' => 2, 'data' => array())));
			}
		}
		$this->db->close();
	}

	public function forgetPassword() {
		$this -> load -> helper('string');
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$email = $arr_data -> email;
		//$email = 'ehab.adel3@gmail.com';
		if (!empty($email)) {
			$check_email = $this -> main_model -> getData('user', 'userId,email', array('email' => $email));
			if ($check_email) {
				$code = random_string('alnum', 12);
				$user_code = $this -> main_model -> insert('user_codes', array('code' => $code, 'userId' => $check_email[0] -> userId));
				if ($user_code) {
					$encrypted_string = $code;
					$this -> load -> library('email');

					$config = array();

					$config['protocol'] = "smtp";
					$config['smtp_host'] = "localhost";
					$config['smtp_port'] = "25";
					$config['mailtype'] = 'html';
					$config['charset'] = 'utf-8';
					$config['newline'] = "\r\n";
					$config['wordwrap'] = TRUE;
					$this -> email -> initialize($config);
					//$this->email->from('noreply@MoraselMobileApp.com', 'Morasel Mobile Application');
					$this -> email -> from('info@fcbstudio.mobi', 'FCB Studio');
					$this -> email -> to($email);

					$this -> email -> subject('Barcelona - Reset password');
					$this -> email -> message('<html><body><div style="text-align:left;">Dear Sir, <br /> To change your password please click <a href=' . base_url() . 'index.php/user/changePassword/' . $encrypted_string . '> here </a>  </div></body></html>');

					$this -> email -> send();

					print_r(json_encode(array('status' => 1)));
				} else
					print_r(json_encode(array('status' => -2)));
			} else

				print_r(json_encode(array('status' => -3, 'ruleName' => 'emailNotExist', 'errorMessage' => 'email Not Exist')));

		} else
			print_r(json_encode(array('status' => -1)));
		$this->db->close();
	}

	public function resetPassword() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$code = $arr_data -> code;
		$password = $arr_data -> password;
		if (empty($code) || empty($password)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$get_code = $this -> main_model -> getData('user_codes', '*', array('code' => $code));
			if ($get_code) {
				$update_password = $this -> main_model -> update('user', array('password' => md5($password)), array('userId' => $get_code[0] -> userId));
				if ($update_password)
					print_r(json_encode(array('status' => 1)));
				else
					print_r(json_encode(array('status' => -2)));
			} else
				print_r(json_encode(array('status' => -2)));
		}
		$this->db->close();
	}

	public function updateProfile() {
		$json_data = $this -> input -> post('data');

		$arr_data = json_decode($json_data);
		$userId = intval($arr_data -> userId);
		$username = $arr_data -> fullName;
		$password = $arr_data -> password;
		$email = $arr_data -> email;
		$dateOfBirth = $arr_data -> dateOfBirth;
		$gender = $arr_data -> gender;
		$countryId = intval($arr_data -> countryId);
		$brief = $arr_data -> brief;

		if (empty($userId))
			print_r(json_encode(array('status' => -1)));
		else {
			if (empty($_FILES['img']['name'])) {
				$user = $this -> main_model -> update('user', array('fullName' => $username, 'email' => $email, 'dateOfBirth' => $dateOfBirth, 'gender' => $gender, 'countryId' => $countryId, 'brief' => $brief), array('userId' => $userId));
				if ($user)
					print_r(json_encode(array('status' => 1)));
				else
					print_r(json_encode(array('status' => -2)));
			} else {
				$image = $_FILES['img'];
				if (isset($image) && $image['error'] == 0) {
					$extension = end(explode('.', $image['name']));
					$updated_name = time() . '.' . $extension;

					$doUpload = @move_uploaded_file($image["tmp_name"], FCPATH . "third_party/uploads/profile/" . $updated_name);
					if (!$doUpload)
						print_r(json_encode(array('status' => -2)));
					else {
						$user = $this -> main_model -> update('user', array('profilePic' => $updated_name, 'userName' => $username, 'password' => md5($password), 'email' => $email, 'dateOfBirth' => $dateOfBirth, 'gender' => $gender, 'countryId' => $countryId, 'brief' => $brief), array('userId' => $userId));

						if ($user)
							print_r(json_encode(array('status' => 1)));
						else
							print_r(json_encode(array('status' => -2)));
					}

				} else {
					print_r(json_encode(array('status' => -2)));
				}
			}
		}
		$this->db->close();
	}

	public function updateStatus() {
		$json_data = $this -> input -> post('data');
		//	$json_data = '{"userId":"1","pageNo":"4"}';
		$arr_data = json_decode($json_data);
		$userId = intval($arr_data -> userId);
		$online = intval($arr_data -> online);
		//if( $online > 1 ||  $online <0  || empty($userId))
		if (($online == 1 || $online == 0) && !empty($userId)) {
			if ($online == 1)
				$res = $this -> main_model -> update('user', array('isOnline' => $online, 'lastLoginDate' => date('Y-m-d H:i:s')), array('userId' => $userId));
			else
				$res = $this -> main_model -> update('user', array('isOnline' => $online), array('userId' => $userId));

			if ($res)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -2)));
		} else {
			print_r(json_encode(array('status' => -1)));
		}
		$this->db->close();
	}

	public function updateLocation() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$userId = intval($arr_data -> userId);
		$lat = $arr_data -> lat;
		$long = $arr_data -> long;
		if (!empty($userId) && !empty($lat) && !empty($long)) {
			$res = $this -> main_model -> update('user', array('latitude' => $lat, 'longitude' => $long), array('userId' => $userId));
			if ($res)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -2)));
		} else {
			print_r(json_encode(array('status' => -1)));
		}
		$this->db->close();
	}

	#############################################################################
	/* ====================== Studio ========================================= */
	#############################################################################

	public function getStudioPhoto() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$photoId = intval($arr_data -> studioPhotoId);
		//$photoId = 3;
		if (empty($photoId))
			print_r(json_encode(array('status' => -1)));
		else {
			$photo = $this -> main_model -> getData('studio_photo', '*', array('imageId' => $photoId), 'uploadedDate desc');
			if ($photo) {
				$output = $additional_params = array();
				unset($photo[0] -> usedCount);
				if ($photo[0] -> photoUrl)
					$photo[0] -> photoUrl = base_url() . 'third_party/uploads/' . $photo[0] -> photoUrl;
				else
					unset($photo[0] -> photoUrl);
				$photo[0] -> imageId = intval($photo[0] -> imageId);
				$photo[0] -> usingCounter = intval($photo[0] -> usingCounter);
				$photo[0] -> photoOrientation = intval($photo[0] -> photoOrientation);
				$photo[0] -> captureType = intval($photo[0] -> captureType);

				$output['photo'] = $photo[0];

				//$additional_params['join']=array('thesaurus','studio_photo_thesaurus.studioPhotoId=thesaurus.thesId','left');
				//$tags = $this->main_model->getBackendData('studio_photo_thesaurus','thesaurus.thesId as tagId,thesaurus.thesName as tagText',array('thesaurus.thesType' => '4'),'','','','',$additional_params);
				$thesID = $this -> main_model -> getData('studio_photo_thesaurus', '*', array('studioPhotoId' => $photo[0] -> imageId));
				$tags = $this -> main_model -> getData('thesaurus', 'thesId as tagId , thesName as tagName', array('thesParentId' => $thesID[0] -> thesId, 'thesType' => '4'));
				$output['tags'] = array();
				if ($tags)
					$output['tags'] = $tags;
				print json_encode(array('status' => 2, 'data' => $output));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	public function getStudioFolders() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$folderId = intval($arr_data -> folderId);
		//$folderId = 1;
		if (empty($folderId))
			print_r(json_encode(array('status' => -1)));
		else {
			$folders = $this -> main_model -> getData('thesaurus', 'thesId as folderId,thesName as folderName,thesPicUrl as folderPicUrl ,rank as playerNumber', array('thesType' => $folderId), 'rank asc,createdDate desc');
			if ($folders) {
				foreach ($folders as $one_folder) {
					$one_folder -> folderId = intval($one_folder -> folderId);
					if ($one_folder -> folderPicUrl)
						$one_folder -> folderPicUrl = base_url() . 'third_party/uploads/' . $one_folder -> folderPicUrl;
				}
				print json_encode(array('status' => 2, 'data' => $folders));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	public function getStudioFolderPhotos() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"1"}';
		$arr_data = json_decode($json_data);

		$folderId = intval($arr_data -> folderId);
		$pageNo = intval($arr_data -> pageNo);
		$sortBy = intval($arr_data -> sortBy);
		$seasons = $arr_data -> seasons;

		if (empty($folderId) || empty($pageNo))
			print_r(json_encode(array('status' => -1)));
		else {
			$additional_params = $output = array();

			$start = ($pageNo - 1) * 24;

			$order_by = 'uploadedDate';
			$order = 'desc';
			if ($sortBy == 2) {
				$order_by = 'usingCounter';
				$order = 'asc';
			}

			if (is_array($seasons))
				$additional_params['where_in'] = array('thesuarus.thesId', implode(',', $seasons));

			$additional_params['join'] = array('studio_photo_thesaurus', 'studio_photo_thesaurus.studioPhotoId=studio_photo.imageId', 'left');
			$additional_params['join2'] = array('thesaurus', 'studio_photo_thesaurus.thesId=thesaurus.thesId', 'left');
			$photos = $this -> main_model -> getBackendData('studio_photo', 'studio_photo.*', array('studio_photo_thesaurus.thesId' => $folderId), 'studio_photo.' . $order_by, $order, 24, $start, $additional_params);

			if ($photos) {
				$additional_params = array();
				foreach ($photos as $one_photo) {
					unset($one_photo -> usedCount);
					if ($one_photo -> photoUrl)
						$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;

					$one_photo -> imageId = intval($one_photo -> imageId);
					$one_photo -> usingCounter = intval($one_photo -> usingCounter);
					$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
					$one_photo -> captureType = intval($one_photo -> captureType);

					$additional_params1 = array('thesaurus', 'studio_photo_thesaurus.studioPhotoId=thesaurus.thesId', 'left');
					$tagsIDs = $this -> main_model -> getBackendData('studio_photo_thesaurus', 'thesId', array('studio_photo_thesaurus.studioPhotoId' => $one_photo -> imageId), '', '', '', '', '');
					if ($tagsIDs) {
						foreach ($tagsIDs as $tagsID)
							$ids[] = $tagsID -> thesId;
					}

					$addition['where_in'] = array('thesId', $ids);
					$tags = $this -> main_model -> getBackendData('thesaurus', 'thesId as tagId ,thesName as tagText', array('thesType' => '4'), '', '', '', '', $addition);

					if (!$tags)
						$tags = array();

					$output[] = array('photo' => $one_photo, 'tags' => $tags);
				}
				print json_encode(array('status' => 2, 'data' => $output));
			} else
				print json_encode(array('status' => 2, 'data' => array()));
		}
		$this->db->close();
	}

	public function getSeasons() {
		$seasons = $this -> main_model -> getData('thesaurus', 'thesId as seasonId,thesName as seasonName', array('thesType' => '3'), 'thesName', 'asc');
		if ($seasons) {
			foreach ($seasons as $one_season) {
				$one_season -> seasonId = intval($one_season -> seasonId);
			}
			print json_encode(array('status' => 2, 'data' => $seasons));
		} else
			print_r(json_encode(array('status' => 2, 'data' => array())));
		$this->db->close();
	}

	#############################################################################
	/* ====================== Fans =========================================== */
	#############################################################################

	public function getFans() {

		$json_data = $this -> input -> post('data');

		//$json_data = '{"pageNo":"5","userId":"646"}';

		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$pageNo = intval($arr_data -> pageNo);
		$fanId = intval($arr_data -> fanId);

		$filters = $arr_data -> filters;

		//	print_r($filters);
		$searchText = $arr_data -> searchText;
		$sortBy = $arr_data -> sortBy;
		$countriesIds = $arr_data -> countriesIds;
		$favoriteFans = $arr_data -> favoriteFans;
		/*
		 $userId =62;
		 $pageNo =1;
		 */
		//$fanId = 2;
		/*
		 $filters->online=1;
		 $filters->offline=1;
		 $filters->male=1;
		 $filters->female=1;
		 $filters->following=1;
		 $filters->followers=1;

		 $searchText='a';
		 $sortBy = 2;
		 $countriesIds = array(1,2);
		 $favoriteFans = 1;*/

		if (!empty($fanId))
			$userId = $fanId;

		if (empty($pageNo)) {
			print_r(json_encode(array('status' => -1)));
		} else {

			if (empty($userId))
				$userId = 0;
			$where = 'user.userId != ' . $userId;

			$order_by = 'user.fullName';
			$order = 'asc';

			$select_fields = 'user.isPremium ,(select userfans.`isFollow` from userfans where `userId` = ' . $userId . ' AND `fanId` = `user`.userId ) as isFollow,';
			$select_fields .= '(select userfans.`isFriend` from userfans where `userId` = ' . $userId . ' AND `fanId` = `user`.userId) as isFriend,';
			$select_fields .= '(select userfans.`isFavourite` from userfans where `userId` = ' . $userId . ' AND `fanId` = `user`.userId) as isFavourite,';
			$select_fields .= '(select userfans.`isBlocked` from userfans where `userId` = ' . $userId . ' AND `fanId` = `user`.userId) as isBlocked,';
			$select_fields .= 'user.userId,user.fullName as userName,user.profilePic,user.isOnline as online,user.brief as fanBrief,';
			$select_fields .= 'country.countryName as countryText,';
			$select_fields .= '(select count(userfans.`id`) from userfans where `userId` = ' . $userId . ' AND isFollow="1") as followingNo,';
			$select_fields .= '(select count(userfans.`id`) from userfans where `fanId` = ' . $userId . ' AND isFollow="1") as followersNo';

			$start = ($pageNo - 1) * 18;

			$additional_params = array();

			if (empty($fanId)) {
				if (!empty($searchText))
					$where .= " and (user.email like '%" . $searchText . "%' or user.fullName like '%" . $searchText . "%')";

				if (!empty($favoriteFans))
					$where .= " and userfans.isFavourite= '1'";

				if ($sortBy == 2) {
					$order_by = 'user.userId';
					$order = 'desc';
				} elseif ($sortBy == 3) {
					$order_by = 'user.noOfPics';
					$order = 'desc';
				}

				if (!empty($countriesIds))
					$additional_params['where_in'] = array('user.countryId', implode(',', $countriesIds));

				if (!empty($filters)) {

					if ($filters -> online == 1 && $filters -> offline == 1) {

					} else {
						if ($filters -> online == 1)
							$where .= " and user.isOnline='1'";
						if ($filters -> offline == 1)
							$where .= " and user.isOnline='0'";
					}
					if ($filters -> male == 1 && $filters -> female == 1) {

					} else {
						if ($filters -> male == 1)
							$where .= " and user.gender='0'";
						if ($filters -> female == 1)
							$where .= " and user.gender='1'";
					}
					if ($filters -> following == 1)
						$where .= " and user.isFollow='1'";
					if ($filters -> followers == 1)
						$where .= " and user.fanId in (select userId from userfans where fanId=" . $userId . " and isFollow='1')";
				}
			}

			$additional_params['join'] = array('userfans', 'user.userId=userfans.fanId', 'left');
			$additional_params['join2'] = array('country', 'country.countryId=user.countryId', 'left');
			$this -> db -> group_by('user.userId');
			$fans = $this -> main_model -> getBackendData('user', $select_fields, $where, $order_by, $order, 18, $start, $additional_params);
			if ($fans) {
				foreach ($fans as $one_fan) {
					if ($one_fan -> profilePic == '')
						unset($one_fan -> profilePic);
					else
					{
						if (strpos($one_fan -> profilePic, 'http') === FALSE)
							$one_fan -> profilePic = base_url() . 'third_party/uploads/profile/' . $one_fan -> profilePic;
					}
					
					$one_fan -> userId = intval($one_fan -> userId);
					$one_fan -> online = intval($one_fan -> online);
					$one_fan -> isFollow = intval($one_fan -> isFollow);
					$one_fan -> isFriend = intval($one_fan -> isFriend);
					$one_fan -> isPremium = intval($one_fan -> isPremium);
					$one_fan -> isBlocked = intval($one_fan -> isBlocked);

					if (!$one_fan -> fanBrief)
						$one_fan -> fanBrief = '';
					if (!$one_fan -> countryText)
						$one_fan -> countryText = '';
				}
				print json_encode(array('status' => 2, 'data' => $fans));
			} else
				print json_encode(array('status' => 2, 'data' => array()));
		}
		$this->db->close();
	}

	public function getFan() {
		$json_data = $this -> input -> post('data');

		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$fanId = intval($arr_data -> fanId);
		/*
		 $userId =1;
		 $fanId =1;
		 */
		if (empty($userId) || empty($fanId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$where = 'userfans.userId=' . $userId . ' and userfans.fanId=' . $fanId;

			$order_by = '';
			$order = '';

			$select_fields = 'userfans.isFollow,userfans.isFriend,user.isPremium,userfans.isBlocked,';
			$select_fields .= 'user.userId,user.fullName as userName,user.profilePic,user.isOnline as online,user.brief as fanBrief,';
			$select_fields .= 'country.countryName as countryText,';
			$select_fields .= '(select count(userfans.`id`) from userfans where `userId` = ' . $userId . ') as followingNo,';
			$select_fields .= '(select count(userfans.`id`) from userfans where `fanId` = ' . $userId . ') as followersNo';

			$start = '';

			$additional_params = array();
			$additional_params['join'] = array('user', 'user.userId=userfans.fanId', 'left');
			$additional_params['join2'] = array('country', 'country.countryId=user.countryId', 'left');

			$fans = $this -> main_model -> getBackendData('userfans', $select_fields, $where, $order_by, $order, 18, $start, $additional_params);
			if ($fans) {
				$one_fan = $fans[0];
				if ($one_fan -> profilePic == '')
					unset($one_fan -> profilePic);
				else
				{
					if (strpos($one_fan -> profilePic, 'http') === FALSE)
						$one_fan -> profilePic = base_url() . 'third_party/uploads/profile/' . $one_fan -> profilePic;
				}
				
				$one_fan -> userId = intval($one_fan -> userId);
				$one_fan -> online = intval($one_fan -> online);
				$one_fan -> isFollow = intval($one_fan -> isFollow);
				$one_fan -> isFriend = intval($one_fan -> isFriend);
				$one_fan -> isPremium = intval($one_fan -> isPremium);
				$one_fan -> isBlocked = intval($one_fan -> isBlocked);

				if (!$one_fan -> fanBrief)
					$one_fan -> fanBrief = '';
				if (!$one_fan -> countryText)
					$one_fan -> countryText = '';

				print json_encode(array('status' => 2, 'data' => $one_fan));
			} else
				print_r(json_encode(array('status' => -2)));
		}
	}

	private function _enable_fan_relation($field_name, $data_arr = '') {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$fanId = intval($arr_data -> fanId);

		if (empty($userId) || empty($fanId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$arr = array('userId' => $userId, 'fanId' => $fanId);
			$exists = $this -> main_model -> getData('userfans', 'id', $arr);
			if ($exists) {
				if (!$data_arr)
					$data_arr = array($field_name => '1');
				$this -> main_model -> update('userfans', $data_arr, $arr);
			} else {
				if (!$data_arr) {
					$arr[$field_name] = '1';
					$data_arr = $arr;
				} else {
					$data_arr['userId'] = $userId;
					$data_arr['fanId'] = $fanId;
				}
				$this -> main_model -> insert('userfans', $data_arr);
			}
			print_r(json_encode(array('status' => 1)));
		}
		$this->db->close();
	}

	private function _disable_fan_relation($field_name, $updated_arr = '') {

		$json_data = $this -> input -> post('data');

		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$fanId = intval($arr_data -> fanId);

		if (empty($userId) || empty($fanId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$where = array('userId' => $userId, 'fanId' => $fanId);
			$exists = $this -> main_model -> getData('userfans', 'id', $where);
			if ($exists) {
				if ($updated_arr)
					$this -> main_model -> update('userfans', $updated_arr, $where);
				else
					$this -> main_model -> update('userfans', array($field_name => '0'), $where);
				print_r(json_encode(array('status' => 1)));
			} else
				print_r(json_encode(array('status' => -2)));
		}
		$this->db->close();
	}

	public function followFan() {
		$this -> _enable_fan_relation('isFollow');
	}

	public function unfollowFan() {
		$this -> _disable_fan_relation('isFollow');
	}

	public function blockFan() {
		$this -> _enable_fan_relation('isBlocked');
	}

	public function unblockFan() {
		$this -> _disable_fan_relation('isBlocked');
	}

	public function addFavoriteFriend() {

		$this -> _enable_fan_relation('isFavourite');
	}

	public function removeFavoriteFriend() {

		$this -> _disable_fan_relation('isFavourite');
	}

	public function removeFriend() {
		$updated_arr = array('isFriend' => '0', 'isFavourite' => '0', 'isFollow' => '0');
		$this -> _disable_fan_relation(TRUE, $updated_arr);
	}

	public function addFriendRequest() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$fanId = intval($arr_data -> fanId);

		if (empty($userId) || empty($fanId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$arr = array('userId' => $userId, 'fanId' => $fanId);
			$exists = $this -> main_model -> getData('userfans', 'id', $arr);
			if ($exists)
				$this -> main_model -> update('userfans', array('friendRequest' => '1'), $arr);
			else {
				$arr['friendRequest'] = '1';
				$this -> main_model -> insert('userfans', $arr);
			}
			$this -> main_model -> insert('notification', array('userId' => $fanId, 'notificationType' => '1', 'notificationText' => 'Friend request'));
			print_r(json_encode(array('status' => 1)));
		}
	}

	public function acceptFriendRequest() {
		$updated_arr = array('friendRequest' => '0', 'isFriend' => '1', 'isFollow' => '1');
		$this -> _enable_fan_relation(TRUE, $updated_arr);
	}

	public function rejectFriendRequest() {
		$this -> _disble_fan_relation('friendRequest');
	}

	#############################################################################
	/* ====================== Notifications ================================== */
	#############################################################################

	public function readNotification() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$notificationId = $arr_data -> notificationId;
		if (empty($notificationId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$notification = $this -> main_model -> getData('notification', 'notificationId', array('notificationId' => $notificationId));
			if ($notification) {
				$delete_notification = $this -> main_model -> delete('notification', array('notificationId' => $notificationId));
				if ($delete_notification)
					print_r(json_encode(array('status' => 1)));
				else
					print_r(json_encode(array('status' => -1)));
			} else {
				print_r(json_encode(array('status' => 1)));
			}
		}
		$this->db->close();
	}

	public function getUnreadNotifications() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$userId = $arr_data -> userId;
		if (empty($userId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$notifications = $this -> main_model -> getData('notification', '', array('userId' => $userId), 'notificationId', 'asc');
			if ($notifications) {
				foreach ($notifications as $one_notification) {
					$one_notification -> notificationId = intval($one_notification -> notificationId);
					$one_notification -> userId = intval($one_notification -> userId);
					$one_notification -> notificationType = intval($one_notification -> notificationType);
					if (!$one_notification -> notificationText)
						$one_notification -> notificationText = '';
				}
				print json_encode(array('status' => 2, 'data' => $notifications));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	#############################################################################
	/* ====================== Wall =========================================== */
	#############################################################################

	public function addComment() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$photoId = intval($arr_data -> photoId);
		$commentText = $arr_data -> commentText;

		if (empty($userId) || empty($photoId) || empty($commentText))
			print_r(json_encode(array('status' => -1)));
		else {
			$insert_arr = array('userId' => $userId, 'photoId' => $photoId, 'commentText' => $commentText);
			$insert = $this -> main_model -> insert('user_comment', $insert_arr);
			if ($insert)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -2)));
		}
		$this->db->close();
	}

	public function removeComment() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$commentId = intval($arr_data -> commentId);

		if (empty($commentId))
			print_r(json_encode(array('status' => -1)));
		else {
			$insert_arr = array('userId' => $userId, 'photoId' => $photoId, 'commentText' => $commentText);
			$this -> main_model -> delete('user_comment', array('commentId' => $commentId));
			print_r(json_encode(array('status' => 1)));
		}
		$this->db->close();
	}

	public function getComments() {
		$json_data = $this -> input -> post('data');
		//$json_data = '{"pageNo":"1","photoId":"150"}';
		$arr_data = json_decode($json_data);
		$photoId = intval($arr_data -> photoId);
		$pageNo = intval($arr_data -> pageNo);

		if (empty($photoId) || empty($pageNo))
			print_r(json_encode(array('status' => -1)));
		else {
			$start = ($pageNo - 1) * 20;
			$additionalParams['join'] = array('user', 'user.userId=user_comment.userId', 'left');
			$comments = $this -> main_model -> getBackendData('user_comment', 'user_comment.commentId , user_comment.userId , user_comment.photoId,user_comment.commentText,user_comment.commentDate,user.fullName,user.profilePic', array('photoId' => $photoId), 'commentDate', 'desc', 20, $start, $additionalParams);
			if ($comments) {
				foreach ($comments as $one_comment) {

					$one_comment -> commentId = intval($one_comment -> commentId);
					$one_comment -> userId = intval($one_comment -> userId);
					$one_comment -> photoId = intval($one_comment -> photoId);

					if (!$one_comment -> commentText)
						$one_comment -> commentText = '';
					if (!$one_comment -> commentDate)
						$one_comment -> commentDate = '';
					if (!$one_comment -> profilePic)
						$one_comment -> profilePic = '';
					else
					{
						if (strpos($one_comment -> profilePic, 'http') === FALSE)
							$one_comment -> profilePic = base_url() . 'third_party/uploads/profile/' . $one_comment -> profilePic;
					}
				}
				print json_encode(array('status' => 2, 'data' => $comments));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	public function likePhoto() {
		$json_data = $this -> input -> post('data');

		//$json_data ='{"photoId":"288","userId":"822"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$photoId = intval($arr_data -> photoId);

		if (empty($userId) || empty($photoId))
			print_r(json_encode(array('status' => -1)));
		else {
			$checkExist = $this -> main_model -> getData('user_photo_likes', '', array('userId' => $userId, 'photoId' => $photoId));
			if ($checkExist)
				print_r(json_encode(array('status' => 1)));
			else {
				$insert_arr = array('userId' => $userId, 'photoId' => $photoId);
				$insert = $this -> main_model -> insert('user_photo_likes', $insert_arr);
				if ($insert)
					print_r(json_encode(array('status' => 1)));
				else
					print_r(json_encode(array('status' => -2)));
			}
		}
		$this->db->close();
	}

	public function dislikePhoto() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$photoId = intval($arr_data -> photoId);

		if (empty($userId) || empty($photoId))
			print_r(json_encode(array('status' => -1)));
		else {
			$where = array('userId' => $userId, 'photoId' => $photoId);
			$delete = $this -> main_model -> delete('user_photo_likes', $where);
			print_r(json_encode(array('status' => 1)));
		}
		$this->db->close();
	}

	/*
	 public function getTopTen()
	 {
	 $json_data 	= $this->input->post('data');
	 $arr_data 	= json_decode($json_data);

	 $filter 	= intval($arr_data->filter);
	 $pageNo 	= intval($arr_data->pageNo);

	 if(empty($filter) || empty($pageNo))
	 print_r(json_encode(array('status'=>-1)));
	 else
	 {
	 $additional_params = array();

	 $start=($pageNo-1)*10;

	 $select = 'user_photo.*,user.fullName as userName,studio_photo.photoUrl,';
	 $select.= '(select count(`commentId`) from user_comment where `photoId` = `user_photo`.photoId)  as commentsCount,';
	 $select.= '(select count(`likeId`) from  user_photo_likes where `photoId` = `user_photo`.photoId AND `userId` = `user_photo`.userId) as liked,';
	 $select.= '(select count(`likeId`) from user_photo_likes where `photoId` = `user_photo`.photoId ) as likesCount,';

	 $where = 'thesaurus.thesType=3';
	 if(!empty($filter))
	 {
	 if($filter == 1)	 $where.=' and user_photo.uploadedDate >= ( NOW( ) - INTERVAL 1 WEEK )';
	 elseif($filter == 2) $where.=' and user_photo.uploadedDate >= ( NOW( ) - INTERVAL 2 WEEK )';
	 elseif($filter == 3) $where.=' and user_photo.uploadedDate >= ( NOW( ) - INTERVAL 1 MONTH )';
	 }
	 $additional_params['join'] = array('user','user.userId = user_photo.userId','left');
	 $additional_params['join2'] = array('studio_photo','user_photo.studioPhotoId = studio_photo.imageId','left');
	 $additional_params['join3'] = array('studio_photo_thesaurus','studio_photo_thesaurus.studioPhotoId = user_photo.studioPhotoId','left');
	 $additional_params['join4'] = array('thesaurus','thesaurus.thesId = studio_photo_thesaurus.thesId','left');
	 $photos = $this->main_model->getBackendData('user_photo',$select,$where,'user_photo.contestRank','asc',10,$start,$additional_params);
	 if($photos)
	 {
	 $output = array();
	 foreach ($photos as $one_photo) {
	 unset($one_photo->contestRank);
	 if($one_photo->imageUrl)
	 $one_photo->imageUrl = base_url().'third_party/uploads/users/'.$one_photo->userId.'/'.$one_photo->imageUrl;
	 else
	 $one_photo->imageUrl = '';

	 $one_photo->photoId = intval($one_photo->photoId);
	 $one_photo->studioPhotoId = intval($one_photo->studioPhotoId);
	 $one_photo->userId = intval($one_photo->userId);
	 $one_photo->privacy = intval($one_photo->privacy);
	 $one_photo->publishedToWall = intval($one_photo->publishedToWall);

	 if(!$one_photo->uploadedDate)$one_photo->uploadedDate='';
	 if(!$one_photo->caption)$one_photo->caption='';

	 if($one_photo->photoUrl)
	 $one_photo->photoUrl = base_url().'third_party/uploads/'.$one_photo->photoUrl;
	 else
	 $one_photo->photoUrl = '';

	 if(!$one_photo->userName)$one_photo->userName='';
	 $one_photo->commentsCount = intval($one_photo->commentsCount);
	 $one_photo->likesCount = intval($one_photo->likesCount);
	 $one_photo->liked = intval($one_photo->liked);

	 $output[] = array(
	 'photo' => array(
	 'imageUrl' => $one_photo->imageUrl,
	 'photoId' => $one_photo->photoId,
	 'studioPhotoId' => $one_photo->studioPhotoId,
	 'userId' => $one_photo->userId,
	 'privacy' => $one_photo->privacy,
	 'publishedToWall' => $one_photo->publishedToWall,
	 'uploadedDate' => $one_photo->uploadedDate,
	 'caption' => $one_photo->caption
	 ),
	 'userName' => $one_photo->userName,
	 'commentsCount' => $one_photo->commentsCount,
	 'likesCount' => $one_photo->likesCount,
	 'liked' => $one_photo->liked,
	 'photoUrl' => $one_photo->photoUrl,
	 'postDateTime' => $one_photo->uploadedDate
	 );
	 }
	 print json_encode(array('status'=>2,'data'=>$output));
	 }
	 else
	 print_r(json_encode(array('status'=>2,'data'=>array())));
	 }
	 }
	 */

	public function getTopTen__() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$userId = intval($arr_data -> userId);

		if (empty($userId))
			$userId = 0;

		$seasons = $this -> main_model -> getData('thesaurus', '*', array('thesType' => 3), 'rank', 'desc');
		//print_r($seasons);
		if ($seasons) {
			foreach ($seasons as $season) {
				$start = 0;

				$select = 'distinct user_photo.photoId as tmpId,user_photo.*,user.fullName as userName,studio_photo.photoUrl,studio_photo.usedCount,studio_photo.photoOrientation,user.profilePic as userProfilePicUrl,studio_photo.usedCount,country.countryName as userCountry,studio_photo_thesaurus.thesId,user_photo.contestRank,';
				$select .= '(select count(`commentId`) from user_comment where `photoId` = `user_photo`.photoId)  as commentsCount,';
				$select .= '(select count(`likeId`) from  user_photo_likes where `photoId` = `user_photo`.photoId AND `userId` = ' . $userId . ') as liked,';
				$select .= '(select count(`likeId`) from user_photo_likes where `photoId` = `user_photo`.photoId ) as likesCount,';

				$where = 'studio_photo_thesaurus.thesId = ' . $season -> thesId;

				$additional_params['join'] = array('user', 'user.userId = user_photo.userId', 'left');
				$additional_params['join2'] = array('studio_photo', 'user_photo.studioPhotoId = studio_photo.imageId', 'left');
				$additional_params['join3'] = array('studio_photo_thesaurus', 'studio_photo_thesaurus.studioPhotoId = user_photo.studioPhotoId', 'left');
				$additional_params['join4'] = array('thesaurus', 'thesaurus.thesId = studio_photo_thesaurus.thesId', 'left');
				$additional_params['join5'] = array('userfans', 'userfans.userId = user_photo.userId', 'left');
				$additional_params['join6'] = array('country', 'user.countryId = country.countryId', 'left');
				$this -> db -> group_by('user_photo.photoId');
				$photos = $this -> main_model -> getBackendData('user_photo', $select, $where, 'user_photo.contestRank', 'desc', 10, $start, $additional_params);
				if ($photos) {
					$output = array();
					foreach ($photos as $one_photo) {
						$tags = $this -> main_model -> getData('thesaurus', 'thesaurus.thesId as tagId,thesaurus.thesName as tagName', array('thesaurus.thesParentId' => $one_photo -> thesId, 'thesType' => '4'));
						if (!$tags)
							$tags = array();

						if ($one_photo -> imageUrl)
							$one_photo -> imageUrl = base_url() . 'third_party/uploads/users/' . $one_photo -> userId . '/' . $one_photo -> imageUrl;
						else
							$one_photo -> imageUrl = '';

						$one_photo -> photoId = intval($one_photo -> photoId);
						$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
						$one_photo -> userId = intval($one_photo -> userId);
						$one_photo -> privacy = intval($one_photo -> privacy);
						$one_photo -> publishedToWall = intval($one_photo -> publishedToWall);
						$one_photo -> contestRank = intval($one_photo -> contestRank);

						if (!$one_photo -> uploadedDate)
							$one_photo -> uploadedDate = '';
						if (!$one_photo -> caption)
							$one_photo -> caption = '';

						if ($one_photo -> photoUrl)
							$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;
						else
							$one_photo -> photoUrl = '';
						if ($one_photo -> userProfilePicUrl)
							$one_photo -> userProfilePicUrl = base_url() . 'third_party/uploads/profile/' . $one_photo -> userProfilePicUrl;
						else
							$one_photo -> userProfilePicUrl = '';

						if (!$one_photo -> userName)
							$one_photo -> userName = '';
						if (!$one_photo -> userCountry)
							$one_photo -> userCountry = '';
						$one_photo -> commentsCount = intval($one_photo -> commentsCount);
						$one_photo -> likesCount = intval($one_photo -> likesCount);
						if ($one_photo -> liked > 0)
							$one_photo -> liked = 1;
						else
							$one_photo -> liked = 0;

						$one_photo -> usedCount = intval($one_photo -> usedCount);
						$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);

						$output[] = array('photo' => array('photoId' => $one_photo -> photoId, 'studioPhotoId' => $one_photo -> studioPhotoId, 'userId' => $one_photo -> userId, 'uploadedDate' => $one_photo -> uploadedDate, 'caption' => $one_photo -> caption, 'imageUrl' => $one_photo -> imageUrl, 'privacy' => $one_photo -> privacy, 'publishedToWall' => $one_photo -> publishedToWall, 'contestRank' => $one_photo -> contestRank),
						//'photoUrl' => $one_photo->photoUrl,
						'photoUrl' => $one_photo -> imageUrl, 'userName' => $one_photo -> userName, 'userCountry' => $one_photo -> userCountry, 'userProfilePicUrl' => $one_photo -> userProfilePicUrl, 'commentsCount' => $one_photo -> commentsCount, 'liked' => $one_photo -> liked, 'likesCount' => $one_photo -> likesCount, 'tags' => $tags, 'photoOrientation' => $one_photo -> photoOrientation);
					}
					//print json_encode(array('status'=>2,'data'=>$output));
					$postsArr = $output;
				} else
					$postsArr = array();

				$arr1[] = array('seasonId' => $season -> thesId, 'seasonName' => $season -> thesName, 'rank' => $season -> rank, 'posts' => $postsArr);
			}

			print_r(json_encode(array('status' => 2, 'data' => $arr1)));
		} else
			print_r(json_encode(array('status' => 2, 'data' => array())));

	}

	public function getWall__() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","userId":"817"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$pageNo = intval($arr_data -> pageNo);

		//$userId=5;
		//$pageNo=1;

		if (empty($userId) || empty($pageNo))
			print_r(json_encode(array('status' => -1)));
		else {
			$additional_params = array();

			$start = ($pageNo - 1) * 6;

			$select = 'distinct (user_photo.photoId) as tmpId,user_photo.*,user.fullName as userName,studio_photo.photoUrl,studio_photo.usedCount,studio_photo.photoOrientation,user.profilePic as userProfilePicUrl,studio_photo.usedCount,country.countryName as userCountry,studio_photo_thesaurus.thesId,user_photo.contestRank,';
			$select .= '(select count(`commentId`) from user_comment where `photoId` = `user_photo`.photoId)  as commentsCount,';
			$select .= '(select count(`likeId`) from  user_photo_likes where `photoId` = `user_photo`.photoId AND `userId` = ' . $userId . ') as liked,';
			$select .= '(select count(`likeId`) from user_photo_likes where `photoId` = `user_photo`.photoId ) as likesCount,';
			// user photos
			$where = '(user_photo.userId=' . $userId . ') 
				OR (
					user_photo.userId in(select fanId from userfans  where userfans.userId=' . $userId . ' 
											and userfans.isFollow="1" and userfans.isBlocked="0" 
										  )
					
					
				)';

			/*
			 (user_photo.userId NOT in(select userId from userfans where userfans.fanId='.$userId.'
			 and userfans.isBlocked="1"
			 )
			 )
			 */
			//$where.="(userfans.fanId in(select fanId from userfans where userfans.userId=".$userId." and userfans.isFollow='1' and userfans.isBlocked='0' ))";
			////$where.=" and (user_photo.privacy=1 or user_photo.privacy=2 or (user_photo.privacy='3' and user_photo.userId in (select userId from userfans where fanId=".$userId." and isFriend='1')))";
			//$where.=')';

			$additional_params['join'] = array('user', 'user.userId = user_photo.userId', 'left');
			$additional_params['join2'] = array('studio_photo', 'user_photo.studioPhotoId = studio_photo.imageId', 'left');
			$additional_params['join3'] = array('studio_photo_thesaurus', 'studio_photo_thesaurus.studioPhotoId = user_photo.studioPhotoId', 'left');
			$additional_params['join4'] = array('thesaurus', 'thesaurus.thesId = studio_photo_thesaurus.thesId', 'left');
			$additional_params['join5'] = array('userfans', 'userfans.userId = user_photo.userId', 'left');
			$additional_params['join6'] = array('country', 'user.countryId = country.countryId', 'left');
			$this -> db -> group_by('user_photo.photoId');

			$photos = $this -> main_model -> getBackendData('user_photo', $select, $where, 'user_photo.uploadedDate', 'desc', 6, $start, $additional_params);
			/*
			 echo "<pre>";
			 print_r($photos);
			 echo "</pre>";
			 */
			if ($photos) {
				$output = array();
				foreach ($photos as $one_photo) {
					$tags = $this -> main_model -> getData('thesaurus', 'thesaurus.thesId as tagId,thesaurus.thesName as tagName', array('thesaurus.thesParentId' => $one_photo -> thesId, 'thesType' => '4'));
					if (!$tags)
						$tags = array();

					if ($one_photo -> imageUrl)
						$one_photo -> imageUrl = base_url() . 'third_party/uploads/users/' . $one_photo -> userId . '/' . $one_photo -> imageUrl;
					else
						$one_photo -> imageUrl = '';

					$one_photo -> photoId = intval($one_photo -> photoId);
					$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
					$one_photo -> userId = intval($one_photo -> userId);
					$one_photo -> privacy = intval($one_photo -> privacy);
					$one_photo -> publishedToWall = intval($one_photo -> publishedToWall);
					$one_photo -> contestRank = intval($one_photo -> contestRank);

					if (!$one_photo -> uploadedDate)
						$one_photo -> uploadedDate = '';
					if (!$one_photo -> caption)
						$one_photo -> caption = '';

					if ($one_photo -> photoUrl)
						$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;
					else
						$one_photo -> photoUrl = '';
					if ($one_photo -> userProfilePicUrl)
						$one_photo -> userProfilePicUrl = base_url() . 'third_party/uploads/profile/' . $one_photo -> userProfilePicUrl;
					else
						$one_photo -> userProfilePicUrl = '';

					if (!$one_photo -> userName)
						$one_photo -> userName = '';
					if (!$one_photo -> userCountry)
						$one_photo -> userCountry = '';
					$one_photo -> commentsCount = intval($one_photo -> commentsCount);
					$one_photo -> likesCount = intval($one_photo -> likesCount);
					if ($one_photo -> liked > 0)
						$one_photo -> liked = 1;
					else
						$one_photo -> liked = 0;
					$one_photo -> usedCount = intval($one_photo -> usedCount);
					$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);

					$output[] = array('photo' => array('photoId' => $one_photo -> photoId, 'studioPhotoId' => $one_photo -> studioPhotoId, 'userId' => $one_photo -> userId, 'uploadedDate' => $one_photo -> uploadedDate, 'caption' => $one_photo -> caption, 'imageUrl' => $one_photo -> imageUrl, 'privacy' => $one_photo -> privacy, 'publishedToWall' => $one_photo -> publishedToWall, 'contestRank' => $one_photo -> contestRank), 'photoUrl' => $one_photo -> imageUrl, 'userName' => $one_photo -> userName, 'userCountry' => $one_photo -> userCountry, 'userProfilePicUrl' => $one_photo -> userProfilePicUrl, 'commentsCount' => $one_photo -> commentsCount, 'liked' => $one_photo -> liked, 'likesCount' => $one_photo -> likesCount, 'tags' => $tags, 'photoOrientation' => $one_photo -> photoOrientation);
				}
				print json_encode(array('status' => 2, 'data' => $output));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	public function getLatest__() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$pageNo = intval($arr_data -> pageNo);

		if (empty($pageNo))
			print_r(json_encode(array('status' => -1)));
		else {
			if (empty($userId))
				$userId = 0;
			$additional_params = array();

			$start = ($pageNo - 1) * 6;

			$select = 'distinct user_photo.photoId as tmpId,user_photo.*,user.fullName as userName,studio_photo.photoUrl,studio_photo.usedCount,studio_photo.photoOrientation,user.profilePic as userProfilePicUrl,studio_photo.usedCount,country.countryName as userCountry,studio_photo_thesaurus.thesId,user_photo.contestRank,';
			$select .= '(select count(`commentId`) from user_comment where `photoId` = `user_photo`.photoId)  as commentsCount,';
			$select .= '(select count(`likeId`) from  user_photo_likes where `photoId` = `user_photo`.photoId AND `userId` = ' . $userId . '  ) as liked ,';
			$select .= '(select count(`likeId`) from user_photo_likes where `photoId` = `user_photo`.photoId ) as likesCount,';

			$where = "user_photo.privacy=1 or user_photo.privacy=2 or (user_photo.privacy='3' and user_photo.userId in (select userId from userfans where fanId=" . $userId . " and isFriend='1'))";

			$additional_params['join'] = array('user', 'user.userId = user_photo.userId', 'left');
			$additional_params['join2'] = array('studio_photo', 'user_photo.studioPhotoId = studio_photo.imageId', 'left');
			$additional_params['join3'] = array('studio_photo_thesaurus', 'studio_photo_thesaurus.studioPhotoId = user_photo.studioPhotoId', 'left');
			$additional_params['join4'] = array('thesaurus', 'thesaurus.thesId = studio_photo_thesaurus.thesId', 'left');
			$additional_params['join5'] = array('userfans', 'userfans.userId = user_photo.userId', 'left');
			$additional_params['join6'] = array('country', 'user.countryId = country.countryId', 'left');
			$this -> db -> group_by('user_photo.photoId');
			$photos = $this -> main_model -> getBackendData('user_photo', $select, $where, 'user_photo.uploadedDate', 'desc', 6, $start, $additional_params);
			if ($photos) {
				$output = array();
				foreach ($photos as $one_photo) {
					$tags = $this -> main_model -> getData('thesaurus', 'thesaurus.thesId as tagId,thesaurus.thesName as tagName', array('thesaurus.thesParentId' => $one_photo -> thesId, 'thesType' => '4'));
					if (!$tags)
						$tags = array();

					if ($one_photo -> imageUrl)
						$one_photo -> imageUrl = base_url() . 'third_party/uploads/users/' . $one_photo -> userId . '/' . $one_photo -> imageUrl;
					else
						$one_photo -> imageUrl = '';

					$one_photo -> photoId = intval($one_photo -> photoId);
					$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
					$one_photo -> userId = intval($one_photo -> userId);
					$one_photo -> privacy = intval($one_photo -> privacy);
					$one_photo -> publishedToWall = intval($one_photo -> publishedToWall);
					$one_photo -> contestRank = intval($one_photo -> contestRank);

					if (!$one_photo -> uploadedDate)
						$one_photo -> uploadedDate = '';
					if (!$one_photo -> caption)
						$one_photo -> caption = '';

					if ($one_photo -> photoUrl)
						$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;
					else
						$one_photo -> photoUrl = '';
					if ($one_photo -> userProfilePicUrl)
					{
						if (strpos($one_photo -> userProfilePicUrl, 'http') === FALSE)
							$one_photo -> userProfilePicUrl = base_url() . 'third_party/uploads/profile/' . $one_photo -> userProfilePicUrl;
					}
					else
						$one_photo -> userProfilePicUrl = '';

					if (!$one_photo -> userName)
						$one_photo -> userName = '';
					if (!$one_photo -> userCountry)
						$one_photo -> userCountry = '';
					$one_photo -> commentsCount = intval($one_photo -> commentsCount);
					$one_photo -> likesCount = intval($one_photo -> likesCount);
					if ($one_photo -> liked > 0)
						$one_photo -> liked = 1;
					else
						$one_photo -> liked = 0;

					$one_photo -> usedCount = intval($one_photo -> usedCount);
					$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);

					$output[] = array('photo' => array('photoId' => $one_photo -> photoId, 'studioPhotoId' => $one_photo -> studioPhotoId, 'userId' => $one_photo -> userId, 'uploadedDate' => $one_photo -> uploadedDate, 'caption' => $one_photo -> caption, 'imageUrl' => $one_photo -> imageUrl, 'privacy' => $one_photo -> privacy, 'publishedToWall' => $one_photo -> publishedToWall, 'contestRank' => $one_photo -> contestRank), 'photoUrl' => $one_photo -> imageUrl, 'userName' => $one_photo -> userName, 'userCountry' => $one_photo -> userCountry, 'userProfilePicUrl' => $one_photo -> userProfilePicUrl, 'commentsCount' => $one_photo -> commentsCount, 'liked' => $one_photo -> liked, 'likesCount' => $one_photo -> likesCount, 'tags' => $tags, 'photoOrientation' => $one_photo -> photoOrientation);
				}
				print json_encode(array('status' => 2, 'data' => $output));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	public function getMyGallery() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$pageNo = intval($arr_data -> pageNo);

		//$userId=5;
		//$pageNo=1;

		if (empty($userId) || empty($pageNo))
			print_r(json_encode(array('status' => -1)));
		else {
			$additional_params = array();

			$start = ($pageNo - 1) * 6;

			$select = 'distinct user_photo.photoId as tmpId,user_photo.*,user.fullName as userName,studio_photo.photoUrl,studio_photo.usedCount,studio_photo.photoOrientation,user.profilePic as userProfilePicUrl,studio_photo.usedCount,country.countryName as userCountry,studio_photo_thesaurus.thesId,user_photo.contestRank,';
			$select .= '(select count(`commentId`) from user_comment where `photoId` = `user_photo`.photoId)  as commentsCount,';
			$select .= '(select count(`likeId`) from  user_photo_likes where `photoId` = `user_photo`.photoId AND `userId` = ' . $userId . ') as liked,';
			$select .= '(select count(`likeId`) from user_photo_likes where `photoId` = `user_photo`.photoId ) as likesCount,';

			$where = 'user_photo.userId=' . $userId . '';

			$additional_params['join'] = array('user', 'user.userId = user_photo.userId', 'left');
			$additional_params['join2'] = array('studio_photo', 'user_photo.studioPhotoId = studio_photo.imageId', 'left');
			$additional_params['join3'] = array('studio_photo_thesaurus', 'studio_photo_thesaurus.studioPhotoId = user_photo.studioPhotoId', 'left');
			$additional_params['join4'] = array('thesaurus', 'thesaurus.thesId = studio_photo_thesaurus.thesId', 'left');
			$additional_params['join5'] = array('userfans', 'userfans.userId = user_photo.userId', 'left');
			$additional_params['join6'] = array('country', 'user.countryId = country.countryId', 'left');
			$this -> db -> group_by('user_photo.photoId');
			$photos = $this -> main_model -> getBackendData('user_photo', $select, $where, 'user_photo.uploadedDate', 'desc', 6, $start, $additional_params);
			if ($photos) {
				$output = array();
				foreach ($photos as $one_photo) {
					$tags = $this -> main_model -> getData('thesaurus', 'thesaurus.thesId as tagId,thesaurus.thesName as tagName', array('thesaurus.thesParentId' => $one_photo -> thesId, 'thesType' => '4'));
					if (!$tags)
						$tags = array();

					if ($one_photo -> imageUrl)
						$one_photo -> imageUrl = base_url() . 'third_party/uploads/users/' . $one_photo -> userId . '/' . $one_photo -> imageUrl;
					else
						$one_photo -> imageUrl = '';

					$one_photo -> photoId = intval($one_photo -> photoId);
					$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
					$one_photo -> userId = intval($one_photo -> userId);
					$one_photo -> privacy = intval($one_photo -> privacy);
					$one_photo -> publishedToWall = intval($one_photo -> publishedToWall);
					$one_photo -> contestRank = intval($one_photo -> contestRank);

					if (!$one_photo -> uploadedDate)
						$one_photo -> uploadedDate = '';
					if (!$one_photo -> caption)
						$one_photo -> caption = '';

					if ($one_photo -> photoUrl)
						$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;
					else
						$one_photo -> photoUrl = '';
					if ($one_photo -> userProfilePicUrl)
					{
						if (strpos($one_photo -> userProfilePicUrl, 'http') === FALSE)
							$one_photo -> userProfilePicUrl = base_url() . 'third_party/uploads/profile/' . $one_photo -> userProfilePicUrl;
					}
					else
						$one_photo -> userProfilePicUrl = '';

					if (!$one_photo -> userName)
						$one_photo -> userName = '';
					if (!$one_photo -> userCountry)
						$one_photo -> userCountry = '';
					$one_photo -> commentsCount = intval($one_photo -> commentsCount);
					$one_photo -> likesCount = intval($one_photo -> likesCount);
					if ($one_photo -> liked > 0)
						$one_photo -> liked = 1;
					else
						$one_photo -> liked = 0;
					$one_photo -> usedCount = intval($one_photo -> usedCount);
					$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);

					$output[] = array('photo' => array('photoId' => $one_photo -> photoId, 'studioPhotoId' => $one_photo -> studioPhotoId, 'userId' => $one_photo -> userId, 'uploadedDate' => $one_photo -> uploadedDate, 'caption' => $one_photo -> caption, 'imageUrl' => $one_photo -> imageUrl, 'privacy' => $one_photo -> privacy, 'publishedToWall' => $one_photo -> publishedToWall, 'contestRank' => $one_photo -> contestRank), 'photoUrl' => $one_photo -> imageUrl, 'userName' => $one_photo -> userName, 'userCountry' => $one_photo -> userCountry, 'userProfilePicUrl' => $one_photo -> userProfilePicUrl, 'commentsCount' => $one_photo -> commentsCount, 'liked' => $one_photo -> liked, 'likesCount' => $one_photo -> likesCount, 'tags' => $tags, 'photoOrientation' => $one_photo -> photoOrientation);
				}
				print json_encode(array('status' => 2, 'data' => $output));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this->db->close();
	}

	public function shareToWall() {
		//file_put_contents('test888.txt', serialize($_POST));
		$json_data = $this -> input -> post('data');
		//$data = '{photo:{},tags:[{},{}]}';

		// $json_data = '{"photo" : {  "photoId" : 0,  "caption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0} , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  }]}';

		$arr_data = json_decode($json_data);

		$photo = $arr_data -> photo;
		$tags = $arr_data -> tags;
		$image = $_FILES['img'];

		if (empty($photo) || empty($tags))
			print_r(json_encode(array('status' => -1)));
		else {
			$updated_img_name = '';
			if (isset($image) && $image['error'] == 0) {
				$extension = end(explode('.', $image['name']));
				$updated_img_name = time() . '.' . $extension;

				if (!file_exists(FCPATH . "third_party/uploads/users/" . $photo -> userId)) {
					mkdir(FCPATH . "third_party/uploads/users/" . $photo -> userId, 0777, true);
				}

				$doUpload = @move_uploaded_file($image["tmp_name"], FCPATH . "third_party/uploads/users/" . $photo -> userId . '/' . $updated_img_name);
				if (!$doUpload) {
					print_r(json_encode(array('status' => -2)));
					$this->db->close();
					exit ;
				}
			}
			$photo -> imageUrl = $updated_img_name;

			$photo_arr = $photo;
			unset($photo_arr -> photoId);
			$photo_exists = '';
			if ($photo -> photoId)
				$photo_exists = $this -> main_model -> getData('user_photo', 'photoId', array('photoId' => $photo -> photoId));
			if ($photo_exists)
				$this -> main_model -> update('user_photo', $photo_arr, array('photoId' => $photo -> photoId));
			else
				$photo -> photoId = $this -> main_model -> insert('user_photo', array('studioPhotoId' => $photo -> studioPhotoId, 'userId' => $photo -> userId, 'uploadedDate' => date('Y-m-d H:i:s'), 'caption' => $photo -> caption, 'privacy' => intval($photo -> privacy), 'publishedToWall' => $photo -> publishedToWall, 'contestRank' => $photo -> contestRank, 'imageUrl' => $updated_img_name));

			$parent_thes_arr = $this -> main_model -> getData('studio_photo_thesaurus', 'thesId', array('studioPhotoId' => $photo -> studioPhotoId));
			$parent_thes_id = $parent_thes_arr[0] -> thesId;

			foreach ($tags as $one_tag) {
				if ($one_tag -> tagId)// StudioPhoto tags
				{
					$tag_id = '';
					$tag_id = $this -> main_model -> getData('user_photo_thesaurus', 'id', array('userPhotoId' => $photo -> photoId, 'thesId' => $one_tag -> tagId));
					if (!$tag_id)
						$this -> main_model -> insert('user_photo_thesaurus', array('userPhotoId' => $photo -> photoId, 'thesId' => $one_tag -> tagId));
				} else// User defined tags
				{
					$inserted_thes_id = $this -> main_model -> insert('thesaurus', array('thesName' => $one_tag -> tagName, 'thesParentId' => $parent_thes_id, 'thesType' => '4'));
					$this -> main_model -> insert('user_photo_thesaurus', array('userPhotoId' => $photo -> photoId, 'thesId' => $inserted_thes_id));
				}
			}

			if (!$photo_exists) {
				$this -> main_model -> increment('user', 'noOfPics', 'userId=' . $photo -> userId);
				$this -> main_model -> increment('studio_photo', 'usedCount', 'imageId=' . $photo -> studioPhotoId);
			}
			$photo -> imageUrl = base_url() . 'third_party/uploads/users/' . $photo -> userId . '/' . $photo -> imageUrl;
			print json_encode(array('status' => 2, 'data' => $photo));
		}
		$this->db->close();
	}

	public function deletePhoto() {
		$json_data = $this -> input -> post('data');

		$arr_data = json_decode($json_data);
		$photoId = $arr_data -> userPhotoId;

		if (empty($photoId)) {
			print_r(json_encode(array('status' => -1)));
		} else {

			$photo = $this -> main_model -> getData('user_photo', '*', array('photoId' => $photoId));
			@unlink(FCPATH . 'third_party/uploads/users/' . $photo[0] -> userId . '/' . $photoId);
			$del = $this -> main_model -> delete('user_photo', array('photoId' => $photoId));
			if ($del)
				print json_encode(array('status' => 1));
			else
				print json_encode(array('status' => 1));

			/*
			 $likes = $this->main_model->getData('user_photo_likes','*',array('photoId'=>$photoId));
			 if($likes)
			 {
			 print_r(json_encode(array('status'=>-3,'ruleName'=>'photoHasRelation','errorMessage'=>'This photo can not be deleted as it has likes and comments')));

			 }
			 else
			 {
			 $comments = $this->main_model->getData('user_comment','*',array('photoId'=>$photoId));
			 if($comments)
			 {
			 print_r(json_encode(array('status'=>-3,'ruleName'=>'photoHasRelation','errorMessage'=>'This photo can not be deleted as it has likes and comments')));
			 }
			 else
			 {
			 $photo = $this->main_model->getData('user_photo','*',array('photoId'=>$photoId));
			 //	@unlink(FCPATH.'third_party/uploads/users/'.$photo[0]->userId.'/'.$photoId );
			 $del = $this->main_model->delete('user_photo',array('photoId'=>$photoId));
			 if($del)
			 print json_encode(array('status'=>1));
			 else
			 print json_encode(array('status'=>-1));
			 }
			 }
			 */
		}
		$this->db->close();
	}

	public function updateProfilePic() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$userId = $arr_data -> userId;

		if (empty($userId))
			print_r(json_encode(array('status' => -1)));
		else {
			$image = $_FILES['img'];
			if (is_uploaded_file($_FILES['img']['tmp_name'])) {
				$extension = end(explode('.', $image['name']));
				$updated_name = time() . '.' . $extension;
				$doUpload = @move_uploaded_file($image["tmp_name"], FCPATH . "third_party/uploads/profile/" . $updated_name);

				if (!$doUpload)
					print_r(json_encode(array('status' => -2)));
				else {
					$user = $this -> main_model -> update('user', array('profilePic' => $updated_name), array('userId' => $userId));

					if ($user) {
						$pic = base_url() . "third_party/uploads/profile/" . $updated_name;
						print json_encode(array('status' => 2, 'data' => array('profilePic' => $pic)));

					} else
						print_r(json_encode(array('status' => -2)));
				}
			} else
				print_r(json_encode(array('status' => -2)));
		}
		$this->db->close();
	}

	/*
	 function asd()
	 {

	 $this->db->group_by('studioPhotoId');
	 $photo = $this->main_model->getData('studio_photo_thesaurus','');
	 print_r($photo);
	 foreach($photo as $s)
	 {

	 $this->main_model->insert('studio_photo_thesaurus',array('studioPhotoId'=>$s->studioPhotoId,'thesId'=>259));
	 echo "done <br>";
	 }

	 }
	 */
}
