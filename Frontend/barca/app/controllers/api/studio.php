<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Studio extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

 
	
	public function addPostTest() {
		error_reporting(E_ALL);
		$image = $_FILES['photo'];
		$extension = end(explode('.', $image['name']));
		$updated_name = time() . '.' . $extension;
		
		require_once ('blob.php');
		$blob = new blob();
		echo $blob->container ."-". $blob->uploads_dir_path . $updated_name ."-". $image['tmp_name'];
		echo $blob->blob_upload($blob->container, $blob->uploads_dir_path.'/users/' . $updated_name , file_get_contents($image['tmp_name']));
		
		/*$blob_list = $blob->blobRestProxy->listBlobs($blob->container);
		$blobs = $blob_list->getBlobs();

		foreach($blobs as $blob)
		{
			echo $blob->getName().": ".$blob->getUrl()."<br />";
		}*/
	
	}
	
	public function addPost() {
		//file_put_contents('test99.txt', serialize($_POST));
		$json_data = $this -> input -> post('data');
		//$json_data = ' {  "postId" : 0,  "postPhotoCaption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0 , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  } , {    "tagId" : "0",    "tagName" : "tag 2"  }] } ';
		$arr_data = json_decode($json_data);
		$post = $arr_data -> post;

		$userId = $post -> userId;
		$studioPhotoId = $post -> studioPhotoId;
		$privacy = $post -> privacy;
		$contestRank = $post -> contestRank;
		$postPhotoCaption = $post -> postPhotoCaption;
		$tags = $post -> tags;

		$image = $_FILES['photo'];

		if (empty($userId) || empty($studioPhotoId) || empty($privacy) || !is_uploaded_file($image['tmp_name']))
			print_r(json_encode(array('status' => -1)));
		else {
			if (isset($image) && $image['error'] == 0) {
				
				$extension = end(explode('.', $image['name']));
				$updated_name = time() . '.' . $extension;
				
				require_once ('blob.php');
				$blob = new blob();
				if ( $blob->blob_upload($blob->container, $blob->uploads_dir_path.'/users/' . $userId . '/' . $updated_name , file_get_contents($image['tmp_name'])) ) {
					if (!empty($tags)) {
						foreach ($tags as $tag) {

							/*
							 $check_tag = $this->main_model->getData('thesuarus','name,thesuarusId,type',array('type'=>4,'name'=>$tag->tagName));
							 if($check_tag)
							 {
							 $this->main_model->insert('postthesaurus',array('postId'=>$postId,'thesuarusId'=>$check_tag[0]->thesuarusId,'type'=>$tag->tagType));
							 }
							 else
							 {
							 $thesId = $this->main_model->insert('thesuarus',array('name'=>$tag->tagName,'type'=>4));
							 $this->main_model->insert('postthesaurus',array('postId'=>$postId,'thesuarusId'=>$thesId,'type'=>$tag->tagType));
							 }
							 */

							 if ($tag -> tagType == 2) {
							 	$tags_data[] = $tag -> tagName;

							 }

							}

							if (!empty($tags_data))
								$tags_string = implode(',', $tags_data);
							else
								$tags_string = "";

						} else
						$tags_string = '';
						if ($userId == 1221) {
							$postPhotoCaption = '#MWC 2015';
							$tags_string = 'WMC2015,WMC15';
						}
						$postId = $this -> main_model -> insert('post', array('studioPhotoId' => $studioPhotoId, 'userId' => $userId, 'postPhotoCaption' => $postPhotoCaption, 'privacy' => intval($privacy), 'contestRank' => intval($contestRank), 'postPhotoUrl' => $updated_name, 'tags' => $tags_string));

						$this -> main_model -> increment('user', 'noOfPics', 'userId=' . $userId);
						$pointsArr = $this -> main_model -> getData('points', 'postImageToWallPrivate,postImageToWallFriends,postImageToWallPublic');
						
						$TransTypeID = 'AP_08'; 
						if ($privacy == 4)
						{
							$points = $pointsArr[0] -> postImageToWallPrivate;
							$TransTypeID = 'AP_06';
						}
						elseif ($privacy == 3)
						{
							$points = $pointsArr[0] -> postImageToWallFriends;
							$TransTypeID = 'AP_07';
						}
						elseif ($privacy == 1 || $privacy == 2)
							$points = $pointsArr[0] -> postImageToWallPublic;
						
						$this -> main_model -> incrementByValue('user', 'credit', 'userId=' . $userId, $points);
						
						$arr = array(
							'TransTypeID' => $TransTypeID,
							'UserID' => $userId,
							'TransDate' => date('Y-m-d'),
							'TransTime' => date('H:i:s'),
							'points' => $points
						);
						$this -> main_model -> insert('TransactionLog',$arr);
						
						$this -> main_model -> increment('studiophoto', 'useCount', 'studioPhotoId=' . $studioPhotoId);
						$where = 'post.postId=' . $postId;
						$output = $this -> getPosts($where, 1, $userId);
						if ($output) {

							print json_encode(array('status' => 2, 'data' => $output[0]));
						} else
						print json_encode(array('status' => 2, 'data' => array()));

					} else {
						print_r(json_encode(array('status' => -2)));

					}

				} else {
					print_r(json_encode(array('status' => -2)));
				}

			}
			$this->db->close();
		}

		public function gettags() {
			$json_data = $this -> input -> post('data');
			//$json_data = '{"postId":"4673","studioPhotoId": "689"}';
		//$json_data = ' {  "postId" : 0,  "postPhotoCaption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0 , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  } , {    "tagId" : "0",    "tagName" : "tag 2"  }] } ';
			$arr_data = json_decode($json_data);

			$postId = intval($arr_data -> postId);
			$studioPhotoId = intval($arr_data -> studioPhotoId);
			$userId = intval($arr_data -> userId);
			if (empty($postId)) {
				print_r(json_encode(array('status' => -1)));
			} else {
				$tags = $this -> main_model -> getData('post', 'postId,tags', array('postId' => $postId));
				if (!empty($tags[0] -> tags)) {
					$tagsArr = explode(',', $tags[0] -> tags);
					foreach ($tagsArr as $tag) {
						$tags_data1['tagId'] = 0;
						$tags_data1['tagName'] = $tag;
						$tags_data1['tagType'] = 2;
						
						$tags_data[] = $tags_data1;
					}

				}
				$additional_params_tags['join'] = array('thesuarus', 'thesuarus.thesuarusId = studiophotothesaurus.thesuarusId', '');
				$studio_tags = $this -> main_model -> getBackendData('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId,studiophotothesaurus.thesuarusId as tagId,thesuarus.name as tagName,thesuarus.type', array('studiophotothesaurus.studioPhotoId' => $studioPhotoId, 'thesuarus.type' => 4), 'studiophotothesaurus.thesuarusId', 'desc', '', '', $additional_params_tags);

				if ($studio_tags) {
					foreach ($studio_tags as $tag) {
						$tag -> tagType = 1;
						$tag -> tagId = intval($tag -> tagId);
						unset($tag -> studioPhotoId);
						unset($tag -> type);
						$tags_data[] = $tag;
					}
				}

				if (empty($tags_data))
					$tags_data = array();
				$reported = $this -> main_model -> getData('postreport', '*', array('postId' => $postId, 'userId' => $userId));
				if ($reported)
					$reported = 1;
				else
					$reported = 0;

				print_r(json_encode(array('status' => 2, 'data' => array('tags' => $tags_data, 'reported' => $reported)), JSON_UNESCAPED_UNICODE));

			}
			$this->db->close();
		}

		public function shareSocailMedia() {
			$json_data = $this -> input -> post('data');
		//$json_data = ' {  "postId" : 0,  "postPhotoCaption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0 , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  } , {    "tagId" : "0",    "tagName" : "tag 2"  }] } ';
			$arr_data = json_decode($json_data);

			$userId = $arr_data -> userId;
			$studioPhotoId = $arr_data -> studioPhotoId;
			if (empty($userId)) {
				print_r(json_encode(array('status' => -1)));
			} else {
				$where = "userId = " . $userId;
				$points = $this -> main_model -> getData('points', 'shareImageSocialMedia');
				$this -> main_model -> incrementByValue('user', 'credit', $where, $points[0]->shareImageSocialMedia);
				
				$arr = array(
					'TransTypeID' => 'AP_04',
					'UserID' => $userId,
					'TransDate' => date('Y-m-d'),
					'TransTime' => date('H:i:s'),
					'points' => $points[0]->shareImageSocialMedia
				);
				$this -> main_model -> insert('TransactionLog',$arr);
				
				print_r(json_encode(array('status' => 1)));
			}
			$this->db->close();
		}

		public function sendPosterRequest() {
			$json_data = $this -> input -> post('data');
		//$json_data = ' {  "postId" : 0,  "postPhotoCaption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0 , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  } , {    "tagId" : "0",    "tagName" : "tag 2"  }] } ';
			$arr_data = json_decode($json_data);

			$userId = $arr_data -> userId;
			$userEmail = $arr_data -> userEmail;
		//$quantity = $arr_data->quantity;

			$maskedPhoto = $_FILES['maskedPhoto'];
			$userPhoto = $_FILES['userPhoto'];

			if (empty($userId) || empty($userEmail) || !is_uploaded_file($userPhoto['tmp_name']))
				print_r(json_encode(array('status' => -1)));
			else {
				if (isset($userPhoto) && $userPhoto['error'] == 0) {
					$extension = end(explode('.', $userPhoto['name']));
					$updated_name = time() . '.' . $extension;
					
					require_once ('blob.php');
					if ( $blob->blob_upload($blob->container, $blob->uploads_dir_path.'/posters/users/' . $userId . '/' . $updated_name, file_get_contents($userPhoto['tmp_name'])) ) {
					
						if (isset($maskedPhoto) && $maskedPhoto['error'] == 0) {
							$extension2 = end(explode('.', $maskedPhoto['name']));
							$updated_name2 = time() . '.' . $extension;
							if ( $blob->blob_upload($blob->container, $blob->uploads_dir_path.'/posters/users/' . $userId . '/' . $updated_name2, file_get_contents($userPhoto['tmp_name'])) ) {
								$ins = $this -> main_model -> insert('posterrequest', array('userId' => $userId, 'email' => $userEmail, 'quantity' => 1, 'userPhotoUrl' => $updated_name, 'maskedPhotoUrl' => $updated_name2));
								if ($ins)
									print_r(json_encode(array('status' => 1)));
							} else
							print_r(json_encode(array('status' => -2)));

						} else
						print_r(json_encode(array('status' => -2)));
					} else
					print_r(json_encode(array('status' => -2)));

				} else
				print_r(json_encode(array('status' => -2)));

			}
			$this->db->close();
		}

		public function getStudioFolders() {

			$where = '(type = 1 OR type = 2) AND is_hidden = 0';
			$folders = $this -> main_model -> getData('thesuarus', 'thesuarusId as folderId,name as folderName,photoUrl as folderPicUrl ,rank , type as folderType', $where, 'rank asc,createdDate desc');
			if ($folders) {
				foreach ($folders as $one_folder) {
					$one_folder -> folderId = intval($one_folder -> folderId);
					$one_folder -> folderType = intval($one_folder -> folderType);
					if ($one_folder -> folderPicUrl)
						$one_folder -> folderPicUrl = base_url() . 'third_party/uploads/' . $one_folder -> folderPicUrl;
					$one_folder -> noOfPics = intval($this -> get_no_of_photos($one_folder -> folderId, $one_folder -> folderType));
				}
				print json_encode(array('status' => 2, 'data' => $folders));
			} else
			print_r(json_encode(array('status' => 2, 'data' => array())));
			$this->db->close();
		}

		public function getStudioFolderPhotos() {
			$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"1","filterBy":"1","seasons":[]}';
			$arr_data = json_decode($json_data);

			$userId = intval($arr_data -> userId);
			$folderId = intval($arr_data -> folderId);
			$pageNo = intval($arr_data -> pageNo);

			$seasons = $arr_data -> seasons;
			$sortBy = intval($arr_data -> filterBy);
			if (empty($pageNo))
				print_r(json_encode(array('status' => -1)));
			else {
				$additional_params = $output = array();

				$start = ($pageNo - 1) * 24;

				$order_by = 'uploadedDate';
				$order = 'desc';
				if ($sortBy == 2) {
					$order_by = 'useCount';
					$order = 'desc';
				}

				if (!empty($seasons))
					$additional_params['where_in'] = array('thesuarus.thesuarusId', implode(',', $seasons));
				if ($folderId > 0) {
					$folderType = $this -> main_model -> getData('thesuarus', 'thesuarusId,type', array('thesuarusId' => $folderId));
					if ($folderType) {
						if ($folderType[0] -> type == 1)
							$where = array('studiophotothesaurus.thesuarusId' => $folderId, 'studiophoto.captureType' => 1, 'studiophoto.isHidden' => 'false');
						else if ($folderType[0] -> type == 2)
							$where = array('studiophotothesaurus.thesuarusId' => $folderId, 'studiophoto.captureType' => 2, 'studiophoto.isHidden' => 'false');
					} else
					$where = array('studiophotothesaurus.thesuarusId' => $folderId , 'studiophoto.isHidden' => 'false');

				} else
				$where = array('studiophoto.isHidden' => 'false');

				$additional_params['join'] = array('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId=studiophoto.studioPhotoId', 'left');
				$additional_params['join2'] = array('thesuarus', 'studiophotothesaurus.thesuarusId=thesuarus.thesuarusId', 'left');
			//$additional_params['join3']=array('purchasedphoto','purchasedphoto.studioPhotoId=studiophoto.studioPhotoId','left');
				$this -> db -> group_by('studiophoto.studioPhotoId');
				$photos = $this -> main_model -> getBackendData('studiophoto', 'studiophoto.*,(select id from purchasedphoto where userId =' . $userId . ' AND studioPhotoId=studiophoto.studioPhotoId limit 1 ) as isPurchased', $where, 'studiophoto.' . $order_by, $order, 24, $start, $additional_params);
				
				if ($photos) {
					$additional_params = array();
					foreach ($photos as $one_photo) {
						unset($one_photo -> usedCount);
						if ($one_photo -> photoUrl)
							$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;

						$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
						$one_photo -> useCount = intval($one_photo -> useCount);
						//$one_photo -> requiredPoints = intval($one_photo -> requiredPoints);
						$one_photo -> isPremium = intval($one_photo -> isPremium);
						if($one_photo -> requiredPoints == 0)
						{
							$points = $this->main_model->getData('points','useImageNormal');
							$normalPoints =  $points[0]->useImageNormal;

							$premPoints = $this->main_model->getData('points','useImagePremium');
							$premiumPoints =  $premPoints[0]->useImagePremium;
							if($one_photo -> isPremium == 0)
							{
								$one_photo -> requiredPoints = intval($normalPoints);	
							} else {
								$one_photo -> requiredPoints = intval($premiumPoints);	
							}
						} else 
						{
							$one_photo -> requiredPoints = intval($one_photo -> requiredPoints);
						}
						$one_photo -> folderId = intval($folderId);
						$one_photo -> uploadedDate = strtotime($one_photo -> uploadedDate);

						$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
						$one_photo -> captureType = intval($one_photo -> captureType);
						$one_photo -> shadows = array();

						if ($one_photo -> isPurchased)
							$one_photo -> isPurchased = 1;
						else
							$one_photo -> isPurchased = 0;

						unset($one_photo -> userId);
					/* tags old
					 $additional_params1=array('thesuarus','studiophotothesaurus.studioPhotoId=thesuarus.thesuarusId','left');
					 $tagsIDs = $this->main_model->getBackendData('studiophotothesaurus','thesuarusId',array('studiophotothesaurus.studioPhotoId'=>$one_photo->imageId),'','','','','');
					 if($tagsIDs)
					 {
					 foreach($tagsIDs  as $tagsID)
					 $ids[] = $tagsID->thesuarusId;
					 }

					 $addition['where_in'] = array('thesuarusId',$ids);
					 $tags = $this->main_model->getBackendData('thesuarus','thesuarusId as tagId ,name as tagText',array('type'=>'4'),'','','','',$addition);

					 if(!$tags)
					 $tags = array();
					 */

					// tags section for studio photo ids and photo ids
					/*
					 $additional_params_tags['join'] = array('thesuarus','thesuarus.thesuarusId = studiophotothesaurus.thesuarusId','left');
					 $studio_tags = $this->main_model->getBackendData('studiophotothesaurus','studiophotothesaurus.studioPhotoId,studiophotothesaurus.thesuarusId as tagId,thesuarus.name as tagName,thesuarus.type',array('studiophotothesaurus.studioPhotoId'=>$one_photo->studioPhotoId,'type'=>4),'studiophotothesaurus.thesuarusId','desc','','',$additional_params_tags);
					 if($studio_tags){
					 foreach ($studio_tags as $tag) {
					 $tag -> tagType = 1;
					 $tag -> tagId = intval($tag -> tagId);
					 unset($tag -> studioPhotoId);
					 unset($tag -> type);
					 $tags[] = $tag;
					 }

					 }

					 // thesures tags for post photoid
					 */
					/*
					 $additional_params_tags_post['join'] = array('thesuarus','thesuarus.thesuarusId = postthesaurus.postId','left');
					 $post_tags = $this->main_model->getBackendData('postthesaurus','postthesaurus.postId,postthesaurus.thesuarusId as tagId,thesuarus.name as tagName,postthesaurus.type as type ,thesuarus.type as thestype',array('postthesaurus.postId'=>$one_photo->postId,'thesuarus.type'=>4),'postthesaurus.postId','desc','','',$additional_params_tags_post);

					 if($post_tags){
					 foreach ($post_tags as $tag) {
					 $tag -> tagType = $tag -> type;
					 $tag -> tagId = intval($tag -> tagId);
					 unset($tag -> postId);
					 unset($tag -> type);
					 $tags[] = $tag;
					 }
					 }

					 // end of post tags
					 */

					 $tags = array();

					 $one_photo -> tags = $tags;
					 $output[] = $one_photo;
					}
					print json_encode(array('status' => 2, 'data' => $output));
				} else
				print json_encode(array('status' => 2, 'data' => array()));
			}
			$this->db->close();
		}

		public function getUserPurchasedPhotos() {
			$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"1","userId":"711"}';
			$arr_data = json_decode($json_data);

			$userId = intval($arr_data -> userId);
			$pageNo = intval($arr_data -> pageNo);

			if (empty($pageNo) || empty($userId))
				print_r(json_encode(array('status' => -1)));
			else {
				$additional_params = $output = array();

				$start = ($pageNo - 1) * 24;

				$order_by = 'uploadedDate';
				$order = 'desc';

				$additional_params['join'] = array('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId=studiophoto.studioPhotoId', 'left');
				$additional_params['join2'] = array('thesuarus', 'studiophotothesaurus.thesuarusId=thesuarus.thesuarusId', 'left');
				$additional_params['join3'] = array('purchasedphoto', 'purchasedphoto.studioPhotoId=studiophoto.studioPhotoId', 'left');
				$this -> db -> group_by('studiophoto.studioPhotoId');
				$photos = $this -> main_model -> getBackendData('studiophoto', 'studiophoto.*', array('purchasedphoto.userId' => $userId), 'studiophoto.' . $order_by, $order, 24, $start, $additional_params);

				if ($photos) {
					$additional_params = array();
					foreach ($photos as $one_photo) {
						unset($one_photo -> usedCount);
						if ($one_photo -> photoUrl)
							$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;

						$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
						$one_photo -> useCount = intval($one_photo -> useCount);
						$one_photo -> requiredPoints = intval($one_photo -> requiredPoints);
						$one_photo -> isPremium = intval($one_photo -> isPremium);
						$one_photo -> folderId = intval($folderId);
						$one_photo -> uploadedDate = strtotime($one_photo -> uploadedDate);

						$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
						$one_photo -> captureType = intval($one_photo -> captureType);
						$one_photo -> shadows = array();

						$one_photo -> isPurchased = 1;

						unset($one_photo -> userId);
					/* tags old
					 $additional_params1=array('thesuarus','studiophotothesaurus.studioPhotoId=thesuarus.thesuarusId','left');
					 $tagsIDs = $this->main_model->getBackendData('studiophotothesaurus','thesuarusId',array('studiophotothesaurus.studioPhotoId'=>$one_photo->imageId),'','','','','');
					 if($tagsIDs)
					 {
					 foreach($tagsIDs  as $tagsID)
					 $ids[] = $tagsID->thesuarusId;
					 }

					 $addition['where_in'] = array('thesuarusId',$ids);
					 $tags = $this->main_model->getBackendData('thesuarus','thesuarusId as tagId ,name as tagText',array('type'=>'4'),'','','','',$addition);

					 if(!$tags)
					 $tags = array();
					 */

					// tags section for studio photo ids and photo ids

					 $tags = array();
					// end of post tags
					 $one_photo -> tags = $tags;
					 $output[] = $one_photo;
					}
					print json_encode(array('status' => 2, 'data' => $output));
				} else
				print json_encode(array('status' => 2, 'data' => array()));
			}
			$this->db->close();
		}

		public function getSeasons() {
			$seasons = $this -> main_model -> getData('thesuarus', 'thesuarusId as seasonId,name as seasonName', array('type' => '3'), 'name', 'asc');
			if ($seasons) {
				foreach ($seasons as $one_season) {
					$one_season -> seasonId = intval($one_season -> seasonId);
				}
				print json_encode(array('status' => 2, 'data' => $seasons));
			} else
			print_r(json_encode(array('status' => 2, 'data' => array())));
			$this->db->close();
		}

		public function updateUserCredit() {
			$json_data = $this -> input -> post('data');
		//$json_data ='{"userId":"711","creditValue":"100"}';
			$arr_data = json_decode($json_data);

			$userId = intval($arr_data -> userId);
			$creditValue = intval($arr_data -> creditValue);
			$TransID = $arr_data -> TransID;
			$deviceType = $arr_data -> deviceType;
			$deviceToken = $arr_data -> deviceToken;
			//$TransID=$deviceType=$deviceToken='';
			
			if (empty($userId) || empty($creditValue) || empty($TransID) || empty($deviceType) || empty($deviceToken))
			//if (empty($userId) || empty($creditValue))
				print_r(json_encode(array('status' => -1)));
			else {
				$trans = $this -> main_model -> getData('TransactionLog', 'TransLogID', array('TransactionID' => $TransID));
				if(isset($trans[0]->TransLogID))
					print_r(json_encode(array('status' => -10)));
				else
				{
					$where = "userId = '" . $userId . "'";
					$update = $this -> main_model -> incrementByValue('user', 'credit', $where, $creditValue);
					
					if ($update)
					{
						$arr = array(
							'TransTypeID' => 'AP_17',
							'TransactionID' => $TransID,
							'UserID' => $userId,
							'DeviceType' => $deviceType,
							'DeviceID' => $deviceToken,
							'TransDate' => date('Y-m-d'),
							'TransTime' => date('H:i:s'),
							'points' => $creditValue
						);
						$this -> main_model -> insert('TransactionLog',$arr);
						
						print_r(json_encode(array('status' => 1)));
					}
					else
						print_r(json_encode(array('status' => -1)));
				}
			}
			$this->db->close();
		}

		public function getUserCredit() {
			$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","folderId":"1","userId":"711"}';
			$arr_data = json_decode($json_data);

			$userId = intval($arr_data -> userId);

			if (empty($userId))
				print_r(json_encode(array('status' => -1)));
			else {
				$credit = $this -> main_model -> getData('user', 'userId,credit', array('userId' => $userId));
				if ($credit)
					print_r(json_encode(array('status' => 2, 'data' => array('credit' => $credit[0] -> credit))));
				else
					print_r(json_encode(array('status' => 2, 'data' => array())));
			}
			$this->db->close();
		}

		public function purchaseStudioPhoto() {
			$json_data = $this -> input -> post('data');
			$arr_data = json_decode($json_data);
			$userId = intval($arr_data -> userId);
			$studioPhotoId = intval($arr_data -> studioPhotoId);

			if (empty($userId) || empty($studioPhotoId)) {
				print_r(json_encode(array('status' => -1)));
			} else {
			// check for this photo is purchased befobr
				$found_before = $this -> main_model -> getData('purchasedphoto', '*', array('userId' => $userId, 'studioPhotoId' => $studioPhotoId));
				if (!$found_before) {
					$pointsArr = $this -> main_model -> getData('points', 'useImagePremium,useImageNormal');
					$premium_img = $this -> main_model -> getData('studiophoto', 'studioPhotoId,isPremium', array('studioPhotoId' => $studioPhotoId));
					if ($premium_img[0] -> isPremium > 0)
						$points = ($pointsArr[0] -> useImagePremium) * (-1);
					else
						$points = ($pointsArr[0] -> useImageNormal) * (-1);

					$this -> main_model -> incrementByValue('user', 'credit', "userId =" . $userId, $points);
					
					$arr = array(
						'TransTypeID' => 'AP_09',
						'UserID' => $userId,
						'TransDate' => date('Y-m-d'),
						'TransTime' => date('H:i:s'),
						'points' => $points
					);
					$this -> main_model -> insert('TransactionLog',$arr);
					
					$ins = $this -> main_model -> insert('purchasedphoto', array('userId' => $userId, 'studioPhotoId' => $studioPhotoId));
					if ($ins) {

						print_r(json_encode(array('status' => 1)));
					} else
					print_r(json_encode(array('status' => -2)));
				} else
				print_r(json_encode(array('status' => 1)));
			}
			$this->db->close();
		}

		public function getStudioPhoto() {
			$json_data = $this -> input -> post('data');
			$arr_data = json_decode($json_data);

			$studioPhotoId = intval($arr_data -> studioPhotoId);
			$userId = intval($arr_data -> userId);

			if (empty($studioPhotoId)) {
				print_r(json_encode(array('status' => -1)));
			} else {
				$where = "studiophoto.studioPhotoId = " . $studioPhotoId;
				$additional_params['join'] = array('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId=studiophoto.studioPhotoId', 'left');
				$additional_params['join2'] = array('thesuarus', 'studiophotothesaurus.thesuarusId=thesuarus.thesuarusId', 'left');
			// $additional_params['join3']=array('purchasedphoto','purchasedphoto.studioPhotoId=studiophoto.studioPhotoId','left');
				$this -> db -> group_by('studiophoto.studioPhotoId');
				$photos = $this -> main_model -> getBackendData('studiophoto', 'studiophoto.*,(select id from purchasedphoto where userId =' . $userId . ' AND studioPhotoId=studiophoto.studioPhotoId limit 1 ) as isPurchased', $where, '', '', 1, $start, $additional_params);

				if ($photos) {
				// $additional_params = array();
					foreach ($photos as $one_photo) {
						unset($one_photo -> usedCount);
						if ($one_photo -> photoUrl)
							$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;

						if(intval($one_photo -> isPremium) == 1)
						{
							$points = $this->main_model->getData('points', 'useImagePremium');

						} else 
						{
							$points = $this->main_model->getData('points', 'useImageNormal');
						}

						$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
						$one_photo -> useCount = intval($one_photo -> useCount);
						$one_photo -> requiredPoints = intval($points);
						$one_photo -> isPremium = intval($one_photo -> isPremium);
						$one_photo -> folderId = intval($folderId);
						$one_photo -> uploadedDate = strtotime($one_photo -> uploadedDate);

						$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
						$one_photo -> captureType = intval($one_photo -> captureType);
						$one_photo -> shadows = array();

						if ($one_photo -> isPurchased > 0)
							$one_photo -> isPurchased = 1;
						else
							$one_photo -> isPurchased = 0;

						unset($one_photo -> userId);

						$tags = array();

						$one_photo -> tags = $tags;
						$output[] = $one_photo;
					}
					print json_encode(array('status' => 2, 'data' => $output));
				} else
				print json_encode(array('status' => 2, 'data' => array()));
			}
			$this->db->close();
		}

		public function useStudioPhoto() {
			$json_data = $this -> input -> post('data');
			$arr_data = json_decode($json_data);

			$studioPhotoId = intval($arr_data -> studioPhotoId);
			$userId = intval($arr_data -> userId);

			if (empty($studioPhotoId)) {
				print_r(json_encode(array('status' => -1)));
			} else {

				$checkPhoto = $this -> main_model -> getData('purchasedphoto', '*', 'studioPhotoId = "' . $studioPhotoId . '" AND userId = "' . $userId . '" ');
				if (count($checkPhoto) > 0 && $checkPhoto == true) {

					$where = "studiophoto.studioPhotoId = " . $studioPhotoId;
					$additional_params['join'] = array('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId=studiophoto.studioPhotoId', 'left');
					$additional_params['join2'] = array('thesuarus', 'studiophotothesaurus.thesuarusId=thesuarus.thesuarusId', 'left');
				// $additional_params['join3']=array('purchasedphoto','purchasedphoto.studioPhotoId=studiophoto.studioPhotoId','left');
					$this -> db -> group_by('studiophoto.studioPhotoId');
					$photos = $this -> main_model -> getBackendData('studiophoto', 'studiophoto.*,(select id from purchasedphoto where userId =' . $userId . ' AND studioPhotoId=studiophoto.studioPhotoId limit 1 ) as isPurchased', $where, '', '', 1, $start, $additional_params);

					if ($photos) {
					// $additional_params = array();
						foreach ($photos as $one_photo) {
							unset($one_photo -> usedCount);
							if ($one_photo -> photoUrl)
								$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;

							if(intval($one_photo -> isPremium) == 1)
							{
								$points = $this->main_model->getData('points', 'useImagePremium');

							} else 
							{
								$points = $this->main_model->getData('points', 'useImageNormal');
							}

							$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
							$one_photo -> useCount = intval($one_photo -> useCount);
							$one_photo -> requiredPoints = intval($points);
							$one_photo -> isPremium = intval($one_photo -> isPremium);
							$one_photo -> folderId = intval($folderId);
							$one_photo -> uploadedDate = strtotime($one_photo -> uploadedDate);

							$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
							$one_photo -> captureType = intval($one_photo -> captureType);
							$one_photo -> shadows = array();

							if ($one_photo -> isPurchased > 0)
								$one_photo -> isPurchased = 1;
							else
								$one_photo -> isPurchased = 0;

							unset($one_photo -> userId);

							$tags = array();

							$one_photo -> tags = $tags;
							$output[] = $one_photo;
						}
						print json_encode(array('status' => 2, 'data' => $output));
					} else {
						print json_encode(array('status' => 2, 'data' => array()));
					}
				} else {
				//Check for premium Image
					$checkPremium = $this -> main_model -> getData('studiophoto', 'studioPhotoId, isPremium', 'studioPhotoId = "' . $studioPhotoId . '"');
					$isPhotoPremium = $checkPremium[0] -> isPremium;

					if ($isPhotoPremium == 0 || empty($isPhotoPremium)) {
						$checkUserCredit = $this -> main_model -> getData('user', 'userId, credit', 'userId = "' . $userId . '" ');
						if ($checkUserCredit) {
							$userCredit = $checkUserCredit[0] -> credit;
							$checkPhotoCredit = $this -> main_model -> getData('points', 'useImageNormal');
							$photoCredit = $checkPhotoCredit[0] -> useImageNormal;
							if ($userCredit >= $photoCredit) {
								$where = "studiophoto.studioPhotoId = " . $studioPhotoId;
								$additional_params['join'] = array('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId=studiophoto.studioPhotoId', 'left');
								$additional_params['join2'] = array('thesuarus', 'studiophotothesaurus.thesuarusId=thesuarus.thesuarusId', 'left');
								$this -> db -> group_by('studiophoto.studioPhotoId');
								$photos = $this -> main_model -> getBackendData('studiophoto', 'studiophoto.*,(select id from purchasedphoto where userId =' . $userId . ' AND studioPhotoId=studiophoto.studioPhotoId limit 1 ) as isPurchased', $where, '', '', 1, $start, $additional_params);

								if ($photos) {
									$creditValue = ($userCredit - $photoCredit);
									$update = $this -> main_model -> update('user', array('credit' => $creditValue), array('userId' => $userId));

									foreach ($photos as $one_photo) {
										unset($one_photo -> usedCount);
										if ($one_photo -> photoUrl)
											$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;

										$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
										$one_photo -> useCount = intval($one_photo -> useCount);
										$one_photo -> requiredPoints = intval($one_photo -> requiredPoints);
										$one_photo -> isPremium = intval($one_photo -> isPremium);
										$one_photo -> folderId = intval($folderId);
										$one_photo -> uploadedDate = strtotime($one_photo -> uploadedDate);

										$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
										$one_photo -> captureType = intval($one_photo -> captureType);
										$one_photo -> shadows = array();

										if ($one_photo -> isPurchased > 0)
											$one_photo -> isPurchased = 1;
										else
											$one_photo -> isPurchased = 0;

										unset($one_photo -> userId);

										$tags = array();

										$one_photo -> tags = $tags;
										$output[] = $one_photo;
									}
									print json_encode(array('status' => 2, 'data' => $output));
								} else {
									print json_encode(array('status' => 2, 'data' => array()));
								}
							} else {
								print json_encode(array('status' => -4));
							}
						}
					} else {
					//User Premium Check
						$checkUserPremium = $this -> main_model -> getData('user', 'isPremium', 'userId = "' . $userId . '"');
						$isUserPremium = $checkUserPremium[0] -> isPremium;
						if ($isUserPremium == 0) {
							print json_encode(array('status' => -5));
						} else {
							$checkUserCredit = $this -> main_model -> getData('user', 'userId, credit', 'userId = "' . $userId . '" ');
							if ($checkUserCredit) {
								$userCredit = $checkUserCredit[0] -> credit;
								$checkPhotoCredit = $this -> main_model -> getData('points', 'useImagePremium');
								$photoCredit = $checkPhotoCredit[0] -> useImagePremium;
								if ($userCredit >= $photoCredit) {
									$where = "studiophoto.studioPhotoId = " . $studioPhotoId;
									$additional_params['join'] = array('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId=studiophoto.studioPhotoId', 'left');
									$additional_params['join2'] = array('thesuarus', 'studiophotothesaurus.thesuarusId=thesuarus.thesuarusId', 'left');
									$this -> db -> group_by('studiophoto.studioPhotoId');
									$photos = $this -> main_model -> getBackendData('studiophoto', 'studiophoto.*,(select id from purchasedphoto where userId =' . $userId . ' AND studioPhotoId=studiophoto.studioPhotoId limit 1 ) as isPurchased', $where, '', '', 1, $start, $additional_params);

									if ($photos) {
										$creditValue = ($userCredit - $photoCredit);
										$update = $this -> main_model -> update('user', array('credit' => $creditValue), array('userId' => $userId));

										foreach ($photos as $one_photo) {
											unset($one_photo -> usedCount);
											if ($one_photo -> photoUrl)
												$one_photo -> photoUrl = base_url() . 'third_party/uploads/' . $one_photo -> photoUrl;

											$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
											$one_photo -> useCount = intval($one_photo -> useCount);
											$one_photo -> requiredPoints = intval($one_photo -> requiredPoints);
											$one_photo -> isPremium = intval($one_photo -> isPremium);
											$one_photo -> folderId = intval($folderId);
											$one_photo -> uploadedDate = strtotime($one_photo -> uploadedDate);

											$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
											$one_photo -> captureType = intval($one_photo -> captureType);
											$one_photo -> shadows = array();

											if ($one_photo -> isPurchased > 0)
												$one_photo -> isPurchased = 1;
											else
												$one_photo -> isPurchased = 0;

											unset($one_photo -> userId);

											$tags = array();

											$one_photo -> tags = $tags;
											$output[] = $one_photo;
										}
										print json_encode(array('status' => 2, 'data' => $output));
									} else {
										print json_encode(array('status' => 2, 'data' => array()));
									}
								} else {
									print json_encode(array('status' => -4));
								}
							}
						}
					//End of user Premium check
					}
				//End of premium Image check
				}
			}
			$this->db->close();
		}

		private function get_no_of_photos($id, $type) {
			$additionalParams['join'] = array('studiophoto', 'studiophoto.studioPhotoId=studiophotothesaurus.studioPhotoId');
			$this -> db -> group_by('studiophoto.studioPhotoId');
			$no = $this -> main_model -> getBackendData('studiophotothesaurus', 'studiophoto.captureType,studiophoto.isHidden,studiophotothesaurus.thesuarusId,studiophotothesaurus.studioPhotoId', array('thesuarusId' => $id, 'captureType' => $type, 'studiophoto.isHidden' => 'false'), '', '', '', '', $additionalParams);
			if ($no)
				return count($no);
			else
				return 0;
		}

		private function userPic($url) {
			if (strpos($url, 'http') !== FALSE)
				return $url;
			else {
				if (empty($url))
					return '';
				else
					return base_url() . 'third_party/uploads/profile/' . $url;
			}

		}

		public function getPosts($where = '', $n = 10, $userId = 817, $lastRequestTime = '', $timeFilter = '', $favorite = 0, $countryList = array()) {
		//$lastRequestTime = "2014-11-7 11:44:38";
		//  $timeFilter = 0;
		//  print_r($countryList);

			$select = 'distinct post.postId as tmpId,post.*,user.fullName as userName,studiophoto.useCount,studiophoto.photoOrientation,user.profilePic as userProfilePicUrl,country.countryName as userCountry,country.countryId as countryId,studiophotothesaurus.thesuarusId ,hidden,postPhotoCaption, ';
			$select .= '(select count(`likeId`) from  postlike where `postId` = `post`.postId AND `userId` = ' . $userId . ') as liked ,';
			$select .= '(select isFollow from  userfans where userfans.fanId = user.userId AND `userId` = ' . $userId . ') as following,';
			$select .= '(select isFavourite from  userfans where userfans.fanId = user.userId AND `userId` = ' . $userId . ') as favorite, ';
			$select .= '(select reportId from  postreport where postreport.postId = post.postId AND `userId` = ' . $userId . ') as reported';

			$additional_params['join'] = array('user', 'user.userId = post.userId', 'left');
			$additional_params['join2'] = array('studiophoto', 'post.studioPhotoId = studiophoto.studioPhotoId', 'left');
			$additional_params['join3'] = array('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId = post.studioPhotoId', 'left');
			$additional_params['join4'] = array('thesuarus', 'thesuarus.thesuarusId = studiophotothesaurus.thesuarusId', 'left');
			$additional_params['join6'] = array('country', 'user.countryId = country.countryId', 'left');

			$this -> db -> group_by('post.postId');
			$photos = $this -> main_model -> getBackendData('post', $select, $where, '', '', '', '', $additional_params);

			if ($photos) {
				$output = array();
				foreach ($photos as $one_photo) {

				// tags section for studio photo ids and photo ids

					$tags = array();

				// end of post tags

					if ($one_photo -> postPhotoUrl)
						$one_photo -> postPhotoUrl = base_url() . 'third_party/uploads/users/' . $one_photo -> userId . '/' . $one_photo -> postPhotoUrl;
					else
						$one_photo -> postPhotoUrl = '';

					$one_photo -> postId = intval($one_photo -> postId);
					$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
					$one_photo -> userId = intval($one_photo -> userId);
					$one_photo -> privacy = intval($one_photo -> privacy);

					$one_photo -> contestRank = intval($one_photo -> contestRank);
					$one_photo -> originalPostId = intval($one_photo -> originalPostId);

					if (!$one_photo -> creationTime)
						$one_photo -> creationTime = '';

					if (strpos($one_photo -> userProfilePicUrl, 'http') !== FALSE)
						$one_photo -> userProfilePicUrl = $one_photo -> userProfilePicUrl;
					else {

						$one_photo -> userProfilePicUrl = base_url() . 'third_party/uploads/profile/' . $one_photo -> userProfilePicUrl;

					}
					if (empty($one_photo -> userProfilePicUrl))
						$one_photo -> userProfilePicUrl = '';

					if (!$one_photo -> userName)
						$one_photo -> userName = '';
					if (!$one_photo -> userCountry)
						$one_photo -> userCountry = '';
					$one_photo -> commentCount = intval($one_photo -> commentCount);
					$one_photo -> likeCount = intval($one_photo -> likeCount);

					$one_photo -> useCount = intval($one_photo -> useCount);
					$one_photo -> reportCount = intval($one_photo -> reportCount);
					$one_photo -> shareCount = intval($one_photo -> shareCount);
					$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
					$one_photo -> contestRank = intval($one_photo -> contestRank);
					$one_photo -> liked = intval($one_photo -> liked);
					$one_photo -> hidden = intval($one_photo -> hidden);
					$one_photo -> following = intval($one_photo -> following);
					$one_photo -> favorite = intval($one_photo -> favorite);
					if ($one_photo -> reported > 0)
						$one_photo -> reported = 1;
					else
						$one_photo -> reported = 0;

				// original post by
					if ($one_photo -> originalPostId > 0) {
						$userOid = $this -> main_model -> getData('post', 'postId,userId', array('postId' => $one_photo -> originalPostId));
						$userOName = $this -> main_model -> getData('user', 'userId,fullName,profilePic,socialMediaId', array('userId' => $userOid[0] -> userId));
						if ($userOid[0] -> userId == $userId) {
							$one_photo -> rePosted = 1;
							$one_photo -> rePostedBy = $one_photo -> fullName;
							$one_photo -> rePosterProfilePic = $one_photo -> userProfilePicUrl;
							$one_photo -> rePosterId = $one_photo -> userId;
							$one_photo -> rePosterIsFollowing = 0;
						} else {
							$one_photo -> rePosted = 0;
							$one_photo -> rePostedBy = $userOName[0] -> fullName;
							if ($userOName[0] -> socialMediaId > 0) {
								if (!empty($userOName[0] -> profilePic))
									$one_photo -> rePosterProfilePic = $one_photo -> userProfilePicUrl;
								else
									$one_photo -> rePosterProfilePic = "";
							} else {
								if (!empty($userOName[0] -> profilePic))
									$one_photo -> rePosterProfilePic = base_url() . 'third_party/uploads/profile/' . $one_photo -> userProfilePicUrl;
								else
									$one_photo -> rePosterProfilePic = "";
							}

							$one_photo -> rePosterId = $userOName[0] -> userId;
							$one_photo -> rePosterIsFollowing = 0;
						}

					} else {
						$one_photo -> rePosted = 0;
						$one_photo -> rePostedBy = "";
						$one_photo -> rePosterProfilePic = "";
						$one_photo -> rePosterId = 0;
						$one_photo -> rePosterIsFollowing = 0;
					}

					if ($favorite == 1) {
						if ($one_photo -> favorite == 1)
							$output[] = array(

						//'photoUrl' => $one_photo->photoUrl,
								'postId' => $one_photo -> postId, 'originalPostId' => $one_photo -> originalPostId, 'lastUpdateTime' => strtotime($one_photo -> lastUpdateTime), 'userId' => $one_photo -> userId, 'studioPhotoId' => $one_photo -> studioPhotoId, 'postPhotoUrl' => $one_photo -> postPhotoUrl, 'userName' => $one_photo -> userName, 'userCountry' => $one_photo -> userCountry, 'userProfilePicUrl' => $one_photo -> userProfilePicUrl, 'creationTime' => strtotime($one_photo -> creationTime), 'liked' => $one_photo -> liked, 'commentCount' => $one_photo -> commentCount, 'likeCount' => $one_photo -> likeCount, 'reportCount' => $one_photo -> reportCount, 'photoOrientation' => $one_photo -> photoOrientation, 'privacy' => $one_photo -> privacy, 'contestRank' => $one_photo -> contestRank, 'tags' => $tags, 'reported' => $one_photo -> reported, 'hidden' => $one_photo -> hidden, 'postPhotoCaption' => $one_photo -> postPhotoCaption, 'rePosted' => $one_photo -> rePosted, 'following' => $one_photo -> following, 'favorite' => $one_photo -> favorite, 'rePostedBy' => $one_photo -> rePostedBy, 'rePosterProfilePic' => $one_photo -> rePosterProfilePic, 'rePosterId' => $one_photo -> rePosterId, 'rePosterIsFollowing' => $one_photo -> rePosterIsFollowing);

} else {
	$output[] = array(

					//'photoUrl' => $one_photo->photoUrl,
		'postId' => $one_photo -> postId, 'originalPostId' => $one_photo -> originalPostId, 'lastUpdateTime' => strtotime($one_photo -> lastUpdateTime), 'userId' => $one_photo -> userId, 'studioPhotoId' => $one_photo -> studioPhotoId, 'postPhotoUrl' => $one_photo -> postPhotoUrl, 'userName' => $one_photo -> userName, 'userCountry' => $one_photo -> userCountry, 'userProfilePicUrl' => $one_photo -> userProfilePicUrl, 'creationTime' => strtotime($one_photo -> creationTime), 'liked' => $one_photo -> liked, 'commentCount' => $one_photo -> commentCount, 'likeCount' => $one_photo -> likeCount, 'reportCount' => $one_photo -> reportCount, 'photoOrientation' => $one_photo -> photoOrientation, 'privacy' => $one_photo -> privacy, 'contestRank' => $one_photo -> contestRank, 'tags' => $tags, 'reported' => $one_photo -> reported, 'hidden' => $one_photo -> hidden, 'postPhotoCaption' => $one_photo -> postPhotoCaption, 'rePosted' => $one_photo -> rePosted, 'following' => $one_photo -> following, 'favorite' => $one_photo -> favorite, 'rePostedBy' => $one_photo -> rePostedBy, 'rePosterProfilePic' => $one_photo -> rePosterProfilePic, 'rePosterId' => $one_photo -> rePosterId, 'rePosterIsFollowing' => $one_photo -> rePosterIsFollowing);

}

				//print json_encode(array('status'=>2,'data'=>$output));

}
			//print json_encode(array('status'=>2,'data'=>array('currentRequestTime'=>'','posts'=>$output)));
$this->db->close();
return $output;
} else
{
	$this->db->close();
	return false;
}
}

public function getLibVer() {
	$version = $this -> main_model -> getData('web_configuration', 'version');
	if ($version) {
		print json_encode(array('status' => 2, 'data' => $version[0] -> version));
	} else {
		print json_encode(array('status' => -2, ));
	}
	$this->db->close();
}

}
