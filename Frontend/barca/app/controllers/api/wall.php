<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Wall extends CI_Controller {

	function __construct() {
		parent::__construct();

	}

	public function reportPost() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$postId = intval($arr_data -> postId);
		if (empty($userId) || empty($postId))
			print_r(json_encode(array('status' => -1)));
		else {
			$insert_arr = array('userId' => $userId, 'postId' => $postId, );

			$insert = $this -> main_model -> insert('postreport', $insert_arr);
			$increment = $this -> main_model -> increment('post', 'reportCount', 'postId = ' . $postId);
			//$updatePostTime = $this->main_model->update('post',array('lastUpdateTime'=>date('Y-m-d H:i:s')),array('postId'=>$postId));
			if ($insert)
				print_r(json_encode(array('status' => 1)));
			else
				print_r(json_encode(array('status' => -2)));
		}
		$this -> db -> close();
	}

	public function addComments() {
		$json_data = $this -> input -> post('data');
		//$json_data = '{"commentText":"hi","postId":"251","userId":"817"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$postId = intval($arr_data -> postId);
		$commentText = $arr_data -> commentText;

		if (empty($userId) || empty($postId) || empty($commentText))
			print_r(json_encode(array('status' => -1)));
		else {
			$insert_arr = array('userId' => $userId, 'postId' => $postId, 'commentText' => $commentText);

			$insert = $this -> main_model -> insert('postcomment', $insert_arr);
			$increment = $this -> main_model -> increment('post', 'commentCount', 'postId = ' . $postId);
			$updatePostTime = $this -> main_model -> update('post', array('lastUpdateTime' => date('Y-m-d H:i:s')), array('postId' => $postId));
			$postowner = $this -> main_model -> getData('post', 'postId,userId', array('postId' => $postId));
			$postownerId = $postowner[0] -> userId;
			$this->main_model->SendNotification($postownerId, 'Some one commented on your post.', 10);
			$this -> main_model -> insert('notification', array('senderId' => $userId, 'receiverId' => $postownerId, 'postId' => $postId, 'notificationType' => 2, 'notificationtime' => date("Y-m-d H:i:s")));

			if ($insert)
			{
				
				print_r(json_encode(array('status' => 1, 'data' => array('commentId' => $insert))));
			}				
			else
				print_r(json_encode(array('status' => -2)));
		}
	}

	public function deleteComment() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$commentId = intval($arr_data -> commentId);
		$postId = intval($arr_data -> postId);

		if (empty($commentId))
			print_r(json_encode(array('status' => -1)));
		else {

			$this -> main_model -> delete('postcomment', array('commentId' => $commentId));
			$decrement = $this -> main_model -> decrement('post', 'commentCount', 'postId =' . $postId);
			//$updatePostTime = $this->main_model->update('post',array('lastUpdateTime'=>date('Y-m-d H:i:s')),array('postId'=>$postId));

			print_r(json_encode(array('status' => 1)));
		}
		$this -> db -> close();
	}

	public function getComments() {
		$json_data = $this -> input -> post('data');
		//	$json_data = '{"pageNo":"1","postId":"251","userId":"817"}';
		$arr_data = json_decode($json_data);
		$postId = intval($arr_data -> postId);
		$userId = intval($arr_data -> userId);
		$pageNo = intval($arr_data -> pageNo);

		if (empty($postId) || empty($pageNo))
			print_r(json_encode(array('status' => -1)));
		else {
			$start = ($pageNo - 1) * 20;
			$select = 'postcomment.commentId , postcomment.userId ,postcomment.commentText,postcomment.date as commentDate,user.fullName,user.profilePic as profilePicURL , (select isFollow from userfans where userfans.userId = ' . $userId . ' AND userfans.fanId = postcomment.userId) as isFollowing';
			$additionalParams['join'] = array('user', 'user.userId=postcomment.userId', 'left');
			$comments = $this -> main_model -> getBackendData('postcomment', $select, array('postId' => $postId), 'date', 'desc', 20, $start, $additionalParams);
			if ($comments) {
				foreach ($comments as $one_comment) {

					$one_comment -> commentId = intval($one_comment -> commentId);
					$one_comment -> userId = intval($one_comment -> userId);
					$one_comment -> isFollowing = intval($one_comment -> isFollowing);
					$one_comment -> commentDate = strtotime($one_comment -> commentDate);

					if (!$one_comment -> commentText)
						$one_comment -> commentText = '';
					if (!$one_comment -> commentDate)
						$one_comment -> commentDate = '';

					if (!$one_comment -> isFollowing)
						$one_comment -> isFollowing = 0;
					if (!$one_comment -> profilePicURL)
						$one_comment -> profilePicURL = '';
					else
					{
						if (strpos($one_comment -> profilePicURL, 'http') === FALSE)
							$one_comment -> profilePicURL = base_url() . 'third_party/uploads/profile/' . $one_comment -> profilePicURL;
					}
				}
				print json_encode(array('status' => 2, 'data' => $comments));
			} else
				print_r(json_encode(array('status' => 2, 'data' => array())));
		}
		$this -> db -> close();
	}

	public function likePost() {
		$json_data = $this -> input -> post('data');

		//$json_data ='{"postId":"313","userId":"817"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$postId = intval($arr_data -> postId);

		if (empty($userId) || empty($postId))
			print_r(json_encode(array('status' => -1)));
		else {
			$additionalParams['join'] = array('post', 'post.postId = postlike.postId');
			$checkExist = $this -> main_model -> getBackendData('postlike', 'postlike.*,post.originalPostId', array('postlike.userId' => $userId, 'postlike.postId' => $postId), '', '', 1, 0, $additionalParams);
			if ($checkExist)
				print_r(json_encode(array('status' => 1)));
			else {
				$postData = $this -> main_model -> getData('post', 'postId, originalPostId', array('postId' => $postId));
				$originalPostId = $postData[0] -> originalPostId;
				if ($originalPostId != 0) {
					$insert_arr = array('userId' => $userId, 'postId' => $originalPostId);
					$insert = $this -> main_model -> insert('postlike', $insert_arr);
				}
				else{
					$insert_arr = array('userId' => $userId, 'postId' => $postId);
					$insert = $this -> main_model -> insert('postlike', $insert_arr);
				}
				
				if ($insert) {
					$where_posts = "postId = " . $postId;
					if ($originalPostId != 0) {
						$where_org_post = "postId = " . $originalPostId;
						$this -> main_model -> increment('post', 'likeCount', $where_org_post);
						$this -> main_model -> increment('post', 'postPoints', $where_org_post);
					}
					$decrement = $this -> main_model -> increment('post', 'likeCount', $where_posts);
					$this -> main_model -> increment('post', 'postPoints', $where_posts);
					//$updatePostTime = $this->main_model->update('post',array('lastUpdateTime'=>date('Y-m-d H:i:s')),array('postId'=>$postId));
					$postowner = $this -> main_model -> getData('post', 'postId,userId', array('postId' => $postId));
					$postownerId = $postowner[0] -> userId;
					$this -> main_model -> insert('notification', array('senderId' => $userId, 'receiverId' => $postownerId, 'postId' => $postId, 'notificationType' => 1, 'notificationtime' => date("Y-m-d H:i:s")));
					$this->main_model->SendNotification($postownerId, 'Some one liked your post.', 10);
				
					print_r(json_encode(array('status' => 1)));
				} else
					print_r(json_encode(array('status' => -2)));
			}
		}
		$this -> db -> close();
	}

	public function dislikePost() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"postId":"288","userId":"817"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$postId = intval($arr_data -> postId);

		if (empty($userId) || empty($postId))
			print_r(json_encode(array('status' => -1)));
		else {
			$postData = $this -> main_model -> getData('post', 'postId, originalPostId', array('postId' => $postId));
			$originalPostId = $postData[0] -> originalPostId;
			if ($originalPostId != 0) {
				$where = array('userId' => $userId, 'postId' => $originalPostId);
				$delete = $this -> main_model -> delete('postlike', $where);
			}
			$where = array('userId' => $userId, 'postId' => $postId);
			if ($originalPostId != 0) {
				$decrement = $this -> main_model -> decrement('post', 'likeCount', 'postId = ' . $postId);
			}
			$decrement = $this -> main_model -> decrement('post', 'likeCount', 'postId = ' . $postId);
			//$updatePostTime = $this->main_model->update('post',array('lastUpdateTime'=>date('Y-m-d H:i:s')),array('postId'=>$postId));

			$delete = $this -> main_model -> delete('postlike', $where);

			print_r(json_encode(array('status' => 1)));
		}
	}

	public function deletePost() {
		$json_data = $this -> input -> post('data');

		$arr_data = json_decode($json_data);
		$postId = $arr_data -> postId;

		if (empty($postId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$post = $this -> main_model -> getData('post', 'userId,originalPostId', array('postId' => $postId));
			if(!$post)
			{
				print_r(json_encode(array('status' => -1)));
			}
			else
			{
				if($post[0]->originalPostId == '0')
					$this -> main_model -> decrement('user', 'noOfPics', 'userId=' . $post[0]->userId);
				
				$del = $this -> main_model -> delete('post', array('originalPostId' => $postId));
				$del1 = $this -> main_model -> delete('post', array('postId' => $postId));
	
				print json_encode(array('status' => 1));
			}
		}
		$this -> db -> close();
	}

	private function _enable_fan_relation($field_name, $data_arr = '') {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$fanId = intval($arr_data -> fanId);

		if (empty($userId) || empty($fanId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$arr = array('userId' => $userId, 'fanId' => $fanId);
			$exists = $this -> main_model -> getData('userfans', 'id', $arr);
			if ($exists) {
				if (!$data_arr)
					$data_arr = array($field_name => '1');
				$this -> main_model -> update('userfans', $data_arr, $arr);
			} else {
				if (!$data_arr) {
					$arr[$field_name] = '1';
					$data_arr = $arr;
				} else {
					$data_arr['userId'] = $userId;
					$data_arr['fanId'] = $fanId;
				}
				$this -> main_model -> insert('userfans', $data_arr);
			}
			if ($field_name == 'isFollow') {

			}
			print_r(json_encode(array('status' => 1)));
		}
	}

	private function _disable_fan_relation($field_name, $updated_arr = '') {

		$json_data = $this -> input -> post('data');

		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$fanId = intval($arr_data -> fanId);

		if (empty($userId) || empty($fanId)) {
			print_r(json_encode(array('status' => -1)));
		} else {
			$where = array('userId' => $userId, 'fanId' => $fanId);
			$exists = $this -> main_model -> getData('userfans', 'id', $where);
			if ($exists) {
				if ($updated_arr)
					$this -> main_model -> update('userfans', $updated_arr, $where);
				else
					$this -> main_model -> update('userfans', array($field_name => '0'), $where);
				print_r(json_encode(array('status' => 1)));
			} else
				print_r(json_encode(array('status' => -2)));
		}
	}

	public function followFan() {
		$this -> _enable_fan_relation('isFollow');
	}

	public function unfollowFan() {
		$this -> _disable_fan_relation('isFollow');
	}

	public function getLikeDetails() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"postId":"277","userId":"711"}';
		$arr_data = json_decode($json_data);
		$userId = $arr_data -> userId;
		$postId = $arr_data -> postId;

		if (empty($userId) || empty($postId))
			print_r(json_encode(array('status' => -1)));
		else {

			$select_fields = 'pl.likeId,u.userId,u.profilePic as profilePicURL,u.fullName,(select isFollow from userfans where userId = ' . $userId . ' AND fanId = u.userId) as isFollowing';
			$additional_params['join'] = array('user as u', 'u.userId=pl.userId', 'left');
			$likes_users = $this -> main_model -> getBackendData('postlike as pl', $select_fields, array('pl.postId' => $postId, 'u.userId ' => $userId), '', '', '', '', $additional_params);
			//echo "<pre>";
			//print_r($likes_users);
			//echo "</pre>";
			if ($likes_users) {
				foreach ($likes_users as $user) {

					$user -> likeId = intval($user -> likeId);
					$user -> userId = intval($user -> userId);
					$user -> isFollowing = intval($user -> isFollowing);

					if ($user -> isFollowing == '')
						$user -> isFollowing = 0;
					
					if(empty($user -> profilePicURL))
						$user -> profilePicURL = '';
					elseif (strpos($one_photo -> userProfilePicUrl, 'http') === FALSE)
						$user -> profilePicURL = base_url() . 'third_party/uploads/profile/' . $user -> profilePicURL;

					$data[] = $user;

				}
				print json_encode(array('status' => 2, 'data' => $data));
			} else {
				print json_encode(array('status' => 2, 'data' => array()));
			}

		}
		$this -> db -> close();
	}

	public function rePost() {
		$json_data = $this -> input -> post('data');
		$arr_data = json_decode($json_data);
		$userId = $arr_data -> userId;
		$postId = $arr_data -> postId;

		if (empty($userId) || empty($postId))
			print_r(json_encode(array('status' => -1)));
		else {
			$post = $this -> main_model -> getData('post', '*', array('postId' => $postId));
			if ($post) {
				
				$repostWhere = "postId = " . $postId;
				$this -> main_model -> incrementByValue('post', 'repostCount', $repostWhere, 1);
				
				//$repostedBefore = $this -> main_model -> getData('userrepost', 'repostId', array('userId' => $userId, 'postId' => $postId));

				//if (!$repostedBefore) {
					if ($post[0] -> originalPostId == 0)
					{
						$insert_post = $this -> main_model -> insert('post', array('originalPostId' => $postId, 'studioPhotoId' => $post[0] -> studioPhotoId, 'userId' => $userId, 'postPhotoUrl' => $post[0] -> postPhotoUrl, 'privacy' => $post[0] -> privacy));
						if($post[0] -> userId != $userId)
						{
							$where_original = "postId = " . $post[0] -> postId;
							//$this -> main_model -> incrementByValue('post', 'postPoints', $where_original, 2);
						}
					}
					
					else {
						$originalUserId = $this -> main_model -> getData('post', 'postId,userId', array('postId' => $post[0] -> originalPostId));
						$insert_post = $this -> main_model -> insert('post', array('originalPostId' => $post[0] -> originalPostId, 'studioPhotoId' => $post[0] -> studioPhotoId, 'userId' => $userId, 'postPhotoUrl' => $post[0] -> postPhotoUrl, 'privacy' => $post[0] -> privacy));
						if($post[0] -> userId != $userId)
						{
							$where = "postId = " . $post[0] -> originalPostId;
						//	$this -> main_model -> incrementByValue('post', 'postPoints', $where, 2);
						}
					}

					//$insert_post -> postId
					// enter in repost table
					$where_user = 'userId = ' . $userId;
					$this -> main_model -> insert('userrepost', array('userId' => $userId, 'postId' => $postId));
					
					$points = $this->main_model->getData('points','repostMyPostNormal,repostMyPostPremium');
					$newPoints = $userPoints + $points[0]->dialyPointsDay1;
					
					$user_arr = $this -> main_model -> getData('user', 'isPremium', $where_user);
					
					if($user_arr[0]->isPremium)
						$upd_points = (-1)*intval($points[0]->repostMyPostPremium);
					else
						$upd_points = (-1)*intval($points[0]->repostMyPostNormal);
					$this -> main_model ->incrementByValue ('user' , 'credit' , $where_user , $upd_points) ;
					
					$arr = array(
						'TransTypeID' => 'AP_10',
						'UserID' => $userId,
						'TransDate' => date('Y-m-d'),
						'TransTime' => date('H:i:s'),
						'points' => $upd_points
					);
					$this -> main_model -> insert('TransactionLog',$arr);
					
					if ($insert_post)
						print_r(json_encode(array('status' => 1)));
					else
						print_r(json_encode(array('status' => -1)));
				//} else
				//	print_r(json_encode(array('status' => -2)));
			} else {
				print_r(json_encode(array('status' => -1)));
			}

		}
		$this -> db -> close();
	}

	/*
	 public function editPost()
	 {
	 $json_data 	= $this->input->post('data');
	 $arr_data 	= json_decode($json_data);
	 $userId 	= $arr_data->userId;
	 $postId 	= $arr_data->postId;

	 if(empty($userId) || empty($postId) )
	 print_r(json_encode(array('status'=>-1)));
	 else
	 {
	 $post = $this->main_model->getData('post','*',array('postId'=>$postId));
	 if($post)
	 {
	 $insert_post = $this->main_model->isert('post',array('originalPostId'=>$postId,'studioPhotoId'=>$post[0]->studioPhotoId,'userId'=>$userId,'postPhotoUrl'=>$post[0]->postPhotoUrl,'privacy'=>$post[0]->privacy));
	 if($insert_post)
	 print_r(json_encode(array('status'=>1)));
	 else
	 print_r(json_encode(array('status'=>-1)));
	 }
	 else {
	 print_r(json_encode(array('status'=>-1)));
	 }

	 }
	 }
	 */
	public function editPost() {
		file_put_contents('test99.txt', serialize($_POST));
		$json_data = $this -> input -> post('data');
		//$json_data = ' {  "postId" : 0,  "postPhotoCaption" : "",  "uploadedDate" : "2014-10-09 05:35:36",  "imageUrl" : null,  "userId" : 62,  "publishedToWall" : false,  "studioPhotoId" : 2,  "privacy" : "4.000000",  "contestRank" : 0 , "tags" : [  {    "tagId" : "5",    "tagName" : "tag 1"  } , {    "tagId" : "0",    "tagName" : "tag 2"  }] } ';
		$arr_data = json_decode($json_data);

		$post = $arr_data -> post;

		$userId = $post -> userId;
		$studioPhotoId = $post -> studioPhotoId;
		$privacy = $post -> privacy;
		$contestRank = $post -> contestRank;
		$postPhotoCaption = $post -> postPhotoCaption;
		$tags = $post -> tags;
		$postId = $post -> postId;

		$image = $_FILES['photo'];

		if (empty($userId) || empty($studioPhotoId) || empty($privacy) || empty($postId))
			print_r(json_encode(array('status' => -1)));
		else {
			if (is_uploaded_file($image['tmp_name'])) {
				if (isset($image) && $image['error'] == 0) {
					require_once ('S3_config.php');
					if (!class_exists('S3'))
						require_once ('S3.php');
					$s3 = new S3($awsAccessKey, $awsSecretKey);

					$extension = end(explode('.', $image['name']));
					$updated_name = time() . '.' . $extension;
					if ($s3 -> putObjectFile($image['tmp_name'], $bucket, $uploads_dir_path . '/users/' . $userId . '/' . $updated_name, S3::ACL_PUBLIC_READ)) {
						$image_name = $updated_name;
					}
				} else {
					print_r(json_encode(array('status' => -2)));
					$this -> db -> close();
					exit ;

				}
			} else {
				$image_name = 0;
			}
			if (!empty($tags)) {
				foreach ($tags as $tag) {

					if ($tag -> tagType == 2) {
						$tags_data[] = $tag -> tagName;

					}

				}

				if (!empty($tags_data))
					$tags_string = implode(',', $tags_data);
				else
					$tags_string = "";

			} else
				$tags_string = '';
			if ($image_name)
				$postIdupd = $this -> main_model -> update('post', array('postPhotoCaption' => $postPhotoCaption, 'privacy' => intval($privacy), 'postPhotoUrl' => $updated_name, 'tags' => $tags_string), array('postId' => $postId));
			else
				$postIdupd = $this -> main_model -> update('post', array('postPhotoCaption' => $postPhotoCaption, 'privacy' => intval($privacy), 'tags' => $tags_string), array('postId' => $postId));

			if ($postIdupd) {

				$where = 'post.postId=' . $postId;
				$output = $this -> getPosts($where, 1, $userId);
				if ($output) {

					print json_encode(array('status' => 2, 'data' => $output[0]));
				}

			} else
				print_r(json_encode(array('status' => -2)));

		}
		$this -> db -> close();
	}

	public function getUserPosts() {
		//$this->output->enable_profiler(true);
		$json_data = $this -> input -> post('data');
		//$json_data ='{"fanId":"817","userId":"0","n":"10","timeFilter":"0","lastRequestTime":"1413089869"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$fanId = intval($arr_data -> fanId);
		$n = intval($arr_data -> n);
		$timeFilter = intval($arr_data -> timeFilter);
		$lastRequestTime = ($arr_data -> lastRequestTime);
		//echo $userId;
		if (empty($fanId) || empty($n))
			print_r(json_encode(array('status' => -1)));
		else {

			$where = "(post.userId = " . $fanId . " AND post.originalPostId = 0 AND ( post.privacy=1 or post.privacy=2 or (post.privacy='3' and (select isFriend from userfans where userfans.userId=" . $fanId . " AND userfans.fanId=" . $userId . " )=1)))";

			$counts = $this -> main_model -> count('post', $where);
			$update = $this -> main_model -> update('user', array("noOfPics" => $counts), array("userId" => $fanId));

			$output = $this -> getPosts($where, $n, $userId, $lastRequestTime, $timeFilter);
			if ($output)
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => $output)));
			else
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => array())));

		}
		$this -> db -> close();
	}

	public function getTagPosts() {

		$json_data = $this -> input -> post('data');
		//$json_data ='{"pageNo":"1","fanId":"711","userId":"817","n":"10","timeFilter":"0","lastRequestTime":"2014-11-05 18:39:09"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$tagName = strval($arr_data -> tagName);

		$n = intval($arr_data -> n);
		$timeFilter = intval($arr_data -> timeFilter);
		$lastRequestTime = ($arr_data -> lastRequestTime);

		if (empty($tagName) || empty($n))
			print_r(json_encode(array('status' => -1)));
		else {
			if (empty($userId))
				$userId = 0;

			$where = "t.name like '%" . $tagName . "%' AND t.type = 4";
			$additional['join'] = array('studiophotothesaurus st', 'st.thesuarusId=t.thesuarusId');
			$studiophotos = $this -> main_model -> getBackendData('thesuarus t', '*', $where, '', '', '', '', $additional);
			if ($studiophotos) {
				foreach ($studiophotos as $studiophoto)
					$ids[] = $studiophoto -> studioPhotoId;
			}
			if (!empty($ids)) {
				$ids_string = implode(',', $ids);
				$where = "( post.privacy = 1 || post.privacy = 2 ) AND (post.tags like '%" . $tagName . "%' OR (post.studioPhotoId in (" . $ids_string . "))) ";
			} else
				$where = "( post.privacy = 1 || post.privacy = 2 ) AND (post.tags like '%" . $tagName . "%' ) ";

			$output = $this -> getPosts($where, $n, $userId, $lastRequestTime, $timeFilter);
			if ($output)
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => $output)));
			else
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => array())));
		}
		$this -> db -> close();
	}

	public function getWall() {
		//file_put_contents('test125.txt', serialize($_POST));
		$json_data = $this -> input -> post('data');
		//$json_data ='{"userId":"817","n":"10","timeFilter":"0","lastRequestTime":"","favoritePostsFlag":"0","countryList":[]}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$n = intval($arr_data -> n);
		$timeFilter = intval($arr_data -> timeFilter);
		$lastRequestTime = ($arr_data -> lastRequestTime);
		$favoritePostsFlag = ($arr_data -> favoritePostsFlag);
		$countryList = ($arr_data -> countryList);

		if (empty($userId) || empty($n))
			print_r(json_encode(array('status' => -1)));
		else {

			$where = '((post.userId=' . $userId . ') OR (post.userId = 1221) or (userfans.isFollow=1 and post.privacy=2) or (userfans.isFriend=1 AND post.privacy!=4))';
				
			//$userfans_join_where = ' and ((userfans.isFollow=1 and post.privacy=2) or (userfans.isFriend=1 AND post.privacy!=4))';
			$output = $this -> getPosts($where, $n, $userId, $lastRequestTime, $timeFilter, $favoritePostsFlag, $countryList); //,'post.lastUpdateTime',$userfans_join_where
			if ($output)
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => $output)));
			else
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => array())));
		}
	}

	public function getLatest() {
		//file_put_contents('test120.txt', date('Y-m-d H:i:s').'
		//'.serialize($_POST),FILE_APPEND);
		$json_data = $this -> input -> post('data');
		//$json_data ='{"n":"10","timeFilter":"0","lastRequestTime":"0","userId":"1221"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);

		if (!$userId)
			$userId = -1;
		$n = intval($arr_data -> n);
		$timeFilter = intval($arr_data -> timeFilter);
		$lastRequestTime = ($arr_data -> lastRequestTime);
		$countryList = ($arr_data -> countryList);
		if (empty($userId) || empty($n))
			print_r(json_encode(array('status' => -1)));
		else {
			if ($userId == -1)
				$where = "( post.privacy=2 )";
			else
				$where = " (post.privacy=2  or (post.privacy='3' and post.userId in (select userId from userfans where fanId=" . $userId . " and isFriend='1'))) ";
			//$where=" (post.privacy=1 or post.privacy=2 or (post.privacy='3' and post.userId in (select userId from userfans where fanId=".$userId." and isFriend='1'))) ";

			$output = $this -> getPosts($where, $n, $userId, $lastRequestTime, $timeFilter, '', $countryList, 'post.creationTime');
			if ($output)
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => $output)));
			else
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => array())));
		}

	}

	public function getCountryList() {

		$countries = $this -> main_model -> getData('country', 'countryId , countryName');

		if ($countries)
			print_r(json_encode(array('status' => 2, 'data' => $countries)));
		else
			print_r(json_encode(array('status' => 2, 'data' => array())));
		$this -> db -> close();
	}

	public function getMyPosts() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"userId":"817","n":"10","timeFilter":"0","lastRequestTime":"1413089869"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$n = intval($arr_data -> n);
		$timeFilter = intval($arr_data -> timeFilter);
		$lastRequestTime = ($arr_data -> lastRequestTime);

		//$userId=5;
		//$pageNo=1;

		if (empty($userId) || empty($n))
			print_r(json_encode(array('status' => -1)));
		else {
			$where = '(post.userId=' . $userId . ') AND hidden=0';
			$output = $this -> getPosts($where, $n, $userId, $lastRequestTime, $timeFilter, '', '');
			if ($output) {

				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => $output)));
			} else
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => array())));
		}
	}

	public function getPost() {
		$json_data = $this -> input -> post('data');
		//$json_data ='{"userId":"817","n":"10","timeFilter":"0","lastRequestTime":"1413089869"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$n = 1;
		$postId = intval($arr_data -> postId);

		//$userId=5;
		//$pageNo=1;

		if (empty($userId) || empty($postId))
			print_r(json_encode(array('status' => -1)));
		else {
			$where = '(post.postId=' . $postId . ')';
			$output = $this -> getPosts($where, $n, $userId, '', '', '', '');
			if ($output) {

				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => $output)));
			} else
				print json_encode(array('status' => 2, 'data' => array('currentRequestTime' => strtotime(date('Y-m-d H:i:s')), 'posts' => array())));
		}
	}

	public function getTopTen() {
		//print_r(json_encode(array('status' => 2, 'data' => array()))); return;

		//$json_data = $this -> input -> post('data');
		$json_data ='{"pageNo":"1","userId":"1221"}';
		$arr_data = json_decode($json_data);

		$userId = intval($arr_data -> userId);
		$refresh = intval($arr_data -> refresh);

		if (empty($userId))
			$userId = 0;

		if (!empty($refresh))
			$seasons = $this -> main_model -> getData('contest', '*', array('status' => 1, 'startDateTime <=' => date("Y-m-d H:i:d"), 'endDateTime >=' => date("Y-m-d H:i:d")), '', '', 1, 0);
		else
			$seasons = $this -> main_model -> getData('contest', '*', '', 'endDateTime', 'desc');
		//print_r($seasons);
		if ($seasons) {
			foreach ($seasons as $season) {
				$start = 0;

				$where = "( post.privacy =2 AND post.hidden=0 AND post.originalPostId=0) AND ( post.creationTime >= '" . $season -> startDateTime . "' and post.creationTime <= '" . $season -> endDateTime . "') ";

				$select = 'distinct post.postId as tmpId,post.*,user.fullName as userName,studiophoto.useCount,studiophoto.photoOrientation,user.profilePic as userProfilePicUrl,country.countryName as userCountry ,hidden,postPhotoCaption, ';  //,studiophotothesaurus.thesuarusId
				$select .= '(select count(`likeId`) from  postlike where `postId` = `post`.postId AND `userId` = ' . $userId . ') as liked ,';
				$select .= '(select isFollow from  userfans where userfans.fanId = user.userId AND `userId` = ' . $userId . ') as following,';
				$select .= '(select isFavourite from  userfans where userfans.fanId = user.userId AND `userId` = ' . $userId . ') as favorite';

				$additional_params['join'] = array('user', 'user.userId = post.userId', 'left');
				$additional_params['join2'] = array('studiophoto', 'post.studioPhotoId = studiophoto.studioPhotoId', 'left');
				//$additional_params['join3'] = array('studiophotothesaurus', 'studiophotothesaurus.studioPhotoId = post.studioPhotoId', 'left');
				//$additional_params['join4'] = array('thesuarus', 'thesuarus.thesuarusId = studiophotothesaurus.thesuarusId', 'left');
				$additional_params['join6'] = array('country', 'user.countryId = country.countryId', 'left');
				$additional_params['order_by'] = array('post.contestRank DESC , post.postPoints DESC');
				$this -> db -> group_by('post.postId');

				$photos = $this -> main_model -> getBackendData('post', $select, $where, 'post.contestRank DESC , post.postPoints DESC', '', 10, $start, $additional_params);
				if ($photos) {
					$output = array();
					foreach ($photos as $one_photo) {
						// tags section for studio photo ids and photo ids

						$tags = array();
						// end of post tags

						if ($one_photo -> postPhotoUrl)
							$one_photo -> postPhotoUrl = base_url() . 'third_party/uploads/users/' . $one_photo -> userId . '/' . $one_photo -> postPhotoUrl;
						else
							$one_photo -> postPhotoUrl = '';

						$one_photo -> postId = intval($one_photo -> postId);
						$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
						$one_photo -> userId = intval($one_photo -> userId);
						$one_photo -> privacy = intval($one_photo -> privacy);

						$one_photo -> contestRank = intval($one_photo -> contestRank);
						$one_photo -> originalPostId = intval($one_photo -> originalPostId);

						if (!$one_photo -> creationTime)
							$one_photo -> creationTime = '';

						if (strpos($one_photo -> userProfilePicUrl, 'http') !== FALSE)
							$one_photo -> userProfilePicUrl = $one_photo -> userProfilePicUrl;
						else {

							$one_photo -> userProfilePicUrl = base_url() . 'third_party/uploads/profile/' . $one_photo -> userProfilePicUrl;

						}
						if (empty($one_photo -> userProfilePicUrl))
							$one_photo -> userProfilePicUrl = '';

						if (!$one_photo -> userName)
							$one_photo -> userName = '';
						if (!$one_photo -> userCountry)
							$one_photo -> userCountry = '';
						$one_photo -> commentCount = intval($one_photo -> commentCount);
						$one_photo -> likeCount = intval($one_photo -> likeCount);

						$one_photo -> useCount = intval($one_photo -> useCount);
						$one_photo -> reportCount = intval($one_photo -> reportCount);
						$one_photo -> shareCount = intval($one_photo -> shareCount);
						$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
						$one_photo -> contestRank = intval($one_photo -> contestRank);
						$one_photo -> liked = intval($one_photo -> liked);
						$one_photo -> hidden = intval($one_photo -> hidden);
						$one_photo -> following = intval($one_photo -> following);
						$one_photo -> favorite = intval($one_photo -> favorite);
						if ($one_photo -> reported > 0)
							$one_photo -> reported = 1;
						else
							$one_photo -> reported = 0;

						// original post by
						if ($one_photo -> originalPostId > 0) {
							$userOid = $this -> main_model -> getData('post', 'postId,userId', array('postId' => $one_photo -> originalPostId));
							$userOName = $this -> main_model -> getData('user', 'userId,fullName,profilePic,socialMediaId', array('userId' => $userOid[0] -> userId));
							if ($userOid[0] -> userId == $userId) {
								$one_photo -> rePosted = 1;
								$one_photo -> rePostedBy = $one_photo -> fullName;
								$one_photo -> rePosterProfilePic = $one_photo -> userProfilePicUrl;
								$one_photo -> rePosterId = $one_photo -> userId;
								$one_photo -> rePosterIsFollowing = 0;
							} else {
								$one_photo -> rePosted = 0;
								$one_photo -> rePostedBy = $userOName[0] -> fullName;
								if ($userOName[0] -> socialMediaId > 0) {
									if (!empty($userOName[0] -> profilePic))
										$one_photo -> rePosterProfilePic = $userOName[0] -> profilePic;
									else
										$one_photo -> rePosterProfilePic = "";
								} else {
									if (!empty($userOName[0] -> profilePic))
										$one_photo -> rePosterProfilePic = base_url() . 'third_party/uploads/profile/' . $userOName[0] -> profilePic;
									else
										$one_photo -> rePosterProfilePic = "";
								}

								$one_photo -> rePosterId = $userOName[0] -> userId;
								$one_photo -> rePosterIsFollowing = 0;
							}

						} else {
							$one_photo -> rePosted = 0;
							$one_photo -> rePostedBy = "";
							$one_photo -> rePosterProfilePic = "";
							$one_photo -> rePosterId = 0;
							$one_photo -> rePosterIsFollowing = 0;
						}

						$output[] = array(

						//'photoUrl' => $one_photo->photoUrl,
						'postId' => $one_photo -> postId, 'originalPostId' => $one_photo -> originalPostId, 'lastUpdateTime' => strtotime($one_photo -> lastUpdateTime), 'userId' => $one_photo -> userId, 'studioPhotoId' => $one_photo -> studioPhotoId, 'postPhotoUrl' => $one_photo -> postPhotoUrl, 'userName' => $one_photo -> userName, 'userCountry' => $one_photo -> userCountry, 'userProfilePicUrl' => $one_photo -> userProfilePicUrl, 'creationTime' => strtotime($one_photo -> creationTime), 'liked' => $one_photo -> liked, 'commentCount' => $one_photo -> commentCount, 'likeCount' => $one_photo -> likeCount, 'reportCount' => $one_photo -> reportCount, 'photoOrientation' => $one_photo -> photoOrientation, 'privacy' => $one_photo -> privacy, 'contestRank' => $one_photo -> contestRank, 'tags' => $tags, 'reported' => $one_photo -> reported, 'hidden' => $one_photo -> hidden, 'postPhotoCaption' => $one_photo -> postPhotoCaption, 'rePosted' => $one_photo -> rePosted, 'following' => $one_photo -> following, 'favorite' => $one_photo -> favorite, 'rePostedBy' => $one_photo -> rePostedBy, 'rePosterProfilePic' => $one_photo -> rePosterProfilePic, 'rePosterId' => $one_photo -> rePosterId, 'rePosterIsFollowing' => $one_photo -> rePosterIsFollowing, );

						//print json_encode(array('status'=>2,'data'=>$output));
					}
					//print json_encode(array('status'=>2,'data'=>$output));

				} else
					$output = array();

				$arr1[] = array('seasonId' => intval($season -> contestId), 'seasonName' => $season -> name, 'posts' => $output);
			}

			print_r(json_encode(array('status' => 2, 'data' => $arr1)));
		} else
			print_r(json_encode(array('status' => 2, 'data' => array())));
		$this -> db -> close();
				file_put_contents('testindex3.txt', json_encode(array('status' => 2, 'data' => $arr1)));
	}

	public function getPosts($where = '', $n = 10, $userId = 817, $lastRequestTime = '', $timeFilter = '', $favorite = 0, $countryList = array(), $orderBy = 'post.lastUpdateTime',$userfans_join_where='') {
		//$lastRequestTime = "2014-11-7 11:44:38";
		//	$timeFilter = 0;
		//	print_r($countryList);
		if($userfans_join_where!='')
			$where = "( post.hidden=0 ) " . $where;
		else
			$where = "( post.hidden=0 ) AND " . $where;
		if ($lastRequestTime != 0) {

			if ($timeFilter == 0)// after
				$where .= ' AND ( post.lastUpdateTime < " ' . date("Y-m-d H:i:s", ($lastRequestTime)) . '" )';
			else// before
				$where .= ' AND ( post.lastUpdateTime > " ' . date("Y-m-d H:i:s", ($lastRequestTime)) . '" )';
		}
		if ($favorite == 1)
			$where .= " AND ( post.userId in (select fanId from userfans where userId = " . $userId . " AND isFavourite =1) )";

		$select = 'post.postId,post.hidden,post.originalPostId,post.postPhotoCaption,post.studioPhotoId,post.userId,post.creationTime,post.lastUpdateTime,post.postPhotoUrl,post.privacy,post.contestRank,post.likeCount,post.commentCount,post.repostCount,post.reportCount,post.shareCount,post.postPoints,post.tags,user.fullName as userName,studiophoto.photoOrientation,user.profilePic as userProfilePicUrl , ';
		$select .= 'postlike.`likeId` as liked ,';
		$select .= 'userrepost.`repostID` as rePosted ,';
		$select .= 'isFollow as following,';

		//$select.= '(select isFavourite from  userfans where userfans.fanId = user.userId AND `userId` = '.$userId.' limit 1) as favorite, ';
		//$select.= '(select reportId from  postreport where postreport.postId = post.postId AND `userId` = '.$userId.' limit 1) as reported';

		//$userfans_join_where
		$additional_params = array();
		
		$additional_params['join'] = array('user', 'user.userId = post.userId', 'left');
		$additional_params['join1'] = array('postlike', "postlike.postId = post.postId AND postlike.userId = $userId", 'left');
		$additional_params['join4'] = array('userrepost', "userrepost.postId = post.postId AND userrepost.userId = $userId", 'left');
		$additional_params['join2'] = array('userfans', "userfans.fanId = post.userId AND userfans.userId = $userId$userfans_join_where", 'left');
		$additional_params['join3'] = array('studiophoto', 'post.studioPhotoId = studiophoto.studioPhotoId', 'left');
		//$additional_params['join3'] = array('studiophotothesaurus','studiophotothesaurus.studioPhotoId = post.studioPhotoId','left');
		//$additional_params['join4'] = array('thesuarus','thesuarus.thesuarusId = studiophotothesaurus.thesuarusId','left');
		$additional_params['join6'] = array('country', 'user.countryId = country.countryId', 'left');
		if (!empty($countryList))
			$additional_params['where_in'] = array('country.countryId', $countryList);

		//$this->db->group_by('post.postId');

		$photos = $this -> main_model -> getBackendData('post', $select, $where, $orderBy, 'desc', $n, 0, $additional_params);

		if ($photos) {
			$output = array();
			foreach ($photos as $one_photo) {

				// tags section for studio photo ids and photo ids

				$tags = array();

				// end of post tags

				if (intval($one_photo -> originalPostId) == 0) {
					if ($one_photo -> postPhotoUrl)
						$one_photo -> postPhotoUrl = base_url() . 'third_party/uploads/users/' . $one_photo -> userId . '/' . $one_photo -> postPhotoUrl;
					else
						$one_photo -> postPhotoUrl = '';
				}

				$one_photo -> postId = intval($one_photo -> postId);
				$one_photo -> studioPhotoId = intval($one_photo -> studioPhotoId);
				$one_photo -> userId = intval($one_photo -> userId);
				$one_photo -> privacy = intval($one_photo -> privacy);

				$one_photo -> contestRank = intval($one_photo -> contestRank);
				$one_photo -> originalPostId = intval($one_photo -> originalPostId);

				if (!$one_photo -> creationTime)
					$one_photo -> creationTime = '';

				if (strpos($one_photo -> userProfilePicUrl, 'http') !== FALSE)
					$one_photo -> userProfilePicUrl = $one_photo -> userProfilePicUrl;
				else {

					$one_photo -> userProfilePicUrl = base_url() . 'third_party/uploads/profile/' . $one_photo -> userProfilePicUrl;

				}
				if (empty($one_photo -> userProfilePicUrl))
					$one_photo -> userProfilePicUrl = '';

				if (!$one_photo -> userName)
					$one_photo -> userName = '';
				if (!$one_photo -> userCountry)
					$one_photo -> userCountry = '';
				$one_photo -> commentCount = intval($one_photo -> commentCount);
				$one_photo -> likeCount = intval($one_photo -> likeCount);

				$one_photo -> useCount = intval($one_photo -> useCount);
				$one_photo -> reportCount = intval($one_photo -> reportCount);
				$one_photo -> repostCount = intval($one_photo -> repostCount);
				$one_photo -> shareCount = intval($one_photo -> shareCount);
				$one_photo -> photoOrientation = intval($one_photo -> photoOrientation);
				$one_photo -> contestRank = intval($one_photo -> contestRank);
				$one_photo -> liked = intval($one_photo -> liked);
				$one_photo -> rePosted = intval($one_photo -> rePosted);
				$one_photo -> hidden = intval($one_photo -> hidden);
				$one_photo -> following = intval($one_photo -> following);
				//$one_photo->favorite = intval($one_photo->favorite);
				$one_photo -> favorite = 0;
				/*
				 if($one_photo->reported > 0 )
				 $one_photo->reported =1;
				 else
				 $one_photo->reported =0;
				 */
				$one_photo -> reported = 0;
				
				if ($one_photo -> rePosted > 0)
							$one_photo -> rePosted = 1;
						else
							$one_photo -> rePosted = 0;
						
				// original post by
				if ($one_photo -> originalPostId > 0) {
					//$userOid = $this->main_model->getData('post','postId,userId',array('postId'=>$one_photo->originalPostId));

					//$userOName = $this->main_model->getData('user','userId,fullName,profilePic,socialMediaId',array('userId'=>$userOid[0]->userId));

					$additional_params0['join'] = array('user', 'user.userId=post.userId');
					//$userOName = $this -> main_model -> getBackendData('post', 'post.postId,post.userId,user.fullName , user.profilePic,(select repostId from userrepost where userId=post.userId and postId= post.postId limit 1 ) as rePosted ,(select isFollow from userfans where userId =' . $one_photo -> userId . ' AND fanId =post.userId) as  rePosterIsFollowing', array('post.postId' => $one_photo -> originalPostId), '', '', 1, 0, $additional_params0);
					$userOName = $this -> main_model -> getBackendData('post', 'post.postId,user.userId,user.fullName , user.profilePic', array('post.postId' => $one_photo -> originalPostId), '', '', 1, 0, $additional_params0);
					if ($userOName) {
						if ($one_photo -> postPhotoUrl) {

							$one_photo -> postPhotoUrl = base_url() . 'third_party/uploads/users/' . $userOName[0] -> userId . '/' . $one_photo -> postPhotoUrl;

						}

						if ($one_photo -> rePosted > 0)
							$one_photo -> rePosted = 1;
						else
							$one_photo -> rePosted = 0;

						$one_photo -> rePostedBy = $one_photo -> userName;

						$one_photo -> userName = $userOName[0] -> fullName;

						$one_photo -> rePosterProfilePic = $this -> userPic($one_photo -> userProfilePicUrl);
						$one_photo -> userProfilePicUrl = $this -> userPic($userOName[0] -> profilePic);
						$one_photo -> rePosterId = $one_photo -> userId;
						$one_photo -> userId = intval($userOName[0] -> userId);

						$one_photo -> rePosterIsFollowing = intval($one_photo -> rePosterIsFollowing);

					} else {
						if ($one_photo -> rePosted > 0)
							$one_photo -> rePosted = 1;
						else
							$one_photo -> rePosted = 0;
						$one_photo -> rePostedBy = "";
						$one_photo -> rePosterProfilePic = "";
						$one_photo -> rePosterId = 0;
						$one_photo -> rePosterIsFollowing = 0;
					}

				} else {
					if ($one_photo -> rePosted > 0)
							$one_photo -> rePosted = 1;
						else
							$one_photo -> rePosted = 0;
					$one_photo -> rePostedBy = "";
					$one_photo -> rePosterProfilePic = "";
					$one_photo -> rePosterId = 0;
					$one_photo -> rePosterIsFollowing = 0;
				}

				$output[] = array(

				//'photoUrl' => $one_photo->photoUrl,
				'postId' => $one_photo -> postId, 'originalPostId' => $one_photo -> originalPostId, 'lastUpdateTime' => strtotime($one_photo -> lastUpdateTime), 'userId' => $one_photo -> userId, 'studioPhotoId' => $one_photo -> studioPhotoId, 'postPhotoUrl' => $one_photo -> postPhotoUrl, 'userName' => $one_photo -> userName, 'userCountry' => $one_photo -> userCountry, 'userProfilePicUrl' => $one_photo -> userProfilePicUrl, 'creationTime' => strtotime($one_photo -> creationTime), 'liked' => $one_photo -> liked, 'commentCount' => $one_photo -> commentCount, 'likeCount' => $one_photo -> likeCount, 'reportCount' => $one_photo -> reportCount, 'repostCount' => $one_photo-> repostCount,'photoOrientation' => $one_photo -> photoOrientation, 'privacy' => $one_photo -> privacy, 'contestRank' => $one_photo -> contestRank, 'tags' => $tags, 'reported' => $one_photo -> reported, 'hidden' => $one_photo -> hidden, 'postPhotoCaption' => $one_photo -> postPhotoCaption, 'rePosted' => $one_photo -> rePosted, 'following' => $one_photo -> following, 'favorite' => $one_photo -> favorite, 'rePostedBy' => $one_photo -> rePostedBy, 'rePosterProfilePic' => $one_photo -> rePosterProfilePic, 'rePosterId' => $one_photo -> rePosterId, 'rePosterIsFollowing' => $one_photo -> rePosterIsFollowing, );

				//print json_encode(array('status'=>2,'data'=>$output));

			}
			//print json_encode(array('status'=>2,'data'=>array('currentRequestTime'=>'','posts'=>$output)));
			$this -> db -> close();
			return $output;
		} else {
			$this -> db -> close();
			return false;
		}
	}

	private function userPic($url) {
		if (strpos($url, 'http') !== FALSE)
			return $url;
		else {
			if (empty($url))
				return '';
			else
				return base_url() . 'third_party/uploads/profile/' . $url;
		}

	}

}
