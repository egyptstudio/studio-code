<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		/* Standard Libraries */
		$this->load->database();
		/* ------------------ */
		
		$this->load->helper('url'); //Just for the examples, this is not required thought for the library
		
		$this->load->library('image_CRUD');
	}
	
	function _example_output($output = null)
	{
		$this->load->view('example.php',$output);	
	}
	
	function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}	
	
	
	function photos()
	{
		$image_crud = new image_CRUD();
	
		$image_crud->set_primary_key_field('imageId');
		$image_crud->set_url_field('imageUrl');
		$image_crud->set_title_field('imageName');
		$image_crud->set_table('studiophoto')
		->set_relation_field('categoryId')
		->set_ordering_field('priority')
		->set_image_path('third_party/uploads');
			
		$output = $image_crud->render();
	
		$this->_example_output($output);
	}

	
}