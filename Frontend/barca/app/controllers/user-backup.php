<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		
	
	}
	
	
	
	function changePassword($id)
	{
		
	
		$data['id']=$id;
		$this->load->view('chang_password_view',$data);
	
	}	
	
	function change_password()
	{
	
		$pass = $this->input->post('pass');
		$code = $this->input->post('code');
		
		//echo $pass.'==vvv=='.$code;
		$get_user = $this->main_model->getData('user_codes','',array('code'=>$code));
		if($get_user)
		{
			$update = $this->main_model->update('user',array('password'=>md5($pass)),array('userId'=>$get_user[0]->userId));
			if($update)
            {

                echo "success";
            }

			else
				echo "FAILED";
		}
		else
			echo "no user found";
		
	}

    public  function  verifyemail($id)
    {
      $user =   $this->main_model->getData('verificationcode','*',array('code'=>$id));
        if($user)
        {

           $upd =  $this->main_model->update('user',array('emailvreified'=>'1'),array('userId'=>$user[0]->userId));
            if($upd)
            {
               $this->main_model->delete('verificationcode',array('code'=>$id));
                $this->send_notification($user[0]->userId,"email verified Succssfully",2);
                echo "Email Verified Successfully.";
            }
            else
                echo "email verified failure";


        }
        else
        echo "NOT Authorized";
    }

    private function send_notification($userID,$msg,$type)
    {
        $where = 'userId = '.$userID. ' AND  (devicetype = 1 OR devicetype = 2)' ;
        $user = $this->main_model->getData('user','userId, pushnotificationtoken , devicetype',$where);
        if($user)
        {
            $device_type = $user[0]->devicetype;
            $device_token = $user[0]->pushnotificationtoken;
            if(!empty($device_token))
            $this->load->library('notification_lib');
            if($device_type == 2)
            {
                $this->notification_lib->push_iphone($device_token,$msg,$type);
            }
            else if ($device_type == 1 )
            {
                $this->notification_lib->push_android($device_token,$msg,$type);
            }

        }
    }

	
}