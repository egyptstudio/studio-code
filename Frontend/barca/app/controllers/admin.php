<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->is_logged_in();
        $this->load->library('grocery_CRUD');
		$this->load->model('main_model');
		
		
	}

	
	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}
	
	public function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			
			redirect('login');		
			
		}		
	}
	
	
	 function encrypt_password_callback($post_array, $primary_key) {
   
    
       // $key = 'super-secret-key';
        $post_array['password'] = md5($post_array['password']);
   
 
  		return $post_array;
	}
	 
	
	
	public function  thesaurus()
	{
		$thesures = $this->main_model->getData('thesaurus','*');
		foreach($thesures as $thes)
		$Arr[$thes->thesId] = $thes->thesName;
		try{
		    $crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('thesaurus');
			$crud->set_subject('thesaurus');
			$crud->fields('thesName','thesParentId','thesPicUrl','rank','thesType');
			//$crud->field_type('password', 'password');
			//$crud->unset_add();
			//$crud->unset_delete();
			//$crud->required_fields('password','username');
			//$crud->callback_before_update(array($this,'encrypt_password_callback'));
		//	$crud->set_rules('password','password','max_length[20]|min_length[8]');
			//$crud->set_rules('username','username','trim|min_length[4]|xss_clean|alpha_numeric');
			
			
			$crud->set_rules('thesType','thes Type','required');
			$crud->required_fields('thesName');
			$crud->set_field_upload('thesPicUrl','third_party/uploads/');
			$crud->field_type('thesType','dropdown',array('1'=>'players','2'=>'events','3'=>'seasons','4'=>'tags'));
			$crud->field_type('thesParentId','dropdown',$Arr);
			
			$crud->callback_before_insert(array($this,'test_check'));
			$output = $crud->render();
			
			$output->title = 'thesaurus'; 
			$this->load->view('example.php',$output);

			//$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	
	public function  studio_photo()
	{
		$thesures = $this->main_model->getData('thesaurus','*');
		foreach($thesures as $thes)
		$Arr[$thes->thesId] = $thes->thesName;
		try{
		    $crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('studio_photo');
			$crud->set_subject('studio_photo');
			$crud->fields('imageName','photoUrl','photoOrientation','captureType','thesures');
			//$crud->field_type('password', 'password');
			//$crud->unset_add();
			//$crud->unset_delete();
			//$crud->required_fields('password','username');
			//$crud->callback_before_update(array($this,'encrypt_password_callback'));
		//	$crud->set_rules('password','password','max_length[20]|min_length[8]');
			//$crud->set_rules('username','username','trim|min_length[4]|xss_clean|alpha_numeric');
			
			$crud->set_relation_n_n('thesures', 'studio_photo_thesaurus', 'thesaurus', 'studioPhotoId', 'thesId', 'thesName');
			$crud->set_field_upload('photoUrl','third_party/uploads/');
			$crud->field_type('thesType','dropdown',array('1'=>'player','2'=>'events','3'=>'seasons','4'=>'tags'));
			$crud->field_type('photoOrientation','dropdown',array('1'=>'portarait','2'=>'landscape'));
			$crud->field_type('captureType','dropdown',array('1'=>'Maskshot','2'=>'screenshot'));
			$crud->field_type('thesParentId','dropdown',$Arr);
		
			//$crud->field_type('usedCount','hidden','0');
			//$crud->field_type('usingCounter','hidden','0');
			$output = $crud->render();
			
			$output->title = 'studio_photo'; 
			$this->load->view('example.php',$output);

			//$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	
	public function user()
	{

		try{
		    $crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('users');
			$crud->set_subject('users');
			$crud->columns('name','email','mobile','country','company_name');
			//$crud->fields('name','icon','user_id');
			
			//$crud->field_type('password', 'password');
			
		//	$crud->set_field_upload('icon','third_party/uploads/');
			
		//	$crud->required_fields('name','icon');
		//	$crud->field_type('user_id','hidden',0);
			//$crud->set_rules('name','name','trim|min_length[4]|xss_clean|alpha_numeric');
			$crud->unset_add();
			$crud->unset_edit();
			$output = $crud->render();
			
			$output->title = 'users'; 
			$this->load->view('example.php',$output);

			//$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
 

		function test_check($post_array)
		{
			
			
			  if($post_array['rank'] < 1  && $post_array['thesType'] == 1)
			  {
			 
				//$crud->get_form_validation()->set_message('test_check',"please select rank for player thesures to ba player number");
				$this->form_validation->set_message('test_check', 'please select rank for player thesures to ba player number');

				return false;
				
				
				
			}
			else
				return $post_array;
				
		}
	
}