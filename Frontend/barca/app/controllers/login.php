<?php

class Login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
        $this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->load->model('main_model');
	}
	
	public function index()
	{
		$data['error'] = '';
		$this->load->view('login_form',$data);
		
		
			
	}
	function aaa(){
	echo sha1('barcaadmin');
	}
	public function validate_credentials()
	{
		
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if($this->form_validation->run() == FALSE)
		{
		$data['error'] = 'please fill fields';
        $this->load->view('login_form',$data);
		}
					
		else{
		$query = $this->main_model->getData('admin','*',array('username'=>$this->input->post('username'),'password'=>sha1($this->input->post('password'))));
		
		if($query) // if the user's credentials validated...
		{
			$data = array(
				'username' => $this->input->post('username'),
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('admin/studio_photo');
		}
		else // incorrect username or password
		{
			$data['error'] = 'username or password incorrect';
			$this->load->view('login_form',$data);
		}
		}
	}	
	
	public function logout()
	{
		$this->session->sess_destroy();
		$this->index();
	}
	
	/*
	function create_member()
	{
		$this->load->library('form_validation');
		
		// field name, error message, validation rules
		$this->form_validation->set_rules('first_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');
		
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('signup_form');
		}
		
		else
		{			
			$this->load->model('membership_model');
			
			if($query = $this->membership_model->create_member())
			{
				$data['main_content'] = 'signup_successful';
				$this->load->view('includes/template', $data);
			}
			else
			{
				$this->load->view('signup_form');			
			}
		}
		
	}
	*/
	

}