<?php
Class Cron_ extends CI_Controller {
	function __construct() {
		parent::__construct();
	}

	function index() {
		$where = array('isPremium' => '1', 'premiumEnd <' => date('Y-m-d'));
		$data = array('isPremium' => '0', 'premiumEnd' => '0000-00-00');

		$this -> db -> where($where);
		$this -> db -> update('user', $data);
	}

	public function send_notif_android() {
		$this -> db -> select('id,android_status,message,where_condition,android_max_user_id');
		$not_where = "(sendVia='notification' or sendVia='all') and android_status!='Completed'";
		$this -> db -> where($not_where);
		$this -> db -> order_by('id', 'asc');
		$this -> db -> limit(1);
		$q = $this -> db -> get('send_notifications');
		$notification = $q -> row();

		if (!empty($notification -> id)) {
			$updated_arr = array();
			if ($notification -> android_status == 'Pending')
				$updated_arr['android_status'] = 'In progress';
			if (!$notification -> android_max_user_id)
				$notification -> android_max_user_id = 0;
			$this -> db -> select('userId,pushnotificationtoken');
			$where = $notification -> where_condition . " and devicetype='1' and userID >" . $notification -> android_max_user_id;
			$this -> db -> where($where, NULL, FALSE);
			$this -> db -> order_by('userId', 'asc');
			$this -> db -> limit(1000);
			$q2 = $this -> db -> get('user');
			$users = $q2 -> result_array();

			if (isset($users[count($users) - 1]['userId']))
				$updated_arr['android_max_user_id'] = $users[count($users) - 1]['userId'];
			else
				$updated_arr['android_status'] = 'Completed';

			$this -> db -> where(array('id' => $notification -> id));
			$this -> db -> update('send_notifications', $updated_arr);

			$reg_ids = array_column($users, 'pushnotificationtoken');
			if ($reg_ids)
			{
				$response = $this -> push_android($reg_ids, $notification -> message, $notification -> id);
				$response_arr = json_decode($response);
				$success = $response_arr->success;
				if($success)
					$this -> db -> query("update `send_notifications` set `android_success`=(`android_success`+".$response_arr->success.") WHERE `id`=" . $notification -> id);
			}
		}
	}

	private function push_android($deviceRegistrationId, $messageText, $mesgID) {
		$apiKey = 'AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs';
		$headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $apiKey);
		$data = array('data' => array('message' => $messageText, 'type' => '7', 'mID' => $mesgID,'sAt' => time()), 'registration_ids' => $deviceRegistrationId);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send");
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

		$response = curl_exec($ch);
		curl_close($ch);
		file_put_contents('push_android_log.txt', '
'.date('Y-m-d H:i:s').' # '.json_encode($data).' # '.$response,FILE_APPEND);
		return $response;
	}

	public function send_notif_ios() {
		$this -> db -> select('id,ios_status,message,where_condition,ios_max_user_id');
		$not_where = "(sendVia='notification' or sendVia='all') and ios_status!='Completed'";
		$this -> db -> where($not_where);
		$this -> db -> order_by('id', 'asc');
		$this -> db -> limit(1);
		$q = $this -> db -> get('send_notifications');
		$notification = $q -> row();
		
		if (!empty($notification -> id)) {
			$updated_arr = array();
			if ($notification -> ios_status == 'Pending')
				$updated_arr['ios_status'] = 'In progress';
			if (!$notification -> ios_max_user_id)
				$notification -> ios_max_user_id = 0;
			$this -> db -> select('userId,pushnotificationtoken');
			$where = $notification -> where_condition . " and devicetype='2' and userID >" . $notification -> ios_max_user_id;
			$this -> db -> where($where, NULL, FALSE);
			$this -> db -> order_by('userId', 'asc');
			$this -> db -> limit(25);
			$q2 = $this -> db -> get('user');
			$users = $q2 -> result_array();

			if (isset($users[count($users) - 1]['userId']))
				$updated_arr['ios_max_user_id'] = $users[count($users) - 1]['userId'];
			else
				$updated_arr['ios_status'] = 'Completed';

			$this -> db -> where(array('id' => $notification -> id));
			$this -> db -> update('send_notifications', $updated_arr);

			$tokens = array_column($users, 'pushnotificationtoken');
			if ($tokens)
			{
				$arr = array_chunk($tokens, 5);
				foreach ($arr as $five_token) {
					$this -> push_ios($five_token, $notification -> message, $notification -> id);
				}
				$this -> db -> query("update `send_notifications` set `ios_success`=(`ios_success`+".count($tokens).") WHERE `id`=" . $notification -> id);
			}
		}
	}

	private function push_ios($deviceToken, $message, $mesgID) {

		$passphrase = "tawasolit";
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', FCPATH . 'third_party/fcb.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)exit();
		
		$body['aps']['alert'] = array('body' => $message);
		$body['aps']['type'] = '7';
		$body['aps']['mID'] = $mesgID;
		$body['aps']['sAt'] = time();
		$body['badge'] = '1';

		$payload = json_encode($body, JSON_UNESCAPED_UNICODE);
		$log = '';
		foreach($deviceToken as $one_device)
		{
			$msg = chr(0) . pack('n', 32) . pack('H*', $one_device) . pack('n', strlen($payload)) . $payload;
			fwrite($fp, $msg, strlen($msg));
			$apple_error_response = fread($fp, 6);
			$error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);
			$log.= ' @ '.$error_response['status_code'].' - '.$apple_error_response;
		}
		
		fclose($fp);
		
		file_put_contents('push_ios_log.txt', '
'.date('Y-m-d H:i:s').' # '.json_encode($body).' # '.$log,FILE_APPEND);
	}
}
?>