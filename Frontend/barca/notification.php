<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		/* Standard Libraries */
		$this->load->database();
		/* ------------------ */
	
	}












    public  function getNotifications()
    {
        $json_data = $this->input->post('data');

        //$json_data ='{"keyword" : "mo","sortedBy":2 ,"favoritesFilter":0}';
        $arr_data = json_decode($json_data);

        $userId = ($arr_data->userId);
        $lastRequestTime = ($arr_data->lastRequestTime);
        $pageNo = ($arr_data->pageNo);
        if( empty($userId) || empty($pageNo))
        {
            print_r(json_encode(array('status'=>-1)));
        }
        else
        {
            $start = ($pageNo-1)*20;
            $select = "n.notificationId , n.senderId , n.notificationType , n.receiverId , n.postId , n.notificationtime as time , sender.fullname as senderFullName ,receiver.fullname as  receiverFullName ";
            $additionalParams['join'] = array('user as sender','sender.userId=n.senderId ','left');
            $additionalParams['join2'] = array('user as receiver','receiver.userId=n.receiverId ','left');
            $where = "((receiverId = ".$userId." AND (notificationType = 1 or notificationType = 2 or notificationType = 3 or notificationType = 4 ) ) OR ( senderId in (select fanId from userfans where isFavourite = 1 AND userId = ".$userId.")  AND (notificationType = 5 or notificationType = 6 or notificationType = 7  ))  ) AND ( notificationtime > '".date('Y-m-d H:i:s',$lastRequestTime)."')";
            $notifications = $this->main_model->getBackendData('notification as n',$select,$where,'notificationtime','asc',20,$start,$additionalParams);
            if($notifications)
            {
                foreach($notifications as $notification)
                {
                    $notification->notificationId = intval($notification->notificationId);
                    $notification->senderId = intval($notification->senderId);
                    $notification->notificationType = intval($notification->notificationType);
                    $notification->receiverId = intval($notification->receiverId);
                    $notification->postId = intval($notification->postId);
                    $notification->time = strtotime($notification->time);
                    $notification->receiverFullName = strval($notification->receiverFullName);
                    $notification->senderFullName = strval($notification->senderFullName);
                    $data[] = $notification;
                }
                print_r(json_encode(array('status'=>2,'data'=>$data)));
            }
            else
                print_r(json_encode(array('status'=>2,'data'=>array())));

        }
    }


// private function




}