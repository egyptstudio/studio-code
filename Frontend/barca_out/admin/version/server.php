<?php
include_once ('../includes/check.php');
include_once ('header.php'); 

$_SESSION['lib'] = 13;
$database->query('SELECT * FROM fcbservers ORDER BY serverId DESC');
$database->execute();
$total_records = $database->rowCount();
?>

<div class="container-fluid">
    <div class="row">

<?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            
            <div class="row">
                
                <div class="col-md-12">
                    <h4 class="heading">Server Control</h4>
                    <div class="head_opts">
                        <a href="javascript:;" class="btn add_version btn-primary">Add New Server &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                    </div>
                </div>
                
                <div class="postSection">
                <div class="aj_sec">
                <?php if($total_records > 0) { ?>
                    
                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Server Name</th>
                                <th>Description</th>
                                <th>Base URL</th>
                                <th style="text-align:center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                            foreach ($result as $row) {
                            ?>
                            <tr>
                                <td>
                                    <span><?php echo $row['name']; ?></span>
                                </td>
                                <td>
                                    <span><?php echo $row['description']; ?></span>
                                </td>
                                <td>
                                    <span><?php echo $row['baseURL']; ?></span>
                                </td>
                                <td style="text-align:center">
                                    <a href="javascript:;" class="btn btn-info btn-edit" data-id="<?php echo $row['serverId']; ?>"><i class="fa fa-pencil"></i> Edit</a>
                                    <a href="javascript:;" class="btn btn-danger btn-delete" data-id="<?php echo $row['serverId']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                                
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php } else { 
                    echo "<h2 style='margin-left: 20px'>List of FCB Version will be displayed here.</h2>"; 
                } ?>
                
                </div><!-- aj_sec -->
                </div><!-- postSection -->

            </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Server</h4>
      </div>
      <div class="modal-body">
        
        <form class="form-horizontal myform">
        <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Name</h4></label>
            <div class="col-sm-8">
                <input type="text" value="" name="1" class="getValue form-control required" data-key="name">
            </div>
        </div>

        <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Base URL</h4></label>
            <div class="col-sm-8">
                <textarea class="getValue required form-control" name="2" data-key="baseURL"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Description</h4></label>
            <div class="col-sm-8">
                <textarea class="getValue required form-control" name="3" data-key="description"></textarea>
            </div>
        </div>

        
        
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary save_version">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data"></div>
    </div>
  </div>
</div>

<?php
include_once ('footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(".add_version").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("../aj_data.php?command=getServer&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 700);
        });

        $(document).on('click', '.save_version', function(e){
            e.preventDefault();
            if (!$(".myform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                    childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            showLoader();
          
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'fcbservers' + '&db=' + arrFields + '&data=' + arrValues + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        $("#myModal").modal('hide');
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            $("#myModal").modal('hide');
                            $(".myform")[0].reset();
                        }); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  
        $(document).on('click', '.btn-delete', function(){
            if(!confirm('Are you sure, you want to delete server?')) return false;
            db = "serverId";
            value = $(this).data('id');
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'fcbservers' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec"); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });

        $(document).on('click', '.edit_version', function(e){
            e.preventDefault();
            if (!$(".myeditform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                    childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            
            var key = "serverId";
            var value = $(this).data('id');
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'fcbservers' + '&db=' + arrFields + '&data=' + arrValues + '&key=' + key + '&value=' + value + '&command=' + 'update',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        $("#myModal").modal('hide');
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            $("#myEditModal").modal('hide');
                        }); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  

    });

</script>