
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../js/jquery.validate.js"></script>

    <script src="../plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <script src="../plugins/upload/jquery.uploadfile.min.js"></script>

    <script src="../js/scripts.js"></script>
  </body>
</html>