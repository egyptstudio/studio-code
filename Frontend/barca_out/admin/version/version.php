<?php
include_once ('../includes/check.php');
include_once ('header.php'); 

$_SESSION['lib'] = 12;
$database->query('SELECT * FROM fcbversion ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
?>

<div class="container-fluid">
    <div class="row">

<?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            
            <div class="row">
                
                <div class="col-md-12">
                    <h4 class="heading">Version Control</h4>
                    <div class="head_opts">
                        <a href="javascript:;" class="btn add_version btn-primary">Add New Version &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                    </div>
                </div>
                
                <div class="postSection">
                <div class="aj_sec">
                <?php if($total_records > 0) { ?>
                    
                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Version No.</th>
                                <th>Type</th>
                                <th>Server</th>
                                <th>Status</th>
                                <th style="text-align:center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                if($row['deviceType'] == 1)
                                {
                                    $device = "Android";
                                } else
                                {
                                    $device = "iOS";
                                }
                                if($row['updateStatus'] == 1)
                                {
                                    $update = "No Update";
                                } else if($row['updateStatus'] == 2)
                                {
                                    $update = "Optional Update";
                                } else if($row['updateStatus'] == 3)
                                {
                                    $update = "Force Update";
                                }
                                $serverId = $row['serverId'];
                                $database->query('SELECT name FROM fcbservers WHERE serverId = :id');
                                $database->bind(":id", $serverId);
                                $database->execute();
                                $serverRow = $database->single();
                            ?>
                            <tr>
                                <td>
                                    <span><?php echo $row['versionNumber']; ?></span>
                                </td>
                                <td>
                                    <span><?php echo $device; ?></span>
                                </td>
                                <td>
                                    <span><?php echo $serverRow['name']; ?></span>
                                </td>
                                <td>
                                    <span><?php echo $update; ?></span>
                                </td>
                                <td style="text-align:center">
                                    <a href="javascript:;" class="btn btn-info btn-edit" data-id="<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i> Edit</a>
                                    <a href="javascript:;" class="btn btn-danger btn-delete" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                                
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php } else { 
                    echo "<h2 style='margin-left: 20px'>List of FCB Version will be displayed here.</h2>"; 
                } ?>
                
                </div><!-- aj_sec -->
                </div><!-- postSection -->

            </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Version</h4>
      </div>
      <div class="modal-body">
        
        <form class="form-horizontal myform">
        <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version No.</h4></label>
            <div class="col-sm-8">
                <input type="text" value="" name="1" class="getValue form-control required" data-key="versionNumber">
            </div>
        </div>

        <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version Description</h4></label>
            <div class="col-sm-8">
                <textarea class="getValue required form-control" name="2" data-key="description"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version type</h4></label>
            <div class="col-sm-8">
                <select class="getValue form-control required" name="3" data-key="deviceType">
                    <option value="1">Android</option>
                    <option value="2">iOS</option>
                </select>  
            </div>
        </div>

        <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version server</h4></label>
            <div class="col-sm-8">
                <select class="getValue form-control required" name="4" data-key="serverId">
                    <?php 
                        $database->query('SELECT serverId, name FROM fcbservers');
                        $database->execute();
                        $serverRow = $database->resultset();
                        foreach ($serverRow as $row) {
                            echo '<option value="'.$row['serverId'].'">'.$row['name'].'</option>';
                        }
                    ?>
                </select>  
            </div>
        </div>

        <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version status</h4></label>
            <div class="col-sm-8">
                <select class="getValue form-control required" name="5" data-key="updateStatus">
                    <option value="1">No Update</option>
                    <option value="2">Optional Update</option>
                    <option value="3">Force Update</option>
                </select>  
            </div>
        </div>
        
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary save_version">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data"></div>
    </div>
  </div>
</div>

<?php
include_once ('footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(".add_version").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("../aj_data.php?command=getVersion&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 700);
        });

        $(document).on('click', '.save_version', function(e){
            e.preventDefault();
            if (!$(".myform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                    childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'fcbversion' + '&db=' + arrFields + '&data=' + arrValues + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        $("#myModal").modal('hide');
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            $("#myModal").modal('hide');
                            $(".myform")[0].reset();
                        }); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  
        $(document).on('click', '.btn-delete', function(){
            if(!confirm('Are you sure, you want to delete version?')) return false;
            db = "id";
            value = $(this).data('id');
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'fcbversion' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec"); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });

        $(document).on('click', '.edit_version', function(e){
            e.preventDefault();
            if (!$(".myeditform").validate().form()) {
                return false;
            }
            var arrFields = $(".myeditform .getValue").map(function() {
                var $elm = $(this),
                    childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myeditform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            
            var key = "id";
            var value = $(this).data('id');
            
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'fcbversion' + '&db=' + arrFields + '&data=' + arrValues + '&key=' + key + '&value=' + value + '&command=' + 'update',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        $("#myModal").modal('hide');
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            $("#myEditModal").modal('hide');
                        }); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  

    });

</script>