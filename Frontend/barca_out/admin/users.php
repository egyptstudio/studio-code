<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 
unset($_SESSION['userFlag']);
unset($_SESSION['userId']);

if(!isset($_REQUEST['id']) || $_REQUEST['id'] == "" || !is_numeric($_REQUEST['id']))
{
    //header('location: index.php');
    $id = 3;
} else {
    $id = $_REQUEST['id'];
}

$_SESSION['lib'] = $id;

$state = isset($_REQUEST['state']) ? $_REQUEST['state'] : '0';
if(!is_numeric($state))
{
    $state = 0;
}
if($state==0)
{
    $text = "Block";
    $updState = 1;
} else
{
    $text = "Unblock";
    $updState = 0;
}

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}
$startFrom = ($page-1) * $recPerPage;

if($_SESSION['lib'] == 3)
{
    if(isset($_REQUEST['query']) && $_REQUEST['query'] != '')
    {
        $query = $_REQUEST['query'];
        $database->query('SELECT * FROM user WHERE (userType = :type AND blocked = :block) AND fullName LIKE "%'.$query.'%" ORDER BY registrationDate DESC');
        $database->bind(':type', 3);
    } else 
    {
        $database->query('SELECT * FROM user WHERE userType = :type AND blocked = :block ORDER BY registrationDate DESC');
        $database->bind(':type', 3);       
    }
    $heading = "Dummy";
} else if($_SESSION['lib'] == 4)
{
    if(isset($_REQUEST['query']) && $_REQUEST['query'] != '')
    {
        $query = $_REQUEST['query'];
        $database->query('SELECT * FROM user WHERE (userType = :type OR userType = :type2) AND blocked = :block AND fullName LIKE "%'.$query.'%" ORDER BY registrationDate DESC');
        $database->bind(':type', 1);
        $database->bind(':type2', 2);
    } else 
    {
        $database->query('SELECT * FROM user WHERE (userType = :type OR userType = :type2) AND blocked = :block ORDER BY registrationDate DESC');
        $database->bind(':type', 1);
        $database->bind(':type2', 2);
    }
    $heading = "App";
} else if($_SESSION['lib'] == 5)
{
    if(isset($_REQUEST['query']) && $_REQUEST['query'] != '')
    {
        $query = $_REQUEST['query'];
        $database->query('SELECT * FROM user WHERE (userType = :type AND blocked = :block) AND fullName LIKE "%'.$query.'%" ORDER BY registrationDate DESC');
        $database->bind(':type', 4);
    } else 
    {
        $database->query('SELECT * FROM user WHERE userType = :type AND blocked = :block ORDER BY registrationDate DESC');
        $database->bind(':type', 4);
    }
    $heading = "Studio";
}
$database->bind(':block', $state);
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

if($_SESSION['lib'] == 3)
{
    if(isset($_REQUEST['query']) && $_REQUEST['query'] != '')
    {
        echo $query = $_REQUEST['query'];
        $database->query('SELECT * FROM user WHERE (userType = :type AND blocked = :block) AND fullName LIKE "%'.$query.'%" ORDER BY registrationDate DESC LIMIT :start, :num');
        $database->bind(':type', 3);
    } else 
    {
        $database->query('SELECT * FROM user WHERE userType = :type AND blocked = :block ORDER BY registrationDate DESC LIMIT :start, :num');
        $database->bind(':type', 3);
    }
} else if($_SESSION['lib'] == 4)
{
    if(isset($_REQUEST['query']) && $_REQUEST['query'] != '')
    {
        $query = $_REQUEST['query'];
        $database->query('SELECT * FROM user WHERE (userType = :type OR userType = :type2) AND blocked = :block AND fullName LIKE "%'.$query.'%" ORDER BY registrationDate DESC LIMIT :start, :num');
        $database->bind(':type', 1);
        $database->bind(':type2', 2);
    } else 
    {
        $database->query('SELECT * FROM user WHERE (userType = :type OR userType = :type2) AND blocked = :block ORDER BY registrationDate DESC LIMIT :start, :num');
        $database->bind(':type', 1);
        $database->bind(':type2', 2);
    }
} else if($_SESSION['lib'] == 5)
{
    if(isset($_REQUEST['query']) && $_REQUEST['query'] != '')
    {
        $query = $_REQUEST['query'];
        $database->query('SELECT * FROM user WHERE (userType = :type AND blocked = :block) AND fullName LIKE "%'.$query.'%" ORDER BY registrationDate DESC LIMIT :start, :num');
        $database->bind(':type', 4);
    } else 
    {
        $database->query('SELECT * FROM user WHERE userType = :type AND blocked = :block ORDER BY registrationDate DESC LIMIT :start, :num');
        $database->bind(':type', 4);
    }
}

$database->bind(':block', $state);

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

<?php include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            
            <div class="row">
                
                <div class="col-md-12">
                    <h4 class="heading"><?php echo $heading; ?> Users</h4>
                    <?php if($_SESSION['lib'] != 4) {?>
                    <div class="head_opts">
                        <a href="add_user.php" class="btn unsetUser btn-primary">Create new User &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                    </div>
                    <?php } ?>
                </div>
                
                <div class="photoOpts2">
                    <div class="chkboxOpts">
                        <label> <input id="selectall" type="checkbox"> Select / Unselect All</label>
                    </div>
                    <button class="btn btn-primary pull-right <?php echo $text; ?>SelectedUser">
                        <?php echo $text; ?> selected
                    </button>
                    
                </div>
                
                <div class="photoOpts1" style="border: 0;width: 70%;margin-top: -62px;">
                        <div class="btn-group" role="group" aria-label="...">
                          <a href="users.php?id=<?php echo $_SESSION['lib']; ?>&page=<?php echo $page; ?>&state=0" class="btn btn-primary <?php if($state==0) {echo 'activePlayerMenu'; } ?>">Normal</a>
                          <a href="users.php?id=<?php echo $_SESSION['lib']; ?>&page=<?php echo $page; ?>&state=1" class="btn btn-primary <?php if($state==1) {echo 'activePlayerMenu'; } ?>">Blocked</a>
                        </div>
                </div>

            
                <div class="postSection">
                <div class="aj_sec">
                <?php if($total_records > 0) { ?>
                <div class="opts3">
                    <nav class="siteNav pull-left">
                      <ul class="pagination">
                        <li>
                          <?php if(isset($_REQUEST['query']) && $_REQUEST['query'] != '') { 
                            $query = $_REQUEST['query'];
                          ?>
                          <a href="users.php?id=<?php echo $_SESSION['lib']; ?>&page=1&query=<?php echo $query; ?>" aria-label="Previous">
                          <?php } else {?>
                          <a href="users.php?id=<?php echo $_SESSION['lib']; ?>&page=1" aria-label="Previous">
                          <?php } ?>
                            <span aria-hidden="true">&laquo;</span>
                          </a>
                        </li>
                        <?php
                        if($page == 1)
                        {
                            $prevPage = 1;
                        } else 
                        {
                            $prevPage = ($page - 1);
                        }

                        if($page == $total_pages)
                        {
                            $nextPage = $total_pages;
                        } else 
                        {
                            $nextPage = ($page + 1);
                        }
                        if(isset($_REQUEST['query']) && $_REQUEST['query'] != '') {
                            $query = $_REQUEST['query'];
                            echo '<li><a href="users.php?id='.$_SESSION["lib"].'&page='.$prevPage.'&query='.$query.'">&lt;</a></li>';
                            echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                            echo '<li><a href="users.php?id='.$_SESSION["lib"].'&page='.$nextPage.'&query='.$query.'">&gt;</a></li>';
                        } else 
                        {
                            echo '<li><a href="users.php?id='.$_SESSION["lib"].'&page='.$prevPage.'">&lt;</a></li>';
                            echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                            echo '<li><a href="users.php?id='.$_SESSION["lib"].'&page='.$nextPage.'">&gt;</a></li>';
                        }
                        ?>
                        <li>
                          <a href="users.php?id=<?php echo $_SESSION['lib']; ?>&page=<?php echo $total_pages ?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                          </a>
                        </li>
                      </ul>
                    </nav>
                    <div class="pull-right col-sm-3">
                        <div class="form-group">
                            <form action="">
                                <div class="input-group">
                                  <input type="text" class="form-control searchInput" placeholder="Search">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default search" type="button"><i class="fa fa-search"></i></button>
                                  </span>
                                </div><!-- /input-group -->
                            </form>
                        </div>
                    </div>
                </div>
                    
                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                                foreach ($result as $row) {

                                    $haystack = $row['profilePic'];
                                    $needle = 'http';

                                    if (strpos($haystack,$needle) !== false) {
                                        $imgPath = $haystack;
                                    } else 
                                    {
                                        $imgPath = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/profile/".$haystack;
                                    }
                            ?>
                            <tr>
                                <td>
                                    <input class="post_photo" data-id=<?php echo $row['userId']; ?> type="checkbox">
                                    <img width="100" src="<?php echo $imgPath; ?>" alt="User">
                                    <span><?php echo $row['fullName']; ?></span>
                                </td>
                                <td>
                                    <span><?php echo $row['email']; ?></span>
                                </td>
                                <td align="center" class="actions">
                                    <?php if($_SESSION['lib'] != 4) { ?>
                                    <a  href="user_detail.php?id=<?php echo $row['userId']; ?>" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                    <?php } ?>
                                    <a href="#" class="btn btn-danger <?php echo $text; ?>User" data-id="<?php echo $row['userId'] ?>"><i class="fa fa-ban"></i> <?php echo $text; ?></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php } else { 
                    echo "<h2 style='margin-left: 20px'>List of users will be displayed here.</h2>"; 
                } ?>
                
                </div><!-- aj_sec -->
                </div><!-- postSection -->

            </div>
                
            </div>
        </div>
    </div>
</div>


<?php
include_once ('includes/footer.php'); ?>
<script type="text/javascript" src="js/users.js"></script>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;

        $(document).on('click', '.<?php echo $text; ?>User', function(event){
            showLoader();
            event.preventDefault();
            var arrFields = ['blocked'];
            var arrValues = ['<?php echo $updState; ?>'];
            var id = $(this).attr('data-id');
            $.ajax({
                type: 'POST',
                url: 'includes/processing.php',
                data: 'table=' + 'user' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + 'userId' + '&value=' + id + '&command=' + 'update',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == "correct")
                    {
                        showNotification('success', "Process done successfully!");
                        $(".postSection").load(''+loc+qs+' .aj_sec');
                    } else 
                    {
                        showNotification('error', "There is some issue, Contact administration!");
                    }
                }
            });
        });


        $(document).on('click', '.<?php echo $text; ?>SelectedUser', function(event){
            event.preventDefault();
            var arrFields = ['blocked'];
            var arrIds = $(".post_photo:checked").map(function() {
                var $elm = $(this),
                    childId = $elm.data('id');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();

            showLoader();
            $.ajax({
                type: 'POST',
                url: 'includes/processing.php',
                data: 'ids=' + encodeURIComponent(arrIds) + '&state=' + <?php echo $updState; ?> + '&command=' + 'changeUserState',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == "correct")
                    {
                        showNotification('success', "Process done successfully!");
                        $(".postSection").load(''+loc+qs+' .aj_sec');
                    } else 
                    {
                        showNotification('error', "There is some issue, Contact administration!");
                    }
                }
            });
        });
        

        $(document).on('click', '.search', function(){
            var query = $(".searchInput").val();
            window.location.href = 'users.php?id=' + <?php echo $id ?> + '&query=' + query;
        })
    });
</script>
