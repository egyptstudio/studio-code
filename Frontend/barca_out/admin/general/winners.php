<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$imgUrl = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/users/";

if(!isset($_REQUEST['id']) || $_REQUEST['id'] == "" || !is_numeric($_REQUEST['id']))
{
    header('location: contests.php');
}
$id = $_REQUEST['id'];
$database->query('SELECT name, startDateTime, status, endDateTime FROM contest WHERE contestId = :id');
$database->bind(":id", $id);
$database->execute();
$row = $database->single();


$database->query('SELECT * FROM contest_text_state WHERE contestId = :id');
$database->bind(":id", $id);
$database->execute();
$stateRow = $database->single();

?>
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">
<div class="container-fluid">
    <div class="row">

<?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            <input type="hidden" value="<?php echo $id; ?>" id="contestId">
            <div class="row">
                
                <div class="col-md-12">
                    <div style="right: auto; font-size: 20px; top: 8px;" class="head_opts">
                        <a href="contests.php"><i class="fa fa-arrow-left"></i> Contests</a>
                    </div>
                    
                    <h4 class="heading"><?php echo $row['name']; ?></h4>
                    <div class="head_opts">
                        <?php if($row['status'] != 4) { ?>
                        <a href="#" class="btn btn-success announce_result">Announce</a>
                        <a href="#" class="btn btn-primary save_contest_state">Save</a>
                        <?php } ?>
                    </div>
                </div>
                
                    
                <div style="margin-bottom: 10px;" class="col-md-12">
                    <div class="col-sm-4">
                        Announcement Notifiation
                    </div>
                    <div class="col-sm-8">
                        <?php if($row['status'] != 4) { ?>
                        <textarea class="form-control notification"><?php echo $stateRow['notification']; ?></textarea>
                        <?php } else { echo $stateRow['notification']; } ?>
                    </div>
                    
                </div>

                <div class="col-md-12">
                    <div class="col-sm-4">
                        Email Notifiation
                    </div>
                    <div class="col-sm-8">
                        <?php if($row['status'] != 4) { ?>
                        <textarea class="form-control email_noti"><?php echo $stateRow['email']; ?></textarea>
                        <?php } else { echo $stateRow['email']; } ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="winner_sec border_top">
                    <div class="col-sm-6 winner_lt">
                        <h4>Top 100 Posts</h4>

                        <table class="table table-bordered" style="background: white">
                            <thead>
                                <tr>
                                    <th style="background: #999">Poster Name</th>
                                    <th style="background: #999">Image Link</th>
                                    <th style="background: #2e6da4">Points</th>
                                </tr>
                            </thead>
                            <?php 
                                $database->query('SELECT postPhotoUrl, postPhotoCaption, userId, postId, postPoints FROM post WHERE creationTime >= :startTime AND creationTime <= :endTime ORDER BY postPoints DESC LIMIT 0, 100');
                                $database->bind(':startTime', $row['startDateTime']);
                                $database->bind(':endTime', $row['endDateTime']);
                                $database->execute();
                                $result = $database->resultset();
                            ?>
                            <tbody class="post_body">
                                <?php $i=0; foreach ($result as $postRow) {  $i = $i+1; 
                                    $database->query('SELECT postId FROM contest_winner_state WHERE postId = :postid');
                                    $database->bind(":postid", $postRow['postId']);
                                    $database->execute();
                                    $checkRow = $database->single();
                                ?>
                                <tr>
                                    <td>
                                    <?php if($row['status'] != 4) { ?>
                                    <input <?php if($checkRow['postId'] == $postRow['postId']) { echo 'style="display:none"';  }?> id="p<?php echo $postRow['postId']; ?>" data-userid="<?php echo $postRow['userId']; ?>" class="post_checkbox" type="checkbox">  
                                    <?php } ?>

                                    <?php echo $postRow['postPhotoCaption']; ?></td>
                                    <td><a class="postPhoto" href="#" data-href="<?php echo $imgUrl.$postRow['userId']."/". $postRow['postPhotoUrl']; ?>">Link</a></td>
                                    <td><?php echo $postRow['postPoints']; ?></td>
                                </tr>
                                <?php } ?>
                                
                            </tbody>
                        </table>
                    </div>
                    
                    <!--<div class="col-sm-2 winner_md">
                        <button class="btn btn-primary add_to_winners">Add to Winners</button>
                        <button class="btn btn-danger remove_from_winners">Remove from Winners</button>
                    </div>-->
                    
                    <div class="col-sm-6 winner_rt">
                        <h4>Winners (Top 10 Posts)</h4>

                        <table class="table table-bordered" style="background: white">
                            <thead>
                                <tr>
                                    <th style="background: #999">Poster Name</th>
                                    <th style="background: #999">Image Link</th>
                                    <th style="background: #2e6da4">Points</th>
                                </tr>
                            </thead>
                            <tbody class="shadow_body sortableClass" id="sortable">
                                
                            
                            <?php 
                                $database->query('SELECT cws.*, p.postId, p.postPhotoUrl, p.postPhotoCaption, p.userId, p.postPoints 
                                                  FROM contest_winner_state cws, post p 
                                                  WHERE cws.postId = p.postId AND cws.contestId = :id LIMIT 0, 10');
                                $database->bind(':id', $id);
                                $database->execute();
                                $result = $database->resultset();
                            ?>
                               <?php $i=0; foreach ($result as $postRow) {  $i = $i+1; ?>
                                <tr>
                                    <td>
                                    <?php if($row['status'] != 4) { ?>
                                    <i class="fa fa-arrows move-arrows"></i> <input id="p<?php echo $postRow['postId']; ?>" data-userid="<?php echo $postRow['userId']; ?>" class="post_checkbox" type="checkbox">
                                    <?php } ?>

                                      <?php echo $postRow['postPhotoCaption']; ?></td>
                                    <td><a class="postPhoto" href="#" data-href="<?php echo $imgUrl.$postRow['userId']."/". $postRow['postPhotoUrl']; ?>">Link</a></td>
                                    <td><?php echo $postRow['postPoints']; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>

                
            </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Image</h4>
      </div>
      <div class="modal-body">
        <img src="" class="postPhotoHolder img-responsive" style="margin: 0 auto" alt="Image">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php
include_once ('../includes/footer.php'); ?>
<script type="text/javascript" src="../js/general.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script>
    $(function(){
        $("#sortable").sortable({ opacity: 0.6, cursor: 'move', update: function(event, ui) {
                setTimeout((function(){ $('#loader').hide(); $(".page_overlay").hide(); }),5);
                /*var arrList = $('.dream_step').map(function () {
                        return $(this).attr('id');
                    }).get();
                
                var list = 'idList='+ arrList + '&command=updateWinnerIds&contest_id=<?php echo $id; ?>';
                $.post("../includes/general_processing.php", list, function(data){
                    //alert(data);
                });*/
            }
        }).disableSelection();
    });
</script>