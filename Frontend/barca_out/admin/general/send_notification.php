<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php');
?>
<link rel="stylesheet" href="../plugins/datepicker/bootstrap-datetimepicker.min.css">
<div class="container-fluid">
    <div class="row">

        <?php
        include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">Send Notification</h4>
                        <div class="head_opts">
                            <button class="btn btn-default btn-black send_noti">Send</button>
                            <a href="notifications.php" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form class="form-horizontal myform">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Are Mandatory fields</h4></label>
                            </div>
                            
                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title">Date</h4></label>
                                
                                <div style="padding: 0 15px" class="input-group date col-sm-8" id="startDate">
                                    <input type="text" id="senddate" name="date" class="form-control getValue required" data-key="date" />
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                                </div>
                                
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title">Time</h4></label>
                                
                                <div style="padding: 0 15px" class="input-group date col-sm-8" id="startTime">
                                    <input type="text" id="sendtime" name="time" class="form-control getValue required" data-key="time" />
                                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                                </div>
                                
                            </div>
                            
                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Country</h4></label>
                                <div class="col-sm-8">
                                    <label class="pull-left"><input checked="" value="all" style="margin-top: 0" type="radio" class="country" name="country"> All Countries</label>
                                    <label style="margin-left: 15px;"><input style="margin-top: 0" class="country" value="select" type="radio" name="country"> Selected Country</label>
                                    <br>
                                    <div class="input-group col-sm-8 country_selection">
                                        <input data-role="tagsinput" id="country" disabled="disabled" type="text" class="form-control" name="countries" aria-describedby="basic-addon1">
                                    </div>                                
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Gender</h4></label>
                                <div class="col-sm-8">
                                    <select class="gender form-control">
                                        <option value="0">Both</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Send To</h4></label>
                                <div class="col-sm-8">
                                    <label class="pull-left"><input checked="" value="all" style="margin-top: 0" type="radio" class="sendto" name="sendto"> All Users</label>
                                    <label style="margin-left: 15px;"><input style="margin-top: 0" class="sendto" value="select" type="radio" name="sendto"> Selected Users</label>
                                    <br>
                                    <div class="input-group col-sm-8 user_selection">
                                        <input data-role="tagsinput" id="users" disabled="disabled" type="text" class="form-control" name="users" aria-describedby="basic-addon1">
                                    </div>                                
                                </div>
                            </div>

                            
                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Last Login</h4></label>
                                <div class="col-sm-8">
                                    <label class="pull-left"><input checked="" value="before" style="margin-top: 0" type="radio" class="login" name="lastlogin"> Before</label>
                                    <label style="margin-left: 15px;"><input style="margin-top: 0" class="login" value="after" type="radio" name="lastlogin"> After</label>
                                    <br>
                                    <div class="input-group date col-sm-8" id="logindate">
                                        <input type="text" id="lastlogin" name="lastlogin" class="form-control required" />
                                        <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                                    </div>                             
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Send Via</h4></label>
                                <div class="col-sm-8">
                                    <label class="pull-left"><input checked="" value="email" style="margin-top: 0" type="radio" class="sendvia" name="sendvia"> Email</label>
                                    <label style="margin-left: 15px;"><input value="notification" style="margin-top: 0" type="radio" class="sendvia" name="sendvia"> Notification</label>
                                    <label style="margin-left: 15px;"><input value="both" style="margin-top: 0" type="radio" class="sendvia" name="sendvia"> Both</label>
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l1" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Message</h4></label>
                                <div class="col-sm-8">
                                   <textarea class="form-control getValue required" id="message" data-key="message"></textarea>
                               </div>
                           </div>


                       </form>
                   </div>

               </div>
           </div>
       </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="aj_data">
      </div>
  </div>
</div>
</div>

<?php include_once ('../includes/footer.php'); ?>

<script src="../plugins/datepicker/moment.js"></script>
<script src="../plugins/datepicker /bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="../js/scripts.js"></script>
<script type="text/javascript" src="../js/notification.js"></script>
<script type="text/javascript">
    $(function(){

        $('#senddate').datetimepicker({
            pickTime: false,
            format: 'DD-MM-YYYY',
            minDate: moment()
        });
        $('#sendtime').datetimepicker({
            pickDate: false,
            //format: 'DD-MM-YYYY',
            minDate: moment()
        });
        $('#lastlogin').datetimepicker({
            //pickTime: false,
            //format: 'DD-MM-YYYY',
            //minDate: moment()
        });

        $(".sendto").click(function(){
            if($(".sendto:checked").val() == 'select')
            {
                $("#users").val('');
                $("#users").attr('data-id', '');
                country = $("#country").val();
                if(country == '')
                {
                    country = 0;
                } else {
                    country = $("#country").data('id');    
                }
                
                showLoader();
                setTimeout(function(){
                    $(".aj_data").load("notifcation_data.php?command=getUsers&country="+country+"", function(){
                        $("#myModal").modal('show');
                        hideLoader();
                    });
                }, 500);
            }
        });
        $(".country").click(function(){
            if($(".country:checked").val() == 'select')
            {
                $("#country").val('');
                $("#country").attr('data-id', '');
                showLoader();
                setTimeout(function(){
                    $(".aj_data").load("notifcation_data.php?command=getCountry", function(){
                        $("#myModal").modal('show');
                        hideLoader();
                    });
                }, 500);
            }
        });


        $(document).on('click', '#searchUser', function(e) {
            e.preventDefault();
            if($(this).data('id') == 'user')
            {
                command = 'searchUser';
                country = $(this).data('country')
            } else 
            {
                command = 'searchCountry';
                country = '';
            }
            if (!$(".searchform").validate().form()) {
                return false;
            }
            text = $(".user_text").val();
            showLoader();

            $.ajax({
                type: 'POST',
                url: 'notifcation_data.php',
                data: 'text=' + text + '&country=' + country + '&command=' + command,
                cache: false,
                success: function(data) {
                    
                    hideLoader();
                    
                    $(".users").html(data);
                }
            });
        });

        $(".send_noti").click(function(){

            if (!$(".myform").validate().form()) {
                return false;
            }
            var date = $("#senddate").val(),
                time = $("#sendtime").val(),
                logincheck = $(".login:checked").val(),
                lastlogin = $("#lastlogin").val();

            if(logincheck == 'before')
            {
                logincheck = '<=';
            } else {
                logincheck = '>=';
            }

            if($(".sendto:checked").val() == "all")
            {
                var sendto = "all";
            } else 
            {
                var sendto = $("#users").attr('data-id');
            }

            if($(".country:checked").val() == "all")
            {
                var country = "all";
            } else 
            {
                var country = $("#country").attr('data-id');
            }
            var gender = $(".gender").val(),
                sendvia = $(".sendvia:checked").val(),
                mesg = $("#message").val();
            
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/notification_processing.php',
                data: 'date=' + date + 
                      '&time=' + time + 
                      '&sendto=' + sendto + 
                      '&country=' + country + 
                      '&logincheck=' + logincheck + 
                      '&lastlogin=' + lastlogin + 
                      '&gender=' + gender + 
                      '&sendvia=' + sendvia + 
                      '&mesg=' + mesg + 
                      '&command=' + 'sendNotification',
                cache: false,
                success: function(data) {
                    console.log(data);
                    hideLoader();
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        setTimeout(function(){
                            window.location.href = 'notifications.php';
                        }, 1000)
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });

        });

});
</script>
