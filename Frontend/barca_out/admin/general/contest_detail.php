<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 
if(!isset($_REQUEST['id']) || $_REQUEST['id'] == "" || !is_numeric($_REQUEST['id']))
{
    header('location: contests.php');
}
$id = $_REQUEST['id'];
$database->query('SELECT * FROM contest WHERE contestId = :id');
$database->bind(":id", $id);
$database->execute();
$row = $database->single();

if($row['status'] == 1)
{
    $status = 'Not started';
} else if($row['status'] == 2)
{
    $status = 'Running';
} else if($row['status'] == 3)
{
    $status = 'Finished';
} else if($row['status'] == 4)
{
    $status = 'Prizes Announced';
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <div style="right: auto; font-size: 20px; top: 8px;" class="head_opts">
                            <a href="contests.php"><i class="fa fa-arrow-left"></i> Contests</a>
                        </div>

                        <h4 class="heading"><?php echo $row['name']; ?></h4>
                        <div class="head_opts">
                            <a href="edit_contest.php?id=<?php echo $id; ?>" class="btn unsetUser btn-primary">Edit &nbsp;&nbsp;&nbsp;<i class="fa fa-edit"></i></a>
                        </div>
                    </div>

                    
                    <div style="margin-bottom: 10px;" class="col-md-12">
                        <div class="col-sm-4">
                            Start Date <strong><?php echo date('d/m/Y', strtotime($row['startDateTime'])); ?></strong>
                        </div>
                        <div class="col-sm-4">
                            Start Time <strong><?php echo date('h:i A', strtotime($row['startDateTime'])); ?></strong>
                        </div>
                        <div class="col-sm-4">
                            Contest Status <strong><?php echo $status; ?></strong>
                        </div>                
                    </div>    
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            End Date <strong><?php echo date('d/m/Y', strtotime($row['endDateTime'])); ?></strong>
                        </div>
                        <div class="col-sm-4">
                            End Time <strong><?php echo date('h:i A', strtotime($row['endDateTime'])); ?></strong>
                        </div>
                    </div>

                    <div class="prizeImgs col-md-12">
                        <div class="aj_data">
                            <?php 
                            $database->query('SELECT contestPrizeId, contestId, prizeText, prizeImageUrl FROM contestPrize WHERE contestId = :id');
                            $database->bind(':id', $id);
                            $database->execute();
                            $result = $database->resultset();
                            foreach ($result as $pRow) { 
                                $haystack = $pRow['prizeImageUrl'];
                                $needle = 'http';

                                if (strpos($haystack,$needle) !== false) {
                                    $imgPath = $haystack;
                                } else 
                                {
                                    $imgPath = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/prizes/".$haystack;
                                }

                                $prizeId = $pRow['contestPrizeId'];
                                $database->query('SELECT * FROM contest_prize_trans WHERE prizeId = :id');
                                $database->bind(':id', $prizeId);
                                $database->execute();
                                $result = $database->resultset();
                                ?>

                                <div class="border_top prizeDiv col-md-12">
                                    <div class="col-sm-5">
                                        <a href="javascript:;" data-id="<?php echo $prizeId; ?>" class="btn btn-primary add_trans"><i class="fa fa-language"></i> Translate</a><br>
                                        <?php echo "<b>en</b><br>". $pRow['prizeText']; ?>
                                        <?php foreach ($result as $transRow) { 

                                            $lang = $transRow['lang'];
                                            if(is_numeric($lang))
                                            {
                                                $database->query('SELECT * FROM languages WHERE id = :langId');
                                                $database->bind(":langId", $lang);
                                                $database->execute();
                                                $langRow = $database->single();
                                                $langText = $langRow['name'];
                                            } else {
                                                $langText = $lang;
                                            }
                                        ?>
                                            <div>
                                                <br><b><?php echo $langText; ?> </b>
                                                <a href="javascript:;" class="del_trans" data-id="<?php echo $transRow['transId'] ?>">X</a>
                                                <br><?php echo $transRow['prizeTransText']; ?>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="col-sm-6">
                                        <img class="img-responsive" src="<?php echo $imgPath; ?>" />
                                    </div>
                                </div>

                                <?php } ?>
                            </div>
                        </div>

                        <?php 
                        if($row['status'] != 2 && $row['status'] != 1) { ?>
                        <div class="prizeDiv col-md-12" style="height: 100%">
                            <a class="userLink" href="winners.php?id=<?php echo $id; ?>"><span class="pull-left">Winners</span> <i class="pull-right fa fa-arrow-right"></i></a>
                        </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Translation</h4>
            </div>
            <form class="form-horizontal myform">
                <div class="modal-body">
                    <input type="hidden" class="getValue" data-key="prizeId" value="" id="prizeId">
                    <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Language</h4></label>
                        <div class="col-sm-8">
                            <select class="getValue gallery-control form-control" data-key="lang">
                                <?php 
                                $database->query("SELECT * FROM languages");
                                $database->execute();
                                $result = $database->resultset();
                                foreach ($result as $langRow) {
                                    echo '<option value="'.$langRow['name'].'">'.$langRow['name'].'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Text</h4></label>
                        <div class="col-sm-8">
                            <input type="text" value="" class="getValue form-control" data-key="prizeTransText">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary save_lang">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>



<?php
include_once ('../includes/footer.php'); ?>
<script type="text/javascript" src="../js/general.js"></script>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        $(document).on('click', '.add_trans', function(){
            $("#myModal").modal('show');
            $("#prizeId").val($(this).data('id'));
        });
        $(document).on('click', '.save_lang', function(){
            showLoader();
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
        
            $.ajax({
                type: 'POST',
                url: '../includes/processing.php',
                data: 'table=' + 'contest_prize_trans' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    if(data == 'correct')
                    {
                        $(".prizeImgs").load(""+loc+qs+" .aj_data", function(){
                            hideLoader();
                            showNotification('success', 'Process done successfully!');
                            $(".myform")[0].reset();
                            $("#myModal").modal('hide');
                        });
                        
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  

        $(document).on('click', '.del_trans', function(){
            if(!confirm('Are you sure, you want to delete data?')) return false;
            db = "transId";
            value = $(this).data('id');
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/processing.php',
                data: 'table=' + 'contest_prize_trans' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
                cache: false,
                success: function(data) {
                    if(data == 'correct')
                    {
                        $(".prizeImgs").load(""+loc+qs+" .aj_data", function(){
                            hideLoader();
                            showNotification('success', 'Process done successfully!');
                        });
                        
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  
});
</script>
