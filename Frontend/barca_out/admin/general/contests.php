<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 
$_SESSION['lib'] = 7;

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}

$startFrom = ($page-1) * $recPerPage;

$database->query('SELECT * FROM contest ORDER BY contestId DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM contest ORDER BY status=4, status=1, status=3, status=2 LIMIT :start, :num');
$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();
$result = $database->resultset();

if($total_records> $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}
if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}

?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Contest Management</h4>
                        <div class="head_opts">
                            <a href="add_contest.php" class="btn unsetUser btn-primary">Create new Contest &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>


                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-right">
                                  <ul class="pagination">
                                    <li>
                                      <a href="contests.php?id=<?php echo $_SESSION['lib']; ?>&page=1" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <?php
                                if($page == 1)
                                {
                                    $prevPage = 1;
                                } else 
                                {
                                    $prevPage = ($page - 1);
                                }

                                if($page == $total_pages)
                                {
                                    $nextPage = $total_pages;
                                } else 
                                {
                                    $nextPage = ($page + 1);
                                }

                                echo '<li><a href="contests.php?id='.$_SESSION["lib"].'&page='.$prevPage.'">&lt;</a></li>';
                                echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                echo '<li><a href="contests.php?id='.$_SESSION["lib"].'&page='.$nextPage.'">&gt;</a></li>';

                                ?>
                                <li>
                                  <a href="contests.php?id=<?php echo $_SESSION['lib']; ?>&page=<?php echo $total_pages ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Contest Name</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($result as $row) {
                                if($row['status'] == 1)
                                {
                                    $status = 'Not started';
                                } else if($row['status'] == 2)
                                {
                                    $status = 'Running';
                                } else if($row['status'] == 3)
                                {
                                    $status = 'Finished';
                                } else if($row['status'] == 4)
                                {
                                    $status = 'Prizes Announced';
                                }
                                ?>
                                <tr>
                                    <td>
                                        <span><?php echo $row['name']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo date('d/m/Y H:i:s', strtotime($row['startDateTime'])); ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo date('d/m/Y H:i:s', strtotime($row['endDateTime'])); ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $status; ?></span>
                                    </td>
                                    <td align="center" class="actions">
                                        <button style="margin-top: 5px" <?php if($row['status'] == 3) { echo 'disabled'; } ?> data-id="<?php echo $row['contestId']; ?>" class="btn btn-danger btn-stop"><i class="fa fa-check-square-o"></i> Stop</button>
                                        <a href="contest_detail.php?id=<?php echo $row['contestId']; ?>" class="btn btn-info"><i class="fa fa-info-circle"></i> View</a>
                                    </td>
                                </tr>
                                <?php } ?>


                            </tbody>
                        </table>
                    </div>
                    <?php } else { 
                        echo "<h2 style='margin-left: 20px'>No contest available right now!</h2>"; 
                    } ?>

                </div><!-- aj_sec -->
            </div><!-- postSection -->

        </div>

    </div>
</div>
</div>
</div>


<?php
include_once ('../includes/footer.php'); ?>
<script type="text/javascript" src="../js/general.js"></script>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        $(document).on('click', '.btn-stop', function(e){
            e.preventDefault();
            var arrFields = 'status';
            var arrValues = '3';
            
            var key = "contestId";
            var value = $(this).data('id');
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/processing.php',
                data: 'table=' + 'contest' + '&db=' + arrFields + '&data=' + arrValues + '&key=' + key + '&value=' + value + '&command=' + 'update',
                cache: false,
                success: function(data) {
                    if(data == 'correct')
                    {
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            hideLoader();
                            showNotification('success', 'Process done successfully!');
                        }); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });
})
</script>
