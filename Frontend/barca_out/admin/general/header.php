<?php $pageName = basename($_SERVER['PHP_SELF']); include_once('../includes/defaults.php');  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FCB Studio Panel</title>

    <!-- Bootstrap -->
    <link href="../plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="../plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link href="../plugins/upload/uploadfile.css" rel="stylesheet" type="text/css">
    
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<div class="alert alert-dismissible" role="alert">
  <button type="button" class="close" data-hide="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong></strong>
</div>

<div id="overlay"></div>
<div id="loader">
  <img src="../images/211.GIF" alt="Loading...">
</div>

<div class="container-fluid top-header">
  <div class="row">
    <div class="col-md-12 header">
      <h1 class="pull-left"><img src="../images/fcbstudio.png" alt="Logo" width="40"> FCB Studio Panel</h1>
      <?php if(isset($log_id)) { ?>
      <a style="margin-top: 20px" href="<?php echo $rootPath; ?>logout.php" class="btn btn-danger btn-md pull-right">Logout</a>
      <?php } ?>
    </div>
  </div>
</div>