<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$_SESSION['lib'] = 8;

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}
$startFrom = ($page-1) * $recPerPage;
$database->query('SELECT * FROM send_notifications ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM send_notifications ORDER BY id DESC LIMIT :start, :num');

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Notifications</h4>
                        <div class="head_opts">
                            <a href="send_notification.php" class="btn add_entry btn-primary">Send New Notification &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-left">
                                  <ul class="pagination">
                                    <li>
                                      <a href="notifications.php?page=1" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <?php
                                if($page == 1)
                                {
                                    $prevPage = 1;
                                } else 
                                {
                                    $prevPage = ($page - 1);
                                }

                                if($page == $total_pages)
                                {
                                    $nextPage = $total_pages;
                                } else 
                                {
                                    $nextPage = ($page + 1);
                                }
                                echo '<li><a href="notifications.php?page='.$prevPage.'">&lt;</a></li>';
                                echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                echo '<li><a href="notifications.php?page='.$nextPage.'">&gt;</a></li>';
                                ?>
                                <li>
                                  <a href="notifications.php?page=<?php echo $total_pages ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                </div>

                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Send Date</th>
                                <th>Send To</th>
                                <th>Send Via</th>
                                <th>Sent Message</th>
                                <th>Status</th>
                                <th>Success counter</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                if($row['sendTo'] != 'all' && $row['sendTo'] != 'Selected')
                                {
                                    if (strpos($row['sendTo'],',') !== false) {
                                        
                                        $userNames = array();
                                        $sendto = explode(',', $row['sendTo']);
                                        var_dump($sento);
                                        foreach ($sendto as $key) {
                                            $db->query('SELECT fullName FROM user WHERE userId = "'.$key.'"');
                                            $db->execute();
                                            $nameRow = $db->single();
                                            $userNames = $nameRow['fullName'];
                                        }
                                    } else {
                                        $key = $row['sendTo'];
                                        $db->query('SELECT fullName FROM user WHERE userId = "'.$key.'"');
                                        $db->execute();
                                        $nameRow = $db->single();
                                        $userNames = $nameRow['fullName'];
                                    }
                                } else {
                                    $userNames = $row['sendTo'];
                                }
                                ?>
                                <tr>
                                    <td>
                                        <span><?php echo date('d/m/Y', strtotime($row['sendDate'])); ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $userNames; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['sendVia']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['message']; ?></span>
                                    </td>
                                    <td>
                                    	<?php 
                                    		if($row['android_status']=='Completed' && $row['android_status']=='Completed')echo '<span style="color:green">Completed</span>'; 
											elseif($row['android_status']=='Pending' && $row['android_status']=='Pending')echo '<span>Pending</span>';
											else echo '<span style="color:range">In progress</span>';
                                    	?>
                                    </td>
                                    <td>
                                        <span><?php echo (intval($row['android_success'])+intval($row['ios_success'])+intval($row['email_success'])); ?></span>
                                    </td>
                                    <td style="text-align:center">
                                        <a href="javascript:;" class="del_entry" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } else { 
                        echo "<h2 style='margin-left: 20px'>List of Notifications will be displayed here.</h2>"; 
                    } ?>

                </div><!-- aj_sec -->
            </div><!-- postSection -->

        </div>

    </div>
</div>
</div>
</div>


<?php
include_once ('../includes/footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;        
        $(document).on('click', '.del_entry', function(){
            //if(!confirm('Are you sure, you want to delete data?')) return false;
            showLoader();
            db = "id";
            value = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'send_notifications' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec"); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });
    });
</script>