<?php include('../includes/functions.php');
$command = $_REQUEST['command'];
$db = new Database();
if(empty($command))
{
	$sql_details = array(
		'user' => DB_USER,
		'pass' => DB_PASS,
		'db'   => DB_NAME,
		'host' => DB_HOST
		);


	$table = 'city';
	$join = 'country';

	$primaryKey = 'cityId';

	$columns = array(
		
		array( 'db' => 'cityId', 'dt' => 0),
		array( 'db' => 'countryName', 'dt' => 1 ),
		array( 'db' => 'name_en',  'dt' => 2 ),
		array( 'db' => 'cityId', 'dt' => 3,
			'formatter' => function($d, $row)
			{
				return '
				<a href="javascript:;" class="btn btn-success btn-lang" data-id="'.$d.'"><i class="fa fa-language"></i> Translate</a>
				<a href="javascript:;" class="btn btn-info btn-edit" data-id="'.$d.'"><i class="fa fa-pencil"></i> Edit</a>
				<a href="javascript:;" class="btn btn-danger btn-delete" data-id="'.$d.'"><i class="fa fa-trash"></i> Delete</a>
				';
			}
			),
		);

	$joinWhere = 'city.countryId = country.countryId';
	require( '../includes/ssp.class.php' );

	echo json_encode(
		SSP::multiple( $_GET, $sql_details, $table, $primaryKey, $columns, $join, $joinWhere )
		);
} else if($command == 'edit')
{ 
	$id = $_REQUEST['id'];
	$db->query('SELECT countryId, name_en FROM city WHERE cityId = :id');
	$db->bind(":id", $id);
	$db->execute();
	$row = $db->single();
	?>
	
	<!-- Modal -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Edit City</h4>
	</div>
	<form class="form-horizontal myeditform">
		<div class="modal-body">

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Country</h4></label>
				<div class="col-sm-8">
					<select class="getValue form-control" data-key="countryId">
						<?php 
						$db->query('SELECT countryName, countryId FROM country');
						$db->execute();
						$result = $db->resultSet();
						foreach ($result as $countryRow) {
							if($countryRow['countryId'] == $row['countryId'])
							{
								echo '<option selected value="'.$countryRow['countryId'].'">'.$countryRow['countryName'].'</option> ';
							} else {
								echo '<option value="'.$countryRow['countryId'].'">'.$countryRow['countryName'].'</option> ';
							}
						}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Name</h4></label>
				<div class="col-sm-8">
					<input type="text" value="<?php echo $row['name_en'] ?>" class="getValue required form-control" name="f3" data-key="name_en">
				</div>
			</div>

		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="submit" data-id="<?php echo $id; ?>" class="btn btn-primary edit_city">Save</button>
		</div>
	</form>
	

	<?php } else if($command == 'translate')
	{

	$id = $_REQUEST['id'];
	$db->query('SELECT name_es, name_ar, name_ca, name_pt, name_fr, name_it, name_ms FROM city WHERE cityId = :id');
	$db->bind(":id", $id);
	$db->execute();
	$row = $db->single();
	?>
	
	<!-- Modal -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Edit Languages</h4>
	</div>
	<form class="form-horizontal myeditform">
		<div class="modal-body">

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> AR</h4></label>
				<div class="col-sm-8">
					<input type="text" value="<?php echo $row['name_ar'] ?>" class="getValue required form-control" name="f2" data-key="name_ar">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> ES</h4></label>
				<div class="col-sm-8">
					<input type="text" value="<?php echo $row['name_es'] ?>" class="getValue required form-control" name="f3" data-key="name_es">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> CA</h4></label>
				<div class="col-sm-8">
					<input type="text" value="<?php echo $row['name_ca'] ?>" class="getValue required form-control" name="f4" data-key="name_ca">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> PI</h4></label>
				<div class="col-sm-8">
					<input type="text" value="<?php echo $row['name_pt'] ?>" class="getValue required form-control" name="f4" data-key="name_pt">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> LID</h4></label>
				<div class="col-sm-8">
					<input type="text" value="<?php echo $row['name_fr'] ?>" class="getValue required form-control" name="f4" data-key="name_fr">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> IT</h4></label>
				<div class="col-sm-8">
					<input type="text" value="<?php echo $row['name_it'] ?>" class="getValue required form-control" name="f4" data-key="name_it">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> MS</h4></label>
				<div class="col-sm-8">
					<input type="text" value="<?php echo $row['name_ms'] ?>" class="getValue required form-control" name="f4" data-key="name_ms">
				</div>
			</div>

		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="submit" data-id="<?php echo $id; ?>" class="btn btn-primary edit_city">Save</button>
		</div>
	</form>
	
<?php } ?>

