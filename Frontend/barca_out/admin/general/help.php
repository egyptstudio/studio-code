<?php
include_once ('../includes/check.php');
include_once ('header.php'); 
$_SESSION['lib'] = 9;

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}

$startFrom = ($page-1) * $recPerPage;

$database->query('SELECT * FROM helpscreen ORDER BY screenId DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM helpscreen ORDER BY screenId DESC LIMIT :start, :num');
$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();
$result = $database->resultset();

if($total_records> $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}
if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

<?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            
            <div class="row">
                
                <div class="col-md-12">
                    <h4 class="heading">Help Section</h4>
                    <div class="head_opts">
                        <a href="add_help.php" class="btn unsetUser btn-primary">Add New Screen &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                    </div>
                </div>
                
            
                <div class="postSection">
                <div class="aj_sec">
                <?php if($total_records > 0) { ?>
                <div class="opts3">
                    <nav class="siteNav pull-right">
                      <ul class="pagination">
                        <li>
                          <a href="help.php?page=1" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                          </a>
                        </li>
                        <?php
                        if($page == 1)
                        {
                            $prevPage = 1;
                        } else 
                        {
                            $prevPage = ($page - 1);
                        }

                        if($page == $total_pages)
                        {
                            $nextPage = $total_pages;
                        } else 
                        {
                            $nextPage = ($page + 1);
                        }
                        
                            echo '<li><a href="help.php?page='.$prevPage.'">&lt;</a></li>';
                            echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                            echo '<li><a href="help.php?page='.$nextPage.'">&gt;</a></li>';
                        
                        ?>
                        <li>
                          <a href="help.php?page=<?php echo $total_pages ?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                          </a>
                        </li>
                      </ul>
                    </nav>
                </div>
                    
                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Screen Image</th>
                                <th>Icon</th>
                                <th>Title</th>
                                <th>Language</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($result as $row) {
                            ?>
                            <tr>
                                <td>
                                    <span><img width="100" src="<?php echo $row['screenImageUrl'] ?>"></span>
                                </td>
                                <td>
                                    <span><img width="100" src="<?php echo $row['screenIconUrl'] ?>"></span>
                                </td>
                                <td>
                                    <span><?php echo $row['screenTitle']; ?></span>
                                </td>
                                <td>
                                    <span><?php echo $row['languageCode']; ?></span>
                                </td>
                                <td>
                                    <span><?php echo $row['description']; ?></span>
                                </td>
                                <td align="center" class="actions">
                                    <a href="edit_help.php?id=<?php echo $row['screenId']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="#" class="btn btn-danger delHelp" data-id="<?php echo $row['screenId']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                            <?php } ?>

                            
                        </tbody>
                    </table>
                </div>
                <?php } else { 
                    echo "<h2 style='margin-left: 20px'>No Help Screen available right now!</h2>"; 
                } ?>
                
                </div><!-- aj_sec -->
                </div><!-- postSection -->

            </div>
                
            </div>
        </div>
    </div>
</div>


<?php
include_once ('footer.php'); ?>
<script type="text/javascript" src="../js/general.js"></script>
