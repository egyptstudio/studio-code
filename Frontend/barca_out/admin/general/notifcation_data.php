<?php 
include_once('../includes/functions.php');
$command = $_REQUEST['command'];
$db = new Database();

if($command == 'getUsers')
{
	$country = $_REQUEST['country'];
	?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Select Users</h4>
	</div>
	<div class="modal-body" style="max-height: 500px; overflow-y: auto">
		<form class="searchform">
			<div class="form-group">
				<div class="col-sm-8">
					<input placeholder="Type Username" required class="form-control user_text" type="search">
				</div>
				<button type="submit" id="searchUser" data-country="<?php echo $country; ?>" data-id="user" class="btn btn-primary"><i class="fa fa-search"></i></button>
			</div>
		</form>

		<div style="clear: both" class="users">
			
		</div>

	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-primary select_user">Select</button>
		<button type="button" class="btn btn-default cancel_user" data-dismiss="modal">Cancel</button>
	</div>
	<?php } else if($command == 'searchUser') { 
		$text = $_REQUEST['text'];
		
		$country = $_REQUEST['country'];
		if($country!=0)
		{
			$db->query('SELECT fullName, userId FROM user WHERE fullName LIKE :search AND countryId = :country AND (userType = :type OR userType = :type2) AND blocked = :block ORDER BY fullName ASC');
			$db->bind(':country', $country);
		}
		else
			$db->query('SELECT fullName, userId FROM user WHERE fullName LIKE :search AND (userType = :type OR userType = :type2) AND blocked = :block ORDER BY fullName ASC');
		$db->bind(':type', 1);
		$db->bind(':type2', 2);
		$db->bind(':block', 0);
		$db->bind(':search', "%" . $text . "%");
		//echo $db->debugDumpParams();
		$db->execute();
		$result = $db->resultSet();
		foreach ($result as $row) {
			echo '<div class="name"><label><input class="user_checkbox" data-id="'.$row['userId'].'" value="'.$row['fullName'].'" type="checkbox"> '.$row['fullName'].'</label></div>';
		}
		?>

		<?php } else if($command == 'getCountry') { ?>
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Select Country</h4>
		</div>
		<div class="modal-body" style="max-height: 500px; overflow-y: auto">
			<form class="searchform">
				<div class="form-group">
					<div class="col-sm-8">
						<input placeholder="Type Country Name" required class="form-control user_text" type="search">
					</div>
					<button type="submit" id="searchUser" data-id="country" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</div>
			</form>

			<div style="clear: both" class="users">

			</div>

		</div>

		<div class="modal-footer">
			<button type="button" class="btn btn-primary select_country">Select</button>
			<button type="button" class="btn btn-default cancel_country" data-dismiss="modal">Cancel</button>
		</div>
		<?php } else if($command == 'searchCountry') { 
			$text = $_REQUEST['text'];

			$db->query('SELECT * FROM country WHERE (countryName LIKE :search) OR 
				(name_ar LIKE :search) OR 
				(name_es LIKE :search) OR 
				(name_ca LIKE :search) OR 
				(name_pt LIKE :search) OR 
				(name_fr LIKE :search) OR 
				(name_lid LIKE :search) OR 
				(name_it LIKE :search) OR 
				(name_ms LIKE :search)
				ORDER BY countryId ASC');
			$db->bind(':search', "%" . $text . "%");

			$db->execute();
			$result = $db->resultSet();
			foreach ($result as $row) {
				echo '<div class="name"><label><input class="country_checkbox" data-id="'.$row['countryId'].'" value="'.$row['countryName'].'" type="radio" name="countries"> '.$row['countryName'].'</label></div>';
			}
			?>

			<?php } ?>