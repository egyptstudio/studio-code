<?php 
include_once('includes/database.class.php'); $command = $_REQUEST['command']; 
$database = new Database();
if($command == 'getPlayers')
{
  ?>
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Select Players</h4>
  </div>
  <div class="modal-body">
    <form class="form-inline" id="add_player_form">
      <div class="form-group" style="width: 80%">
        <input style="width: 100%" type="text" class="form-control getValue" data-key="name" name="player_name" required id="newplayer" placeholder="Write new player name">
        <input type="hidden" class="getValue" data-key="type" value="1">
      </div>
      <div class="form-group" style="width: 80%">
        <input style="width: 100%" type="number" class="form-control getValue" data-key="rank" name="player_rank" required id="rankplayer" placeholder="Rank">
      </div>

      <button type="submit" class="btn btn-default btn-black add_player">Add Player</button>
    </form>
    <div class="input-group search_input">
      <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
      <input id="searchPlayer" type="text" name="season" class="form-control" placeholder="Search" aria-describedby="basic-addon3">
    </div>

    <div class="players">
     <div class="players_data">
       <?php 
       $database->query('SELECT thesuarusId, name FROM thesuarus WHERE type = :type ORDER BY name ASC');
       $database->bind(':type', '1');
       $database->execute();
       if($database->rowCount() > 0)
       {
         $result = $database->resultSet();
         foreach ($result as $row) {
          echo '<div class="name"><label><input class="player_checkbox" data-id="'.$row['thesuarusId'].'" value="'.$row['name'].'" type="checkbox"> '.$row['name'].'</label></div>';
        }
      } else 
      {

      }
      ?>
    </div>
  </div>

</div>
<div class="modal-footer">
 <button type="button" class="btn btn-primary select_player">Select</button>
 <button type="button" class="btn btn-default cancel_player" data-dismiss="modal">Cancel</button>
</div>
<script type="text/javascript">

	$("#searchPlayer").searchText({
		dataKey : '.players_data div'
	});
	$(".add_player").umkPlugin({
		className: '#add_player_form .getValue',
		type: 'add',
		pageName: 'add_player',
		formName: '#add_player_form',
		table: 'thesuarus',
		successMsg : 'Player addedd successfully!',
		errorMsg: 'There is some issue, Please contact administration!'
	});
</script>
<?php } else if($command == 'getEvents') { ?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Select Event</h4>
</div>
<div class="modal-body">
  <form class="form-inline" id="add_event_form">
    <div class="form-group" style="width: 80%">
      <input style="width: 100%" type="text" class="form-control getValue" data-key="name" required id="newplayer" placeholder="Write new event name">
      <input type="hidden" class="getValue" data-key="type" value="2">
    </div>
    <button type="submit" class="btn btn-default btn-black add_event">Add Event</button>
  </form>
  <div class="input-group search_input">
    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
    <input id="searchPlayer" type="text" name="season" class="form-control" placeholder="Search" aria-describedby="basic-addon3">
  </div>

  <div class="players">
   <div class="event_data">
     <?php 
     $database->query('SELECT thesuarusId, name FROM thesuarus WHERE type = :type ORDER BY name ASC');
     $database->bind(':type', '2');
     $database->execute();
     if($database->rowCount() > 0)
     {
       $result = $database->resultSet();
       foreach ($result as $row) {
        echo '<div class="name"><label><input class="event_radio" data-id="'.$row['thesuarusId'].'" name="name" value="'.$row['name'].'" type="radio"> '.$row['name'].'</label></div>';
      }
    } else 
    {

    }
    ?>
  </div>
</div>

</div>
<div class="modal-footer">
 <button type="button" class="btn btn-primary select_event">Select</button>
 <button type="button" class="btn btn-default cancel_event" data-dismiss="modal">Cancel</button>
</div>
<script type="text/javascript">
	$("#searchPlayer").searchText({
		dataKey : '.event_data div'
	});
	$(".add_event").umkPlugin({
		className: '#add_event_form .getValue',
		type: 'add',
		pageName: 'add_event',
		formName: '#add_event_form',
		table: 'thesuarus',
		successMsg : 'Event addedd successfully!',
		errorMsg: 'There is some issue, Please contact administration!'
	});
</script>

<?php } else if($command == 'getSeason') { ?>


<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Select Season</h4>
</div>
<div class="modal-body">
  <form class="form-inline" id="add_season_form">
    <div class="form-group" style="width: 79%">
      <input style="width: 100%" type="text" class="form-control getValue" data-key="name" required id="newplayer" placeholder="Write new season name">
      <input type="hidden" class="getValue" data-key="type" value="3">
    </div>
    <button type="submit" class="btn btn-default btn-black add_season">Add Season</button>
  </form>
  <div class="input-group search_input">
    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
    <input id="searchPlayer" type="text" name="season" class="form-control" placeholder="Search" aria-describedby="basic-addon3">
  </div>

  <div class="players">
   <div class="season_data">
     <?php 
     $database->query('SELECT thesuarusId, name FROM thesuarus WHERE type = :type ORDER BY name ASC');
     $database->bind(':type', '3');
     $database->execute();
     if($database->rowCount() > 0)
     {
       $result = $database->resultSet();
       foreach ($result as $row) {
        echo '<div class="name"><label><input class="season_radio" data-id="'.$row['thesuarusId'].'" name="name" value="'.$row['name'].'" type="radio"> '.$row['name'].'</label></div>';
      }
    } else 
    {

    }
    ?>
  </div>
</div>

</div>
<div class="modal-footer">
 <button type="button" class="btn btn-primary select_season">Select</button>
 <button type="button" class="btn btn-default cancel_season" data-dismiss="modal">Cancel</button>
</div>
<script type="text/javascript">
	$("#searchPlayer").searchText({
		dataKey : '.season_data div'
	});
	$(".add_season").umkPlugin({
		className: '#add_season_form .getValue',
		type: 'add',
		pageName: 'add_season',
		formName: '#add_season_form',
		table: 'thesuarus',
		successMsg : 'Season addedd successfully!',
		errorMsg: 'There is some issue, Please contact administration!'
	});
</script>

<?php } else if($command == 'getTags')
{?>
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Select Tags</h4>
  </div>
  <div class="modal-body">
    <form class="form-inline" id="add_tag_form">
      <div class="form-group" style="width: 80%">
        <input style="width: 100%" type="text" class="form-control getValue" data-key="name" name="tag_name" required id="tags_data" placeholder="Write new Tag name">
        <input type="hidden" class="getValue" data-key="type" value="4">
      </div>
      <button type="button" class="btn btn-default btn-black add_tag">Add Tag</button>
    </form>
    <div class="input-group search_input">
      <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
      <input id="searchTag" type="text" name="season" class="form-control" placeholder="Search" aria-describedby="basic-addon3">
    </div>

    <div class="tags">
     <div class="tags_data">
       <?php 
       $database->query('SELECT thesuarusId, name FROM thesuarus WHERE type = :type ORDER BY name ASC');
       $database->bind(':type', '4');
       $database->execute();
       if($database->rowCount() > 0)
       {
         $result = $database->resultSet();
         foreach ($result as $row) {
          echo '<div class="name"><label><input class="tag_checkbox" data-id="'.$row['thesuarusId'].'" value="'.$row['name'].'" type="checkbox">'.$row['name'].'</label></div>';
        }
      } else 
      {

      }
      ?>
    </div>
  </div>

</div>
<div class="modal-footer">
 <button type="button" class="btn btn-primary select_tag">Select</button>
 <button type="button" class="btn btn-default cancel_tag" data-dismiss="modal">Cancel</button>
</div>
<script type="text/javascript">

  $("#searchTag").searchText({
    dataKey : '.tags_data div'
  });
  
  $(".add_tag").click(function(){
    if (!$("#add_tag_form").validate().form()) {
      return false;
    }
    var arrFields = $("#add_tag_form .getValue").map(function() {
      var $elm = $(this),
      childId = $elm.data('key');
      if (childId.length == 0) {
        alert("Error");
        return false;
      }
      return childId;
    }).get();
    var arrValues = $("#add_tag_form .getValue").map(function() {
      if ($(this).val().length == 0) {
        $(this).val('');
      }
      return this.value.escapeHTML();
    }).get();
    showLoader();

    $.ajax({
      type: 'POST',
      url: 'includes/processing.php',
      data: 'table=' + 'thesuarus' + '&db=' + arrFields + '&data=' + arrValues + '&command=' + 'addTag',
      cache: false,
      success: function(data) {
        hideLoader();
        var arr = data.split('~');
        if(arr[0] == "correct")
        {
          $("#tags_data").val('');
          $(".tags_data").prepend('<div class="name"><label><input data-id="'+arr[1]+'" value="'+arrValues[0]+'" class="tag_checkbox" type="checkbox"> ' + arrValues[0] + '</label></div>');
          showNotification('success', 'Process done successfully!');
        } else 
        {
          showNotification('warning', 'Tag already added!');
        }
      }
    });
  });

  /*$(".add_tag").umkPlugin({
    className: '#add_tag_form .getValue',
    type: 'add',
    pageName: 'add_tag',
    formName: '#add_tag_form',
    table: 'thesuarus',
    successMsg : 'Tag addedd successfully!',
    errorMsg: 'There is some issue, Please contact administration!'
  });
*/
</script>
<?php } else if($command == 'changeThumb') { ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Change Thumbnail</h4>
</div>
<div class="modal-body">
  <div class="form-group" style="display: inline-block; width: 100%">
    <label for="l3" class="col-sm-4 control-label"><h4 class="tb_title">Upload Image</h4></label>
    <div class="col-sm-8">
      <div class="thes">
        <div id="thesfile">Upload</div>
        <div class="uploaderStatus" id="thes_file"></div>
      </div>
      <input type="hidden" class="thes_file" value="">
    </div>
  </div>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-primary save_thumb">Save</button>
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
</div>

<script type="text/javascript">
  $(function(){
    var settings = {
      url: "includes/processing.php",
      dragDrop: false,
      dragDropStr: "<span><b>Drag & Drop File</b></span>",
      multiple: false,
      fileName: "myfile",
      formData: {"command":"uploadData", "dir" : "third_party/uploads/", "thumb" : "No"},
      allowedTypes: "jpg,png,jpeg",
      returnType: "json",
      showFileCounter: false,
      showDone: false,
      maxFileCount:1,
      onSuccess: function (files, data, xhr) {
       $(".thes_file").val(data);
     },
     onSubmit:function(files)
     {
                //alert(files);
                setTimeout(function(){
                  $("#loader, #overlay").hide();
                }, 100);
              },
              showDelete: true,
              deleteCallback: function (data, pd) {
                for (var i = 0; i < data.length; i++) {
                  $.post("includes/processing.php", {
                    op: "delete",
                    dir: "third_party/uploads/",
                    command: "deleteData",
                    name: data[i]
                  },
                  function (resp, textStatus, jqXHR) {
                            //alert(resp);
                            $("#thes_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                          });
                }
                pd.statusbar.hide(); //You choice to hide/not.

              }
            }

            var uploadObj = $("#thesfile").uploadFile(settings);
          });
</script>
<?php } else if($command == "getUsers") { ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Select Users</h4>
</div>
<div class="modal-body" style="max-height: 500px; overflow-y: auto">

  <div class="users">
    <div class="users_data">
      <?php 
      $database->query('SELECT fullName, userId FROM user WHERE (userType = :type OR userType = :type2) AND blocked = :block ORDER BY fullName ASC');
      $database->bind(':type', 1);
      $database->bind(':type2', 2);
      $database->bind(':block', 0);
      $database->execute();
      $result = $database->resultSet();
      foreach ($result as $row) {
        echo '<div class="name"><label><input class="user_checkbox" data-id="'.$row['userId'].'" value="'.$row['fullName'].'" type="checkbox"> '.$row['fullName'].'</label></div>';
      }
      ?>
    </div>
  </div>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-primary select_user">Select</button>
  <button type="button" class="btn btn-default cancel_user" data-dismiss="modal">Cancel</button>
</div>
<?php } else if($command == 'getVersion') { 
  $id = $_REQUEST['id'];
  $database->query('SELECT * FROM fcbversion WHERE id = :id');
  $database->bind(":id", $id);
  $database->execute();
  $row = $database->single();

  ?>
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Add Version</h4>
  </div>
  <div class="modal-body">

    <form class="form-horizontal myeditform">
      <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version No.</h4></label>
        <div class="col-sm-8">
          <input type="text" value="<?php echo $row['versionNumber']; ?>" name="1" class="getValue form-control required" data-key="versionNumber">
        </div>
      </div>

      <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version Description</h4></label>
        <div class="col-sm-8">
          <textarea class="getValue required form-control" name="2" data-key="description"><?php echo $row['description']; ?></textarea>
        </div>
      </div>

      <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version type</h4></label>
        <div class="col-sm-8">
          <select class="getValue form-control required" name="3" data-key="deviceType">
            <option <?php if($row['deviceType'] == 1) { echo 'selected'; } ?> value="1">Android</option>
            <option <?php if($row['deviceType'] == 2) { echo 'selected'; } ?> value="2">iOS</option>
          </select>  
        </div>
      </div>

      <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version server</h4></label>
        <div class="col-sm-8">
          <select class="getValue form-control required" name="4" data-key="serverId">
            <?php 
            $database->query('SELECT serverId, name FROM fcbservers');
            $database->execute();
            $serverRow = $database->resultset();
            foreach ($serverRow as $row2) {
              if($row2['serverId'] == $row['serverId'])
              {
                echo '<option selected value="'.$row2['serverId'].'">'.$row2['name'].'</option>';
              } else 
              {
                echo '<option value="'.$row2['serverId'].'">'.$row2['name'].'</option>';
              }
            }
            ?>
          </select>  
        </div>
      </div>

      <div class="form-group">
        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Version status</h4></label>
        <div class="col-sm-8">
          <select class="getValue form-control required" name="5" data-key="updateStatus">
            <option <?php if($row['updateStatus'] == 1) { echo 'selected'; } ?> value="1">No Update</option>
            <option <?php if($row['updateStatus'] == 2) { echo 'selected'; } ?> value="2">Optional Update</option>
            <option <?php if($row['updateStatus'] == 3) { echo 'selected'; } ?> value="3">Force Update</option>
          </select>  
        </div>
      </div>

    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" data-id="<?php echo $id; ?>" class="btn btn-primary edit_version">Save</button>
  </div>
  <?php } else if($command == 'getServer') { 
    $id = $_REQUEST['id'];
    $database->query('SELECT * FROM fcbservers WHERE serverId = :id');
    $database->bind(":id", $id);
    $database->execute();
    $row = $database->single();

    ?>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Add Server</h4>
    </div>
    <div class="modal-body">

      <form class="form-horizontal myeditform">
        <div class="form-group">
          <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Name</h4></label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo $row['name']; ?>" name="1" class="getValue form-control required" data-key="name">
          </div>
        </div>

        <div class="form-group">
          <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Base Url</h4></label>
          <div class="col-sm-8">
            <input class="getValue required form-control" name="2" data-key="baseURL" value="<?php echo $row['baseURL']; ?>">
          </div>
        </div>

        <div class="form-group">
          <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Description</h4></label>
          <div class="col-sm-8">
            <textarea class="getValue required form-control" name="3" data-key="description"><?php echo $row['description']; ?></textarea>
          </div>
        </div>

      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="button" data-id="<?php echo $id; ?>" class="btn btn-primary edit_version">Save</button>
    </div>
    <?php } else if($command == 'getFeedback') { 
      $id = $_REQUEST['id'];
      $database->query('SELECT * FROM userfeedback WHERE id = :id');
      $database->bind(":id", $id);
      $database->execute();
      $row = $database->single();

      $database->query('SELECT * FROM userfeedback WHERE id > :id LIMIT 1');
      $database->bind(":id", $id);
      $database->execute();
      $nextCount = $database->rowCount();
      $nextRow = $database->single();

      $database->query('SELECT * FROM userfeedback WHERE id < :id ORDER BY id DESC LIMIT 1');
      $database->bind(":id", $id);
      $database->execute();
      $prevCount = $database->rowCount();
      $prevRow = $database->single();
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">View Feedback</h4>
      </div>
      <div class="modal-body">

        <div class="arrow-controls">
          <?php if($nextCount != 0) { ?>
          <a class="arrow-left btn btn-primary" data-id="<?php echo $nextRow['id']; ?>" href="javascript:;"><i class="fa fa-arrow-left"></i></a>
          <?php } ?>
          <?php if($prevCount != 0) { ?>
          <a class="arrow-right btn btn-primary" data-id="<?php echo $prevRow['id']; ?>" href="javascript:;"><i class="fa fa-arrow-right"></i></a>
          <?php } ?>
        </div>

        <form class="form-horizontal myeditform">
          <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Email</h4></label>
            <div class="col-sm-8">
              <input type="text" value="<?php echo $row['username']; ?>" disabled class="getValue form-control">
            </div>
          </div>

          <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Subject</h4></label>
            <div class="col-sm-8">
              <input class="getValue required form-control" disabled value="<?php echo $row['title']; ?>">
            </div>
          </div>

          <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Platform</h4></label>
            <div class="col-sm-8">
              <input class="getValue required form-control" disabled value="<?php echo $row['platform']; ?>">
            </div>
          </div>

          <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Handset Model</h4></label>
            <div class="col-sm-8">
              <input class="getValue required form-control" disabled value="<?php echo $row['os']; ?>">
            </div>
          </div>

          <div class="form-group">
            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Message</h4></label>
            <div class="col-sm-8">
              <textarea disabled="" class="getValue required form-control"><?php echo $row['message']; ?></textarea>
            </div>
          </div>
          <?php
          if($row['screenshot'] != "") { 
            $imgArray = explode('&#44;', $row['screenshot']);
            for($i=0;$i<count($imgArray);$i++)
            {
              ?>
              <div class="form-group">
                <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Screenshot</h4></label>
                <div class="col-sm-8">
                  <img src="https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/feedback/<?php echo $imgArray[$i] ?>" class="img-responsive">
                </div>
              </div>
              <?php } } ?>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

          <?php } else if($command == 'getRegistration') { 
            $id = $_REQUEST['id'];
            $database->query('SELECT * FROM web_registration WHERE id = :id');
            $database->bind(":id", $id);
            $database->execute();
            $row = $database->single();
            ?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">View Registration</h4>
            </div>
            <div class="modal-body">

              <form class="form-horizontal myeditform">
                <div class="form-group">
                  <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Name</h4></label>
                  <div class="col-sm-8">
                    <?php echo $row['name']; ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Email</h4></label>
                  <div class="col-sm-8">
                    <a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a>
                  </div>
                </div>

                <div class="form-group">
                  <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Mobile</h4></label>
                  <div class="col-sm-8">
                    <?php echo $row['mobile']; ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Age Range</h4></label>
                  <div class="col-sm-8">
                    <?php echo $row['age_range']; ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Gender</h4></label>
                  <div class="col-sm-8">
                    <?php echo $row['gender']; ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Country</h4></label>
                  <div class="col-sm-8">
                    <?php $database->query('SELECT countryId, countryName FROM country WHERE countryId = :countryId');
                    $database->bind(':countryId', $row['country_id']); 
                    $database->execute();
                    $countryRow = $database->single(); 
                    echo $countryRow['countryName']; ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Platform</h4></label>
                  <div class="col-sm-8">
                    <?php $database->query('SELECT * FROM platform WHERE id = :id');
                    $database->bind(':id', $row['platform']); 
                    $database->execute();
                    $platformRow = $database->single(); 
                    echo $platformRow['platform']; ?>
                  </div>
                </div>

              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

            <?php } else if($command == 'getContact') { 
              $id = $_REQUEST['id'];
              $database->query('SELECT * FROM web_contacts WHERE id = :id');
              $database->bind(":id", $id);
              $database->execute();
              $row = $database->single();

              ?>
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">View Contact</h4>
              </div>
              <div class="modal-body">

                <form class="form-horizontal myeditform">
                  <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Name</h4></label>
                    <div class="col-sm-8">
                      <?php echo $row['name']; ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Email</h4></label>
                    <div class="col-sm-8">
                      <a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Mobile</h4></label>
                    <div class="col-sm-8">
                      <?php echo $row['mobile']; ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Date Time</h4></label>
                    <div class="col-sm-8">
                      <?php echo date("d-m-Y H:i:s", strtotime($row['datetime'])); ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Message</h4></label>
                    <div class="col-sm-8">
                      <?php echo $row['message']; ?>
                    </div>
                  </div>

                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>


              <?php } else if($command == 'getShopItem') { 
                $id = $_REQUEST['id'];
                $database->query('SELECT * FROM inappproducts WHERE id = :id');
                $database->bind(":id", $id);
                $database->execute();
                $row = $database->single();

                $lang = $row['language'];
                if(!is_numeric($lang))
                {
                  $database->query('SELECT * FROM languages WHERE name = :lang');
                  $database->bind(":lang", $lang);
                  $database->execute();
                  $langRow = $database->single();
                  $langId = $langRow['id'];
                } else {
                  $langId = $lang;
                }
                
                ?>
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Edit Data</h4>
                </div>
                <div class="modal-body">
                  <form class="form-horizontal myeditform">
                    <div class="form-group">
                      <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Device Type</h4></label>
                      <div class="col-sm-8">
                        <select class="getValue form-control" data-key="deviceType">
                          <option <?php if($row['deviceType'] == 'iOS') {echo 'selected'; } ?> value="iOS">iOS</option>
                          <option <?php if($row['deviceType'] == 'Android') {echo 'selected'; } ?> value="Android">Android</option>
                        </select>    
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">InApp ID</h4></label>
                      <div class="col-sm-8">
                        <input type="text" value="<?php echo $row['inAppID'] ?>" class="inappid getValue form-control" data-key="inAppID">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Type</h4></label>
                      <div class="col-sm-8">
                        <select class="getValue form-control" data-key="inAppType">
                          <option <?php if($row['inAppType'] == 'Poster') {echo 'selected'; } ?> value="Poster">Poster</option>
                          <option <?php if($row['inAppType'] == 'Premium Subscription') {echo 'selected'; } ?> value="Premium Subscription">Premium Subscription</option>
                          <option <?php if($row['inAppType'] == 'Points') {echo 'selected'; } ?> value="Points">Points</option>
                        </select>  
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Price</h4></label>
                      <div class="col-sm-8">
                        <input type="number" value="<?php echo $row['price']; ?>" class="getValue form-control" data-key="price">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Language</h4></label>
                      <div class="col-sm-8">
                        <select class="getValue form-control" data-key="language">
                          <?php 
                          $database->query("SELECT * FROM languages");
                          $database->execute();
                          $result = $database->resultset();
                          foreach ($result as $langRow) {
                            if($langRow['id'] == $langId)
                            {
                              echo '<option selected value="'.$langRow['id'].'">'.$langRow['name'].'</option>';
                            } else {
                              echo '<option value="'.$langRow['id'].'">'.$langRow['name'].'</option>';  
                            }
                            
                          }
                          ?>
                        </select>    
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Description</h4></label>
                      <div class="col-sm-8">
                        <textarea class="getValue form-control" data-key="description"><?php echo $row['description']; ?></textarea>
                      </div>
                    </div>

                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" data-id="<?php echo $id; ?>" class="btn btn-primary edit_entry">Save</button>
                </div>
                <?php } else if($command == 'getGallery') { 
                  $id = $_REQUEST['id'];
                  $database->query('SELECT * FROM web_gallery WHERE id = :id');
                  $database->bind(":id", $id);
                  $database->execute();
                  $row = $database->single();
                  ?>
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Gallery</h4>
                  </div>
                  <div class="modal-body">

                    <form class="form-horizontal editform">

                      <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Title</h4></label>
                        <div class="col-sm-8">
                          <input type="text" value="<?php echo $row['title']; ?>" class="getValue form-control" data-key="title">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Alt</h4></label>
                        <div class="col-sm-8">
                          <textarea class="getValue form-control" data-key="media_alt"><?php echo $row['media_alt']; ?></textarea>
                        </div>
                      </div>

                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" data-id="<?php echo $id; ?>" class="btn btn-primary edit_entry">Save</button>
                  </div>
                  <?php } else if($command == 'getTawasolGallery') { 
                    $id = $_REQUEST['id'];
                    $database->query('SELECT * FROM tawasol_gallery WHERE id = :id');
                    $database->bind(":id", $id);
                    $database->execute();
                    $row = $database->single();
                    ?>
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Edit Gallery</h4>
                    </div>
                    <div class="modal-body">

                      <form class="form-horizontal editform">

                        <div class="form-group">
                          <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Title</h4></label>
                          <div class="col-sm-8">
                            <input type="text" value="<?php echo $row['title']; ?>" class="getValue form-control" data-key="title">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Alt</h4></label>
                          <div class="col-sm-8">
                            <textarea class="getValue form-control" data-key="media_alt"><?php echo $row['media_alt']; ?></textarea>
                          </div>
                        </div>

                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" data-id="<?php echo $id; ?>" class="btn btn-primary edit_entry">Save</button>
                    </div>
                    <?php } else if($command == 'getStoreLink') { 
                      $id = $_REQUEST['id'];
                      $database->query('SELECT * FROM storelink_urls WHERE id = :id');
                      $database->bind(":id", $id);
                      $database->execute();
                      $row = $database->single();
                      ?>
                      <!-- Modal -->
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Link</h4>
                      </div>
                      <div class="modal-body">

                        <form class="form-horizontal myeditform">
                          <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Language</h4></label>
                            <div class="col-sm-8">
                              <select class="getValue form-control required" data-key="language">
                                <option <?php if($row['language'] == 'en') {echo 'selected'; } ?> value="en">EN</option>
                                <option <?php if($row['language'] == 'es') {echo 'selected'; } ?> value="es">ES</option>
                                <option <?php if($row['language'] == 'ca') {echo 'selected'; } ?> value="ca">CA</option>
								<option <?php if($row['language'] == 'ar') {echo 'selected'; } ?> value="ar">AR</option>
								<option <?php if($row['language'] == 'fr') {echo 'selected'; } ?> value="fr">FR</option>
								<option <?php if($row['language'] == 'pt') {echo 'selected'; } ?> value="pt">PT</option>
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> URL</h4></label>
                            <div class="col-sm-8">
                              <input type="url" class="getValue required form-control" name="2" data-key="url" value="<?php echo $row['url'] ?>">
                            </div>
                          </div>

                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" data-id="<?php echo $id; ?>" class="btn btn-primary edit_url">Save</button>
                      </div>
                      <?php } else if($command == 'getGeneralLink') { 
                        $id = $_REQUEST['id'];
                        $database->query('SELECT * FROM general_links WHERE id = :id');
                        $database->bind(":id", $id);
                        $database->execute();
                        $row = $database->single();
                        ?>
                        <form class="form-horizontal myeditform">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Link</h4>
                          </div>
                          <div class="modal-body">
                           <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Language</h4></label>
                            <div class="col-sm-8">
                              <select class="getValue form-control required" data-key="lang">
                                <option <?php if($row['lang'] == 'en') {echo 'selected'; } ?> value="en">EN</option>
                                <option <?php if($row['lang'] == 'es') {echo 'selected'; } ?> value="es">ES</option>
                                <option <?php if($row['lang'] == 'ca') {echo 'selected'; } ?> value="ca">CA</option>
                                <option <?php if($row['lang'] == 'ar') {echo 'selected'; } ?> value="ar">AR</option>
								<option <?php if($row['lang'] == 'fr') {echo 'selected'; } ?> value="fr">FR</option>
								<option <?php if($row['lang'] == 'pt') {echo 'selected'; } ?> value="pt">PT</option>
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Name</h4></label>
                            <div class="col-sm-8">
                              <input type="text" class="getValue required form-control" name="f2" data-key="name" value="<?php echo $row['name'] ?>">
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> URL</h4></label>
                            <div class="col-sm-8">
                              <input type="url" class="getValue required form-control" name="f3" data-key="link" value="<?php echo $row['link'] ?>">
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" data-id="<?php echo $id; ?>" class="btn btn-primary edit_url">Save</button>
                        </div>
                      </form>

                      <?php } ?>