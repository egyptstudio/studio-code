<?php
include_once ('../includes/check.php');
include_once ('header.php');
if(isset($_GET['id']) && is_numeric($_GET['id']))
{
    $id = $_GET['id'];
    $class = "edit_about";
    $database->query("SELECT * FROM web_about WHERE id = :id");
    $database->bind(":id", $id);
    $database->execute();
    $aboutRow = $database->single();
} else 
{
    $id = "";
    $class="save_about";
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">About Content</h4>
                        <div class="head_opts">
                            <a href="#" class="btn btn-default btn-black <?php echo $class; ?>">Save</a>
                            <a href="about.php" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>

                    <form class="form-horizontal myform">

                        <div class="form-group">
                            <label for="l2" class="col-sm-2 col-sm-offset-1 control-label"><h4 class="tb_title">Language</h4></label>
                            <div class="col-sm-8">
                                <select class="form-control getValue" data-key="language_id">
                                    <option>Select</option>
                                    <?php $database->query("SELECT * FROM languages");
                                    $database->execute();
                                    $result = $database->resultset(); 
                                    foreach ($result as $row) {
                                        if($row['id'] == $aboutRow['language_id'])
                                        {
                                            echo '<option selected value="'.$row['id'].'">'.$row['name'].'</option>';
                                        } else 
                                        {
                                            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';    
                                        }
                                        
                                    }?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="l2" class="col-sm-2 col-sm-offset-1 control-label"><h4 class="tb_title">Content</h4></label>
                            <div class="col-sm-8">
                                <textarea class="editor1 form-control" name="editor1">
                                    <?php echo $aboutRow['about_html']; ?>
                                </textarea>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>

<?php
include_once ('footer.php'); ?>
<script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;

        var roxyFileman = '../plugins/ckeditor/fileman/index.html'; 
        CKEDITOR.replace( 'editor1',{filebrowserBrowseUrl:roxyFileman,
            filebrowserImageBrowseUrl:roxyFileman+'?type=image',
            removeDialogTabs: 'link:upload;image:upload'}); 
        
        $(document).on('click', '.save_about', function(){
            showLoader();
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            
            var textareaData = CKEDITOR.instances.editor1.getData();
            arrFields.splice(1, 0, 'about_html');
            arrValues.splice(1, 0, textareaData.escapeHTML());

            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'web_about' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&pageName=' + 'about' + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        window.location.href = "about.php";
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  

$(document).on('click', '.edit_about', function(){
            showLoader();
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            
            var textareaData = CKEDITOR.instances.editor1.getData();
            arrFields.splice(1, 0, 'about_html');
            arrValues.splice(1, 0, textareaData.escapeHTML());

            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'web_about' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + 'id' + '&value=' + <?php echo $id; ?> + '&pageName=' + 'about' + '&command=' + 'update',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        window.location.href = "about.php";
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });  

});
</script>