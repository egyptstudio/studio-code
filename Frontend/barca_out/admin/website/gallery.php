<?php
include_once ('../includes/check.php');
include_once ('header.php'); 

$_SESSION['lib'] = 21;

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}
$startFrom = ($page-1) * $recPerPage;
$database->query('SELECT * FROM web_gallery ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM web_gallery ORDER BY id DESC LIMIT :start, :num');

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Gallery</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_entry btn-primary">Add New File &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-left">
                                  <ul class="pagination">
                                    <li>
                                      <a href="gallery.php?page=1" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <?php
                                if($page == 1)
                                {
                                    $prevPage = 1;
                                } else 
                                {
                                    $prevPage = ($page - 1);
                                }

                                if($page == $total_pages)
                                {
                                    $nextPage = $total_pages;
                                } else 
                                {
                                    $nextPage = ($page + 1);
                                }
                                echo '<li><a href="gallery.php?page='.$prevPage.'">&lt;</a></li>';
                                echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                echo '<li><a href="gallery.php?page='.$nextPage.'">&gt;</a></li>';
                                ?>
                                <li>
                                  <a href="gallery.php?page=<?php echo $total_pages ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                </div>

                <div class="col-md-12">

                    <table class="table table-bordered userTable">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Url</th>
                                <th>Alt</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $result = $database->resultset();
                            foreach ($result as $row) {
                                ?>
                                <tr>
                                    <td>
                                        <span><?php echo $row['title']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['media_type']; ?></span>
                                    </td>
                                    <td>
                                        <span><?php if($row['media_type'] == 'image') { echo '<img src="'.$row['media_url'].'" width="100" />'; } else { echo '<a href="'.$row['media_url'].'">'.$row['media_url'].'</a>'; }  ?></span>
                                    </td>
                                    <td>
                                        <span><?php echo $row['media_alt']; ?></span>
                                    </td>
                                    <td style="text-align:center; vertical-align: middle">
                                        <a href="javascript:;" class="btn-edit btn btn-info" data-id="<?php echo $row['id']; ?>"><i class="fa fa-info"></i> Edit</a>
                                        <a href="javascript:;" class="del_entry btn btn-danger" data-type="<?php echo $row['media_type']; ?>" data-img="<?php if($row['media_type'] == 'image') { echo $row['media_url']; } ?>" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                    </td>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } else { 
                        echo "<h2 style='margin-left: 20px'>List of images/videos will be displayed here.</h2>"; 
                    } ?>

                </div><!-- aj_sec -->
            </div><!-- postSection -->

        </div>

    </div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Add Gallery</h4>
        </div>
        <div class="modal-body">

            <form class="form-horizontal myform">

                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Title</h4></label>
                    <div class="col-sm-8">
                        <input type="text" value="" class="getValue form-control" data-key="title">
                    </div>
                </div>

                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Media type</h4></label>
                    <div class="col-sm-8">
                        <select class="getValue gallery-control form-control" data-key="media_type">
                            <option>Select</option>
                            <option selected="" value="image">Image</option>
                            <option value="video">Video</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Alt</h4></label>
                    <div class="col-sm-8">
                        <textarea class="getValue form-control" data-key="media_alt"></textarea>
                    </div>
                </div>

                <div class="controller">
                    <div class="form-group uploader">
                        <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Url</h4></label>
                        <div class="col-sm-8 img-control">
                            <div class="original">
                                <div id="original">Upload</div>
                                <div class="uploaderStatus" id="ori_file"></div>
                            </div>
                            <input type="hidden" class="ori_file">
                        </div>
                        <div class="col-sm-8 vid-control">
                            <input type="url" class="getValue form-control" data-key="media_url">
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary save_entry">Save</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data"></div>
    </div>
  </div>
</div>

<?php
include_once ('footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(".add_entry").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.save_entry', function(){
            showLoader();
            var arrFields = $(".myform .getValue:visible").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue:visible").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            
            if($(".ori_file").val() != "")
            {
                arrFields.splice(1, 0, 'media_url');
                arrValues.splice(1, 0, $(".ori_file").val());
            }

            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'web_gallery' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        $("#myModal").modal('hide');
                        $(".controller").load(""+loc+qs+" .uploader", function(){
                            $("#original").uploadFile(settings1);
                        });
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec"); 
                        $(".myform")[0].reset();
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
});  
$(document).on('click', '.del_entry', function(){
    if(!confirm('Are you sure, you want to delete data?')) return false;
    db = "id";
    value = $(this).data('id');
    type = $(this).data('type');
    if(type == 'image')
    {
        img = $(this).data('img');
    } else {
        img = "";
    }
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'web_gallery' + '&db=' + db + '&data=' + value + '&file=' + img + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            hideLoader();
            if(data == 'correct')
            {
                showNotification('success', 'Process done successfully!');
                $(".postSection").load(""+loc+qs+" .aj_sec"); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("../aj_data.php?command=getGallery&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                });
            }, 700);
        });

$(document).on('click', '.edit_entry', function(e){
            e.preventDefault();
            if (!$(".editform").validate().form()) {
                return false;
            }
            var arrFields = $(".editform .getValue").map(function() {
                var $elm = $(this),
                    childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".editform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();
            
            var key = "id";
            var value = $(this).data('id');
            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/processing.php',
                data: 'table=' + 'web_gallery' + '&db=' + arrFields + '&data=' + arrValues + '&key=' + key + '&value=' + value + '&command=' + 'update',
                cache: false,
                success: function(data) {
                    hideLoader();
                    if(data == 'correct')
                    {
                        showNotification('success', 'Process done successfully!');
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            $("#myEditModal").modal('hide');
                        }); 
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });


$(".gallery-control").change(function(){
    if($(this).val() == 'image')
    {
        $(".vid-control").hide();
        $(".img-control").fadeIn();
    } else
    {
        $(".img-control").hide();
        $(".vid-control").fadeIn();
    }
});

var settings1 = {
    url: "../includes/shop_processing.php",
    dragDrop: false,
    dragDropStr: "<span><b>Drag & Drop File</b></span>",
    multiple: false,
    fileName: "myfile",
    formData: {"command":"uploadData", "dir" : "third_party/uploads/gallery/", "thumb" : "No"},
    allowedTypes: "jpg,png,jpeg",
    returnType: "json",
    showFileCounter: false,
    showDone: false,
    maxFileCount:1,
    onSuccess: function (files, data, xhr) {
         //alert(data);
         
         $(".ori_file").val('https://barca.s3.amazonaws.com/third_party/uploads/gallery/'+data);
     },
     onSubmit:function(files)
     {
        setTimeout(function(){
            $("#loader, #overlay").hide();
        }, 100);
    },
    showDelete: true,
    deleteCallback: function (data, pd) {
        for (var i = 0; i < data.length; i++) {
            $.post("../includes/shop_processing.php", {
                op: "delete",
                dir: "third_party/uploads/gallery/",
                command: "deleteData",
                name: data[i]
            },
            function (resp, textStatus, jqXHR) {
                $("#ori_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                $(".ori_file").val('');
            });
        }
        pd.statusbar.hide(); //You choice to hide/not.

    }
}

var uploadObj = $("#original").uploadFile(settings1);

});
</script>