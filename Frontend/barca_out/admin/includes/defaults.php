<?php 
$rootPath = "http://barca.tawasoldev.com/barca_out/admin/";

function findKey($array, $keySearch) {
    foreach ($array as $key => $item){
        if ($key == $keySearch){
            echo $item;
            return true;
        }
        else 
        {
            if (isset($array[$key]))
                findKey($array[$key], $keySearch);
        }
    }
    return false;
}

$menuArray['General Settings'] = array(
    '6' => 'Points Management',
    '7' => 'Contest Management',
    '8' => 'Notifications',
    '10' => 'Moderation',
    '11' => 'Shop Items',
    '24' => 'Store Links',
    '29' => 'General Links',
    '9' => 'Help Screens',
    '14' => 'View Feedback',
    '31' => 'Countries',
    '32' => 'Cities',
    );
$menuArray['Website'] = array(
    '15' => 'Website Subscriptions',
    '16' => 'Configurations',
    '17' => 'About Content',
    '18' => 'Registrations',
    '19' => 'Sponsors',
    '20' => 'Contact Us',
    '21' => 'Gallery',
    '22' => 'Platform',
    '23' => 'Operating System',
    );
$menuArray['FAQs'] = array(
    '25' => 'Categories',
    '26' => 'Questions',
    '27' => 'Answers',
    );
$menuArray['FCB Users'] = array(
    '3' => 'FCB_DUMMY',
    '4' => 'FCB_APP',
    '5' => 'FCB_STD'
    );

$menuArray['Version & Server'] = array(
    '12' => 'Version Control',
    '13' => 'Server Control',
    );
$menuArray['libraries'] = array('1' => 'Players', '2' => 'Teams');
$menuArray['Wall Management'] = array('28' => 'Latest Posts');

?>