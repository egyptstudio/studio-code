<?php 
include_once("functions.php");
include_once("defaults.php");

$log_id = isset($_SESSION["log_id"]) ? $_SESSION["log_id"] : "";
$name = isset($_SESSION["log_name"]) ? $_SESSION["log_name"] : "";
$category = isset($_SESSION["category"]) ? $_SESSION["category"] : "";

$database = new Database();
if($category == 'user')
{
	$database->query('SELECT email, userId FROM user WHERE userId = :id AND email = :username');
	$database->bind(':id', $log_id);
	$database->bind(':username', $name);
} else 
{
	$database->query('SELECT * FROM admin WHERE id = :id AND username = :username');
	$database->bind(':id', $log_id);
	$database->bind(':username', $name);
}
$row = $database->single();
//echo $row['username'];

if($category == 'user')
{
	$chkName = $row['email'];
} else 
{
	$chkName = $row['username'];
}

if(!isset($log_id) || $name !== $chkName)
{
	if($category == 'user')
	{
		echo "<script language='javascript'>document.location.href='".$rootPath."login-user.php';</script>";
	} else 
	{
		echo "<script language='javascript'>document.location.href='".$rootPath."login.php';</script>";
	}
}
?>