<?php
include_once('functions.php');
include_once('class.phpmailer.php');
include_once('class.smtp.php');
$command  = $_REQUEST['command'];
$database = new Database();

if ($command == 'add') {
    $fields = explode(',', $_POST['db']);
    $values = explode(',', $_POST['data']);
    
    $umk = new umkPlugin($_POST['table'], $fields, $values);
    $umk->insert();
    
} else if ($command == 'delete') {
    $fields = $_POST['db'];
    $values = $_POST['data'];
    
    $umk = new umkPlugin($_POST['table'], $fields, $values);
    $umk->delete();
    
} else if ($command == 'update') {
    $fields = explode(',', $_POST['db']);
    $values = explode(',', $_POST['data']);
    
    $key   = $_POST['key'];
    $value = $_POST['value'];
    
    $umk = new umkPlugin($_POST['table'], $fields, $values);
    $umk->update("WHERE $key = '$value'");
}
// File Processing
if ($command == "uploadData") {

    $output_dir = $_POST['dir'];
    
    if (isset($_FILES["myfile"])) {
        $ret = array();
        
        $error = $_FILES["myfile"]["error"];
        
        if (!is_array($_FILES["myfile"]["name"])) //single file
        {
            $fileName = str_replace(" ", "", $_FILES["myfile"]["name"]);
            
            if (!class_exists('S3'))
                require_once('S3.php');
            if (!defined('awsAccessKey'))
                define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
            if (!defined('awsSecretKey'))
                define('awsSecretKey', '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');
            $s3           = new S3(awsAccessKey, awsSecretKey);
            $extension    = end(explode('.', $fileName));
            $updated_name = time() . '.' . $extension;
            //'third_party/uploads/'
            if ($s3->putObjectFile($_FILES["myfile"]["tmp_name"], "barca", $output_dir . $updated_name, S3::ACL_PUBLIC_READ)) {
                $ret[] = $updated_name;
            } else {
                $ret[] = "error";
            }
            
        } else //Multiple files, file[]
        {
            $fileCount = count($_FILES["myfile"]["name"]);
            for ($i = 0; $i < $fileCount; $i++) {
                $fileName = $_FILES["myfile"]["name"][$i];
                move_uploaded_file($_FILES["myfile"]["tmp_name"][$i], $output_dir . $fileName);
                $ret[] = $fileName;
            }
            
        }
        echo json_encode($ret);
    }
} else if ($command == "deleteData") {
    $output_dir = $_POST['dir'];
    if (isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name'])) {
        $fileName = $_POST['name'];
        $filePath = $output_dir . $fileName;
        
        if (!class_exists('S3'))
            require_once('S3.php');
        if (!defined('awsAccessKey'))
            define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
        if (!defined('awsSecretKey'))
            define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);
        
        if ($s3->deleteObject("barca", $filePath)) {
            echo "Deleted File " . $fileName . "<br>";
        }
        
    }
}

//Custom Commands
else if ($command == 'saveContest') {
    $fields = explode(',', $_POST['db']);
    $values = explode(',', $_POST['data']);
    
    $values[1] = date('Y-m-d H:i:s', strtotime(html_entity_decode($values[1])));
    $values[2] = date('Y-m-d H:i:s', strtotime(html_entity_decode($values[2])));
    
    $today     = date('Y-m-d');
    $startDate = date('Y-m-d', strtotime(html_entity_decode($values[1])));
    $endDate   = date('Y-m-d', strtotime(html_entity_decode($values[2])));
    
    $database->query('SELECT status FROM contest WHERE status = 2');
    $database->execute();
    $count = $database->rowCount();
    
    if ($startDate == $today && $endDate != $today && $count == 0) {
        $fields[3] = 'status';
        $values[3] = 2;
    } else {
        $fields[3] = 'status';
        $values[3] = 1;
    }
    
    $umk = new umkPlugin('contest', $fields, $values);
    $umk->insert();
    
    $contestId = $database->lastInsertId();
    if ($contestId != '') {
        $prizeName = explode(",", $_POST['prizes']);
        $prizeFile = explode(",", $_POST['files']);
        for ($i = 0; $i < count($prizeFile); $i++) {
            $j = ($i + 1);
            if ($prizeFile[$i] != "") {
                $database->query("INSERT INTO contestPrize (contestId, prizeRank, prizeText, prizeImageUrl) VALUES (:id, :rank, :ptext, :imageurl)");
                $database->bind(':id', $contestId);
                $database->bind(':rank', $j);
                $database->bind(':ptext', $prizeName[$i]);
                $database->bind(':imageurl', $prizeFile[$i]);
                $database->execute();
            }
        }
    }
} else if ($command == 'editContest') {
    $fields = explode(',', $_POST['db']);
    $values = explode(',', $_POST['data']);
    
    $values[1] = date('Y-m-d H:i:s', strtotime(html_entity_decode($values[1])));
    $values[2] = date('Y-m-d H:i:s', strtotime(html_entity_decode($values[2])));
    
    $today     = date('Y-m-d');
    $startDate = date('Y-m-d', strtotime(html_entity_decode($values[1])));
    $endDate   = date('Y-m-d', strtotime(html_entity_decode($values[2])));
    
    $key   = $_POST['key'];
    $value = $_POST['value'];
    
    $database->query('SELECT status FROM contest WHERE contestId = "$value"');
    $database->execute();
    
    if ($startDate != $today && $endDate != $today) {
        $fields[3] = 'status';
        $values[3] = 1;
    } else if ($endDate == $today) {
        $fields[3] = 'status';
        $values[3] = 3;
    }
    
    $umk         = new umkPlugin('contest', $fields, $values);
    $queryResult = $umk->update("WHERE $key = '$value'");
    
    
    $prizeName = explode(",", $_POST['prizes']);
    $prizeFile = explode(",", $_POST['files']);
    $prizeId   = explode(",", $_POST['pIds']);
    
    for ($i = 0; $i < count($prizeFile); $i++) {
        $j = ($i + 1);
        if ($prizeFile[$i] != "") {
            if ($prizeId[$i] == "") {
                $database->query("INSERT INTO contestPrize (contestId, prizeRank, prizeText, prizeImageUrl) VALUES (:id, :rank, :ptext, :imageurl)");
                $database->bind(':id', $value);
                $database->bind(':rank', $j);
                $database->bind(':ptext', $prizeName[$i]);
                $database->bind(':imageurl', $prizeFile[$i]);
                $database->execute();
            } else {
                $database->query("UPDATE contestPrize set prizeRank = :rank, prizeText = :ptext, prizeImageUrl = :imageurl WHERE contestPrizeId = :id");
                $database->bind(':id', $prizeId[$i]);
                $database->bind(':rank', $j);
                $database->bind(':ptext', $prizeName[$i]);
                $database->bind(':imageurl', $prizeFile[$i]);
                $database->execute();
            }
        }
    }
} else if ($command == "removeContestImg") {
    $id  = $_POST['id'];
    $src = $_POST['src'];
    if (!class_exists('S3'))
        require_once('S3.php');
    if (!defined('awsAccessKey'))
        define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
    if (!defined('awsSecretKey'))
        define('awsSecretKey', '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');
    $s3 = new S3(awsAccessKey, awsSecretKey);
    
    if ($s3->deleteObject("barca", $src)) {
        echo "correct";
        $database->query('UPDATE contestPrize SET prizeImageUrl = "" WHERE contestPrizeId = :cid');
        $database->bind(":cid", $id);
        $database->execute();
    } else {
        echo "incorrect";
    }
} else if ($command == 'saveHelp') {
    $fields = explode(',', $_POST['db']);
    $values = explode(',', $_POST['data']);
    
    $umk = new umkPlugin('helpscreen', $fields, $values);
    $umk->insert();
} else if ($command == "delHelp") {
    $id = $_POST['id'];
    $database->query('SELECT * FROM helpscreen WHERE screenId = :id');
    $database->bind(":id", $id);
    $database->execute();
    $row = $database->single();
    
    if (!class_exists('S3'))
        require_once('S3.php');
    if (!defined('awsAccessKey'))
        define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
    if (!defined('awsSecretKey'))
        define('awsSecretKey', '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');
    $s3 = new S3(awsAccessKey, awsSecretKey);
    
    if ($s3->deleteObject("barca", $row['screenImageUrl'])) {
        $s3->deleteObject("barca", $row['screenIconUrl']);
        echo 'correct';
        $database->query('DELETE FROM helpscreen WHERE screenId = :id');
        $database->bind(":id", $id);
        $database->execute();
    } else {
        echo "incorrect";
    }
    
} else if ($command == "editHelp") {
    $fields = explode(',', $_POST['db']);
    $values = explode(',', $_POST['data']);
    
    if ($_POST['subCommand'] == 'updateFile') {
        $database->query('SELECT screenImageUrl, screenIconUrl FROM helpscreen WHERE screenId = :id');
        $database->bind(":id", $_POST['value']);
        $database->execute();
        $row = $database->single();
        
        if (!class_exists('S3'))
            require_once('S3.php');
        if (!defined('awsAccessKey'))
            define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
        if (!defined('awsSecretKey'))
            define('awsSecretKey', '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');
        $s3 = new S3(awsAccessKey, awsSecretKey);
        
        if ($row['screenImageUrl'] != $values[0] && !empty($row['screenImageUrl'])) {
            $fileName = $row['screenImageUrl'];
            if ($s3->deleteObject("barca", $fileName)) {
                //echo "Deleted File " . $fileName . "<br>";
            }
        }
        if ($row['screenIconUrl'] != $values[1] && !empty($row['screenIconUrl'])) {
            $fileName = $row['screenIconUrl'];
            if ($s3->deleteObject("barca", $fileName)) {
                //echo "Deleted File " . $fileName . "<br>";
            }
        }
    }
    
    $key   = $_POST['key'];
    $value = $_POST['value'];
    
    $umk = new umkPlugin('helpscreen', $fields, $values);
    $umk->update("WHERE $key = '$value'");
} else if ($command == "saveContestState") {
    $noti       = $_POST['noti'];
    $email_noti = $_POST['email_noti'];
    $contestId  = $_POST['contestId'];
    $userId     = $_POST['userId'];
    $postId     = $_POST['postId'];
    
    $database->query('SELECT id, contestId FROM contest_text_state WHERE contestId = :id');
    $database->bind(':id', $contestId);
    $database->execute();
    
    if ($database->rowCount() > 0) {
        $row = $database->single();
        $id  = $row['id'];
        $database->query('UPDATE contest_text_state SET notification = :noti, email = :email WHERE id = :id');
        $database->bind(":noti", $noti);
        $database->bind(":email", $email_noti);
        $database->bind(":id", $id);
        
    } else {
        $database->query('INSERT INTO contest_text_state (contestId, notification, email) VALUES (:id, :noti, :email)');
        $database->bind(":noti", $noti);
        $database->bind(":email", $email_noti);
        $database->bind(":id", $contestId);
    }
    
    if ($database->execute()) {
        $database->query('DELETE FROM contest_winner_state WHERE contestId = :id');
        $database->bind(":id", $contestId);
        $database->execute();
        
        
        $arrayPostId = explode(',', $postId);
        $arrayUserId = explode(',', $userId);
        
        
        for ($i = 0; $i < count($arrayPostId); $i++) {
            $database->query('INSERT INTO contest_winner_state (contestId, postId, userId, sortId) VALUES (:id, :postid, :userid, :sortid)');
            $database->bind(":id", $contestId);
            $database->bind(":postid", $arrayPostId[$i]);
            $database->bind(":userid", $arrayUserId[$i]);
            $database->bind(":sortid", ($i + 1));
            $database->execute();
        }
        echo "correct";
    } else {
        echo "incorrect";
    }
    
} else if ($command == "announceResult") {
    $noti       = $_POST['noti'];
    $email_noti = $_POST['email_noti'];
    $contestId  = $_POST['contestId'];
    $userId     = $_POST['userId'];
    $postId     = $_POST['postId'];
    
    $database->query('SELECT id, contestId FROM contest_text_state WHERE contestId = :id');
    $database->bind(':id', $contestId);
    $database->execute();
    
    if ($database->rowCount() > 0) {
        $row = $database->single();
        $id  = $row['id'];
        $database->query('UPDATE contest_text_state SET notification = :noti, email = :email WHERE id = :id');
        $database->bind(":noti", $noti);
        $database->bind(":email", $email_noti);
        $database->bind(":id", $id);
        
    } else {
        $database->query('INSERT INTO contest_text_state (contestId, notification, email) VALUES (:id, :noti, :email)');
        $database->bind(":noti", $noti);
        $database->bind(":email", $email_noti);
        $database->bind(":id", $contestId);
    }
    
    if ($database->execute()) {
        $database->query('DELETE FROM contest_winner_state WHERE contestId = :id');
        $database->bind(":id", $contestId);
        $database->execute();
        
        
        $arrayPostId = explode(',', $postId);
        $arrayUserId = explode(',', $userId);
        
        
        for ($i = 0; $i < count($arrayPostId); $i++) {
            $database->query('INSERT INTO contest_winner_state (contestId, postId, userId, sortId) VALUES (:id, :postid, :userid, :sortid)');
            $database->bind(":id", $contestId);
            $database->bind(":postid", $arrayPostId[$i]);
            $database->bind(":userid", $arrayUserId[$i]);
            $database->bind(":sortid", ($i + 1));
            $database->execute();
        }
        
        $database->query('SELECT contestPrizeId, contestId FROM contestPrize WHERE contestId = :id ORDER BY prizeRank ASC');
        $database->bind(":id", $contestId);
        $database->execute();
        if ($database->rowCount() > 0) {
            $result = $database->resultset();
            $j      = 0;
            
            //Apple Push noti settings
            $passphrase = 'tawasolit';
            $ctx        = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'fcb.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            
            //Apple Push noti settings
            
            foreach ($result as $row) {
                $updId = $row['contestPrizeId'];
                $database->query('UPDATE contestPrize SET winnerPostId = :userid WHERE contestPrizeId = :id');
                $database->bind(':userid', $arrayUserId[$j]);
                $database->bind(':id', $updId);
                $database->execute();
                
                $database->query('SELECT fullName, email, pushnotificationtoken, devicetype FROM user WHERE userId = :id');
                $database->bind(":id", $arrayUserId[$j]);
                $database->execute();
                $userRow = $database->single();
                
                //Send Noti
                
                //Android
                if ($userRow['devicetype'] == 1) {
                    if (!empty($userRow['pushnotificationtoken'])) {
                        $apiKey = "AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs";
                        
                        $registrationIDs = array(
                            $userRow['pushnotificationtoken']
                            );
                        
                        $message = $noti;
                        
                        $url = 'https://android.googleapis.com/gcm/send';
                        
                        $fields = array(
                            'registration_ids' => $registrationIDs,
                            'data' => array(
                                "message" => $message
                                )
                            );
                        
                        $headers = array(
                            'Authorization: key=' . $apiKey,
                            'Content-Type: application/json'
                            );
                        
                        $ch = curl_init();
                        
                        curl_setopt($ch, CURLOPT_URL, $url);
                        
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                        
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        
                        $result = curl_exec($ch);
                        if ($result === FALSE) {
                            die('Curl failed: ' . curl_error($ch));
                        }
                        
                        curl_close($ch);
                        //var_dump($result);
                    }
                } else {
                    if (!empty($userRow['pushnotificationtoken'])) {
                        $deviceToken = trim($userRow['pushnotificationtoken']);
                        
                        $message = $noti;
                        
                        if (!$fp)
                            exit("Failed to connect: $err $errstr" . PHP_EOL);
                        
                        //echo 'Connected to APNS' . PHP_EOL;
                        
                        $body['aps'] = array(
                            'badge' => +1,
                            'alert' => $message,
                            'sound' => 'default'
                            );
                        
                        $payload = json_encode($body);
                        
                        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
                        
                        $result = fwrite($fp, $msg, strlen($msg));
                        
                        /*
                        if (!$result)
                        echo    $noti_msg = 'Message not delivered';
                        else
                        echo    $noti_msg = 'Message successfully delivered ' . $message;
                        */
                        
                    }
                }
                
                //Apple Push noti
                @socket_close($fp);
                fclose($fp);
                //Apple Push noti
                
                //Send Email
                if (!empty($userRow['email'])) {
                    /*$mail = new PHPMailer();
                    $mail->AddReplyTo('um.air.mist@gmail.com', 'Barca Out');
                    $mail->SetFrom('um.air.mist@gmail.com', 'Barca Out');
                    
                    $mail->AddAddress($userRow['email'], $userRow['fullName']);
                    $mail->Subject = 'Contest Email Notification';
                    $mail->MsgHTML($email_noti);
                    
                    $mail->Send();
*/
                    $mail = new PHPMailer();
                    $mail->IsSMTP(); 
            //$mail->Host       = "smtp.gmail.com";
            //$mail->SMTPDebug  = 1;           
            // 1 = errors and messages
            // 2 = messages only
                    $mail->SMTPAuth   = true;         
                    $mail->Host       = "smtp.gmail.com";
                    $mail->SMTPSecure = 'tls'; 
                    $mail->Port       = 587;
                    $mail->Username   = "no-reply@fcbstudio.info";
                    $mail->Password   = "T@W@$0lIT"; 

                    $mail->AddReplyTo('no-reply@fcbstudio.info');
                    $mail->SetFrom('no-reply@fcbstudio.info');
                    $mail->AddAddress($userRow['email'], $userRow['fullName']);
                    $mail->Subject = 'Contest Email Notification';
                    $mail->Body =  html_entity_decode($email_noti);
                    $mail->IsHTML(true);
                    $mail->Send();
                }
                //End
                $j = ($j + 1);
            }
        }
        $database->query('UPDATE contest SET status = 4 WHERE contestId = :id');
        $database->bind(':id', $contestId);
        if ($database->execute()) {
            echo "correct";
        } else {
            echo 'incorrect';
        }
    } else {
        echo "incorrect";
    }
} else if ($command == 'savePoints') {
    $database->query('SELECT id FROM points');
    $database->execute();
    $count = $database->rowCount();
    $pRow  = $database->single();
    
    $comm    = $_POST['comm'];
    $commArr = explode(",", $comm);
    
    $commVal    = $_POST['commVal'];
    $commValArr = explode(",", $commVal);
    
    for ($i = 0; $i < count($commValArr); $i++) {
        if ($commArr[$i] == 'dec') {
            $commValArr[$i] = "-" . $commValArr[$i];
        }
    }
    
    $db     = $_POST['db'];
    $fields = explode(",", $db);
    
    //Image
    if ($commArr[14] != 'no') {
        $database->query('SELECT studioPhotoId, isPremium, requiredPoints FROM studiophoto WHERE isPremium = "0"');
        $database->execute();
        $result = $database->resultset();
        
        foreach ($result as $row) {

            $points = $commValArr[14];
            
            $database->query('UPDATE studiophoto SET requiredPoints = :points WHERE studioPhotoId = :id');
            $database->bind(":points", $points);
            $database->bind(":id", $row['studioPhotoId']);
            $database->execute();
        }
    }
    //Premium Image
    if ($commArr[15] != 'no') {
        $database->query('SELECT studioPhotoId, isPremium, requiredPoints FROM studiophoto WHERE isPremium = "1"');
        $database->execute();
        $result = $database->resultset();
        
        foreach ($result as $row) {

            $points = $commValArr[15];
            $database->query('UPDATE studiophoto SET requiredPoints = :points WHERE studioPhotoId = :id');
            $database->bind(":points", $points);
            $database->bind(":id", $row['studioPhotoId']);
            $database->execute();
        }
    }
    if ($count > 0) {
        $umk = new umkPlugin('points', $fields, $commValArr);
        $umk->update("WHERE id = '1'");
        $count = 1;
    } else {
        $umk = new umkPlugin('points', $fields, $commValArr);
        $umk->insert();
    }
} else if ($command == "sendNotification") {
    $date    = date("Y-m-d", strtotime($_POST['date']));
    $time    = date("H:i:s", strtotime($_POST['time']));
    $sendto  = $_POST['sendto'];
    if($sendto == "all")
    {
        $saveSendto = "All";
    } else 
    {
        $saveSendto = "Selected";
    }
    $sendvia = $_POST['sendvia'];
    $mesg    = $_POST['mesg'];
    
    $database->query('INSERT INTO send_notifications (sendDate, sendTime, sendTo, sendVia, message) VALUES (:edate, :etime, :sendto, :sendvia, :mesg)');
    $database->bind(":edate", $date);
    $database->bind(":etime", $time);
    $database->bind(":sendto", $saveSendto);
    $database->bind(":sendvia", $sendvia);
    $database->bind(":mesg", $mesg);
    if ($database->execute()) {
        //Apple Push noti settings
        $passphrase = 'tawasolit';
        $ctx        = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'fcb.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        
        //Apple Push noti settings
        if ($sendvia == "Notification") {
            if ($sendto == "all") {
                $database->query('SELECT fullName, email, pushnotificationtoken, devicetype FROM user WHERE (userType = :type OR userType = :type2) AND blocked = :block ORDER BY fullName ASC');
                $database->bind(':type', 1);
                $database->bind(':type2', 2);
                $database->bind(':block', 0);
                $database->execute();
            } else {
                $database->query('SELECT fullName, email, pushnotificationtoken, devicetype FROM user WHERE userId IN('.$sendto.') AND (userType = :type OR userType = :type2) AND blocked = :block');
                
                $database->bind(':type', 1);
                $database->bind(':type2', 2);
                $database->bind(':block', 0);
                $database->execute();
            }
            $result = $database->resultset();
            //Send Noti
            //echo $database->rowCount();
            
            foreach ($result as $userRow) {

                //Android
                if ($userRow['devicetype'] == 1) {
                    if (!empty($userRow['pushnotificationtoken'])) {
                        $apiKey = "AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs";
                        
                        $registrationIDs = array(
                            $userRow['pushnotificationtoken']
                            );
                        
                        $message = $mesg;
                        
                        $url = 'https://android.googleapis.com/gcm/send';
                        
                        $fields = array(
                            'registration_ids' => $registrationIDs,
                            'data' => array(
                                "message" => $message
                                )
                            );
                        
                        $headers = array(
                            'Authorization: key=' . $apiKey,
                            'Content-Type: application/json'
                            );
                        
                        $ch = curl_init();
                        
                        curl_setopt($ch, CURLOPT_URL, $url);
                        
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                        
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        
                        $result = curl_exec($ch);
                        if ($result === FALSE) {
                            die('Curl failed: ' . curl_error($ch));
                        }
                        
                        curl_close($ch);
                        //var_dump($result);
                    }
                } else {
                    if (!empty($userRow['pushnotificationtoken'])) {
                        $deviceToken = trim($userRow['pushnotificationtoken']);
                        
                        $message = $mesg;
                        
                        if (!$fp)
                            exit("Failed to connect: $err $errstr" . PHP_EOL);
                        
                        //echo 'Connected to APNS' . PHP_EOL;
                        
                        $body['aps'] = array(
                            'badge' => +1,
                            'alert' => $message,
                            'sound' => 'default'
                            );
                        
                        $payload = json_encode($body);
                        
                        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
                        
                        $result = fwrite($fp, $msg, strlen($msg));
                        
                        
                        /*if (!$result)
                        echo    $noti_msg = 'Message not delivered';
                        else
                        echo    $noti_msg = 'Message successfully delivered ' . $message;
                        */
                        
                    }
                }
            }
            //Apple Push noti
            @socket_close($fp);
            fclose($fp);
            //Apple Push noti
        } //Notification
        else {

            if ($sendto == "all") {
                $database->query('SELECT fullName, email, pushnotificationtoken, devicetype FROM user WHERE (userType = :type OR userType = :type2) AND blocked = :block ORDER BY fullName ASC');
                $database->bind(':type', 1);
                $database->bind(':type2', 2);
                $database->bind(':block', 0);
                $database->execute();
            } else {
                $database->query('SELECT fullName, email, pushnotificationtoken, devicetype FROM user WHERE userId IN('.$sendto.') AND (userType = :type OR userType = :type2) AND blocked = :block');
                $database->bind(':type', 1);
                $database->bind(':type2', 2);
                $database->bind(':block', 0);
                $database->execute();
            }
            $result = $database->resultset();
            foreach ($result as $userRow) {
                //Send Email
                if (!empty($userRow['email'])) {
                    /*$mail = new PHPMailer();
                    $mail->AddReplyTo('um.air.mist@gmail.com', 'Barca Out');
                    $mail->SetFrom('um.air.mist@gmail.com', 'Barca Out');
                    
                    $mail->AddAddress($userRow['email'], $userRow['fullName']);
                    $mail->Subject = 'Email Notification';
                    $mail->MsgHTML($mesg);
                    
                    $mail->Send();
*/
                    $mail = new PHPMailer();
                    $mail->IsSMTP(); 
            //$mail->Host       = "smtp.gmail.com";
            //$mail->SMTPDebug  = 1;           
            // 1 = errors and messages
            // 2 = messages only
                    $mail->SMTPAuth   = true;         
                    $mail->Host       = "smtp.gmail.com";
                    $mail->SMTPSecure = 'tls'; 
                    $mail->Port       = 587;
                    $mail->Username   = "no-reply@fcbstudio.info";
                    $mail->Password   = "T@W@$0lIT"; 

                    $mail->AddReplyTo('no-reply@fcbstudio.info');
                    $mail->SetFrom('no-reply@fcbstudio.info');
                    $mail->AddAddress($userRow['email'], $userRow['fullName']);
                    $mail->Subject = 'Email Notification';
                    $mail->Body =  html_entity_decode($mesg);
                    $mail->IsHTML(true);
                    $mail->Send();
                }
            }
        }
        echo "correct";
    } else {
        echo 'incorrect';
    }
}
?>