<?php $pageName = basename($_SERVER['PHP_SELF']); 
include_once('defaults.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!--<meta charset="utf-8">-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FCB Studio Panel</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $rootPath; ?>images/favicon.ico">
    <!-- Bootstrap -->
    <link href="<?php echo $rootPath; ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $rootPath; ?>plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link href="<?php echo $rootPath; ?>plugins/upload/uploadfile.css" rel="stylesheet" type="text/css">
    
    <link rel="stylesheet" type="text/css" href="<?php echo $rootPath; ?>css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<div class="alert alert-dismissible" role="alert">
  <button type="button" class="close" data-hide="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong></strong>
</div>

<div id="overlay"></div>
<div id="loader">
  <img src="<?php echo $rootPath; ?>images/211.GIF" alt="Loading...">
</div>

<div class="container-fluid top-header">
  <div class="row">
    <div class="col-md-12 header">
      <h1 class="pull-left"><a href="<?php echo $rootPath; ?>index.php"><img src="<?php echo $rootPath; ?>images/fcbstudio.png" alt="Logo" width="40"> FCB Studio Panel</a></h1>
      <?php if(isset($log_id)) { ?>
      <a style="margin-top: 20px" href="<?php echo $rootPath; ?>logout.php" class="btn btn-danger btn-md pull-right">Logout</a>
      <a style="margin-top: 20px; margin-right: 10px" href="#changeModal" data-dismiss="modal" data-toggle="modal" data-target="#changeModal" role="button" class="btn btn-primary btn-md pull-right">Change Password</a>
      <?php } ?>
    </div>
  </div>
</div>

<div class="modal fade" id="changeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header modal_color">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">
              Change Password
            </h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" id="change_pass_form" role="form">
              <div class="form-group">
                <label for="email" class="col-sm-3 control-label">
                  Password
                </label>
                <div class="col-sm-9">
                  <input type="password" id="oldpass" maxlength="90" name="oldpass" class="form-control required" />
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 control-label" >
                  New password
                </label>
                <div class="col-sm-9">
                  <input type="password" data-key="password" maxlength="90" id="cpassword" name="cpassword" class="form-control required" />
                </div>
              </div>
              <div class="form-group">
                <label  for="email" class="col-sm-3 control-label" style="white-space: nowrap">
                  Re Type Password
                </label>
                <div class="col-sm-9">
                  <input type="password" name="pass" id="new-repass" maxlength="90" class="form-control" equalTo='#cpassword' />
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default cancelKey" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-primary modal_color" id="change">
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
    <!--/change password modal-->