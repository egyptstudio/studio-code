<?php 
include 'database.class.php';

class umkPlugin extends Database {

    public $table;
    public $fields;
    public $values;
    
    public $database;

    public function __construct($table, $fields, $values){
        $this->fields = $fields;
        $this->values = $values;
        $this->table = $table;
        $this->database = new Database();
    }

    public function fields_to_string(){
        return implode(", ", $this->fields);
    }

    public function fields_to_parameter(){
        return ":".implode(", :", $this->fields);
    }

    public function properties_to_string(){
        return "'".implode("', '", $this->values)."'";
    }
	
	public function fields_to_string_for_udpate(){
		foreach($this->fields as $field_name => $field_value) {
   			$sql_str[] = "".$field_value." = :".$field_value."";
		}
		return implode(", ", $sql_str);
    }

    //Login

    public function fields_to_string_for_login(){
		$sql_str = array();
		foreach($this->fields as $field_name => $field_value) {
   			$sql_str[] = "".$field_value." = :".$field_value."";
		}
		return $sql_str[0] ." and ". $sql_str[1];
    }

    public function login($custom_fields){
		//echo 'SELECT '.$this->fields_to_string() .', '. $custom_fields .' FROM '.$this->table.' WHERE '.$this->fields_to_string_for_login();
		$this->database->query('SELECT '.$this->fields_to_string() .', '. $custom_fields .' FROM '.$this->table.' WHERE '.$this->fields_to_string_for_login());
		for($i=0; $i<count($this->fields); $i++)
		{
		//	echo $this->values[$i];
			$this->database->bind(":".$this->fields[$i], $this->values[$i]);
		}
		$this->database->execute();
		$count = $this->database->rowCount();
		if($count == 1)
		{
			$row = $this->database->single();

			$_SESSION["log_id"]= $row["id"];
			$_SESSION["log_name"]=$row["username"];
			$_SESSION["category"]= 'admin';
			echo 'correct';
		} else 
		{
				$this->database->query('SELECT email, password, userId FROM user WHERE (userType = 1 or userType = 2) AND (email = :username AND password = :password)');
				for($i=0; $i<count($this->fields); $i++)
				{
					$this->database->bind(":".$this->fields[$i], $this->values[$i]);
				}
				
				$this->database->execute();
				$count = $this->database->rowCount();
				if($count == 1)
				{
					$row = $this->database->single();
					$_SESSION["log_id"]= $row["userId"];
					$_SESSION["log_name"]=$row["email"];
					$_SESSION["category"]= 'user';
					echo 'correct';
				} else 
				{
					echo 'incorrect';
				}
		}
		
	}

    //Login

    //Insertion
    public function insert(){
    	//echo 'INSERT INTO {'.$this->table.'} ('.$this->fields_to_string().') VALUES ('.$this->fields_to_parameter().')';
		$this->database->query('INSERT INTO '.$this->table.' ('.$this->fields_to_string().') VALUES ('.$this->fields_to_parameter().')');
		
		for($i=0; $i<count($this->fields); $i++)
		{
			$this->database->bind(":".$this->fields[$i], $this->values[$i]);
		}

		if($this->database->execute())
		{
			echo "correct";
		} else 
		{
			echo "incorrect";
		}
    }
	//Insertion

	//Updation
    public function update($where){
    	
    	$this->database->query('UPDATE '.$this->table.' SET '.$this->fields_to_string_for_udpate().' '.$where.'');
		
		for($i=0; $i<count($this->fields); $i++)
		{
			$this->database->bind(":".$this->fields[$i], $this->values[$i]);
		}
		
		if($this->database->execute())
		{
			echo "correct";
		} else 
		{
			echo "incorrect";
		}
    }
	//Updation

	//Deletion
    public function delete()
	{
		
		$this->database->query('DELETE FROM '.$this->table.' WHERE '.$this->fields.' = :'.$this->fields.'');
		//echo count($this->fields);
		
		$this->database->bind(":".$this->fields, $this->values);
		
		if($this->database->execute())
		{
			echo "correct";
		} else 
		{
			echo "incorrect";
		}
	}
    //Deletion

	//Update File
	public function updateFile($where, $filefld, $filepath, $fileName){
		
		//echo 'SELECT '.$this->fields_to_string() .', '. $custom_fields .' FROM '.$this->table.' WHERE '.$this->fields_to_string_for_login();
		$this->database->query('SELECT '.$this->fields_to_string() .' FROM '.$this->table.' '.$where.'');
		$this->database->execute();
		$count = $this->database->rowCount();
		
		$row = $this->database->single();

		if($count >= 1 && file_exists($_SERVER['DOCUMENT_ROOT'].$filepath.$row[$filefld]))
		{
			$file = $_SERVER['DOCUMENT_ROOT'].str_replace("uploads", "cropped", $filepath).$row[$filefld];
			unlink($file);
		}

		update($where);
    }
	//Update File



	//Delete File
	public function deleteFile($where, $filefld, $filepath, $fileName){
		//echo 'SELECT '.$this->fields_to_string() .', '. $custom_fields .' FROM '.$this->table.' WHERE '.$this->fields_to_string_for_login();
		$this->database->query('SELECT '.$this->fields_to_string() .' FROM '.$this->table.' '.$where.'');
		$this->database->execute();
		$count = $this->database->rowCount();
		$row = $this->database->single();
		if($count >= 1 && file_exists($_SERVER['DOCUMENT_ROOT'].$filepath.$row[$filefld]))
		{
			$file = $_SERVER['DOCUMENT_ROOT'].str_replace("uploads", "cropped", $filepath).$row[$filefld];
			unlink($file);
		}

		delete();
    }
	//Delete File



}// end of class
?>