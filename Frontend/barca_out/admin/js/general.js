$(document).ready(function() {
    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    var fileName = loc.substring(loc.lastIndexOf('/') + 1);
    var topLoc = window.top.location.pathname;
    var qs = window.top.location.search;

    $('#selectall').click(function(event) { 
        if (this.checked) { 
            $('.post_photo').each(function() { 
                this.checked = true; 
            });
        } else {
            $('.post_photo').each(function() { 
                this.checked = false;
            });
        }
    });

    $(".save_contest").click(function(event){
        event.preventDefault();
        
        if($("#name").val() == "" || $("#startdate").val() == "" || $("#enddate").val() == "")
        {
            showNotification('error', 'Please fill all mandatory fields!');
            return false;
        }
        //showLoader();
        
        var startDateTime = $("#startdate").val() + " " + $("#starttime").val();
        var endDateTime = $("#enddate").val() + " " + $("#endtime").val();
        
        var arrFields = ['name', 'startDateTime', 'endDateTime'];
        var arrValues = [$("#name").val(), startDateTime.escapeHTML(), endDateTime.escapeHTML()];

        var arrPrizeNames = $(".myform .getPName").map(function() {
            if ($(this).val().length == 0) {
                $(this).val('');
            }
            return this.value.escapeHTML();
        }).get();

        var arrPrizeFiles = $(".myform .getPFile").map(function() {
            if ($(this).val().length == 0) {
                $(this).val('');
            }
            return this.value.escapeHTML();
        }).get();
        showLoader();
        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&prizes=' + encodeURIComponent(arrPrizeNames) + '&files=' + encodeURIComponent(arrPrizeFiles) + '&command=' + 'saveContest',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                    showNotification('success', 'Contest added successfully!');
                    setTimeout(function(){
                        window.location.href = 'contests.php';
                    }, 2000);
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });        

    });

    $(".edit_contest").click(function(event){
        event.preventDefault();
        
        if($("#name").val() == "" || $("#startdate").val() == "" || $("#enddate").val() == "")
        {
            showNotification('error', 'Please fill all mandatory fields!');
            return false;
        }
        //showLoader();
        
        var startDateTime = $("#startdate").val() + " " + $("#starttime").val();
        var endDateTime = $("#enddate").val() + " " + $("#endtime").val();
        
        var arrFields = ['name', 'startDateTime', 'endDateTime'];
        var arrValues = [$("#name").val(), startDateTime.escapeHTML(), endDateTime.escapeHTML()];

        var arrPrizeNames = $(".myform .getPName").map(function() {
            if ($(this).val().length == 0) {
                $(this).val('');
            }
            return this.value.escapeHTML();
        }).get();

        var arrPrizeFiles = $(".myform .getPFile").map(function() {
            if ($(this).val().length == 0) {
                $(this).val('');
            }
            return this.value.escapeHTML();
        }).get();

        var arrPrizeIds = $(".myform .getPFile").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId == null) {
                childId = "";
            }
            return childId;
        }).get();
        
        var idVal = $("#contestId").val();
        
        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&prizes=' + encodeURIComponent(arrPrizeNames) + '&files=' + encodeURIComponent(arrPrizeFiles) + '&pIds=' + arrPrizeIds + '&key=' + 'contestId' + '&value=' + idVal + '&command=' + 'editContest',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                    showNotification('success', 'Contest updated successfully!');
                    setTimeout(function(){
                        window.location.href = 'contest_detail.php?id='+idVal;
                    }, 2000);
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });        

    });

    //Delete Image
    $(".delImage").click(function(event){
        event.preventDefault();
        var id = $(this).data('id');
        var src = $(this).data('src');
        showLoader();
        var $tis = $(this);
        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'id=' + id + '&src=' + src + '&command=' + 'removeContestImg',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                    $tis.parent("div.prizeImgSec").hide();
                    $tis.parent("div.prizeImgSec").next("div.prizeUploader").fadeIn();
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });
    });

    // Add help
    $(".save_help").click(function(event){
        event.preventDefault();
        
        if($("#title").val() == "" || $("#simage_fileName").val() == "" || $("#sicon_fileName").val() == "")
        {
            showNotification('error', 'Please fill all mandatory fields!');
            return false;
        }

        var arrFields = $(".myform .getValue").map(function() {
            var $elm = $(this),
                childId = $elm.data('key');
            if (childId == null) {
                childId = "";
            }
            return childId;
        }).get();

        var arrValues = $(".myform .getValue").map(function() {
            if ($(this).val().length == 0) {
                $(this).val('');
            }
            return this.value.escapeHTML();
        }).get();

        showLoader();
        
        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'saveHelp',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                   showNotification('success', 'Help Screen Added successfully!');
                    setTimeout(function(){
                        window.location.href = 'help.php';
                    }, 2000);
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });
    });
    //Open Uploader
    $(".openUploader").click(function(){
        $(this).parent('div.prizeImgSec').hide();
        $(this).parent('div.prizeImgSec').next('div.prizeUploader').fadeIn();
    });

//Delete Help
    $(document).on('click', '.delHelp', function(evnet){
        event.preventDefault();
        
        var id = $(this).data('id');
        
        showLoader();
        
        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'id=' + id + '&command=' + 'delHelp',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                   showNotification('success', 'Help Screen Deleted successfully!');
                   $(".postSection").load(""+loc+" .aj_sec"); 
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });
    });

    //Edit Help
    $(".edit_help").click(function(event){
        event.preventDefault();
        
        var img1 = $(".old_file1").val(),
        screenImg = $("#simage_fileName").val(),

        img2 = $(".old_file2").val(),
        iconImg = $("#sicon_fileName").val();

        if($("#title").val() == "")
        {
            showNotification('error', 'Please fill all mandatory fields!');
            return false;
        }

        if((img1 != screenImg && screenImg != "") || (img2 != iconImg && iconImg != ""))
        {
            command = "updateFile";
            if(screenImg == "")
            {
                $("#simage_fileName").val(img1);
            }
            if(iconImg == "")
            {
                $("#sicon_fileName").val(img2);
            }
        } else
        {
            command = "update";
            $("#simage_fileName").val(img1);
            $("#sicon_fileName").val(img2);
        }

        var arrFields = $(".myform .getValue").map(function() {
            var $elm = $(this),
                childId = $elm.data('key');
            if (childId == null) {
                childId = "";
            }
            return childId;
        }).get();

        var arrValues = $(".myform .getValue").map(function() {
            if ($(this).val().length == 0) {
                $(this).val('');
            }
            return this.value.escapeHTML();
        }).get();

        var idVal = $("#screenId").val();
        showLoader();

        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + 'screenId' + '&value=' + idVal + '&subCommand=' + command + '&command=' + 'editHelp',
            cache: false,
            success: function(data) {
                
                hideLoader();
                if(data == 'correct')
                {
                   showNotification('success', 'Help Screen Edited successfully!');
                    setTimeout(function(){
                        window.location.href = 'help.php';
                    }, 2000);
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });
    });

    //Post Photo Pop
    $(document).on('click', '.postPhoto', function(event){
        event.preventDefault();
        showLoader();
        var url = $(this).attr('data-href');
        $(".postPhotoHolder").attr('src', url);
        setTimeout(function(){
            hideLoader();
           $("#myModal").modal('show'); 
        }, 800);
    });

    //Add to winners
    $(".post_body .post_checkbox").click(function(){
        if($(this).is(':checked'))
        {
            var arrPostId = $(".shadow_body .post_checkbox").map(function() {
                var $elm = $(this),
                    childId = $elm.attr('id').replace("p", "");
                if (childId == null) {
                    childId = "";
                }
                return childId;
            }).get();
            if(arrPostId.length == 10)
            {
                showNotification('warning', 'Cannot add more than 10 posts!');
                return false;
            }
            var dhtml = $(this).parents('tr').html();
            $(".shadow_body").append("<tr>"+dhtml+"</tr>");
            var did = $(this).attr('id');
            $(".shadow_body").find('input#'+did).before('<i class="fa fa-arrows move-arrows"></i>');
            $(this).hide();
        }
    });
    $(document).on('click', '.shadow_body .post_checkbox, .winner_body .post_checkbox', function(){
        if($(this).is(':checked'))
        {
            var inputId = $(this).attr('id');
            $("#"+inputId).prop("checked", false);
            $("#"+inputId).show();
            var dhtml = $(this).parents('tr').remove();
        }
    });

    //Save contest state
    $(".save_contest_state").click(function(){
        var noti = $(".notification").val();
        var email_noti = $(".email_noti").val();
        var contestId = $("#contestId").val();

        var arrUserId = $(".shadow_body .post_checkbox").map(function() {
            var $elm = $(this),
                childId = $elm.data('userid');
            if (childId == null) {
                childId = "";
            }
            return childId;
        }).get();

        var arrPostId = $(".shadow_body .post_checkbox").map(function() {
            var $elm = $(this),
                childId = $elm.attr('id').replace("p", "");
            if (childId == null) {
                childId = "";
            }
            return childId;
        }).get();

        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'noti=' + noti + '&email_noti=' + email_noti +'&contestId=' + contestId + '&userId=' + arrUserId + '&postId=' + arrPostId + '&command=' + 'saveContestState',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                   showNotification('success', 'State Saved successfully!');
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });
    });
    
    //Announce result
    $(".announce_result").click(function(){
       var noti = $(".notification").val();
        var email_noti = $(".email_noti").val();
        var contestId = $("#contestId").val();

        var arrUserId = $(".shadow_body .post_checkbox").map(function() {
            var $elm = $(this),
                childId = $elm.data('userid');
            if (childId == null) {
                childId = "";
            }
            return childId;
        }).get();

        var arrPostId = $(".shadow_body .post_checkbox").map(function() {
            var $elm = $(this),
                childId = $elm.attr('id').replace("p", "");
            if (childId == null) {
                childId = "";
            }
            return childId;
        }).get();
        showLoader();
        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'noti=' + noti + '&email_noti=' + email_noti +'&contestId=' + contestId + '&userId=' + arrUserId + '&postId=' + arrPostId + '&command=' + 'announceResult',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                   showNotification('success', 'State Saved successfully!');
                   setTimeout(function(){
                        window.location.href = loc+qs;
                   }, 2000);
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });
    });
    
    // Points Management
    $(".save_points").click(function(event){
        event.preventDefault();
        
        var arrComm = [];
        var arrCommVal = [];
        var arrDB = [];
        
        for(i=1; i<=22; i++)
        {
            window["commVal" + i] = $("#f"+i).parents('div.form-group').find('input.xp_text').val();
            window["db" + i] = $("#f"+i).parents('div.form-group').find('input.xp_text').attr('data-key');
            
            if(!$("#f"+i).prop('checked'))
            {
                window["comm" + i] = 'no';
            }
            if(window["commVal" + i] < 0)
            {
                showNotification('warning', 'Please enter all positive numbers');
                $("#f"+i).parents('div.form-group').find('input.xp_text').val('');
                return false;
            }
            //alert(window["comm" + i]);
            arrComm.push(window["comm" + i]);
            arrCommVal.push(window["commVal" + i]);
            arrDB.push(window["db" + i]);
        }
        
        showLoader();
        $.ajax({
            type: 'POST',
            url: '../includes/general_processing.php',
            data: 'comm=' + arrComm + '&commVal=' + arrCommVal + '&db=' + arrDB + 
                  '&command=' + 'savePoints',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                   showNotification('success', 'Points Saved successfully!');
                   setTimeout(function(){
                        window.location.href = loc;
                   }, 1000);
                } else 
                {
                    showNotification('error', 'There is some error, Please contact administration!');
                }
            }
        });
    });

    //Users
    var userArray = [];
    var idArray = [];
    $(document).on('click', '.user_checkbox', function() {
        
        if($(this).is(':checked'))
        {
            userArray.push($(this).val());
            $("#users").val(userArray);

            idArray.push($(this).data('id'));
            $("#users").attr('data-id', idArray);
        } else 
        {
            text = $(this).val();
            pos = getPosition(userArray, text);
        
            userArray = jQuery.grep(userArray, function(value) {
                return value != text;
            });
            
            var remove = idArray[pos];
            idArray = jQuery.grep(idArray, function(value) {
                return value != remove;
            });
            
            var newVal = removeValue($("#users").attr('data-id'), remove);
            $("#users").attr('data-id', newVal);
        }
    });
    $(document).on('click', '.select_user', function() {
        //$('input#players').tagsinput('removeAll');
        for (i = 0; i < userArray.length; i++) {
            $('input#users').tagsinput('add', userArray[i]);
        }
        $(".user_selection input").removeAttr('placeholder');
        $("#myModal").modal('hide');
    });

    $(document).on('click', '.cancel_user', function(){
        $("#users").attr('data-id', '');
        $('input#users').tagsinput('removeAll');
        userArray = [];
        idArray = [];
    });

    $(document).on('click', '.user_selection span.tag span', function(){
        var text = $(this).parent('span').text();
        var pos = getPosition(userArray, text);
       
        userArray = jQuery.grep(userArray, function(value) {
            return value != text;
        });
        var remove = idArray[pos];
        var newVal = removeValue($("#users").attr('data-id'), remove);
        $("#users").attr('data-id', newVal);
    });


}); // Main
function showLoader() {
    $("#overlay").show();
    $("#loader").show();
}

function hideLoader() {
    $("#overlay").hide();
    $("#loader").hide();
}

function showNotification(type, text) {
    if (type == 'error') {
        var notiClass = "alert-danger";
    } else if (type == 'success') {
        var notiClass = "alert-success";
    } else if (type == 'warning') {
        var notiClass = "alert-warning";
    } else if (type == 'info') {
        var notiClass = "alert-info";
    }
    $(".alert").addClass(notiClass);
    $(".alert strong").text(text);
    $(".alert").fadeIn().delay(3000).fadeOut();
}