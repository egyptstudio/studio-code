(function($) {
    $('ul[id=sub]').hide();
    $(".active_menu").parents('ul').show();
    $('label.tree-toggler').click(function() {
        $(this).parent().children('ul.tree').toggle(300);
    });

    var __entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        //"/": '&#x2F;',
        ",": '&#44;',
    };
    String.prototype.escapeHTML = function() {
        return String(this).replace(/[&<>"'\,]/g, function(s) {
            return __entityMap[s];
        });
    }
    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    var fileName = loc.substring(loc.lastIndexOf('/') + 1);
    var topLoc = window.top.location.pathname;
    var qs = window.top.location.search;
    //Login
    $("#login").click(function(e) {
        e.preventDefault();
        if (!$("#login_form").validate().form()) {
            return false;
        }
        showLoader();
        var arrFields = $("#login_form .getValue").map(function() {
            var $elm = $(this),
            childId = $elm.data('key');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();
        var arrValues = $("#login_form .getValue").map(function() {
            if ($(this).val().length == 0) {
                $(this).val('');
            }
            return this.value.escapeHTML();
        }).get();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'login',
            cache: false,
            success: function(data) {
                hideLoader();
                if (data == "correct") {
                    window.location.href = dir + "/index.php";
                } else {
                    showNotification('error', 'Username or Password is incorrect!');
                }
            }
        });
    });
    //Login
    //Search
    $.fn.searchText = function(settings) {
        var options = $.extend({
            key: null,
            dataKey: null
        }, settings);
        return this.on('keyup', function(event) {
            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val(),
            count = 0;
            console.log(options.dataKey);
            // Loop through the comment list
            $(options.dataKey).each(function() {
                // If the list item does not contain the text phrase fade it out
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).fadeOut();
                    // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $(this).show();
                    count++;
                }
            });
            // Update the count
            //var numberItems = count;
            //$("#filter-count").text("Number of Comments = "+count);
        });
    };
    //Search
    //Add
    $.fn.umkPlugin = function(settings) {
        var options = $.extend({
            table: null,
            className: null,
            type: null,
            dbType: null,
            pageName: null,
            formName: null,
            successMsg: null,
            errorMsg: null
        }, settings);
        return this.on('click', function(event) {
            event.preventDefault();
            if (options.formName != null) {
                if (!$(options.formName).validate().form()) {
                    return false;
                }
            }
            showLoader();
            if (options.type == 'add') {
                var arrFields = $(options.className).map(function() {
                    var $elm = $(this),
                    childId = $elm.data('key');
                    if (childId.length == 0) {
                        alert("Error");
                        return false;
                    }
                    return childId;
                }).get();
                var arrValues = $(options.className).map(function() {
                    if ($(this).val().length == 0) {
                        $(this).val('');
                    }
                    return this.value.escapeHTML();
                }).get();

                if($(".low_file").val() == "")
                {
                 showNotification('warning', 'Please upload files first.');
                 hideLoader();
                 $(options.className).val('');
                 return false;
             } else 
             {
                 arrFields.splice(1, 0, 'photoUrl');
                 arrValues.splice(1, 0, $(".low_file").val());
             } 

             $.ajax({
                type: 'POST',
                url: 'includes/processing.php',
                data: 'table=' + options.table + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&pageName=' + 'add_library' + '&command=' + options.type,
                cache: false,
                success: function(data) {
                    hideLoader();
                    var arr = data.split('~');
                    if (arr[0] == "correct") {
                        if (options.pageName == 'add_player') {
                            $(".players_data").prepend('<div class="name"><label><input data-id="'+arr[1]+'" value="'+arrValues[0]+'" class="player_checkbox" type="checkbox"> ' + arrValues[0] + '</label></div>');
                        } else if (options.pageName == 'add_event') {
                            $(".event_data").prepend('<div class="name"><label><input data-id="'+arr[1]+'" value="'+arrValues[0]+'" class="event_radio" type="radio"> ' + arrValues[0] + '</label></div>');
                        } else if (options.pageName == 'add_season') {
                            $(".season_data").prepend('<div class="name"><label><input data-id="'+arr[1]+'" value="'+arrValues[0]+'" class="season_radio" type="radio"> ' + arrValues[0] + '</label></div>');
                        } else if (options.pageName == 'add_tag') {
                            $(".tag_data").prepend('<div class="name"><label><input data-id="'+arr[1]+'" value="'+arrValues[0]+'" class="tag_checkbox" type="checkbox"> ' + arrValues[0] + '</label></div>');
                        }
                        showNotification('success', options.successMsg);
                        $(options.formName)[0].reset();
                    } else {
                        showNotification('error', options.errorMsg);
                    }
                }
            });
}
});
};
    //Add

    //Players
    var playerArray = [];
    var idArray = [];
    var flag = 0;
    $(document).on('click', '.player_checkbox', function() {
        if(flag == 0)
        {
            if($("#players").attr('data-id') != null)
            {
                idArray.push($("#players").data('id'))
            }
            flag = 1;
        }
        if($(this).is(':checked'))
        {
            playerArray.push($(this).val());
            $("#players").val(playerArray);

            idArray.push($(this).data('id'));
            $("#players").attr('data-id', idArray);
        } else 
        {
            text = $(this).val();
            pos = getPosition(playerArray, text);

            playerArray = jQuery.grep(playerArray, function(value) {
                return value != text;
            });
            
            var remove = idArray[pos];
            idArray = jQuery.grep(idArray, function(value) {
                return value != remove;
            });
            
            var newVal = removeValue($("#players").attr('data-id'), remove);
            $("#players").attr('data-id', newVal);
        }
    });
$(document).on('click', '.select_player', function() {
        //$('input#players').tagsinput('removeAll');
        for (i = 0; i < playerArray.length; i++) {
            $('input#players').tagsinput('add', playerArray[i]);
        }
        $(".players_selection input").removeAttr('placeholder');
        $("#myModal").modal('hide');
        //playerArray = [];
        //idArray = [];
    });
    //Events
    $(document).on('click', '.event_radio', function() {
        $("#event").attr('data-id', $(this).data('id'));
    });
    $(document).on('click', '.select_event', function() {
        $('input#event').tagsinput('removeAll');
        $('input#event').tagsinput('add', $(".event_radio:checked").val());
        $(".event_selection input").removeAttr('placeholder');
        $("#myModal").modal('hide');
    });
    
    //Season
    $(document).on('click', '.season_radio', function() {
        $("#season").attr('data-id', $(this).data('id'));
    });
    $(document).on('click', '.select_season', function() {
        $('input#season').tagsinput('removeAll');
        $('input#season').tagsinput('add', $(".season_radio:checked").val());
        $(".season_selection input").removeAttr('placeholder');
        $("#myModal").modal('hide');
    });
    
    $(document).on('click', 'span.tag span', function() {
        $(this).parent('span.tag').remove();
    });

    $(document).on('click', '.players_selection span.tag span', function() {
        var text = $(this).parent('span').text();
        var pos = getPosition(playerArray, text); //returns 1
        
        playerArray = jQuery.grep(playerArray, function(value) {
          return value != text;
      });
        var remove = idArray[pos];
        var newVal = removeValue($("#players").attr('data-id'), remove);
        $("#players").attr('data-id', newVal);
    });

    //Tags Check
    var tags = [];
    var tagIds = [];
    var tflag = 0;
    $(document).on('click', '.tag_checkbox', function() {
        if(tflag == 0)
        {
            /*if($("#tags").attr('data-id') != null)
            {
                tagIds.push($("#tags").data('id'));
                tags.push($("#tags").val().trim());
            }*/
            
            if($("#tags").attr('data-id') != "")
            {
            var id = $("#tags").data('id');
            var idTemp = id.split(',');
            jQuery.each(idTemp, function(index, item) {
                tagIds.push(item);
            });

            var value = $("#tags").val().trim();
            var valTemp = value.split(',');
            jQuery.each(valTemp, function(index, item) {
                tags.push(item);
            });

            }
            tflag = 1;
        }
        if($(this).is(':checked'))
        {
            tags.push($(this).val());
            $("#tags").val(tags);
            
            tagIds.push($(this).data('id'));
            $("#tags").attr('data-id', tagIds);
        } else 
        {
            text = $(this).val().trim();
            pos = getPosition(tags, text);
            
            tags = $.map(tags, $.trim);
            
            tags = jQuery.grep(tags, function(value) {
                return value != text;
            });

            var remove = tagIds[pos];
            tagIds = jQuery.grep(tagIds, function(value) {
                return value != remove;
            });
            
            var newVal = removeValue($("#tags").attr('data-id'), remove);
            $("#tags").attr('data-id', newVal);
        }
    });
$(document).on('click', '.select_tag', function() {
        $('input#tags').tagsinput('removeAll');
        for (i = 0; i < tags.length; i++) {
            $('input#tags').tagsinput('add', tags[i]);
        }
        $(".tags_selection input").removeAttr('placeholder');
        $("#myModal").modal('hide');
        //playerArray = [];
        //idArray = [];
    });

$(document).on('click', '.tags_selection span.tag span', function() {
    var text = $(this).parent('span').text();
        var pos = getPosition(tags, text); //returns 1
        
        tags = jQuery.grep(tags, function(value) {
          return value != text;
      });
        var remove = tagIds[pos];
        var newVal = removeValue($("#tags").attr('data-id'), remove);
        $("#tags").attr('data-id', newVal);
    });


$('#tags_data').on('itemAdded', function(event) {
    var tagName = event.item;
    $.ajax({
        type: 'POST',
        url: 'includes/processing.php',
        data: 'tag=' + tagName + '&command=' + 'checkTag',
        cache: false,
        success: function(data) {
                //var arr = data.split('~');
                if (data != "correct") {
                    //$("#tags_data").tagsinput('remove', tagName);
                    $("#tags_data").val('');
                    showNotification('warning', 'Tag already exists!');
                }
            }
        });
});

$(document).on('click', '.removeTag', function(event) {
    event.preventDefault();
    var id = $(this).data('id');
    var $this = $(this);
    $.ajax({
        type: 'POST',
        url: 'includes/processing.php',
        data: 'table=' + 'thesuarus' + '&db=' + 'thesuarusId' + '&data=' + id + '&pageName=' + 'deleteTags' + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                $this.parent('span').remove();
            } else 
            {
                showNotification('error', 'There is some issue, Please contact administration!')
            }
        }
    });
});

    //Tags check
    //Save Player
    $(".save_player").click(function() {

        if ($(".ori_file").val() == "" || 
        	$(".high_file").val() == "" || 
        	$(".low_file").val() == "" || 
        	!$("input[name='category']").is(':checked') || 
        	!$("input[name='type']").is(':checked') ||
        	$("#players").val() == "" || 
        	$("#event").val() == "" || 
        	$("#season").val() == "") {
            showNotification('error', 'Please fill all mandatory fields.');
        return false;
    }

    var oriImg = $(".ori_file").val(),
    highImg = $(".high_file").val(),
    lowImg = $(".low_file").val(),
    type = $(".p_type:checked").val(),
    premium = $(".premium:checked").val(),
    points = $(".points").val(),
    players = $("#players").data('id'),
    playerEvent = $("#event").data('id'),
    season = $("#season").data('id'),
    tags = $("#tags").data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: 'includes/processing.php',
        data: 'oriImg=' + oriImg + '&highImg=' + highImg + '&lowImg=' + lowImg + '&type=' + type + '&premium=' + premium + '&points=' + points + '&players=' + players + '&playerEvent=' + playerEvent + '&season=' + season + '&tags=' + tags + '&command=' + 'savePlayer',
        cache: false,
        success: function(data) {
                //alert(data);
                hideLoader();
                if(data == 'correct')
                {
                	showNotification('success', 'Player saved successfully!');
                	setTimeout(function(){
                        if(type == 1)
                        {
                            window.location.href = dir + "/players.php";
                        } else 
                        {
                            window.location.href = dir + "/teams.php";
                        }
                    }, 2000);
                }
            }
        });
});
    //Save Player

    $("[data-hide]").on("click", function(){
        $(this).closest("." + $(this).attr("data-hide")).hide();
    });


    $(document).on('click', '.cancel_player', function(){
        $("#players").attr('data-id', '');
        $('input#players').tagsinput('removeAll');
        playerArray = [];
        idArray = [];
        flag = 0;
    });
    $(document).on('click', '.cancel_event', function(){
        $("#event").attr('data-id', '');
        $('input#event').tagsinput('removeAll');
    });
    $(document).on('click', '.cancel_season', function(){
        $("#season").attr('data-id', '');
        $('input#season').tagsinput('removeAll');
    });
    $(document).on('click', '.cancel_tag', function(){
        $("#tags").attr('data-id', '');
        $('input#tags').tagsinput('removeAll');
        tags = [];
        tagIds = [];
        tflag = 0;
    });

$(".open").click(function(){
    $(".mymenus").addClass('col-md-3');
    $(".mymenus").show();
    $(".content_area").parent('div').removeClass('col-md-12');
    $(".content_area").parent('div').addClass('col-md-9');
    $(".open").hide();
});
$(".menuClose").click(function(){
    $(".mymenus").removeClass('col-md-3');
    $(".mymenus").hide();
    $(".content_area").parent('div').removeClass('col-md-9');
    $(".content_area").parent('div').addClass('col-md-12');
    $(".open").show();
});

//forget password
    $.fn.changePassword = function (table, field, oldpass, newpass, id) {
        this.click(function (event) {
            event.preventDefault();
            
            if (!$("#change_pass_form").validate({
                onkeyup: false,
                onclick: false,
                rules: {
                    password: {
                        required: true,
                    }
                }
            }).form()) {
                return false;
            }

            showLoader();
            $.ajax({
                type: 'POST',
                url: 'http://barca1.tawasoldev.com/barca_out/admin/includes/processing.php',
                data: 'table=' + table + '&field=' + field + '&oldpass=' + encodeURIComponent($(oldpass).val()) + '&id=' + id + '&newpass=' + encodeURIComponent($(newpass).val()) + '&command=' + 'changePassword',
                cache: false,
                success: function (data) {
                    hideLoader();
                    if (data == "correct") {
                        showNotification('success', 'Password changed successfully!');
                        $("#change_pass_form")[0].reset();
                        $('#changeModal').modal('hide');
                        setTimeout(function(){
                            window.location.href= 'http://barca1.tawasoldev.com/barca_out/admin/logout.php';
                        }, 1000);
                    } else if (data == "incorrect") {
                        showNotification('error', 'There is some issue, Please contact administration!');
                    } else if(data == 'oldincorrect')
                    {
                        showNotification('error', 'Old password is incorrect!')
                    }
                }
            });

        });
    };

})(jQuery);

function showLoader() {
    $("#overlay").show();
    $("#loader").show();
}

function hideLoader() {
    $("#overlay").hide();
    $("#loader").hide();
}

function showNotification(type, text) {
    if (type == 'error') {
        var notiClass = "alert-danger";
    } else if (type == 'success') {
        var notiClass = "alert-success";
    } else if (type == 'warning') {
        var notiClass = "alert-warning";
    } else if (type == 'info') {
        var notiClass = "alert-info";
    }
    $(".alert").removeClass('alert-danger');
    $(".alert").removeClass('alert-success');
    $(".alert").removeClass('alert-warning');
    $(".alert").removeClass('alert-info');
    $(".alert").addClass(notiClass);
    $(".alert strong").text(text);
    $(".alert").fadeIn().delay(3000).fadeOut();
}

function getPosition(arrayName,arrayItem) {
    for(var i=0;i<arrayName.length;i++){ 
        if(arrayName[i].trim()==arrayItem)
            return i;
    }
}
function removeValue(list, value) {
  return list.replace(new RegExp(",?" + value + ",?"), function(match) {
      var first_comma = match.charAt(0) === ',',
      second_comma;

      if (first_comma &&
          (second_comma = match.charAt(match.length - 1) === ',')) {
        return ',';
}
return '';
});
};