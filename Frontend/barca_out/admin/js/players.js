$(document).ready(function() {

    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    var fileName = loc.substring(loc.lastIndexOf('/') + 1);
    var topLoc = window.top.location.pathname;
    var qs = window.top.location.search;

    $('#selectall').click(function(event) { //on click 
        if (this.checked) { // check select status
            $('.player_photo').each(function() { //loop through each checkbox
                this.checked = true; //select all checkboxes with class "checkbox1"               
            });
        } else {
            $('.player_photo').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
    $("#Show_selected").click(function(event) {
        event.preventDefault();
        var arrValues = $(".player_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'ids=' + encodeURIComponent(arrValues) + '&command=' + 'isHiddenFalse',
            cache: false,
            success: function(data) {
                hideLoader();
                if (data == "correct") {
                    $(".aj_sec").load(""+loc+qs+" .aj_data");
                    showNotification('success', 'Process done successfully!');
                } else {
                    showNotification('error', 'There is some issue, Contact Administration!');
                }
            }
        });
    });
    $("#Hide_selected").click(function(event) {
        event.preventDefault();
        var arrValues = $(".player_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'ids=' + encodeURIComponent(arrValues) + '&command=' + 'isHiddenTrue',
            cache: false,
            success: function(data) {
                hideLoader();
                if (data == "correct") {
                    $(".aj_sec").load(""+loc+qs+" .aj_data");
                    showNotification('success', 'Process done successfully!');
                } else {
                    showNotification('error', 'There is some issue, Contact Administration!');
                }
            }
        });
    });

    $(".openUploader").click(function(event){
        event.preventDefault();
        if($(this).hasClass('showUploader'))
        {
            $(this).removeClass('showUploader');
            $(this).addClass('hideUploader');
            $(".player_img").fadeIn();
            $(".uploaders").hide();
            $(this).text('Change Picture');
        } else 
        {
            $(this).removeClass('hideUploader');
            $(this).addClass('showUploader');
            $(".player_img").hide();
            $(".uploaders").fadeIn();
            $(this).text('Cancel Upload');
        }
    });

    //Edit Player
    $(document).on('click', '.edit_player', function(){
       var img1 = $(".old_ori_file").val(),
        oriImg = $(".ori_file").val(),

        img2 = $(".old_high_file").val(),
        highImg = $(".high_file").val(),

        img3 = $(".old_low_file").val(),
        lowImg = $(".low_file").val(),

        type = $(".p_type:checked").val(),
        premium = $(".premium:checked").val(),
        players = $("#players").attr('data-id'),
        points = $(".points").val(),
        playerEvent = $("#event").attr('data-id'),
        season = $("#season").attr('data-id'),
        tags = $("#tags").attr('data-id'),
        id = $("#studioId").val();

        //alert(players);
        //alert(playerEvent);
        //alert(season);

        if( !$("input[name='category']").is(':checked') || 
            !$("input[name='type']").is(':checked') ||
            players == "" ||
            playerEvent == "" ||
            season == ""
        )
        {
            showNotification('error', 'Please fill all mandatory fields.');
            return false;
        }

        if((img1 != oriImg && oriImg != "") || (img2 != highImg && highImg != "") || (img3 != lowImg && lowImg != "") )
        {
            command = "updateFile";
        } else
        {
            command = "update";
            oriImg = img1;
            highImg = img2;
            lowImg = img3;
        }
        
        var arrValues = $(".removeTag").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        var pageId = $("#pageId").val();
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'notDel=' + arrValues + '&oriImg=' + oriImg + '&highImg=' + highImg + '&lowImg=' + lowImg + '&type=' + type + '&premium=' + premium + '&points=' + points + '&players=' + players + '&playerEvent=' + playerEvent + '&season=' + season + '&tags=' + tags + '&id=' + id + '&command=' + 'editPlayer' + '&subCommand=' + command,
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == 'correct')
                {
                    showNotification('success', 'Player updated successfully!');
                    setTimeout(function(){
                        window.location.href = dir + "/" + pageId + ".php";
                    }, 2000);
                } else 
                {
                    showNotification('error', 'There is some issue, Contact administration!');
                }
            }
        });

    });

    $(document).on('click', '.changeThumb', function(event){
        event.preventDefault();
        var id = $(this).data('id');
        var file = $(this).data('src');
        $("#thesId").val(id);
        $("#thesFile").val(file);
        showLoader();
        setTimeout(function(){
            $(".aj_data").load("aj_data.php?command=changeThumb", function(){
                $("#myModal").modal('show');
                hideLoader();
            });
        }, 1000);
    });

    $(document).on('click', '.save_thumb', function(event){
        event.preventDefault();
        var id = $("#thesId").val();
        var old = $("#thesFile").val();
        var file = $(".thes_file").val(),
            hide = $(".playerhide:checked").val();
        if(file == "")
        {
            showNotification('warning', 'Upload file first!');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'id=' + id + '&old=' + old + '&file=' + file + '&hide=' + hide + '&command=' + 'updateThumb',
            cache: false,
            success: function(data) {
                if(data == 'correct')
                {
                    $(".thes_file").val('');
                    $(".aj_sec").load(""+loc+" .aj_row");
                    $("#myModal").modal('hide');
                } else 
                {
                    showNotification('error', 'There is some issue, Contact administration!');
                }
            }
        });

    })

}); // Main

function showLoader() {
    $("#overlay").show();
    $("#loader").show();
}

function hideLoader() {
    $("#overlay").hide();
    $("#loader").hide();
}

function showNotification(type, text) {
    if (type == 'error') {
        var notiClass = "alert-danger";
    } else if (type == 'success') {
        var notiClass = "alert-success";
    } else if (type == 'warning') {
        var notiClass = "alert-warning";
    } else if (type == 'info') {
        var notiClass = "alert-info";
    }
    $(".alert").addClass(notiClass);
    $(".alert strong").text(text);
    $(".alert").fadeIn().delay(3000).fadeOut();
}