﻿<?php 
include_once ('includes/check.php');
include_once ('includes/header.php'); 
$imgUrl = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/";
$_SESSION['lib'] = 1;
$_SESSION['lib_name'] = 'players';
?>

<div class="container-fluid">
    <div class="row">

        <?php
        include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">Players</h4>
                        <div class="head_opts">
                            <a href="add_library.php" class="btn btn-primary">Add new Photo &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="aj_sec">

                        <div class="col-md-12 aj_row">
                            <?php 
                            $database->query('SELECT thesuarusId, photoUrl, name, is_hidden FROM thesuarus WHERE type = :type ORDER BY name ASC');
                            $database->bind(':type', 1);
                            $database->execute();
                            $result = $database->resultSet();
                            foreach ($result as $row) {
                                $thesuarusId = $row['thesuarusId'];
                                $database->query('SELECT spt.*, the.thesuarusId, sp.captureType FROM studiophotothesaurus spt, thesuarus the, studiophoto sp WHERE sp.captureType = 1 AND spt.studioPhotoId = sp.studioPhotoId AND spt.thesuarusId = the.thesuarusId AND the.thesuarusId = :id');
                                $database->bind(':id', $thesuarusId);
                                $database->execute();
                                $picCount = $database->rowCount();
                                ?>
                                <div class="col-md-3">
                                    <div class="photoDetail">
                                        <a href="photos.php?id=<?php echo $thesuarusId; ?>">
                                            <img src="<?php echo $imgUrl.$row['photoUrl']; ?>" class="img-responsive" alt="Player">
                                            <h4><?php echo $row['name']; ?></h4>
                                            <p><?php echo $picCount; ?> Photos</p>
                                        </a>
                                        <a href="#" data-id="<?php echo $row['thesuarusId']; ?>" data-src="<?php echo $imgUrl.$row['photoUrl']; ?>" class="changeThumb btn btn-info"><i class="fa fa-edit"></i></a>
                                        <div class="hideplayerSec">
                                            <label>Hide: <input <?php if($row['is_hidden'] == 1) {  echo 'checked'; } ?> type="checkbox" data-id="<?php echo $thesuarusId; ?>" class="hideplayer"></label>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                        </div><!-- //aj_sec-->

                    </div>

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="thesId">
    <input type="hidden" id="thesFile">
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="aj_data">
            </div>
        </div>
    </div>
</div>

<?php
include_once ('includes/footer.php'); ?>
<script type="text/javascript" src="js/players.js"></script>
<script>
    $(function(){
        $(document).on('click', '.hideplayer', function(){
            id = $(this).data('id');
            var $this = $(this);
            setTimeout(function(){
                if($this.is(':checked'))
                {
                    hide = 1;
                } else {
                    hide = 0;
                }
                showLoader();
                $.ajax({
                    type: 'POST',
                    url: 'includes/processing.php',
                    data: 'id=' + id + '&hide=' + hide + '&command=' + 'playerHide',
                    cache: false,
                    success: function(data) {
                        hideLoader();
                        if(data == 'correct')
                        {
                            showNotification('success', 'Player updated successfully!');
                        } else 
                        {
                            showNotification('error', 'There is some issue, Contact administration!');
                        }
                    }
                });
            }, 400);
        });
})    
</script>
