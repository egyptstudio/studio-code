<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 
$_SESSION['userFlag'] = 1;
if(isset($_SESSION['newUserId']))
{
    $userid = $_SESSION['newUserId'];
} else if(!isset($_REQUEST['id']) || !is_numeric($_REQUEST['id']))
{
    $userid = $_SESSION['userId'];
} else 
{
    $_SESSION['userId'] = $_REQUEST['id'];
    $userid = $_SESSION['userId'];
}

$imgUrl = "https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/users/".$userid."/";

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
    $page  = $_GET["page"];
} else { 
    $page=1; 
}

$startFrom = ($page-1) * $recPerPage;

$database->query('SELECT * FROM post WHERE userId = :id ORDER BY postId DESC');
$database->bind(':id', $userid);
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

$database->query('SELECT * FROM post WHERE userId = :id ORDER BY postId DESC LIMIT :start, :num');
$database->bind(':id', $userid);
$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records> $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else 
{
    $rowsGen = $database->rowCount();
}
if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">
            <input type="hidden" value="<?php echo $userid ?>" id="userid">
            <div class="row">
                <div class="col-md-12">
                    <?php if(!isset($_SESSION['userId'])) { ?>
                    <div style="right: auto; font-size: 20px; top: 8px;" class="head_opts">
                        <a href="add_user.php"><i class="fa fa-arrow-left"></i> Create New User</a>
                    </div>
                    <?php } else { ?>
                    <div style="right: auto; font-size: 20px; top: 8px;" class="head_opts">
                        <a href="user_detail.php?id=<?php echo $userid; ?>"><i class="fa fa-arrow-left"></i> User Detail</a>
                    </div>
                    <?php } ?>
                    <h4 class="heading">User Posts</h4>
                    
                </div>
                
                <div class="photoOpts2">
                    <div class="chkboxOpts">
                        <label> <input id="selectall" type="checkbox"> Select / Unselect All</label>
                    </div>
                    <div style="position: absolute;right: 0;top: 0; width: 35%;">
                        <form action="includes/download.php" method="post">
                            <input type="hidden" value="" name="files" id="fileNames">
                            <button style="margin-left: 5px;"  type="submit" class="btn btn-primary pull-right downloadSelectedPost">
                                Download Posts
                            </button>
                        </form>
                        <button class="btn btn-primary pull-right delSelectedPost">
                            Delete selected Posts
                        </button>
                    </div>
                </div>
                
                <div class="postSection">
                <div class="aj_sec">
                    <nav class="siteNav">
                      <ul class="pagination">
                        <li>
                          <a href="posts.php?page=1" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                          </a>
                        </li>
                        <?php /*for ($i=1; $i<=$total_pages; $i++) { 
                            if($i == $page)
                            {
                                echo "<li class='active'><a href='posts.php?page=".$i."'>".$i."</a></li> "; 
                            } else 
                            {
                                echo "<li class=''><a href='posts.php?page=".$i."'>".$i."</a></li> "; 
                            }
                        };*/
                        if($page == 1)
                        {
                            $prevPage = 1;
                        } else 
                        {
                            $prevPage = ($page - 1);
                        }

                        if($page == $total_pages)
                        {
                            $nextPage = $total_pages;
                        } else 
                        {
                            $nextPage = ($page + 1);
                        }
                        echo '<li><a href="posts.php?page='.$prevPage.'">&lt;</a></li>';
                        echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                        echo '<li><a href="posts.php?page='.$nextPage.'">&gt;</a></li>';
                        
                         ?>
                        <li>
                          <a href="posts.php?page=<?php echo $total_pages ?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                          </a>
                        </li>
                      </ul>
                    </nav>

                <div class="">
                <div class="col-md-12 aj_data">
                    <?php
                    $result = $database->resultset();
                    if( $database->rowCount() > 0 ) { 

                        foreach ($result as $row) {
                    ?>
                    <div class="col-md-2 photosCol">
                        
                        <div class="photoDetail">
                            <img src="<?php echo $imgUrl.$row['postPhotoUrl']; ?>" class="img-responsive" alt="Player">
                            <div class="team_btm">
                            <label class="pull-left">
                                <input data-id="<?php echo $row['postId'] ?>" data-file="<?php echo $imgUrl.$row['postPhotoUrl'] ?>" class="post_photo" type="checkbox"> Select
                            </label>
                            <div style="margin: 5px" class="pull-right">
                            <a href="#" class="delPost" data-id="<?php echo $row['postId'] ?>" data-file="<?php echo $row['postPhotoUrl'] ?>"><i class="fa fa-trash"></i></a>
                            </div>
                            </div>
                        </div>
                    </div>
                    <?php } 
                } else {
                    echo "<h2 style='margin-left: 20px'>Posts will be displayed here.</h2>";
                }
                    ?>
                </div>
                

                <div class="post_uploader">
                    <form class="form-horizontal myform">
                        <div class="form-group">
                            <label for="l1" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Upload user photo</h4></label>
                            <div class="col-sm-9">
                                <div class="postfile">
                                    <div id="postfile">Upload</div>
                                    <div class="uploaderStatus" id="post_file"></div>
                                </div>
                                <input type="hidden" class="post_file" value="">

                                <p>Supported formats for photos are : <strong>JPEG, PNG, JPG, GIF</strong></p>
                                <p>The maximum allowable size for photos is : <strong>500KB</strong></p>
                            </div>
                        </div>
                    </form>
                </div>
                </div><!-- aj_sec -->
                </div><!-- postSection -->
            </div>
                
            </div>
        </div>
    </div>
</div>


<?php
include_once ('includes/footer.php'); ?>
<script type="text/javascript" src="js/users.js"></script>
<script type="text/javascript">
$(function(){
    var loc = window.location.pathname;
    var qs = window.top.location.search;
    var settings = {
        url: "includes/processing.php",
        dragDrop: false,
        dragDropStr: "<span><b>Drag & Drop File</b></span>",
        multiple: false,
        fileName: "myfile",
        formData: {
            "command": "uploadPost",
            "dir": "third_party/uploads/users/<?php echo $userid; ?>/",
            "thumb": "No"
        },
        allowedTypes: "jpg,png,jpeg",
        returnType: "json",
        showFileCounter: false,
        showDone: false,
        maxFileCount: 1,
        maxFileSize:1024*500,
        onSuccess: function(files, data, xhr) {
            //alert(data);
            var arrFields = ["userId", "studioPhotoId", "postPhotoUrl"];
            var arrValues = ["<?php echo $userid; ?>", "0", data];
            $.ajax({
                type: 'POST',
                url: 'includes/processing.php',
                data: 'table=' + 'post' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&pageName=' + 'add_post' + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    if(data == 'correct')
                    {
                        /*setTimeout(function(){
                            <?php if(!isset($_SESSION['userId'])) { ?>
                           window.location.href = 'posts.php?page=<?php echo $page; ?>' 
                            <?php } else { ?>
                            window.location.href = 'posts.php?id=<?php echo $_REQUEST["id"] ?>&page=<?php echo $page; ?>' 
                            <?php } ?> 
                        },2000);*/
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            $("#postfile").uploadFile(settings);
                        });

                        showNotification('success', 'Process done successfully!');
                        //$(".ajax-file-upload-statusbar").remove();
                        
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        },
        onSubmit: function(files) {
            //alert(files);
            setTimeout(function() {
                $("#loader, #overlay").hide();
            }, 100);
        },
        showDelete: true,
        deleteCallback: function(data, pd) {
            for (var i = 0; i < data.length; i++) {
                $.post("includes/processing.php", {
                    op: "delete",
                    dir: "third_party/uploads/users/<?php echo $userid; ?>/",
                    command: "deleteData",
                    name: data[i]
                }, function(resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#post_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
            }
            pd.statusbar.hide(); //You choice to hide/not.
        }
    }
    var uploadObj = $("#postfile").uploadFile(settings);
    //Delete Post
    $(document).on('click', '.delPost', function(event){
        event.preventDefault();
        var id = $(this).attr('data-id');
        var file = $(this).attr('data-file');
        if(!confirm("Are your sure you want to delete these post?")) return false;
        showLoader();
        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'id=' + id + '&file=' + file + '&userid=' + $("#userid").val() + '&command=' + 'delPost',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Post deleted successfully!");
                    $(".postSection").load('posts.php .aj_sec', function(){
                        $("#postfile").uploadFile(settings);
                    });
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });

    //Delete selected Post
    $(".delSelectedPost").click(function(event){
        event.preventDefault();
        if($(".post_photo:checked").val() == null)
        {
            showNotification('error', 'Please select post first!');
            return false;
        }
        if(!confirm("Are your sure you want to delete these posts?")) return false;
        var arrIds = $(".post_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('id');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();

        var arrFiles = $(".post_photo:checked").map(function() {
            var $elm = $(this),
                childId = $elm.data('file');
            if (childId.length == 0) {
                alert("Error");
                return false;
            }
            return childId;
        }).get();
        
        showLoader();

        $.ajax({
            type: 'POST',
            url: 'includes/processing.php',
            data: 'userid=' + $("#userid").val() + '&ids=' + arrIds + '&files=' + arrFiles + '&command=' + 'delSelectedPost',
            cache: false,
            success: function(data) {
                hideLoader();
                if(data == "correct")
                {
                    showNotification('success', "Post(s) deleted successfully!");
                    $(".postSection").load('posts.php .aj_sec', function(){
                        $("#postfile").uploadFile(settings);
                    });
                } else 
                {
                    showNotification('error', "There is some issue, Contact administration!");
                }
            }
        });
    });
    var imgArray = [];
    $(".post_photo").click(function(){
        
        if($(this).is(':checked'))
        {
            imgArray.push($(this).attr('data-file'));
            $("#fileNames").val(imgArray);
            /*var value = $(this).find('input').attr('data-file');
            var input = $('#fileNames');
            input.val(input.val() + value + ', ');
            return false;*/
        } else 
        {
            text = $(this).attr('data-file');
            pos = getPosition(imgArray, text);
            alert(pos);
            imgArray = jQuery.grep(imgArray, function(value) {
                return value != text;
            });
             $("#fileNames").val(imgArray);
        }
    });

});
</script>