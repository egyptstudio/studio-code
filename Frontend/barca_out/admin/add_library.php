<?php
include_once ('includes/check.php');
include_once ('includes/header.php'); 
$imgPath = "third_party/uploads/";
$_SESSION['lib'] = 1;
?>

<div class="container-fluid">
    <div class="row">

        <?php
        include_once ('includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="heading">Add new Photo</h4>
                        <div class="head_opts">
                            <a href="#" class="btn btn-default btn-black save_player">Save</a>
                            <a href="players.php" class="btn btn-default btn-black">Cancel</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form class="form-horizontal myform">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Are Mandatory fields</h4></label>
                            </div>
                            <div class="form-group">
                                <label for="l1" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Orignal Raw Image</h4></label>
                                <div class="col-sm-6">
                                    <div class="original">
                                        <div id="original">Upload</div>
                                        <div class="uploaderStatus" id="ori_file"></div>
                                    </div>
                                    <input type="hidden" class="ori_file" value="">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="l2" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Processed image in high resolution</h4></label>
                                <div class="col-sm-6">
                                    <div class="high">
                                        <div id="high">Upload</div>
                                        <div class="uploaderStatus" id="high_file"></div>
                                    </div>
                                    <input type="hidden" class="high_file" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="l3" class="col-sm-5 control-label"><h4 class="tb_title"><span>*</span>Processed image in low mobile resolution</h4></label>
                                <div class="col-sm-6">
                                    <div class="low">
                                        <div id="low">Upload</div>
                                        <div class="uploaderStatus" id="low_file"></div>
                                    </div>
                                    <input type="hidden" class="low_file" value="">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l4" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Photo Type</h4></label>
                                <div class="col-sm-8">
                                    <label class="pull-left"><input value="1" type="radio" name="category" class="form-control p_type" id="l4">Player</label>
                                    <label class="pull-right"><input value="2" type="radio" name="category" class="form-control p_type">Team</label>
                                </div>
                            </div>
                            <div class="form-group border_top">
                                <label for="l5" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Premium</h4></label>
                                <div class="col-sm-8">
                                    <label class="pull-left"><input value="1" type="radio" name="type" class="form-control premium" id="l5">Yes</label>
                                    <label class="pull-right"><input checked="checked" value="0" type="radio" name="type" class="form-control premium">No</label>
                                </div>
                            </div>
                            <div class="form-group border_top">
                                <label for="l11" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Points</h4></label>
                                <div class="col-sm-8">
                                    <input type="number" name="points" class="form-control points">
                                </div>
                            </div>

                            <div class="form-group border_top">
                                <label for="l6" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Players</h4></label>
                                <div class="input-group col-sm-8 players_selection">
                                  <input data-role="tagsinput" id="players" disabled="disabled" type="text" class="form-control" name="players" placeholder="Select Players" aria-describedby="basic-addon1">
                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-plus"></i></span>
                              </div>
                          </div>
                          <div class="form-group border_top">
                            <label for="l7" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Event</h4></label>
                            <div class="input-group col-sm-8 event_selection">
                              <input data-role="tagsinput" id="event" disabled="disabled" type="text" name="events" class="form-control" placeholder="Select Event" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="fa fa-plus"></i></span>
                          </div>
                      </div>
                      <div class="form-group border_top">
                        <label for="l8" class="col-sm-3 control-label"><h4 class="tb_title"><span>*</span>Season</h4></label>
                        <div class="input-group col-sm-8 season_selection">
                          <input data-role="tagsinput" id="season" disabled="disabled" type="text" name="season" class="form-control" placeholder="Select Season" aria-describedby="basic-addon3">
                          <span class="input-group-addon" id="basic-addon3"><i class="fa fa-plus"></i></span>
                      </div>
                  </div>
                  
                  <div class="form-group border_top">
                    <label for="l10" class="col-sm-3 control-label"><h4 class="tb_title">Tags</h4></label>
                    <div class="input-group col-sm-8 tags_selection">
                      <input data-role="tagsinput" id="tags" data-id="" disabled="disabled" type="text" name="tags" class="form-control" placeholder="Select Tag" aria-describedby="basic-addon3">
                      <span class="input-group-addon" id="basic-addon4"><i class="fa fa-plus"></i></span>
                  </div>
                  <div class="col-sm-3"></div>                       
                  <div class="col-sm-8 def_tags">  
                    <span class="tag label label-primary">Playername</span>
                    <span class="tag label label-primary">Event</span>
                    <span class="tag label label-primary">Season</span>
                </div>
                            <!--<div class="input-group col-sm-8">
                              <input data-role="tagsinput" id="tags_data" name="tags" type="text" class="form-control" placeholder="Write your tag here" aria-describedby="basic-addon4">
                                <div class="def_tags">  
                                    <span class="tag label label-primary">Playername</span>
                                    <span class="tag label label-primary">Event</span>
                                    <span class="tag label label-primary">Season</span>
                                </div>
                            </div>-->
                            
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="aj_data">
      </div>
  </div>
</div>
</div>

<?php
include_once ('includes/footer.php'); 
//echo $_SERVER['DOCUMENT_ROOT'] . $oriImg;
?>
<script>
    $(document).ready(function(){
        var settings1 = {
            url: "includes/processing.php",
            dragDrop: false,
            dragDropStr: "<span><b>Drag & Drop File</b></span>",
            multiple: false,
            fileName: "myfile",
            formData: {"command":"uploadData", "dir" : "<?php echo $imgPath; ?>", "thumb" : "No"},
            allowedTypes: "jpg,png,jpeg",
            returnType: "json",
            showFileCounter: false,
            showDone: false,
            maxFileCount:1,
            onSuccess: function (files, data, xhr) {
         //alert(data);
         //$(id_ori+" .ajax-file-upload form:first").remove();
         $(".ori_file").val(data);
     },
     onSubmit:function(files)
     {
        //alert(files);
        setTimeout(function(){
            $("#loader, #overlay").hide();
        }, 100);
    },
    showDelete: true,
    deleteCallback: function (data, pd) {
        for (var i = 0; i < data.length; i++) {
            $.post("includes/processing.php", {
                op: "delete",
                dir: "<?php echo $imgPath; ?>",
                command: "deleteData",
                name: data[i]
            },
            function (resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#ori_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
        }
        pd.statusbar.hide(); //You choice to hide/not.

    }
}

var uploadObj = $("#original").uploadFile(settings1);


var settings2 = {
    url: "includes/processing.php",
    dragDrop: false,
    dragDropStr: "<span><b>Drag & Drop File</b></span>",
    multiple: false,
    fileName: "myfile",
    formData: {"command":"uploadData", "dir" : "<?php echo $imgPath; ?>", "thumb" : "No"},
    allowedTypes: "jpg,png,jpeg",
    returnType: "json",
    showFileCounter: false,
    showDone: false,
    maxFileCount:1,
    onSuccess: function (files, data, xhr) {
         //alert(data);
         //$(id_ori+" .ajax-file-upload form:first").remove();
         $(".high_file").val(data);
     },
     onSubmit:function(files)
     {
        //alert(files);
        setTimeout(function(){
            $("#loader, #overlay").hide();
        }, 100);
    },
    showDelete: true,
    deleteCallback: function (data, pd) {
        for (var i = 0; i < data.length; i++) {
            $.post("includes/processing.php", {
                op: "delete",
                dir: "<?php echo $imgPath; ?>",
                command: "deleteData",
                name: data[i]
            },
            function (resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#high_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
        }
        pd.statusbar.hide(); //You choice to hide/not.

    }
}

var uploadObj = $("#high").uploadFile(settings2);

var settings3 = {
    url: "includes/processing.php",
    dragDrop: false,
    dragDropStr: "<span><b>Drag & Drop File</b></span>",
    multiple: false,
    fileName: "myfile",
    formData: {"command":"uploadData", "dir" : "<?php echo $imgPath; ?>", "thumb" : "No"},
    allowedTypes: "jpg,png,jpeg",
    returnType: "json",
    showFileCounter: false,
    showDone: false,
    maxFileCount:1,
    onSuccess: function (files, data, xhr) {
         //alert(data);
         //$(id_ori+" .ajax-file-upload form:first").remove();
         $(".low_file").val(data);
     },
     onSubmit:function(files)
     {
        //alert(files);
        setTimeout(function(){
            $("#loader, #overlay").hide();
        }, 100);
    },
    showDelete: true,
    deleteCallback: function (data, pd) {
        for (var i = 0; i < data.length; i++) {
            $.post("includes/processing.php", {
                op: "delete",
                dir: "<?php echo $imgPath; ?>",
                command: "deleteData",
                name: data[i]
            },
            function (resp, textStatus, jqXHR) {
                    //alert(resp);
                    $("#low_file").append("<div>File Deleted</div>").delay('5000').fadeOut();
                });
        }
        pd.statusbar.hide(); //You choice to hide/not.

    }
}

var uploadObj = $("#low").uploadFile(settings3);


$("#basic-addon1").click(function(){
    showLoader();
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getPlayers", function(){
            $("#myModal").modal('show');
            hideLoader();
        });
    }, 1000);
});

$("#basic-addon2").click(function(){
    showLoader();
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getEvents", function(){
            $("#myModal").modal('show');
            hideLoader();
        });
    }, 1000);
});

$("#basic-addon3").click(function(){
    showLoader();
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getSeason", function(){
            $("#myModal").modal('show');
            hideLoader();
        });
    }, 1000);
});

$("#basic-addon4").click(function(){
    showLoader();
    setTimeout(function(){
        $(".aj_data").load("aj_data.php?command=getTags", function(){
            $("#myModal").modal('show');
            hideLoader();
        });
    }, 1000);
});

});// main

</script>
