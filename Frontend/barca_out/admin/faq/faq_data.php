<?php
//error_reporting(E_ALL | E_WARNING | E_NOTICE);
//ini_set('display_errors', TRUE);
include_once ('../includes/database.class.php');
$command = $_REQUEST['command'];
$db = new Database();
if ($command == 'getFaqCatTranslation') { 
	$pid = $_REQUEST['id'];
	
	?>

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Translation</h4>
	</div>
	<div class="modal-body">
		<form class="form-inline langform" id="langform">
			<div class="form-group" style="width: 24%">
				<select style="width: 95%" class="form-control getValue" data-key="lang">
					<?php 
					$db->query("SELECT * FROM languages WHERE name != 'en'");
					$db->execute();
					$result = $db->resultset();
					foreach ($result as $langRow) {
						echo '<option value="'.$langRow['name'].'">'.$langRow['name'].'</option>';
					}
					?>
				</select>
			</div>
			<div class="form-group" style="width: 64%">
				<input style="width: 95%" type="text" class="form-control getValue" data-key="cattransTxt" required>
				<input type="hidden" value="<?php echo $pid; ?>" class="getValue catPid" data-key="catPid">
			</div>

			<button type="submit" class="btn btn-default btn-black add-lang">Add</button>
		</form>
		<br>
		<div class="translations">
			<div class="langtable">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Language</th>
							<th>Translation</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$db->query('SELECT * FROM cat_trans WHERE catPid = :id ORDER BY catId ASC');
						$db->bind('id', $pid);
						$db->execute();
						$result = $db->resultset();
						foreach ($result as $row) {
							$lang = $row['lang'];
							if(is_numeric($lang))
							{
								$db->query('SELECT * FROM languages WHERE id = :langId');
								$db->bind(":langId", $lang);
								$db->execute();
								$langRow = $db->single();
								$langText = $langRow['name'];
							} else {
								$langText = $lang;
							}
							?>
							<tr>
								<td>
									<span><?php echo $langText; ?></span>
								</td>
								<td>
									<span><?php echo $row['cattransTxt']; ?></span>
								</td>
								<td style="text-align:center">
									<a href="javascript:;" class="del-lang btn btn-danger" data-id="<?php echo $row['catId']; ?>"><i class="fa fa-trash"></i> Delete</a>
								</td>

							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>

		<?php } else if($command == 'getFaqQuesTranslation') {
			$pid = $_REQUEST['id'];
			?>

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Translation</h4>
			</div>
			<div class="modal-body">
				<form class="form-inline langform" id="langform">
					<div class="form-group" style="width: 24%">
						<select style="width: 95%" class="form-control getValue" data-key="lang">
							<?php 
							$db->query("SELECT * FROM languages WHERE name != 'en'");
							$db->execute();
							$result = $db->resultset();
							foreach ($result as $langRow) {
								echo '<option value="'.$langRow['name'].'">'.$langRow['name'].'</option>';
							}
							?>
						</select>
					</div>
					<div class="form-group" style="width: 64%">
						<input style="width: 95%" type="text" class="form-control getValue" data-key="faq_qtransTxt" required>
						<input type="hidden" value="<?php echo $pid; ?>" class="getValue qPid" data-key="qPid">
					</div>

					<button type="submit" class="btn btn-default btn-black add-lang">Add</button>
				</form>
				<br>
				<div class="translations">
					<div class="langtable">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Language</th>
									<th>Translation</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$db->query('SELECT * FROM faq_qtrans WHERE qPid = :id ORDER BY qtransId DESC');
								$db->bind('id', $pid);
								$db->execute();
								$result = $db->resultset();
								foreach ($result as $row) {
									$lang = $row['lang'];
									if(is_numeric($lang))
									{
										$db->query('SELECT * FROM languages WHERE id = :langId');
										$db->bind(":langId", $lang);
										$db->execute();
										$langRow = $db->single();
										$langText = $langRow['name'];
									} else {
										$langText = $lang;
									}
									?>
									<tr>
										<td>
											<span><?php echo $langText; ?></span>
										</td>
										<td>
											<span><?php echo $row['faq_qtransTxt']; ?></span>
										</td>
										<td style="text-align:center">
											<a href="javascript:;" class="del-lang btn btn-danger" data-id="<?php echo $row['qtransId']; ?>"><i class="fa fa-trash"></i> Delete</a>
										</td>

									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<?php } else if($command == 'getFaqAns') { 
					$id = $_REQUEST['id'];
					$db->query('SELECT * FROM faq_a WHERE answerId = :id');
					$db->bind('id', $id);
					$db->execute();
					$rows = $db->single();
					$question = $rows['qPid'];
					$language = $rows['lang'];
					?>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Edit Entry</h4>
					</div>

					<form class="form-horizontal myeditform">

						<div class="modal-body">
							<div class="form-group">
								<label for="l2" class="col-sm-3 control-label"><h4 class="tb_title">Language</h4></label>
								<div class="col-sm-9">
									<select class="form-control langDropDown getValue" data-key='lang'>
										<?php 
										$db->query("SELECT * FROM languages");
										$db->execute();
										$result = $db->resultset();
										foreach ($result as $langRow) {
											if($langRow['name'] == $language)
											{
												echo '<option selected value="'.$langRow['name'].'">'.$langRow['name'].'</option>';
											} else {
												echo '<option value="'.$langRow['name'].'">'.$langRow['name'].'</option>';
											}
										}
										?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="l2" class="col-sm-3 control-label"><h4 class="tb_title">Question</h4></label>
								<div class="col-sm-9">
									<select class="form-control quesDropDown getValue" data-key='qPid'>
										<?php 
										if($language == 'en')
										{
											$db->query('SELECT qTxt, qPid FROM faq_q ORDER BY qPid ASC');
											$field = 'qTxt';
										} else {
											$db->query('SELECT faq_qtransTxt, qPid FROM faq_qtrans WHERE lang = :lang ORDER BY qPid ASC');
											$db->bind(":lang", $language);
											$field = 'faq_qtransTxt';
										}
										
										$db->execute();
										$result = $db->resultset();
										foreach ($result as $row) {
											if($question == $row['qPid'])
											{
												echo '<option selected value="'.$row['qPid'].'">'.$row[$field].'</option>';
											} else {
												echo '<option value="'.$row['qPid'].'">'.$row[$field].'</option>';
											}
										} ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="l2" class="col-sm-3 control-label"><h4 class="tb_title">Answer</h4></label>
								<div class="col-sm-9">
									<textarea class="editor2 form-control" name="editor2">
										<?php echo $rows['answersTxt']; ?>
									</textarea>
								</div>
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary edit_entry" data-id="<?php echo $id; ?>">Save</button>
						</div>

						<?php } else if($command == 'getFaqQues') { 
							$id = $_REQUEST['id'];
							$db->query('SELECT * FROM faq_q WHERE qPid = :id');
							$db->bind(":id", $id);
							$db->execute();
							$row = $db->single();
							?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Edit Entry</h4>
							</div>

							<form class="form-horizontal myeditform">

								<div class="modal-body">
									<div class="form-group">
										<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Category</h4></label>
										<div class="col-sm-8">
											<input type="hidden" value="en" class="getValue form-control" data-key="lang">
											<select class="form-control getValue" data-key='catPid'>
												<?php 
												$db->query('SELECT catTxt, pid FROM faq_cat ORDER BY pid ASC');
												$db->execute();
												$result = $db->resultset();
												foreach ($result as $catRow) {
													if($catRow['pid'] == $row['catPid'])
													{
														echo '<option selected value="'.$catRow['pid'].'">'.$catRow['catTxt'].'</option>';
													} else {
														echo '<option value="'.$catRow['pid'].'">'.$catRow['catTxt'].'</option>';
													}
												} ?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title">Question</h4></label>
										<div class="col-sm-8">
											<input type="type" value="<?php echo $row['qTxt'] ?>" class="getValue required form-control" data-key="qTxt">
										</div>
									</div>

								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="submit" data-id="<?php echo $id; ?>" class="btn btn-primary edit_entry">Save</button>
								</div>
							</form>
							<?php } else if($command == 'getTransQues') { 
								$lang = $_REQUEST['lang'];
								if($lang == 'en')
								{	
									$db->query('SELECT * FROM faq_q WHERE lang = :lang');
									$field = 'qTxt';
								} else 
								{
									$db->query('SELECT * FROM faq_qtrans WHERE lang = :lang');
									$field = 'faq_qtransTxt';
								}
								$db->bind(":lang", $lang);
								$db->execute();
								$result = $db->resultset();
								foreach ($result as $row) {
									echo '<option value="'.$row['qPid'].'">'.$row[$field].'</option>';
								}
							?>
		
							<?php } ?>