<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php');
$_SESSION['lib'] = 27;
$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page']))
{
    $page  = $_GET["page"];
} else {
    $page=1;
}
$startFrom = ($page-1) * $recPerPage;
if(isset($_REQUEST['q']) || !empty($_REQUEST['q']))
{
    $q = $_REQUEST['q'];
    $database->query('SELECT * FROM faq_q, faq_a WHERE faq_a.qPid = faq_q.qPid AND (answersTxt like :q OR qTxt like :q) ORDER BY faq_a.answerId DESC');
    $database->bind(':q', '%'.$q.'%');
} else {
    $database->query('SELECT * FROM faq_q, faq_a WHERE faq_a.qPid = faq_q.qPid ORDER BY faq_a.answerId DESC');
}
$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage);

if(isset($_REQUEST['q']) || !empty($_REQUEST['q']))
{
    //echo $_REQUEST['q'];
    $q = $_REQUEST['q'];
    $database->query('SELECT * FROM faq_q, faq_a WHERE faq_a.qPid = faq_q.qPid AND (answersTxt like :q OR qTxt like :q) ORDER BY faq_a.answerId DESC LIMIT :start, :num');
    $database->bind(':q', '%'.$q.'%');
} else {
    $database->query('SELECT * FROM faq_q, faq_a WHERE faq_a.qPid = faq_q.qPid ORDER BY faq_a.answerId DESC LIMIT :start, :num');
}
$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();
if($total_records > $recPerPage)
{
    $rowsGen = ($recPerPage + $startFrom);
} else
{
    $rowsGen = $database->rowCount();
}
if($rowsGen > $total_records)
{
    $rowsGen = $total_records;
}

?>
<div class="container-fluid">
    <div class="row">
        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">Answers</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_entry btn-primary">Add New Entry &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>
                            <div class="opts3">
                                <nav class="siteNav pull-left">
                                    <ul class="pagination">
                                        <li>
                                            <a href="answers.php?page=1" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                        <?php
                                        if($page == 1)
                                        {
                                            $prevPage = 1;
                                        } else
                                        {
                                            $prevPage = ($page - 1);
                                        }
                                        if($page == $total_pages)
                                        {
                                            $nextPage = $total_pages;
                                        } else
                                        {
                                            $nextPage = ($page + 1);
                                        }
                                        echo '<li><a href="answers.php?page='.$prevPage.'">&lt;</a></li>';
                                        echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
                                        echo '<li><a href="answers.php?page='.$nextPage.'">&gt;</a></li>';
                                        ?>
                                        <li>
                                            <a href="answers.php?page=<?php echo $total_pages ?>" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                                <form class="form-inline search-form" action="">
                                    <div class="form-group">
                                        <input type="search" name="q" class="form-control" id="search" placeholder="Search">
                                    </div>
                                    <button type="submit" class="btn btn-default">Search</button>
                                </form>
                            </div>
                            
                            <div class="col-md-12">
                                <table class="table table-bordered userTable">
                                    <thead>
                                        <tr>
                                            <th>Language</th>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $database->resultset();
                                        foreach ($result as $row) {
                                            $lang = $row['lang'];
                                            if(is_numeric($lang))
                                            {
                                                $database->query('SELECT * FROM languages WHERE id = :langId');
                                                $database->bind(":langId", $lang);
                                                $database->execute();
                                                $langRow = $database->single();
                                                $langText = $langRow['name'];
                                            } else {
                                                $langText = $lang;
                                            }

                                            if($langText != 'en')
                                            {
                                                $qPid = $row['qPid'];
                                                $database->query('SELECT * FROM faq_qtrans WHERE qPid = :id AND lang = :lang');
                                                $database->bind(":id", $qPid);
                                                $database->bind(":lang", $langText);
                                                $database->execute();
                                                $quesRow = $database->single();
                                                $quesText = $quesRow['faq_qtransTxt'];
                                            } else 
                                            {
                                                $quesText = $row['qTxt'];
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <span><?php echo $langText; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $quesText; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo html_entity_decode($row['answersTxt']); ?></span>
                                                </td>
                                                <td style="text-align:center">
                                                    <a href="javascript:;" class="btn-edit btn btn-info" data-id="<?php echo $row['answerId']; ?>"><i class="fa fa-info"></i> Edit</a>
                                                    <a href="javascript:;" class="del_entry btn btn-danger" data-id="<?php echo $row['answerId']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                                </td>

                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } else {
                                    echo "<h2 style='margin-left: 20px'>List of Answers will be displayed here.</h2>";
                                } ?>

                            </div><!-- aj_sec -->
                        </div><!-- postSection -->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Entry</h4>
                </div>

                <form class="form-horizontal myform">

                    <div class="modal-body">
                       <div class="form-group">
                        <label for="l2" class="col-sm-3 control-label"><h4 class="tb_title">Language</h4></label>
                        <div class="col-sm-9">
                            <select class="form-control langDropDown getValue" data-key='lang'>
                                <?php 
                                $database->query("SELECT * FROM languages");
                                $database->execute();
                                $result = $database->resultset();
                                foreach ($result as $langRow) {
                                    echo '<option value="'.$langRow['name'].'">'.$langRow['name'].'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="l2" class="col-sm-3 control-label"><h4 class="tb_title">Question</h4></label>
                        <div class="col-sm-9">
                            <select class="form-control quesDropDown getValue" data-key='qPid'>
                                <?php 
                                $database->query('SELECT qTxt, qPid FROM faq_q ORDER BY qPid ASC');
                                $database->execute();
                                $result = $database->resultset();
                                foreach ($result as $row) {
                                    echo '<option value="'.$row['qPid'].'">'.$row['qTxt'].'</option>';
                                } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="l2" class="col-sm-3 control-label"><h4 class="tb_title">Answer</h4></label>
                        <div class="col-sm-9">
                            <textarea class="editor1 form-control" name="editor1"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save_entry">Save</button>
                </div>
            </form>

        </div>
    </div>
</div>
<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="aj_data"></div>
        </div>
    </div>
</div>
<?php
include_once ('../includes/footer.php');  ?>
<script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;

        var roxyFileman = '../plugins/ckeditor/fileman/index.html'; 
        CKEDITOR.replace( 'editor1',{filebrowserBrowseUrl:roxyFileman,
            filebrowserImageBrowseUrl:roxyFileman+'?type=image',
            removeDialogTabs: 'link:upload;image:upload'}); 

        $(".add_entry").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("faq_data.php?command=getFaqAns&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    var roxyFileman = '../plugins/ckeditor/fileman/index.html'; 
                    CKEDITOR.replace( 'editor2',{filebrowserBrowseUrl:roxyFileman,
                        filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                        removeDialogTabs: 'link:upload;image:upload'}); 

                    hideLoader();
                });
            }, 300);
        });
        
        $(document).on('click', '.save_entry', function(e){
            e.preventDefault();
            if (!$(".myform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();

            var textareaData = CKEDITOR.instances.editor1.getData();
            arrFields.splice(1, 0, 'answersTxt');
            arrValues.splice(1, 0, textareaData.escapeHTML());

            showLoader();
            $.ajax({
                type: 'POST',
                url: '../includes/shop_processing.php',
                data: 'table=' + 'faq_a' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    if(data == 'correct')
                    {
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            hideLoader();
                            $("#myModal").modal('hide');
                            showNotification('success', 'Process done successfully!');
                            $(".myform")[0].reset();    
                        });
                    } else
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
        });

$(document).on('click', '.del_entry', function(){
    if(!confirm('Are you sure, you want to delete data?')) return false;
    db = "answerId";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'faq_a' + '&db=' + db + '&data=' + value + '&pageName=' + 'answers' + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                    hideLoader();    
                    showNotification('success', 'Process done successfully!');
                });
            } else
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('click', '.edit_entry', function(e){
    e.preventDefault();
    if (!$(".myeditform").validate().form()) {
        return false;
    }
    var arrFields = $(".myeditform .getValue").map(function() {
        var $elm = $(this),
        childId = $elm.data('key');
        if (childId.length == 0) {
            alert("Error");
            return false;
        }
        return childId;
    }).get();
    var arrValues = $(".myeditform .getValue").map(function() {
        if ($(this).val().length == 0) {
            $(this).val('');
        }
        return this.value.escapeHTML();
    }).get();

    var textareaData = CKEDITOR.instances.editor2.getData();
    arrFields.splice(1, 0, 'answersTxt');
    arrValues.splice(1, 0, textareaData.escapeHTML());


    var key = "answerId";
    var value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/shop_processing.php',
        data: 'table=' + 'faq_a' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + key + '&value=' + value + '&command=' + 'update',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                    hideLoader();
                    $("#myEditModal").modal('hide');    
                    showNotification('success', 'Process done successfully!');
                });
                
            } else
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('change', '.langDropDown', function(e){
    id = $(this).val();
    $.ajax({
        type: 'POST',
        url: 'faq_data.php',
        data: 'lang=' + id + '&command=' + 'getTransQues',
        cache: false,
        success: function(data) {
            $(".quesDropDown").html(data);
        }
    });
});

});
</script>