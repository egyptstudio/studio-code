<?php
	$deviceToken = 'f9fc799e64e1b43e4f27edeab7824f22d0eca1e0a59cc0a7a8ea82320bea6227';
    $message = 'Testing';

    $passphrase = "tawasolit";
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'fcb.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
    $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        //$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

    if (!$fp) 
        exit("Failed to connect: $err $errstr" . PHP_EOL);

    echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
    $body['aps'] = array('alert' => array('body' => $message, 'action-loc-key' => 'Bango App', ), 'badge' => 2, 'sound' => 'oven.caf', );



    $payload = json_encode($body,JSON_UNESCAPED_UNICODE);
    
    
    //print_r($payload);
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

    $result = fwrite($fp, $msg, strlen($msg));

    if (!$result) echo 'Message not delivered' . PHP_EOL;
    else echo 'Message successfully delivered' . PHP_EOL . '<br>';

    fclose($fp);
?>