<?php
include_once ('../includes/database.class.php');
include_once ('../includes/defaults.php');
$command = $_REQUEST['command'];
$db = new Database();

if ($command == 'getPrivileges' || $command == 'getPrivilegesEdit') { ?>
<style type="text/css">
	.privileges_data span{
		display: block;
		text-transform: uppercase;
		font-weight: bold;
		margin-bottom: 12px;
	}
	.privileges_data
	{
		margin-bottom: 20px
	}
</style>
<div class="modal-header">
	<button type="button" class="close <?php if($command == 'getPrivileges') { echo 'cancel_privileges'; } ?>" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">Select Privileges</h4>
</div>
<div class="modal-body">
	<div class="players">
		<?php foreach ($menuArray as $key => $value) {
			echo '<div class="privileges_data col-md-6"><span>'.$key.'</span>';
			foreach ($value as $key => $value) {
				echo '<label><input class="privilege_checkbox" data-id="'.$key.'" value="'.$value.'" type="checkbox"> '.$value.'</label><br>';
			}
				//echo '<div class="players_data col-md-6"><label><input class="privilages_checkbox" data-id="'.$key.'" value="" type="checkbox"> '.$value.'</label></div>';	
			echo '</div>';
		} ?>
	</div>

</div>
<div class="modal-footer">
	<button type="button" class="btn btn-primary select_privileges">Select</button>
	<?php if($command == 'getPrivilegesEdit') { ?>
	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	<?php } else { ?>
	<button type="button" class="btn btn-default cancel_privileges" data-dismiss="modal">Cancel</button>
	<?php } ?>
</div>
<?php } else if($command == 'getUser') { 
	$id = $_REQUEST['id'];
	$db->query('SELECT * FROM admin WHERE id = :id');
	$db->bind(":id", $id);
	$db->execute();
	$row = $db->single();
	$priArr = explode("-", $row['privileges']);
	$menuIds = implode(",", $priArr);
	?>

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Edit User</h4>
	</div>
	<form class="form-horizontal myeditform">

		<div class="modal-body">

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Full Name</h4></label>
				<div class="col-sm-8">
					<input type="text" name="1" value="<?php echo $row['fullname']; ?>" class="getValue form-control required" data-key="fullname">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> User Name</h4></label>
				<div class="col-sm-8">
					<input type="text" name="2" value="<?php echo $row['username']; ?>" class="getValue form-control required" data-key="username">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Password</h4></label>
				<div class="col-sm-8">
					<input type="text" name="password" value="<?php echo $row['passtext']; ?>" id="password" class="getValue form-control required" data-key="password">
				</div>
			</div>

			<div class="form-group">
				<label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Email</h4></label>
				<div class="col-sm-8">
					<input type="email" name="email" value="<?php echo $row['email']; ?>" id="password" class="getValue form-control required" data-key="email">
				</div>
			</div>

			<div class="form-group">
				<label for="l10" class="col-sm-4 control-label"><h4 class="tb_title">Privileges</h4></label>
				<div style="padding-left: 15px; padding-right: 15px" class="input-group col-sm-8 privileges_selection">
					<input data-role="tagsinput" id="privileges1" data-id="<?php echo $menuIds; ?>" value="<?php for ($i=0; $i <count($priArr) ; $i++) { 
						if($i == (count($priArr) - 1))
						{
							findKey($menuArray, $priArr[$i]);
						} else {
							echo findKey($menuArray, $priArr[$i]).", ";
						}
					} ?>" disabled="disabled" type="text" name="privileges1" class="form-control" placeholder="Select Privilages" aria-describedby="privileges-addon0">
					<span class="input-group-addon" id="privileges-addon1"><i class="fa fa-plus"></i></span>
				</div>

			</div>

		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="submit" data-id="<?php echo $id; ?>" class="btn btn-primary edit_user">Save</button>
		</div>
	</form>

	<?php } ?>