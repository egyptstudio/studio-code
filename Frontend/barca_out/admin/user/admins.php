<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$_SESSION['lib'] = 30;
$database->query('SELECT * FROM admin WHERE id != 1 ORDER BY id DESC');
$database->execute();
$total_records = $database->rowCount();
?>

<div class="container-fluid">
    <div class="row">

        <?php include_once ('../includes/menu.php'); ?>
        <div class="col-md-9">
            <div class="well well-sm content_area">

                <div class="row">

                    <div class="col-md-12">
                        <h4 class="heading">User Management</h4>
                        <div class="head_opts">
                            <a href="javascript:;" class="btn add_user btn-primary">Add New User &nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="postSection">
                        <div class="aj_sec">
                            <?php if($total_records > 0) { ?>

                            <div class="col-md-12">

                                <table class="table table-bordered userTable">
                                    <thead>
                                        <tr>
                                            <th>Full Name</th>
                                            <th>User Name</th>
                                            <th>Password</th>
                                            <th>Email</th>
                                            <th>Privileges</th>
                                            <th style="text-align:center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $database->resultset();
                                        foreach ($result as $row) {
                                            $priArr = explode("-", $row['privileges']);
                                            ?>
                                            <tr>
                                                <td>
                                                    <span><?php echo $row['fullname']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $row['username']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $row['passtext']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $row['email']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php
                                                        for ($i=0; $i <count($priArr) ; $i++) { 
                                                            if($i == (count($priArr) - 1))
                                                            {
                                                                echo findKey($menuArray, $priArr[$i]);
                                                            } else {
                                                                echo findKey($menuArray, $priArr[$i]). ", ";    
                                                            }
                                                        }
                                                        ?></span>
                                                    </td>

                                                    <td style="text-align:center">
                                                        <a href="javascript:;" class="btn btn-info btn-edit" data-id="<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i> Edit</a>
                                                        <a href="javascript:;" class="btn btn-danger btn-delete" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                                                    </td>

                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php } else { 
                                        echo "<h2 style='margin-left: 20px'>List of Users will be displayed here.</h2>"; 
                                    } ?>

                                </div><!-- aj_sec -->
                            </div><!-- postSection -->

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add User</h4>
                </div>
                <form class="form-horizontal myform">

                    <div class="modal-body">

                        <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Full Name</h4></label>
                            <div class="col-sm-8">
                                <input type="text" name="1" class="getValue form-control required" data-key="fullname">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> User Name</h4></label>
                            <div class="col-sm-8">
                                <input type="text" name="2" class="getValue form-control required" data-key="username">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Password</h4></label>
                            <div class="col-sm-8">
                                <input type="text" name="password" id="password" class="getValue form-control required" data-key="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Re-type Pass</h4></label>
                            <div class="col-sm-8">
                                <input type="text" name="4" class="form-control" equalto="#password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="l2" class="col-sm-4 control-label"><h4 class="tb_title"><span>*</span> Email</h4></label>
                            <div class="col-sm-8">
                                <input type="email" name="email" id="password" class="getValue form-control required" data-key="email">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="l10" class="col-sm-4 control-label"><h4 class="tb_title">Privileges</h4></label>
                            <div style="padding-left: 15px; padding-right: 15px" class="input-group col-sm-8 privileges_selection">
                              <input data-role="tagsinput" id="privileges" data-id="" disabled="disabled" type="text" name="privileges" class="form-control" placeholder="Select Privilages" aria-describedby="basic-addon3">
                              <span class="input-group-addon" id="privileges-addon"><i class="fa fa-plus"></i></span>
                          </div>

                      </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save_user">Save</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="aj_data"></div>
    </div>
</div>
</div>

<div class="modal fade" id="mySelectionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="selection_data"></div>
    </div>
</div>
</div>

<?php
include_once ('../includes/footer.php'); ?>
<script type="text/javascript">
    $(function(){
        var loc = window.location.pathname;
        var qs = window.top.location.search;
        
        $(".add_user").click(function(){
            $("#myModal").modal('show');
        });

        $(document).on('click', '.btn-edit', function(){
            showLoader();
            var id = $(this).data('id');
            setTimeout(function(){
                $(".aj_data").load("user_data.php?command=getUser&id="+id+"", function(){
                    $("#myEditModal").modal('show');
                    hideLoader();
                    
                    $('#privileges1').tagsinput('refresh');
                });
            }, 700);
        });

        $(document).on('click', '.save_user', function(e){
            e.preventDefault();
            if (!$(".myform").validate().form()) {
                return false;
            }
            var arrFields = $(".myform .getValue").map(function() {
                var $elm = $(this),
                childId = $elm.data('key');
                if (childId.length == 0) {
                    alert("Error");
                    return false;
                }
                return childId;
            }).get();
            var arrValues = $(".myform .getValue").map(function() {
                if ($(this).val().length == 0) {
                    $(this).val('');
                }
                return this.value.escapeHTML();
            }).get();

            var pri = $(".myform #privileges").attr('data-id');
            pri = pri.replace(/,/g , "-");
            arrFields.splice(0, 0, 'privileges');
            arrValues.splice(0, 0, pri);

            arrFields.splice(1, 0, 'passtext');
            arrValues.splice(1, 0, $(".myform #password").val());

            showLoader();

            $.ajax({
                type: 'POST',
                url: '../includes/processing.php',
                data: 'table=' + 'admin' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&pageName=' + 'admins' + '&command=' + 'add',
                cache: false,
                success: function(data) {
                    //alert(data);
                    if(data == 'correct')
                    {
                        $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                            hideLoader();
                            $("#myModal").modal('hide');
                            showNotification('success', 'Process done successfully!');
                            $(".myform")[0].reset();
                            $('input#privileges').tagsinput('removeAll');
                        }); 
                    } else if(data == 'not')
                    {
                        hideLoader();
                        showNotification('warning', 'Username not available!');
                    } else 
                    {
                        showNotification('error', 'There is some error, Please contact administration!');
                    }
                }
            });
});  
$(document).on('click', '.btn-delete', function(){
    if(!confirm('Are you sure, you want to delete user?')) return false;
    db = "id";
    value = $(this).data('id');
    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/processing.php',
        data: 'table=' + 'admin' + '&db=' + db + '&data=' + value + '&command=' + 'delete',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                    hideLoader();
                    showNotification('success', 'Process done successfully!');
                }); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$(document).on('click', '.edit_user', function(e){
    e.preventDefault();
    if (!$(".myeditform").validate().form()) {
        return false;
    }
    var arrFields = $(".myeditform .getValue").map(function() {
        var $elm = $(this),
        childId = $elm.data('key');
        if (childId.length == 0) {
            alert("Error");
            return false;
        }
        return childId;
    }).get();
    var arrValues = $(".myeditform .getValue").map(function() {
        if ($(this).val().length == 0) {
            $(this).val('');
        }
        return this.value.escapeHTML();
    }).get();

    var pri = $("#privileges1").attr('data-id');
    pri = pri.replace(/,/g , "-");
    arrFields.splice(0, 0, 'privileges');
    arrValues.splice(0, 0, pri);

    arrFields.splice(1, 0, 'passtext');
    arrValues.splice(1, 0, $(".myeditform #password").val());

    var key = "id";
    var value = $(this).data('id');

    showLoader();
    $.ajax({
        type: 'POST',
        url: '../includes/processing.php',
        data: 'table=' + 'admin' + '&db=' + encodeURIComponent(arrFields) + '&data=' + encodeURIComponent(arrValues) + '&key=' + key + '&value=' + value + '&pageName=' + 'admins' + '&command=' + 'update',
        cache: false,
        success: function(data) {
            if(data == 'correct')
            {
                $(".postSection").load(""+loc+qs+" .aj_sec", function(){
                    hideLoader();
                    showNotification('success', 'Process done successfully!');
                    $("#myEditModal").modal('hide');
                }); 
            } else 
            {
                showNotification('error', 'There is some error, Please contact administration!');
            }
        }
    });
});

$("#privileges-addon").click(function(){
    showLoader();
    setTimeout(function(){
        $(".selection_data").load("user_data.php?command=getPrivileges", function(){
            $("#mySelectionModal").modal('show');
            hideLoader();
            $("#privileges").attr('data-id','');
        });
    }, 300);
});

$(document).on('click', "#privileges-addon1" ,function(){
    showLoader();
    setTimeout(function(){
        $(".selection_data").load("user_data.php?command=getPrivilegesEdit", function(){
            $("#mySelectionModal").modal('show');
            hideLoader();
            var id = $("#privileges1").attr('data-id');
            var values = id.split(',');
            $('.privileges_data [data-id="'+values.join('"],[data-id="')+'"]').prop('checked',true);

        });
    }, 300);
});

//Privilege Check
var privileges = [];
var privilegeIds = [];
var pflag = 0;
var selectorId = ".privileges_selection:visible input";
$(document).on('click', '.privilege_checkbox', function() {
    if(pflag == 0)
    {       
        if($(selectorId).attr('data-id') != "")
        {
            var id = $(selectorId).data('id');
            var idTemp = id.split(',');
            jQuery.each(idTemp, function(index, item) {
                privilegeIds.push(item);
            });

            var value = $(selectorId).val().trim();
            var valTemp = value.split(',');
            jQuery.each(valTemp, function(index, item) {
                privileges.push(item);
            });

        }
        pflag = 1;
    }
    if($(this).is(':checked'))
    {
        privileges.push($(this).val());
        $(selectorId).val(privileges);

        privilegeIds.push($(this).data('id'));
        $(selectorId).attr('data-id', privilegeIds);
    } else 
    {
        text = $(this).val().trim();
        pos = getPosition(privileges, text);

        privileges = $.map(privileges, $.trim);

        privileges = jQuery.grep(privileges, function(value) {
            return value != text;
        });

        var remove = privilegeIds[pos];
        privilegeIds = jQuery.grep(privilegeIds, function(value) {
            return value != remove;
        });

        var newVal = removeValue($(selectorId).attr('data-id'), remove);
        $(selectorId).attr('data-id', newVal);
    }
});
$(document).on('click', '.select_privileges', function() {
    for (i = 0; i < privileges.length; i++) {
        $(selectorId).tagsinput('destroy');
        $(selectorId).tagsinput('add', privileges[i]);
    }
    $(selectorId).removeAttr('placeholder');
    $("#mySelectionModal").modal('hide');
});

$(document).on('click', '.privileges_selection:visible span.tag span', function() {
    if($(selectorId).attr('data-id') != "")
    {
        privileges = [];
        privilegeIds = [];

        var id = $(selectorId).data('id');
        var idTemp = id.split(',');
        jQuery.each(idTemp, function(index, item) {
            privilegeIds.push(item);
        });

        var value = $(selectorId).val().trim();
        var valTemp = value.split(',');
        jQuery.each(valTemp, function(index, item) {
            privileges.push(item);
        });
    }
    var text = $(this).parent('span.tag').text();

        var pos = getPosition(privileges, text); //returns 1
        privileges = jQuery.grep(privileges, function(value) {
          return value != text;
      });
        var remove = privilegeIds[pos];
        var newVal = removeValue($(selectorId).attr('data-id'), remove);
        $(selectorId).attr('data-id', newVal);
    });

$(document).on('click', '.cancel_privileges', function(){
    $(selectorId).attr('data-id', '');
    $(selectorId).tagsinput('removeAll');
    privileges = [];
    privilegeIds = [];
    pflag = 0;
});

});
</script>