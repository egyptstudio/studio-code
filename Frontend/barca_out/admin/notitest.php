<?php

// set time limit to zero in order to avoid timeout
set_time_limit(0);

// charset header for output
header('content-type: text/html; charset: utf-8');

// this is the pass phrase you defined when creating the key
$passphrase = '';

// you can post a variable to this string or edit the message here
if (!isset($_POST['msg'])) {
	$_POST['msg'] = "Notification message here!";
}

// tr_to_utf function needed to fix the Turkish characters
$message = tr_to_utf($_POST['msg']);

// load your device ids to an array
$deviceIds = array(
	'APA91bGt6mIVUW3aE792Au91UF00375o6ldgcX2tuITrisGZxC3hVNTLtpbUVDa3oXCV5llIHp711JCSs0OurbGUS_7VzCXlwkiulhpY47_Iqkx-8q5Mrf2hf0AKUm_ocV6ozvqcrOyL',
	'APA91bECLB74Sf2l0uActCZxQSY-_92kw1uBlYXcUyBeg3Gr7IwVRk3Q0s0HE-JF4A7bvbPdFRZdt0iwSpK0A3NSj1iwcDwdd_sqBu_TaIjMak-shPklLZ6LMyPa4dR1mh2o875X3bVNpUJFZsp-2J31fSkHgMg-8g',
	'APA91bHhX_DDxkXC3J509CxxO9n3aUIYZU4I765v2vpuGTcL9byWrPS8LwKh9vYrAgi6nA8Y5Cw-OlMWHRVqMUCHm1jcacAJQELtfm2JhbO9rf4JWbtiIS7ygnOUnw2fKf6dAbkiK4db'
	//'lh142lk3h1o2141p2y412d3yp1234y1p4y1d3j4u12p43131p4y1d3j4u12p4313',
//'y1p4y1d3j4u12p43131p4y1d3j4u12p4313lh142lk3h1o2141p2y412d3yp1234'
	);

// this is where you can customize your notification
$payload = '{"aps":{"alert":"' . $message . '","sound":"default"}}';

$result = 'Start' . '<br />';

////////////////////////////////////////////////////////////////////////////////
// start to create connection
$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'MyAppGenerated.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
// Open a connection to the APNS server
$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

echo count($deviceIds) . ' devices will receive notifications.<br />';

foreach ($deviceIds as $item) {
    // wait for some time
	sleep(1);

	$apiKey = "AIzaSyAqJBTeLp_1Hdt0DkL3m3HyyHgnYXuSBbs";

	/*$registrationIDs = array(
		'APA91bECLB74Sf2l0uActCZxQSY-_92kw1uBlYXcUyBeg3Gr7IwVRk3Q0s0HE-JF4A7bvbPdFRZdt0iwSpK0A3NSj1iwcDwdd_sqBu_TaIjMak-shPklLZ6LMyPa4dR1mh2o875X3bVNpUJFZsp-2J31fSkHgMg-8g'
	);*/
	
	$registrationIDs = $deviceIds;
	$message = 'Studio Library Modified';

	$url = 'https://android.googleapis.com/gcm/send';

	$fields = array(
		'registration_ids' => $registrationIDs,
		'data' => array(
			"message" => $message
			)
		);

	$headers = array(
		'Authorization: key=' . $apiKey,
		'Content-Type: application/json'
		);

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Curl failed: ' . curl_error($ch));
	}

	curl_close($ch);
    var_dump($result); 

    /*if (!$fp) {
        exit("Failed to connect: $err $errstr" . '<br />');
    } else {
        echo 'Apple service is online. ' . '<br />';
    }
 
    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $item) . pack('n', strlen($payload)) . $payload;
     
    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));
     
    if (!$result) {
        echo 'Undelivered message count: ' . $item . '<br />';
    } else {
        echo 'Delivered message count: ' . $item . '<br />';
    }
 
    if ($fp) {
        fclose($fp);
        echo 'The connection has been closed by the client' . '<br />';
    }*/
}

echo count($deviceIds) . ' devices have received notifications.<br />';

// function for fixing Turkish characters
function tr_to_utf($text) {
	$text = trim($text);
	$search = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�');
	$replace = array('Ü', 'Ş', '&#286;�', 'Ç', 'İ', 'Ö', 'ü', 'ş', 'ğ', 'ç', 'ı', 'ö');
	$new_text = str_replace($search, $replace, $text);
	return $new_text;
}

// set time limit back to a normal value
set_time_limit(30);
?>