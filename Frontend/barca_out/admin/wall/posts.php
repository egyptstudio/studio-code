<?php
include_once ('../includes/check.php');
include_once ('../includes/header.php'); 

$privacy = array(
	'1' => 'Contest',
	'2' => 'Public',
	'3' => 'Friends',
	'4' => 'Private'
	);

if($_GET['clear_search'])
{
	unset($_SESSION['search_where']);
	unset($_SESSION['search_user']);
	unset($_SESSION['search_date_from']);
	unset($_SESSION['search_date_to']);
	unset($_SESSION['search_post']);
	unset($_SESSION['search_repost']);
	unset($_SESSION['search_privacy']);	
}

$_SESSION['lib'] = 28;

$state = isset($_REQUEST['state']) ? $_REQUEST['state'] : '0';

$where = '';
if(!empty($_POST['search']))
{
	unset($_SESSION['search_where']);
	unset($_SESSION['search_user']);
	unset($_SESSION['search_date_from']);
	unset($_SESSION['search_date_to']);
	unset($_SESSION['search_post']);
	unset($_SESSION['search_repost']);
	unset($_SESSION['search_privacy']);
	
	if(!empty($_POST['user']))
	{
		$_SESSION['search_user']=$_POST['user'];
		$where.= " and u.fullName like '%".$_POST['user']."%'";
	}
	
	if(!empty($_POST['privacy']))
	{
		$_SESSION['search_privacy']=$_POST['privacy'];
		$where.= " and p.privacy ='".$_POST['privacy']."'";
	}

	if(isset($_POST['post']) && isset($_POST['repost']))
	{
		$_SESSION['search_post']=$_POST['post'];
		$_SESSION['search_repost']=$_POST['repost'];
	}
	elseif(isset($_POST['post']))
	{
		$_SESSION['search_post']=$_POST['post'];
		$where.= " and p.originalPostId=0";
	}
	elseif(isset($_POST['repost']))
	{
		$_SESSION['search_repost']=$_POST['repost'];
		$where.= " and p.originalPostId!=0";
	}
	
	
	if(!empty($_POST['date_from']) && !empty($_POST['date_to']))
	{
		$_SESSION['search_date_from']=$_POST['date_from'];
		$_SESSION['search_date_to']=$_POST['date_to'];
		$where.= " and p.creationTime >= '".date('Y-m-d H:i:s',strtotime($_POST['date_from']))."' and p.creationTime <= '".date('Y-m-d H:i:s',strtotime($_POST['date_to'].' 23:59:59'))."'";
	}
	elseif(!empty($_POST['date_from']))
	{
		$_SESSION['search_date_from']=$_POST['date_from'];
		$where.= " and p.creationTime >= '".date('Y-m-d H:i:s',strtotime($_POST['date_from']))."' and p.creationTime <= '".date('Y-m-d H:i:s',strtotime($_POST['date_from'].' 23:59:59'))."'";
	}
	elseif(!empty($_POST['date_to']))
	{
		$_SESSION['search_date_to']=$_POST['date_to'];
		$where.= " and p.creationTime >= '".date('Y-m-d H:i:s',strtotime($_POST['date_to']))."' and p.creationTime <= '".date('Y-m-d H:i:s',strtotime($_POST['date_to'].' 23:59:59'))."'";
	}

	if(!empty($where))
		$_SESSION['search_where'] = $where;
}
elseif(!empty($_SESSION['search_where']))
{
	$_POST['user'] = $_SESSION['search_user'];
	$_POST['date_from'] = $_SESSION['search_date_from'];
	$_POST['date_to'] = $_SESSION['search_date_to'];
	$_POST['post'] = $_SESSION['search_post'];
	$_POST['repost'] = $_SESSION['search_repost'];
	$_POST['privacy'] = $_SESSION['search_privacy'];	
	
	$where = ' '.$_SESSION['search_where'];
}

if(!is_numeric($state))
{
	$state = 0;

} 
if($state == 0 || $state == 2)
{
	$text = 'Hide';
}
else {
	$text = 'Show';
}

$recPerPage = 20;
if (isset($_GET["page"]) && is_numeric($_GET['page'])) 
{ 
	$page  = $_GET["page"];
} else { 
	$page=1; 
}
$startFrom = ($page-1) * $recPerPage;

$sortTo = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : 'creationTime';
if($sortTo != 'userId' && $sortTo != 'creationTime' && $sortTo != 'reportCount')
{
	$sortTo = 'creationTime';
}
$sort = 'p.'.$sortTo;

$orderTo = isset($_REQUEST['order']) ? $_REQUEST['order'] : 'DESC';
if($orderTo != 'DESC' && $orderTo != 'ASC')
{
	$orderTo = 'DESC';
}
if($orderTo == 'DESC')
{
	$order = 'ASC';
} else {
	$order = 'DESC';
}
if($state == 2)
{
	$database->query('SELECT p.privacy,p.creationTime,p.originalPostId,u.fullName, p.userId, p.postId, p.creationTime, p.postPhotoUrl, p.likeCount, p.reportCount, p.hidden FROM post p, user u WHERE p.reportCount > 0 AND p.hidden = 0 AND u.userId = p.userId'.$where.' ORDER BY '.$sort.' '.$orderTo.' ');
} else {
	$database->query('SELECT p.privacy,p.creationTime,p.originalPostId,u.fullName, p.userId, p.postId, p.creationTime, p.postPhotoUrl, p.likeCount, p.reportCount, p.hidden FROM post p, user u WHERE p.hidden = :state AND u.userId = p.userId'.$where.' ORDER BY '.$sort.' '.$orderTo.' ');
	$database->bind(':state', $state);       
}

$database->execute();
$total_records = $database->rowCount();
$total_pages = ceil($total_records / $recPerPage); 

if($state == 2)
{
	$database->query('SELECT p.privacy,p.creationTime,p.originalPostId,u.fullName, p.userId, p.postId, p.creationTime, p.postPhotoUrl, p.likeCount, p.reportCount, p.hidden FROM post p, user u WHERE p.reportCount > 0 AND p.hidden = 0 AND u.userId = p.userId'.$where.' ORDER BY '.$sort.' '.$orderTo.' LIMIT :start, :num');
} else {
	$database->query('SELECT p.privacy,p.creationTime,p.originalPostId,u.fullName, p.userId, p.postId, p.creationTime, p.postPhotoUrl, p.likeCount, p.reportCount, p.hidden FROM post p, user u WHERE p.hidden = :state AND u.userId = p.userId'.$where.' ORDER BY '.$sort.' '.$orderTo.' LIMIT :start, :num');
	$database->bind(':state', $state);
}

$database->bind(':start', $startFrom);
$database->bind(':num', $recPerPage);
$database->execute();

if($total_records > $recPerPage)
{
	$rowsGen = ($recPerPage + $startFrom);
} else 
{
	$rowsGen = $database->rowCount();
}

if($rowsGen > $total_records)
{
	$rowsGen = $total_records;
}
?>

<div class="container-fluid">
	<div class="row">

		<?php include_once ('../includes/menu.php'); ?>
		<div class="col-md-9">
			<div class="well well-sm content_area">

				<div class="row">
					

					<div class="col-md-12">
						<h4 class="heading">Latest Posts</h4>
						<div class="head_opts">
							<a href="javascript:;" class="btn btn-<?php echo $text; ?> btn-primary"><?php echo $text; ?> Selected</a>
						</div>

					</div>

					<div class="photoOpts1" style="border: 0;width: 70%;">
						<div class="btn-group" role="group" aria-label="...">
							<a href="posts.php?page=1&state=0" class="btn btn-primary <?php if($state==0) {echo 'activePlayerMenu'; } ?>">Posts</a>
							<a href="posts.php?page=1&state=1" class="btn btn-primary <?php if($state==1) {echo 'activePlayerMenu'; } ?>">Hidden Posts</a>
							<a href="posts.php?page=1&state=2" class="btn btn-primary <?php if($state==2) {echo 'activePlayerMenu'; } ?>">Reported Posts</a>
						</div>
					</div>


					<div class="postSection">
						<div class="aj_sec">
							<?php if($total_records > 0) { ?>
							<div class="opts3" style="margin-top: -10px;">
								<nav class="siteNav pull-left">
									<ul class="pagination">
										<li>
											<a href="posts.php?page=1&state=<?php echo $state; ?>&sort=<?php echo $sortTo; ?>&order=<?php echo $orderTo; ?>" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
											</a>
										</li>
										<?php
										if($page == 1)
										{
											$prevPage = 1;
										} else 
										{
											$prevPage = ($page - 1);
										}

										if($page == $total_pages)
										{
											$nextPage = $total_pages;
										} else 
										{
											$nextPage = ($page + 1);
										}
										echo '<li><a href="posts.php?page='.$prevPage.'&state='.$state.'&sort='.$sortTo.'&order='.$orderTo.'">&lt;</a></li>';
										echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
										echo '<li><a href="posts.php?page='.$nextPage.'&state='.$state.'&sort='.$sortTo.'&order='.$orderTo.'">&gt;</a></li>';
										?>
										<li>
											<a href="posts.php?page=<?php echo $total_pages ?>&state=<?php echo $state; ?>&sort=<?php echo $sortTo; ?>&order=<?php echo $orderTo; ?>" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
											</a>
										</li>
									</ul>
								</nav>
							</div>

							<!-- Ewida start -->
							<div class="col-md-12">



								<style>
									.ui-autocomplete-loading {
										background: white url("ui-anim_basic_16x16.gif") right center no-repeat;
									}

								</style>

								<form method="post" action="posts.php?state=<?php echo $state; ?>&sort=<?php echo $sortTo; ?>&order=<?php echo $orderTo; ?>">
									<label for="user">User: </label>
									<input id="user" name="user" value="<?php echo $_POST['user']; ?>" style="width: 120px">&nbsp;&nbsp;
									<label for="date_from">From date</label>
									<input type="text" id="date_from" name="date_from" style="width: 80px" value="<?php echo $_POST['date_from']; ?>">
									<label for="date_to">To date</label>
									<input type="text" id="date_to" name="date_to" style="width: 80px" value="<?php echo $_POST['date_to']; ?>">&nbsp;&nbsp;
									<label>Type: </label>&nbsp;
									<input type="checkbox" id="post" name="post" <?php if(isset($_POST['post'])){ ?>checked="checked"<?php } ?>>&nbsp;<label for="post" style="font-weight: normal; ">Post </label>
									<input type="checkbox" id="repost" name="repost" <?php if(isset($_POST['repost'])){ ?>checked="checked"<?php } ?>>&nbsp;<label for="repost" style="font-weight: normal; ">Repost </label>
									&nbsp;&nbsp;<select name="privacy">
									<option value="">-- Select privacy --</option>
									<option value="1"<?php if($_POST['privacy'] == '1'){ ?> selected="selected"<?php } ?>>Contest</option>
									<option value="2"<?php if($_POST['privacy'] == '2'){ ?> selected="selected"<?php } ?>>Public</option>
									<option value="3"<?php if($_POST['privacy'] == '3'){ ?> selected="selected"<?php } ?>>Friends</option>
									<option value="4"<?php if($_POST['privacy'] == '4'){ ?> selected="selected"<?php } ?>>Private</option>
								</select>&nbsp;
								<input class="btn btn-success" type="submit" value="Search" name="search" />
								<a href="posts.php?clear_search=1&state=<?php echo $state; ?>&sort=<?php echo $sortTo; ?>&order=<?php echo $orderTo; ?>" class="btn btn-danger">Cancel</a>
							</form>	
							<br>

						</div>
						<!-- Ewida end -->
						<div class="col-md-12">

							<table class="table table-bordered userTable">
								<thead>
									<tr>
										<th><input type="checkbox" id="selectall"></th>
										<th>Image</th>
										<th><a href="posts.php?state=<?php echo $state; ?>&sort=userId&order=<?php echo $order; ?>">Username</a></th>
										<th>Status</th>
										<th>Likes count</th>
										<th><a href="posts.php?state=<?php echo $state; ?>&sort=reportCount&order=<?php echo $order; ?>">Reports count</a></th>
										<th>Main post?</th>
										<th>Privacy</th>
										<th><a href="posts.php?state=<?php echo $state; ?>&sort=creationTime&order=<?php echo $order; ?>">Date</a></th>
									</tr>
								</thead>
								<tbody>
									<?php

									if (!class_exists('S3'))require_once('../includes/S3.php');
									if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJONZOQ6UX2QGY66A');
									if (!defined('awsSecretKey')) define('awsSecretKey', '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');
									$s3 = new S3(awsAccessKey, awsSecretKey);

									$result = $database->resultset();
									$database->query('SELECT startDateTime, endDateTime FROM contest WHERE status = :c');
									$database->bind(":c", 2);
									$database->execute();
									$contestRow = $database->single();

									foreach ($result as $row) {
										if(($row['creationTime'] >= $contestRow['startDateTime']) && ($row['creationTime'] <= $contestRow['endDateTime']))
										{
											$postStatus = 'In Contest';
										} else 
										{
											$postStatus = 'Out of Contest';
										}

										if($row['originalPostId'])
										{
											$database->query('SELECT userId FROM post WHERE postId='.$row['originalPostId']);
											$database->execute();
											$result2 = $database->resultset();
											if(isset($result2[0]['userId']))
												$row['userId'] = $result2[0]['userId'];
										}

										//$info = $s3->getObjectInfo('barca', 'third_party/uploads/users/'.$row['userId'].'/'.$row['postPhotoUrl']);
										$info = 'https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/users/'.$row['userId'].'/'.$row['postPhotoUrl'];
										$img_url = 'https://cpportalvhds24j8j4r74686.blob.core.windows.net/pro-s3/third_party/uploads/users/'.$row['userId'].'/'.$row['postPhotoUrl'];
						//if ($info)
						//{
										?>
										<tr <?php if($row['originalPostId']) echo 'class="active"'; ?>>
											<td>
												<span><input class="checkbox" type="checkbox" value="<?php echo $row['postId']; ?>"<?php if($row['privacy']!='2')echo ' disabled'; ?>></span>
											</td>
											<td>
												<?php if ($info){ ?><a href="<?php echo $img_url; ?>" class="fancybox"><img width="100" src="<?php echo $img_url; ?>"></a><?php } ?>
											</td>
											<td>
												<span class="fullname"><?php echo $row['fullName']; ?></span>
											</td>
											<td>
												<span class="pstatus"><?php echo $postStatus; ?></span>
											</td>
											<td>
												<span class="plikes"><?php echo $row['likeCount']; ?></span>
											</td>
											<td>
												<span class="rcount"><?php echo $row['reportCount']; ?></span>
											</td>
											<td>
												<span class="rcount"><?php if(!$row['originalPostId'])echo 'Yes'; ?></span>
											</td>
											<td>
												<span class="rcount"><?php echo $privacy[$row['privacy']]; ?></span>
											</td>
											<td>
												<span class="rcount"><?php echo $row['creationTime']; ?></span>
											</td>
										</tr>
										<?php } //} ?>
									</tbody>
								</table>
							</div>
							<?php } else { ?>
							<!-- Ewida start -->
							<div class="col-md-12">



								<style>
									.ui-autocomplete-loading {
										background: white url("ui-anim_basic_16x16.gif") right center no-repeat;
									}

								</style>

								<form method="post" action="posts.php?state=<?php echo $state; ?>">
									<label for="user">User: </label>
									<input id="user" name="user" value="<?php echo $_POST['user']; ?>">&nbsp;&nbsp;
									<label for="date_from">From date</label>
									<input type="text" id="date_from" name="date_from" style="width: 80px" value="<?php echo $_POST['date_from']; ?>">
									<label for="date_to">To date</label>
									<input type="text" id="date_to" name="date_to" style="width: 80px" value="<?php echo $_POST['date_to']; ?>">&nbsp;&nbsp;
									<label>Type: </label>&nbsp;
									<input type="checkbox" id="post" name="post" <?php if(isset($_POST['post'])){ ?>checked="checked"<?php } ?>>&nbsp;<label for="post" style="font-weight: normal; ">Post </label>
									<input type="checkbox" id="repost" name="repost" <?php if(isset($_POST['repost'])){ ?>checked="checked"<?php } ?>>&nbsp;<label for="repost" style="font-weight: normal; ">Repost </label>
									&nbsp;&nbsp;<select name="privacy">
									<option value="">Select privacy</option>
									<option value="1"<?php if($_POST['privacy'] == '1'){ ?> selected="selected"<?php } ?>>Contest</option>
									<option value="2"<?php if($_POST['privacy'] == '2'){ ?> selected="selected"<?php } ?>>Public</option>
									<option value="3"<?php if($_POST['privacy'] == '3'){ ?> selected="selected"<?php } ?>>Friends</option>
									<option value="4"<?php if($_POST['privacy'] == '4'){ ?> selected="selected"<?php } ?>>Private</option>
								</select>&nbsp;
								<input class="btn btn-success" type="submit" value="Search" name="search" />
								<a href="posts.php?clear_search=1&state=<?php echo $state; ?>" class="btn btn-danger">Cancel</a>
							</form>	
							<br>

						</div>
						<!-- Ewida end -->
						<?php
						echo "<h2 style='margin-left: 20px'>List of posts will be displayed here.</h2>"; 
					} ?>

					<div class="opts3">
						<nav style="margin-bottom: 0px;" class="siteNav pull-left">
							<ul style="margin-bottom: 0px;" class="pagination">
								<li>
									<a href="posts.php?page=1&state=<?php echo $state; ?>" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
									</a>
								</li>
								<?php
								if($page == 1)
								{
									$prevPage = 1;
								} else 
								{
									$prevPage = ($page - 1);
								}

								if($page == $total_pages)
								{
									$nextPage = $total_pages;
								} else 
								{
									$nextPage = ($page + 1);
								}
								echo '<li><a href="posts.php?page='.$prevPage.'&state='.$state.'">&lt;</a></li>';
								echo '<li class="active"><a href="">'.$rowsGen.' of '.$total_records.'</a></li>';
								echo '<li><a href="posts.php?page='.$nextPage.'&state='.$state.'">&gt;</a></li>';
								?>
								<li>
									<a href="posts.php?page=<?php echo $total_pages ?>&state=<?php echo $state; ?>" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							</ul>
						</nav>
					</div>

				</div><!-- aj_sec -->
			</div><!-- postSection -->

		</div>

	</div>
</div>
</div>
</div>
<input type="hidden" value="" id="ids">
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title userName" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<img width="200" src="" class="postPhotoHolder img-responsive" style="margin: 0 auto" alt="Image">
				<form style="margin-top: 10px; padding-top: 15px; border-top: 1px solid #ccc;" class="form-horizontal myform">
					<div class="form-group">
						<label for="l2" class="col-sm-6 control-label"><h4 class="tb_title">Photo Status</h4></label>
						<div class="col-sm-6">
							<p class="postStatus"></p>    
						</div>
					</div>

					<div class="form-group">
						<label for="l2" class="col-sm-6 control-label"><h4 class="tb_title">Number of likes</h4></label>
						<div class="col-sm-6">
							<p class="likes"></p>    
						</div>
					</div>

					<div class="form-group">
						<label for="l2" class="col-sm-6 control-label"><h4 class="tb_title">Number of Reports</h4></label>
						<div class="col-sm-6">
							<p class="reports"></p>    
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" data-id="" class="btn btn-success btn-keep">Keep</button>
				<button type="button" data-id="" class="btn btn-danger btn-hide">Hide</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<?php
include_once ('../includes/footer.php'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link type="text/css" rel="stylesheet" href="../plugins/fancybox/jquery.fancybox.css" />
<script src="../plugins/fancybox/jquery.fancybox-1.3.4.js"></script>
<script src="../plugins/fancybox/jquery.easing-1.3.pack.js"></script>

<script type="text/javascript">

	$(function(){
		$(".fancybox").fancybox();
		$('#user').autocomplete({
			source: "../includes/processing.php?command=users_complete",
			minLength: 3,
			delay: 100
		});

		$( "#date_from" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function( selectedDate ) {
				$( "#to" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		$( "#date_to" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function( selectedDate ) {
				$( "#from" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
		var loc = window.location.pathname;
		var qs = window.top.location.search;
		var idArray = [];
        /*$(document).on('click', '.openPop', function(){
            $(".userName").text($(this).parents('tr').find('span.fullname').text());
            $(".postStatus").text($(this).parents('tr').find('span.pstatus').text());
            $(".likes").text($(this).parents('tr').find('span.plikes').text());
            $(".reports").text($(this).parents('tr').find('span.rcount').text());
            $(".postPhotoHolder").attr('src', $(this).find('img').attr('src'));
            $(".btn-keep").attr('data-id', $(this).attr('data-id'));
            $(".btn-hide").attr('data-id', $(this).attr('data-id'));
            $("#myModal").modal('show');
        });*/


$(document).on('click', ".checkbox", function(){
            //if($(this).is(':checked'))
            if(this.checked)
            {
            	idArray.push($(this).val());
            	$("#ids").val(idArray);

            } else 
            {
            	var text = $(this).val();
            	pos = getPosition(idArray, text);

            	var remove = idArray[pos];
            	idArray = jQuery.grep(idArray, function(value) {
            		return value != remove;
            	});

            	var newVal = removeValue($("#ids").val(), remove);
            	$("#ids").val(newVal);
            }
        });

$('#selectall').click(function(event) { 
	if(this.checked) { 
		$('.checkbox').each(function() {
			if(this.disabled != true)
			{
				this.checked = true;
				idArray.push($(this).val());
			}
		});
		$("#ids").val(idArray);
	}else{
		$('.checkbox').each(function() { 
			this.checked = false; 
		});
		$("#ids").val(''); 
		idArray = [];
	}
});


$(".btn-Hide").click(function(){
	if($("#ids").val() == "")
	{
		showNotification('warning', 'Please select atleast 1 post!');
		return false;
	}
	showLoader();
	$.ajax({
		type: 'POST',
		url: '../includes/processing.php',
		data: 'id=' + $("#ids").val() + '&command=' + 'hidePosts',
		cache: false,
		success: function(data) {
			hideLoader();
			if(data == 'correct')
			{
				showNotification('success', 'Process done successfully!');
				$(".postSection").load(""+loc+qs+" .aj_sec"); 
			} else 
			{
				showNotification('error', 'There is some error, Please contact administration!');
			}
		}
	});
});

$(".btn-Show").click(function(){
	if($("#ids").val() == "")
	{
		showNotification('warning', 'Please select atleast 1 post!');
		return false;
	}
	showLoader();
	$.ajax({
		type: 'POST',
		url: '../includes/processing.php',
		data: 'id=' + $("#ids").val() + '&command=' + 'showPosts',
		cache: false,
		success: function(data) {
			hideLoader();
			if(data == 'correct')
			{   
				showNotification('success', 'Process done successfully!');
				$(".postSection").load(""+loc+qs+" .aj_sec"); 
			} else 
			{
				showNotification('error', 'There is some error, Please contact administration!');
			}
		}
	});
});

});
</script>