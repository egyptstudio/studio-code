<?php $_SESSION['lib'] = isset($_SESSION['lib']) ? $_SESSION['lib'] : ''; ?>
<div class="col-md-3">
  <div class="well well-sm">
    <ul class="nav nav-list">
      <li><label class="nav-header">User</label></li>
      <li class="nav-divider"></li>
      <li><a <?php if($_SESSION['lib'] == 1) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/index.php">Latest</a>
      <li class="nav-divider"></li>
      <li><a <?php if($_SESSION['lib'] == 2) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/wall.php">Wall</a>
      <li class="nav-divider"></li>
      <li><a <?php if($_SESSION['lib'] == 3) { echo 'class="active_menu"'; } ?> href="<?php echo $url; ?>/my-pics.php">My Pics</a>

    </ul>
  </div>
</div>