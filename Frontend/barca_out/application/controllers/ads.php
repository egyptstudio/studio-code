<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ads extends CI_Controller
{   
    function __construct() {
        parent::__construct();
        
        $headers = apache_request_headers();
        
        if(empty($headers['Appkey']) || !isset($headers['Appkey']))
        {
            print_r(json_encode(array(
                'error' => 'App Key is missing.'
            )));
            return false;
        }

        if($headers['Appkey'] != 'FCB')
        {
            print_r(json_encode(array(
                'error' => 'Invalid App Key.'
            )));
            return false;
        }
        
        $this->load->model('main_model');
    }

    public function index() {
        echo "Ads Web Services.";
    }
    
    function getScreensAdLookup() {
        $json_data = $this->input->post('data');
        //$json_data = '{"userID" : "711"}';
        $arrayData = json_decode($json_data);
        
        $userId = $arrayData->userID;
        if ( empty($userId) || !is_numeric($userId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide user identification.'
            )));
            return false;
        }

        $user = $this->main_model->getData('user', 'userId, ads_status', array('userId' => $userId, 'ads_status' => '1'));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect user identification.'
            )));
            return false;
        }
        
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'No Ads applicable for this user.'
            )));
            return false;
        }
        
        $screen = $this->main_model->getData('ads_screen');
        if ($screen) {
            foreach ($screen as $row) {
                $adType = $this->main_model->getData('ads_ad_type', 'title', array('ad_type_id' => $row->ad_type_id));
                $data[] = array(
                    'screen_id' => $row->screen_id,
                    'screen_title' => $row->title,
                    'ad_type_id' => $row->ad_type_id,
                    'ad_type_title' => $adType[0]->title
                );
            }
            print_r(json_encode($data));
        } 
        else {
            print_r(json_encode(array(
                'error' => 'Something went wrong. Please try again later.'
            )));
        }
    }

    function getAdByScreenID() {
        $json_data = $this->input->post('data');
        //$json_data = '{"screenID" : "1", "userID" : "711"}';
        $arrayData = json_decode($json_data);
        
        $userId = $arrayData->userID;
        $screenId = $arrayData->screenID;
        //Validations
        if ( empty($userId) || !is_numeric($userId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide user identification.'
            )));
            return false;
        }

        if ( empty($screenId) || !is_numeric($screenId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide a target screen.'
            )));
            return false;
        }

        $user = $this->main_model->getData('user', 'userId, ads_status', array('userId' => $userId, 'ads_status' => '1'));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect user identification.'
            )));
            return false;
        }
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'No Ads applicable for this user.'
            )));
            return false;
        }

        $screen = $this->main_model->getData('ads_screen', 'screen_id, status', array('screen_id' => $screenId, 'status' => '1'));
        if (count($screen) == 0 && $screen == true) {
            print_r(json_encode(array(
                'error' => 'This screen is not active now for advertising.'
            )));
            return false;
        }
        if($screen == false)
        {
            print_r(json_encode(array(
                'error' => 'Incorrect screen id.'
            )));
            return false;
        }        
        //Validations
        
        $ads = $this->main_model->getData('ads_ad');
        if ($ads) {
            foreach ($ads as $row) {
                $adType = $this->main_model->getData('ads_ad_type', 'title', array('ad_type_id' => $row->ad_type_id));
                $campaign = $this->main_model->getData('ads_campaign', 'sponsor_id', array('campaign_id' => $row->campaign_id));
                $sponsorId = $campaign[0]->sponsor_id;
                $sponsor = $this->main_model->getData('ads_sponsor', 'contact_name', array('sponsor_id' => $sponsorId));

                $data[] = array(
                    'ad_id' => $row->ad_id,
                    'ad_url' => $row->ad_link,
                    'ad_url_type' => $row->link_type,
                    'ad_redirect_url' => $row->redirect_link,
                    'ad_type' => $row->ad_type_id,
                    'sponsor_name' => $sponsor[0]->contact_name,
                    'ad_title' => $adType[0]->title,
                    'impression_value' => $row->impression_value,
                    'click_value' => $row->click_value
                );
            }
            print_r(json_encode($data));
        } 
        else {
            print_r(json_encode(array(
                'error' => 'Something went wrong. Please try again later.'
            )));
        }
    }

    function getBrandedAd() {
        $json_data = $this->input->post('data');
        //$json_data = '{"userID" : "711"}';
        $arrayData = json_decode($json_data);
        
        $userId = $arrayData->userID;
        if ( empty($userId) || !is_numeric($userId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide user identification.'
            )));
            return false;
        }

        $user = $this->main_model->getData('user', 'userId, ads_status', array('userId' => $userId, 'ads_status' => '1'));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect user identification.'
            )));
            return false;
        }
        
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'No Ads applicable for this user.'
            )));
            return false;
        }
        
        $brand = $this->main_model->getData('ads_brand_ad');
        if ($brand) {
            foreach ($brand as $row) {
                $campaign = $this->main_model->getData('ads_campaign', 'sponsor_id', array('campaign_id' => $row->campaign_id));
                $sponsorId = $campaign[0]->sponsor_id;
                $sponsor = $this->main_model->getData('ads_sponsor', 'contact_name', array('sponsor_id' => $sponsorId));

                $campaign = $this->main_model->getData('ads_campaign_ads', 'ad_id', array('campaign_id' => $row->campaign_id));
                $adId = $campaign[0]->ad_id;
                $ad = $this->main_model->getData('ads_ad', 'redirect_link, impression_value', array('ad_id' => $adId));

                $data[] = array(
                    'brand_ad_id' => $row->brand_id,
                    'brand_ad_url' => $row->brand_image_url,
                    'ad_redirect_url' => $ad[0]->redirect_link,
                    'sponsor_name' => $sponsor[0]->contact_name,
                    'brand_ad_title' => $row->brand_title,
                    'impression_value' => $ad[0]->impression_value
                );
            }
            print_r(json_encode($data));
        } 
        else {
            print_r(json_encode(array(
                'error' => 'Something went wrong. Please try again later.'
            )));
        }
    }


    function getProductAd() {
        $json_data = $this->input->post('data');
        //$json_data = '{"userID" : "711"}';
        $arrayData = json_decode($json_data);
        
        $userId = $arrayData->userID;
        if ( empty($userId) || !is_numeric($userId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide user identification.'
            )));
            return false;
        }

        $user = $this->main_model->getData('user', 'userId, ads_status', array('userId' => $userId, 'ads_status' => '1'));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect user identification.'
            )));
            return false;
        }
        
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'No Ads applicable for this user.'
            )));
            return false;
        }
        
        $products = $this->main_model->getData('ads_product_ad');
        if ($products) {
            foreach ($products as $row) {
                $data[] = array(
                    'productAd_id' => $row->product_id,
                    'productAd_title' => $row->product_title,
                    'productAd_url' => $row->product_image_url,
                    'productAd_desc' => $row->product_desc,
                    'productAd_redirect_url' => $row->product_redirect_url,
                    'product_impression_value' => $row->product_impression_value,
                    'product_click_value' => $row->product_impression_value
                );
            }
            print_r(json_encode($data));
        } 
        else {
            print_r(json_encode(array(
                'error' => 'Something went wrong. Please try again later.'
            )));
        }
    }


    function getInterstitialAds() {
        $json_data = $this->input->post('data');
        //$json_data = '{"userID" : "711"}';
        $arrayData = json_decode($json_data);
        $userId = $arrayData->userID;
        if ( empty($userId) || !is_numeric($userId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide user identification.'
            )));
            return false;
        }

        $user = $this->main_model->getData('user', 'userId, ads_status', array('userId' => $userId, 'ads_status' => '1'));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect user identification.'
            )));
            return false;
        }
        
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'No Ads applicable for this user.'
            )));
            return false;
        }
        
        $result = $this->main_model->getData('ads_interstitial_ads', 'full_ad_id', '', '', '', '', 'full_ad_id');
        if ($result) {
            foreach ($result as $row) {
                $fullAdId = $row->full_ad_id;
                $fullAd = $this->main_model->getData('ads_ad', '', array('ad_id' => $fullAdId));
                $ads = [];
                $bannerAdResult = $this->main_model->getData('ads_interstitial_ads', '', array('full_ad_id' => $fullAdId));
                foreach ($bannerAdResult as $bannerRow) {
                    $bannerAdId = $bannerRow->banner_ad_id;
                    $bannerAd = $this->main_model->getData('ads_ad', '', array('ad_id' => $bannerAdId));
                    $ads[] = array(
                        'bannerAd_id' => $bannerAd[0]->ad_id,
                        'bannerAd_url' => $bannerAd[0]->ad_link,
                    );
                }
                $data[] = array(
                    'fullAd_id' => $fullAd[0]->ad_id,
                    'fullAd_url' => $fullAd[0]->ad_link,
                    'fullAd_redirect_url' => $fullAd[0]->redirect_link,
                    'ads' => $ads
                );
            }
            print_r(json_encode($data));
        } 
        else {
            print_r(json_encode(array(
                'error' => 'Something went wrong. Please try again later.'
            )));
        }
    }

    function getBulkAds() {
        $json_data = $this->input->post('data');
        //$json_data = '{"userID" : "711"}';
        $arrayData = json_decode($json_data);
        
        $userId = $arrayData->userID;
        if ( empty($userId) || !is_numeric($userId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide user identification.'
            )));
            return false;
        }

        $user = $this->main_model->getData('user', 'userId, ads_status', array('userId' => $userId, 'ads_status' => '1'));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect user identification.'
            )));
            return false;
        }
        
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'No Ads applicable for this user.'
            )));
            return false;
        }
        
        $result = $this->main_model->getData('ads_ad');
        if ($result) {
            foreach ($result as $row) {
                $adType = $this->main_model->getData('ads_ad_type', 'title', array('ad_type_id' => $row->ad_type_id));
                $data[] = array(
                    'ad_ID' => $row->ad_id,
                    'ad_type' => $adType[0]->title,
                    'ad_url' => $row->ad_link,
                    'ad_redirect_url' => $row->redirect_link,
                );
            }
            print_r(json_encode($data));
        } 
        else {
            print_r(json_encode(array(
                'error' => 'Something went wrong. Please try again later.'
            )));
        }
    }


    function logAdView() {
        $json_data = $this->input->post('data');
        //$json_data = '{"adID" : "1", "userID" : "711"}';
        $arrayData = json_decode($json_data);
        
        $userId = $arrayData->userID;
        $adId = $arrayData->adID;
        $screenId = $arrayData->screenID;
        
        if ( empty($userId) || !is_numeric($userId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide user identification.'
            )));
            return false;
        }

        $user = $this->main_model->getData('user', 'userId, ads_status', array('userId' => $userId, 'ads_status' => '1'));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect user identification.'
            )));
            return false;
        }
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'No Ads applicable for this user.'
            )));
            return false;
        }
        //adID
        if ( empty($adId) || !is_numeric($adId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide a target ad.'
            )));
            return false;
        }

        $user = $this->main_model->getData('ads_ad', 'ad_id', array('ad_id' => $adId));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect ad id.'
            )));
            return false;
        }
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'Incorrect ad id.'
            )));
            return false;
        }
        //Screen Id

        if ( empty($screenId) || !is_numeric($screenId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide a screen Id.'
            )));
            return false;
        }
        //Validation

        $jd=cal_to_jd(CAL_GREGORIAN,date("m"),date("d"),date("Y"));
        $weekDay = jddayofweek($jd,1);

        $appId = $this->main_model->getData('ads_screen', 'application_id', array('screen_id' => $screenId));

        $insertArr = array(
            'log_datetime' => date("Y-m-d H:i:s"),
            'week_day' => $weekDay,
            'user_id' => $userId,
            'ad_id' => $adId,
            'screen_id' => $screenId,
            'application_id' => $appId[0]->application_id
        );
        $result = $this->main_model->insert('ads_ad_impression_logger', $insertArr);
        if ($result) {

            $value = $this->main_model->getData('ads_ad', 'current_impressions', array('ad_id' => $adId));
            $newValue = ($value[0]->current_impressions + 1);
            $this->main_model->update('ads_ad', array(
                'current_impressions' => $newValue,
            ) , array(
                'ad_id' => $adId
            ));

            print_r(json_encode(array(
                'log message' => 'ad view logged successfully.'
            )));
        } 
        else {
            print_r(json_encode(array(
                'error' => 'Something went wrong. Please try again later.'
            )));
        }
    }


    function logAdClick() {
        $json_data = $this->input->post('data');
        //$json_data = '{"adID" : "1", "userID" : "711"}';
        $arrayData = json_decode($json_data);
        
        $userId = $arrayData->userID;
        $adId = $arrayData->adID;
        $screenId = $arrayData->screenID;
        if ( empty($userId) || !is_numeric($userId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide user identification.'
            )));
            return false;
        }

        $user = $this->main_model->getData('user', 'userId, ads_status', array('userId' => $userId, 'ads_status' => '1'));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect user identification.'
            )));
            return false;
        }
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'No Ads applicable for this user.'
            )));
            return false;
        }
        //adID
        if ( empty($adId) || !is_numeric($adId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide a target ad.'
            )));
            return false;
        }

        $user = $this->main_model->getData('ads_ad', 'ad_id', array('ad_id' => $adId));
        if (count($user) == 0 && $user == true) {
            print_r(json_encode(array(
                'error' => 'Incorrect ad id.'
            )));
            return false;
        }
        if($user == false)
        {
            print_r(json_encode(array(
                'error' => 'Incorrect ad id.'
            )));
            return false;
        }
        //Screen Id
        if ( empty($screenId) || !is_numeric($screenId) ) {
            print_r(json_encode(array(
                'error' => 'Please provide a screen Id.'
            )));
            return false;
        }
        //Validation

        $jd=cal_to_jd(CAL_GREGORIAN,date("m"),date("d"),date("Y"));
        $weekDay = jddayofweek($jd,1);

        $appId = $this->main_model->getData('ads_screen', 'application_id', array('screen_id' => $screenId));
        $insertArr = array(
            'log_datetime' => date("Y-m-d H:i:s"),
            'week_day' => $weekDay,
            'user_id' => $userId,
            'ad_id' => $adId,
            'screen_id' => $screenId,
            'application_id' => $appId[0]->application_id
        );
        $result = $this->main_model->insert('ads_ad_click_logger', $insertArr);
        if ($result) {
            
            $value = $this->main_model->getData('ads_ad', 'current_clicks', array('ad_id' => $adId));
            $newValue = ($value[0]->current_clicks + 1);
            $this->main_model->update('ads_ad', array(
                'current_clicks' => $newValue,
            ) , array(
                'ad_id' => $adId
            ));

            print_r(json_encode(array(
                'log message' => 'ad click logged successfully.'
            )));
        } 
        else {
            print_r(json_encode(array(
                'error' => 'Something went wrong. Please try again later.'
            )));
        }
    }

}
 // Main Class
