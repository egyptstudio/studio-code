<?php 
Class Help extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('help_model');
	}

	//Functions
	function getHelpScreen()
	{
		//$json_data = $this->input->post('data');
		$json_data ='{"lang" : "ar"}';
		$array_data = json_decode($json_data);
		$array_count = json_decode($json_data, true);

		if(empty($array_data->lang) || count($array_count) != 1)
		{
			$status = array(
				"status" => -1,
			);
			echo json_encode($status);
			return false;
		}

		$lang = $array_data->lang;

		$result = $this->help_model->getHelpScreen($lang);
		if(is_array($result))
		{
			foreach($result as $row)
			{
				$data[] = array(
					'screenTitle' => $row['screenTitle'],
					'screenImageUrl' => $row['screenImageUrl'],
					'screenIconUrl' => $row['screenIconUrl'],
					'description' => $row['description']
				);
			}
			$status = array(
				'status' => 2,
				'data' => $data
			);
		} else 
		{
			$data = array();
			$status = array(
				'status' => 2,
				'data' => $data
			);
		}
		echo json_encode($status);
	}

}// Main Class
?>