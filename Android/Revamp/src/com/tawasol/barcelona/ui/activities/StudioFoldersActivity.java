package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.tawasol.barcelona.R;

public class StudioFoldersActivity extends BaseActivity implements OnClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_studio_folders);
//		hideBottomBar();
		setTitle(getResources().getString(R.string.Gallary));
		showMenuBtn();
		
		getRightBtn().setCompoundDrawablesWithIntrinsicBounds(R.drawable.fan_filter, 0, 0, 0);
		getRightBtn().setText(" ");
		getRightBtn().setOnClickListener(this);
		showRightBtn();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, StudioFoldersActivity.class);
	}
	@Override
	protected void onResume() {
		super.onResume();
		refreshMenu();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rightBtn:
			startActivity(FoldersFilterActivity.getActivityIntent(StudioFoldersActivity.this));
			break;

		default:
			break;
		}
		
	}


}
