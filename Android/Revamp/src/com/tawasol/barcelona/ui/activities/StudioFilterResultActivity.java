package com.tawasol.barcelona.ui.activities;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.Seasons;

public class StudioFilterResultActivity extends BaseActivity{
	
	public static final String FILTER_BY_KEY="FilterBy";
	public static final String SEASONS_KEY="Seasons";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_studio_filter_result);
		showBackBtn();
		setTitle(getString(R.string.filterResult_Results));
	}

	public static Intent getActivityIntent(Context context,int filterBy,ArrayList<Integer> seasons) {
		return new Intent(context, StudioFilterResultActivity.class).
				putExtra(FILTER_BY_KEY, filterBy)
				.putExtra(SEASONS_KEY, seasons);
	}

}
