package com.tawasol.barcelona.ui.activities;
 
import android.content.Context; 
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener; 

import com.tawasol.barcelona.R; 
import com.tawasol.barcelona.managers.UserManager;
/**
 * @author Turki
 * 
 */
public class ChatActivity extends BaseActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);
		setTitle(getResources().getString(R.string.Home_Message));
		
		/** Invite & Search Button **/
		showChatSearchAndInviteLayout();
		getChatInviteFriends().setOnClickListener(this);
		
//		hideChatInviteFriends();
		
		/** Hide Bottom bar**/
//		hideBottomBar();
		hideRightBtn();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, ChatActivity.class);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.chat_invite_friends:
			startActivity(ChatInviteActivity.getActivityIntent(ChatActivity.this));
			break;		
		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		if (UserManager.getInstance().getCurrentUser() != null
				&& UserManager.getInstance().getCurrentUserId() == 0) {
			startActivity(HomeActivity.getActivityIntent(ChatActivity.this));
		} 
		super.onResume();
	}
	
	
}	