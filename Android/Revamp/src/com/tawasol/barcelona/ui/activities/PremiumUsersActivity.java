package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.managers.FanManager;

import android.os.Bundle;
import android.view.View;

public class PremiumUsersActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.premium_users_activity);
		showBackBtn();
		setTitle("Fans");
		showFilterhBtn();
		getFilterBtn().setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(FanFilterActivity.getIntent(PremiumUsersActivity.this, true));	
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		FanManager.getInstance().clearPremium();
		super.onBackPressed();
	}
}
