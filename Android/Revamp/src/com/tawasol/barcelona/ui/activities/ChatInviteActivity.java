package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.data.connection.Params;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * 
 * @author Turki
 *
 */
public class ChatInviteActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_chat_invite);
		setTitle(getResources().getString(R.string.initialMessages_Invite));
		
		/**  Initialize InviteMessage Activity **/
		init();
	}
	
	/**  Get InviteMessageActivity Intent to start it from any activity **/
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, ChatInviteActivity.class);
	}
	
	private void init(){
		/** Hide BottomBar **/
		hideBottomBar();
		
		/** Show RightButton, BackButton**/
		showRightBtn();
		showBackBtn();
		
		getRightBtn().setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.share_ico,0);
	}
}
