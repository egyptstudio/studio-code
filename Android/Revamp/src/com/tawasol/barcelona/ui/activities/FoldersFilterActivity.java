package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class FoldersFilterActivity extends BaseActivity implements OnClickListener {
	@Override
	protected void onCreate(Bundle bundle) {

		super.onCreate(bundle);
		setContentView(R.layout.activity_folder_filter);


		setTitle(getResources().getString(R.string.PhotoFilter_title));
		hideBackBtn();
		 hideBottomBar();
		 hideMenuBtn();
		 
		 getSkipButton().setText("");
		getSkipButton().setCompoundDrawablesWithIntrinsicBounds(0	, 0, R.drawable.close_navigation, 0);
			getSkipButton().setOnClickListener(this);
			showSkipButton();
	}

	

	public static Intent getActivityIntent(Context context) {
		
		return new Intent(context, FoldersFilterActivity.class);
				
	}
	

	@Override
	public void onBackPressed() {
		// set the result to be Canceled
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}



	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.skip:
			finish();
			break;

		default:
			break;
		}
	}
	
}
