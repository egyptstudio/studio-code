package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.barcelona.R;

public class AboutMeActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_about_me);
		setTitle(getString(R.string.MyProfile_MoreAboutMe));
		showBackBtn();
		hideMenuBtn();
		hideBottomBar();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, AboutMeActivity.class);
	}


}
