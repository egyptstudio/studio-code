package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.barcelona.R;

public class ContestActivity extends BaseActivity  {
	public static final int WINNER_INTENT_RESULT_CODE = 27965; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contest);
		setTitle(getResources().getString(R.string.menu_Contest));
//		showBackBtn();
		//hideMenuBtn();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, ContestActivity.class);
	}
	
	/*@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		super.onActivityResult(arg0, arg1, arg2);
		if (arg1 == Activity.RESULT_OK
				&& arg0 == WINNER_INTENT_RESULT_CODE) {
			ContestWinnersFragment winnerFrag = new ContestWinnersFragment();
			winnerFrag.onActivityResult(arg0, arg1, arg2);
		}
	}
*/
}
