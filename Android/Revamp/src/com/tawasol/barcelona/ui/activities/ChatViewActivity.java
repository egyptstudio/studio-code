package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.FriendsEntity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * 
 * @author Turki
 *
 */
public class ChatViewActivity extends BaseActivity {

	public static final String CONTACT_BUNDLE = "Friend_Contact_ID";
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_chat_view);
		/**  Set screen title **/
		setTitle();
		/**  Initialize Inviter Activity **/
		init();
	}
	
	/**  Get ChatViewActivity Intent to start it from any activity **/
	public static Intent getActivityIntent(Context context, FriendsEntity friendObj) {
		return new Intent(context, ChatViewActivity.class)
		.putExtra(CONTACT_BUNDLE, friendObj);
	}
		
	private void init(){
		/** Show BackButton **/
		showBackBtn();
		
		/** Hide BottomBar, BackButton**/
		hideBottomBar();
		hideMenuBtn();
		
	}
	
	private void setTitle(){
		/** set screen title from intent value **/
		FriendsEntity friend = (FriendsEntity) getIntent().getSerializableExtra(ChatViewActivity.CONTACT_BUNDLE);
		setTitle(friend.getFanName());
	}
}
