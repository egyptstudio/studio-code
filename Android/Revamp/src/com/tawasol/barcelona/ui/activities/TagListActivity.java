package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class TagListActivity extends BaseActivity {

	public static final String TAG_ID = "tagId";
	public static final String USER_ID = "userId";
	public static final String IS_TAG_LIST = "isTag";
	public static final String USER_NAME = "userName";
	public static final String TAG_NAME = "tagName";
	public static final String BOTTOM_BAR_HEIGHT = "bottomBarHeight";

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		if(getIntent().getExtras().getString(TAG_NAME)!=null)
			setTitle(getIntent().getExtras().getString(TAG_NAME));
		else if(getIntent().getExtras().getString(USER_NAME)!=null)
			setTitle(getIntent().getExtras().getString(USER_NAME));
		getIntent().putExtra(BOTTOM_BAR_HEIGHT, getBottomBarHeight());
		setContentView(R.layout.activity_tag_list);
		showBackBtn();
		hideMenuBtn();
		hideBottomBar();
	}
	
	public static Intent getIntent(Context context, int tagId, String tagName ,int userId, String userName , 
			boolean tag) {
		return new Intent(context, TagListActivity.class)
				.putExtra(TAG_ID, tagId).putExtra(USER_ID, userId)
				.putExtra(USER_NAME, userName)
				.putExtra(TAG_NAME, tagName)
				.putExtra(IS_TAG_LIST, tag);
	}
}
