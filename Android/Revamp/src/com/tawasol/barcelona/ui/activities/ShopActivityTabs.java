package com.tawasol.barcelona.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.ui.fragments.PremiumGridFragment;
import com.tawasol.barcelona.ui.fragments.ShopFragmentTabs;

public class ShopActivityTabs extends BaseActivity{

	public static final int POINTS_REQUEST_CODE = 485625;
	public static final int PREMIUM_REQUEST_CODE = 485625;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shop_activity_tabs);
//		hideBottomBar();
		setTitle(getResources().getString(R.string.Home_Shop));
		showMenuBtn();
	}
	
	public static Intent getIntent(Context context){
		return new Intent(context, ShopActivityTabs.class);
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {

			ShopFragmentTabs fragment = (ShopFragmentTabs) getSupportFragmentManager()
					.findFragmentById(R.id.shop_fragment_id);
			fragment.onActivityResult(requestCode, resultCode, data);
			// refreshMenu();
		}
	}
	
}
