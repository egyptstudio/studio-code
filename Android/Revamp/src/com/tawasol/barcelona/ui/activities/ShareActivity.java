package com.tawasol.barcelona.ui.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.aviary.android.feather.FeatherActivity;
import com.aviary.android.feather.common.utils.StringUtils;
import com.aviary.android.feather.library.Constants;
import com.aviary.android.feather.library.filters.FilterLoaderFactory;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.data.cache.WallTable;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.entities.KeyValuePairs;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.entities.StudioPhoto;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.facebook.DialogErrorCustom;
import com.tawasol.barcelona.facebook.FacebookCustom.DialogListener;
import com.tawasol.barcelona.facebook.FacebookErrorCustom;
import com.tawasol.barcelona.listeners.OnLoginResponseListener;
import com.tawasol.barcelona.listeners.OnShareListener;
import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.dialogs.CustomAlertDialog;
import com.tawasol.barcelona.utils.Custom_FB_Util;
import com.tawasol.barcelona.utils.Twitter_Util;
import com.tawasol.barcelona.utils.UIUtils;
import com.tawasol.barcelona.utils.Utils;
import com.tawasol.barcelona.utils.ValidatorUtils;

/**
 * @author Mohga
 * */

@SuppressLint("NewApi")
public class ShareActivity extends BaseActivity implements
		OnSeekBarChangeListener, android.view.View.OnClickListener,
		OnLoginResponseListener, OnShareListener, OnSuccessVoidListener,
		DialogListener {

	public static final String STUDIO_PHOTO_OBJECT_BUNDLE = "StudioPhoto";
	public static final String STUDIO_PHOTO_IMG_PATH_BUNDLE = "ImagePath";
	public static final String PHOTO_ORIANTATION = "oriantation";
	public static final String ORIGINAL_PIC = "original";
	public static final String OPEN_AVIARY = "OpenAviary";
	public static final String SHARE_SCREEN = "shareScreen";
	public static final String POST_ID = "postID";
	private static final String IMAGE_FILE_PATH = "SavedImagePath";
	private static final String EDITED_FILE_PATH = "EditedImagePath";
	private static final String OPEN_ADD_POSTER = "OpenAddPoster";
	private static final String CAMERA_WIDTH = "cameraWidth";

	private String mApiKey;
	private static final int EXTERNAL_STORAGE_UNAVAILABLE = 1;
	private static final String API_SECRET = "XXXXX";
	private String mSessionId;
	public static final String LOG_TAG = "aviary-launcher";
	private boolean firstTime = true;
	private static final int ACTION_REQUEST_FEATHER = 100;
	private static final int ACTION_REQUEST_LOGIN = 101;
	private String newFile = "";
	String afterChangePath,originalImgPath;
	private String mySessionId;

	// controls
	ImageView capturedPhoto;
	EditText photoCaption;
	EditText tagTxt;
	ImageButton addTagBtn;
	HorizontalScrollView tagsHSV;
	LinearLayout tagsHSVContainer;
	SeekBar privacySeekBar;
	ImageView contestImgIndicator;
	TextView contestTxtIndicator;
	Button twitterBtn;
	LoginButton facebookBtn;
	ImageView facebookImg;
	TextView sharingStatusTxt;
	TextView sharingPointsStatus;
	Button shareBtn;
	ProgressBar sharePB;
	LinearLayout contestLayout;
	private ProgressDialog mDialog;
	Custom_FB_Util customFB;
	public static String shareLink="http://fcbstudio.mobi/share.php?postid=";//"http://fcbstudio.mobi/share.php?id=";

	// sharing credit
	int sharingCredit = 0;

	// bundle data
	StudioPhoto photo;
	String photoPath;
	boolean openAviary;
	int cameraWidth;

	boolean isTwitterSelected, isFaceBookSelected;
	Twitter_Util twitterUtil;

	int sharingPoints;
	private static String photoCaptionTxt;

	// Define Facebook variables
	// define the publish permission and life-cycle helper
	private static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");
	private UiLifecycleHelper uiHelper;

	// the session callback change
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
	private String orientation;
	private String finalImageWithWaterMark;

	@Override
	protected void onCreate(Bundle arg0) {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		super.onCreate(arg0);
		setContentView(R.layout.activity_share);
		setTitle(getResources().getString(R.string.AboutUs_Share));
		showBackBtn();
		getBackButton().setOnClickListener(this);
		hideBottomBar();
		hideMenuBtn();

		getBundleData();
		if (openAviary)
			startFeather(Uri.fromFile(new File(photoPath)));
		viewByID();
		initializeViewControls();
		// initialize Twitter Utilities
		twitterUtil = new Twitter_Util(this);

		// initialize the life-cycle callback with bundle
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(arg0);

		if (!isFBInstalled()) {
			facebookBtn.setVisibility(View.GONE);
			facebookImg.setVisibility(View.VISIBLE);
		}

	}

	private boolean isExternalStorageAvilable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}

	/**
	 * Once you've chosen an image you can start the feather activity
	 * 
	 * @param uri
	 */
	@SuppressWarnings("deprecation")
	private void startFeather(Uri uri) {
		Log.d(LOG_TAG, "uri: " + uri);

		// first check the external storage availability
		if (!isExternalStorageAvilable()) {
			showDialog(EXTERNAL_STORAGE_UNAVAILABLE);
			return;
		}

		// // create a temporary file where to store the resulting image
		// File file = getNextFileName();
		//
		// if (null != file) {
		// path = file.getAbsolutePath();
		// } else {
		// new AlertDialog.Builder(this)
		// .setTitle(android.R.string.dialog_alert_title)
		// .setMessage("Failed to create a new File").show();
		// return;
		// }

		// Create the intent needed to start feather
		Intent newIntent = new Intent(this, FeatherActivity.class);

		// === INPUT IMAGE URI (MANDATORY) ===
		// Set the source image uri
		// if (firstTime)
		newIntent.setData(uri);
		// else
		// newIntent.setData(Uri.parse(afterChangePath));
		// === API KEY SECRET (MANDATORY) ====
		// You must pass your Aviary key secret
		newIntent.putExtra(Constants.EXTRA_IN_API_KEY_SECRET, API_SECRET);

		// === OUTPUT (OPTIONAL/RECOMMENDED)====
		// Pass the uri of the destination image file.
		// This will be the same uri you will receive in the onActivityResult7
		newIntent.putExtra(Constants.EXTRA_OUTPUT,
				Uri.parse("file://" + photoPath));

		// === OUTPUT FORMAT (OPTIONAL) ===
		// Format of the destination image
		newIntent.putExtra(Constants.EXTRA_OUTPUT_FORMAT,
				Bitmap.CompressFormat.JPEG.name());

		// === OUTPUT QUALITY (OPTIONAL) ===
		// Output format quality (jpeg only)
		newIntent.putExtra(Constants.EXTRA_OUTPUT_QUALITY, 90);

		// === WHITE LABEL (OPTIONAL/PREMIUM ONLY) ===
		// If you want to hide the 'feedback' button and the 'aviary' logo
		// pass this intent-extra
		// Note that you need to have the 'whitelabel' permissions enabled in
		// order
		// to use this extra
		newIntent.putExtra(Constants.EXTRA_WHITELABEL, true);

		// == TOOLS LIST ===
		// Optional
		// You can force feather to display only some tools ( see
		// FilterLoaderFactory#Filters )
		// you can omit this if you just want to display the default tools

		newIntent.putExtra("tools-list", new String[] {
				// FilterLoaderFactory.Filters.ENHANCE.name(),
				FilterLoaderFactory.Filters.EFFECTS.name(),
				// FilterLoaderFactory.Filters.STICKERS.name(),
				FilterLoaderFactory.Filters.CROP.name(),
				FilterLoaderFactory.Filters.TILT_SHIFT.name(),
				FilterLoaderFactory.Filters.ADJUST.name(),
				FilterLoaderFactory.Filters.BRIGHTNESS.name(),
				FilterLoaderFactory.Filters.CONTRAST.name(),
				FilterLoaderFactory.Filters.SATURATION.name(),
				FilterLoaderFactory.Filters.COLORTEMP.name(),
				// FilterLoaderFactory.Filters.SHARPNESS.name(),
				FilterLoaderFactory.Filters.COLOR_SPLASH.name(),
				FilterLoaderFactory.Filters.DRAWING.name(),
				FilterLoaderFactory.Filters.TEXT.name(),
				FilterLoaderFactory.Filters.RED_EYE.name(),
				FilterLoaderFactory.Filters.WHITEN.name(),
				FilterLoaderFactory.Filters.BLEMISH.name(),
				FilterLoaderFactory.Filters.MEME.name(), });

		// === EXIT ALERT (OPTIONAL) ===
		// Uou want to hide the exit alert dialog shown when back is pressed
		// without saving image first
		// newIntent.putExtra( Constants.EXTRA_HIDE_EXIT_UNSAVE_CONFIRMATION,
		// true );

		// === VIBRATION (OPTIONAL) ===
		// Some aviary tools use the device vibration in order to give a better
		// experience
		// to the final user. But if you want to disable this feature, just pass
		// any value with the key "tools-vibration-disabled" in the calling
		// intent.
		// This option has been added to version 2.1.5 of the Aviary SDK
		// newIntent.putExtra( Constants.EXTRA_TOOLS_DISABLE_VIBRATION, true );

		// === MAX SIZE (OPTIONAL) ===
		// you can pass the maximum allowed image size (for the preview),
		// otherwise feather will determine
		// the max size based on the device informations.
		// This will not affect the hi-res image size.
		// Here we're passing the current display size as max image size because
		// after
		// the execution of Aviary we're saving the HI-RES image so we don't
		// need a big
		// image for the preview
		final DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int max_size = Math.max(metrics.widthPixels, metrics.heightPixels);
		max_size = (int) ((float) max_size / 1.2f);
		newIntent.putExtra(Constants.EXTRA_MAX_IMAGE_SIZE, max_size);

		// === HI-RES (OPTIONAL) ===
		// You need to generate a new session id key to pass to Aviary feather
		// this is the key used to operate with the hi-res image ( and must be
		// unique for every new instance of Feather )
		// The session-id key must be 64 char length.
		// In your "onActivityResult" method, if the resultCode is RESULT_OK,
		// the returned
		// bundle data will also contain the "session" key/value you are passing
		// here.
		mSessionId = StringUtils
				.getSha256(System.currentTimeMillis() + mApiKey);
		Log.d(LOG_TAG,
				"session: " + mSessionId + ", size: " + mSessionId.length());
		newIntent.putExtra(Constants.EXTRA_OUTPUT_HIRES_SESSION_ID, mSessionId);

		// === NO CHANGES (OPTIONAL) ==
		// With this extra param you can tell to FeatherActivity how to manage
		// the press on the Done button even when no real changes were made to
		// the image.
		// If the value is true then the image will be always saved, a RESULT_OK
		// will be returned to your onActivityResult and the result Bundle will
		// contain an extra value "EXTRA_OUT_BITMAP_CHANGED" indicating if the
		// image was changed during the session.
		// If "false" is passed then a RESULT_CANCEL will be sent when an user
		// will
		// hit the 'Done' button without any modifications ( also the
		// EXTRA_OUT_BITMAP_CHANGED
		// extra will be sent back to the onActivityResult.
		// By default this value is true ( even if you omit it )
		newIntent.putExtra(Constants.EXTRA_IN_SAVE_ON_NO_CHANGES, true);

		// ..and start feather
		startActivityForResult(newIntent, ACTION_REQUEST_FEATHER);
		// Share.this.finish();
		// dialog2.dismiss();
	}

	private void viewByID() {
		capturedPhoto = (ImageView) findViewById(R.id.shared_img);
		photoCaption = (EditText) findViewById(R.id.photo_caption_txt);
		tagTxt = (EditText) findViewById(R.id.tag_txt);
		addTagBtn = (ImageButton) findViewById(R.id.add_tag_imd);
		tagsHSV = (HorizontalScrollView) findViewById(R.id.horizontalScrollView1);
		tagsHSVContainer = (LinearLayout) findViewById(R.id.TagsContainer);
		privacySeekBar = (SeekBar) findViewById(R.id.privacy_seekBar);
		contestImgIndicator = (ImageView) findViewById(R.id.contest_img);
		contestTxtIndicator = (TextView) findViewById(R.id.contest_txt);
		twitterBtn = (Button) findViewById(R.id.twitter_btn);
		facebookImg = (ImageView) findViewById(R.id.fb_img_id);
		facebookBtn = (LoginButton) findViewById(R.id.facebook_btn);
		sharingStatusTxt = (TextView) findViewById(R.id.sharing_status_txt);
		sharingPointsStatus = (TextView) findViewById(R.id.sharing_points_txt);
		shareBtn = (Button) findViewById(R.id.posting_btn);
		sharePB = (ProgressBar) findViewById(R.id.share_pb);
		contestLayout = (LinearLayout) findViewById(R.id.contest_layout);
	}

	public static Intent getActivityIntent(Context context, StudioPhoto photo,
			String imagePath, String oriantation, String original,
			boolean openAviary , boolean share , int postId,boolean openAddPoster , int cameraWidth) {
		return new Intent(context, ShareActivity.class)
				.putExtra(STUDIO_PHOTO_OBJECT_BUNDLE, photo)
				.putExtra(STUDIO_PHOTO_IMG_PATH_BUNDLE, imagePath)
				.putExtra(PHOTO_ORIANTATION, oriantation)
				.putExtra(ORIGINAL_PIC, original)
				.putExtra(OPEN_AVIARY, openAviary)
				.putExtra(SHARE_SCREEN, share)
				.putExtra(POST_ID, postId)
				.putExtra(OPEN_ADD_POSTER, openAddPoster)
				.putExtra(CAMERA_WIDTH, cameraWidth);
	}

	/** Basyouni's Method to add water mark to image */
	public Bitmap addWaterMark(Bitmap image) {
		Bitmap bitmap = null;
		try {

			bitmap = Bitmap.createBitmap(image.getWidth(), image.getHeight(),
					Config.ARGB_8888);
			Canvas c = new Canvas(bitmap);
			Resources res = getResources();

			Bitmap bitmap1 = image;// BitmapFactory.decodeResource(res,
									// R.drawable.test1); //blue
			Bitmap bitmap2 = null;

			if (photo.getCaptureType() == StudioPhoto.PHOTO_CAPTURE_TYPE_MASK_SHOT) {
				if (photo.getPhotoOrientation() == StudioPhoto.STUDIO_PHOTO_ORIENTATION_PORTRAIT)
					bitmap2 = BitmapFactory.decodeResource(res,
							R.drawable.water_mark_port); // green
				else if (photo.getPhotoOrientation() == StudioPhoto.STUDIO_PHOTO_ORIENTATION_LANDSCAPE)
					bitmap2 = BitmapFactory.decodeResource(res,
							R.drawable.water_mark_land); // green
			} else if (photo.getCaptureType() == StudioPhoto.PHOTO_CAPTURE_TYPE_SCREEN_SHOT) {
				if (("portrait").equalsIgnoreCase(orientation))
					bitmap2 = BitmapFactory.decodeResource(res,
							R.drawable.water_mark_port); // green
				else
					bitmap2 = BitmapFactory.decodeResource(res,
							R.drawable.water_mark_land); // green
			}

			Drawable drawable1 = new BitmapDrawable(getResources(), bitmap1);
			Drawable drawable2 = new BitmapDrawable(getResources(), bitmap2);

			drawable1.setBounds(0, 0, image.getWidth(), image.getHeight());
			// drawable2.setBounds(0,
			// image.getHeight() - (bitmap2.getHeight()),
			// image.getWidth(), image.getHeight());
			drawable2.setBounds(0, 0, image.getWidth(), image.getHeight());

			drawable1.draw(c);
			drawable2.draw(c);

		} catch (Exception e) {
		}
		return bitmap;
	}

	private void initializeViewControls() {
		/** Set the image source */
		// get the bitmap from photo path

		 capturedPhoto.setImageBitmap(BitmapFactory.decodeFile(photoPath));

		// Bitmap imgBitmap = BitmapFactory.decodeFile(photoPath);
		// capturedPhoto.setImageBitmap(imgBitmap);
		if (!openAviary) {
			// ContextWrapper cw = new ContextWrapper(this);
			// File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
			// File mypath = new File(directory, "fcbPhoto.jpg");
			// finalImageWithWaterMark = Environment
			// .getExternalStorageDirectory().getAbsolutePath()
			// + File.separator
			// + "FCBStudio"
			// + File.separator
			// + "Barchelona"
			// + System.currentTimeMillis() + ".jpg";//
			// mypath.getAbsolutePath();

			// String waterMarkPath =
			// saveToInternalSorage(addWaterMark(BitmapFactory
			// .decodeFile(photoPath)));
			// File tmpFile = new File(waterMarkPath, "fcbPhoto.jpg");
			// finalImageWithWaterMark = tmpFile.getAbsolutePath();
			// capturedPhoto.setImageBitmap(addWaterMark(BitmapFactory
			// .decodeFile(photoPath)));

			// String waterMarkPath =
			// saveToInternalSorage(addWaterMark(BitmapFactory
			// .decodeFile(photoPath)));
			// File tmpFile = new File(waterMarkPath, "fcbPhoto.jpg");
			// finalImageWithWaterMark = tmpFile.getAbsolutePath();
			//
			// capturedPhoto.setImageBitmap(BitmapFactory
			// .decodeFile(finalImageWithWaterMark));

//			finalImageWithWaterMark = Environment.getExternalStorageDirectory()
//					.getAbsolutePath()
//					+ File.separator
//					+ "FCBStudio"
//					+ File.separator
//					+ "Barchelona"
//					+ System.currentTimeMillis() + ".jpg";// mypath.getAbsolutePath();
//			capturedPhoto.setImageBitmap(BitmapFactory
//					.decodeFile(finalImageWithWaterMark));

		}

		/** Set the tags in Horizontal Scroll View */
		// inflate the tag view item , get the controls of the view
		// set the tag name , then add the inflated view
		if (photo.getTags() != null) {
			for (KeyValuePairs tag : photo.getTags()) {

				final View view = getLayoutInflater().inflate(
						R.layout.share_tag_item, null);
				TextView tagName = (TextView) view
						.findViewById(R.id.tag_name_txt);
				if (tag.getText() != null
						&& !((String) tag.getText()).isEmpty()) {
					tagName.setText((String) tag.getText());
					if (tag.get_Id() == -1 && tag.getType() == 2) {
						ImageButton removeTagBtn = (ImageButton) view
								.findViewById(R.id.remove_tag_btn);
						removeTagBtn.setVisibility(View.VISIBLE);
						removeTagBtn
								.setOnClickListener(new View.OnClickListener() {

									@Override
									public void onClick(View v) {
										photo.getTags()
												.remove(Integer
														.toString(((ViewGroup) view
																.getParent())
																.indexOfChild(view)));
										((ViewManager) view.getParent())
												.removeView(view);
									}
								});
					}
					tagsHSVContainer.addView(view);
				}

			}
		}

		/** check if user has complete profile */
		if(UserManager.getInstance().getCurrentUserId() !=0){
		if (!UserManager.getInstance().canJoinContest()) {
			new CustomAlertDialog.customBuilder(this)
					.setMessage(R.string.complete_profile_msg)
					.setPositiveButton(android.R.string.ok,
							new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							}).create().show();
		} else if (!UserManager.getInstance().getCurrentUser()
				.isEmailVerified()) {
			new CustomAlertDialog.customBuilder(this)
					.setMessage(R.string.HomeUnconfirmedEmailAndPhone_emailMessage)
					.setPositiveButton(android.R.string.ok,
							new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							}).create().show();
		} else if (!UserManager.getInstance().getCurrentUser()
				.isPhoneVerified()) {

			new CustomAlertDialog.customBuilder(this)
					.setMessage(R.string.HomeUnconfirmedEmailAndPhone_phoneMessage)
					.setPositiveButton(android.R.string.ok,
							new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							}).create().show();
		}
		}

		// set facebook button and image
		facebookBtn.setBackgroundResource(R.drawable.facebook_edit);
		facebookBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

		/** Set the Contest Flag Color */
		SetContestIndicator();

		// initialize the sharing points variable
		// sharingPoints =0;

		// set the listeners
		privacySeekBar.setOnSeekBarChangeListener(this);
		addTagBtn.setOnClickListener(this);
		facebookImg.setOnClickListener(this);
		shareBtn.setOnClickListener(this);
		twitterBtn.setOnClickListener(this);
		contestLayout.setOnClickListener(this);

		photoCaption.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				photoCaptionTxt = photoCaption.getText().toString();
			}
		});
		
		tagTxt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(s.toString().startsWith(" "))
					tagTxt.setText("");
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		photoCaption.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(s.toString().startsWith(" "))
					photoCaption.setText("");
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
			}
		});

	}

	// save to internal storage
	private String saveToInternalSorage(Bitmap bitmapImage) {
		ContextWrapper cw = new ContextWrapper(this);
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		// Create imageDir
		File mypath = new File(directory, "fcbPhoto.jpg");

		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(mypath);

			// Use the compress method on the BitMap object to write image to
			// the OutputStream
			if(cameraWidth < 3500)
				bitmapImage.compress(CompressFormat.JPEG, 80, fos);
			else if (cameraWidth > 3500 && cameraWidth < 4200)
				bitmapImage.compress(CompressFormat.JPEG, 60, fos);
			else
				bitmapImage.compress(CompressFormat.JPEG, 40, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return directory.getAbsolutePath();
	}

	private void SetContestIndicator() {
		// Check if user in Registration Level 2
		if (UserManager.getInstance().canJoinContest()) {
			// Check if Privacy is "public" --> Green Flag
			if (privacySeekBar.getProgress() == 2) {
				contestImgIndicator.setImageResource(R.drawable.contest_green);
				contestTxtIndicator.setText(getResources().getString(
						R.string.EditMyPhoto_contestEligabilityState1));
				contestTxtIndicator.setTextColor(getResources().getColor(
						R.color.green));
			}
			// Check if Privacy is not "public" --> Yellow Flag
			else {
				contestImgIndicator.setImageResource(R.drawable.contest_yellow);
				contestTxtIndicator.setText(getResources().getString(
						R.string.EditMyPhoto_contestEligabilityState3));
				contestTxtIndicator.setTextColor(getResources().getColor(
						R.color.yellow));
			}
		}
		// Check if user has incomplete profile
		else {
			// Check if Privacy is "public" --> Red Flag
			if (privacySeekBar.getProgress() == 2) {
				contestImgIndicator.setImageResource(R.drawable.contest_red);
				contestTxtIndicator.setText(getResources().getString(
						R.string.EditMyPhoto_contestEligabilityState2));
				contestTxtIndicator.setTextColor(Color.RED);
			}
			// Check if Privacy is "public" --> Blue Flag
			else {
				contestImgIndicator.setImageResource(R.drawable.contest_blue);
				contestTxtIndicator.setText(getResources().getString(
						R.string.EditMyPhoto_contestEligabilityState4));
				contestTxtIndicator.setTextColor(getResources().getColor(
						R.color.blue));
			}
		}
	}

	private void getBundleData() {
		photo = (StudioPhoto) getIntent().getExtras().getSerializable(
				ShareActivity.STUDIO_PHOTO_OBJECT_BUNDLE);
		photoPath = getIntent().getExtras().getString(
				ShareActivity.STUDIO_PHOTO_IMG_PATH_BUNDLE);
		orientation = getIntent().getExtras().getString(
				ShareActivity.PHOTO_ORIANTATION);
		openAviary = getIntent().getExtras().getBoolean(
				ShareActivity.OPEN_AVIARY);
		originalImgPath = getIntent().getExtras().getString(ORIGINAL_PIC);
		cameraWidth = getIntent().getExtras().getInt(CAMERA_WIDTH);
	}

	private boolean isFBInstalled() {
		try {
			ApplicationInfo info = getPackageManager().getApplicationInfo(
					"com.facebook.katana", 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	private void handleTwitter() {
		if (twitterUtil.isTwitterLoggedInAlready()) {
			if (!isTwitterSelected) {
				twitterBtn.setBackgroundResource(R.drawable.twitter_edit_sc);
				isTwitterSelected = true;
				// sharingPoints +=50;
			} else {
				twitterBtn.setBackgroundResource(R.drawable.twitter_edit);
				isTwitterSelected = false;
				// sharingPoints -=50;
			}
			//handleSharingStatus();
		} else {
			twitterUtil.loginToTwitter(this, true);
			// TODO after login should change twitter button to selected, and
			// set isTwitterSelected=true in listener response :)
		}
	}

	private void handleSharingStatus() {
		if (isFaceBookSelected) {
			if (isTwitterSelected) {
				sharingStatusTxt.setText(getString(R.string.sharing_on_fb_tw));
				sharingPointsStatus.setText("+ 100 "
						+ getString(R.string.studio_xp));
				sharingCredit = 100;
			} else {
				sharingStatusTxt.setText(getString(R.string.sharing_on_fb));
				sharingPointsStatus.setText("+ 50 "
						+ getString(R.string.studio_xp));
				sharingCredit = 50;
			}
		} else {
			if (isTwitterSelected) {
				sharingStatusTxt.setText(getString(R.string.sharing_on_tw));
				sharingPointsStatus.setText("+ 50 "
						+ getString(R.string.studio_xp));
				sharingCredit = 50;
			} else {
				sharingStatusTxt.setText("");
				sharingPointsStatus.setText("");
				sharingCredit = 0;
			}
		}

	}

	private void backKeyPressed() {
		if(openAviary)
			startFeather(Uri.fromFile(new File(finalImageWithWaterMark)));
		else //  edit post 
		{
			DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						ShareActivity.super.onBackPressed();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						dialog.dismiss();
						break;
					}
				}
			};
			Dialog dialog = new CustomAlertDialog.customBuilder(this).setTitle(R.string.exit)
					.setMessage(R.string.save_changes_msg)
					.setPositiveButton(android.R.string.ok, listener)
					.setNegativeButton(android.R.string.cancel, listener).create();
			dialog.show();
		}
	}

	private void navigateToHome() {
		switch (privacySeekBar.getProgress()) {
		case 0: // ONLY Me ==> My Pics
			UIUtils.showToast(this,
					getResources().getString(R.string.share2_affirm1));
			startActivity(HomeActivity.getActivityIntent(this,
					FragmentInfo.MY_PIC , true));
			break;
		case 1: // Friends ==> My Pics
			UIUtils.showToast(this,
					getResources().getString(R.string.share2_affirm2));
			startActivity(HomeActivity.getActivityIntent(this,
					FragmentInfo.MY_PIC , true));
			break;
		case 2: // Public ==> Latest
			UIUtils.showToast(this,
					getResources().getString(R.string.share2_affirm3));
			startActivity(HomeActivity.getActivityIntent(this,
					FragmentInfo.LATEST_TAB , false));
			break;

		default:
			break;
		}
	}

	/**
	 * -------------------- Facebook Configuration methods part
	 * --------------------
	 **/
	@Override
	protected void onPause() {
		super.onPause();
		uiHelper.onPause();
		StudioManager.getInstance().removeListener(this);
		WallManager.getInstance().removeListener(this);

	}

	@Override
	public void onResume() {
		super.onResume();
		StudioManager.getInstance().addListener(this);
		WallManager.getInstance().addListener(this);
		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		else 
		{
			facebookBtn.setTag(true);
		}
		uiHelper.onResume();
		if (!ValidatorUtils.isRequired(photoCaptionTxt))
			photoCaption.setText(photoCaptionTxt);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// uiHelper.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case ACTION_REQUEST_FEATHER:
				boolean changed = true;

				if (null != data) {
					File f = new File(data.getData().toString());
					String[] parts = f.getPath().split(":/");
					afterChangePath = parts[1];

					firstTime = false;
					Bundle extra = data.getExtras();
					if (null != extra) {
						// image was changed by the user?
						changed = extra
								.getBoolean(Constants.EXTRA_OUT_BITMAP_CHANGED);
					}
				}
				if (mSessionId != null)
					mySessionId = mSessionId;
				String waterMarkPath = saveToInternalSorage(addWaterMark(BitmapFactory
						.decodeFile(photoPath)));
				File tmpFile = new File(waterMarkPath, "fcbPhoto.jpg");
				finalImageWithWaterMark = tmpFile.getAbsolutePath();
				capturedPhoto.setImageBitmap(BitmapFactory
						.decodeFile(finalImageWithWaterMark));
				break;

				
			case ACTION_REQUEST_LOGIN:
				if (data != null) {
					finalImageWithWaterMark = data
							.getStringExtra(IMAGE_FILE_PATH);
					capturedPhoto.setImageBitmap(BitmapFactory
							.decodeFile(finalImageWithWaterMark));
					photoPath = data.getStringExtra(EDITED_FILE_PATH);
				}
				break;
			}
		} else if (resultCode == RESULT_CANCELED) {
			switch (requestCode) {
			case ACTION_REQUEST_FEATHER:
				ShareActivity.this.finish();
				break;
			case ACTION_REQUEST_LOGIN:
				deleteFileNoThrow(newFile);
				break;
			}

		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			// first hide the FB login button and show the other one
			facebookBtn.setVisibility(View.GONE);
			facebookImg.setVisibility(View.VISIBLE);

			// Check for publish permissions
			List<String> permissions = session.getPermissions();
			if (!isSubsetOf(PERMISSIONS, permissions))
				addPublishPermission(session);
			else {
				// TODO need to check first if the button was clicked or not
				// "to solve the issue of sharing successfully then exist the app and open it again "
				if((Boolean)facebookBtn.getTag() != null){
				if (!isFaceBookSelected && (Boolean)facebookBtn.getTag()) {
					facebookImg.setImageResource(R.drawable.facebook_edit_sc);
					isFaceBookSelected = true;
					//handleSharingStatus();
					//facebookBtn.setPressed(false);
					facebookBtn.setTag(false);
				}
				}
			}
		}
	}

	private void publishStory(String Caption, String PostId) {
		Session session = Session.getActiveSession();
		if (session != null) {

			Bundle postParams = new Bundle();

			postParams.putString("caption", Caption);
			postParams.putString("link",
					shareLink + PostId);

			Request.Callback callback = new Request.Callback() {
				public void onCompleted(Response response) {
					JSONObject graphResponse;
					try {
						graphResponse = response.getGraphObject()
								.getInnerJSONObject();
					} catch (NullPointerException e) {
						Toast.makeText(ShareActivity.this,
								"Something wrong in response ! ",
								Toast.LENGTH_LONG).show();
						return;
					}
					String postId = null;
					try {
						postId = graphResponse.getString("id");
					} catch (JSONException e) {
						Log.i("JSON RESPONSE : ",
								"JSON error " + e.getMessage());
					}
					FacebookRequestError error = response.getError();
					if (error != null) {
						Toast.makeText(ShareActivity.this,
								error.getErrorMessage(), Toast.LENGTH_SHORT)
								.show();
					} else {
						/*
						 * Toast.makeText(ShareActivity.this,
						 * "Facebook postId : " + postId,
						 * Toast.LENGTH_LONG).show();
						 */
						if(openAviary && getIntent().getBooleanExtra(OPEN_ADD_POSTER, true)) // post creating (from use studio or use wall )
						StudioManager.getInstance().shareToSocilaMedia();
					}
				}
			};

			Request request = new Request(session, "me/feed", postParams,
					HttpMethod.POST, callback);

			RequestAsyncTask task = new RequestAsyncTask(request);
			task.execute();
		}
	}

	private boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	private void addPublishPermission(Session session) {
		Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
				this, PERMISSIONS);
		try {
			session.requestNewPublishPermissions(newPermissionsRequest);
		} catch (UnsupportedOperationException e) {
			// UIUtils.showToast(this,
			// getResources().getString(R.string.requesting_permission_error));
		}

	}

	/**
	 * ------------------------ End of Facebook Configuration methods part
	 * --------------------------
	 **/

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		SetContestIndicator();
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onException(AppException ex) {
		// sharePB.setVisibility(View.GONE);
		hideLoader();
		if (ex.getMessage() != null && !ex.getMessage().isEmpty())
			UIUtils.showToast(this, ex.getMessage());
	}

	@Override
	public void onLoginSuccess(User user, int LoginState) {
		twitterBtn.setBackgroundResource(R.drawable.twitter_edit_sc);
		isTwitterSelected = true;
		//handleSharingStatus();
	}

	@Override
	public void onLoginWithSocialMediaNavigation(User user) {
	}

	@Override
	public void onSuccess(PostViewModel obj) {
		hideLoader();
		
		// cache the returned post in Database
		WallTable.getInstance().insert(obj);
		
		// sharePB.setVisibility(View.GONE);
		// UIUtils.showToast(this, obj.getPostId() + "");
		Log.v("Post : ", obj.getPostId() + "");
		if(getIntent().getBooleanExtra(OPEN_ADD_POSTER, true))
			startActivity(AddPosterActivity.getActivityIntent(this, originalImgPath, finalImageWithWaterMark, privacySeekBar.getProgress()));
		else
		navigateToHome();
		
		// twitter share
		if (isTwitterSelected) {
			try {
				twitterUtil.shareLink("Download App", obj.getPostId() + "",(openAviary && getIntent().getBooleanExtra(OPEN_ADD_POSTER, true))); 
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// facebook share
		if (isFaceBookSelected) {
			if (isFBInstalled())
				publishStory("Download App", obj.getPostId() + "");
			else
				customFB.PublishStory("Download App", obj.getPostId() + "",(openAviary && getIntent().getBooleanExtra(OPEN_ADD_POSTER, true)));
		}

		/*if (sharingCredit != 0)
			StudioManager.getInstance().updateUserCredit(sharingCredit);*/
		photoCaptionTxt = "";
		
		
	}

	@Override
	public void onSuccess() {
	}

	@Override
	public void onBackPressed() {
		backKeyPressed();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_tag_imd:
			if (tagTxt.getText().toString() == null
					|| tagTxt.getText().toString().trim().isEmpty())
				UIUtils.showToast(ShareActivity.this,
						getResources().getString(R.string.tag_text_required));
			else {

				final View view = getLayoutInflater().inflate(
						R.layout.share_tag_item, null);
				TextView tagNameTxt = (TextView) view
						.findViewById(R.id.tag_name_txt);
				tagNameTxt.setText(tagTxt.getText().toString());
				ImageButton removeTagBtn = (ImageButton) view
						.findViewById(R.id.remove_tag_btn);
				removeTagBtn.setVisibility(View.VISIBLE);
				removeTagBtn.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						photo.getTags().remove(
								Integer.toString(((ViewGroup) view.getParent())
										.indexOfChild(view)));
						((ViewManager) view.getParent()).removeView(view);
					}
				});

				tagsHSVContainer.addView(view, 0);

				// set the tag properties
				KeyValuePairs tag = new KeyValuePairs();
				tag.set_Id(-1);
				tag.setType(2);
				tag.setText(tagTxt.getText().toString());

				// add tag to tag list of photo
				photo.getTags().add(tag);
				tagTxt.setText("");
			}
			break;
		case R.id.twitter_btn:
			handleTwitter();
			break;
		case R.id.fb_img_id:
			if (isFBInstalled()) {
				Session session = Session.getActiveSession();
				if (session.isOpened()) {
					List<String> permissions = session.getPermissions();
					// do{
					// Check for publish permissions
					if (!isSubsetOf(PERMISSIONS, permissions))
						addPublishPermission(session);
					// }
					// while(!isSubsetOf(PERMISSIONS, permissions));

				}
				if (!isFaceBookSelected) {
					facebookImg.setImageResource(R.drawable.facebook_edit_sc);
					isFaceBookSelected = true;
				} else {
					facebookImg.setImageResource(R.drawable.facebook_edit);
					isFaceBookSelected = false;
				}
				//handleSharingStatus();
			} else {
				// UIUtils.showToast(
				// this,
				// getResources().getString(
				// R.string.share_fb_not_installed));
				customFB = new Custom_FB_Util(this);
				if (!customFB.isSessionValid()) {
					customFB.loginAndAutherize(this);
				} else {
					facebookImg.setImageResource(R.drawable.facebook_edit_sc);
					isFaceBookSelected = true;
					//handleSharingStatus();
				}
				// else {
				// customFB.PublishStory();
				// }
			}

			break;
		case R.id.posting_btn:
			if (UserManager.getInstance().getCurrentUser().getUserId() != 0) {
				// if (!ValidatorUtils.isRequired(photoCaption.getText()
				// .toString())) {
				// sharePB.setVisibility(View.VISIBLE);
				deleteFileNoThrow(newFile);
				saveImage();
				showLoader();
				User user = UserManager.getInstance().getCurrentUser();
				PostViewModel post = new PostViewModel();
				post.setPhotoOrientation(photo.getCaptureType());
				post.setPostPhotoCaption(photoCaption.getText().toString());
				if(finalImageWithWaterMark != null)
					post.setPostPhotoUrl(finalImageWithWaterMark);
				else
					post.setPostPhotoUrl(photoPath);

				if (privacySeekBar.getProgress() == 0)
					post.setPrivacy(StudioPhoto.PHOTO_PRIVACY_ONLY_ME);
				else if (privacySeekBar.getProgress() == 1)
					post.setPrivacy(StudioPhoto.PHOTO_PRIVACY_FRIENDS);
				else if (privacySeekBar.getProgress() == 2)
					post.setPrivacy(StudioPhoto.PHOTO_PRIVACY_PUBLIC);

				post.setStudioPhotoId(photo.getStudioPhotoId());
				post.setTags(photo.getTags());
				post.setUserCountry(user.getCountryName());
				post.setUserId(user.getUserId());
				post.setUserName(user.getFullName());
				post.setUserProfilePicUrl(user.getProfilePic());
				if(getIntent().getBooleanExtra(SHARE_SCREEN, false))
					StudioManager.getInstance().addPost(post);
				else{
					post.setPostId(getIntent().getIntExtra(POST_ID, 0));
					WallManager.getInstance().editPost(post);
				}
				// } else
				// UIUtils.showToast(this,
				// getResources().getString(R.string.caption_required));
			} else {
				saveImage();
				UIUtils.showToast(this,
						"Image is saved to your phone but you have to login in order to share it");
				startActivityForResult(LogInActivity.getActivityIntent(this,
						UserManager.LOGIN_FOR_RESULT,
						new Intent().putExtra(IMAGE_FILE_PATH, newFile)
								.putExtra(EDITED_FILE_PATH, photoPath)),
						ACTION_REQUEST_LOGIN);
				// ShareActivity
				// .getActivityIntent(this, photo, photoPath,
				// orientation, null, false))
			}
			break;

		case R.id.backBtn:
			backKeyPressed();
			break;

		case R.id.contest_layout:
			if (privacySeekBar.getProgress() == 2
					&& !UserManager.getInstance().canJoinContest()) // incomplete
																	// profile
				startActivity(Edit_UserProfile_Activity.getActivityIntent(this));
			break;

		default:
			break;
		}

	}

	private void showLoader() {
		if (mDialog == null) {
			mDialog = new ProgressDialog(this);
			mDialog = new ProgressDialog(this,R.style.MyTheme);
			mDialog.setCanceledOnTouchOutside(false);
			mDialog.setCancelable(false);
		}
		if (!mDialog.isShowing()) {
			try {
				mDialog.show();
			} catch (Exception e) {
			}
		}
	}

	private void hideLoader() {

		if (!isFinishing() && mDialog != null && mDialog.isShowing()) {
			try {
				mDialog.dismiss();
			} catch (Exception e) {
			}
		}

	}

	private void saveImage() {
		File dir_image2 = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + File.separator + "FCBStudio");
		dir_image2.mkdirs();
		newFile = Environment.getExternalStorageDirectory().getAbsolutePath()
				+ File.separator + "FCBStudio" + File.separator + "Barchelona"
				+ System.currentTimeMillis() + ".jpg";
		copyFile(finalImageWithWaterMark, newFile);
	}

	private boolean copyFile(String from, String to) {
		try {
			int bytesum = 0;
			int byteread = 0;
			File oldfile = new File(from);
			if (oldfile.exists()) {
				InputStream inStream = new FileInputStream(from);
				FileOutputStream fs = new FileOutputStream(to);
				byte[] buffer = new byte[1444];
				while ((byteread = inStream.read(buffer)) != -1) {
					bytesum += byteread;
					fs.write(buffer, 0, byteread);
				}
				inStream.close();
				fs.close();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private boolean deleteFileNoThrow(String path) {
		File file;
		try {
			file = new File(path);
		} catch (NullPointerException e) {
			return false;
		}

		if (file.exists()) {
			return file.delete();
		}
		return false;
	}

	@Override
	public void onComplete(Bundle values) {
		if (customFB != null) {
			customFB.saveCredentials();
			facebookImg.setImageResource(R.drawable.facebook_edit_sc);
			isFaceBookSelected = true;
			//handleSharingStatus();

		}
	}

	@Override
	public void onFacebookError(FacebookErrorCustom e) {
		UIUtils.showToast(this, "Authentication with Facebook failed!");
	}

	@Override
	public void onError(DialogErrorCustom e) {
		UIUtils.showToast(this, "Authentication with Facebook failed!");
	}

	@Override
	public void onCancel() {
		UIUtils.showToast(this, "Authentication with Facebook cancelled!");
	}

}
