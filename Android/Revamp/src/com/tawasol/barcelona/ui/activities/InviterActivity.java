package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.ui.fragments.InviteFragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * 
 * @author Turki
 *
 */
public class InviterActivity extends BaseActivity {

	public static final int REQUEST_INVITE_FRIEND = 12;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_invite);
		setTitle(getResources().getString(R.string.smsInvite_title));
		
		/**  Initialize Inviter Activity **/
		init();
	}
	
	/**  Get InviterActivity Intent to start it from any activity **/
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, InviterActivity.class);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		cancelInvitation();
		super.onBackPressed();
	}
	
	/** Cancel invitation and GOTO Registration Activity to handle the result **/
	private void cancelInvitation(){
		Intent intent = new Intent();
		intent.putExtra(Params.SMS_INVITATION.INVITATION_FLAG, true);
		setResult(Activity.RESULT_CANCELED, intent);
	}
	
	private void init(){
		/** Hide BottomBar, BackButton**/
		hideBottomBar();
		hideBackBtn();
		
		/** Show SkipButton, RightButton**/
		showRightBtn();
		showSkipButton();
		
		/** Set listener to SkipButton to cancel the invitation on click **/
		getSkipButton().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				cancelInvitation();
				finish();
			}
		});	
	}
}
