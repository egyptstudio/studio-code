package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.ui.fragments.PostFullScreenFourDirectionSwipe;
import com.tawasol.barcelona.ui.fragments.PremiumGridFragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PremiumPackagesActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.premium_packages_activity);
		showBackBtn();
		getRightBtn().setCompoundDrawablesWithIntrinsicBounds(0, // left
				0, // top
				R.drawable.benefits, // right
				0/* bottom */);
		setTitle(getResources().getString(R.string.Home_Shop));
		getRightBtn().setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(PremiumPackagesActivity.this,
						BenifitList.class));
			}
		});
		showRightBtn();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {

			PremiumGridFragment fragment = (PremiumGridFragment) getSupportFragmentManager()
					.findFragmentById(R.id.premium_fragment_id);
			fragment.onActivityResult(requestCode, resultCode, data);
			// refreshMenu();
		}
	}
}
