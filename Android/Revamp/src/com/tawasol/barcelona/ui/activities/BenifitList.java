package com.tawasol.barcelona.ui.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;

public class BenifitList extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.benifit_list_fragment);
		showBackBtn();
		hideBottomBar();
		hideMenuBtn();
		setTitle(getResources().getString(R.string.benefits_title));
		ListView benifitList = (ListView)findViewById(R.id.benifitList);
		BenifitAdapter adapter = new BenifitAdapter();
		benifitList.setAdapter(adapter);
		
	}
	
	private class BenifitAdapter extends BaseAdapter{

		String [] benifits;
		
		public BenifitAdapter(){
			benifits = getResources().getStringArray(R.array.benifitList);
		}
		
		@Override
		public int getCount() {
			return benifits.length;
		}

		@Override
		public Object getItem(int position) {
			return benifits[position];
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(App.getInstance()
					.getApplicationContext());
			View rootView = convertView;
			final ViewHolder holder;
			if(rootView == null){
				rootView = inflater.inflate(R.layout.benifit_list_item, parent, false);
				holder = new ViewHolder();
				holder.benifitNum = (TextView)rootView.findViewById(R.id.benifit_num);
				holder.benifitContent = (TextView)rootView.findViewById(R.id.benfit_details);
				rootView.setTag(holder);
			}else{
				holder = (ViewHolder) rootView.getTag();
			}
			holder.benifitNum.setText(String.valueOf(position + 1));
			holder.benifitContent.setText(benifits[position]);
			return rootView;
		}
		
		
		private class ViewHolder{
			TextView benifitNum;
			TextView benifitContent;
		}
	}
}
