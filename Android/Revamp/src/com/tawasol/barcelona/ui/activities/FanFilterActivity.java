package com.tawasol.barcelona.ui.activities;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.customView.PostViewFactory;
import com.tawasol.barcelona.entities.Fan;
import com.tawasol.barcelona.entities.FansFilterParams;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnSuggestionListRecieved;
import com.tawasol.barcelona.listeners.UpdateDialogListener;
import com.tawasol.barcelona.managers.FanManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.fragments.WallFragment;
import com.tawasol.barcelona.utils.UIUtils;

public class FanFilterActivity extends BaseActivity implements
		UpdateDialogListener, OnSuggestionListRecieved {

	public static final String PREMIUM = "premium";
	public static final int FILTER_REQUEST = 454;
	DisplayImageOptions options;
	RadioButton sortBy_location;
	RadioButton sortBy_morePics;
	CheckBox filter_online;
	CheckBox filter_offline;
	CheckBox filter_male;
	CheckBox filter_female;
	CheckBox filter_favo_only;
	CheckBox filter_followers;
	CheckBox filter_following;
	ProgressBar autoCompleteProgress;
	Button applyButton;
	FansFilterParams param = new FansFilterParams();
	// TextView countriesNum;
	AutoCompleteTextView filterText;
	private List<Integer> countries;
	Thread thread;
	List<Fan> fans = null;
	RelativeLayout chooseCountryBtn;
	List<AsyncTask> tasks;
	getSuggestionAsync suggestionAsync;
	CustomerAdapter adapter;

	private class getSuggestionAsync extends AsyncTask<Void, Void, Void> {

		private boolean running = true;

		@Override
		protected Void doInBackground(Void... params) {
//			while (running) {
				try {
					fans = FanManager.getInstance().getSuggestedFansFromServer(
							filterText.getText().toString());
				} catch (final Exception e) {
					if (e instanceof AppException)
						App.getInstance().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								UIUtils.showToast(FanFilterActivity.this,
										e.getMessage());
							}
						});
					e.printStackTrace();
				}
//			}
			return null;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			running = false;
		}

		@Override
		protected void onPostExecute(Void result) {
			autoCompleteProgress.setVisibility(View.GONE);
			adapter = new CustomerAdapter(
					FanFilterActivity.this, fans);
			filterText.setAdapter(adapter);
			adapter.getFilter().filter(filterText.getText().toString());
			if(!isFinishing())
				filterText.showDropDown();
			super.onPostExecute(result);
		}

	}

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.fan_filter_activity);
		showBackBtn();
		setTitle("Fans");
		hideBottomBar();
		hideMenuBtn();
		tasks = new ArrayList<AsyncTask>();
		suggestionAsync = new getSuggestionAsync();
		options = new DisplayImageOptions.Builder()
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.fan_no_image).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		autoCompleteProgress = (ProgressBar) findViewById(R.id.autoCompleteProgress);
		filterText = (AutoCompleteTextView) findViewById(R.id.filterKeword);
		filterText.setOnEditorActionListener(new OnEditorActionListener() {

	        @Override
	        public boolean onEditorAction(TextView v, int actionId,
	                KeyEvent event) {
	            if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
	                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

	                // NOTE: In the author's example, he uses an identifier
	                // called searchBar. If setting this code on your EditText
	                // then use v.getWindowToken() as a reference to your 
	                // EditText is passed into this callback as a TextView

	                in.hideSoftInputFromWindow(v
	                        .getApplicationWindowToken(),
	                        InputMethodManager.HIDE_NOT_ALWAYS);
	               // Must return true here to consume event
	               return true;

	            }
	            return false;
	        }
	    });
		filterText.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable mEdit) {

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (count >= 3) {
					AsyncTask runningTask = null;
					if (!tasks.isEmpty())
						runningTask = tasks.get(0);
					if (runningTask != null) {
						if (runningTask instanceof getSuggestionAsync) {
							if (runningTask.getStatus() == AsyncTask.Status.RUNNING
									|| runningTask.getStatus() == AsyncTask.Status.PENDING)
								runningTask.cancel(true);
							getSuggestionAsync suggestAsync = new getSuggestionAsync();
							suggestAsync.execute();
							tasks.clear();
							tasks.add(suggestAsync);
							autoCompleteProgress.setVisibility(View.VISIBLE);
						}
					} else {
						suggestionAsync.execute();
						tasks.add(suggestionAsync);
						autoCompleteProgress.setVisibility(View.VISIBLE);
					}
				}else if(count ==0){
					if(adapter != null && !adapter.getList().isEmpty()){
						App.getInstance().runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								adapter.getList().clear();
							}
						});
					}
				}
			}
		});
		sortBy_location = (RadioButton) findViewById(R.id.location_and_online);
		sortBy_morePics = (RadioButton) findViewById(R.id.most_pic);
		filter_online = (CheckBox) findViewById(R.id.online_filter);
		filter_offline = (CheckBox) findViewById(R.id.offline);
		filter_male = (CheckBox) findViewById(R.id.male_filter);
		filter_female = (CheckBox) findViewById(R.id.female_filter);
		filter_favo_only = (CheckBox) findViewById(R.id.favo_filter);
		filter_followers = (CheckBox) findViewById(R.id.followers_filter);
		filter_following = (CheckBox) findViewById(R.id.following_filter);
		applyButton = (Button) findViewById(R.id.button1);
		// countriesNum = (TextView) findViewById(R.id.numOfSelectedCountries);
		chooseCountryBtn = (RelativeLayout) findViewById(R.id.countrySelectLayout);

		chooseCountryBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(
						FilterActivity.getIntent(FanFilterActivity.this),
						FILTER_REQUEST);
			}
		});

		applyButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				checkChecked();
			}
		});
		sortBy_location.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// if(!UserManager.getInstance().getCurrentUser().isAllowLocation())
				// TODO show dialog for GPS enabling
				sortBy_morePics.setChecked(false);
				sortBy_location.setChecked(true);
			}
		});

		sortBy_morePics.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				sortBy_morePics.setChecked(true);
				sortBy_location.setChecked(false);
			}
		});
	}

	@Override
	protected void onResume() {
		FanManager.getInstance().addListener(this);
		WallManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		FanManager.getInstance().removeListener(this);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		WallManager.getInstance().removeListener(this);
		super.onDestroy();
	}

	protected void checkChecked() {
		if (sortBy_location.isChecked())
			param.setSortedBy(FansFilterParams.SORT_BY_LOCATION_AND_ONLINE);
		else if (sortBy_morePics.isChecked())
			param.setSortedBy(FansFilterParams.SORT_BY_NUMBER_OF_POSTS);
		else
			param.setSortedBy(FansFilterParams.SORT_BY_NUMBER_OF_POSTS);
		if (filter_male.isChecked() && !filter_female.isChecked())
			param.setGender(FansFilterParams.FILTER_MALE_ONLY);
		else if (filter_female.isChecked() && !filter_male.isChecked())
			param.setGender(FansFilterParams.FILTER_FEMALE_ONLY);
		else if (filter_male.isChecked() && filter_female.isChecked())
			param.setGender(FansFilterParams.FILTER_ANY_GENDER);
		else
			param.setGender(FansFilterParams.FILTER_ANY_GENDER);
		if (filter_online.isChecked() && !filter_offline.isChecked())
			param.setUserState(FansFilterParams.FILTER_ONLINE_ONLY);
		else if (filter_offline.isChecked() && !filter_online.isChecked())
			param.setUserState(FansFilterParams.FILTER_OFFLINE_ONLY);
		else if (filter_online.isChecked() && filter_offline.isChecked())
			param.setUserState(FansFilterParams.FILTER_ONLINE_AND_OFFLINE);
		else
			param.setUserState(FansFilterParams.FILTER_ONLINE_AND_OFFLINE);
		if (filter_favo_only.isChecked())
			param.setFavoritesFilter(FansFilterParams.FILTER_FAVOURATE_ONLY);
		else
			param.setFavoritesFilter(FansFilterParams.FILTER_FAVOURATE_OR_NOT);
		if (getIntent().getBooleanExtra(PREMIUM, false))
			param.setOnlyPremuim(FansFilterParams.PREMIUM_USERS_ONLY);
		else
			param.setOnlyPremuim(FansFilterParams.NOT_PREMIUM_USER);
		if (this.countries != null && !this.countries.isEmpty())
			param.setCountryList(this.countries);
		else {
			List<Integer> countries = new ArrayList<Integer>();
			param.setCountryList(countries);
		}
		if (filterText.getText().length() >= 1)
			param.setKeyword(filterText.getText().toString());
		else
			param.setKeyword("");

		if (filter_followers.isChecked() && !filter_following.isChecked())
			param.setFollowersOrFollwong(FansFilterParams.FOLLOWERS);
		else if (filter_following.isChecked() && !filter_followers.isChecked())
			param.setFollowersOrFollwong(FansFilterParams.FOLLOWING);
		else if (filter_followers.isChecked() && filter_following.isChecked())
			param.setFollowersOrFollwong(FansFilterParams.FOLLOWING_OR_FOLLOWERS);
		else
			param.setFollowersOrFollwong(FansFilterParams.ANY_USER);
		param.setUserId(UserManager.getInstance().getCurrentUserId());// TODO
		startActivity(FanFilterActivityResult.getIntent(this, param));
		this.finish();
	}

	public static Intent getIntent(Context context, boolean premium) {
		return new Intent(context, FanFilterActivity.class).putExtra(PREMIUM,
				premium);
	}

	@Override
	public void onException(AppException ex) {
		UIUtils.showToast(this, ex.getMessage());
	}

	@Override
	public void countryList(List<Integer> countries) {
		if (countries.size() > 0) {
			// countriesNum.setVisibility(View.VISIBLE);
			// countriesNum.setText(countries.size() + " selected");
			// this.countries = countries;
		}
	}

	public class CustomerAdapter extends ArrayAdapter<Fan> {
		private LayoutInflater layoutInflater;
		List<Fan> fansList;

		private Filter mFilter = new Filter() {
			@Override
			public String convertResultToString(Object resultValue) {
				return ((Fan) resultValue).getFanName();
			}

			@SuppressLint("DefaultLocale")
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();

				if (constraint != null) {
					ArrayList<Fan> suggestions = new ArrayList<Fan>();
					for (Fan fan : fansList) {
						// Note: change the "contains" to "startsWith" if you
						// only want starting matches
						if (fan.getFanName().toLowerCase()
								.contains(constraint.toString().toLowerCase())) {
							suggestions.add(fan);
						}
					}

					results.values = suggestions;
					results.count = suggestions.size();
				}

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				clear();
				if (results != null && results.count > 0) {
					// we have filtered results
					addAll((ArrayList<Fan>) results.values);
				} else {
					// no filter, add entire original list back in
					addAll(fansList);
				}
				notifyDataSetChanged();
			}
		};

		public CustomerAdapter(Context context, List<Fan> fans) {
			super(context, R.layout.auto_completelayout, fans);
			// copy all the customers into a master list
			fansList = new ArrayList<Fan>(fans.size());
			fansList.addAll(fans);
			layoutInflater = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
		}

		List<Fan> getList(){
			return fansList;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;

			if (view == null) {
				view = layoutInflater.inflate(R.layout.auto_completelayout,
						parent, false);
			}

			Fan fan = getItem(position);

			String fanName = fan.getFanName();

			if (fanName.contains("\n")) {
				fanName = fanName.replace("\n", "");
			}
			TextView name = (TextView) view.findViewById(R.id.fan_name);
			name.setText(fanName);
			ImageView fanProfileImage = (ImageView) view
					.findViewById(R.id.fan_profile_pic);
			ProgressBar progressBar = (ProgressBar) view
					.findViewById(R.id.progressBar1);
			display(fanProfileImage, fan.getFanPicture(), progressBar);

			return view;
		}

		public void display(ImageView img, String url, final ProgressBar spinner) {
			App.getInstance()
					.getImageLoader()
					.displayImage(url, img, options,
							new ImageLoadingListener() {
								@Override
								public void onLoadingStarted(String imageUri,
										View view) {
									spinner.setVisibility(View.VISIBLE);
								}

								@Override
								public void onLoadingFailed(String imageUri,
										View view, FailReason failReason) {
									spinner.setVisibility(View.GONE);

								}

								@Override
								public void onLoadingComplete(String imageUri,
										View view, Bitmap loadedImage) {
									spinner.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingCancelled(String imageUri,
										View view) {

								}

							});
		}

		@Override
		public Filter getFilter() {
			return mFilter;
		}
	}

	@Override
	public void onSuccess(List<Fan> objs) {
		autoCompleteProgress.setVisibility(View.GONE);
		CustomerAdapter adapter = new CustomerAdapter(this, objs);
		filterText.setAdapter(adapter);
		adapter.getFilter().filter(filterText.getText().toString());
		filterText.showDropDown();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == FILTER_REQUEST) {
				ArrayList<Integer> countries = data
						.getIntegerArrayListExtra(WallFragment.COUNTRY_LIST);
				// countriesNum.setVisibility(View.VISIBLE);
				// countriesNum.setText(countries.size() + " selected");
				((TextView) chooseCountryBtn
						.findViewById(R.id.choose_contry_btn))
						.setText(countries.size() + " selected");
				this.countries = countries;
			}

		}
	}

}
