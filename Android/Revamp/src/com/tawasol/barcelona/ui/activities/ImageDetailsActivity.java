package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.fragments.ImageDitailsFragment;

public class ImageDetailsActivity extends FragmentActivity {
	public static String POST_KEY;
	public static String INFO_RETURN = "infoPost";
	public static String VERTICAL_RETURN = "vertical_return";
	public static String HORIZONTAL_RETURN = "horizontal_return";
	public static String WHICH_TAB = "whichTab";
	public static String POSITION = "positionInList";
	public static String FROM_FULL_SCREEN = "fromFullScreen";
	public static String VERTICAL_POSITION = "vertical_position";
	public static String HORIZONTAL_POSITION = "horizontal_position";

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_image_details);
	}

	public static Intent getActivityIntent(Context context, PostViewModel post,
			int whichTab, int position, boolean fromFullScreen,
			int verticalPosition, int horiontalPosition) {
		Intent intent = new Intent(context, ImageDetailsActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(POST_KEY, post);
		bundle.putInt(WHICH_TAB, whichTab);
		bundle.putInt(POSITION, position);
		bundle.putBoolean(FROM_FULL_SCREEN, fromFullScreen);
		bundle.putInt(VERTICAL_POSITION, verticalPosition);
		bundle.putInt(HORIZONTAL_POSITION, horiontalPosition);
		intent.putExtras(bundle);
		return intent;
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		ImageDitailsFragment fragment = (ImageDitailsFragment) getSupportFragmentManager()
				.findFragmentById(R.id.imageDetails_fragment_id);
		fragment.onActivityResult(arg0, arg1, arg2);
	}

	@Override
	public void onBackPressed() {
		setResult(
				RESULT_OK,
				new Intent()
						.putExtra(
								INFO_RETURN,
								getIntent().getExtras().getSerializable(
										POST_KEY))
						.putExtra(
								VERTICAL_RETURN,
								getIntent().getExtras().getInt(
										VERTICAL_POSITION, 0))
						.putExtra(
								HORIZONTAL_RETURN,
								getIntent().getExtras().getInt(
										HORIZONTAL_POSITION, 0)));
		this.finish();
		// if(!ImageDitailsFragment.actions.isEmpty())
		// WallManager.getInstance().notifyPostFullScreen(ImageDitailsFragment.actions
		// , ImageDitailsFragment.modifiedPost);
		super.onBackPressed();
	}
}
