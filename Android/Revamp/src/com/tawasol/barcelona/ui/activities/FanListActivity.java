package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.customView.MaskedImageView;

import android.os.Bundle;

public class FanListActivity extends BaseActivity {

	private MaskedImageView profileImg;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.fan_list_item);
		profileImg = (MaskedImageView) findViewById(R.id.registeration_profile_img_id);
		profileImg.setImageDrawable(getResources().getDrawable(R.drawable.image_sample));
	}
}
