package com.tawasol.barcelona.ui.activities;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.ui.fragments.AddPosterFragment;
import com.tawasol.barcelona.ui.fragments.Edit_More_Profile_Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;


public class AddPosterActivity extends BaseActivity {
public static final String ORIGINAL_IMG_PATH="OriginalImgPath";
public static final String PRIVACY_PROGRESS_KEY="PrivacyProgressKey";
public static final String MASKED_IMG_PATH="MaskedImgPath";

	@Override
	protected void onCreate(Bundle bundle) {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		super.onCreate(bundle);
		setContentView(R.layout.activity_add_poster);
		setTitle(getString(R.string.extra_items));
		showRightBtn();
	}

	public static Intent getActivityIntent(Context context,String originalImgPth,String maskedImgPath ,int privacyProgress) {
		return new Intent(context, AddPosterActivity.class)
		.putExtra(ORIGINAL_IMG_PATH, originalImgPth)
		.putExtra(MASKED_IMG_PATH, maskedImgPath)
		.putExtra(PRIVACY_PROGRESS_KEY, privacyProgress);
	}
	
	
	@Override
		public void onBackPressed() {
		AddPosterFragment addPosterFrag = (AddPosterFragment) getSupportFragmentManager().findFragmentByTag("add_poster_fragment");
		addPosterFrag.navigateToHome();
		
		}
}
