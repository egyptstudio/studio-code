package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.utils.UIUtils;

public class FriendsList extends BaseActivity implements OnClickListener{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friend_list_activity);
		setTitle(getResources().getString(R.string.menu_Friends));

		/** Invite & Search Button **/
		showChatSearchAndInviteLayout();
		getChatInviteFriends().setOnClickListener(this);

		/** Hide Bottom bar **/
//		hideBottomBar();
		hideRightBtn();
		
//		showBackBtn();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, FriendsList.class);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.chat_invite_friends:
		//	UIUtils.showToast(FriendsList.this, "Invite not implemented yet");
			startActivity(ChatInviteActivity.getActivityIntent(this));
			break;
		default:
			break;
		}
	}
}
