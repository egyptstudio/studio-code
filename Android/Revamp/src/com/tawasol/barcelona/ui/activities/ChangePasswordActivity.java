package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle; 
import com.tawasol.barcelona.R; 
/**
 * @author Turki
 * 
 */
public class ChangePasswordActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_change_password);
		setTitle(getResources().getString(R.string.Settings_ChangePassword));
		showBackBtn(); 
		hideMenuBtn();
		hideBottomBar();
	}
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, ChangePasswordActivity.class);
	}
}
