package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.barcelona.R;

public class InfoActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_info);
		setTitle(getString(R.string.i_title));
		showBackBtn();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, InfoActivity.class);
	}

}
