package com.tawasol.barcelona.ui.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.dialogs.CustomAlertDialog;
import com.tawasol.barcelona.ui.fragments.WallFragment;

public class HomeActivity extends BaseActivity   {
public static String TAB_OBJ ="Tab";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homel);
		//getWindow().setBackgroundDrawable(null);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		hideBottomBar();
		setTitle(Html.fromHtml(getResources().getString(R.string.home)));
		showMenuBtn();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, HomeActivity.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
	}

	public static Intent getActivityIntent(Context context, int TabIndex , boolean refresh) {
		return new Intent(context, HomeActivity.class).putExtra(TAB_OBJ,
				TabIndex).putExtra(WallFragment.refresh, refresh).setFlags(
				Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
	}

	@Override
	public void onBackPressed() {
		if (getSlideLayout().isPanelExpanded()) {
			getSlideLayout().collapsePanel();
			return;
		}
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					HomeActivity.super.onBackPressed();
					WallManager.getInstance().clearAllData();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					dialog.dismiss();
					break;
				}
			}
		};
		/*Dialog dialog = new AlertDialog.Builder(this).setTitle("Warning")
				.setMessage(R.string.exit_msg)
				.setPositiveButton(android.R.string.yes, listener)
				.setNegativeButton(android.R.string.no, listener).create();
		dialog.show();*/
		
		
		Dialog dialog = new CustomAlertDialog.customBuilder(HomeActivity.this).setTitle(R.string.exit)
				.setMessage(R.string.exit_msg)
				.setPositiveButton(android.R.string.yes, listener)
				.setNegativeButton(android.R.string.no, listener).create();
		dialog.show();
		
	}

	@Override
		protected void onResume() {
			refreshMenu();
			super.onResume();
		}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
		// if(requestCode == 3){
		// WallFragment_new fragment1 = (WallFragment_new)
		// getSupportFragmentManager().findFragmentByTag("Latest");
		if (resultCode == Activity.RESULT_OK) {
			WallFragment fragment = new WallFragment();
			fragment.onActivityResult(requestCode, resultCode, data);
//			refreshMenu();
		}
		// }

	}

	
	@Override
		protected void onDestroy() {
			super.onDestroy();
			WallManager.getInstance().clearAllData();
		}
	

}
