package com.tawasol.barcelona.ui.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.view.WindowManager;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.ui.fragments.PostFullScreenFourDirectionSwipe;

public class PostFullScreenDetailsActivity extends FragmentActivity {
	
	public static final String CURRENT_INDEX_BUNDLE_KEY = "com.tawasol.barcelona.ui.activities.currentIndex";
	public static final String WHICH_TAB                = "com.tawasol.barcelona.ui.activities.whichTab";
	public static final String GROUP_POSITION           = "com.tawasol.barcelona.ui.activities.topTenGroupPosition";
	public static final String IS_FILTERED              = "com.tawasol.barcelona.ui.activities.isFiltered";
	public static final String IS_FAVORITE              = "com.tawasol.barcelona.ui.activities.isFavorite";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_post_full_screen_details);
		BroadcastReceiver broadcast_reciever = new BroadcastReceiver() {

		    @Override
		    public void onReceive(Context arg0, Intent intent) {
		        String action = intent.getAction();
		        if (action.equals("finish_activity")) {
		            finish();
		        }
		    }
		};
		registerReceiver(broadcast_reciever, new IntentFilter("finish_activity"));
	}

	
	public static Intent getIntent(Context context, int currentIndex, int whichTab , int groupPosition, boolean isFiltered , boolean isFavorite ) {
		Intent intent = new Intent(context, PostFullScreenDetailsActivity.class);
		intent.putExtra(CURRENT_INDEX_BUNDLE_KEY, currentIndex);
		intent.putExtra(WHICH_TAB               , whichTab);
		intent.putExtra(GROUP_POSITION          , groupPosition);
		intent.putExtra(IS_FILTERED             , isFiltered);
		intent.putExtra(IS_FAVORITE             , isFavorite);
		return intent;
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if (arg1 == Activity.RESULT_OK) {
			
			PostFullScreenFourDirectionSwipe fragment = (PostFullScreenFourDirectionSwipe) getSupportFragmentManager().findFragmentById(R.id.post_full_screen_fragment_id);
 			fragment.onActivityResult(arg0, arg1, arg2);
//			refreshMenu();
		}
	}
	
}
