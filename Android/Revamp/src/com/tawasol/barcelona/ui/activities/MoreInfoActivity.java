package com.tawasol.barcelona.ui.activities;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.customView.MaskedImageView;
import com.tawasol.barcelona.entities.Fan;

public class MoreInfoActivity extends BaseActivity {

	public static final String INFO = "info";
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_more_inf);
		showBackBtn();
		TextView info = (TextView)findViewById(R.id.info);
		info.setText("\n");
		Fan fan = (Fan) getIntent().getExtras().getSerializable(INFO);
		for(int x = 0 ; x < fan.getMoreInfo().size() ; x++){
			info.setText(info.getText().toString() + fan.getMoreInfo().get(x).getTitle() + "\n" + fan.getMoreInfo().get(x).getValue() +"\n\n");
		}
		
	}
	
	public static Intent getIntent(Context context , Fan fan){
		return new Intent(context, MoreInfoActivity.class).putExtra(INFO, fan);
	}
	
	@Override
	public void onBackPressed() {
		MaskedImageView.clearMask();
		super.onBackPressed();
	}
}
