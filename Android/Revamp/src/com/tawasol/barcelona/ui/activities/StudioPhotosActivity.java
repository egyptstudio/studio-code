package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.StudioFolder;
import com.tawasol.barcelona.ui.fragments.ImageDitailsFragment;
import com.tawasol.barcelona.ui.fragments.StudioPhotosFragment;

public class StudioPhotosActivity extends BaseActivity {

	static StudioFolder selectedfolder;
	public static final String STUDIO_FOLDER_KEY="SelectedFolder";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_studio_photos);
//		hideBottomBar();
		if(selectedfolder != null)
		setTitle(selectedfolder.getFolderName());
		showMenuBtn();
		showBackBtn();
		
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		refreshMenu();
	}
	public static Intent getActivityIntent(Context context,StudioFolder folder) {
		selectedfolder = folder;
		return new Intent(context, StudioPhotosActivity.class)
		.putExtra(STUDIO_FOLDER_KEY, folder);
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		StudioPhotosFragment photosFrag = (StudioPhotosFragment) getSupportFragmentManager()
				.findFragmentById(R.id.photos_fragment_id);
		//photosFrag.onActivityResult(arg0, arg1, arg2);
	}
	}


