package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle; 
import com.tawasol.barcelona.R; 
/**
 * @author Turki
 * 
 */
public class SettingsActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_settings);
		setTitle(getResources().getString(R.string.Settings_title));
//		hideMenuBtn();
	}
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, SettingsActivity.class);
	}
}
