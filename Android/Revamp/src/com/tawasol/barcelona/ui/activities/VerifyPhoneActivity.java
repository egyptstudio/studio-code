package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle; 
import com.tawasol.barcelona.R; 
/**
 * @author Turki
 * 
 */
public class VerifyPhoneActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_verify_phone);
		setTitle(getResources().getString(R.string.HomeUnconfirmedEmailAndPhone_verifyTitle));
		showBackBtn();
		hideBottomBar();
		hideMenuBtn();
	}
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, VerifyPhoneActivity.class);
	}
}
