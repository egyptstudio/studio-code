package com.tawasol.barcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.barcelona.R;

public class NotificationsActivity extends BaseActivity   {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notifications);
		setTitle(getResources().getString(R.string.notifications));
		
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, NotificationsActivity.class);
	}

	

}
