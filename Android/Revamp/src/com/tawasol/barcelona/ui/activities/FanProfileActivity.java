package com.tawasol.barcelona.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.managers.FanManager;
import com.tawasol.barcelona.ui.fragments.FansListFragment;

public class FanProfileActivity extends BaseActivity {

	public static final String FANID = "fanID";
	public static final String FAN_NAME = "fanName";
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.fan_profile_activity);
		showBackBtn();
		setTitle(getString(R.string.User_Profile));
	}
	
	@Override
	public void onBackPressed() {
		FanManager.getInstance().resetFan();
		super.onBackPressed();
	}
	
	public static Intent getIntent(Context context , int fanId , String fanName){
		return new Intent(context, FanProfileActivity.class).putExtra(FANID, fanId).putExtra(FAN_NAME, fanName);
	}
	
	// added by mohga
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK
				&& requestCode == FansListFragment.INTENT_RESULT_CODE) 
			refreshMenu();
		

	}
}
