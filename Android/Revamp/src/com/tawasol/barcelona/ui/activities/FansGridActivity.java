package com.tawasol.barcelona.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.managers.FanManager;
import com.tawasol.barcelona.ui.fragments.FansListFragment;

public class FansGridActivity extends BaseActivity {

	public static final String VIEWTYPE = "viewType";
	public static final String FAN_ID = "fanID";
	public static final int LOADFANS = -45486548;
	public static final int LOADFOLLOWERS = -45486545;
	public static final int LOADFOLLOWING = -45486540;
	public static final int LOADFOLLOWINGSuggestions = -454340;
	public static final String IsPremium = "isPremium";

	String[] imageUrls;

	DisplayImageOptions options;
	protected AbsListView listView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fan_list_activity);
		// Get a Tracker (should auto-report)
		((App) getApplication()).getTracker(App.TrackerName.APP_TRACKER);
		getFilterBtn().setBackgroundResource(R.drawable.fan_filter);
		if (getIntent().getIntExtra(VIEWTYPE, 0) == LOADFANS)
			showFilterhBtn();
		if (getIntent().getIntExtra(VIEWTYPE, 0) == LOADFOLLOWERS
				|| getIntent().getIntExtra(VIEWTYPE, 0) == LOADFOLLOWING)
			showBackBtn();
		getFilterBtn().setEnabled(false);
		if (getIntent().getIntExtra(VIEWTYPE, 0) == LOADFANS)
			setTitle(getString(R.string.menu_Fans));
		else if (getIntent().getIntExtra(VIEWTYPE, 0) == LOADFOLLOWERS)
			setTitle(getString(R.string.menu_Followers));
		else if (getIntent().getIntExtra(VIEWTYPE, 0) == LOADFOLLOWING)
			setTitle(getString(R.string.menu_Following));
		else if (getIntent().getIntExtra(VIEWTYPE, 0) == LOADFOLLOWINGSuggestions)
			setTitle(getString(R.string.suggestion));
		getFilterBtn().setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(FanFilterActivity.getIntent(
						FansGridActivity.this, false));
			}
		});
	}

	@Override
	public void onBackPressed() {
		if (getIntent().getIntExtra(VIEWTYPE, 0) == LOADFOLLOWERS
				|| getIntent().getIntExtra(VIEWTYPE, 0) == LOADFOLLOWING)
			FanManager.getInstance().resetFollowList();
		if (getIntent().getIntExtra(VIEWTYPE, 0) == LOADFANS)
			FanManager.getInstance().clearFans();
		super.onBackPressed();
	}

	public static Intent getIntent(Context context, int loadView) {
		return new Intent(context, FansGridActivity.class).putExtra(VIEWTYPE,
				loadView)/* .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) */;
	}

	public static Intent getIntent(Context context, int loadView, int fanID) {
		return new Intent(context, FansGridActivity.class).putExtra(VIEWTYPE,
				loadView).putExtra(FAN_ID, fanID)/*
												 * .setFlags(Intent.
												 * FLAG_ACTIVITY_CLEAR_TOP)
												 */;
	}

	public void enableFilterButton() {
		getFilterBtn().setEnabled(true);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK
				&& requestCode == FansListFragment.INTENT_RESULT_CODE) {
			// FansListFragment fragment = new FansListFragment();
			// fragment.onActivityResult(requestCode, resultCode, data);
			refreshMenu();
		}

	}

	/*
	 * // added by mohga
	 * 
	 * @Override protected void onResume() { super.onResume(); refreshMenu(); }
	 */
	
	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// Stop the analytics tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

}