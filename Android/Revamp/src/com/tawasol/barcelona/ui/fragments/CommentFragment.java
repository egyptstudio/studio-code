/**
 * 
 */
package com.tawasol.barcelona.ui.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.commonsware.cwac.endless.EndlessAdapter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.entities.UserComment;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnCommentAdded;
import com.tawasol.barcelona.listeners.OnCommentDeleted;
import com.tawasol.barcelona.listeners.OnCommentsListReceived;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.ui.activities.CommentActivity;
import com.tawasol.barcelona.ui.activities.FanProfileActivity;
import com.tawasol.barcelona.ui.activities.UserProfileActivity;
import com.tawasol.barcelona.utils.NetworkingUtils;
import com.tawasol.barcelona.utils.UIUtils;

/**
 * @author Basyouni
 *
 */
public class CommentFragment extends BaseFragment implements
		OnCommentsListReceived, OnCommentDeleted, OnCommentAdded {

	ListView commentList;
	TextView commentText;
	Button postComment;
	int postId;
	LinearLayout noComment;
	int pageNum = 0;
	List<UserComment> comments;
	static CommentAdapter adapter;
	DisplayImageOptions profileOption;
	ProgressBar bar;

	public static CommentAdapter getAdapter() {
		return adapter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.comment_list, container,
				false);
		adapter = null;
		profileOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1000)).build();
		commentList = (ListView) rootView.findViewById(R.id.commentList);
		PauseOnScrollListener listener = new PauseOnScrollListener(App
				.getInstance().getImageLoader(), true, true);
		commentList.setOnScrollListener(listener);
		noComment = (LinearLayout) rootView
				.findViewById(R.id.beTheFirstCommenter);
		bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
		commentText = (TextView) rootView.findViewById(R.id.commentText);
		postComment = (Button) rootView.findViewById(R.id.postComment);
		postComment.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (NetworkingUtils.isNetworkConnected(getActivity())) {
					if (commentText.getText().toString().trim().length() > 0) {
						postComment.setEnabled(false);
						User user = UserManager.getInstance().getCurrentUser();
						UserComment userComment = new UserComment();

						userComment
								.setComment(commentText.getText().toString());
						userComment.setFullName(user.getFullName());
						userComment.setUserId(user.getUserId());
						userComment.setCommentDate(System.currentTimeMillis() / 1000);
						userComment.setPhotoId(postId);
						userComment.setProfilePicture(user.getProfilePic());
						userComment.setFollowing(true);
						WallManager.getInstance().addComment(userComment,
								adapter != null ? adapter.getCount() : 0);
						bar.setVisibility(View.VISIBLE);
					}
				} else {
					UIUtils.showToast(getActivity(), "You have to be connected");
				}
			}
		});
		this.postId = getActivity().getIntent().getIntExtra(
				CommentActivity.POST_ID, 0);
		WallManager.getInstance().getComments(postId, pageNum + 1);
		pageNum++;
		return rootView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tawasol.barcelona.listeners.OnEntitiesListReceivedListener#
	 * onEntitiesListReceived(java.util.List, int)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> void onEntitiesListReceived(List<T> list, int pageNo) {
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		if (commentList.getVisibility() == View.GONE)
			commentList.setVisibility(View.VISIBLE);
		if (list != null) {
			if (list.isEmpty()) {
				noComment.setVisibility(View.VISIBLE);
				commentList.setVisibility(View.GONE);
			}
			((BaseActivity) getActivity()).setTitle(list.size() + " Comments");
			adapter = new CommentAdapter((List<UserComment>) list);
			commentList.setAdapter(new updateComments(getActivity(),
					new CommentAdapter((List<UserComment>) list),
					R.layout.progress_bar));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		WallManager.getInstance().addListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onPause()
	 */
	@Override
	public void onPause() {
		super.onPause();
		WallManager.getInstance().removeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tawasol.barcelona.listeners.SimpleUIListsner#onException(com.tawasol
	 * .barcelona.exception.AppException)
	 */
	@Override
	public void onException(AppException ex) {
		postComment.setEnabled(true);
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		if (ex.getType() == AppException.NO_DATA_EXCEPTION) {
			noComment.setVisibility(View.VISIBLE);
			commentList.setVisibility(View.GONE);
		} else {
			Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT)
					.show();
		}
	}

	public class CommentAdapter extends BaseAdapter {

		List<UserComment> comments;

		public CommentAdapter(List<UserComment> comments) {
			this.comments = comments;
		}

		/**
		 * @param userComment
		 */
		public void addComment(UserComment userComment) {
			this.comments.add(0, userComment);
			notifyDataSetChanged();
			commentList.invalidateViews();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount() {
			return this.comments.size();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Object getItem(int position) {
			return null;
		}

		List<UserComment> getList() {
			return this.comments;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int position) {
			return 0;
		}

		public void appendMembers(List<UserComment> newComments) {
			this.comments.addAll(newComments);
		}

		public void deleteComment(int postId, int commentId) {
			UserComment comment = new UserComment(commentId, postId);
			if (this.comments.contains(comment)) {
				this.comments.remove(comment);
				commentList.invalidateViews();
				if (this.comments.isEmpty()) {
					noComment.setVisibility(View.VISIBLE);
					commentList.setVisibility(View.GONE);
				}
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.Adapter#getView(int, android.view.View,
		 * android.view.ViewGroup)
		 */
		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflator = LayoutInflater.from(App.getInstance()
					.getApplicationContext());
			View rootView = convertView;
			if (rootView == null)
				rootView = inflator.inflate(R.layout.comment_item, parent,
						false);
			ImageView profileImage = (ImageView) rootView
					.findViewById(R.id.comment_profile_Image);
			TextView userName = (TextView) rootView
					.findViewById(R.id.comment_userName);
			ImageView follow = (ImageView) rootView
					.findViewById(R.id.comment_follow);
			TextView date = (TextView) rootView.findViewById(R.id.comment_date);
			TextView commentText = (TextView) rootView
					.findViewById(R.id.commentText);
			ImageButton deleteButton = (ImageButton) rootView
					.findViewById(R.id.deleteComment);
			if (this.comments.get(position).getUserId() == UserManager
					.getInstance().getCurrentUserId()
					|| getActivity().getIntent().getBooleanExtra(
							CommentActivity.OWN_POST, false))
				deleteButton.setVisibility(View.VISIBLE);
			else
				deleteButton.setVisibility(View.GONE);
			deleteButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (NetworkingUtils.isNetworkConnected(getActivity())) {
						ShowAlertDilaog(
								CommentAdapter.this.comments.get(position)
										.getCommentId(),
								CommentFragment.this.postId);
					} else {
						UIUtils.showToast(getActivity(), "no internet");
					}
				}
			});
			if (commentText != null) {
				commentText.setText(this.comments.get(position)
						.getCommentText());
			}
			profileImage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (comments.get(position).getUserId() == UserManager
							.getInstance().getCurrentUserId())
						getActivity().startActivity(
								UserProfileActivity
										.getActivityIntent(getActivity()));
					else
						getActivity().startActivity(
								FanProfileActivity.getIntent(getActivity(),
										comments.get(position).getUserId(),
										comments.get(position).getUserName()));
				}
			});
			if (profileImage != null
					&& this.comments.get(position).getProfilePic() != "")
				App.getInstance()
						.getImageLoader()
						.displayImage(
								this.comments.get(position).getProfilePic(),
								profileImage, profileOption);
			if (userName != null) {
				userName.setText(this.comments.get(position).getUserName());
				userName.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						if (comments.get(position).getUserId() == UserManager
								.getInstance().getCurrentUserId())
							getActivity().startActivity(
									UserProfileActivity
											.getActivityIntent(getActivity()));
						else
							getActivity().startActivity(
									FanProfileActivity.getIntent(getActivity(),
											comments.get(position).getUserId(),
											comments.get(position)
													.getUserName()));
					}
				});
			}
			if (follow != null) {
				if (this.comments.get(position).isFollowing()
						|| this.comments.get(position).getUserId() == UserManager
								.getInstance().getCurrentUserId())
					follow.setVisibility(View.GONE);/*setImageBitmap(BitmapFactory.decodeResource(
							getActivity().getResources(), R.drawable.followed));*/
				else{
					follow.setVisibility(View.VISIBLE);
					follow.setImageBitmap(BitmapFactory
							.decodeResource(getActivity().getResources(),
									R.drawable.unfollowed));
				}
			}
			if (date != null)
				date.setText(formatedate(this.comments.get(position)
						.getCommentDate()));
			return rootView;
		}

	}

	private void ShowAlertDilaog(final int commentId, final int postId) {
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			String Message = "";

			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						WallManager.getInstance().deleteComment(postId,
								commentId,
								adapter != null ? adapter.getCount() : 0);
						bar.setVisibility(View.VISIBLE);
					case DialogInterface.BUTTON_NEGATIVE:
						dialog.dismiss();
						break;
					}
				}
			};

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

			Message = getString(R.string.deleteComment);
			builder.setMessage(Message)
					.setPositiveButton("OK", dialogClickListener)
					.setNegativeButton("Cancel", dialogClickListener);
			builder.show();
		}
	}

	private class updateComments extends EndlessAdapter {

		List<UserComment> temp;

		public updateComments(Context context, CommentAdapter wrapped,
				int pendingResource) {
			super(context, wrapped, pendingResource);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.commonsware.cwac.endless.EndlessAdapter#appendCachedData()
		 */
		@Override
		protected void appendCachedData() {
			if (getWrappedAdapter() != null)
				((CommentAdapter) getWrappedAdapter()).appendMembers(temp);
			temp = null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.commonsware.cwac.endless.EndlessAdapter#cacheInBackground()
		 */
		@Override
		protected boolean cacheInBackground() throws Exception {
			temp = WallManager.getInstance().getCommentsFromServer(postId,
					pageNum + 1);
			pageNum++;
			return !temp.isEmpty();
		}
	}

	// @SuppressWarnings("unused")
	// private Period calcDiff(Date startDate, Date endDate) {
	// DateTime START_DT = (startDate == null) ? null
	// : new DateTime(startDate);
	// DateTime END_DT = (endDate == null) ? null : new DateTime(endDate);
	//
	// Period period = new Period(START_DT, END_DT);
	//
	// return period;
	//
	// }

	private String formatedate(long date1) {

		long time = date1 * (long) 1000;
		// Date date = new Date(time);
		// SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm",
		// Locale.getDefault());
		// format.setTimeZone(TimeZone.getTimeZone("GMT"));
		// String dt = format.format(date);
		// ---------------ago------------------------
		// Period dateDiff = calcDiff(new Date(time),
		// new Date(System.currentTimeMillis()));
		// String d = PeriodFormat.wordBased().print(dateDiff);
		// String[] da = d.split(",");
		// return da[0] + " ago";
		// ------------------------------------------
		Date date = new Date(time);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm",
				Locale.getDefault());
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		String beforeSplit = format.format(date);
		String[] afterSplit = beforeSplit.split(" ");
		return afterSplit[0] + "\n" + afterSplit[1];
	}

	// private String formatedate(long date1) {
	//
	// CharSequence desiredString = DateUtils.getRelativeTimeSpanString(date1 *
	// (long) 1000 ,
	// System.currentTimeMillis(),
	// DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_SHOW_DATE |
	// DateUtils.FORMAT_SHOW_YEAR );
	//
	// long time = date1 * (long) 1000;
	// Date date = new Date(time);
	// SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm" ,
	// Locale.getDefault());
	// format.setTimeZone(TimeZone.getTimeZone("GMT"));
	// return format.format(date);
	// }

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onCommentDeleted(int postId, int commentId) {
		bar.setVisibility(View.GONE);
		adapter.deleteComment(postId, commentId);
		((CommentActivity) getActivity()).updateTitle(adapter.getCount());
	}

	@Override
	public void onSuccess(List<UserComment> objs) {
		// TODO Auto-generated method stub

	}

	@Override
	public void commentAdded(UserComment comment) {
		postComment.setEnabled(true);
		bar.setVisibility(View.GONE);
		if (adapter != null) {
			if (noComment.getVisibility() == View.VISIBLE) {
				noComment.setVisibility(View.GONE);
				commentList.setVisibility(View.VISIBLE);
			}
			adapter.addComment(comment);
			((CommentActivity) getActivity()).updateTitle(adapter.getCount());
		} else {
			noComment.setVisibility(View.GONE);
			commentList.setVisibility(View.VISIBLE);
			List<UserComment> newComment = new ArrayList<UserComment>();
			newComment.add(comment);
			adapter = new CommentAdapter(newComment);
			commentList.setAdapter(new updateComments(getActivity(),
					new CommentAdapter(newComment), R.layout.progress_bar));
			((CommentActivity) getActivity()).updateTitle(adapter.getCount());
		}
		commentText.setText("");
	}

}
