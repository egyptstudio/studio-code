package com.tawasol.barcelona.ui.fragments;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.managers.ShopManager;
import com.tawasol.barcelona.utils.NetworkingUtils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class NikeStore extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.nike_store, container, false);

		ShopManager.getInstance().LoadInAppProductIDs();
		
		WebView nikeStore = (WebView) rootView.findViewById(R.id.webView1);
		/** Added by turki **/
		nikeStore.getSettings().setJavaScriptEnabled(true);   
		nikeStore.getSettings().setBuiltInZoomControls(true);
		nikeStore.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				return false;
			}
		});

		TextView offline = (TextView)rootView.findViewById(R.id.textView1);
		
		if(NetworkingUtils.isNetworkConnected(getActivity()))
			nikeStore.loadUrl("http://track.webgains.com/click.html?wgcampaignid=170253&wgprogramid=6373&wgtarget=http://www.nike.com/nl/en_gb/c/fcbarcelona?sitesrc=fcb");
		else{
			offline.setVisibility(View.VISIBLE);
			nikeStore.setVisibility(View.GONE);
		}
		
		return rootView;
	}

}
