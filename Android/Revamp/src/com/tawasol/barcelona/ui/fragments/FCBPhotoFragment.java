package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;

import com.tawasol.barcelona.R; 
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.FCBPhoto; 
import com.tawasol.barcelona.ui.adapters.FCBPhotoAdapter; 

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

/**
 * @author Turki
 * 
 */
public class FCBPhotoFragment extends BaseFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_fcb_photo, container, false);
		
		/** Get FCBPhoto list from intent **/
		ArrayList<FCBPhoto> list = getFCBPhotoList();
		
		/** Initialize registration view **/
		init(rootView, list);

		return rootView;
	}

	public static Fragment newFragment() {
		return new FCBPhotoFragment();
	}	

	private void init(View rootView, final ArrayList<FCBPhoto> list) {
		
		GridView gridview = (GridView) rootView.findViewById(R.id.grid);
	    gridview.setAdapter(new FCBPhotoAdapter(getActivity(),list));
	    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	Intent intent = new Intent();
            	intent.putExtra(Params.UPLOAD_USER_PHOTO.FCB_PHOTO_OBJECT, list.get(position));
				getActivity().setResult(Activity.RESULT_OK, intent);
				getActivity().finish();
            }
        });
	}
	
	private ArrayList<FCBPhoto> getFCBPhotoList(){
		Intent intent = getActivity().getIntent();
		Bundle bundle = intent.getBundleExtra(Params.UPLOAD_USER_PHOTO.FCB_BUNDLE);
		ArrayList<FCBPhoto> list = (ArrayList<FCBPhoto>) bundle.getSerializable(Params.UPLOAD_USER_PHOTO.FCB_LIST_NAME);	
		return list;
	}
	
}
