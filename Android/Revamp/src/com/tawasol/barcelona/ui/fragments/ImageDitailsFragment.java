/**
 * 
 */
package com.tawasol.barcelona.ui.fragments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.customView.PostViewFactory;
import com.tawasol.barcelona.entities.KeyValuePairs;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.entities.StudioPhoto;
import com.tawasol.barcelona.entities.TagsData;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnCommentScreenFinish;
import com.tawasol.barcelona.listeners.OnStudioPhotoRecievedForUse;
import com.tawasol.barcelona.listeners.OnTagsDataReceived;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.CaptureActivity;
import com.tawasol.barcelona.ui.activities.CommentActivity;
import com.tawasol.barcelona.ui.activities.FanProfileActivity;
import com.tawasol.barcelona.ui.activities.ImageDetailsActivity;
import com.tawasol.barcelona.ui.activities.LikeActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.PostFullScreenDetailsActivity;
import com.tawasol.barcelona.ui.activities.ShareActivity;
import com.tawasol.barcelona.ui.activities.TagListActivity;
import com.tawasol.barcelona.ui.activities.UserProfileActivity;
import com.tawasol.barcelona.ui.dialogs.CustomAlertDialog;
import com.tawasol.barcelona.utils.NetworkingUtils;
import com.tawasol.barcelona.utils.UIUtils;

/**
 * @author Basyouni
 *
 */
public class ImageDitailsFragment extends Fragment implements OnClickListener,
		OnCommentScreenFinish, OnStudioPhotoRecievedForUse, OnTagsDataReceived {

	private final static int DELETE_POST = 1;
	private final static int REPORT_POST = 2;
	private PostViewModel post;
	private ProgressDialog dialog;
	ImageButton details;
	ImageButton likeButton;
	TextView likeCount;
	ImageButton back;
	ImageView profileImage;
	TextView userName;
	ImageView follow;
	// TextView repostedBy;
	TextView dateText;
	LinearLayout topTenIndicator;
	TextView topTenRank;
	TextView comment;
	ImageButton repost;
	ImageButton share;
	ProgressBar tagsProgress;
	LinearLayout use;
	LinearLayout editAndDelete;
	LinearLayout editButton;
	LinearLayout deleteButton;
	LinearLayout tagsContainer;
	TextView captionText;
	// PostViewModel postId;
	Button reportImage;
	ImageView postImage;
	DisplayImageOptions profileOption;
	private String newPath;
	private boolean getResult;
	ImageView reposterProfileImage;
	TextView reposterUserName;
	ImageView reposterFollow;
	int whichTab;
	public static List<Integer> actions = new ArrayList<Integer>();
	public static PostViewModel modifiedPost;

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.image_details_fragment,
				container, false);
		actions.clear();
		getResult = false;
		profileOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
				.resetViewBeforeLoading(true)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1000)).build();

		dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		initView(rootView);

		initControlsActions();

		if (!getResult) {
			initPost();

			populatePost();
		}

		return rootView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// HomeManager.getInstance().addListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		WallManager.getInstance().addListener(this);
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		WallManager.getInstance().removeListener(this);
	}

	/**
	 * 
	 */
	private void populatePost() {
		if (post.getStudioPhotoId() == 0)
			use.setVisibility(View.GONE);
		if (post.getUserId() == UserManager.getInstance().getCurrentUserId()) {
			editAndDelete.setVisibility(View.VISIBLE);
			reportImage.setVisibility(View.GONE);
		} else {
			editAndDelete.setVisibility(View.GONE);
			reportImage.setVisibility(View.VISIBLE);
		}

		if (post.getUserName() != null) {
			userName.setText(post.getUserName());
			dateText.setText(formatedate(post.getCreationTime()));
			if (post.getCommentCount() > 0)
				comment.setText(Integer.toString(post.getCommentCount()));
			else {
				comment.setText("");
			}
			// usesCountTxt.setText(Integer.toString(post.getUsedCount()));
			if (post.getPostPhotoCaption() != null)
				captionText.setText(post.getPostPhotoCaption());
			else
				captionText.setVisibility(View.GONE);

			ImageLoader imageLoder = App.getInstance().getImageLoader();

			if (post.getPostPhotoUrl() != null) {
				imageLoder.displayImage(post.getPostPhotoUrl(), postImage);
			}

			if (post.isRePosted())
				repost.setBackgroundResource(R.drawable.repost_sc);
			else
				repost.setBackgroundResource(R.drawable.repost);

			if (post.getOriginalPostId() != 0) {
				if (post.getRepostedBy() != null) {
					reposterUserName.setText(post.getRepostedBy());
				} else
					reposterUserName.setVisibility(View.GONE);
				if (post.getRePosterProfilePic() != null)
					imageLoder.displayImage(post.getRePosterProfilePic(),
							reposterProfileImage, profileOption);
				if (post.isRePosterIsFollowing())
					reposterFollow.setVisibility(View.GONE);
				else {
					if (post.getRePosterId() == UserManager.getInstance()
							.getCurrentUserId())
						reposterFollow.setVisibility(View.GONE);
					else {
						reposterFollow.setVisibility(View.VISIBLE);
						reposterFollow.setImageBitmap(BitmapFactory
								.decodeResource(getActivity().getResources(),
										R.drawable.unfollowed));
					}
				}
			} else {
				if (post.getUserName() != null) {
					reposterUserName.setText(post.getUserName());
				} else
					reposterUserName.setVisibility(View.GONE);
				// if (post.getRePosterProfilePic() != null)

				if (post.getUserProfilePicUrl() != null)
					if (!post.getUserProfilePicUrl().equals("")) {
						imageLoder.displayImage(post.getUserProfilePicUrl(),
								reposterProfileImage, profileOption);
						imageLoder.displayImage(post.getUserProfilePicUrl(),
								profileImage, profileOption,
								new ImageLoadingListener() {

									@Override
									public void onLoadingFailed(String arg0,
											View arg1, FailReason arg2) {
										profileImage
												.setImageResource(R.drawable.avatar);

									}

									@Override
									public void onLoadingCancelled(String arg0,
											View arg1) {

									}

									@Override
									public void onLoadingComplete(String arg0,
											View arg1, Bitmap arg2) {

									}

									@Override
									public void onLoadingStarted(String arg0,
											View arg1) {

									}

								});
					} else
						profileImage.setImageResource(R.drawable.avatar);
				if (post.isRePosterIsFollowing())
					reposterFollow.setVisibility(View.GONE);/*
															 * setImageBitmap(
															 * BitmapFactory
															 * .decodeResource(
															 * getActivity
															 * ().getResources
															 * (),
															 * R.drawable.followed
															 * ));
															 */
				else
					reposterFollow.setImageBitmap(BitmapFactory
							.decodeResource(getActivity().getResources(),
									R.drawable.unfollowed));
			}
			if (!post.getUserProfilePicUrl().equals("")) {
				imageLoder.displayImage(post.getUserProfilePicUrl(),
						profileImage, profileOption,
						new ImageLoadingListener() {

							@Override
							public void onLoadingFailed(String arg0, View arg1,
									FailReason arg2) {
								profileImage
										.setImageResource(R.drawable.avatar);

							}

							@Override
							public void onLoadingCancelled(String arg0,
									View arg1) {

							}

							@Override
							public void onLoadingComplete(String arg0,
									View arg1, Bitmap arg2) {

							}

							@Override
							public void onLoadingStarted(String arg0, View arg1) {

							}

						});
			} else
				profileImage.setImageResource(R.drawable.avatar);
			updateFollow();
			updatePostLikes();
			updateTopTen();

		} else {
			Toast.makeText(getActivity(), "there was an error ",
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 
	 */
	private void updateTopTen() {
		if (topTenIndicator != null && topTenRank != null) {
			if (post.getContestRank() > 0) {
				topTenIndicator.setVisibility(View.VISIBLE);
				topTenRank.setText(String.valueOf(post.getContestRank()));
			} else {
				topTenIndicator.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * 
	 */
	private void updateFollow() {

		if (post.getOriginalPostId() > 0) {
			if (follow != null) {
				if (this.post.isFollowing())
					follow.setVisibility(View.GONE);/*
													 * setImageBitmap(BitmapFactory
													 * . decodeResource(
													 * getActivity
													 * ().getResources(),
													 * R.drawable.followed));
													 */
				else {
					if (post.getUserId() == UserManager.getInstance()
							.getCurrentUserId())
						follow.setVisibility(View.GONE);
					else {
						follow.setVisibility(View.VISIBLE);
						follow.setImageBitmap(BitmapFactory.decodeResource(
								getActivity().getResources(),
								R.drawable.unfollowed));
					}
				}
			}
			if (reposterFollow != null) {
				if (this.post.isRePosterIsFollowing())
					reposterFollow.setVisibility(View.GONE);/*
															 * setImageBitmap(
															 * BitmapFactory.
															 * decodeResource(
															 * getActivity
															 * ().getResources
															 * (),
															 * R.drawable.followed
															 * ));
															 */
				else {
					reposterFollow.setVisibility(View.VISIBLE);
					reposterFollow.setImageBitmap(BitmapFactory
							.decodeResource(getActivity().getResources(),
									R.drawable.unfollowed));
				}

			}
		} else {
			if (follow != null) {
				if (this.post.isFollowing()) {
					follow.setVisibility(View.GONE);
					reposterFollow.setVisibility(View.GONE);/*
															 * setImageBitmap(
															 * BitmapFactory .
															 * decodeResource(
															 * getActivity
															 * ().getResources
															 * (),
															 * R.drawable.followed
															 * ));
															 */
				} else {
					if (post.getUserId() == UserManager.getInstance()
							.getCurrentUserId()) {
						follow.setVisibility(View.GONE);
						reposterFollow.setVisibility(View.GONE);
					} else {
						follow.setVisibility(View.VISIBLE);
						follow.setImageBitmap(BitmapFactory.decodeResource(
								getActivity().getResources(),
								R.drawable.unfollowed));
						reposterFollow.setVisibility(View.VISIBLE);
						reposterFollow.setImageBitmap(BitmapFactory
								.decodeResource(getActivity().getResources(),
										R.drawable.unfollowed));
					}
				}
			}
		}
	}

	/**
	 * @param tags
	 * @param tagsContainer2
	 */
	@SuppressLint("InflateParams")
	private void populateDefaultTags(List<KeyValuePairs> tags,
			LinearLayout tagsContainer) {
		for (int x = 0; x < tags.size(); x++) {
			final KeyValuePairs KeyValuePairs = tags.get(x);
			final View view = getActivity().getLayoutInflater().inflate(
					R.layout.share_tag_item, null);
			TextView userTag = (TextView) view.findViewById(R.id.tag_name_txt);
			userTag.setText(KeyValuePairs.getText().toString());
			ImageButton removeTag = (ImageButton) view
					.findViewById(R.id.remove_tag_btn);
			removeTag.setVisibility(View.INVISIBLE);
			tagsContainer.addView(view, 0);
			view.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					startActivity(TagListActivity.getIntent(getActivity(),
							KeyValuePairs.get_Id(), KeyValuePairs.getText()
									.toString(), 0, null, true));
				}
			});
		}
	}

	private void updatePostLikes() {
		if (post.isLiked()) {
			likeButton.setBackgroundResource(R.drawable.like_sc);
		} else {
			likeButton.setBackgroundResource(R.drawable.like);
		}

		if (post.getLikeCount() > 0)
			likeCount.setText(Integer.toString(post.getLikeCount()));
		else {
			likeCount.setText("");
		}
	}

	// private Period calcDiff(Date startDate, Date endDate) {
	// DateTime START_DT = (startDate == null) ? null
	// : new DateTime(startDate);
	// DateTime END_DT = (endDate == null) ? null : new DateTime(endDate);
	//
	// Period period = new Period(START_DT, END_DT);
	//
	// return period;
	//
	// }

	private String formatedate(long date1) {

		long time = date1 * (long) 1000;
		// Date date = new Date(time);
		// SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm",
		// Locale.getDefault());
		// format.setTimeZone(TimeZone.getTimeZone("GMT"));
		// String dt = format.format(date);
		// ---------------ago------------------------
		// Period dateDiff = calcDiff(new Date(time),
		// new Date(System.currentTimeMillis()));
		// String d = PeriodFormat.wordBased().print(dateDiff);
		// String[] da = d.split(",");
		// return da[0] + " ago";
		// ------------------------------------------
		Date date = new Date(time);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm",
				Locale.getDefault());
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		String beforeSplit = format.format(date);
		String[] afterSplit = beforeSplit.split(" ");
		return afterSplit[0] + "\n" + afterSplit[1];
	}

	/**
	 * 
	 */
	private void initPost() {
		this.post = (PostViewModel) getActivity().getIntent()
				.getSerializableExtra(ImageDetailsActivity.POST_KEY);
		WallManager.getInstance().getTags(post.getPostId());
		this.whichTab = getActivity().getIntent().getIntExtra(
				ImageDetailsActivity.WHICH_TAB, 0);
		modifiedPost = post;
	}

	/**
	 * 
	 */
	private void initControlsActions() {
		details.setOnClickListener(this);
		likeButton.setOnClickListener(this);
		likeCount.setOnClickListener(this);
		back.setOnClickListener(this);
		profileImage.setOnClickListener(this);
		userName.setOnClickListener(this);
		follow.setOnClickListener(this);
		comment.setOnClickListener(this);
		repost.setOnClickListener(this);
		share.setOnClickListener(this);
		use.setOnClickListener(this);
		editButton.setOnClickListener(this);
		deleteButton.setOnClickListener(this);
		reportImage.setOnClickListener(this);
		reposterProfileImage.setOnClickListener(this);
		reposterUserName.setOnClickListener(this);
	}

	/**
	 * @param rootView
	 */
	private void initView(View rootView) {
		postImage = (ImageView) rootView.findViewById(R.id.postImage);
		details = (ImageButton) rootView.findViewById(R.id.details);
		likeButton = (ImageButton) rootView.findViewById(R.id.like_btn);
		likeCount = (TextView) rootView.findViewById(R.id.likes_num_txt);
		back = (ImageButton) rootView.findViewById(R.id.back);
		profileImage = (ImageView) rootView.findViewById(R.id.user_pic_img);
		userName = (TextView) rootView.findViewById(R.id.username_txt);
		follow = (ImageView) rootView.findViewById(R.id.follow);
		// repostedBy = (TextView) rootView.findViewById(R.id.reposted_by);
		dateText = (TextView) rootView.findViewById(R.id.date_txt);
		topTenIndicator = (LinearLayout) rootView
				.findViewById(R.id.topTenIndicator);
		topTenRank = (TextView) rootView.findViewById(R.id.topTenRank);
		comment = (TextView) rootView.findViewById(R.id.comments_num_txt);
		repost = (ImageButton) rootView.findViewById(R.id.repost_btn);
		share = (ImageButton) rootView.findViewById(R.id.share_btn);
		use = (LinearLayout) rootView.findViewById(R.id.use_btn);
		editAndDelete = (LinearLayout) rootView
				.findViewById(R.id.editAndDeleteLayout);
		tagsProgress = (ProgressBar) rootView.findViewById(R.id.tagsProgress);
		editButton = (LinearLayout) rootView.findViewById(R.id.edit_btn_id);
		deleteButton = (LinearLayout) rootView.findViewById(R.id.delete_btn_id);
		tagsContainer = (LinearLayout) rootView
				.findViewById(R.id.TagsContainer);
		captionText = (TextView) rootView.findViewById(R.id.Image_caption);
		reportImage = (Button) rootView.findViewById(R.id.report_post_btn_id);
		reposterUserName = (TextView) rootView
				.findViewById(R.id.reposter_username_txt);
		reposterProfileImage = (ImageView) rootView
				.findViewById(R.id.Reposter_pic_img);
		reposterFollow = (ImageView) rootView
				.findViewById(R.id.reposter_follow);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.details:
			if (getActivity().getIntent().getBooleanExtra(
					ImageDetailsActivity.FROM_FULL_SCREEN, false))
				getActivity().onBackPressed();
			else
				getActivity().startActivity(
						PostFullScreenDetailsActivity.getIntent(
								getActivity(),
								getActivity().getIntent().getIntExtra(
										ImageDetailsActivity.POSITION, 0),
								whichTab, 0, WallManager.getInstance()
										.isFiltered(), WallManager
										.getInstance().isFilteredUsingFav()));
			break;
		case R.id.like_btn:
			handleLike(post.getPostId());
			break;
		case R.id.likes_num_txt:
			handleLikeCount();
			break;
		case R.id.back:
			getActivity().finish();
			break;
		case R.id.user_pic_img:
			if (NetworkingUtils.isNetworkConnected(getActivity()))
				if (post.getUserId() == UserManager.getInstance()
						.getCurrentUserId())
					startActivity(UserProfileActivity
							.getActivityIntent(getActivity()));
				else
					startActivity(FanProfileActivity.getIntent(getActivity(),
							post.getUserId(), post.getUserName()));
			else
				UIUtils.showToast(getActivity(),
						"Please make sure you are connected to Internet");
			break;
		case R.id.username_txt:
			if (NetworkingUtils.isNetworkConnected(getActivity()))
				startActivity(FanProfileActivity.getIntent(getActivity(),
						post.getUserId(), post.getUserName()));
			else
				UIUtils.showToast(getActivity(),
						"Please make sure you are connected to Internet");
			break;
		case R.id.follow:
			handleFollow();
			break;
		case R.id.reposter_follow:
			handleReposterFollow();
			break;
		case R.id.comments_num_txt:
			commentOnPhoto();
			break;
		case R.id.repost_btn:
			handleRepost(post.getPostId());
			break;
		case R.id.share_btn:
			sharePhoto();
			break;
		case R.id.use_btn:
			usePhoto();
			break;
		case R.id.edit_btn_id:
			// dialog.show();
			new EditPhoto().execute();
			break;
		case R.id.delete_btn_id:
			ShowAlertDilaog(DELETE_POST);
			break;
		case R.id.report_post_btn_id:
			ShowAlertDilaog(REPORT_POST);
			break;
		case R.id.Reposter_pic_img:
			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				if (post.getOriginalPostId() == 0)
					startActivity(FanProfileActivity.getIntent(getActivity(),
							post.getUserId(), post.getUserName()));
				else
					startActivity(FanProfileActivity.getIntent(getActivity(),
							post.getRePosterId(), post.getRepostedBy()));
			} else
				UIUtils.showToast(getActivity(),
						"Please make sure you are connected to Internet");
			break;
		case R.id.reposter_username_txt:
			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				if (post.getOriginalPostId() == 0)
					startActivity(FanProfileActivity.getIntent(getActivity(),
							post.getUserId(), post.getUserName()));
				else
					startActivity(FanProfileActivity.getIntent(getActivity(),
							post.getRePosterId(), post.getRepostedBy()));
			} else
				UIUtils.showToast(getActivity(),
						"Please make sure you are connected to Internet");
			break;
		default:
			break;
		}
	}

	private void handleReposterFollow() {
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			if (UserManager.getInstance().getCurrentUserId() != 0) {
				if (!post.isFollowing()) {
					follow.setImageResource(R.drawable.followed);
					WallManager.getInstance().handleFollow(
							UserManager.getInstance().getCurrentUserId(),
							post.getRePosterId(), post.getPostId(),
							getActivity());
					post.setFollowing(true);
					modifiedPost.setFollowing(true);
					if (post.getOriginalPostId() == 0) {
						follow.setImageResource(R.drawable.followed);
						post.setRePosterIsFollowing(true);
					}
					actions.add(PostViewFactory.ACTION_FOLLOW_REPOSTER);
				}
			} else {
				getActivity()
						.startActivityForResult(
								LogInActivity
										.getActivityIntent(
												getActivity(),
												UserManager.LOGIN_FOR_RESULT,
												new Intent()
														.putExtra(
																PostViewFactory.INTENT_EXTRA_ACTION,
																PostViewFactory.ACTION_FOLLOW_REPOSTER)),
								PostViewFactory.INTENT_REQUEST_CODE);
			}
		} else {
			UIUtils.showToast(getActivity(),
					"Please make sure you are connected to Internet");
		}
	}

	/**
	 * 
	 */
	private void handleLikeCount() {
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			startActivity(LikeActivity.getIntent(getActivity(),
					post.getPostId(), post.getLikeCount()));
		} else {
			getActivity().startActivityForResult(
					LogInActivity.getActivityIntent(getActivity(),
							UserManager.LOGIN_FOR_RESULT,
							new Intent().putExtra(
									PostViewFactory.INTENT_EXTRA_ACTION,
									PostViewFactory.ACTION_OPEN_LIKE)),
					PostViewFactory.INTENT_REQUEST_CODE);
		}
	}

	private void ShowAlertDilaog(final int Type) {
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			if (UserManager.getInstance().getCurrentUserId() != 0) {
				String Message = "";

				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case DialogInterface.BUTTON_POSITIVE:
							if (Type == DELETE_POST) {
								DeletePost();
								dialog.dismiss();
							} else if (Type == REPORT_POST) {
								ReportPost();
								dialog.dismiss();
							}
							break;

						case DialogInterface.BUTTON_NEGATIVE:
							dialog.dismiss();
							break;
						}
					}
				};

				CustomAlertDialog.customBuilder builder = new CustomAlertDialog.customBuilder(
						getActivity());

				// set the message and Buttons
				if (Type == DELETE_POST) {
					Message = getString(R.string.PhotoDetails_delete_alert);
					builder.setMessage(Message)
							.setPositiveButton("OK", dialogClickListener)
							.setNegativeButton("Cancel", dialogClickListener);
				} else if (Type == REPORT_POST) {
					Message = getString(R.string.PhotoDetails_report_alert);
					builder.setMessage(Message)
							.setPositiveButton(getString(R.string.report_alert), dialogClickListener)
							.setNegativeButton(getString(R.string.cancel_alert), dialogClickListener);
				}
				builder.create().show();
			} else {
				if (Type == REPORT_POST) {
					getActivity()
							.startActivityForResult(
									LogInActivity
											.getActivityIntent(
													getActivity(),
													UserManager.LOGIN_FOR_RESULT,
													new Intent()
															.putExtra(
																	PostViewFactory.INTENT_EXTRA_ACTION,
																	PostViewFactory.ACTION_REPORT)),
									PostViewFactory.INTENT_REQUEST_CODE);
				}
			}
		} else {
			UIUtils.showToast(getActivity(),
					"Please make sure you are connected to Internet");
		}
	}

	private void ReportPost() {
		if (post == null)
			return;
		WallManager.getInstance().ReportPost(
				UserManager.getInstance().getCurrentUserId(), post.getPostId());
	}

	private void DeletePost() {
		if (post == null)
			return;
		WallManager.getInstance().deletePost(post.getPostId(), getActivity());
		actions.add(PostViewFactory.ACTION_DELETE);
	}

	/**
	 * 
	 */
	private void handleFollow() {
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			if (UserManager.getInstance().getCurrentUserId() != 0) {
				if (!post.isFollowing()) {
					follow.setImageResource(R.drawable.followed);
					WallManager.getInstance().handleFollow(
							UserManager.getInstance().getCurrentUserId(),
							post.getUserId(), post.getPostId(), getActivity());
					post.setFollowing(true);
					modifiedPost.setFollowing(true);
					if (post.getOriginalPostId() == 0) {
						reposterFollow.setImageResource(R.drawable.followed);
						post.setRePosterIsFollowing(true);
					}
					actions.add(PostViewFactory.ACTION_FOLLOW);
				}
			} else {
				getActivity().startActivityForResult(
						LogInActivity.getActivityIntent(getActivity(),
								UserManager.LOGIN_FOR_RESULT,
								new Intent().putExtra(
										PostViewFactory.INTENT_EXTRA_ACTION,
										PostViewFactory.ACTION_FOLLOW)),
						PostViewFactory.INTENT_REQUEST_CODE);
			}
		} else {
			UIUtils.showToast(getActivity(),
					"Please make sure you are connected to Internet");
		}
	}

	/**
	 * 
	 */
	private void sharePhoto() {
		if (NetworkingUtils.isNetworkConnected(getActivity())) {

			Intent share = new Intent(Intent.ACTION_SEND);
			share.setType("image/jpeg");
			@SuppressWarnings("deprecation")
			File file = App.getInstance().getImageLoader().getDiscCache()
					.get(post.getPostPhotoUrl());
			File dir_image = new File(Environment.getExternalStorageDirectory()
					.getAbsolutePath() + File.separator + "MYImage");
			// create directory MYImage
			dir_image.mkdirs();

			// I CHOOSE TO SAVE
			// THE FILE IN THE SD CARD IN THE FOLDER "MYImage"

			File tmpFile = new File(dir_image, "temp.jpg");
			if (file != null) {
				copyFile(file.getAbsolutePath(), tmpFile.getAbsolutePath());
				newPath = Environment.getExternalStorageDirectory()
						.getAbsolutePath()
						+ File.separator
						+ "MYImage"
						+ File.separator + "temp.jpg";
				Uri sendFile = Uri.parse("file://" + newPath);
				share.putExtra(Intent.EXTRA_STREAM, sendFile);
				startActivity(Intent.createChooser(share, "Share Image"));
				// getActivity().startActivityForResult(
				// Intent.createChooser(share, "Share Image"), 5);

				// TODO share image
				// File file = App.getInstance().getImageLoader()
				// .getDiscCache().get(post.getPostPhotoUrl());
				// Intent shareActivityIntent = Share_Activity
				// .getActivityIntent(getActivity());
				// shareActivityIntent.putExtra("path",
				// file.getAbsolutePath());
				// getActivity().startActivity(shareActivityIntent);
			} else {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						UIUtils.showToast(getActivity(),
								"Retrieving image failed");
					}
				});
			}
		} else {
			UIUtils.showToast(getActivity(),
					"Please make sure you are connected to Internet");
		}
	}

	public static boolean copyFile(String from, String to) {
		try {
			int bytesum = 0;
			int byteread = 0;
			File oldfile = new File(from);
			if (oldfile.exists()) {
				InputStream inStream = new FileInputStream(from);
				FileOutputStream fs = new FileOutputStream(to);
				byte[] buffer = new byte[1444];
				while ((byteread = inStream.read(buffer)) != -1) {
					bytesum += byteread;
					fs.write(buffer, 0, byteread);
				}
				inStream.close();
				fs.close();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK
				&& requestCode == PostViewFactory.INTENT_REQUEST_CODE) {
			getResult = true;
			initPost();

			populatePost();

			int action = data.getIntExtra(PostViewFactory.INTENT_EXTRA_ACTION,
					0);
			switch (action) {
			case PostViewFactory.ACTION_LIKe:
				WallManager.getInstance().handleLike(WallManager.LIKE_POST,
						UserManager.getInstance().getCurrentUserId(),
						post.getPostId());
				likeButton.setBackgroundResource(R.drawable.like_sc);
				likeCount.setText(String.valueOf(post.getLikeCount() + 1));
				// post.setLiked(true);
				// post.setLikeCount(post.getLikeCount() + 1);
				modifiedPost.setLiked(true);
				modifiedPost.setLikeCount(post.getLikeCount() + 1);
				actions.add(PostViewFactory.ACTION_LIKe);
				break;
			case PostViewFactory.ACTION_DISLIKe:
				WallManager.getInstance().handleLike(WallManager.DISLIKE_POST,
						UserManager.getInstance().getCurrentUserId(),
						post.getPostId());
				likeButton.setBackgroundResource(R.drawable.like);
				likeCount.setText(post.getLikeCount() - 1);
				// post.setLiked(false);
				// post.setLikeCount(post.getLikeCount() - 1);
				modifiedPost.setLiked(false);
				modifiedPost.setLikeCount(post.getLikeCount() - 1);
				actions.add(PostViewFactory.ACTION_DISLIKe);
				break;
			case PostViewFactory.ACTION_COMMENT:
				getActivity().startActivity(
						CommentActivity.getIntent(getActivity(), post
								.getPostId(), post.getCommentCount(), whichTab,
								post.getUserId() == UserManager.getInstance()
										.getCurrentUserId() ? true : false));
				actions.add(PostViewFactory.ACTION_COMMENT);
				break;
			case PostViewFactory.ACTION_REPOST:
				WallManager.getInstance().handleRepost(
						UserManager.getInstance().getCurrentUserId(),
						post.getPostId());
				repost.setImageResource(R.drawable.repost_sc);
				UIUtils.showToast(getActivity(), getActivity().getResources()
						.getString(R.string.Home_repostAlert));
				actions.add(PostViewFactory.ACTION_REPOST);
				post.setRePosted(true);
				modifiedPost.setRePosted(true);
				break;
			case PostViewFactory.ACTION_OPEN_LIKE:
				getActivity().startActivity(
						LikeActivity.getIntent(getActivity(), post.getPostId(),
								post.getLikeCount()));
				break;
			case PostViewFactory.ACTION_REPORT:
				ShowAlertDilaog(REPORT_POST);
				break;
			case PostViewFactory.ACTION_FOLLOW:
				handleFollow();
				actions.add(PostViewFactory.ACTION_FOLLOW);
				post.setFollowing(true);
				modifiedPost.setFollowing(true);
				break;
			case PostViewFactory.ACTION_FOLLOW_REPOSTER:
				handleReposterFollow();
				actions.add(PostViewFactory.ACTION_FOLLOW_REPOSTER);
				post.setRePosterIsFollowing(true);
				modifiedPost.setRePosterIsFollowing(true);
				break;
			default:
				break;
			}
		}

	}

	/**
	 * 
	 */
	private void usePhoto() {
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			if (UserManager.getInstance().getCurrentUserId() != 0) {
				dialog.show();
				WallManager.getInstance().getStudioPhoto(
						post.getStudioPhotoId());
				// TODO useButton
				// StudioManager.getInstance().getStudioPhoto(post.getStudioPhotoId());
			} else {
				getActivity()
						.startActivityForResult(
								LogInActivity.getActivityIntent(getActivity(),
										UserManager.LOGIN_FOR_RESULT,
										new Intent()), 55);
			}
		} else {
			UIUtils.showToast(getActivity(),
					"Please make sure you are connected to Internet");
		}
	}

	/**
	 * @param postId
	 */
	private void handleRepost(int postId) {
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			if (UserManager.getInstance().getCurrentUserId() == post
					.getUserId()) {
				UIUtils.showToast(getActivity(),
						"You can't repost your own posts");
			} else {
				if (!post.isRePosted())
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						WallManager.getInstance().handleRepost(
								UserManager.getInstance().getCurrentUserId(),
								post.getPostId());
						repost.setBackgroundResource(R.drawable.repost_sc);
						post.setRePosted(true);
						modifiedPost.setRePosted(true);
						actions.add(PostViewFactory.ACTION_REPOST);
						UIUtils.showToast(
								getActivity(),
								getActivity().getResources().getString(
										R.string.Home_repostAlert));
					} else {
						getActivity()
								.startActivityForResult(
										LogInActivity
												.getActivityIntent(
														getActivity(),
														UserManager.LOGIN_FOR_RESULT,
														new Intent()
																.putExtra(
																		PostViewFactory.INTENT_EXTRA_ACTION,
																		PostViewFactory.ACTION_REPOST)),
										PostViewFactory.INTENT_REQUEST_CODE);
					}
				// TODO navigate to login
				// getActivity().startActivity(
				// LogInActivity.getActivityIntent(getActivity()));tyIntent(getActivity()));
			}
		} else {
			UIUtils.showToast(getActivity(),
					"Please make sure you are connected to Internet");
		}
	}

	private void commentOnPhoto() {
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			startActivity(CommentActivity.getIntent(getActivity(), post
					.getPostId(), post.getCommentCount(), whichTab, post
					.getUserId() == UserManager.getInstance()
					.getCurrentUserId() ? true : false));
			actions.add(PostViewFactory.ACTION_COMMENT);
		} else {
			getActivity().startActivityForResult(
					LogInActivity.getActivityIntent(getActivity(),
							UserManager.LOGIN_FOR_RESULT,
							new Intent().putExtra(
									PostViewFactory.INTENT_EXTRA_ACTION,
									PostViewFactory.ACTION_COMMENT)),
					PostViewFactory.INTENT_REQUEST_CODE);
		}
	}

	/**
	 * @param postId
	 */
	private void handleLike(int postId) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (NetworkingUtils.isNetworkConnected(getActivity())) {
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						if (post.isLiked()) {
							likeButton.setBackgroundResource(R.drawable.like);
							if (post.getLikeCount() > 1)
								likeCount.setText(String.valueOf(post
										.getLikeCount() - 1));
							else
								likeCount.setText("");
							WallManager.getInstance().handleLike(
									WallManager.DISLIKE_POST,
									UserManager.getInstance()
											.getCurrentUserId(),
									post.getPostId());
							// post.setLiked(false);
							// post.setLikeCount(post.getLikeCount() - 1);
							modifiedPost.setLiked(false);
							modifiedPost.setLikeCount(post.getLikeCount() - 1);
							actions.add(PostViewFactory.ACTION_DISLIKe);
						} else if (!post.isLiked()) {
							likeButton
									.setBackgroundResource(R.drawable.like_sc);
							likeCount.setText(String.valueOf(post
									.getLikeCount() + 1));
							WallManager.getInstance().handleLike(
									WallManager.LIKE_POST,
									UserManager.getInstance()
											.getCurrentUserId(),
									post.getPostId()); // post.setLiked(true);
							// post.setLiked(true);
							// post.setLikeCount(post.getLikeCount() + 1);
							modifiedPost.setLiked(true);
							modifiedPost.setLikeCount(post.getLikeCount() + 1);
							actions.add(PostViewFactory.ACTION_LIKe);
						}
					} else {
						getActivity()
								.startActivityForResult(
										LogInActivity
												.getActivityIntent(
														getActivity(),
														UserManager.LOGIN_FOR_RESULT,
														new Intent()
																.putExtra(
																		PostViewFactory.INTENT_EXTRA_ACTION,
																		post.isLiked() ? PostViewFactory.ACTION_DISLIKe
																				: PostViewFactory.ACTION_LIKe)),
										PostViewFactory.INTENT_REQUEST_CODE);
					}
				} else {
					UIUtils.showToast(getActivity(),
							"Please make sure you are connected to Internet");
				}
			}
		});
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (newPath != null) {
			ScheduledExecutorService worker = Executors
					.newSingleThreadScheduledExecutor();
			Runnable task = new Runnable() {
				public void run() {
					deleteFileNoThrow(newPath);
				}
			};
			worker.schedule(task, 5 * 60, TimeUnit.SECONDS);
		}
	}

	private boolean deleteFileNoThrow(String path) {
		File file;
		try {
			file = new File(path);
		} catch (NullPointerException e) {
			return false;
		}

		if (file.exists()) {
			return file.delete();
		}
		return false;
	}

	@Override
	public void onException(AppException ex) {
		tagsProgress.setVisibility(View.GONE);
		dialog.dismiss();
		if (ex.getErrorCode() != AppException.NO_DATA_EXCEPTION)
			UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void getCount(int count) {
		if (count > 0) {
			comment.setText(String.valueOf(count));
			post.setCommentCount(count);
		} else {
			comment.setText("");
		}
	}

	@Override
	public void onStudioPhotoRecieved(StudioPhoto photo) {
		new DownloadImageShare(photo).execute(photo.getPhotoUrl());
	}

	@Override
	public void onStudioPhotoRecievedFailed(String msg) {
		dialog.dismiss();
		UIUtils.showToast(getActivity(), msg);
	}

	private class DownloadImageShare extends AsyncTask<String, Void, String> {

		StudioPhoto photo;

		public DownloadImageShare(StudioPhoto photo) {
			this.photo = photo;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		protected String doInBackground(String... urls) {
			String path = "";
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
				path = saveToInternalSorage(mIcon11);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return path;
		}

		protected void onPostExecute(String result) {
			dialog.dismiss();
			startActivity(CaptureActivity.getActivityIntent(getActivity(),
					result, photo, true));
		}
	}

	private String saveToInternalSorage(Bitmap bitmapImage) {
		ContextWrapper cw = new ContextWrapper(getActivity());
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		// Create imageDir
		File mypath = new File(directory, "image.jpg");

		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(mypath);

			// Use the compress method on the BitMap object to write image to
			// the OutputStream
			bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return directory.getAbsolutePath();
	}

	private class EditPhoto extends AsyncTask<Void, Void, StudioPhoto> {

		boolean exception = false;
		private Handler customHandler;
		StudioPhoto photo = null;

		@Override
		protected void onPreExecute() {
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected StudioPhoto doInBackground(Void... params) {
			try {
				photo = WallManager.getInstance().getStudioPhotoFromServer(
						post.getStudioPhotoId());
			} catch (final Exception e) {
				exception = true;
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						dialog.dismiss();
						UIUtils.showToast(getActivity(), AppException
								.getAppException(e).getMessage());
					}
				});
				e.printStackTrace();
			}
			return photo;
		}

		@Override
		protected void onPostExecute(StudioPhoto result) {
			if (!exception) {
				customHandler = new Handler();
				customHandler.postDelayed(checkData, 0);
			}
			super.onPostExecute(result);
		}

		private Runnable checkData = new Runnable() {

			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				File file = App.getInstance().getImageLoader().getDiscCache()
						.get(post.getPostPhotoUrl());
				if (file == null) {
					customHandler.postDelayed(this, 1000);
				} else {
					String path = file.getAbsolutePath();
					Camera camera = Camera.open();// open camera
					android.hardware.Camera.Parameters parameters = camera.getParameters();
					android.hardware.Camera.Size size = parameters.getPictureSize();
					startActivity(ShareActivity.getActivityIntent(
							getActivity(), photo, path, "portrait", null,
							false, false, post.getPostId(),false , size.width));
					dialog.dismiss();
				}
			}
		};

	}

	/*@Override
	public void onSuccess(List<KeyValuePairs> tags) {
		tagsProgress.setVisibility(View.GONE);
		if (tags != null && tags.size() > 0)
			populateDefaultTags(tags, tagsContainer);
	}*/

	@Override
	public void onSuccess(TagsData tagsData) {

		tagsProgress.setVisibility(View.GONE);
		if (tagsData != null)
			{
			if( tagsData.getTags() != null && tagsData.getTags().size() > 0)
				populateDefaultTags(tagsData.getTags(), tagsContainer);
			// set report visibility
			if(tagsData.isReported())
				reportImage.setVisibility(View.GONE);
			else
				reportImage.setVisibility(View.VISIBLE);
				
			}
		
	
	}

}
