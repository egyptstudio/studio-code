package com.tawasol.barcelona.ui.fragments;

import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.vending.billing.util.BarcaIabHelper;
import com.android.vending.billing.util.BarcaIabHelper.OnIabPurchaseFinishedListener;
import com.android.vending.billing.util.BarcaIabHelper.OnIabSetupFinishedListener;
import com.android.vending.billing.util.BarcaIabResult;
import com.android.vending.billing.util.BarcaPurchase;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.SharedPrefsHelper;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.InAppPointsEntity;
import com.tawasol.barcelona.managers.ShopManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.ShopActivityTabs;
import com.tawasol.barcelona.utils.NetworkingUtils;
import com.tawasol.barcelona.utils.UIUtils;

public class PointPAckages extends Fragment implements
		OnIabSetupFinishedListener, OnIabPurchaseFinishedListener {

	private BarcaIabHelper billingHelper;
	private static final String PURCHASE_PAYLOAD_CACHE_KEY = "com.tawasol.Barchelona.PAYLOAD";
	public static final String RESPONSE_PRODUCT_ID = "productId";
	public static final String RESPONSE_PAYLOAD = "developerPayload";
	public static boolean shopAvailable;
	public static final int loginRequestCode = 1547;
	PointsAdapter adapter;

	ListView pointsList;
	ProgressBar pointsProgress;
	private Handler customHandler;
	TextView offline;
	private ProgressDialog dialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.points_packages, container,
				false);
		dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			billingHelper = new BarcaIabHelper(getActivity(),
					Params.Common.BASE_64_KEY);
			billingHelper.startSetup(this);
			pointsList = (ListView) rootView.findViewById(R.id.pointsList);
			pointsProgress = (ProgressBar) rootView
					.findViewById(R.id.pointsPackagesProgress);
			offline = (TextView) rootView.findViewById(R.id.offline);
			customHandler = new Handler();
			customHandler.postDelayed(checkData, 0);
		} else {
			offline.setVisibility(View.VISIBLE);
		}
		return rootView;
	}

	private Runnable checkData = new Runnable() {

		@Override
		public void run() {
			if (ShopManager.getInstance().isDataAvailableException()) {
				pointsProgress.setVisibility(View.GONE);
				UIUtils.showToast(getActivity(), ShopManager.getInstance()
						.dataRetrievalException());
			} else {
				if (!ShopManager.getInstance().isDataAvailable()) {
					pointsProgress.setVisibility(View.VISIBLE);
					customHandler.postDelayed(this, 1000);
				} else {
					pointsProgress.setVisibility(View.GONE);
					setAdapter();
				}
			}
		}
	};

	protected void setAdapter() {
		List<InAppPointsEntity> points = ShopManager.getInstance().getPoints();
		if (points != null) {
			adapter = new PointsAdapter(points);
			pointsList.setAdapter(adapter);
		}
	}

	private class PointsAdapter extends BaseAdapter {

		List<InAppPointsEntity> points;

		public PointsAdapter(List<InAppPointsEntity> points) {
			this.points = points;
		}

		public List<InAppPointsEntity> getPoints() {
			return this.points;
		}

		@Override
		public int getCount() {
			return points.size();
		}

		@Override
		public Object getItem(int position) {
			return points.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rootView = convertView;
			final InAppPointsEntity point = points.get(position);
			if (rootView == null) {
				rootView = LayoutInflater.from(
						App.getInstance().getApplicationContext()).inflate(
						R.layout.points_item, parent, false);
			}
			TextView greenNum = (TextView) rootView.findViewById(R.id.greenNum);
			TextView smallNum = (TextView) rootView.findViewById(R.id.samllNum);

			TextView pointDescription = (TextView) rootView
					.findViewById(R.id.Pointdescription);
			TextView freePoints = (TextView) rootView
					.findViewById(R.id.freePoints);

			if (point.getDescription() != null) {
				if (point.getDescription().contains("+")) {
					String[] description = point.getDescription().split(
							Pattern.quote("+"));
					pointDescription.setText(description[0].trim());
					freePoints.setVisibility(View.VISIBLE);
					freePoints.setText(description[1].trim());
				} else {
					pointDescription.setText(point.getDescription());
					freePoints.setVisibility(View.GONE);
				}
			}
			if (point.getTitle() != null) {
				greenNum.setText(point.getTitle());
				smallNum.setText(point.getTitle()
						+ getActivity().getResources().getString(
								R.string.points));
			}

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			int digitNum = Integer.parseInt(point.getTitle());
			if(digitNum(digitNum) == 4)
				params.setMargins((int)getActivity().getResources().getDimension(R.dimen.marginLeft), (int)getActivity().getResources().getDimension(R.dimen.marginTop), 0, 0);
			else
				params.setMargins((int)getActivity().getResources().getDimension(R.dimen.marginLeftLarge), (int)getActivity().getResources().getDimension(R.dimen.marginTop), 0, 0);

			greenNum.setLayoutParams(params);
			rootView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						if (shopAvailable) {
							String purchasePayload = BarcaIabHelper.ITEM_TYPE_INAPP
									+ ":" + UUID.randomUUID().toString();
							savePurchasePayload(purchasePayload);
							if (billingHelper != null)
								billingHelper.flagEndAsync();
							billingHelper.launchPurchaseFlow(getActivity(),
									point.getInAppId(),
									ShopActivityTabs.POINTS_REQUEST_CODE,
									PointPAckages.this, purchasePayload);
						} else {
							UIUtils.showToast(getActivity(),
									"SomeThing went wrong");
						}
					} else {
						getActivity().startActivityForResult(
								LogInActivity.getActivityIntent(getActivity(),
										UserManager.LOGIN_FOR_RESULT,
										new Intent()),
								loginRequestCode);
					}
				}
			});

			return rootView;
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		int responseCode = data.getIntExtra(BarcaIabHelper.RESPONSE_CODE,
				BarcaIabHelper.BILLING_RESPONSE_RESULT_OK);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK
				&& responseCode == BarcaIabHelper.BILLING_RESPONSE_RESULT_OK) {
			if (requestCode == ShopActivityTabs.POINTS_REQUEST_CODE) {
				try {
					String purchaseData = data
							.getStringExtra(BarcaIabHelper.RESPONSE_INAPP_PURCHASE_DATA);
					JSONObject purchase = new JSONObject(purchaseData);
					String developerPayload = purchase
							.getString(RESPONSE_PAYLOAD);
					if (developerPayload == null)
						developerPayload = "";
					if (getPurchasePayload().equals(developerPayload)) {
						billingHelper.handleActivityResult(requestCode,
								resultCode, data);
					}
				} catch (Exception e) {

				}
			}
		} else if (resultCode == Activity.RESULT_OK
				&& requestCode == FansListFragment.INTENT_RESULT_CODE) {
			((BaseActivity) getActivity()).refreshMenu();
		}
	}

	@Override
	public void onIabPurchaseFinished(BarcaIabResult result, BarcaPurchase info) {
		if (result.isFailure()) {
			// dealWithPurchaseFailed(result);
		} else if (result.isSuccess()) {
			dealWithPurchaseSuccess(info);
		}
	}

	private void dealWithPurchaseSuccess(BarcaPurchase info) {
		if (adapter != null) {
			if (adapter.getPoints() != null) {
				InAppPointsEntity point = adapter.getPoints().get(
						adapter.getPoints().indexOf(
								new InAppPointsEntity(info.getSku(), 0)));
				if (point != null) {
					dialog.show();
					ShopManager.getInstance().addPointsForUser(
							UserManager.getInstance().getCurrentUserId(),
							Integer.parseInt(point.getTitle()), info,
							billingHelper, dialog, true);
				}
			}
		}
	}

	@Override
	public void onIabSetupFinished(BarcaIabResult result) {
		if (result.isSuccess())
			shopAvailable = true;
		String purchaseToken = "inapp:" + getActivity().getPackageName()
				+ ":android.test.purchased";
		BarcaPurchase info = new BarcaPurchase(purchaseToken,
				"android.test.purchased", 0);
		BarcaIabHelper.OnConsumeFinishedListener listener = new BarcaIabHelper.OnConsumeFinishedListener() {

			@Override
			public void onConsumeFinished(BarcaPurchase purchase,
					BarcaIabResult result) {
				if (result.isSuccess())
					return;
				if (result.isFailure())
					return;
			}
		};
		billingHelper.consumeAsync(info, listener);
	}

	protected void savePurchasePayload(String purchasePayload) {
		SharedPrefsHelper.setRegiterKey(PURCHASE_PAYLOAD_CACHE_KEY,
				purchasePayload);
	}

	private String getPurchasePayload() {
		return SharedPrefsHelper.getRegisterKey(PURCHASE_PAYLOAD_CACHE_KEY);
	}

	private int digitNum(int n) {
		if (n < 100000) {
			// 5 or less
			if (n < 100) {
				// 1 or 2
				if (n < 10)
					return 1;
				else
					return 2;
			} else {
				// 3 or 4 or 5
				if (n < 1000)
					return 3;
				else {
					// 4 or 5
					if (n < 10000)
						return 4;
					else
						return 5;
				}
			}
		} else {
			// 6 or more
			if (n < 10000000) {
				// 6 or 7
				if (n < 1000000)
					return 6;
				else
					return 7;
			} else {
				// 8 to 10
				if (n < 100000000)
					return 8;
				else {
					// 9 or 10
					if (n < 1000000000)
						return 9;
					else
						return 10;
				}
			}
		}
	}

}
