package com.tawasol.barcelona.ui.fragments;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.entities.WALLPosts;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.ImageDetailsActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.PostFullScreenDetailsActivity;
import com.tawasol.barcelona.ui.custom.pager.CustomViewPager;
import com.tawasol.barcelona.ui.custom.pager.OverscrollContainer.OnOverScrolledListener;
import com.tawasol.barcelona.ui.custom.pager.OverscrollViewPager;
import com.tawasol.barcelona.ui.custom.pager.VerticalOverscrollViewPager;
import com.tawasol.barcelona.utils.UIUtils;
import com.tawasol.barcelona.utils.Utils;

/**
 * 
 * @author Basyouni
 *
 */
public class PostFullScreenFourDirectionSwipe extends Fragment {
	static DisplayImageOptions options;

	CustomViewPager verticalOverscrollViewPager;
	VerticalPagerAdapter verticalPagerAdapter;
	HorizontalViewPager childPager;
	private int position;
	private int childPosition;
	OverscrollViewPager overscrollViewPager;
	private long currentUpdateTime;
	private long lastRequestTime;
	private static final int REQUEST_TYPE_OLDER = 1;
	private static final int REQUEST_TYPE_NEWER = 2;

	private static final int REQUEST_CODE = 48884;
	private List<PostViewModel> verticalPosts = new ArrayList<PostViewModel>();
	// private List<PostViewModel> horizontalPosts = new
	// ArrayList<PostViewModel>();
	Map<Integer, List<PostViewModel>> data = new LinkedHashMap<Integer, List<PostViewModel>>();
	private ProgressDialog dialog;
	int whichTab;
	int currentPosition;

	interface UpdatableAdapter {
		void update(int position);
	}

	interface HorizontalUpdatableAdapter {
		void update(View convertView, int position);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.post_full_screen_details_fragment, container, false);

		dialog = new ProgressDialog(getActivity() , R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		dialog.setCancelable(false);
		populateScreen(rootView);

		return rootView;
	}

	private void populateScreen(View rootView) {

		int groupPosition = 0;
		whichTab = getActivity().getIntent().getIntExtra(
				PostFullScreenDetailsActivity.WHICH_TAB, 0);
		currentPosition = getActivity().getIntent().getIntExtra(
				PostFullScreenDetailsActivity.CURRENT_INDEX_BUNDLE_KEY, 0);
		final boolean isFiltered = getActivity().getIntent().getBooleanExtra(
				PostFullScreenDetailsActivity.IS_FILTERED, false);
		final boolean isFavorite = getActivity().getIntent().getBooleanExtra(
				PostFullScreenDetailsActivity.IS_FAVORITE, false);

		/** If tab is TopTen so we need group position of the current item **/
		if (whichTab == FragmentInfo.TOP_TEN) {
			groupPosition = getActivity().getIntent().getIntExtra(
					PostFullScreenDetailsActivity.GROUP_POSITION, 0);
		}

		currentUpdateTime = setCurrentUpdateTime(whichTab);

		/** Get posts from selected tab **/
		List<PostViewModel> comingPosts = loadOfflineData(whichTab,
				groupPosition, currentPosition, isFiltered);

		verticalOverscrollViewPager = (CustomViewPager) rootView
				.findViewById(R.id.post_full_scr_pager);
		verticalPagerAdapter = new VerticalPagerAdapter(comingPosts, whichTab,
				isFiltered, isFavorite, verticalOverscrollViewPager);
		verticalOverscrollViewPager.getOverscrollView().setAdapter(
				verticalPagerAdapter);
		verticalOverscrollViewPager.getOverscrollView().setCurrentItem(
				currentPosition);
		if(whichTab != FragmentInfo.MY_PIC){
			verticalOverscrollViewPager
				.setOnOverScrollListener(new OnOverScrolledListener() {
					@Override
					public void onOverScroll(int overScrollType) {
						if (OVER_SCROLL_TOP == overScrollType) {
//							dialog.setMessage("");
							new LoadTopMoreTask(verticalOverscrollViewPager
									.getOverscrollView().getCurrentItem(),
									isFiltered, isFavorite).execute(

							REQUEST_TYPE_NEWER, whichTab);
						} else if (OVER_SCROLL_BOTTOM == overScrollType) {
//							dialog.setMessage("");
							new LoadTopMoreTask(verticalOverscrollViewPager
									.getOverscrollView().getCurrentItem(),
									isFiltered, isFavorite).execute(
									REQUEST_TYPE_OLDER, whichTab);
						}
						dialog.show();
					}
				});
		}else
			verticalOverscrollViewPager.setPagingEnabled(false);
	}

	private long setCurrentUpdateTime(int whichTab) {
		long currentUpdateTimeOfTab = 0;
		if (whichTab == FragmentInfo.LATEST_TAB) {
			;
			currentUpdateTimeOfTab = WallManager.getInstance()
					.getLatestCurrentTime();
		} else if (whichTab == FragmentInfo.WALL) {
			currentUpdateTimeOfTab = WallManager.getInstance()
					.getWallCurrentTime();
		} else if (whichTab == FragmentInfo.MY_PIC) {
			currentUpdateTimeOfTab = WallManager.getInstance()
					.getMyPicsCurrentTime();
		}
		return currentUpdateTimeOfTab;
	}

	private List<PostViewModel> loadOfflineData(int whichTab,
			int groupPosition, int currentPosition, boolean isFiltered) {
		List<PostViewModel> comingPosts = new ArrayList<PostViewModel>();

		switch (whichTab) {

		/** If LatestTab check if filtered or not, then get posts List **/
		case FragmentInfo.LATEST_TAB:
			if (isFiltered)
				comingPosts.addAll(WallManager.getInstance().getfilteredList());
			else
				comingPosts.addAll(WallManager.getInstance().getLatest());
			break;

		/** If TopTenTab get TopTen posts List of selected groupPosition **/
		case FragmentInfo.TOP_TEN:
			comingPosts.addAll(WallManager.getInstance().getTopTenSessons()
					.get(groupPosition).getPosts());
			break;

		/** If WallTab check if filtered or not, then get posts List **/
		case FragmentInfo.WALL:
			if (isFiltered)
				comingPosts.addAll(WallManager.getInstance().getfilteredList());
			else
				comingPosts.addAll(WallManager.getInstance().getWall());
			break;

		/** If MyPicTab get MyPic selected post only "No vertical swipe" **/
		case FragmentInfo.MY_PIC:
			comingPosts.add(WallManager.getInstance().getMyPics()
					.get(currentPosition));
			break;

		default:
			break;
		}
		return comingPosts;
	}

	private class VerticalPagerAdapter extends PagerAdapter implements
			UpdatableAdapter {

		private int tabType = 0;
		private boolean isFiltered;
		private boolean isFavorite;
		VerticalOverscrollViewPager verticalOverscrollViewPager;

		public VerticalPagerAdapter(List<PostViewModel> posts, int tabType,
				boolean isFiltered, boolean isFavorite,
				VerticalOverscrollViewPager verticalOverscrollViewPager) {
			this.tabType = tabType;
			this.isFiltered = isFiltered;
			this.isFavorite = isFavorite;
			this.verticalOverscrollViewPager = verticalOverscrollViewPager;
			PostFullScreenFourDirectionSwipe.this.verticalPosts = posts;
		}

		// public List<PostViewModel> getList(){
		// return vert
		// }

		@Override
		public int getCount() {
			return PostFullScreenFourDirectionSwipe.this.verticalPosts.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {

			View convertView = LayoutInflater.from(container.getContext())
					.inflate(R.layout.fragment_vertical_page_item, container,
							false);

			convertView.setTag(verticalOverscrollViewPager.getOverscrollView()
					.getCurrentItem());

			initViews(convertView, position);

			container.addView(convertView);

			return convertView;
		}

		public void addPosts(List<PostViewModel> newPosts) {
			if (newPosts.size() > 0) {

				PostViewModel firstNewPost = newPosts.get(0);
				PostViewModel lastPost = verticalPosts
						.get(verticalPosts.size() - 1);

				if (firstNewPost.compareTo(lastPost) == -1) {
					/**
					 * first.lastUpdateTime - last.lastUpdateTime (last > first)
					 **/
					/** 1419242442 - 0 **/
					verticalPosts.addAll(0, newPosts);
				} else {
					verticalPosts.addAll(newPosts);
				}
				if (verticalPagerAdapter != null && getActivity() != null
						&& !getActivity().isFinishing())
					notifyDataSetChanged();
			} else
				System.out.println("No More Posts");
		}

		// @Override
		// public int getItemPosition(Object object) {
		// return POSITION_NONE;
		// }

		public void initViews(View rootView, int position) {
			final PostViewModel mPost = verticalPosts.get(position);
			if (mPost.getUserId() != 0) {
				List<PostViewModel> userPosts = WallManager.getInstance()
						.getPostsForFan(mPost.getUserId());
				Collections.sort(userPosts);
				data.put(position, userPosts);
				overscrollViewPager = (OverscrollViewPager) rootView
						.findViewById(R.id.vertical_pager_item);
				overscrollViewPager.setTag(position);
				childPager = new HorizontalViewPager(
						/* data.get(position) */userPosts, overscrollViewPager);

				final int index = userPosts.indexOf(mPost);
				// }
				overscrollViewPager.getOverscrollView().setAdapter(childPager);
				// overscrollViewPager.postDelayed(new Runnable() {
				//
				// @Override
				// public void run() {
				// overscrollViewPager.getOverscrollView().setCurrentItem(
				// index);
				// }
				// } , 1000);

				overscrollViewPager.getOverscrollView().setCurrentItem(index);
				// overscrollViewPager
				// .setOnOverScrollListener(new OnOverScrolledListener() {
				// @Override
				// public void onOverScroll(int overScrollType) {
				// final ProgressDialog dialog = new ProgressDialog(
				// getActivity());
				// if (OVER_SCROLL_LEFT == overScrollType) {
				// dialog.setMessage("Left");
				// } else if (OVER_SCROLL_RIGHT == overScrollType) {
				// dialog.setMessage("Right");
				// }
				//
				// new Handler().postDelayed(new Runnable() {
				// @Override
				// public void run() {
				// dialog.dismiss();
				// }
				// }, 3000);
				// dialog.show();
				// }
				// });

			}
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0.equals(arg1);
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public void update(int position) {
			// getItem(position);
		}
	}

	private class HorizontalViewPager extends PagerAdapter implements
			HorizontalUpdatableAdapter {

		List<PostViewModel> horizontalList;
		private DisplayImageOptions displayOptions;
		OverscrollViewPager overscrollViewPager;

		public HorizontalViewPager(List<PostViewModel> userPosts,
				OverscrollViewPager overscrollViewPager) {
			horizontalList = userPosts;
			this.overscrollViewPager = overscrollViewPager;
			displayOptions = new DisplayImageOptions.Builder()
					.showImageForEmptyUri(R.drawable.ic_launcher)
					.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
					.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
					.bitmapConfig(Bitmap.Config.RGB_565)
					.considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1000)).build();
		}

		public List<PostViewModel> getList() {
			return horizontalList;
		}

		@Override
		public int getCount() {
			return horizontalList.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View convertView = LayoutInflater.from(container.getContext())
					.inflate(R.layout.photo_details_pager_item, container,
							false);

			convertView.setTag(position);

			// data.put(position, horizontalList);
			updateView(convertView, position);

			container.addView(convertView);

			return convertView;
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public void update(View convertView, int position) {
			updateView(convertView, position);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		private void updateView(final View convertView, final int position) {
			final PostViewModel post = horizontalList.get(position);

			ImageView img = (ImageView) convertView
					.findViewById(R.id.photo_details_img);
			ImageView usrImg = (ImageView) convertView
					.findViewById(R.id.photo_details_usr_photo);
			TextView usrName = (TextView) convertView
					.findViewById(R.id.photo_details_usr_name);
			ImageView closeImg = (ImageView) convertView
					.findViewById(R.id.photo_details_close);
			ImageView infoImg = (ImageView) convertView
					.findViewById(R.id.photo_details_info);

			final TextView likesTxt = (TextView) convertView
					.findViewById(R.id.photo_details_likes_txt);

			final View contentLayout = convertView
					.findViewById(R.id.photo_details_content);
			final View progressView = convertView.findViewById(R.id.progress);
			contentLayout.setVisibility(View.VISIBLE);
			progressView.setVisibility(View.GONE);
			overscrollViewPager
					.setOnOverScrollListener(new OnOverScrolledListener() {

						@Override
						public void onOverScroll(int overScrollType) {
//							getArguments().putSerializable(POST_OBJ_BUNDLE_KEY,posts.get(pager.getOverscrollView().getCurrentItem()));
//							getActivity().setRequestedOrientation(
//									ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
							// contentLayout.setVisibility(View.GONE);
							// progressView.setVisibility(View.VISIBLE);
							// TODO: Request more posts

							if (OVER_SCROLL_LEFT == overScrollType) {
								dialog.setMessage("");
								new LoadMoreTask(HorizontalViewPager.this,
										convertView, overscrollViewPager
												.getOverscrollView()
												.getCurrentItem(),
										horizontalList).execute(
										REQUEST_TYPE_OLDER,
										verticalPosts.get(
												verticalOverscrollViewPager
														.getOverscrollView()
														.getCurrentItem())
												.getUserId());

							} else if (OVER_SCROLL_RIGHT == overScrollType) {
								dialog.setMessage("");
								new LoadMoreTask(HorizontalViewPager.this,
										convertView, overscrollViewPager
												.getOverscrollView()
												.getCurrentItem(),
										horizontalList).execute(
										REQUEST_TYPE_NEWER,
										verticalPosts.get(
												verticalOverscrollViewPager
														.getOverscrollView()
														.getCurrentItem())
												.getUserId());
							}
							dialog.show();
						}
					});

			// Post image
			if (!TextUtils.isEmpty(post.getPostPhotoUrl())) {
				App.getInstance()
						.getImageLoader()
						.displayImage(post.getPostPhotoUrl(), img,
								App.getInstance().getDisplayOption(),
								new SimpleImageLoadingListener() {
									@Override
									public void onLoadingCancelled(
											String imageUri, View view) {
										progressView.setVisibility(View.GONE);
										super.onLoadingCancelled(imageUri, view);
									}

									@Override
									public void onLoadingComplete(
											String imageUri, View view,
											Bitmap loadedImage) {
										progressView.setVisibility(View.GONE);
										super.onLoadingComplete(imageUri, view,
												loadedImage);
									}

									@Override
									public void onLoadingFailed(
											String imageUri, View view,
											FailReason failReason) {
										progressView.setVisibility(View.GONE);
										super.onLoadingFailed(imageUri, view,
												failReason);
									}

									@Override
									public void onLoadingStarted(
											String imageUri, View view) {
										progressView
												.setVisibility(View.VISIBLE);
										super.onLoadingStarted(imageUri, view);
									}

								});
			} else {
				// TODO set default image for photo post
			}

			// User profile image
			if (!TextUtils.isEmpty(post.getUserProfilePicUrl())) {
				App.getInstance()
						.getImageLoader()
						.displayImage(post.getUserProfilePicUrl(), usrImg,
								displayOptions);
			} else {
				// TODO set default image for profile
			}
			usrImg.setVisibility(Utils.isPortrait() ? View.VISIBLE : View.GONE);

			// User name
			usrName.setText(TextUtils.isEmpty(post.getUserName()) ? "" : post
					.getUserName());
			usrName.setVisibility(Utils.isPortrait() ? View.VISIBLE : View.GONE);

			// Likes
			likesTxt.setText(post.getLikeCount() <= 0 ? "" : String
					.valueOf(post.getLikeCount()));
			likesTxt.setCompoundDrawablesWithIntrinsicBounds(
					post.isLiked() ? R.drawable.like_sc : R.drawable.like, 0,
					0, 0);
			likesTxt.setVisibility(Utils.isPortrait() ? View.VISIBLE
					: View.GONE);
			likesTxt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					handleLike(post.getPostId(), post, likesTxt, whichTab);
				}
			});

			// Close icon
			closeImg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					getActivity().finish();
				}
			});
			closeImg.setVisibility(Utils.isPortrait() ? View.VISIBLE
					: View.GONE);

			childPager.getList();
			// Info icon
			infoImg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					getActivity().startActivityForResult(
							ImageDetailsActivity.getActivityIntent(
									getActivity(), post, whichTab,
									currentPosition, true,
									verticalOverscrollViewPager
											.getOverscrollView()
											.getCurrentItem(),
									overscrollViewPager.getOverscrollView()
											.getCurrentItem()), REQUEST_CODE);
				}
			});
			infoImg.setVisibility(Utils.isPortrait() ? View.VISIBLE : View.GONE);
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			View view = container.findViewWithTag(position);
			if (view != null)
				container.removeView(view);
		}

		public void addPosts(List<PostViewModel> newPosts,
				List<PostViewModel> horizontalList) {
			if (newPosts.size() > 0) {
				Collections.sort(newPosts);

				PostViewModel firstNewPost = newPosts.get(0);
				PostViewModel lastPost = horizontalList.get(horizontalList
						.size() - 1);

				if (firstNewPost.compareTo(lastPost) < -1) {
					// horizontalList.addAll(0, newPosts);
					List<PostViewModel> posts = new ArrayList<PostViewModel>();
					posts.addAll(newPosts);
					posts.addAll(horizontalList);
					childPager.notifyDataSetChanged();
					verticalPagerAdapter.notifyDataSetChanged();
					OverscrollViewPager v = (OverscrollViewPager) verticalOverscrollViewPager
							.findViewWithTag(
									verticalOverscrollViewPager
											.getOverscrollView()
											.getCurrentItem()).findViewById(
									R.id.vertical_pager_item);
					v.getOverscrollView().setAdapter(
							new HorizontalViewPager(posts, v));

					// horizontalList.addAll(0, newPosts);
					// childPager.notifyDataSetChanged();
					v.getOverscrollView().setCurrentItem(newPosts.size());
					// childPager.notifyDataSetChanged();
					// verticalPagerAdapter.notifyDataSetChanged();
				} else {
					List<PostViewModel> posts = new ArrayList<PostViewModel>();
					posts.addAll(horizontalList);
					posts.addAll(newPosts);
					childPager.notifyDataSetChanged();
					verticalPagerAdapter.notifyDataSetChanged();
					OverscrollViewPager v = (OverscrollViewPager) verticalOverscrollViewPager
							.findViewWithTag(
									verticalOverscrollViewPager
											.getOverscrollView()
											.getCurrentItem()).findViewById(
									R.id.vertical_pager_item);
					v.getOverscrollView().setAdapter(
							new HorizontalViewPager(posts, v));
					v.getOverscrollView().setCurrentItem(horizontalList.size());
					// horizontalList.addAll(newPosts);
					// childPager.notifyDataSetChanged();
					// verticalPagerAdapter.notifyDataSetChanged();
				}

				// childPager.notifyDataSetChanged();
				// verticalPagerAdapter.notifyDataSetChanged();
			} else
				System.out.println("No More Posts");
		}

	}

	private class LoadMoreTask extends
			AsyncTask<Integer, Void, List<PostViewModel>> {

		private WeakReference<HorizontalUpdatableAdapter> adapterRef;
		private WeakReference<View> viewRef;
		private int position;
		private List<PostViewModel> horizontalPosts;

		public LoadMoreTask(HorizontalUpdatableAdapter adapter,
				View convertView, int position,
				List<PostViewModel> horizontalPosts) {
			adapterRef = new WeakReference<HorizontalUpdatableAdapter>(adapter);
			viewRef = new WeakReference<View>(convertView);
			this.position = position;
			this.horizontalPosts = horizontalPosts;
		}

		@Override
		protected List<PostViewModel> doInBackground(Integer... params) {
			int type = params[0];
			int fanId = params[1];

			List<PostViewModel> posts = null;
			if (type == REQUEST_TYPE_NEWER) {
				WALLPosts wallpost = WallManager.getInstance()
						.getFanNewerPosts(fanId, currentUpdateTime); // 1420116419

				if (wallpost != null && wallpost.getPosts() != null
						&& wallpost.getPosts().size() > 0) {
					posts = wallpost.getPosts();
					currentUpdateTime = wallpost.getCurrentRequestTime();
				}

			} else if (type == REQUEST_TYPE_OLDER) {
				lastRequestTime = horizontalPosts.get(0).getLastUpdateTime();
				WALLPosts wallpost = WallManager.getInstance()
						.getFanOlderPosts(fanId, lastRequestTime);

				if (wallpost != null && wallpost.getPosts() != null
						&& wallpost.getPosts().size() > 0) {// 1420041994
															// 1420041994
					posts = wallpost.getPosts();
				}
			}
			return posts;
		}

		@Override
		protected void onPostExecute(List<PostViewModel> result) {
			dialog.dismiss();
			// overscrollViewPager.findViewWithTag(overscrollViewPager.getOverscrollView().getCurrentItem()).findViewById(R.id.progress).setVisibility(View.GONE);

			HorizontalUpdatableAdapter adapter = adapterRef.get();
			View convertView = viewRef.get();

			if (adapter == null || convertView == null || result == null
					|| result.size() < 1) {
				if (getActivity() != null){
//					UIUtils.showToast(getActivity(), "No more Posts");
				}
				return;
			}
			if (result != null && !result.isEmpty()) {
				// adapter.update(convertView, position);

				childPager.addPosts(result, horizontalPosts);
			}

		}

	}

	private void handleLike(int postId, final PostViewModel post,
			final TextView likeButton, int tab) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (UserManager.getInstance().getCurrentUserId() != 0) {
					if (post.isLiked()) {

						likeButton.setCompoundDrawablesWithIntrinsicBounds(
								R.drawable.like, 0, 0, 0);
						int count = post.getLikeCount();

						likeButton.setText(count <= 1 ? "" : String
								.valueOf(count - 1));
						WallManager.getInstance().handleLike(
								WallManager.DISLIKE_POST,
								UserManager.getInstance().getCurrentUserId(),
								post.getPostId());
						if (checkPost(whichTab, post.getPostId()) == null) {
							post.setLiked(false);
							post.setLikeCount(post.getLikeCount() - 1);
						}
					} else if (!post.isLiked()) {
						likeButton.setCompoundDrawablesWithIntrinsicBounds(
								R.drawable.like_sc, 0, 0, 0);
						likeButton.setText(String.valueOf(post.getLikeCount() + 1));

						WallManager.getInstance().handleLike(
								WallManager.LIKE_POST,
								UserManager.getInstance().getCurrentUserId(),
								post.getPostId()); // post.setLiked(true);
						// pager.invalidate();
						if (checkPost(whichTab, post.getPostId()) == null) {
							post.setLiked(true);
							post.setLikeCount(post.getLikeCount() + 1);
						}
					}
				} else {
					if (getActivity() != null) {
						getActivity().startActivityForResult(
								LogInActivity.getActivityIntent(getActivity(),
										UserManager.LOGIN_FOR_RESULT,
										new Intent()), 4548);
					}
				}
			}
		});
	}

	public PostViewModel checkPost(int currentTab, int postID) {
		PostViewModel checkPost = null;
		if (currentTab == FragmentInfo.LATEST_TAB)
			checkPost = WallManager.getInstance().getLatestPostForId(postID);
		else if (currentTab == FragmentInfo.TOP_TEN)
			checkPost = WallManager.getInstance().getTopTenPostForId(postID);
		else if (currentTab == FragmentInfo.WALL)
			checkPost = WallManager.getInstance().getWallPostForId(postID);
		else if (currentTab == FragmentInfo.MY_PIC)
			checkPost = WallManager.getInstance().getMyPicsPostForId(postID);
		else if (currentTab == FragmentInfo.Filtered)
			checkPost = WallManager.getInstance().getFilterPostForId(postID);
		return checkPost;
	}

	private class LoadTopMoreTask extends
			AsyncTask<Integer, Void, List<PostViewModel>> {
		// private WeakReference<UpdatableAdapter> adapterRef;
		private int position;
		private int type;
		private boolean isFiltered;
		private boolean isFavorite;

		public LoadTopMoreTask(int position, /* UpdatableAdapter adapter, */
				boolean isFiltered, boolean isFavorite) {
			this.position = position;
			this.isFiltered = isFiltered;
			this.isFavorite = isFavorite;
			// adapterRef = new WeakReference<UpdatableAdapter>(adapter);
		}

		@Override
		protected List<PostViewModel> doInBackground(Integer... params) {
			type = params[0];
			int methodName = params[1];
			List<PostViewModel> posts = null;

			if (type == REQUEST_TYPE_NEWER) {
				WALLPosts wallpost = WallManager.getInstance().getNewerPosts(
						currentUpdateTime, getMethodName(methodName),
						isFiltered, isFavorite);
				if (wallpost != null && wallpost.getPosts() != null
						&& wallpost.getPosts().size() > 0) {
					posts = wallpost.getPosts();
					currentUpdateTime = wallpost.getCurrentRequestTime();
				}
			} else if (type == REQUEST_TYPE_OLDER) {
				lastRequestTime = verticalPosts.get(verticalPosts.size() - 1)
						.getLastUpdateTime();
				WALLPosts wallpost = WallManager.getInstance().getOlderPosts(
						lastRequestTime, getMethodName(methodName), isFiltered,
						isFavorite);
				if (wallpost != null && wallpost.getPosts() != null
						&& wallpost.getPosts().size() > 0) {
					posts = wallpost.getPosts();
				}
			}
			return posts;
		}

		@Override
		protected void onPostExecute(List<PostViewModel> result) {
			dialog.dismiss();
			// UpdatableAdapter adapter = adapterRef.get();
			// adapter.update(position);
			if (result == null)
				return;
			if (type == REQUEST_TYPE_NEWER) {
				result = checkRepetaedItemInList(verticalPosts, result);
				verticalPagerAdapter.addPosts(result);
			} else {
				// View v = pager.findViewWithTag(pager.getCurrentItem());
				// v.invalidate();
				verticalPagerAdapter.addPosts(result);
			}
		}

		public String getMethodName(int tabName) {
			String methodName = null;
			if (tabName == FragmentInfo.LATEST_TAB)
				methodName = "wall/getLatest";
			else if (tabName == FragmentInfo.WALL)
				methodName = "wall/getWall";
			else if (tabName == FragmentInfo.MY_PIC)
				methodName = "wall/getMyPosts";
			else if (tabName == FragmentInfo.TOP_TEN)
				methodName = "wall/getTopTen";
			return methodName;
		}
	}

	public List<PostViewModel> checkRepetaedItemInList(
			List<PostViewModel> oldPosts, List<PostViewModel> newPosts) {

		/**
		 * If new posts is > WallManager.NUM_OF_POSTS="10", then clear all
		 * oldList and add new posts
		 **/
		if (newPosts.size() > WallManager.NUM_OF_POSTS) {
			oldPosts.clear();
			oldPosts.addAll(newPosts);

			/**
			 * If new posts is < WallManager.NUM_OF_POSTS="10", then clear the
			 * repeated posts only in OldList and add new posts
			 **/
		} else if (newPosts.size() < WallManager.NUM_OF_POSTS
				&& newPosts.size() > 0) {
			for (int x = 0; x < newPosts.size(); x++) {
				for (int y = 0; y < oldPosts.size(); y++) {
					if (newPosts.get(x).getPostId() == oldPosts.get(y)
							.getPostId()) {
						oldPosts.remove(y);
					}
				}
				oldPosts.addAll(newPosts);
			}
		}
		return oldPosts;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode,
			final Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == REQUEST_CODE) {
				final PostViewModel post = ((PostViewModel) data
						.getSerializableExtra(ImageDetailsActivity.INFO_RETURN));
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// int index =
						// NewOmElEftkasa.this.data.get(verticalOverscrollViewPager.getOverscrollView().getCurrentItem()).indexOf(markedPost);
						// NewOmElEftkasa.this.data.get(verticalOverscrollViewPager.getOverscrollView().getCurrentItem()).remove(index);
						// NewOmElEftkasa.this.data.get(verticalOverscrollViewPager.getOverscrollView().getCurrentItem()).add(index,
						// ((PostViewModel)data.getSerializableExtra(ImageDetailsActivity.INFO_RETURN)));
						TextView v = (TextView) verticalOverscrollViewPager
								.findViewWithTag(
										verticalOverscrollViewPager
												.getOverscrollView()
												.getCurrentItem())
								.findViewById(R.id.photo_details_likes_txt);
						if (post.isLiked()) {
							v.setText(post.getLikeCount() > 0 ? String
									.valueOf(post.getLikeCount()) : "");
							v.setCompoundDrawablesWithIntrinsicBounds(
									R.drawable.like_sc, 0, 0, 0);
						} else {
							v.setText(post.getLikeCount() > 0 ? String
									.valueOf(post.getLikeCount()) : "");
							v.setCompoundDrawablesWithIntrinsicBounds(
									R.drawable.like, 0, 0, 0);
						}
						// v.requestLayout();
						// v.invalidate();
						int verticalPosition = data.getIntExtra(
								ImageDetailsActivity.VERTICAL_RETURN, 0);
						int horizontalPosition = data.getIntExtra(
								ImageDetailsActivity.HORIZONTAL_RETURN, 0);
						verticalOverscrollViewPager.getOverscrollView()
								.setCurrentItem(verticalPosition);
						overscrollViewPager.getOverscrollView().setCurrentItem(
								horizontalPosition);
						childPager.notifyDataSetChanged();
						verticalPagerAdapter.notifyDataSetChanged();
					}
				});
			}
		}
	}

}
