package com.tawasol.barcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.FragmentInfo;
/**
 * @author Turki
 * 
 */
public class ChatFragment extends Fragment {

	private FragmentTabHost mTabHost;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_chat, container,	false);

		initView(rootView);

		return rootView;
	}

	private void initView(View rootView) {
		FragmentInfo info;
		
		mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
		mTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
		mTabHost.getTabWidget().setStripEnabled(false);
		
		Bundle bundle = new Bundle();
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources().getString(R.string.Messages_Recent)), 
				R.drawable.chat_left_tab),
				RecentFragment.class, bundle);
		
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources().getString(R.string.Messages_Friends)),
				R.drawable.chat_center_tab), 
				FriendsFragment.class, bundle);
				
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources().getString(R.string.contacts)),
				R.drawable.chat_right_tab), 
				ContactsFragment.class, bundle);
		
		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
					TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(R.id.tab_text);
					tv.setTextColor(getActivity().getResources().getColor(R.color.new_yellow));
				}
				TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).findViewById(R.id.tab_text);
				tv.setTextColor(Color.WHITE);
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(R.id.tab_text);
			tv.setTextColor(getActivity().getResources().getColor(R.color.new_yellow));
		}
		TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).findViewById(R.id.tab_text);
		tv.setTextColor(Color.WHITE);
	}

	
	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid) {
		// TODO Auto-generated method stub
		View v = LayoutInflater.from(getActivity()).inflate(R.layout.tab_host_text, null);
		v.setBackgroundResource(resid);

		final TextView text = (TextView) v.findViewById(R.id.tab_text);
		text.setText(spec.getTag());
		text.setTextColor(getActivity().getResources().getColor(android.R.color.white));
		text.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mTabHost.getCurrentTabTag().equalsIgnoreCase(text.getText().toString())) {
					Fragment chat = getChildFragmentManager().findFragmentByTag(text.getText().toString());
					if(chat instanceof RecentFragment){
						
					}else if(chat instanceof FriendsFragment){
						
					}else if(chat instanceof ContactsFragment){
						
					}
				}else{
					mTabHost.setCurrentTabByTag(text.getText().toString());
				}
			}
		});
		return spec.setIndicator(v);
	}
}

