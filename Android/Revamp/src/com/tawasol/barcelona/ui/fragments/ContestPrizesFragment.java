package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.ContestData;
import com.tawasol.barcelona.entities.ContestPrize;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnContestReceived;
import com.tawasol.barcelona.managers.ContestManager;
import com.tawasol.barcelona.ui.adapters.ContestPrizesAdapter;

public class ContestPrizesFragment extends BaseFragment implements
		OnContestReceived {
	TextView contestNameTxt;
	ListView contestPrizesLV;
	ContestPrizesAdapter contestPrizesAdapter;
	ArrayList<ContestPrize> prizes;
	LinearLayout runningContestPrizesLayout, initialStateLayout;
	ContestData contest;
	ProgressBar prizesPB;
	TextView prizesTxt;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contest_prizes,
				container, false);
		prizes = new ArrayList<ContestPrize>();

		viewById(rootView);

		initViewcontrols();

		// get data from manager
		contest = ContestManager.getInstance().getContest();

		// showLoadingDialog();
		prizesPB.setVisibility(View.VISIBLE);

		// if null call manager to get data from WB , else reset layouts
		// according to contest object value
		if (contest == null)
			ContestManager.getInstance().getContestData();
		else if (contest.getContestPrize() != null) {
			// set the list and adapter of prizes
			prizes.addAll(contest.getContestPrize());
			contestPrizesAdapter.notifyDataSetChanged();
			// set the contest name
			contestNameTxt.setText(contest.getContest().getContestName());
			// set prizes text
			prizesTxt
					.setText(getResources().getString(R.string.Contest_Prizes));
			// hideLoadingDialog();
			prizesPB.setVisibility(View.GONE);
		}

		return rootView;
	}

	private void viewById(View rootView) {
		contestNameTxt = (TextView) rootView
				.findViewById(R.id.contest_name_txt);
		contestPrizesLV = (ListView) rootView
				.findViewById(R.id.contest_prizes_listview);
		runningContestPrizesLayout = (LinearLayout) rootView
				.findViewById(R.id.running_cintest_prizes_layout);
		initialStateLayout = (LinearLayout) rootView
				.findViewById(R.id.contest_initial_state_layout);
		prizesPB = (ProgressBar) rootView
				.findViewById(R.id.contest_prizes_frag_pb);
		prizesTxt = (TextView) rootView.findViewById(R.id.prizes_txt);
	}

	/** set adapter of the list */
	private void initViewcontrols() {
		contestPrizesAdapter = new ContestPrizesAdapter(getActivity(), prizes);
		contestPrizesLV.setAdapter(contestPrizesAdapter);
	}

	@Override
	public void onResume() {
		super.onResume();
		ContestManager.getInstance().addListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		ContestManager.getInstance().removeListener(this);
	}

	@Override
	public void onException(AppException ex) {
		// hideLoadingDialog();
		prizesPB.setVisibility(View.GONE);

		// hide the displayed data layout of prizes , and show no running
		// contest layout
		initialStateLayout.setVisibility(View.VISIBLE);
		runningContestPrizesLayout.setVisibility(View.GONE);
	}

	@Override
	public void onSuccess(ContestData obj) {
		// hideLoadingDialog();
		prizesPB.setVisibility(View.GONE);

		// validate the returned contest object and lists of it
		if (obj != null && obj.getContestPrize() != null
				&& !obj.getContestPrize().isEmpty()) {
			// update list and adapter
			prizes.clear();
			prizes.addAll(obj.getContestPrize());
			contestPrizesAdapter.notifyDataSetChanged();
			contest = obj;

			// set the name of the contest
			contestNameTxt.setText(contest.getContest().getContestName());
			// set the prizes text
			prizesTxt
					.setText(getResources().getString(R.string.Contest_Prizes));
		} else // if there the returned data wasn't valid , then hide the
				// current layout and show no running contest layout
		{
			initialStateLayout.setVisibility(View.VISIBLE);
			runningContestPrizesLayout.setVisibility(View.GONE);
		}

	}

}
