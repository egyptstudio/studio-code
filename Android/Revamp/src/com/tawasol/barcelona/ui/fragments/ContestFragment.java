package com.tawasol.barcelona.ui.fragments;

import com.tawasol.barcelona.R;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;



public class ContestFragment extends BaseFragment    {

	
	
	private FragmentTabHost contestTabHost;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contest, container,
				false);
		contestTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);
		setUpTabHost();
		
		return rootView;
	}
	
	private void setUpTabHost()
	{
		contestTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
		
		Bundle data;
		// adding Prizes tab
		data = new Bundle();
		contestTabHost.addTab(
				setIndicator(contestTabHost.newTabSpec(getActivity().getResources().getString(R.string.Contest_Prizes)), R.drawable.chat_left_tab),
				ContestPrizesFragment.class, data);
		
		
		//adding Rules tab
		contestTabHost.addTab(
				setIndicator(contestTabHost.newTabSpec(getActivity().getResources().getString(R.string.Contest_Rules)), R.drawable.chat_center_tab),
				ContestRulesFragment.class, data);
		
		
		// adding winners tab
				data = new Bundle();
				contestTabHost.addTab(
						setIndicator(contestTabHost.newTabSpec(getActivity().getResources().getString(R.string.contest_winners)), R.drawable.chat_right_tab),
						ContestWinnersFragment.class, data);
				
				contestTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

					@Override
					public void onTabChanged(String tabId) {
						setTabColors();
					}
				});
		}
	

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid) {
		View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_host_text, null);
		v.setBackgroundResource(resid);
		
		final TextView text = (TextView) v.findViewById(R.id.tab_text);
		text.setText(spec.getTag());
		
		
		text.setTextColor(getActivity().getResources().getColor(
				android.R.color.white));
		
		
		return spec.setIndicator(v);
	}
	@Override
	public void onResume() {
		super.onResume();
		setTabColors();
	}
	private void setTabColors()
	{
		for (int i = 0; i < contestTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) contestTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.tab_text);
			tv.setTextColor(getActivity().getResources().getColor(
					R.color.new_yellow));
		}
		TextView tv = (TextView) contestTabHost.getTabWidget()
				.getChildAt(contestTabHost.getCurrentTab())
				.findViewById(R.id.tab_text);
		tv.setTextColor(Color.WHITE);
	}
	

}
