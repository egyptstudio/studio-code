package com.tawasol.barcelona.ui.fragments;
  
import java.util.List;
import java.util.Locale;

import com.tawasol.barcelona.R; 
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.NotificationsTable;
import com.tawasol.barcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.LanguageEntity;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnSupportedLanguageResponseListener;
import com.tawasol.barcelona.managers.NotificationsManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.ui.activities.ChangePasswordActivity;
import com.tawasol.barcelona.ui.activities.ContactUsActivity;
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.PrivacySettingsActivity;
import com.tawasol.barcelona.ui.activities.SettingsActivity;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.UIUtils; 
 
import android.app.Dialog;
import android.os.Bundle; 
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * @author TURKI
 *  
 */
public class SettingsFragment extends BaseFragment implements OnClickListener, OnSupportedLanguageResponseListener{


	private TextView languageTxt, privacySettingsTxt, systemMsgsTxt, contactUsTxt, logoutTxt, changePasswordTxt;
	private ImageView languageArrow;
	private String listValue;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
		
		/** Initialize registration view **/
		init(rootView);

		return rootView;
	}

	/** Handle OnClickListeners **/
	@Override
	public void onClick(View v) {
	 
		switch (v.getId()) {
		case R.id.language_arrow:
			showList(languageTxt);
			break;
		case R.id.privacy_setting_txt:
			startActivity(PrivacySettingsActivity.getActivityIntent(getActivity()));
			break;
		case R.id.system_msg_txt:
//			startActivity(SystemMessagesActivity.getActivityIntent(getActivity()));
			UIUtils.showToast(getActivity(), "Not implememted yet");
			break;
		case R.id.contact_us_txt:
			startActivity(ContactUsActivity.getActivityIntent(getActivity()));
			break;
		case R.id.change_password_txt:
			startActivity(ChangePasswordActivity.getActivityIntent(getActivity()));
			break;
		case R.id.logout_txt:
			SharedPrefrencesDataLayer.saveIntPreferences(App.getInstance().getApplicationContext(),
					UserManager.LOGOUT_USER_ID_KEY, UserManager.getInstance().getCurrentUserId());
			User user = new User();
			UserManager.getInstance().cacheUser(user);
			((BaseActivity) getActivity()).refreshMenu();
			
			// cancel the periodic task of notifications , Added by Mohga
			App.getInstance().stopNotificationService();
			// reset the cached notification table 
			clearNotificationsData();
			// reset the last request time 
			NotificationsManager.getInstance().saveLastRequestTime(0);
			
			// navigate to home 
			startActivity(HomeActivity.getActivityIntent(getActivity()));
			//startActivity(LogInActivity.getActivityIntent(getActivity(),UserManager.NORMAL_LOGIN, null));
			break;
		}
	}
	
	private void clearNotificationsData()
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				NotificationsTable.getInstance().deleteAll();
			}
		});
		
	}
	

	/** Initialize registration view **/
	private void init(View rootView) {

		languageTxt        = (TextView) rootView.findViewById(R.id.language_list);
		privacySettingsTxt = (TextView) rootView.findViewById(R.id.privacy_setting_txt);
		contactUsTxt       = (TextView) rootView.findViewById(R.id.contact_us_txt);
		changePasswordTxt  = (TextView) rootView.findViewById(R.id.change_password_txt);
		logoutTxt          = (TextView) rootView.findViewById(R.id.logout_txt);
		systemMsgsTxt      = (TextView) rootView.findViewById(R.id.system_msg_txt);
		languageArrow      = (ImageView) rootView.findViewById(R.id.language_arrow);
		
		languageTxt.setText(SharedPrefrencesDataLayer.getStringPreferences(getActivity(), 
				LanguageUtils.APP_LANGUAGE_KEY, getDefaultLanguage()));
		if(SharedPrefrencesDataLayer.getIntPreferences(App.getInstance().getApplicationContext(),
				UserManager.LOGIN_TYPE, Params.APP_LOGIN)== Params.SOCIAL_LOGIN)
			changePasswordTxt.setVisibility(View.GONE);
		else
			changePasswordTxt.setVisibility(View.VISIBLE);
		languageArrow.setOnClickListener(this);
		privacySettingsTxt.setOnClickListener(this);
		contactUsTxt.setOnClickListener(this);
		changePasswordTxt.setOnClickListener(this);
		logoutTxt.setOnClickListener(this);
		systemMsgsTxt.setOnClickListener(this);
	}
	
	/**  Initialize of list Dialog **/
	private String showList(final TextView textView){

		String language[] = getResources().getStringArray(R.array.languages_list_items);
		final String langCode[] = getResources().getStringArray(R.array.languages_code_list_items);
		
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.settings_list_dialog);
	    
	    final ListView listView = (ListView) dialog.findViewById(R.id.settings_list);
	    
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,language){
	    	@Override
	    	public View getView(int position, View convertView, ViewGroup parent) {
	    		// TODO Auto-generated method stub
	    		View view = super.getView(position, convertView, parent);
	    		TextView textView = (TextView) view.findViewById(android.R.id.text1);
	    		textView.setGravity(Gravity.CENTER);
	    		textView.setTextSize(getActivity().getResources().getDimension(R.dimen.smallestTextSize));
	    		textView.setTextColor(getActivity().getResources().getColor(R.color.black));
	    		return view;
	    	}
	    };
	    
	    listView.setAdapter(adapter);
	    
	    listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
				listValue = listView.getItemAtPosition(position).toString();
				textView.setText(listValue);
				switchLanguage(langCode[position], listValue);
				dialog.dismiss();
			}
		});
	    dialog.setCancelable(true); 
	    dialog.show(); 
	    
	    return listValue;
	}

	@Override
	public void onSuccess(List<LanguageEntity> languageList) {
		UserManager.getInstance().addListener(this);		
	}

	@Override
	public void onException(AppException ex) {
		UserManager.getInstance().removeListener(this);		
	}
	
	private void switchLanguage(String languageCode, String language) {
		// Change application language
		LanguageUtils.changeApplicationLanguage(languageCode);
		
		// save app language in preferences
		SharedPrefrencesDataLayer.saveStringPreferences(getActivity(), LanguageUtils.APP_LANGUAGE_CODE_KEY, languageCode);
		SharedPrefrencesDataLayer.saveStringPreferences(getActivity(), LanguageUtils.APP_LANGUAGE_KEY, language);
		LanguageUtils.getInstance().setLanguage(languageCode);
		getActivity().finish();
		startActivity(SettingsActivity.getActivityIntent(getActivity()));
	}
	
	String getDefaultLanguage(){
		String defaultValue = "English"; 
		String langCode[] = getResources().getStringArray(R.array.languages_code_list_items);
		 for(int i=0; i<langCode.length; i++){
		    	if(Locale.getDefault().getISO3Language().substring(0, 2).equalsIgnoreCase(langCode[i])){
		    		defaultValue = Locale.getDefault().getDisplayLanguage();
		    		break;
		    	}
		    }
		return defaultValue;
	}
}
