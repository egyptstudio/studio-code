package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;

import android.app.Activity; 
import android.content.Intent; 
import android.os.Bundle; 
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.tawasol.barcelona.R; 
import com.tawasol.barcelona.data.connection.Params; 
import com.tawasol.barcelona.entities.InvitationEntity;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.exception.NullIntentException;
import com.tawasol.barcelona.listeners.OnInviteResponseListener;
import com.tawasol.barcelona.managers.UserManager; 
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.ui.activities.RegistrationActivity;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.UIUtils;
import com.tawasol.barcelona.utils.ValidatorUtils;

/**
 * 
 * @author Turki
 *
 */
public class InviteMessageFragment extends BaseFragment implements OnInviteResponseListener{

	private EditText invitationEt;
	private TextView userNameTxt;
	private String invitation = " ";
	private TextView inviteBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView      = inflater.inflate(R.layout.fragment_invite_message, container, false);

		/** Initialize registration view **/
		init(rootView);
		
		return rootView;
	}
	
	
	private void init(View rootView){
		invitationEt       = (EditText) rootView.findViewById(R.id.user_message_txt);
		userNameTxt        = (TextView) rootView.findViewById(R.id.user_name);
		inviteBtn          = (TextView) getActivity().findViewById(R.id.rightBtn);
		userNameTxt.setText(UserManager.getInstance().getCurrentUser().getFullName());
		
		/** Set InviteButton image with R.drawable.done **/
		inviteBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.done, 0);
		inviteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/** Call invite friend method to handle invitation process **/
				showLoadingDialog();
				inviteFriend();
			}
		});
	}
	/** Get PhoneList from coming intent **/
	private ArrayList<String> getPonesList(){
		Intent intent = getActivity().getIntent();
		Bundle bundle = intent.getBundleExtra(Params.SMS_INVITATION.PHONES_BUNDLE);
		ArrayList<String> list = (ArrayList<String>) bundle.getSerializable(Params.SMS_INVITATION.PHONES_LIST_NAME);	
		return list;
	}
	
	/** Build Invitation Entity and calling callInviteFriend method to send invitation**/
	private void inviteFriend(){
		/** Build Invitation Entity **/
		InvitationEntity invitationObj = new InvitationEntity();
		
		invitationObj.setPhoneNumbers(getPonesList());
		if(!ValidatorUtils.isRequired(invitationEt.getText().toString()))
			invitation = invitationEt.getText().toString();
		invitationObj.setInvitationText(invitation);
		invitationObj.setUserId(UserManager.getInstance().getCurrentUser().getUserId());
		
		/** Call SendInvitationSMS **/
		UserManager.getInstance().callInviteFriend(invitationObj,  LanguageUtils.getInstance().getDeviceLanguage());
	}
	
	/**
	 * @param loginType
	 * @param intent
	 * Three levels of registration :-
	 * 	1- Normal register, will go to HomeActivity after register successfully.
	 * 	2- Intent register, will go to specific intent after register successfully.
	 * 	3- Intent With Action register, will will go to specific intent and perform specific action after register successfully.	 
	 */
	public void proceedNextScreen(int loginType, Intent intent) {

		switch (loginType) {

		/** Normal Register **/
		case UserManager.NORMAL_LOGIN:
			startActivity(new Intent(getActivity(), HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;

		/** Intent Register **/
		case UserManager.LOGIN_WITH_INTENT:
			if (intent != null) {
				startActivity(intent);
			} else
				throw new NullIntentException();
			
		/** Intent With Action Register **/
		case UserManager.LOGIN_FOR_RESULT:
			if (intent != null) {
				getActivity().setResult(Activity.RESULT_OK, intent);
			} else
				throw new NullIntentException();
		default:
			break;
		}
	}
	
	/** Handle SendSMSInvitation response */
	@Override
	public void onSuccess() {
		// TODO Auto-generated method stub
		hideLoadingDialog();
		if(RegistrationActivity.returnedIntent != null)
			proceedNextScreen(RegistrationActivity.typeId, RegistrationActivity.returnedIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		else
			proceedNextScreen(RegistrationActivity.typeId, RegistrationActivity.returnedIntent);
		getActivity().finish();
	}

	/** Handle any exception in SendSMSInvitation */
	@Override
	public void onException(AppException ex) {
		// TODO Auto-generated method stub
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}
	
	/** Add Invitation listener onResume **/
	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	/** Remove Invitation listener onPause **/
	@Override
	public void onPause() {
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}
}
