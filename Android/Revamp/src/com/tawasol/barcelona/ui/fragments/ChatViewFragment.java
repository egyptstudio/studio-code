package com.tawasol.barcelona.ui.fragments;
 
import java.util.ArrayList; 
import java.util.Calendar; 
import java.util.List; 
 
import android.os.Bundle;    
import android.view.LayoutInflater;
import android.view.View;  
import android.view.View.OnClickListener; 
import android.view.ViewGroup; 
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener; 
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView; 
import android.widget.TextView;
 
import com.tawasol.barcelona.R;  
import com.tawasol.barcelona.entities.FriendsEntity;
import com.tawasol.barcelona.entities.Message; 
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnChatDeleteMessageListener;
import com.tawasol.barcelona.listeners.OnChatSendMessageListener;
import com.tawasol.barcelona.listeners.OnChatUpdatedListener;
import com.tawasol.barcelona.managers.ChatManager; 
import com.tawasol.barcelona.ui.activities.ChatViewActivity;
import com.tawasol.barcelona.ui.adapters.ChatViewAdapter; 
import com.tawasol.barcelona.utils.NetworkingUtils;
import com.tawasol.barcelona.utils.UIUtils; 
import com.tawasol.barcelona.utils.ValidatorUtils;

/**
 * 
 * @author Turki
 *
 */
public class ChatViewFragment extends BaseFragment implements OnChatUpdatedListener, OnChatSendMessageListener, OnChatDeleteMessageListener{
	
	private ArrayList<Message> messages = new ArrayList<Message>();
	private ChatViewAdapter adapter;
	private EditText text; 
	private ListView messageListView;
	private Button send;
	private FriendsEntity friendObj;
	private Message deletedMsg = new Message();
	private FrameLayout sendMsgValidationRuleFrame;
	private TextView sendTwoMsgTo;
	private Button addFriend;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView      = inflater.inflate(R.layout.fragment_chat_view, container, false);

		/** Get friend object from intent **/
		getBundle();
		
		/** Initialize registration view **/
		init(rootView);
		
		return rootView;
	}

	private void getBundle(){
		friendObj = (FriendsEntity) getActivity().getIntent().getSerializableExtra(ChatViewActivity.CONTACT_BUNDLE);
	}
	
	private void init(final View rootView){		
		sendMsgValidationRuleFrame  = (FrameLayout) rootView.findViewById(R.id.only_two_msg_validation_rule);
		addFriend        = (Button) rootView.findViewById(R.id.add_friend_btn);
		sendTwoMsgTo     = (TextView) rootView.findViewById(R.id.send_two_msg_to);
		
		text             = (EditText) rootView.findViewById(R.id.text);
		messageListView  = (ListView) rootView.findViewById(R.id.message_listView);
		messageListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int index, long id) { 
				Message msg = (Message) messageListView.getItemAtPosition(index);
//				showEditMessageDialog(msg, view);
				UIUtils.showToast(getActivity(), " " + msg.getId());
				return false;
			}
		});
		send = (Button) rootView.findViewById(R.id.send_sms);
		send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Message sendMsg = new Message(text.getText().toString(), friendObj.getFanID(), 0);
				sendMessage(sendMsg);
			}
		}); 
		addFriend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UIUtils.showToast(getActivity(), "Add Friend not implemented yet");
			}
		});
		adapter = new ChatViewAdapter(getActivity(), messages);
		messageListView.setAdapter(adapter);
	}

	/** Add LoadMessages listeners onResume **/
	@Override
	public void onResume() {
		ChatManager.noMsg = true;
		ChatManager.getInstance().addListener(this);
		showLoadingDialog();
		List<Message> msgList =  ChatManager.getInstance().getMessages(friendObj.getFanID());
		if(msgList!= null && msgList.size()>0){
			hideLoadingDialog();
			messages.clear();
			messages.addAll(msgList);
			adapter.notifyDataSetChanged();
			messageListView.setSelection(messages.size() - 1);
		}else{
			hideLoadingDialog();
		}
		super.onResume();
	}

	/** Remove LoadMessages listeners onPause **/
	@Override
	public void onPause() {
		ChatManager.getInstance().removeListener(this);
		super.onPause();
	}
	
	@Override
	public void onSuccess() {
		ChatManager.noMsg = false;
		hideLoadingDialog();
		List<Message> msgList = ChatManager.getInstance().getMessages(friendObj.getFanID());
		if(msgList != null && msgList.size() >0){
			messages.clear();
			messages.addAll(msgList); 
			adapter.notifyDataSetChanged();
			messageListView.setSelection(messages.size() - 1);
		}
	}

	@Override
	public void onException(AppException ex) { 
		hideLoadingDialog();
		send.setEnabled(true);
		if(ex.getErrorCode() == AppException.WB_VALIDATION_RULE_EXCEPTION){
			sendTwoMsgTo.setText(getResources().getString(R.string.InitialMessagesBetweenFans_initial)+" "+friendObj.getFanName());
			sendMsgValidationRuleFrame.setVisibility(View.VISIBLE);
		}
		UIUtils.showToast(getActivity(), ex.getMessage());
	}
	
	public void sendMessage(Message msg) { 
		if(ValidatorUtils.isRequired(msg.getText()))
			UIUtils.showToast(getActivity(), "Message is empty");
		else if(!NetworkingUtils.isNetworkConnected(getActivity())){
			UIUtils.showToast(getActivity(), "no internet connection please check your connection and try again");
		}else{
			send.setEnabled(false);
			ChatManager.getInstance().callSendMessage(msg, friendObj);
		}
	}

	void addNewMessage(Message m) { 
  	    long now = System.currentTimeMillis();
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(now);        
		m.setMessageTime(calendar.getTimeInMillis());
		messages.add(m);
		adapter.notifyDataSetChanged();
		messageListView.setSelection(messages.size() - 1);
	}
	
	@Override
	public void onSuccess(Integer storedMessageID) { 
		addNewMessage(new Message(text.getText().toString(),1));
		text.setText(""); 
		send.setEnabled(true);
//		UIUtils.showToast(getActivity(), "storedMessageID: "+ storedMessageID.toString());
	}
	
	/**  Initialize of PhotoList Dialog **/
	private void showEditMessageDialog(final Message msg, final View view) {

//				TextView deleteBtn = (TextView) popupView.findViewById(R.id.deleteBtn);
//				deleteBtn.setOnClickListener(new OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						deletedMsg = msg;
//						ChatManager.getInstance().deleteMessageAsync(msg.getMessageId());
//						popupWindow.dismiss();
//					}
//				});
	}

	@Override
	public void onDeleteSuccess() {
		// TODO Auto-generated method stub
		if(messages.contains(deletedMsg)){
			messages.remove(deletedMsg);
			adapter.notifyDataSetChanged();
		}
	}
}
