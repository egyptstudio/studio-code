package com.tawasol.barcelona.ui.fragments;
  
import com.tawasol.barcelona.R;    
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnPrivacySettingsListener;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.utils.UIUtils;
 
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle; 
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup; 
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener; 
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * @author TURKI
 *  
 */
public class PrivacySettingsFragment extends BaseFragment implements OnClickListener, OnPrivacySettingsListener{

	private  boolean PUSH_NOTIFICATION_SETTING = false;
	
	private ImageView requestArrow, personalArrow, contactArrow;
	private ToggleButton pushNotification;
	private String listValue;
	private TextView requestTxt, personalTxt, contactTxt;
	private String values[];
	
	private static final int REQUEST       = 1;
	private static final int PERSONAL_INFO = 2;
	private static final int CONTACT_INFO  = 3;
	private User userEntity;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_privacy_settings, container, false);
		
		/** Initialize privacy settings view **/
		init(rootView);

		/** set values of privacy settings view **/
		setValues();
		
		return rootView;
	}

	/** Handle OnClickListeners **/
	@Override
	public void onClick(View v) {
	 
		switch (v.getId()) {
		case R.id.request_arrow:
			showList(requestTxt, REQUEST);  
			break;
			
		case R.id.personal_arrow:
			showList(personalTxt, PERSONAL_INFO);  
			break;
			
		case R.id.contact_arrow:
			showList(contactTxt, CONTACT_INFO);  
			break;
		}
	}

	/** Initialize privacy settings view **/
	private void init(View rootView) {
		userEntity      = UserManager.getInstance().getCurrentUser();
		values          = getResources().getStringArray(R.array.settings_list_items);
		requestTxt      = (TextView) rootView.findViewById(R.id.request_list);
		personalTxt     = (TextView) rootView.findViewById(R.id.personal_list);
		contactTxt      = (TextView) rootView.findViewById(R.id.contact_list);
		
		requestArrow      = (ImageView) rootView.findViewById(R.id.request_arrow);
		personalArrow     = (ImageView) rootView.findViewById(R.id.personal_arrow);
		contactArrow      = (ImageView) rootView.findViewById(R.id.contact_arrow);
		
		requestArrow.setOnClickListener(this);
		personalArrow.setOnClickListener(this);
		contactArrow.setOnClickListener(this);
		
		pushNotification  = (ToggleButton) rootView.findViewById(R.id.push_toggle);
		pushNotification.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(pushNotification.isChecked())
					PUSH_NOTIFICATION_SETTING=true;
				else 
					PUSH_NOTIFICATION_SETTING=false; 
					userEntity.setReceivePushNotification(PUSH_NOTIFICATION_SETTING);
//					UserManager.getInstance().udateProfile(userEntity, false, false);
			}
		});
	}
	
	private void setValues(){
		User user = UserManager.getInstance().getCurrentUser();
		if(user.getReceiveFriendRequest() == 0)
			requestTxt.setText(values[2]);
		else
			requestTxt.setText(values[user.getReceiveFriendRequest()-1]);
		
		if(user.getViewPersonalInfo() == 0)
			personalTxt.setText(values[1]);
		else
			personalTxt.setText(values[user.getViewPersonalInfo()-1]);
		
		if(user.getViewContactsInfo() == 0)
			contactTxt.setText(values[0]);
		else
			contactTxt.setText(values[user.getViewContactsInfo()-1]);
		
		pushNotification.setChecked(user.isReceivePushNotification());
		
	}
	
	/**  Initialize of list Dialog **/
	private String showList(final TextView textView, final int whichTab){
		
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    dialog.setContentView(R.layout.settings_list_dialog);

		// set dialog background transparent to use my own view instead
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	    
	    final ListView listView = (ListView) dialog.findViewById(R.id.settings_list);
	    
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,values){
	    	@Override
	    	public View getView(int position, View convertView, ViewGroup parent) {
	    		// TODO Auto-generated method stub
	    		View view = super.getView(position, convertView, parent);
	    		TextView textView = (TextView) view.findViewById(android.R.id.text1);
	    		textView.setGravity(Gravity.CENTER);
	    		textView.setTextSize(getActivity().getResources().getDimension(R.dimen.smallestTextSize));
	    		textView.setTextColor(getActivity().getResources().getColor(R.color.black));
	    		return view;
	    	}
	    };
	    
	    listView.setAdapter(adapter);
	    
	    listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
				listValue = listView.getItemAtPosition(position).toString();
				textView.setText(listValue);
				
				if(whichTab == REQUEST)
					userEntity.setReceiveFriendRequest(position+1);
				else if(whichTab == PERSONAL_INFO)
					userEntity.setViewPersonalInfo(position+1);
				else if(whichTab == CONTACT_INFO)
					userEntity.setViewContactsInfo(position+1);
				
	            dialog.dismiss();
			}
		});
	    dialog.setCancelable(true); 
	    dialog.show(); 
	    
	    return listValue;
	}

	@Override
	public void onSuccess() {
		UIUtils.showToast(getActivity(), "Privacy Settings changed successfully");
	}

	@Override
	public void onException(AppException ex) {
		UIUtils.showToast(getActivity(), ex.getMessage());
	}
	
	/** Add Privacy Settings listener onResume **/
	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	/** Remove Privacy Settings listener onPause **/
	@Override
	public void onPause() {
		UserManager.getInstance().callChangePrivacySettings(userEntity);
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}
}
