/**
 * 
 */
package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.customView.SideBar;
import com.tawasol.barcelona.entities.Country;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnCountriesRecieved;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.FilterActivity;
import com.tawasol.barcelona.utils.UIUtils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SectionIndexer;

/**
 * @author Basyouni
 *
 */
public class FilterFragment extends Fragment implements OnCountriesRecieved {
	SideBar indexBar;
	ListView list;
	ProgressBar bar;
	ArrayList<Integer> listToSend = new ArrayList<Integer>();

	public ArrayList<Integer> getList(){
		return listToSend;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.filter_fragment, container,
				false);
		list = (ListView) rootView.findViewById(R.id.filterListView);
		indexBar = (SideBar) rootView.findViewById(R.id.sideBar);
		indexBar.setEnabled(false);
		bar = (ProgressBar) rootView.findViewById(R.id.progressBar);
		WallManager.getInstance().getCountryList();
		bar.setVisibility(View.VISIBLE);

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		WallManager.getInstance().addListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		WallManager.getInstance().removeListener(this);
	}

	@Override
	public void onSuccess(List<Country> objs) {
		bar.setVisibility(View.GONE);
		FilterAdapter adapter = new FilterAdapter(getActivity(), objs);
		list.setAdapter(adapter);
		indexBar.setEnabled(true);
		indexBar.setListView(list, false);
	}

	@Override
	public void onException(AppException ex) {
		bar.setVisibility(View.GONE);
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	private class FilterAdapter extends BaseAdapter implements SectionIndexer {
		private List<Country> stringArray;
		private Activity context;
		// List<int> checkedPosition = new ArrayList<int>();
		private boolean[] checkBoxState;

		public FilterAdapter(Activity _context, List<Country> objs) {
			stringArray = objs;
			context = _context;
			checkBoxState = new boolean[objs.size()];
		}

		public int getCount() {
			return stringArray.size();
		}

		public Object getItem(int arg0) {
			return stringArray.get(arg0);
		}

		public long getItemId(int arg0) {
			return 0;
		}

		public View getView(final int position, View v, ViewGroup parent) {
			LayoutInflater inflate = ((Activity) context).getLayoutInflater();
			View view = v;
			if (view == null)
				view = inflate.inflate(R.layout.filter_row, parent, false);
			String label = stringArray.get(position).getCountryName();
			final CheckBox checkBox = (CheckBox) view
					.findViewById(R.id.country_check_box);
			checkBox.setText(label);
			checkBox.setChecked(checkBoxState[position]);
			checkBox.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (checkBox.isChecked()) {
						listToSend.add(Integer.valueOf(stringArray
								.get(position).getCountryId()));
						((FilterActivity) context).addtoCountryList(stringArray
								.get(position).getCountryId());
						checkBoxState[position] = true;
					} else {
						listToSend.remove(Integer.valueOf(stringArray
								.get(position).getCountryId()));
						((FilterActivity) context)
								.removeFromCountryList(stringArray
										.get(position).getCountryId());
						checkBoxState[position] = false;
					}
				}
			});
			return view;
		}

		public int getPositionForSection(int section) {
			if (section == 35) {
				return 0;
			}
			for (int i = 0; i < stringArray.size(); i++) {
				String l = stringArray.get(i).getCountryName();
				char firstChar = l.toUpperCase().charAt(0);
				if (firstChar == section) {
					return i;
				}
			}
			return -1;
		}

		public int getSectionForPosition(int arg0) {
			return 0;
		}

		public Object[] getSections() {
			return null;
		}
	}
}
