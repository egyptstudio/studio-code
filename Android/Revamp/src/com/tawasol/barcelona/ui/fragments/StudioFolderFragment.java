package com.tawasol.barcelona.ui.fragments;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.managers.UserManager;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;



public class StudioFolderFragment extends BaseFragment    {

	/*public static final String SELECTED_TAB_KEY ="Studio_Selected_Tab";
	public static final int PLAYERS_TAB = 1;
	public static final int TEAM_TAB = 2;
	public static final int PURCHASED_TAB = 3;*/
	
	private FragmentTabHost folderTabHost;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_studio_folder, container,
				false);
		folderTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);
		setUpTabHost();
		
		if(StudioManager.getInstance().getSeasonList() == null)
			StudioManager.getInstance().getSeasons();
		
		return rootView;
	}
	
	private void setUpTabHost()
	{
		folderTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
		
		Bundle data;
		
		// adding player tab
		data = new Bundle();
		//data.putInt(SELECTED_TAB_KEY, PLAYERS_TAB);
		folderTabHost.addTab(
				setIndicator(folderTabHost.newTabSpec(getActivity().getResources().getString(R.string.Studio_Players)), R.drawable.chat_left_tab),
				PlayerFragment.class, data);
		//adding Team tab
		folderTabHost.addTab(
				setIndicator(folderTabHost.newTabSpec(getActivity().getResources().getString(R.string.Studio_Team)), R.drawable.chat_center_tab),
				TeamFragment.class, data);
		
		// adding Purchased photos tab
		//boolean enabled =UserManager.getInstance().getCurrentUser().getUserId() == 0 ? false:true;
		folderTabHost.addTab(
				setIndicator(folderTabHost.newTabSpec(getActivity().getResources().getString(R.string.Studio_Purchased)), R.drawable.chat_right_tab),
				StudioPhotosFragment.class, data);
//		if(!enabled){
//			folderTabHost.getTabWidget().getChildTabViewAt(2).setEnabled(false);
//		}
		
		folderTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				setTabColors();
			}
		});
	}
	

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid) {
		View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_host_text, null);
		v.setBackgroundResource(resid);
		
		final TextView text = (TextView) v.findViewById(R.id.tab_text);
		text.setText(spec.getTag());
		
		
		text.setTextColor(getActivity().getResources().getColor(
				android.R.color.white));
		
		
		return spec.setIndicator(v);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		setTabColors();
	}

private void setTabColors()
{
	for (int i = 0; i < folderTabHost.getTabWidget().getChildCount(); i++) {
		TextView tv = (TextView) folderTabHost.getTabWidget().getChildAt(i)
				.findViewById(R.id.tab_text);
		tv.setTextColor(getActivity().getResources().getColor(
				R.color.new_yellow));
	}
	TextView tv = (TextView) folderTabHost.getTabWidget()
			.getChildAt(folderTabHost.getCurrentTab())
			.findViewById(R.id.tab_text);
	tv.setTextColor(Color.WHITE);
}

}
