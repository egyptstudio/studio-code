package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.JSONObject;

import com.android.vending.billing.util.BarcaIabHelper;
import com.android.vending.billing.util.BarcaIabHelper.OnIabPurchaseFinishedListener;
import com.android.vending.billing.util.BarcaIabHelper.OnIabSetupFinishedListener;
import com.android.vending.billing.util.BarcaIabResult;
import com.android.vending.billing.util.BarcaPurchase;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.SharedPrefsHelper;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.InAppPointsEntity;
import com.tawasol.barcelona.entities.PremiumStateObject;
import com.tawasol.barcelona.entities.ShopEntity;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnPremiumStateRecieved;
import com.tawasol.barcelona.managers.ShopManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.ShopActivityTabs;
import com.tawasol.barcelona.utils.NetworkingUtils;
import com.tawasol.barcelona.utils.UIUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PremiumPackageFragment extends Fragment implements
		OnIabSetupFinishedListener, OnIabPurchaseFinishedListener,
		OnPremiumStateRecieved {

	private BarcaIabHelper billingHelper;
	private static final String PURCHASE_PAYLOAD_CACHE_KEY = "com.tawasol.Barchelona.PAYLOAD2";
	public static final String RESPONSE_PRODUCT_ID = "productId";
	public static final String RESPONSE_PAYLOAD = "developerPayload";
	public static boolean shopAvailable;
	public static final int loginRequestCode = 1549;

	GridView premiumGrid;
	private ProgressBar pointsProgress;
	private TextView offline;
	private Handler customHandler;
	private PremiumAdapter adapter;
	ProgressDialog dialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.premium_package_grid,
				container, false);
		premiumGrid = (GridView) rootView.findViewById(R.id.premium_grid);
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			billingHelper = new BarcaIabHelper(getActivity(),
					Params.Common.BASE_64_KEY);
			billingHelper.startSetup(this);
			pointsProgress = (ProgressBar) rootView
					.findViewById(R.id.pointsPackagesProgress);
			offline = (TextView) rootView.findViewById(R.id.offline);
			dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
			dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			customHandler = new Handler();
			customHandler.postDelayed(checkData, 0);
		} else {
			offline.setVisibility(View.VISIBLE);
		}
		return rootView;
	}

	private Runnable checkData = new Runnable() {

		@Override
		public void run() {
			if (ShopManager.getInstance().isDataAvailableException()) {
				pointsProgress.setVisibility(View.GONE);
				UIUtils.showToast(getActivity(), ShopManager.getInstance()
						.dataRetrievalException());
			} else {
				if (!ShopManager.getInstance().isDataAvailable()) {
					pointsProgress.setVisibility(View.VISIBLE);
					customHandler.postDelayed(this, 1000);
				} else {
					setAdapter();
				}
			}
		}
	};

	protected void setAdapter() {
		List<InAppPointsEntity> premium = ShopManager.getInstance()
				.getPremium();
		if (premium != null) {
			adapter = new PremiumAdapter(premium);
			premiumGrid.setAdapter(adapter);
		}
	}

	private class PremiumAdapter extends BaseAdapter {
		List<InAppPointsEntity> premium;

		public PremiumAdapter(List<InAppPointsEntity> premium) {
			this.premium = premium;
		}

		List<InAppPointsEntity> getList() {
			return this.premium;
		}

		@Override
		public int getCount() {
			return premium.size();
		}

		@Override
		public Object getItem(int position) {
			return premium.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View rootView = convertView;
			if (rootView == null) {
				rootView = LayoutInflater.from(
						App.getInstance().getApplicationContext()).inflate(
						R.layout.premium_package_item, parent, false);
			}
			final InAppPointsEntity premium = this.premium.get(position);
			RelativeLayout PackageContainer = (RelativeLayout) rootView
					.findViewById(R.id.shop_item_container);
			TextView title = (TextView) rootView.findViewById(R.id.title);
			TextView price = (TextView) rootView.findViewById(R.id.price);
			TextView packageDuration = (TextView) rootView
					.findViewById(R.id.packageDuration);
			TextView packageDescription = (TextView) rootView
					.findViewById(R.id.packageDescription);

			if (position == 0) {
				PackageContainer
						.setBackgroundResource(R.drawable.full_basic_pk);
				title.setText(getActivity().getResources().getString(
						R.string.basic));
			} else if (position == 1) {
				PackageContainer
						.setBackgroundResource(R.drawable.full_bronze_pk);
				title.setText(getActivity().getResources().getString(
						R.string.bronze));
			} else if (position == 2) {
				PackageContainer
						.setBackgroundResource(R.drawable.full_silver_pk);
				title.setText(getActivity().getResources().getString(
						R.string.silver));
			} else if (position == 3) {
				PackageContainer
						.setBackgroundResource(R.drawable.full_golden_pk);
				title.setText(getActivity().getResources().getString(
						R.string.gold));
			}
			if (premium.getTitle() != null)
				packageDuration.setText(premium.getTitle());
			if (premium.getDescription() != null)
				packageDescription.setText(premium.getDescription());
			if (premium.getPrice() != 0)
				price.setText(premium.getPrice() + " $");

			rootView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						if (shopAvailable) {
							dialog.show();
							ShopManager.getInstance().MyPremiumStatus(
									UserManager.getInstance()
											.getCurrentUserId(),
									premium.getInAppId());

						} else {
							UIUtils.showToast(getActivity(),
									"SomeThing went wrong");
						}
					} else {
						getActivity().startActivityForResult(
								LogInActivity.getActivityIntent(getActivity(),
										UserManager.LOGIN_FOR_RESULT,
										new Intent()),
								loginRequestCode);
					}
				}
			});

			return rootView;
		}

	}

	@Override
	public void onIabSetupFinished(BarcaIabResult result) {
		if (result.isSuccess())
			shopAvailable = true;
	}

	@Override
	public void onIabPurchaseFinished(BarcaIabResult result, BarcaPurchase info) {
		if (result.isFailure()) {
			// dealWithPurchaseFailed(result);
		} else if (result.isSuccess()) {
			dealWithPurchaseSuccess(info);
		}
	}

	private void dealWithPurchaseSuccess(BarcaPurchase info) {
		if (adapter != null) {
			if (adapter.getList() != null) {
				InAppPointsEntity point = adapter.getList().get(
						adapter.getList().indexOf(
								new InAppPointsEntity(info.getSku(), 0)));
				if (point != null) {
					int subscriptionDuration = 0;
					if (point.getTitle()
							.equalsIgnoreCase(ShopManager.ONE_MONTH))
						subscriptionDuration = ShopManager.ONE_MONTH_SUBSCRIPTION;
					else if (point.getTitle().equalsIgnoreCase(
							ShopManager.THREE_MONTHS))
						subscriptionDuration = ShopManager.THREE_MONTHS_SUBSCRIPTION;
					else if (point.getTitle().equalsIgnoreCase(
							ShopManager.SIX_MONTHS))
						subscriptionDuration = ShopManager.SIX_MONTHS_SUBSCRIPTION;
					else if (point.getTitle().equalsIgnoreCase(
							ShopManager.ONE_YEAR))
						subscriptionDuration = ShopManager.ONE_YEAR_SUBSCRIPTION;
					ShopManager.getInstance().subscripeInPremium(
							UserManager.getInstance().getCurrentUserId(),
							subscriptionDuration);
				}
				ShopManager.getInstance().addPointsForUser(
						UserManager.getInstance().getCurrentUserId(),
						Integer.parseInt(point.getTitle()), info,
						billingHelper, null, true);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		ShopManager.getInstance().addListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		ShopManager.getInstance().removeListener(this);
	}

	@Override
	public void onException(AppException ex) {
		dialog.dismiss();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onPremiumStateRecieved(PremiumStateObject premium,
			String productID) {
		dialog.dismiss();
		if (premium.isPremium())
			UIUtils.showToast(getActivity(), getActivity().getResources()
					.getString(R.string.valid_sub) + " " + premium.getEndDate());
		else {
			String purchasePayload = BarcaIabHelper.ITEM_TYPE_INAPP + ":"
					+ UUID.randomUUID().toString();
			savePurchasePayload(purchasePayload);
			if (billingHelper != null)
				billingHelper.flagEndAsync();
			billingHelper.launchSubscriptionPurchaseFlow(getActivity(),
					productID, ShopActivityTabs.PREMIUM_REQUEST_CODE,
					PremiumPackageFragment.this, purchasePayload);
		}
	}

	protected void savePurchasePayload(String purchasePayload) {
		SharedPrefsHelper.setRegiterKey(PURCHASE_PAYLOAD_CACHE_KEY,
				purchasePayload);
	}

	private String getPurchasePayload() {
		return SharedPrefsHelper.getRegisterKey(PURCHASE_PAYLOAD_CACHE_KEY);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		int responseCode = data.getIntExtra(BarcaIabHelper.RESPONSE_CODE,
				BarcaIabHelper.BILLING_RESPONSE_RESULT_OK);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK
				&& responseCode == BarcaIabHelper.BILLING_RESPONSE_RESULT_OK) {
			if (requestCode == ShopActivityTabs.PREMIUM_REQUEST_CODE) {
				try {
					String purchaseData = data
							.getStringExtra(BarcaIabHelper.RESPONSE_INAPP_PURCHASE_DATA);
					JSONObject purchase = new JSONObject(purchaseData);
					String developerPayload = purchase
							.getString(RESPONSE_PAYLOAD);
					if (developerPayload == null)
						developerPayload = "";
					if (getPurchasePayload().equals(developerPayload)) {
						billingHelper.handleActivityResult(requestCode,
								resultCode, data);
					}
				} catch (Exception e) {

				}
			}
		} else if (resultCode == Activity.RESULT_OK
				&& requestCode == FansListFragment.INTENT_RESULT_CODE) {
			((BaseActivity) getActivity()).refreshMenu();
		}
	}

}
