package com.tawasol.barcelona.ui.fragments;
 

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.customView.PostViewFactory;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.entities.WALLPosts;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.PostFullScreenListener;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.ImageDetailsActivity;
import com.tawasol.barcelona.ui.activities.PostFullScreenDetailsActivity;
import com.tawasol.barcelona.ui.custom.pager.CustomViewPager;
import com.tawasol.barcelona.ui.custom.pager.OverscrollContainer.OnOverScrolledListener;
import com.tawasol.barcelona.ui.custom.pager.OverscrollViewPager;
import com.tawasol.barcelona.utils.UIUtils;
import com.tawasol.barcelona.utils.Utils;

/**
 * Vertical pager page item that shows full screen image details.
 * 
 * @author Morabea
 */
public class VerticalPageFragment extends BaseFragment implements
		PostFullScreenListener {

	public static final String POST_OBJ_BUNDLE_KEY = "com.tawasol.barcelona.ui.fragments.fan_bundle_key";
	public static final String CURRENT_UPDATE_TIME = "com.tawasol.barcelona.ui.fragments.current_update_time";
	public static final String CURRENT_TAB = "com.tawasol.barcelona.ui.fragments.current_tap";

	private static final int REQUEST_TYPE_OLDER = 1;
	private static final int REQUEST_TYPE_NEWER = 2;
	private ProgressDialog dialog;

	interface UpdatableAdapter {
		void update(View convertView, int position);
	}

	private PostViewModel mPost;
	private int currentTab;

	private CustomPagerAdapter mAdapter;

	private long currentUpdateTime;
	private long lastRequestTime = 0;
	private List<PostViewModel> posts;
	OverscrollViewPager pager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mPost = (PostViewModel) getArguments().getSerializable(POST_OBJ_BUNDLE_KEY);
		currentTab = getArguments().getInt(CURRENT_TAB);
		currentUpdateTime = getArguments().getLong(CURRENT_UPDATE_TIME);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_vertical_page_item, container, false);

		populateScreen(rootView);

		return rootView;
	}

	private void populateScreen(View rootView) {
		pager = (OverscrollViewPager) rootView.findViewById(R.id.vertical_pager_item);
		// int index = 0;
		// if (mAdapter == null) {
		if (mPost.getUserId() != 0) {
//			List<PostViewModel> posts = WallManager.getInstance().getPostsForFan(mPost.getUserId());
//			if(posts.get(0).equals(WallManager.EMPTY_POST))
//				posts.remove(0);
//			if(posts.get(posts.size() - 1).equals(WallManager.EMPTY_POST))
//				posts.remove(posts.size() - 1);


			Collections.sort(posts);
//			posts.add(0, WallManager.getInstance().getEmptyPost());
//			posts.add(WallManager.getInstance().getEmptyPost());

			mAdapter = new CustomPagerAdapter(posts);
			final int index = posts.indexOf(mPost);
			// }
			pager.getOverscrollView().setAdapter(mAdapter);
//			pager.setPagingEnabled(Utils.isPortrait());
			// pager.setCurrentItem(index);
			// pager.postDelayed(new Runnable() {
			// @Override
			// public void run() {
			// pager.setCurrentItem(index);
			// }
			// }, 0);
			pager.post(new Runnable() {

				@Override
				public void run() {
					pager.getOverscrollView().setCurrentItem(index);
				}
			});
		}
	}

	private class CustomPagerAdapter extends PagerAdapter implements UpdatableAdapter {

		private DisplayImageOptions displayOptions;

		@SuppressWarnings("deprecation")
		public CustomPagerAdapter(List<PostViewModel> posts) {
			VerticalPageFragment.this.posts = posts;
			// this.posts.add(0, WallManager.getInstance().getEmptyPost());
			// this.posts.add(WallManager.getInstance().getEmptyPost());
			displayOptions = new DisplayImageOptions.Builder()
					.showImageForEmptyUri(R.drawable.ic_launcher)
					.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
					.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
					.bitmapConfig(Bitmap.Config.RGB_565)
					.considerExifParams(true)
					.displayer(new RoundedBitmapDisplayer(1000)).build();
		}

		public void addPosts(List<PostViewModel> newPosts) {
			if (newPosts.size() > 0) {
				Collections.sort(newPosts);

				PostViewModel firstNewPost = newPosts.get(0);
//				PostViewModel lastPost = posts.get(posts.size() - 2);
				PostViewModel lastPost = posts.get(posts.size()-1);
				
				if (firstNewPost.compareTo(lastPost) < -1) {

//					posts.remove(0);
					posts.addAll(0, newPosts);
//					posts.add(0, WallManager.getInstance().getEmptyPost());
					notifyDataSetChanged();
					pager.getOverscrollView().setCurrentItem(1);
				} else {
					posts.remove(posts.size() - 1);
					posts.addAll(newPosts);
//					posts.add(WallManager.getInstance().getEmptyPost());

				}

				notifyDataSetChanged();
			} else
				System.out.println("No More Posts");
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View convertView = LayoutInflater.from(container.getContext()).inflate(R.layout.photo_details_pager_item, 
					container, false);

			convertView.setTag(position);

			updateView(convertView, position);

			container.addView(convertView);

			return convertView;
		}

		private void updateView(final View convertView, final int position) {
			final PostViewModel post = posts.get(position);

			ImageView img = (ImageView) convertView.findViewById(R.id.photo_details_img);
			ImageView usrImg = (ImageView) convertView.findViewById(R.id.photo_details_usr_photo);
			TextView usrName = (TextView) convertView.findViewById(R.id.photo_details_usr_name);
			ImageView closeImg = (ImageView) convertView.findViewById(R.id.photo_details_close);
			ImageView infoImg = (ImageView) convertView.findViewById(R.id.photo_details_info);

			final TextView likesTxt = (TextView) convertView.findViewById(R.id.photo_details_likes_txt);

			final View contentLayout = convertView.findViewById(R.id.photo_details_content);
			final View progressView = convertView.findViewById(R.id.progress);
			contentLayout.setVisibility(View.VISIBLE);
			progressView.setVisibility(View.GONE);
			pager.setOnOverScrollListener(new OnOverScrolledListener() {
				@Override
				public void onOverScroll(int overScrollType) {
					getArguments().putSerializable(POST_OBJ_BUNDLE_KEY,posts.get(pager.getOverscrollView().getCurrentItem()));
					getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
					contentLayout.setVisibility(View.GONE);
					progressView.setVisibility(View.VISIBLE);
					// TODO: Request more posts

					dialog = new ProgressDialog(getActivity());
					dialog.setCancelable(false);
				
					
					if (OVER_SCROLL_LEFT == overScrollType) {
						dialog.setMessage("Left");
						new LoadMoreTask(CustomPagerAdapter.this, convertView,
								pager.getOverscrollView().getCurrentItem()).
								execute(REQUEST_TYPE_OLDER, mPost.getUserId());
						
					} else if (OVER_SCROLL_RIGHT == overScrollType) {
						dialog.setMessage("Right");
						new LoadMoreTask(CustomPagerAdapter.this, convertView,
								pager.getOverscrollView().getCurrentItem()).
								execute(REQUEST_TYPE_NEWER, mPost.getUserId());
					}
					dialog.show();
				}
			});
//			pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//				@Override
//				public void onPageSelected(int arg0) {
//					getArguments().putSerializable(POST_OBJ_BUNDLE_KEY,
//							posts.get(pager.getCurrentItem()));
//
//					if (WallManager.isEmptyPost(posts.get(pager
//							.getCurrentItem()))) {
//						getActivity().setRequestedOrientation(
//								ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//						contentLayout.setVisibility(View.GONE);
//						progressView.setVisibility(View.VISIBLE);
//						// TODO: Request more posts
//						new LoadMoreTask(CustomPagerAdapter.this, convertView,
//								pager.getCurrentItem()).execute(pager
//								.getCurrentItem() == 0 ? REQUEST_TYPE_OLDER
//								: REQUEST_TYPE_NEWER, mPost.getUserId());
//						return;
//					} else
//						getActivity().setRequestedOrientation(
//								ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//				}
//
//				@Override
//				public void onPageScrolled(int arg0, float arg1, int arg2) {
//					// TODO Auto-generated method stub
//				}
//
//				@Override
//				public void onPageScrollStateChanged(int arg0) {
//					// TODO Auto-generated method stub
//				}
//			});

			// Post image
			if (!TextUtils.isEmpty(post.getPostPhotoUrl())) {
				App.getInstance().getImageLoader().displayImage(post.getPostPhotoUrl(), 
						img,App.getInstance().getDisplayOption(),
								new SimpleImageLoadingListener() {
									@Override
									public void onLoadingCancelled(String imageUri, View view) {
										progressView.setVisibility(View.GONE);
										super.onLoadingCancelled(imageUri, view);
									}

									@Override
									public void onLoadingComplete(String imageUri, View view,Bitmap loadedImage) {
										progressView.setVisibility(View.GONE);
										super.onLoadingComplete(imageUri, view,	loadedImage);
									}

									@Override
									public void onLoadingFailed(String imageUri, View view,	FailReason failReason) {
										progressView.setVisibility(View.GONE);
										super.onLoadingFailed(imageUri, view,failReason);
									}

									@Override
									public void onLoadingStarted(String imageUri, View view) {
										progressView.setVisibility(View.VISIBLE);
										super.onLoadingStarted(imageUri, view);
									}

								});
			} else {
				// TODO set default image for photo post
			}

			// User profile image
			if (!TextUtils.isEmpty(post.getUserProfilePicUrl())) {
				App.getInstance().getImageLoader().displayImage(post.getUserProfilePicUrl(), usrImg, displayOptions);
			} else {
				// TODO set default image for profile
			}
			usrImg.setVisibility(Utils.isPortrait() ? View.VISIBLE : View.GONE);

			// User name
			usrName.setText(TextUtils.isEmpty(post.getUserName()) ? "" : post.getUserName());
			usrName.setVisibility(Utils.isPortrait() ? View.VISIBLE : View.GONE);

			// Likes
			likesTxt.setText(post.getLikeCount() <= 0 ? "" : String.valueOf(post.getLikeCount()));
			likesTxt.setCompoundDrawablesWithIntrinsicBounds(post.isLiked() ? 
					R.drawable.like_sc : R.drawable.like, 0,0, 0);
			likesTxt.setVisibility(Utils.isPortrait() ? View.VISIBLE: View.GONE);
			likesTxt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					handleLike(post.getPostId(), post, likesTxt, currentTab);
				}
			});

			// Close icon
			closeImg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					getActivity().finish();
				}
			});
			closeImg.setVisibility(Utils.isPortrait() ? View.VISIBLE : View.GONE);

			// Info icon
			infoImg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(ImageDetailsActivity.getActivityIntent(getActivity(), post, currentTab,
									getArguments().getInt(PostFullScreenDetailsActivity.CURRENT_INDEX_BUNDLE_KEY),true , 0 , 0));
				}
			});
			infoImg.setVisibility(Utils.isPortrait() ? View.VISIBLE : View.GONE);
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			View view = container.findViewWithTag(position);
			if (view != null)
				container.removeView(view);
		}

		@Override
		public int getCount() {
			return posts.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public void update(View convertView, int position) {
			updateView(convertView, position);
		}

	}

	private class LoadMoreTask extends AsyncTask<Integer, Void, List<PostViewModel>> {

		private WeakReference<UpdatableAdapter> adapterRef;
		private WeakReference<View> viewRef;
		private int position;

		public LoadMoreTask(UpdatableAdapter adapter, View convertView, int position) {
			adapterRef = new WeakReference<UpdatableAdapter>(adapter);
			viewRef = new WeakReference<View>(convertView);
			this.position = position;
		}

		@Override
		protected List<PostViewModel> doInBackground(Integer... params) {
			int type = params[0];
			int fanId = params[1];

			List<PostViewModel> posts = null;
			if (type == REQUEST_TYPE_NEWER) {
				WALLPosts wallpost = WallManager.getInstance().getFanNewerPosts(fanId, currentUpdateTime); // 1420116419

				if (wallpost != null && wallpost.getPosts() != null && wallpost.getPosts().size() > 0) {
					posts = wallpost.getPosts();
					currentUpdateTime = wallpost.getCurrentRequestTime();
				}

			} else if (type == REQUEST_TYPE_OLDER) {
				lastRequestTime = VerticalPageFragment.this.posts.get(0).getLastUpdateTime();
				WALLPosts wallpost = WallManager.getInstance().getFanOlderPosts(fanId, lastRequestTime);

				if (wallpost != null && wallpost.getPosts() != null
						&& wallpost.getPosts().size() > 0) {// 1420041994
															// 1420041994
					posts = wallpost.getPosts();
				}
			}
			return posts;
		}

		@Override
		protected void onPostExecute(List<PostViewModel> result) {
			dialog.dismiss();
			pager.findViewWithTag(pager.getOverscrollView().getCurrentItem()).findViewById(R.id.progress).setVisibility(View.GONE);

			UpdatableAdapter adapter = adapterRef.get();
			View convertView = viewRef.get();

			if (adapter == null || convertView == null || result == null
					|| result.size() < 1) {
				if (getActivity() != null){
//					UIUtils.showToast(getActivity(), "No more Posts");
				}
				return;
			}
			adapter.update(convertView, position);
			mAdapter.addPosts(result);

		}

	}

	public static VerticalPageFragment newInstance(PostViewModel post, long currentUpdateTime,
			int currentTab, int currentPosition) {
		VerticalPageFragment frag = new VerticalPageFragment();

		Bundle bundle = new Bundle();
		bundle.putSerializable(POST_OBJ_BUNDLE_KEY, post);
		bundle.putLong(CURRENT_UPDATE_TIME, currentUpdateTime);
		bundle.putInt(CURRENT_TAB, currentTab);
		bundle.putInt(PostFullScreenDetailsActivity.CURRENT_INDEX_BUNDLE_KEY,
				currentPosition);
		frag.setArguments(bundle);
		return frag;
	}

	@Override
	public void onException(AppException ex) {
		// TODO Auto-generated method stub
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void handle(List<Integer> actions, PostViewModel post) {
		View view = pager.findViewWithTag(pager.getOverscrollView().getCurrentItem());
		if (view != null && isFirstTime) {
			for (Integer action : actions) {
				if (action == PostViewFactory.ACTION_LIKe) {
					((TextView) view.findViewById(R.id.photo_details_likes_txt))
							.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_sc, 0, 0, 0);
					posts.get(pager.getOverscrollView().getCurrentItem()).setLiked(true);
					int count = post.getLikeCount();
					posts.get(pager.getOverscrollView().getCurrentItem()).setLikeCount(count);
					((TextView) view.findViewById(R.id.photo_details_likes_txt)).setText(String.valueOf(count));

					// posts.remove(position);
					// posts.add(position, post);

				} else if (action == PostViewFactory.ACTION_DISLIKe) {
					((TextView) view.findViewById(R.id.photo_details_likes_txt))
							.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like, 0, 0, 0);
					posts.get(pager.getOverscrollView().getCurrentItem()).setLiked(false);
					int count = post.getLikeCount();
					posts.get(pager.getOverscrollView().getCurrentItem()).setLikeCount(count);
					((TextView) view.findViewById(R.id.photo_details_likes_txt)).setText(count <= 0 ? "" : String.valueOf(count));

					// posts.remove(position);
					// posts.add(position, post);
				} else if (action == PostViewFactory.ACTION_FOLLOW) {
					posts.get(pager.getOverscrollView().getCurrentItem()).setFollowing(true);

				} else if (action == PostViewFactory.ACTION_REPOST) {
					posts.get(pager.getOverscrollView().getCurrentItem()).setRePosted(true);

				} else if (action == PostViewFactory.ACTION_COMMENT) {
					int commentCount = post.getCommentCount();
					posts.get(pager.getOverscrollView().getCurrentItem()).setCommentCount(commentCount);

				} else if (action == PostViewFactory.ACTION_DELETE) {
					// posts.remove(position);
				} else if (action == PostViewFactory.ACTION_FOLLOW_REPOSTER) {
					posts.get(pager.getOverscrollView().getCurrentItem()).setRePosterIsFollowing(true);
				}
			}
			isFirstTime = false;
			mAdapter.notifyDataSetChanged();
		}
		// mAdapter.notifyDataSetChanged();
	}

	boolean isFirstTime;

	@Override
	public void onResume() {
		isFirstTime = true;
		WallManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		WallManager.getInstance().removeListener(this);
	}

	private void handleLike(int postId, final PostViewModel post,
			final TextView likeButton, int tab) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (UserManager.getInstance().getCurrentUserId() != 0) {
					if (post.isLiked()) {

						likeButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like, 0, 0, 0);
						int count = post.getLikeCount();

						likeButton.setText(count <= 1 ? "" : String.valueOf(count - 1));
						WallManager.getInstance().handleLike(
								WallManager.DISLIKE_POST,
								UserManager.getInstance().getCurrentUserId(),
								post.getPostId());
						if (checkPost(currentTab, post.getPostId()) == null) {
							post.setLiked(false);
							post.setLikeCount(post.getLikeCount() - 1);
						}
					} else if (!post.isLiked()) {
						likeButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_sc, 0, 0, 0);
						likeButton.setText(String.valueOf(post.getLikeCount() + 1));

						WallManager.getInstance().handleLike(
								WallManager.LIKE_POST,
								UserManager.getInstance().getCurrentUserId(),
								post.getPostId()); // post.setLiked(true);
						// pager.invalidate();
						if (checkPost(currentTab, post.getPostId()) == null) {
							post.setLiked(true);
							post.setLikeCount(post.getLikeCount() + 1);
						}
					}
				} else {
					if (getActivity() != null)
						UIUtils.showToast(getActivity(), "You must be LoggedIn");
				}
			}
		});
	}

	public PostViewModel checkPost(int currentTab, int postID) {
		PostViewModel checkPost = null;
		if (currentTab == FragmentInfo.LATEST_TAB)
			checkPost = WallManager.getInstance().getLatestPostForId(postID);
		else if (currentTab == FragmentInfo.TOP_TEN)
			checkPost = WallManager.getInstance().getTopTenPostForId(postID);
		else if (currentTab == FragmentInfo.WALL)
			checkPost = WallManager.getInstance().getWallPostForId(postID);
		else if (currentTab == FragmentInfo.MY_PIC)
			checkPost = WallManager.getInstance().getMyPicsPostForId(postID);
		else if (currentTab == FragmentInfo.Filtered)
			checkPost = WallManager.getInstance().getFilterPostForId(postID);
		return checkPost;
	}
}
