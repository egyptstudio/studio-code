package com.tawasol.barcelona.ui.fragments;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.NotificationsTable;
import com.tawasol.barcelona.entities.Notification;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;



public class NotificationsFragment extends BaseFragment    {
	
	public static final String NOTIFCATION_TYPE="NotificationViewType";
	public static final int LIKES = 100;
	public static final int COMMENTS=101;
	public static final int MESSAGES=102;
	public static final int REQUESTS=103;
	public static final int FOLLOWING=104;
	public static final int FAVORITES=105;
	
	static int LikesCount,CommentsCount,RequestsCount,FollowingsCount,FavouritesCount;
	
	private static FragmentTabHost notificationsTabHost;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_notifications, container,
				false);
		notificationsTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);
		setUpTabHost();
		
		return rootView;
	}
	
	private static void getNotificationsCount()
	{
		 LikesCount  = NotificationsTable.getInstance().getFilteredUnreadNotificationsCount(Notification.LIKE_NOTIFICATIONS);
		 CommentsCount  = NotificationsTable.getInstance().getFilteredUnreadNotificationsCount(Notification.COMMENT_NOTIFICATIONS);
		 RequestsCount  = NotificationsTable.getInstance().getFilteredUnreadNotificationsCount(Notification.FRIEND_REQUEST_NOTIFICATIONS);
		 FollowingsCount  = NotificationsTable.getInstance().getFilteredUnreadNotificationsCount(Notification.FOLLOWING_NOTIFICATIONS);
		 FavouritesCount  = NotificationsTable.getInstance().getFavouritesUnreadCount();
	}
	private void setUpTabHost()
	{
		getNotificationsCount();
		
		notificationsTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
		
		Bundle data ;
		
		// adding likes tab
		
		data = new Bundle();
		data.putInt(NOTIFCATION_TYPE, LIKES);
		notificationsTabHost.addTab(
				setIndicator(notificationsTabHost.newTabSpec(getActivity().getResources().getString(R.string.notifications_likes)), R.drawable.notifications_likes_tab,LikesCount),
				NotificationsListFragment.class, data);
		
		// adding comments tab

		data = new Bundle();
		data.putInt(NOTIFCATION_TYPE, COMMENTS);
				notificationsTabHost.addTab(
						setIndicator(notificationsTabHost.newTabSpec(getActivity().getResources().getString(R.string.notifications_comments)), R.drawable.notifications_comments_tab,CommentsCount),
						NotificationsListFragment.class, data);
				
				// adding messages tab

			/*	data = new Bundle();
				data.putInt(NOTIFCATION_TYPE, MESSAGES);
				notificationsTabHost.addTab(
						setIndicator(notificationsTabHost.newTabSpec(getActivity().getResources().getString(R.string.notifications_messages)), R.drawable.notifications_messages_tab),
						NotificationsMessagesFragment.class, data);*/
				
				// adding requests tab

				/*data = new Bundle();
				data.putInt(NOTIFCATION_TYPE, REQUESTS);*/
				notificationsTabHost.addTab(
						setIndicator(notificationsTabHost.newTabSpec(getActivity().getResources().getString(R.string.notifications_requests)), R.drawable.notifications_requests_tab,RequestsCount),
						NotificationsRequestsFragment.class, null);
				
				// adding following tab

				data = new Bundle();
				data.putInt(NOTIFCATION_TYPE, FOLLOWING);
				notificationsTabHost.addTab(
						setIndicator(notificationsTabHost.newTabSpec(getActivity().getResources().getString(R.string.notifications_likes_Following)), R.drawable.notifications_following_tab,FollowingsCount),
						NotificationsListFragment.class, data);
				
				// adding Favorite tab

				data = new Bundle();
				data.putInt(NOTIFCATION_TYPE,FAVORITES);
				notificationsTabHost.addTab(
						setIndicator(notificationsTabHost.newTabSpec(getActivity().getResources().getString(R.string.FanProfileLevel2_Favorate)), R.drawable.notifications_favorite_tab,FavouritesCount),
						NotificationsListFragment.class, data);
				
				notificationsTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

					@Override
					public void onTabChanged(String tabId) {
						updateTab();
					}
				});
		
	}
	

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid,int notificationCount) {
		View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_host_notifications, null);
		ImageView tabImage = (ImageView)v.findViewById(R.id.tab_image);
		TextView tabText = (TextView) v.findViewById(R.id.tab_text);
		TextView tabNumberText = (TextView)v.findViewById(R.id.tab_notification_number);
		
		tabImage.setImageResource(resid);
		tabText.setText(spec.getTag());
		
		if(notificationCount >0)
		{
			tabNumberText.setVisibility(View.VISIBLE);
			tabNumberText.setText(String.valueOf(notificationCount));
		}
		else
			tabNumberText.setVisibility(View.GONE);
		
		tabText.setTextColor(getActivity().getResources().getColor(
				R.color.yellow));
		
		
		
		return spec.setIndicator(v);
	}
	
	public static void updateTab()
	{
		getNotificationsCount();
		for (int i = 0; i < notificationsTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) notificationsTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.tab_text);
			tv.setTextColor(App.getInstance().getApplicationContext().getResources().getColor(
					R.color.white));
					
		}
		TextView tv = (TextView) notificationsTabHost.getTabWidget()
				.getChildAt(notificationsTabHost.getCurrentTab())
				.findViewById(R.id.tab_text);
		tv.setTextColor(App.getInstance().getApplicationContext().getResources().getColor(
				R.color.new_yellow));
		for(int i =0 ;i<notificationsTabHost.getTabWidget().getChildCount();i++)
		{
			TextView numberTV = (TextView) notificationsTabHost.getTabWidget()
					.getChildAt(i)
					.findViewById(R.id.tab_notification_number);
					switch (i) {
					case 0: // likes 
						if(LikesCount > 0)
							numberTV.setText(String.valueOf(LikesCount));
						else
							numberTV.setVisibility(View.GONE);
						break;
						
					case 1: // comments 
						if(CommentsCount > 0)
							numberTV.setText(String.valueOf(CommentsCount));
						else
							numberTV.setVisibility(View.GONE);
						break;
						
					case 2: // requests 
						if(RequestsCount > 0)
							numberTV.setText(String.valueOf(RequestsCount));
						else
							numberTV.setVisibility(View.GONE);
						break;
						
					case 3: // following 
						if(FollowingsCount > 0)
							numberTV.setText(String.valueOf(FollowingsCount));
						else
							numberTV.setVisibility(View.GONE);
						break;
						
					case 4: // favorites 
						if(FavouritesCount > 0)
							numberTV.setText(String.valueOf(FavouritesCount));
						else
							numberTV.setVisibility(View.GONE);
						break;

					default:
						break;
					}

		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		updateTab();
	}
	

	

}
