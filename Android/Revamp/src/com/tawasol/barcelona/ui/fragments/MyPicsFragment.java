package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import com.tawasol.barcelona.R; 
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnPostsRecieved;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.Edit_UserProfile_Activity;
import com.tawasol.barcelona.ui.adapters.MyPicsAdapter;
import com.tawasol.barcelona.utils.UIUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;


public class MyPicsFragment extends BaseFragment  {
	ArrayList<PostViewModel> myPics;
	MyPicsAdapter adapter;
	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_my_pics, container, false);
		
		
		myPics =(ArrayList<PostViewModel>) getActivity().getIntent().getExtras().getSerializable(Edit_UserProfile_Activity.MY_PICS_BUNLE_OBJ);
		
		GridView gridview = (GridView) rootView.findViewById(R.id.my_pics_grid);
		 adapter = new MyPicsAdapter(getActivity(),myPics);
	    gridview.setAdapter(adapter);
	    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	Intent intent = new Intent();
            	intent.putExtra(Edit_UserProfile_Activity.MY_PICS_BUNLE_OBJ, myPics.get(position));
				getActivity().setResult(Activity.RESULT_OK, intent);
				getActivity().finish();
            }
        });
		return rootView;
	}

	public static Fragment newFragment() {
		return new MyPicsFragment();
	}	

	
	
	
}
