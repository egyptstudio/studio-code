package com.tawasol.barcelona.ui.fragments;
 
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.managers.ChatManager;
import com.tawasol.barcelona.managers.UserManager;

/**
 * 
 * @author Turki
 *
 */
public class ChatInviteFragment extends BaseFragment {

	private EditText invitationEt; 
	private TextView inviteBtn;
	private static String appURL;
	private String invtationMsg;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView      = inflater.inflate(R.layout.fragment_chat_invite, container, false);

		/** Initialize registration view **/
		init(rootView);
		
		return rootView;
	}
	
	
	private void init(View rootView){
		invitationEt       = (EditText) rootView.findViewById(R.id.user_message_txt);
		inviteBtn 		   = (TextView) getActivity().findViewById(R.id.rightBtn);
		inviteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				invtationMsg       = invitationEt.getText().toString();
				
				// first call WB method of send invitation text
				ChatManager.getInstance().InviteFriends(invtationMsg);
				// then open send intent action 
				//final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
				/*try {
					appURL = "market://details?id=" + appPackageName; 
				} catch (android.content.ActivityNotFoundException anfe) {
					appURL = "http://play.google.com/store/apps/details?id=" + appPackageName;
				}*/
				appURL = "http://fcbstudio.mobi/invite.php?userId="+UserManager.getInstance().getCurrentUserId();
				
				Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getActivity().getResources().getString(R.string.smsInvite_Subject));
				shareIntent.putExtra(Intent.EXTRA_TEXT,invtationMsg  +"\n"+ appURL);
				startActivity(shareIntent);
			}
		});
	}
}
