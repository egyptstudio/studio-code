package com.tawasol.barcelona.ui.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.Seasons;
import com.tawasol.barcelona.entities.StudioFolder;
import com.tawasol.barcelona.entities.StudioPhoto;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnStudioPhotosReceivedListener;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.CaptureActivity;
import com.tawasol.barcelona.ui.activities.StudioFilterResultActivity;
import com.tawasol.barcelona.ui.adapters.StudioPhotosAdapter;
import com.tawasol.barcelona.utils.UIUtils;

public class StudioFilterResultFragment extends BaseFragment implements 
OnStudioPhotosReceivedListener , OnScrollListener, OnItemClickListener{

	TextView noOfPhotos;
	GridView photosResultGV;
	TextView noResultsTxt;
	ImageView noResultsImg;
	private static int pageNo = 1;
	private boolean gettinMoreData = true;
	int filterBy;
	ArrayList<Seasons> seasons;
	
	List<StudioPhoto> photos;
	StudioPhotosAdapter adapter;
	
	Dialog dialog;
	ViewPager photosPager;
	PhotoPagerAdapter photospagerAdapter;
	ImageView imageView;
	
	private boolean userScrolled;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_studio_filter_result,
				container, false);
		pageNo = 1;
		
		/* get view controls */
		viewByID(rootView);
		
		getFilterData();
		
		initViewControls();
		
		/* call Filter WB*/
		StudioManager.getInstance().getStudioPhotos(0, pageNo, filterBy, seasons);
		
		return rootView;
	}
	
	private void viewByID(View rootView)
	{
		noOfPhotos= (TextView)rootView.findViewById(R.id.no_of_photos_result_txt);
		photosResultGV =(GridView)rootView.findViewById(R.id.result_photos_gridview);
		noResultsTxt=(TextView)rootView.findViewById(R.id.no_filter_result_txt);
		noResultsImg = (ImageView)rootView.findViewById(R.id.no_results_img);
	}
	
	private void initViewControls()
	{
		showLoadingDialog();
		// initialize the list
				photos = new ArrayList<StudioPhoto>();

				// initialize the adapter
				adapter = new StudioPhotosAdapter(getActivity(),
						(ArrayList<StudioPhoto>) photos);
				photosResultGV.setAdapter(adapter);

				photosResultGV.setOnItemClickListener(this);
				photosResultGV.setOnScrollListener(this);
	}
	

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			ShowPhotosPopUp(position);
		}


	
	@SuppressWarnings("unchecked")
	private void getFilterData()
	{
		this.filterBy = getActivity().getIntent().getExtras().getInt(StudioFilterResultActivity.FILTER_BY_KEY);
		this.seasons = (ArrayList<Seasons>) getActivity().getIntent().getExtras().getSerializable(StudioFilterResultActivity.SEASONS_KEY);
	}

	private void ShowPhotosPopUp(final int Selected_Position) {
		if (dialog != null && dialog.isShowing())
			return;
		// custom dialog
		dialog = new Dialog(getActivity(), R.style.FullHeightDialog);
		dialog.setContentView(R.layout.photos_dialog);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		// get and set user the credit text
		TextView userCreditTxt = (TextView) dialog
				.findViewById(R.id.usercredit_txt);
		
			userCreditTxt.setText(UserManager.getInstance().getCurrentUser().getCredit() + "\n "
					+ getString(R.string.studio_xp));

		// get and set the photo points
		TextView photoPoints = (TextView) dialog
				.findViewById(R.id.photo_points_txt);
		photoPoints.setText(getString(R.string.photoUse_unock_credit) + " "
				+ photos.get(Selected_Position).getRequiredPoints() + " "
				+ getString(R.string.studio_xp));
		// define the view pager
		photosPager = (ViewPager) dialog.findViewById(R.id.photo_pager_id);
		// define the pervious btn
		ImageButton prevBtn = (ImageButton) dialog
				.findViewById(R.id.photos_previous_img_id);
		// define the next btn
		ImageButton nextBtn = (ImageButton) dialog
				.findViewById(R.id.photos_next_img_id);
		// define the user button
		Button useBtn = (Button) dialog.findViewById(R.id.photos_use_btn_id);

		// setting adapter of the pager
		photospagerAdapter = new PhotoPagerAdapter();
		photosPager.setAdapter(photospagerAdapter);
		// set the on pagescroll listener to detect the last swiped image
		OnPageChangeListener mListener = new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// check if standing at the last item , then call web service to
				// get more pictures
				if (photosPager.getCurrentItem() == photos.size() - 1 && !gettinMoreData && pageNo != -1){
					gettinMoreData = true;
					StudioManager.getInstance().getStudioPhotos(0, pageNo ,filterBy,seasons);
				}
			}
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		};
		photosPager.setOnPageChangeListener(mListener);

		// set the position of pager with the selected item
		photosPager.setCurrentItem(Selected_Position);

		// setting the click listener of previous button
		prevBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				photosPager.setCurrentItem(photosPager.getCurrentItem() - 1,
						true);
				// Toast.makeText(getActivity(),
				// photosPager.getCurrentItem()-1+"",
				// Toast.LENGTH_SHORT).show();

			}
		});
		// setting the click listener of next button
		nextBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				photosPager.setCurrentItem(photosPager.getCurrentItem() + 1,
						true);
				// check if standing at the last item , then call web service to
				// get more pictures
				if (photosPager.getCurrentItem() == photos.size() - 1&& !gettinMoreData && pageNo != -1){
					gettinMoreData = true;
					StudioManager.getInstance().getStudioPhotos(0,pageNo,filterBy,seasons);
				}
			}
		});

		// setting the listener of Use button
		useBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO first check user , unlock ! :)

				// get the selected photo
				final StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				// get the bitmap of the image

				App.getInstance()
						.getImageLoader()
						.loadImage(selectedPhoto.getPhotoURL(),
								new SimpleImageLoadingListener() {

									@Override
									public void onLoadingComplete(
											String imageUri, View view,
											Bitmap loadedImage) {
										// TODO navigate to Capture :)
										// save the Stream to device
										super.onLoadingComplete(imageUri, view,
												loadedImage);
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										loadedImage.compress(
												Bitmap.CompressFormat.PNG, 60,
												stream);
										String path = saveToInternalSorage(loadedImage);
										startActivity(CaptureActivity
												.getActivityIntent(
														getActivity(),
														path,
														selectedPhoto,
														selectedPhoto
																.getFolderId() == StudioFolder.FOLDER_TYPE_PLAYER ? true
																: false));

									}

								});
			}
		});

		// dismiss the dialog button
		ImageButton CloseBtn = (ImageButton) dialog
				.findViewById(R.id.photos_close_img_id);
		CloseBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

	}

	// save to internal storage
		private String saveToInternalSorage(Bitmap bitmapImage) {
			ContextWrapper cw = new ContextWrapper(getActivity());
			// path to /data/data/yourapp/app_data/imageDir
			File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
			// Create imageDir
			File mypath = new File(directory, "image.jpg");

			FileOutputStream fos = null;
			try {

				fos = new FileOutputStream(mypath);

				// Use the compress method on the BitMap object to write image to
				// the OutputStream
				bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return directory.getAbsolutePath();
		}

		private class PhotoPagerAdapter extends PagerAdapter {

			// ArrayList<StudioPhoto> images;
			private LayoutInflater inflater;

			PhotoPagerAdapter() {
				// this.images = images;
				inflater = getActivity().getLayoutInflater();
			}

			@Override
			public void destroyItem(ViewGroup container, int position, Object object) {
				container.removeView((View) object);
			}

			@Override
			public int getCount() {
				return photos.size();
			}

			@Override
			public Object instantiateItem(ViewGroup view, final int position) {
				View imageLayout = null;

				if (imageLayout == null) {
					imageLayout = inflater.inflate(R.layout.photo_popup_item, view,
							false);
				}
				final ProgressBar bar = (ProgressBar) imageLayout
						.findViewById(R.id.progressBar1);
				imageView = (ImageView) imageLayout.findViewById(R.id.imageView1);
				App.getInstance()
						.getImageLoader()
						.displayImage(photos.get(position).getPhotoURL(),
								imageView, App.getInstance().getDisplayOption(),
								new SimpleImageLoadingListener() {
									@Override
									public void onLoadingStarted(String imageUri,
											View view) {
										bar.setVisibility(View.VISIBLE);
										System.out.println("Loading");
									}

									@Override
									public void onLoadingFailed(String imageUri,
											View view, FailReason failReason) {
										String message = null;
										switch (failReason.getType()) {
										case IO_ERROR:
											message = "Input/Output error";
											break;
										case DECODING_ERROR:
											message = "Image can't be decoded";
											break;
										case NETWORK_DENIED:
											message = "Downloads are denied";
											break;
										case OUT_OF_MEMORY:
											message = "Out Of Memory error";
											break;
										case UNKNOWN:
											message = "Unknown error";
											break;
										}
										Toast.makeText(
												getActivity()
														.getApplicationContext(),
												message, Toast.LENGTH_SHORT).show();

										bar.setVisibility(View.GONE);
									}

									@Override
									public void onLoadingComplete(String imageUri,
											View view, Bitmap loadedImage) {
										bar.setVisibility(View.GONE);
									}
								});

				view.addView(imageLayout);

				return imageLayout;
			}

			@Override
			public boolean isViewFromObject(View view, Object object) {
				return view.equals(object);
			}

			@Override
			public void restoreState(Parcelable state, ClassLoader loader) {
			}

			@Override
			public Parcelable saveState() {
				return null;
			}
		}

	@Override
	public void onResume() {
		super.onResume();
		StudioManager.getInstance().addListener(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		StudioManager.getInstance().removeListener(this);
	}
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if (scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
			userScrolled = true;
		}

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		int lastInScreen = firstVisibleItem + visibleItemCount;
		if ((lastInScreen == photos.size()) && userScrolled && pageNo !=-1) {
			if(!gettinMoreData)
			{
				gettinMoreData=true;
			showLoadingDialog();
			StudioManager.getInstance().getStudioPhotos(0, pageNo, filterBy, seasons);
			}
		}

	}

	@Override
	public void onSuccess(List<StudioPhoto> objs) {

		hideLoadingDialog();
		gettinMoreData =false;
		if (objs != null && !objs.isEmpty()) {
			this.photos.addAll(objs);
			adapter.notifyDataSetChanged();
			if(photospagerAdapter != null)
				photospagerAdapter.notifyDataSetChanged();
			
			// check if there is no more data set page no to -1 , else if there was more data in server increment the page no
			if(objs.size()<24)
				pageNo=-1;
			else
				pageNo++;
		}
		else
		{
			noResultsTxt.setVisibility(View.VISIBLE);
			noResultsImg.setVisibility(View.VISIBLE);
			photosResultGV.setVisibility(View.GONE);
		}

	
	}

	@Override
	public void onException(AppException ex) {
		gettinMoreData=false;
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
		pageNo = -1;
	}
}
