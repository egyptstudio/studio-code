/**
 * 
 */
package com.tawasol.barcelona.ui.fragments;

import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.PostLike;
import com.tawasol.barcelona.entities.UserComment;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnLikeRecieved;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.FanProfileActivity;
import com.tawasol.barcelona.ui.activities.LikeActivity;
import com.tawasol.barcelona.ui.activities.UserProfileActivity;
import com.tawasol.barcelona.utils.UIUtils;

/**
 * @author Basyouni
 *
 */
public class LikeFragment extends BaseFragment implements OnLikeRecieved {

	ListView likeList;
	LikeAdapter adapter;
	DisplayImageOptions profileOption;
	int postId;
	ProgressBar bar;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.like_list_fragment,
				container, false);
		postId = getActivity().getIntent().getIntExtra(LikeActivity.POST_ID, 0);
		WallManager.getInstance().getLikeData(
				UserManager.getInstance().getCurrentUserId(), postId);
		profileOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1000)).build();
		bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
		likeList = (ListView) rootView.findViewById(R.id.likeList);

		return rootView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		WallManager.getInstance().addListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onPause()
	 */
	@Override
	public void onPause() {
		super.onPause();
		WallManager.getInstance().removeListener(this);
	}

	private class LikeAdapter extends BaseAdapter {

		List<PostLike> likes;

		public LikeAdapter(List<PostLike> likes) {
			this.likes = likes;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount() {
			return likes.size();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Object getItem(int position) {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int position) {
			return 0;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.Adapter#getView(int, android.view.View,
		 * android.view.ViewGroup)
		 */
		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflator = LayoutInflater.from(App
					.getInstance().getApplicationContext());
			View rootView = convertView;
			if (rootView == null)
				rootView = inflator.inflate(R.layout.like_list_item, parent,
						false);

			ImageView profileImage = (ImageView) rootView
					.findViewById(R.id.profile_image);
			profileImage.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (likes.get(position).getUserId() == UserManager.getInstance()
							.getCurrentUserId())
						getActivity().startActivity(UserProfileActivity
								.getActivityIntent(getActivity()));
					else
						getActivity().startActivity(FanProfileActivity.getIntent(
								getActivity(), likes.get(position).getUserId(), likes.get(position).getFullName()));
				}
			});
			TextView userName = (TextView) rootView.findViewById(R.id.userName);
			userName.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (likes.get(position).getUserId() == UserManager.getInstance()
							.getCurrentUserId())
						getActivity().startActivity(UserProfileActivity
								.getActivityIntent(getActivity()));
					else
						getActivity().startActivity(FanProfileActivity.getIntent(
								getActivity(), likes.get(position).getUserId(), likes.get(position).getFullName()));
				}
			});
			final ImageView follow = (ImageView) rootView
					.findViewById(R.id.follow);
			follow.setTag(likes.get(position).getUserId());
			if (profileImage != null
					&& likes.get(position).getProfilePicURL() != "")
				App
						.getInstance()
						.getImageLoader()
						.displayImage(
								this.likes.get(position).getProfilePicURL(),
								profileImage, profileOption);
			if (userName != null)
				userName.setText(this.likes.get(position).getFullName());
			if (UserManager.getInstance().getCurrentUserId() != this.likes.get(
					position).getUserId()) {
				if (follow != null) {
					if (this.likes.get(position).isFollowing())
						follow.setImageBitmap(BitmapFactory.decodeResource(
								getActivity().getResources(),
								R.drawable.followed));
					else
						follow.setImageBitmap(BitmapFactory.decodeResource(
								getActivity().getResources(),
								R.drawable.unfollowed));
				}
			}
			follow.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							if (UserManager.getInstance().getCurrentUserId() != 0) {
								if (likes.get(position).isFollowing()) {
								} else if (!likes.get(position).isFollowing()) {
									follow.setImageResource(R.drawable.followed);
									WallManager.getInstance().handleFollow(
											UserManager.getInstance()
													.getCurrentUserId(),
											likes.get(position).getUserId(),
											postId , getActivity());
								}
							} else {
//								getActivity()
//										.startActivity(
//												LogInActivity
//														.getActivityIntent(getActivity()));
							}
						}
					});

				}
			});

			return rootView;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tawasol.barcelona.listeners.OnLikeRecieved#onException(com.tawasol
	 * .barcelona.exception.AppException)
	 */
	@Override
	public void onException(AppException ex) {
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onSuccess(List<PostLike> likes) {
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		adapter = new LikeAdapter(likes);
		likeList.setAdapter(adapter);
	}

}
