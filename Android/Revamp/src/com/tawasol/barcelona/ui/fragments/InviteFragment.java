package com.tawasol.barcelona.ui.fragments;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import android.R.bool;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;


import com.google.android.gms.internal.ep;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.customView.SideBar;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.ContactEntity;
import com.tawasol.barcelona.entities.InvitationEntity;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnInviteResponseListener;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.FCBPhotoActivity;
import com.tawasol.barcelona.ui.activities.InviteMessageActivity;
import com.tawasol.barcelona.ui.adapters.ContactListAdapter;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.UIUtils;

/**
 * 
 * @author Turki
 *
 */
public class InviteFragment extends BaseFragment {

	LinkedHashMap<String, List<ContactEntity>> contacts = new LinkedHashMap<String, List<ContactEntity>>();
	List<String> characters = new ArrayList<String>();
	private String[] l;
	private ContactListAdapter adapter;
	private SideBar bar;
	private ExpandableListView expandableListView;
	private Button selectBtn;
	private static boolean selectFlag = false;
	private TextView inviteBtn;
	private boolean noConatctFlag = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView      = inflater.inflate(R.layout.invite_fragment, container, false);
		
		/** Initialize registration view **/
		init(rootView);
		
		/** Initialize char section **/
		initiateMap();
		
		/** Load all contacts **/
		loadContacts();
		
		/** Add # to put any contact that start with char not belong to A-Z **/
		characters.add("#");
		adapter = new ContactListAdapter(getActivity(), characters, contacts);
		expandableListView.setAdapter(adapter);
		bar.setListView(expandableListView, true);
		
		/** Check adapter to ExpandGroup **/
		if (adapter != null) {
			for (int i = 0; i < adapter.getGroupCount(); i++)
				expandableListView.expandGroup(i);
		}
		
		/** Handle expandableListView OnGroupClickListener **/
		expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return true;
			}
		});
		
		/** Handle expandableListView OnChildClickListener **/
		expandableListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long arg4) {
				// TODO Auto-generated method stub
				
				return false;
			}
		});
		
		/** Handle selectButton on click to change status of all CheckBox **/
		selectBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/** If CheckBox is already Selected change to UnSelected and change ButtonText to SELECT */
				if(selectFlag){
					selectBtn.setText(getResources().getString(R.string.DeleteMsg_Select_All));
					selectFlag = false;
					selectAllCheckBox(selectFlag);

				/** If CheckBox is already UnSelected change to UnSelected and change ButtonText to UN-SELECT */
				}else{
					selectBtn.setText(getResources().getString(R.string.smsInvite_SelectAll));
					selectFlag = true;
					selectAllCheckBox(selectFlag);
				}
			}
		});
		return rootView;
	}

	private void init(View rootView){
		bar                = (SideBar) rootView.findViewById(R.id.sideBar);
		expandableListView = (ExpandableListView) rootView.findViewById(R.id.contact_list);
		selectBtn          = (Button) rootView.findViewById(R.id.select_btn);
		
		/** Set InviteButton image with R.drawable.done **/
		inviteBtn          = (TextView) getActivity().findViewById(R.id.rightBtn);
		inviteBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.done, 0);
		inviteBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/** Set value to OutComing intent **/
				setIntentValue();	
			}
		});
	}
	@SuppressLint("DefaultLocale")
	private void loadContacts() {
		ContentResolver cr = getActivity().getContentResolver();
		Cursor cur         = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		
		if (cur.getCount() > 0) {
			selectBtn.setEnabled(true);
			noConatctFlag = true;
			while (cur.moveToNext()) {
				String id   = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				
				/** Get Contact Name **/
				String name     = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				String imageURI = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
				List<ContactEntity> phoneList = null;
				
				/** Check if 1st char of Contact Name belong to predefined Characters, if not add it to # category **/
				if (characters.contains(String.valueOf(name.charAt(0)).toUpperCase())) {
					phoneList = contacts.get(String.valueOf(name.charAt(0)).toUpperCase());
				} else {
					phoneList = contacts.get("#");
				}
				
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {	
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
										   ContactsContract.CommonDataKinds.Phone.CONTACT_ID+ " = ?", new String[] {id}, null);
					
					while (pCur.moveToNext()) {
						String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						ContactEntity con = new ContactEntity(name, phoneNo, imageURI, false);
						phoneList.add(con);
					}
					contacts.put(String.valueOf(name.charAt(0)), phoneList);
					pCur.close();
				}
			}
		}else{
			selectBtn.setEnabled(false);
			noConatctFlag = false;
		}
	}
	
	/** Get SelectAll & UnSelectAll CheckBox **/
	private void selectAllCheckBox(boolean flag){
		int groupSize=0;
		int childSize=0;
		
		/** Get Group size **/
		groupSize = adapter.getGroupCount();
		for(int i=0; i<groupSize; i++){
			
			/** Get Child size of each Group **/
			childSize = adapter.getChildrenCount(i);
			for(int j=0; j<childSize; j++){
				
				/** Check if this last child or not **/
				boolean isLast = false;
				if(j == childSize-1)
					isLast = true;
				
				/** Set entity selected with coming flag**/
				((ContactEntity) adapter.getChild(i, j)).setSelected(flag);
			}
		}
		adapter.notifyDataSetChanged();
	}
	
	/** Get selected contact Phone List **/
	private ArrayList<String> getSelectedContact(){
		int groupSize=0;
		int childSize=0;
		ArrayList<String> selectedPhoneNumbers = new ArrayList<String>();
		
		/** Get Group size **/
		groupSize = adapter.getGroupCount();
		for(int i=0; i<groupSize; i++){
			
			/** Get Child size of each Group **/
			childSize = adapter.getChildrenCount(i);
			for(int j=0; j<childSize; j++){
				
				/** Check if this last child or not **/
				boolean isLast = false;
				if(j == childSize-1)
					isLast = true;
				
				/** Check if this entity is selected or not to add it on PhonesList**/
				ContactEntity contatcEntity = ((ContactEntity) adapter.getChild(i, j));
				if(contatcEntity.isSelected())
					selectedPhoneNumbers.add(contatcEntity.getPhone());
			}
		}
		return selectedPhoneNumbers;
	}
	
	/** Set invitation phonesList to OutComing intent  */
	public void setIntentValue(){
		
		/** Get selected PhoneList and send it to InviteMessageActivity intent and start the activity */
		ArrayList<String> list = new ArrayList<String>();
		list = getSelectedContact();
		
		if(list.size() > 0){
			Intent intent = InviteMessageActivity.getActivityIntent(getActivity());
			Bundle bundle = new Bundle();
			bundle.putSerializable(Params.SMS_INVITATION.PHONES_LIST_NAME, (Serializable) list);
			intent.putExtra(Params.SMS_INVITATION.PHONES_BUNDLE,bundle);
			startActivity(intent);
		}
		
		if(!noConatctFlag){
			cancelInvitation();
			getActivity().finish();
		}
	}
	
	/** Cancel invitation and GOTO Registration Activity to handle the result **/
	private void cancelInvitation(){
		Intent intent = new Intent();
		intent.putExtra(Params.SMS_INVITATION.INVITATION_FLAG, true);
		getActivity().setResult(Activity.RESULT_CANCELED, intent);
	}
	
	/** Define all sections from A-Z **/
	private void initiateMap() {
		l = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
						   "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", 
						   "U", "V", "W", "X", "Y", "Z" };
		characters = new ArrayList<String>(Arrays.asList(l));
		contacts.put("A", new ArrayList<ContactEntity>());
		contacts.put("B", new ArrayList<ContactEntity>());
		contacts.put("C", new ArrayList<ContactEntity>());
		contacts.put("D", new ArrayList<ContactEntity>());
		contacts.put("E", new ArrayList<ContactEntity>());
		contacts.put("F", new ArrayList<ContactEntity>());
		contacts.put("G", new ArrayList<ContactEntity>());
		contacts.put("H", new ArrayList<ContactEntity>());
		contacts.put("I", new ArrayList<ContactEntity>());
		contacts.put("J", new ArrayList<ContactEntity>());
		contacts.put("K", new ArrayList<ContactEntity>());
		contacts.put("L", new ArrayList<ContactEntity>());
		contacts.put("M", new ArrayList<ContactEntity>());
		contacts.put("N", new ArrayList<ContactEntity>());
		contacts.put("O", new ArrayList<ContactEntity>());
		contacts.put("P", new ArrayList<ContactEntity>());
		contacts.put("Q", new ArrayList<ContactEntity>());
		contacts.put("R", new ArrayList<ContactEntity>());
		contacts.put("S", new ArrayList<ContactEntity>());
		contacts.put("T", new ArrayList<ContactEntity>());
		contacts.put("U", new ArrayList<ContactEntity>());
		contacts.put("V", new ArrayList<ContactEntity>());
		contacts.put("W", new ArrayList<ContactEntity>());
		contacts.put("X", new ArrayList<ContactEntity>());
		contacts.put("Y", new ArrayList<ContactEntity>());
		contacts.put("Z", new ArrayList<ContactEntity>());
		contacts.put("#", new ArrayList<ContactEntity>());
	}
	
}
