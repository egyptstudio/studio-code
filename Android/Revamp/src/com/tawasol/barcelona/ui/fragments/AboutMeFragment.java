package com.tawasol.barcelona.ui.fragments;

import java.io.File;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.managers.UserManager;

public class AboutMeFragment extends BaseFragment {
	private static final String ABOUT_ME_INITIAL_STATE = "ProfileInitialState";
	TextView statusTxt  , aboutTxt , relationshipTxt /*, religionTxt*/ , educationTxt , jobTxt , companyTxt,incomeTxt;
	LinearLayout aboutMeLayout ;
	User user;
	private TextView statusTitleTxt;
	private TextView relationshipTitleTxt;
	//private TextView religionTitleTxt;
	private TextView educationTitleTxt;
	private TextView jobTitleTxt;
	private TextView companyTitleTxt;
	private TextView incomeTitleTxt;
	private TextView aboutmeTitleTxt;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_about_me, container, false);
		
		findViewByID(rootView);
		initUser();
		InitViewControls();
		if( IsInitialState())
			handleAboutInitialState();
		return rootView;
	}

	private void findViewByID(View rootView)
	{
		statusTxt = (TextView)rootView.findViewById(R.id.about_status_txt);
		aboutTxt = (TextView)rootView.findViewById(R.id.about_me_txt);
		relationshipTxt = (TextView)rootView.findViewById(R.id.about_relationship_txt);
		//religionTxt = (TextView)rootView.findViewById(R.id.about_relogion_txt);
		educationTxt = (TextView)rootView.findViewById(R.id.about_education_txt);
		jobTxt = (TextView)rootView.findViewById(R.id.about_job_txt);
		companyTxt = (TextView)rootView.findViewById(R.id.about_company_txt);
		incomeTxt = (TextView)rootView.findViewById(R.id.about_income_txt);
		aboutMeLayout = (LinearLayout)rootView.findViewById(R.id.aboutMe_LinearLayout);
		//initialStateLayout = (LinearLayout)rootView.findViewById(R.id.initial_state_LinearLayout);
		
		statusTitleTxt=(TextView)rootView.findViewById(R.id.status_txt);
		aboutmeTitleTxt = (TextView)rootView.findViewById(R.id.about_me_txt_txt);
		relationshipTitleTxt = (TextView)rootView.findViewById(R.id.relationship_txt);
		//religionTitleTxt =(TextView)rootView.findViewById(R.id.religion_txt);
		educationTitleTxt=(TextView)rootView.findViewById(R.id.education_txt);
		jobTitleTxt=(TextView)rootView.findViewById(R.id.job_txt);
		companyTitleTxt=(TextView)rootView.findViewById(R.id.company_txt);
		incomeTitleTxt = (TextView)rootView.findViewById(R.id.income_txt);
	}

	public boolean IsInitialState() {
		return !(new File(getActivity().getFilesDir() + "/" + ABOUT_ME_INITIAL_STATE)
				.exists());
	}

	private void handleAboutInitialState()
	{
//		aboutMeLayout.setVisibility(View.GONE);
//		initialStateLayout.setVisibility(View.VISIBLE);
		
		statusTxt.setText(" "+getResources().getString(R.string.point_50XP));
		aboutTxt.setText(" "+getResources().getString(R.string.point_50XP));
		relationshipTxt.setText(" "+getResources().getString(R.string.point_50XP));
		educationTxt.setText(" "+getResources().getString(R.string.point_50XP));
		jobTxt.setText(" "+getResources().getString(R.string.point_50XP));
		companyTxt.setText(" "+getResources().getString(R.string.point_50XP));
		incomeTxt.setText(" "+getResources().getString(R.string.point_50XP)); // TODO :)
		//religionTxt.setText(" "+getResources().getString(R.string.point_50XP)); // TODO :)
		
		statusTitleTxt.setVisibility(View.GONE);
		aboutmeTitleTxt.setVisibility(View.GONE);
		relationshipTitleTxt.setVisibility(View.GONE);
		educationTitleTxt.setVisibility(View.GONE);
		jobTitleTxt.setVisibility(View.GONE);
		companyTitleTxt.setVisibility(View.GONE);
		incomeTitleTxt.setVisibility(View.GONE);
		//religionTitleTxt.setVisibility(View.GONE);
		
		removeInitialStateIndicator();
		
	}
	/**Initialize the user variable using the UserManger to get the current logged in user*/
	private void initUser()
	{
		this.user = UserManager.getInstance().getCurrentUser();
	}

	private void removeInitialStateIndicator()
	{
		
			App.getInstance().runInBackground(new Runnable() {

				@Override
				public void run() {
					try {
						InternalFileSaveDataLayer.saveObject(getActivity(),ABOUT_ME_INITIAL_STATE
								, "");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
	}
	private void InitViewControls()
{
		// set the data of Status ,About me , Education , Job , Company , Religion
		if((user.getStatus() != null && !user.getStatus().isEmpty()))
		{
			statusTxt.setText(user.getStatus());
			statusTitleTxt.setVisibility(View.VISIBLE);
		}
		else{
			statusTitleTxt.setVisibility(View.GONE);
		}

		if (user.getEducation() != null && !user.getEducation().isEmpty())
		{
			educationTxt.setText(user.getEducation());
			educationTitleTxt.setVisibility(View.VISIBLE);
		}
		else
			educationTitleTxt.setVisibility(View.GONE);

		if(user.getAboutMe() != null && !user.getAboutMe().isEmpty())
		{
			aboutTxt.setText(user.getAboutMe());
			aboutmeTitleTxt.setVisibility(View.VISIBLE);
		}
		else
			aboutmeTitleTxt.setVisibility(View.GONE);

		if(user.getJob() != null && !user.getJob().isEmpty())
		{
			jobTxt.setText(user.getJob());
			jobTitleTxt.setVisibility(View.VISIBLE);
		}
		else
			jobTitleTxt.setVisibility(View.GONE);

		if((user.getCompany() != null && !user.getCompany().isEmpty()))
		{
			companyTxt.setText(user.getCompany());
			companyTitleTxt.setVisibility(View.VISIBLE);
		}
		else
			companyTitleTxt.setVisibility(View.GONE);


		/*if((user.getReligion()!= null && !user.getReligion().isEmpty()))
		{
			religionTxt.setText(user.getReligion());
			religionTitleTxt.setVisibility(View.VISIBLE);
		}
		else
			religionTitleTxt.setVisibility(View.GONE);*/

		
		// set the Relationship text
		if(user.getRelationship() !=0){
			setRelationship(user.getRelationship());
			relationshipTitleTxt.setVisibility(View.VISIBLE);
		}
		else
			relationshipTitleTxt.setVisibility(View.GONE);
		
			
			// set the income text
		if(user.getIncome() != 0){
			setIncome(user.getIncome());
			incomeTitleTxt.setVisibility(View.VISIBLE);
		}
		else
			incomeTitleTxt.setVisibility(View.GONE);
			// set the status text
						//statusTxt.setText(user.getStatus().equals("")? getString(R.string.initial_state_status) : user.getStatus());
					
					// set the About text
					//	aboutTxt.setText(user.getAboutMe().equals("")? getString(R.string.initial_state_about_me) : user.getAboutMe());
			
			// set the religion text
			//	religionTxt.setText(user.getReligion().equals("")? getString(R.string.initial_state_religion) : user.getReligion());
				
				// set the education text
				//educationTxt.setText(user.getEducation().equals("")? getString(R.string.initial_state_Education) : user.getEducation());
				
				// set the job text
			//	jobTxt.setText(user.getJob().equals("")? getString(R.string.initial_state_job) : user.getJob());
				
				// set the company text
				//companyTxt.setText(user.getCompany().equals("")? getString(R.string.initial_state_company) : user.getCompany());
				
				
}
	
	// set the relationShipStatus
	private void setRelationship(int key)
	{
		if(key == User.RELATIONSHIP_MARRIED)
			relationshipTxt.setText(getString(R.string.relationship_list_item2));
		else if(key == User.RELATIONSHIP_ENGAGED)
			relationshipTxt.setText(getString(R.string.relationship_list_item3));
		else if(key == User.RELATIONSHIP_IN_RELATIONSHIP)
			relationshipTxt.setText(getString(R.string.relationship_list_item4));
		else if(key == User.RELATIONSHIP_SINGLE)
			relationshipTxt.setText(getString(R.string.relationship_list_item1));
		else
			relationshipTxt.setText(getResources().getString(R.string.MyProfile_Relationship));
	}
	//set the relationShipStatus
	private void setIncome(int key)
	{
		if(key == User.INCOME_LOW)
			incomeTxt.setText(getString(R.string.income_list_item1));
		else if(key == User.INCOME_HIGH)
			incomeTxt.setText(getString(R.string.income_list_item3));
		else if(key == User.INCOME_AVERAGE)
			incomeTxt.setText(getString(R.string.income_list_item2));
		else
			incomeTxt.setText(getResources().getString(R.string.initial_state_income));
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(UserManager.getInstance().getCurrentUserId() ==0)
			getActivity().finish();
		
	}

}
