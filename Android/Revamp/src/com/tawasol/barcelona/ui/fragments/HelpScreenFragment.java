package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import com.aviary.android.feather.cds.billing.util.SkuDetails;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.data.cache.WallTable;
import com.tawasol.barcelona.entities.HelpScreen;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnHelpScreensReceivedListener;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.ui.adapters.HelpScreenAdapter;
import com.tawasol.barcelona.utils.UIUtils;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

public class HelpScreenFragment extends BaseFragment implements
		OnPageChangeListener, OnClickListener, OnHelpScreensReceivedListener {
	
	private ViewPager helpScreenPager;
	private TextView helpImagesCountTxt;
	private TextView helpSkipTxt;
	private HelpScreenAdapter helpScreenAdapter;
	ArrayList<HelpScreen> helpScrnList;
	private boolean isFullScreen;
	private ProgressDialog mDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i("Inside Help Screen Count",String.valueOf(WallTable.getInstance().getCount()));
		View rootView = inflater.inflate(R.layout.fragment_help_screen,
				container, false);

		viewById(rootView);
		
		isFullScreen  = (getActivity().getWindow().getAttributes().flags & WindowManager.LayoutParams.FLAG_FULLSCREEN) != 0;
		//getBundleData();

		

		return rootView;
	}

	private void viewById(View rootView) {
		helpScreenPager = (ViewPager) rootView
				.findViewById(R.id.help_screen_view_pager);
		helpImagesCountTxt = (TextView) rootView
				.findViewById(R.id.help_screen_images_count_txt);
		helpSkipTxt = (TextView) rootView
				.findViewById(R.id.help_screen_skip_txt);
	}

	private void initViewControls() {
		helpScrnList = new ArrayList<HelpScreen>();
		helpScreenAdapter = new HelpScreenAdapter(getActivity(), helpScrnList);
		helpScreenPager.setAdapter(helpScreenAdapter);

		// set the listener of the pager scroll
		helpScreenPager.setOnPageChangeListener(this);

		// set the click listener of skip button
		helpSkipTxt.setOnClickListener(this);
		
		// check if not full screen 
		if(!isFullScreen)
		{
			// hide skip button 
			helpSkipTxt.setVisibility(View.INVISIBLE);
			
			// set the gravity of images counter to middle 
			helpImagesCountTxt.setGravity(Gravity.CENTER);
		}
		else 
		{
			// show the skip button 
			helpSkipTxt.setVisibility(View.VISIBLE);
			
			// set the gravity of images counter to middle 
			helpImagesCountTxt.setGravity(Gravity.LEFT);
			
			
		}

	}
	
	/** get the bundle data of IS Full Screen , to check whether it is the first time the app installed or not */
//	private void getBundleData()
//	{
//		isFullScreen = getActivity().getIntent().getExtras().getBoolean(HelpScreenActivity.IS_FULL_SCREEN);
//	}

	@Override
	public void onResume() {
		super.onResume();
		UserManager.getInstance().addListener(this);
		
		initViewControls();

		// get help screens
		UserManager.getInstance().getHelpScreens();
		showLoader();
	}

	@Override
	public void onPause() {
		super.onPause();
		UserManager.getInstance().removeListener(this);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		int currentImageIndex = helpScreenPager.getCurrentItem() + 1;
		// set the text
		helpImagesCountTxt.setText(currentImageIndex + " "
				+ getResources().getString(R.string.help_screen_of) + " "
				+ helpScrnList.size());
	}

	@Override
	public void onPageSelected(int arg0) {
	}
	private void showLoader() {
		if (mDialog == null) {
			mDialog = new ProgressDialog(getActivity());
			mDialog.setCanceledOnTouchOutside(false);
			mDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
			mDialog.setCancelable(false);
		}
		if (!mDialog.isShowing()) {
			try {
				mDialog.show();
			} catch (Exception e) {
			}
		}
	}

	private void hideLoader() {

		if (!getActivity().isFinishing() && mDialog != null && mDialog.isShowing()) {
			try {
				mDialog.dismiss();
			} catch (Exception e) {
			}
		}

	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.help_screen_skip_txt:
			startActivity(HomeActivity.getActivityIntent(getActivity()));
			getActivity().finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onSuccess(List<HelpScreen> objs) {
		hideLoader();
		if (objs != null && objs.size() > 0) {
			helpImagesCountTxt.setVisibility(View.VISIBLE);
			helpScrnList.clear();
			helpScrnList.addAll(objs);
			helpScreenAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onException(AppException ex) {
		hideLoader();
		UIUtils.showToast(getActivity(), ex.getMessage());
		if(isFullScreen){
			startActivity(HomeActivity.getActivityIntent(getActivity()));
		getActivity().finish();
		}
	}

}
