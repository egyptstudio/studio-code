package com.tawasol.barcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.ui.activities.ShopActivityTabs;

public class ShopFragmentTabs extends Fragment {

	private FragmentTabHost mTabHost;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.shop_fragment_tabs,
				container, false);

		initView(rootView);

		return rootView;
	}

	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);

		mTabHost.setup(getActivity(), getChildFragmentManager(),
				android.R.id.tabcontent);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
						.getString(R.string.nike_store)), R.drawable.left_tab),
				NikeStore.class, null);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
						.getString(R.string.premium_packages)),
						R.drawable.center_tab), PremiumPackageFragment.class,
				null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
						.getString(R.string.points_packages)),
						R.drawable.center_tab), PointPAckages.class, null);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
						.getString(R.string.Redemption)), R.drawable.right_tab),
				RedemptionFragment.class, null);

		// mTabHost.getTabWidget().getChildTabViewAt(1).setEnabled(false);
		// mTabHost.getTabWidget().getChildTabViewAt(3).setEnabled(false);
		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// for (int i = 0; i < mTabHost.getTabWidget().getChildCount();
				// i++) {
				// TextView tv = (TextView) mTabHost.getCurrentTabView()
				// .findViewById(R.id.tabs_text);
				// tv.setTextColor(Color.WHITE);
				//
				// }
				// //
				// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				//
				// TextView tv = (TextView)
				// mTabHost.getChildAt(mTabHost.getCurrentTab())
				// .findViewById(R.id.tabs_text);
				// tv.setTextColor(Color.BLACK);

				for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
					TextView tv = (TextView) mTabHost.getTabWidget()
							.getChildAt(i).findViewById(R.id.tabs_text);
					// .setBackgroundResource(R.drawable.tab); // unselected

					// TextView tv = (TextView) mTabHost.getCurrentTabView()
					// .findViewById(R.id.tabs_text); // for Selected
					// Tab
					tv.setTextColor(getActivity().getResources().getColor(
							R.color.new_yellow));
				}
				TextView tv = (TextView) mTabHost.getTabWidget()
						.getChildAt(mTabHost.getCurrentTab())
						.findViewById(R.id.tabs_text);
				// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				// .setBackgroundResource(R.drawable.tab_pressed); // selected
				// TextView tv = (TextView) mTabHost.getCurrentTabView()
				// .findViewById(android.R.id.title); // for Selected Tab
				tv.setTextColor(Color.WHITE);

				// WallFragment wall = (WallFragment) getChildFragmentManager()
				// .findFragmentByTag("Latest");
				// if (getActivity().getIntent().getExtras() == null)
				// wall.getToFirst();
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		// for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
		// TextView tv = (TextView) mTabHost.getCurrentTabView()
		// .findViewById(R.id.tabs_text);
		// tv.setTextColor(Color.WHITE);
		//
		// }
		// // mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
		//
		// TextView tv = (TextView)
		// mTabHost.getChildAt(mTabHost.getCurrentTab())
		// .findViewById(R.id.tabs_text);
		// tv.setTextColor(Color.BLACK);
		// .setBackgroundResource(R.drawable.tab_pressed);

		for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.tabs_text);
			// .setBackgroundResource(R.drawable.tab); // unselected

			// TextView tv = (TextView) mTabHost.getCurrentTabView()
			// .findViewById(R.id.tabs_text); // for Selected
			// Tab
			tv.setTextColor(getActivity().getResources().getColor(
					R.color.new_yellow));
			// tv.setTextSize(getActivity().getResources().getDimension(
			// R.dimen.smallTextSize));
		}
		TextView tv = (TextView) mTabHost.getTabWidget()
				.getChildAt(mTabHost.getCurrentTab())
				.findViewById(R.id.tabs_text);
		// tv.setTextSize(getActivity().getResources().getDimension(
		// R.dimen.smallTextSize));
		// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
		// .setBackgroundResource(R.drawable.tab_pressed); // selected
		// TextView tv = (TextView) mTabHost.getCurrentTabView()
		// .findViewById(android.R.id.title); // for Selected Tab
		tv.setTextColor(Color.WHITE);
	}

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid) {
		// TODO Auto-generated method stub
		// View v = LayoutInflater.from(getActivity()).inflate(
		// R.layout.shop_tabs, null);
		View v = getActivity().getLayoutInflater().inflate(R.layout.shop_tabs,
				null);
		v.setBackgroundResource(resid);
		// LinearLayout container =
		// (LinearLayout)v.findViewById(R.id.CUSTOMCONTAINER);
		// container.setBackgroundResource(resid);
		final TextView text = (TextView) v.findViewById(R.id.tabs_text);
		text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getActivity()
				.getResources().getDimensionPixelSize(R.dimen.tabsText));
		text.setText(spec.getTag());
		// text.setTextSize(getActivity().getResources().getDimension(
		// R.dimen.smallTextSize));
		text.setTextColor(getActivity().getResources().getColor(
				android.R.color.white));
		// text.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// if (mTabHost.getCurrentTabTag().equalsIgnoreCase(
		// text.getText().toString())) {
		// Fragment wall = getChildFragmentManager()
		// .findFragmentByTag(text.getText().toString());
		// if (wall instanceof WallFragment) {
		// ((WallFragment) wall).getToFirst();
		// } else if (wall instanceof TopTenFragment) {
		// ((TopTenFragment) wall).getToFirst();
		// }
		// } else {
		// mTabHost.setCurrentTabByTag(text.getText().toString());
		// }
		// }
		// });
		return spec.setIndicator(v);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {

			if (requestCode == ShopActivityTabs.POINTS_REQUEST_CODE) {
				PointPAckages fragment = (PointPAckages) getChildFragmentManager()
						.findFragmentByTag(
								getResources().getString(
										R.string.points_packages));
				fragment.onActivityResult(requestCode, resultCode, data);
			} else if (requestCode == PointPAckages.loginRequestCode) {
				PointPAckages fragment = (PointPAckages) getChildFragmentManager()
						.findFragmentByTag(
								getResources().getString(
										R.string.points_packages));
				fragment.onActivityResult(requestCode, resultCode, data);
			} else if (requestCode == PremiumPackageFragment.loginRequestCode) {
				PremiumPackageFragment fragment = (PremiumPackageFragment) getChildFragmentManager()
						.findFragmentByTag(
								getResources().getString(
										R.string.premium_packages));
				fragment.onActivityResult(requestCode, resultCode, data);
			}

			else {
				PremiumPackageFragment fragment = (PremiumPackageFragment) getChildFragmentManager()
						.findFragmentByTag(
								getResources().getString(
										R.string.premium_packages));
				fragment.onActivityResult(requestCode, resultCode, data);
			}
			// refreshMenu();
		}
	}

}
