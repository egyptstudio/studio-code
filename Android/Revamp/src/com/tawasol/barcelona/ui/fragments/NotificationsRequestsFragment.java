package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.NotificationsTable;
import com.tawasol.barcelona.entities.Notification;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnFriendRequestResponseListener;
import com.tawasol.barcelona.listeners.OnNotificationsReceivedListener;
import com.tawasol.barcelona.managers.NotificationsManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.BaseActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * handle notification requests view of Notification Type "3"
 * @author Mohga 
 * */
public class NotificationsRequestsFragment extends BaseFragment implements OnNotificationsReceivedListener,OnFriendRequestResponseListener {

	private static NotificationsRequestsFragment notificationListFrag;

	private ListView notificationsLV;
	NotificationsRequestsAdapter requestsAdapter;
	ArrayList<Notification> notifications;
	int NotificationViewType;
	LinearLayout listLayout , initialLayout;
	DisplayImageOptions profileOption;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_notifications_requests_list,
				container, false);
		
		// get the list
		notificationsLV = (ListView)rootView.findViewById(R.id.notifications_requests_list_view);
		
		//initialize the list
		notifications = new ArrayList<Notification>();
		
		// get notification form server
		//notifications = (ArrayList<Notification>) NotificationsManager.getInstance().getCachedFilteredNotifications(Notification.LIKE_NOTIFICATIONS);

		
		// initialize the adapter
		requestsAdapter = new NotificationsRequestsAdapter();
		
		// set the adapter
		notificationsLV.setAdapter(requestsAdapter);

		
		
		initView(rootView);

		
		return rootView;
	}

	
	private void initView(View rootView) {
		listLayout = (LinearLayout)rootView.findViewById(R.id.notifications_list_layout);
		initialLayout = (LinearLayout)rootView.findViewById(R.id.notifications_initial_state_layout);
		/*notificationsLV.setPadding(0, 0, 0,
				((BaseActivity) getActivity()).getBottomBarHeight());*/
		profileOption = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.resetViewBeforeLoading(true)
		.showImageOnFail(R.drawable.ic_launcher)
		.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(1000)).build();
		
		notifications.clear();
		
			notifications.addAll((ArrayList<Notification>) NotificationsManager.getInstance().getFriendRequestsNotifications()/*(ArrayList<Notification>) NotificationsManager.getInstance().getCachedFilteredNotifications(Notification.FRIEND_REQUEST_NOTIFICATIONS)*/);
			if(notifications != null && !notifications.isEmpty() )
				handleLayouts();
				else{
					showLoadingDialog();
					NotificationsManager.getInstance().getNotificationsFromServer();
				}
			
	}
	private void handleLayouts()
	{
		if(notifications != null && !notifications.isEmpty())
		{
			
				requestsAdapter.notifyDataSetChanged();
			listLayout.setVisibility(View.VISIBLE);
		}
		else
		{
			listLayout.setVisibility(View.GONE);
			initialLayout.setVisibility(View.VISIBLE);
		}
	}
	

	public static NotificationsRequestsFragment newInstance() {
		if (notificationListFrag == null)
			notificationListFrag = new NotificationsRequestsFragment();
		return notificationListFrag;
	}

@Override
	public void onResume() {
		super.onResume();
		/*if (requestsAdapter != null)
			requestsAdapter.notifyDataSetChanged();*/
		NotificationsManager.getInstance().addListener(this);
	}



	@Override
	public void onPause() {
		super.onPause();
		NotificationsManager.getInstance().removeListener(this);
	}


	@Override
	public void onSuccess(List<Notification> objs) {
		if(objs != null && !objs.isEmpty())
		{
			notifications.clear();
			
			notifications.addAll((ArrayList<Notification>) NotificationsManager.getInstance().getFriendRequestsNotifications());
			if(notifications != null && !notifications.isEmpty() )
				handleLayouts();
				else{
					showLoadingDialog();
					NotificationsManager.getInstance().getNotificationsFromServer();
				}
			
			/*if(notifications!= null )
			list.addAll(notifications);
			notifications.clear();
			notifications.addAll(list);
			notifications.addAll(objs);
			adapter.notifyDataSetChanged();
			handleLayouts();*/
		
			/*
			ArrayList<Notification>list= new ArrayList<Notification>();
			if(notifications!= null )
			list.addAll(notifications);
			notifications.clear();
			notifications.addAll(list);
			notifications.addAll(objs);
			requestsAdapter.notifyDataSetChanged();
			handleLayouts();
		*/}
		
	}


	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		//UIUtils.showToast(getActivity(), ex.getMessage());
		
		if(notifications != null && !notifications.isEmpty())
		{
				requestsAdapter.notifyDataSetChanged();
			listLayout.setVisibility(View.VISIBLE);
		}
		else
		{
			listLayout.setVisibility(View.GONE);
			initialLayout.setVisibility(View.VISIBLE);
		}
		
	}


	@Override
	public void onSuccess(int senderId) {
		hideLoadingDialog();
		int index = getNotificationPosition(senderId);
		NotificationsTable.getInstance().delete(notifications.get(index).getNotificationId());
		notifications.remove(index);
		requestsAdapter.notifyDataSetChanged();
		
		NotificationsFragment.updateTab();

		if(notifications == null || notifications.isEmpty()){
		listLayout.setVisibility(View.GONE);
		initialLayout.setVisibility(View.VISIBLE);
		}
	
	}
	private int getNotificationPosition(int senderId) {
		for (int x = 0; x < notifications.size(); x++) {
			if (senderId == notifications.get(x).getSenderId())
				return x;
		}
		return 0;
	}
	
	
	private class NotificationsRequestsAdapter extends BaseAdapter{

		//private Context context;
		//private ArrayList<Notification> notifications ;
		
		/*public NotificationsRequestsAdapter (Context context , ArrayList<Notification> notifications)
		{
			this.context = context;
			this.notifications = notifications;
		}*/
		@Override
		public int getCount() {
			return notifications.size();
		}

		@Override
		public Notification getItem(int position) {
			return notifications.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final NotificationsRequestsViewHolder holder;
			
			if(convertView ==  null)
			{

				LayoutInflater inflater = (LayoutInflater) 
						getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				holder = new NotificationsRequestsViewHolder();
				convertView = inflater.inflate(
						R.layout.notifications_request_item, parent,
						false);
				
				holder.requesterName = (TextView)convertView.findViewById(R.id.fan_name);
				holder.requesterImg = (ImageView)convertView.findViewById(R.id.fan_profile_img_id);
			//	holder.favImg=(ImageButton)convertView.findViewById(R.id.favourate_button);
				//holder.followImg = (ImageButton)convertView.findViewById(R.id.followed_fan);
				//holder.noOfPicsTxt = (TextView)convertView.findViewById(R.id.num_of_pics);
				holder.acceptImg = (ImageView)convertView.findViewById(R.id.acceptRequest);
				holder.rejectImg = (ImageView)convertView.findViewById(R.id.reject_request);
				holder.requesterimgPB = (ProgressBar)convertView.findViewById(R.id.progress);
				
				// set the tag of buttons
				holder.acceptImg.setTag(position);
				holder.rejectImg.setTag(position);
				
				convertView.setTag(holder);
			}
			else
				holder = (NotificationsRequestsViewHolder) convertView.getTag();
			
			// set the requester Name 
			holder.requesterName.setText(notifications.get(position).getSenderFullName());
			
			// set the requester image 
			App.getInstance().getImageLoader().displayImage(notifications.get(position).getSenderImgUrl(), holder.requesterImg, profileOption, new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String arg0, View arg1) {				
				}
				
				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					holder.requesterimgPB.setVisibility(View.GONE);
					holder.requesterImg.setImageResource(R.drawable.profile_icon);	
				}
				
				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					holder.requesterimgPB.setVisibility(View.GONE);				
				}
				
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {				
				}
			});
				
				
			
			// set the fav image 
			// TODO ask hamed :) 
			
			// set the follow image 
			// TODO ask hamed :) 
			
			// set the no of pics
			// TODO ask hamed :) 
			
			holder.acceptImg.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showLoadingDialog();
					NotificationsManager.getInstance().handleFriend(UserManager.getInstance().getCurrentUserId(), notifications.get((Integer) v.getTag()).getSenderId(), true);
					//((BaseActivity)getActivity()).refreshMenu();
				}
			});
			
			holder.rejectImg.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showLoadingDialog();
					NotificationsManager.getInstance().handleFriend(UserManager.getInstance().getCurrentUserId(), notifications.get((Integer) v.getTag()).getSenderId(), false);
				//((BaseActivity)getActivity()).refreshMenu();
				}
			});
				return convertView;
		}
		
		private class NotificationsRequestsViewHolder
		{
			TextView requesterName;
			ImageView requesterImg;
//			ImageButton favImg;
//			ImageButton followImg;
//			TextView noOfPicsTxt;
			ImageView acceptImg;
			ImageView rejectImg;
			ProgressBar requesterimgPB;
		}

	}


}
