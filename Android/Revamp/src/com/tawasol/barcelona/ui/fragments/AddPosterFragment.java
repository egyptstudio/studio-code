package com.tawasol.barcelona.ui.fragments;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.AddPosterActivity;
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.utils.UIUtils;
import com.tawasol.barcelona.utils.Utils;

public class AddPosterFragment extends BaseFragment implements OnClickListener,OnSuccessVoidListener{

	private ImageView capturedImg;
	private TextView shopPrice;
	private Button continueBtn;
	private TextView rightSkipTxt;
	
	private String originalImgPath,maskedImgPath;
	private int privacyProgress;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_poster,
				container, false);
		
		
		viewByID(rootView);
		
		getBundleData();
		
		// set the captured image  
		capturedImg.setImageBitmap(BitmapFactory
				.decodeFile(maskedImgPath)); 
		
		// set the shop price text
		shopPrice.setText("2 $"); // TODO get the price from shop :)
		
		// set  skip text
		rightSkipTxt.setText(getResources().getString(R.string.AppEntry_Skip));
		
		// set on click listener of continue button and skip text
		continueBtn.setOnClickListener(this);
		rightSkipTxt.setOnClickListener(this);
		
		// set the on click listener of skip button 
		
		// call get countries WB
		UserManager.getInstance().getCountries();
		// call get my pics to hide or view "My Pics" button
		UserManager.getInstance().getMyPics(100);
		
	return rootView;
	}
	
	private void viewByID(View rootView)
	{
		capturedImg = (ImageView)rootView.findViewById(R.id.poster_captured_img);
		shopPrice = (TextView)rootView.findViewById(R.id.poster_price_points_txt);
		continueBtn = (Button)rootView.findViewById(R.id.continue_btn);
		rightSkipTxt = (TextView)getActivity().findViewById(R.id.rightBtn);
	}

	private void getBundleData()
	{
		originalImgPath = getActivity().getIntent().getStringExtra(AddPosterActivity.ORIGINAL_IMG_PATH);
		maskedImgPath = getActivity().getIntent().getStringExtra(AddPosterActivity.MASKED_IMG_PATH);
		privacyProgress = getActivity().getIntent().getIntExtra(AddPosterActivity.PRIVACY_PROGRESS_KEY, 0);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.continue_btn:
			showLoadingDialog();
			StudioManager.getInstance().sendPosterRequest(originalImgPath, maskedImgPath);
			break;
		case R.id.rightBtn:
			Utils.deleteFileNoThrow(originalImgPath);
				navigateToHome();
			break;
		
		default:
			break;
		}
	}
	
	
	
	public void navigateToHome() {
		switch (privacyProgress) {
		case 0: // ONLY Me ==> My Pics
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.share2_affirm1));
			startActivity(HomeActivity.getActivityIntent(getActivity(),
					FragmentInfo.MY_PIC , true));
			break;
		case 1: // Friends ==> My Pics
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.share2_affirm2));
			startActivity(HomeActivity.getActivityIntent(getActivity(),
					FragmentInfo.MY_PIC , true));
			break;
		case 2: // Public ==> Latest
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.share2_affirm3));
			startActivity(HomeActivity.getActivityIntent(getActivity(),
					FragmentInfo.LATEST_TAB , false));
			break;

		default:
			break;
		}
	}

	@Override
	public void onResume() {
		StudioManager.getInstance().addListener(this);
		super.onResume();
	}
	
	@Override
	public void onPause() {
		StudioManager.getInstance().removeListener(this);
		super.onPause();
	}
	
	
	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onSuccess() {
		hideLoadingDialog();
			navigateToHome();
	}
	

}
