package com.tawasol.barcelona.ui.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnUpdateProfileListener;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.Edit_More_Profile_Activity;
import com.tawasol.barcelona.ui.activities.UserProfileActivity;
import com.tawasol.barcelona.utils.UIUtils;

public class Edit_More_Profile_Fragment extends BaseFragment implements OnClickListener ,OnUpdateProfileListener{
	private static final int CASE_RELATIOSHIP =1;
	private static final int CASE_INCOME =2;
	private EditText statusTxt;
	private EditText aboutMeTxt;
	private TextView relationshipTxt;
	//private EditText religionTxt;
	private EditText educationTxt;
	private EditText jobTxt;
	private EditText companyTxt;
	private TextView incomeTxt;
	private TextView aboutMeCounter;
	private TextView doneTxt;
	private User user;
	private String[] relationshipList;
	private String[] incomeList;
	private ArrayAdapter<String> adapter;
	private boolean isRelationShipchanged =false,isIncomeChanged=false;
	private ImageView backImg;
	
	
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
	View rootView = inflater.inflate(R.layout.fragment_edit_more_about_me,
			container, false);
	ViewByID(rootView);
	populateUser();
	InitViewControls();
return rootView;
}

private void ViewByID(View rootView)
{
	statusTxt=(EditText)rootView.findViewById(R.id.edit_more_status_txt);
	aboutMeTxt=(EditText)rootView.findViewById(R.id.edit_more_about_me_txt);
	relationshipTxt=(TextView)rootView.findViewById(R.id.edit_more_relationship_txt);
	//religionTxt=(EditText)rootView.findViewById(R.id.edit_more_religion_txt);
	educationTxt=(EditText)rootView.findViewById(R.id.edit_more_education_txt);
	jobTxt=(EditText)rootView.findViewById(R.id.edit_more_job_txt);
	companyTxt=(EditText)rootView.findViewById(R.id.edit_more_copany_txt);
	incomeTxt=(TextView)rootView.findViewById(R.id.edit_more_income_txt);
	aboutMeCounter=(TextView)rootView.findViewById(R.id.edit_more_about_me_counter_txt);
	doneTxt = (TextView)getActivity().findViewById(R.id.rightBtn);
	
}

private void populateUser()
{
	this.user = (User) getActivity().getIntent().getExtras().getSerializable(Edit_More_Profile_Activity.USER_OBJ_KEY);
}

private void InitViewControls()
{
	// set the drawable of relationship
	relationshipTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_arrows, 0);
	// set the drawable of income
	incomeTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_arrows, 0);

	
	// set the right button
	doneTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.done, 0);
		
	
	
	statusTxt.setText((user.getStatus() != null && !user.getStatus().isEmpty()) ? user.getStatus() : null);
	aboutMeTxt.setText((user.getAboutMe() != null && !user.getAboutMe().isEmpty()) ? user.getAboutMe() : null);
	educationTxt.setText((user.getEducation() != null && !user.getEducation().isEmpty()) ? user.getEducation() : null);
	jobTxt.setText((user.getJob() != null && !user.getJob().isEmpty()) ? user.getJob() : null);
	companyTxt.setText((user.getCompany() != null && !user.getCompany().isEmpty()) ? user.getCompany(): null);
	//religionTxt.setText((user.getReligion()!= null && !user.getReligion().isEmpty())? user.getReligion() : "");
	
	// set the count of about me text 
	aboutMeCounter.setText((400 - aboutMeTxt.getText().toString().length())+"");
	
	// set the state of pickers ! 
	setRelationship(user.getRelationship());
	setIncome(user.getIncome());
	
	 // handle the counter of the about me text
    /*************** Chat Counter Text SetUp **************/
	aboutMeTxt.addTextChangedListener(new TextWatcher() {
		@Override
		public void afterTextChanged(Editable mEdit) {
			aboutMeCounter.setText((400 - mEdit.toString().length())+"");
		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			aboutMeCounter.setText((400 - aboutMeTxt.getText().toString().length())+"");
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}
	});
	/************************************************/
	
	
	// get the income and relationship lists
	incomeList = getResources().getStringArray(R.array.income_list);
	relationshipList = getResources().getStringArray(R.array.relationship_list);
	
	// set on click listeners
	statusTxt.setOnClickListener(this);
	relationshipTxt.setOnClickListener(this);
	//religionTxt.setOnClickListener(this);
	incomeTxt.setOnClickListener(this);
	doneTxt.setOnClickListener(this);
	
	textwatcherListeners();
}

/**  Initialize of List Dialog for country , city 
 * @param <D>**/
private <D> void showListDialog (final int state)
{
	
	// Initialization of dialog 
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.popup_list);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.GRAY));
		dialog.getWindow().setTitleColor(android.graphics.Color.WHITE);
		
		// set the title and list
		String[] stringsList = null ;
		if(state == CASE_RELATIOSHIP){
			stringsList = relationshipList;
			dialog.setTitle(getResources().getString(R.string.select_relationship_header));
		}
		else if(state == CASE_INCOME){
			stringsList = incomeList;
			dialog.setTitle(getResources().getString(R.string.select_income_header));
		}
		
		// initailize controls
		ListView listView = (ListView)dialog.findViewById(R.id.popup_list);
		// countries list and adapter settings
		adapter = new ArrayAdapter<String>(getActivity(),
			    R.layout.popup_list_item,stringsList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(state == CASE_RELATIOSHIP)
					relationshipTxt.setText(relationshipList[position]);
				else if(state == CASE_INCOME)
					incomeTxt.setText(incomeList[position]);
				dialog.dismiss();
			}
		});
		
		
		// show the dialog
		dialog.show();
				
		
}


//set the relationShipStatus
	private void setRelationship(int key)
	{
		if(key == User.RELATIONSHIP_MARRIED)
			relationshipTxt.setText(getString(R.string.relationship_list_item2));
		else if(key == User.RELATIONSHIP_ENGAGED)
			relationshipTxt.setText(getString(R.string.relationship_list_item3));
		else if(key == User.RELATIONSHIP_IN_RELATIONSHIP)
			relationshipTxt.setText(getString(R.string.relationship_list_item4));
		else /*if(key == User.RELATIONSHIP_SINGLE)*/
			relationshipTxt.setText(getString(R.string.relationship_list_item1));
//		else
//			relationshipTxt.setText(getResources().getString(R.string.MyProfile_Relationship));
	}
	//set the relationShipStatus
	private void setIncome(int key)
	{
		if(key == User.INCOME_LOW)
			incomeTxt.setText(getString(R.string.income_list_item1));
		else if(key == User.INCOME_HIGH)
			incomeTxt.setText(getString(R.string.income_list_item3));
		else /*if(key == User.INCOME_AVERAGE)*/
			incomeTxt.setText(getString(R.string.income_list_item2));
//		else
//			incomeTxt.setText(getResources().getString(R.string.initial_state_income));
	}
private int getRelationship()
{
	if(relationshipTxt.getText().toString().equals(getResources().getString(R.string.relationship_list_item2)))
		return User.RELATIONSHIP_MARRIED;
	else if(relationshipTxt.getText().toString().equals(getResources().getString(R.string.relationship_list_item3)))
		return User.RELATIONSHIP_ENGAGED;
	else if(relationshipTxt.getText().toString().equals(getResources().getString(R.string.relationship_list_item4)))
		return User.RELATIONSHIP_IN_RELATIONSHIP;
	else if(relationshipTxt.getText().toString().equals(getResources().getString(R.string.relationship_list_item1)))
		return User.RELATIONSHIP_SINGLE;
	else return 0;
}
private int getIncome()
{
	if(incomeTxt.getText().toString().equals(getResources().getString(R.string.income_list_item1)))
		return User.INCOME_LOW;
	else if(incomeTxt.getText().toString().equals(getResources().getString(R.string.income_list_item3)))
		return User.INCOME_HIGH;
	else if(incomeTxt.getText().toString().equals(getResources().getString(R.string.income_list_item2)))
		return User.INCOME_AVERAGE;
	else 
		return 0;
}

public User buildUser()
{
	this.user.setStatus(statusTxt.getText().toString());
	this.user.setAboutMe(aboutMeTxt.getText().toString());
	this.user.setEducation(educationTxt.getText().toString());
	this.user.setJob(jobTxt.getText().toString());
	this.user.setCompany(companyTxt.getText().toString());
	//this.user.setReligion(religionTxt.getText().toString());
	if(isRelationShipchanged)
	this.user.setRelationship(getRelationship());
	if(isIncomeChanged)
	this.user.setIncome(getIncome());
	return user;
}

@Override
public void onResume() {
	super.onResume();
	if(UserManager.getInstance().getCurrentUserId() !=0)
	UserManager.getInstance().addListener(this);
	else
		getActivity().finish();
}
@Override
public void onPause() {
	super.onPause();
	UserManager.getInstance().removeListener(this);
}

@Override
public void onClick(View v) {
	switch (v.getId()) {
	case R.id.rightBtn:
		showLoadingDialog();
		UserManager.getInstance().udateProfile(buildUser(),false,false);
		break;
		
	case R.id.edit_more_relationship_txt:
		isRelationShipchanged = true;
		showListDialog(CASE_RELATIOSHIP);
		break;
	case R.id.edit_more_income_txt:
		isIncomeChanged = true;
		showListDialog(CASE_INCOME);
		break;

	default:
		break;
	}

}

@Override
public void onSuccess(User obj) {
	hideLoadingDialog();
	UIUtils.showToast(getActivity(),"Profile Updated" );
	startActivity(UserProfileActivity.getActivityIntent(getActivity()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	//((Edit_More_Profile_Activity)getActivity()).onBackPressed();
	getActivity().finish();
}

@Override
public void onException(AppException ex) {
	hideLoadingDialog();
	UIUtils.showToast(getActivity(),ex.getMessage() );
	
}


private void textwatcherListeners()
{
	statusTxt.addTextChangedListener(new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(s.toString().startsWith(" "))
				statusTxt.setText("");
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}
		@Override
		public void afterTextChanged(Editable s) {
		}
	});
	aboutMeTxt.addTextChangedListener(new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(s.toString().startsWith(" "))
				aboutMeTxt.setText("");
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}
		@Override
		public void afterTextChanged(Editable s) {
		}
	});
	educationTxt.addTextChangedListener(new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(s.toString().startsWith(" "))
				educationTxt.setText("");
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}
		@Override
		public void afterTextChanged(Editable s) {
		}
	});
	companyTxt.addTextChangedListener(new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(s.toString().startsWith(" "))
				companyTxt.setText("");
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}
		@Override
		public void afterTextChanged(Editable s) {
		}
	});
}


InputFilter filter = new InputFilter() { 
    boolean canEnterSpace = false;

    public CharSequence filter(CharSequence source, int start, int end,
            Spanned dest, int dstart, int dend) {

       

        StringBuilder builder = new StringBuilder();

        for (int i = start; i < end; i++) { 
            char currentChar = source.charAt(i);

            if(i ==0)
            	canEnterSpace = false;
            
            if (!Character.isWhitespace(currentChar)/*Character.isLetterOrDigit(currentChar) || currentChar == '_'*/) {
                builder.append(currentChar);
                canEnterSpace = true;
            }

            if(Character.isWhitespace(currentChar) && canEnterSpace) {
                builder.append(currentChar);
                
            }


        }
        return builder.toString();          
    }

};


}
