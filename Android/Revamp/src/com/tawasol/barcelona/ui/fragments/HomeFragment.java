package com.tawasol.barcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.ui.activities.HomeActivity;

public class HomeFragment extends Fragment {

	private FragmentTabHost mTabHost;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home, container,
				false);

		initView(rootView);
		switchTabs();

		return rootView;
	}

	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);

		mTabHost.setup(getActivity(), getChildFragmentManager(),
				android.R.id.tabcontent);

		mTabHost.getTabWidget().setStripEnabled(false);
		FragmentInfo info;
		Bundle bundle = new Bundle();
		info = new FragmentInfo(FragmentInfo.LATEST_TAB,
				FragmentInfo.ACTION_FIRST_TIME);
		bundle.putSerializable(FragmentInfo.FRAGMENT_INFO_KEY, info);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
						.getString(R.string.Home_tab1)), R.drawable.left_tab),
				WallFragment.class, bundle);
		bundle = new Bundle();
		info = new FragmentInfo(FragmentInfo.TOP_TEN,
				FragmentInfo.ACTION_FIRST_TIME);
		bundle.putSerializable(FragmentInfo.FRAGMENT_INFO_KEY, info);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
						.getString(R.string.Home_tab2)), R.drawable.center_tab),
				TopTenFragment.class, bundle);
		bundle = new Bundle();
		info = new FragmentInfo(FragmentInfo.WALL,
				FragmentInfo.ACTION_FIRST_TIME);
		bundle.putSerializable(FragmentInfo.FRAGMENT_INFO_KEY, info);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
						.getString(R.string.Home_tab3)), R.drawable.center_tab),
				WallFragment.class, bundle);
		bundle = new Bundle();
		info = new FragmentInfo(FragmentInfo.MY_PIC,
				FragmentInfo.ACTION_FIRST_TIME);
		bundle.putSerializable(FragmentInfo.FRAGMENT_INFO_KEY, info);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
						.getString(R.string.Home_tab4)), R.drawable.right_tab),
				WallFragment.class, bundle);
		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// for (int i = 0; i < mTabHost.getTabWidget().getChildCount();
				// i++) {
				// TextView tv = (TextView) mTabHost.getCurrentTabView()
				// .findViewById(R.id.tab_text);
				// tv.setTextColor(Color.WHITE);
				//
				// }
				// //
				// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				//
				// TextView tv = (TextView)
				// mTabHost.getChildAt(mTabHost.getCurrentTab())
				// .findViewById(R.id.tab_text);
				// tv.setTextColor(Color.BLACK);

				for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
					TextView tv = (TextView) mTabHost.getTabWidget()
							.getChildAt(i).findViewById(R.id.tab_text);
					// .setBackgroundResource(R.drawable.tab); // unselected

					// TextView tv = (TextView) mTabHost.getCurrentTabView()
					// .findViewById(R.id.tab_text); // for Selected
					// Tab
					tv.setTextColor(getActivity().getResources().getColor(
							R.color.new_yellow));
				}
				TextView tv = (TextView) mTabHost.getTabWidget()
						.getChildAt(mTabHost.getCurrentTab())
						.findViewById(R.id.tab_text);
				// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				// .setBackgroundResource(R.drawable.tab_pressed); // selected
				// TextView tv = (TextView) mTabHost.getCurrentTabView()
				// .findViewById(android.R.id.title); // for Selected Tab
				tv.setTextColor(Color.WHITE);

//				WallFragment wall = (WallFragment) getChildFragmentManager()
//						.findFragmentByTag("Latest");
//				if (getActivity().getIntent().getExtras() == null)
//					wall.getToFirst();
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		// for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
		// TextView tv = (TextView) mTabHost.getCurrentTabView()
		// .findViewById(R.id.tab_text);
		// tv.setTextColor(Color.WHITE);
		//
		// }
		// // mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
		//
		// TextView tv = (TextView)
		// mTabHost.getChildAt(mTabHost.getCurrentTab())
		// .findViewById(R.id.tab_text);
		// tv.setTextColor(Color.BLACK);
		// .setBackgroundResource(R.drawable.tab_pressed);

		for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.tab_text);
			// .setBackgroundResource(R.drawable.tab); // unselected

			// TextView tv = (TextView) mTabHost.getCurrentTabView()
			// .findViewById(R.id.tab_text); // for Selected
			// Tab
			tv.setTextColor(getActivity().getResources().getColor(
					R.color.new_yellow));
//			tv.setTextSize(getActivity().getResources().getDimension(
//					R.dimen.smallTextSize));
		}
		TextView tv = (TextView) mTabHost.getTabWidget()
				.getChildAt(mTabHost.getCurrentTab())
				.findViewById(R.id.tab_text);
//		tv.setTextSize(getActivity().getResources().getDimension(
//				R.dimen.smallTextSize));
		// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
		// .setBackgroundResource(R.drawable.tab_pressed); // selected
		// TextView tv = (TextView) mTabHost.getCurrentTabView()
		// .findViewById(android.R.id.title); // for Selected Tab
		tv.setTextColor(Color.WHITE);
	}

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid) {
		// TODO Auto-generated method stub
		View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_host_text, null);
		v.setBackgroundResource(resid);
		// LinearLayout container =
		// (LinearLayout)v.findViewById(R.id.CUSTOMCONTAINER);
		// container.setBackgroundResource(resid);
		final TextView text = (TextView) v.findViewById(R.id.tab_text);
		text.setText(spec.getTag());
//		text.setTextSize(getActivity().getResources().getDimension(
//				R.dimen.smallTextSize));
		text.setTextColor(getActivity().getResources().getColor(
				android.R.color.white));
		text.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mTabHost.getCurrentTabTag().equalsIgnoreCase(
						text.getText().toString())) {
					Fragment wall = getChildFragmentManager()
							.findFragmentByTag(text.getText().toString());
					if (wall instanceof WallFragment) {
						((WallFragment) wall).getToFirst();
					} else if (wall instanceof TopTenFragment) {
						((TopTenFragment) wall).getToFirst();
					}
				} else {
					mTabHost.setCurrentTabByTag(text.getText().toString());
				}
			}
		});
		return spec.setIndicator(v);
	}

	private void switchTabs() {
		if (getActivity().getIntent().getExtras() != null) {
			int tab = getActivity().getIntent().getExtras()
					.getInt(HomeActivity.TAB_OBJ);
			if (tab != 0) {
				switch (tab) {
				case FragmentInfo.LATEST_TAB:
					mTabHost.setCurrentTab(0);
					break;
				case FragmentInfo.TOP_TEN:
					mTabHost.setCurrentTab(1);
					break;
				case FragmentInfo.WALL:
					mTabHost.setCurrentTab(2);
					break;
				case FragmentInfo.MY_PIC:
					mTabHost.setCurrentTab(3);
					break;

				default:
					break;
				}
			}
		}
	}
}
