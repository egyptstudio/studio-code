package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.commonsware.cwac.endless.EndlessAdapter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.customView.FanMaskImageView;
import com.tawasol.barcelona.entities.Fan;
import com.tawasol.barcelona.entities.FansFilterParams;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnFanListRecieved;
import com.tawasol.barcelona.managers.FanManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.FanProfileActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.TagListActivity;
import com.tawasol.barcelona.ui.activities.UserProfileActivity;

public class PremiumUsersFragment extends Fragment implements OnFanListRecieved {

	DisplayImageOptions options;
	protected AbsListView listView;
	List<Fan> fans;
	private ProgressBar loadingBar;
	ImageAdapter adapter;
	TextView noUsers;
	public static final int INTENT_RESULT_CODE = 58484;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.premium_fan_grid_view,
				container, false);
		List<Integer> countries = new ArrayList<Integer>();
		FanManager.getInstance().getFans(
				new FansFilterParams(UserManager.getInstance()
						.getCurrentUserId(), 0,
						FansFilterParams.FILTER_ANY_GENDER,
						FansFilterParams.FILTER_ONLINE_AND_OFFLINE,
						FansFilterParams.FILTER_FAVOURATE_OR_NOT,
						FansFilterParams.ANY_USER,
						countries, FansFilterParams.PREMIUM_USERS_ONLY,
						FansFilterParams.SORT_BY_LOCATION_AND_ONLINE, ""));
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.fan_no_image)
				.resetViewBeforeLoading(true)
				.showImageForEmptyUri(R.drawable.fan_no_image)
				.showImageOnFail(R.drawable.fan_no_image).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		initViews(rootView);
		// TODO fans = from manager
		return rootView;
	}

	private void initViews(View rootView) {
		loadingBar = (ProgressBar) rootView.findViewById(R.id.loadingBar);
		listView = (GridView) rootView.findViewById(R.id.fan_grid);
		TextView upgradeToPremium = (TextView) rootView
				.findViewById(R.id.become_premium_button);
		noUsers = (TextView) rootView.findViewById(R.id.no_premium_user);
		upgradeToPremium.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showToast("Upgrade");
			}
		});
		if (UserManager.getInstance().getCurrentUserId() != 0)
			if (UserManager.getInstance().getCurrentUser().isPremium())
				upgradeToPremium.setVisibility(View.GONE);
	}

	void showToast(String msg) {
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onResume() {
		FanManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
		FanManager.getInstance().removeListener(this);
		super.onPause();
	}

	private class ViewHolder {
		ImageView fanProfileImage;
		RelativeLayout fanState;
		ProgressBar progressBar;
		ImageButton onlineState;
		ImageButton follow;
		TextView fanName;
		TextView numOfPics;
	}

	public class ImageAdapter extends BaseAdapter {

		List<Fan> fans;
		ViewHolder holder;

		public ImageAdapter(List<Fan> fans) {
			this.fans = fans;
			holder = new ViewHolder();
		}

		@Override
		public int getCount() {
			return this.fans.size();
		}

		@Override
		public Object getItem(int position) {
			return this.fans.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		public View getRowFromList(AbsListView listView, int id) {
			return listView.getChildAt(id - listView.getFirstVisiblePosition());
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final Fan fan = this.fans.get(position);
			if (view == null) {
				view = getActivity().getLayoutInflater().inflate(
						R.layout.fan_list_item_modified, parent, false);
				holder = new ViewHolder();
				holder.numOfPics = (TextView) view
						.findViewById(R.id.num_of_pics);
				holder.fanProfileImage = (ImageView) view
						.findViewById(R.id.fan_profile_img_id);
				holder.progressBar = (ProgressBar) view
						.findViewById(R.id.progress);
				holder.onlineState = (ImageButton) view
						.findViewById(R.id.onlineState);
				holder.follow = (ImageButton) view
						.findViewById(R.id.followed_fan);
				holder.fanName = (TextView) view.findViewById(R.id.fanName);
				holder.fanState = (RelativeLayout) view
						.findViewById(R.id.fan_profile_frame);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}
			if (fan.isFanOnline())
				holder.onlineState.setVisibility(View.VISIBLE);
			else
				holder.onlineState.setVisibility(View.GONE);

			if (fan.isPremium())
				holder.fanState
						.setBackgroundResource(R.drawable.premium_frame);
			else
				holder.fanState
						.setBackgroundResource(R.drawable.basic_frame);
			if (fan.isFollowed())
				holder.follow.setImageResource(R.drawable.followed);
			else
				holder.follow.setImageResource(R.drawable.unfollowed);// TODO
																		// unfollow
																		// image
			holder.follow.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						if (fan.isFollowed()) {
							((ImageButton) getRowFromList(listView, position)
									.findViewById(R.id.followed_fan))
									.setImageResource(R.drawable.unfollowed);
							FanManager.getInstance().handleFollow(
									UserManager.getInstance()
											.getCurrentUserId(),
									fan.getFanID(), false, getActivity());
						} else {
							((ImageButton) getRowFromList(listView, position)
									.findViewById(R.id.followed_fan))
									.setImageResource(R.drawable.followed);
							FanManager.getInstance().handleFollow(
									UserManager.getInstance()
											.getCurrentUserId(),
									fan.getFanID(), true, getActivity());
						}
					} else {
						getActivity().startActivityForResult(
								LogInActivity.getActivityIntent(getActivity(),
										UserManager.LOGIN_FOR_RESULT,
										new Intent()), INTENT_RESULT_CODE);
					}
				}
			});
			holder.fanName.setText(fan.getFanName());
			holder.numOfPics.setText(String.valueOf(fan.getFanPicNumber()));
			holder.numOfPics.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					startActivity(TagListActivity.getIntent(getActivity(), 0,
							null, fan.getFanID(), fan.getFanName(), false));
				}
			});
//			holder.favorute.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					if (UserManager.getInstance().getCurrentUserId() != 0) {
//						if (fan.isFavorite()) {
//							((ImageButton) getRowFromList(listView, position)
//									.findViewById(R.id.favourate_button))
//									.setImageResource(R.drawable.unfavourate);
//							FanManager.getInstance().handleFavo(
//									UserManager.getInstance()
//											.getCurrentUserId(),
//									fan.getFanID(), false);
//						} else {
//							((ImageButton) getRowFromList(listView, position)
//									.findViewById(R.id.favourate_button))
//									.setImageResource(R.drawable.favourate);
//							FanManager.getInstance().handleFavo(
//									UserManager.getInstance()
//											.getCurrentUserId(),
//									fan.getFanID(), true);
//						}
//					} else {
//						getActivity().startActivityForResult(
//								LogInActivity.getActivityIntent(getActivity(),
//										UserManager.LOGIN_FOR_RESULT,
//										new Intent()), INTENT_RESULT_CODE);
//					}
//
//				}
//			});

			display(holder.fanProfileImage, fan.getFanPicture(),
					holder.progressBar);
			holder.fanProfileImage
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (fan.getFanID() == UserManager.getInstance()
									.getCurrentUserId())
								startActivity(UserProfileActivity
										.getActivityIntent(getActivity()));
							else
								startActivity(FanProfileActivity.getIntent(
										getActivity(), fan.getFanID(),
										fan.getFanName()));
						}
					});

			return view;
		}

		public void addToList(List<Fan> tempList) {
			this.fans.addAll(tempList);
		}
	}

	public void display(ImageView img, String url, final ProgressBar spinner) {
		App.getInstance().getImageLoader()
				.displayImage(url, img, options);/*, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						spinner.setVisibility(View.VISIBLE);
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						spinner.setVisibility(View.GONE);

					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						spinner.setVisibility(View.GONE);
					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {

					}

				});*/
	}

	private class updatePosts extends EndlessAdapter {
		List<Fan> tempList;

		public updatePosts(Context context, ImageAdapter wrapped,
				int pendingResource) {
			super(context, wrapped, pendingResource);
		}

		@Override
		protected void appendCachedData() {
			if (getWrappedAdapter() != null)
				((ImageAdapter) getWrappedAdapter()).addToList(tempList);
			tempList = null;
		}

		@Override
		protected boolean cacheInBackground() throws Exception {
			List<Integer> countries = new ArrayList<Integer>();
			tempList = FanManager.getInstance().getFansFromServer(
					new FansFilterParams(UserManager.getInstance()
							.getCurrentUserId(), adapter.getCount(),
							FansFilterParams.FILTER_ANY_GENDER,
							FansFilterParams.FILTER_ONLINE_AND_OFFLINE,
							FansFilterParams.FILTER_FAVOURATE_OR_NOT,
							FansFilterParams.ANY_USER,
							countries, FansFilterParams.PREMIUM_USERS_ONLY,
							FansFilterParams.SORT_BY_LOCATION_AND_ONLINE,
							""));
			return !tempList.isEmpty();
		}
	}

	@Override
	public void onSuccess(List<Fan> objs) {
		loadingBar.setVisibility(View.INVISIBLE);
		if (objs != null && !objs.isEmpty()) {
			noUsers.setVisibility(View.GONE);
			adapter = new ImageAdapter(objs);
			((GridView) listView).setAdapter(new updatePosts(getActivity(),
					adapter, R.layout.progress_bar));
		} else {
			noUsers.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onException(AppException ex) {
		// TODO Auto-generated method stub

	}
}