package com.tawasol.barcelona.ui.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.StudioPhotosTable;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.StudioFolder;
import com.tawasol.barcelona.entities.StudioPhoto;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnStudioPhotosReceivedListener;
import com.tawasol.barcelona.listeners.OnUserCreditReceived;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.CaptureActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.ShopActivityTabs;
import com.tawasol.barcelona.ui.activities.StudioPhotosActivity;
import com.tawasol.barcelona.ui.adapters.StudioPhotosAdapter;
import com.tawasol.barcelona.utils.UIUtils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class StudioPhotosFragment extends BaseFragment implements
		OnStudioPhotosReceivedListener, OnScrollListener, OnItemClickListener,
		OnUserCreditReceived {

	private final static int USE = 100;
	private final static int UPGRADE_TO_PREMIUM = 101;
	private final static int BUY_POINTS = 102;
	private final static int STUDIO_PHOTOS_INTENT_REQUEST_CODE = 103;
	private static final int ACTION_REQUEST_LOGIN = 104;
	
	GridView purchasedPhotosGV;
	List<StudioPhoto> photos;
	StudioPhotosAdapter adapter;
	private static int pageNo = 1;
	StudioFolder folder;
	private boolean userScrolled;
	Dialog dialog;
	ViewPager photosPager;
	PhotoPagerAdapter photospagerAdapter;
	ImageView imageView;
	LinearLayout initialStateLayout,noPhotosLayout;
	Button loginBtn;
	String UserCredit = "-1";
	TextView userCreditTxt;
	private boolean gettinMoreData = true;
	private ProgressBar creditPB;

	private static StudioPhotosFragment studioPhotosFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_purchased_photos,
				container, false);
		pageNo = 1;

		getBundleData();
		initView(rootView);

		
		return rootView;
	}

	private void getBundleData() {
		try {
			folder = (StudioFolder) getActivity().getIntent().getExtras()
					.getSerializable(StudioPhotosActivity.STUDIO_FOLDER_KEY);
		} catch (Exception e) {
			folder = null;
		}
	}

	private void initView(View rootView) {
		showLoadingDialog();

		// get the grid view
		purchasedPhotosGV = (GridView) rootView
				.findViewById(R.id.purchased_photos_gridview);
		//purchasedPhotosGV.setPadding(0, 0, 0, BaseActivity.bottomBarHeight);
		// get the initial state layout and login button
		initialStateLayout = (LinearLayout) rootView
				.findViewById(R.id.purchased_photos_initail_state_id);
		noPhotosLayout = (LinearLayout)rootView
				.findViewById(R.id.no_photos_layout);
		loginBtn = (Button) rootView
				.findViewById(R.id.purchased_photos_login_btn);

		// initialize the list
		photos = new ArrayList<StudioPhoto>();
		
//		purchasedPhotosGV.setPadding(0, 0, 0,
//				((BaseActivity) getActivity()).getBottomBarHeight());

		// initialize the adapter
		adapter = new StudioPhotosAdapter(getActivity(),
				(ArrayList<StudioPhoto>) photos);
		purchasedPhotosGV.setAdapter(adapter);

		purchasedPhotosGV.setOnItemClickListener(this);

		pageNo=1;
		
		if (folder != null)
			StudioManager.getInstance().getStudioPhotos(folder.getFolderId(),
					pageNo);
		else {
			if (UserManager.getInstance().getCurrentUserId() != 0) // if logged
																	// in
				StudioManager.getInstance().getPurchasedPhotos(pageNo);
			else {
				hideLoadingDialog();
				initialStateLayout.setVisibility(View.VISIBLE);
				loginBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						startActivity(LogInActivity.getActivityIntent(
								getActivity(), UserManager.NORMAL_LOGIN, null));
					}
				});
			}
		}

	}

	public static StudioPhotosFragment newInstance() {
		if (studioPhotosFragment == null)
			studioPhotosFragment = new StudioPhotosFragment();
		return studioPhotosFragment;
	}

	private void ShowPhotosPopUp(final int Selected_Position) {
		if (dialog != null && dialog.isShowing())
			return;
		// custom dialog
		dialog = new Dialog(getActivity(), R.style.FullHeightDialog);
		dialog.setContentView(R.layout.photos_dialog);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		// get and set user the credit text
		userCreditTxt = (TextView) dialog.findViewById(R.id.usercredit_txt);
		creditPB = (ProgressBar)dialog.findViewById(R.id.user_credit_progress_bar);
		
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			if (this.UserCredit != null && !this.UserCredit.equals("-1"))
				userCreditTxt.setText(getString(R.string.photoUse_unock_credit)
						+ " "
						+ this.UserCredit +" "
						+ getString(R.string.studio_xp));
			creditPB.setVisibility(View.GONE);
			/*else
				userCreditTxt.setText(UserManager.getInstance()
						.getCurrentUser().getCredit()
						+ "\n " + getString(R.string.studio_xp));*/
		} else{
			userCreditTxt.setVisibility(View.GONE);
			creditPB.setVisibility(View.GONE);
		}

		// get and set the photo points
		final TextView photoPoints = (TextView) dialog
				.findViewById(R.id.photo_points_txt);

		// define the view pager
		photosPager = (ViewPager) dialog.findViewById(R.id.photo_pager_id);
		// define the pervious btn
		ImageButton prevBtn = (ImageButton) dialog
				.findViewById(R.id.photos_previous_img_id);
		// define the next btn
		ImageButton nextBtn = (ImageButton) dialog
				.findViewById(R.id.photos_next_img_id);
		// define the user button
		final Button useBtn = (Button) dialog
				.findViewById(R.id.photos_use_btn_id);

		// setting adapter of the pager
		photospagerAdapter = new PhotoPagerAdapter();
		photosPager.setAdapter(photospagerAdapter);
		// set the on pagescroll listener to detect the last swiped image
		OnPageChangeListener mListener = new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// check if standing at the last item , then call web service to
				// get more pictures
				photoPoints.setText(photos.get(photosPager.getCurrentItem())
								.getRequiredPoints() + " "
						+ getString(R.string.studio_xp));
				// get the selected photo
				StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				if (canUsePhoto(selectedPhoto) == USE) // USE
				{
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Use));
					useBtn.setTag(USE);
				} else {
					if (canUsePhoto(selectedPhoto) == UPGRADE_TO_PREMIUM)
						useBtn.setTag(UPGRADE_TO_PREMIUM);
					else if (canUsePhoto(selectedPhoto) == BUY_POINTS)
						useBtn.setTag(BUY_POINTS);

					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
				}

				if (photosPager.getCurrentItem() == photos.size() - 1
						&& !gettinMoreData && pageNo != -1) {
					gettinMoreData = true;
					if (folder != null)
						StudioManager.getInstance().getStudioPhotos(
								photos.get(photosPager.getCurrentItem())
										.getFolderId(), pageNo);
					else
						StudioManager.getInstance().getPurchasedPhotos(pageNo);
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		};
		photosPager.setOnPageChangeListener(mListener);

		// set the position of pager with the selected item
		photosPager.setCurrentItem(Selected_Position);

		// setting the click listener of previous button
		prevBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				photoPoints.setText(photos.get(photosPager.getCurrentItem())
								.getRequiredPoints() + " "
						+ getString(R.string.studio_xp));

				// get the selected photo
				StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				if (canUsePhoto(selectedPhoto) == USE) // USE
				{
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Use));
					useBtn.setTag(USE);
				} else if (canUsePhoto(selectedPhoto) == UPGRADE_TO_PREMIUM) {
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
					useBtn.setTag(UPGRADE_TO_PREMIUM);
				} else if (canUsePhoto(selectedPhoto) == BUY_POINTS) {
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
					useBtn.setTag(BUY_POINTS);
				}
				photosPager.setCurrentItem(photosPager.getCurrentItem() - 1,
						true);
				// Toast.makeText(getActivity(),
				// photosPager.getCurrentItem()-1+"",
				// Toast.LENGTH_SHORT).show();

			}
		});
		// setting the click listener of next button
		nextBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				photoPoints.setText(photos.get(photosPager.getCurrentItem())
								.getRequiredPoints() + " "
						+ getString(R.string.studio_xp));

				// get the selected photo
				StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				if (canUsePhoto(selectedPhoto) == USE) // USE
				{
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Use));
					useBtn.setTag(USE);
				} else if (canUsePhoto(selectedPhoto) == UPGRADE_TO_PREMIUM) {
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
					useBtn.setTag(UPGRADE_TO_PREMIUM);
				} else if (canUsePhoto(selectedPhoto) == BUY_POINTS) {
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
					useBtn.setTag(BUY_POINTS);
				}

				photosPager.setCurrentItem(photosPager.getCurrentItem() + 1,
						true);
				// check if standing at the last item , then call web service to
				// get more pictures
				if (photosPager.getCurrentItem() == photos.size() - 1
						&& !gettinMoreData && pageNo != -1) {
					gettinMoreData = true;
					if (folder != null)
						StudioManager.getInstance().getStudioPhotos(
								photos.get(photosPager.getCurrentItem())
										.getFolderId(), pageNo);
					else
						StudioManager.getInstance().getPurchasedPhotos(pageNo);
				}

			}
		});

		// setting the listener of Use button
		useBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				if(UserManager.getInstance().getCurrentUserId() !=0){
				

				// TODO first check user , unlock ! :)

				if ((Integer) useBtn.getTag() == USE) {
					// get the selected photo

					// get the bitmap of the image

					App.getInstance()
							.getImageLoader()
							.loadImage(selectedPhoto.getPhotoURL(),
									new SimpleImageLoadingListener() {

										@Override
										public void onLoadingComplete(
												String imageUri, View view,
												Bitmap loadedImage) {
											// save the Stream to device
											super.onLoadingComplete(imageUri,
													view, loadedImage);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											loadedImage.compress(
													Bitmap.CompressFormat.PNG,
													60, stream);

											try {
												boolean single = false;
												if(selectedPhoto.getCaptureType() == StudioPhoto.PHOTO_CAPTURE_TYPE_MASK_SHOT) // 1 --> rotatable a5er klam isA :D
													single = true;
												else if(selectedPhoto.getCaptureType() == StudioPhoto.PHOTO_CAPTURE_TYPE_SCREEN_SHOT) // 2 -- not rotatable 
													single = false;
													
												String path = saveToInternalSorage(loadedImage);
												startActivity(CaptureActivity
														.getActivityIntent(
																getActivity(),
																path,
																selectedPhoto,
																single));
//selectedPhoto.getFolderId() == StudioFolder.FOLDER_TYPE_PLAYER ? true: false
												
												// TODO call update user credit
												// .. to be discussed with Hamed
												// !
												// TODO call purchase photo WB
												if(selectedPhoto.getIsPurchased() != 1){
												 StudioManager.getInstance().purchasePhoto(selectedPhoto.getStudioPhotoId());
												 StudioPhotosTable.getInstance().setPurchased(selectedPhoto.getStudioPhotoId());
												}

											}

											catch (Exception e) {
											}
										}
									});
					
					/** --- Added by Turki --- **/
					StudioManager.getInstance().updateUserCredit(-1*selectedPhoto.getRequiredPoints());
				    /** ---                --- **/
				} else if ((Integer) useBtn.getTag() == UPGRADE_TO_PREMIUM) // UNLOCK
																			// (Upgrade
																			// To
																			// Premium)
				{
					/*UIUtils.showToast(getActivity(),
							"navigate to shop to upgrade to premium");*/
					startActivity(ShopActivityTabs.getIntent(getActivity()));

					// TODO Redirect to Shop Screen
					/*startActivityForResult(
							new Intent(getActivity(),
									PremiumPackagesActivity.class).putExtra(
									Params.Studio.FOLDER_ID,
									folder.getFolderId()).putExtra(
									Params.Studio.STUDIO_PHOTO_ID,
									selectedPhoto.getStudioPhotoId()),
							STUDIO_PHOTOS_INTENT_REQUEST_CODE);*/
				} else if ((Integer) useBtn.getTag() == BUY_POINTS) // UNLOCK
																	// (Buy
																	// Points)
				{
					/*UIUtils.showToast(getActivity(),
							"navigate to shop to buy points");*/
					startActivity(ShopActivityTabs.getIntent(getActivity()));

					// TODO Redirect to Shop Screen
					/*startActivityForResult(
							ShopActivity
									.getIntent(getActivity(), false)
									.putExtra(Params.Studio.FOLDER_ID,
											folder.getFolderId())
									.putExtra(Params.Studio.STUDIO_PHOTO_ID,
											selectedPhoto.getStudioPhotoId()),
							STUDIO_PHOTOS_INTENT_REQUEST_CODE);*/

				}
			}
				else
				{
					if(folder != null)
					getActivity().startActivityForResult(LogInActivity.getActivityIntent(getActivity(),
							UserManager.LOGIN_FOR_RESULT,
							new Intent().putExtra(Params.Studio.FOLDER_ID, folder.getFolderId())
							//.putExtra(Params.Studio.STUDIO_PHOTO_ID, selectedPhoto.getStudioPhotoId())
							),
							ACTION_REQUEST_LOGIN);
				}
		}
		});
		

		

		// dismiss the dialog button
		ImageButton CloseBtn = (ImageButton) dialog
				.findViewById(R.id.photos_close_img_id);
		CloseBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

	}

	/** Check the Photo usability Status */
	private int canUsePhoto(StudioPhoto selectedPhoto) {

		//if (UserManager.getInstance().getCurrentUserId() != 0) // logged in
		//{
		if(selectedPhoto.getIsPurchased() != 1){
			User currentUser = UserManager.getInstance().getCurrentUser();

			if (selectedPhoto.getIsPermium() == 1) // if Photo Premium
			{
				if (currentUser.isPremium()) // if User is Premium
				{
					return doesUserHasEnoughPoints(currentUser, selectedPhoto);
				} else // Not Premium User
				{
					// TODO Navigate to Shop to upgrade to premium
					return UPGRADE_TO_PREMIUM;
				}
			} else // Photo is not premium
			{
				 return doesUserHasEnoughPoints(currentUser, selectedPhoto);
			}
		}
		else
			return USE;
//		} else
//			// not logged in
//			return USE;
	//	return USE;

	}
private int doesUserHasEnoughPoints(User currentUser, StudioPhoto selectedPhoto )
{


	if (currentUser.getCredit() >= selectedPhoto.getRequiredPoints() && currentUser.getCredit() > 0 /*&& selectedPhoto.getRequiredPoints() >0*/) // User
																		// has
																		// enough
																		// points
	{
		// TODO set "Use" Button and navigate to Capture :)
		return USE;
	} else // No Enough Credit
	{
		// TODO Navigate to Shop to buy points
		return BUY_POINTS;
	}


}
	

	// save to internal storage
	private String saveToInternalSorage(Bitmap bitmapImage) {
		ContextWrapper cw = new ContextWrapper(getActivity());
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		// Create imageDir
		File mypath = new File(directory, "image.jpg");

		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(mypath);

			// Use the compress method on the BitMap object to write image to
			// the OutputStream
			bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return directory.getAbsolutePath();
	}

	/** to Handle the result returned form Shop */
	/*@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK
				&& ( requestCode == STUDIO_PHOTOS_INTENT_REQUEST_CODE || requestCode == ACTION_REQUEST_LOGIN)) {

			if(dialog != null)
				dialog.dismiss();
			int folderId = data.getIntExtra(Params.Studio.FOLDER_ID, 0);

			//int studioPhotoId = data.getIntExtra(Params.Studio.STUDIO_PHOTO_ID, 0);

			if (folderId != 0) {
				// call get user credit
					UserManager.getInstance().getUserCredit();


				*//** get the photos list *//*
				// StudioManager.getInstance().getStudioPhotos(folderId, 1);
				photos = new ArrayList<StudioPhoto>();
				photos.clear();
				photos = StudioManager.getInstance().getCahcedPhotos(folderId);
				adapter.notifyDataSetChanged();

				// get the photo
				int photoPosition = photoPosition(studioPhotoId);
				if (photoPosition != -1)
					ShowPhotosPopUp(photoPosition);

			}

		}

	}*/

	private int photoPosition(int studioPhotoId) {
		if (!(photos == null || photos.isEmpty())) {
			for (int i = 0; i < photos.size(); i++)
				if (photos.get(i).getStudioPhotoId() == studioPhotoId)
					return i;

		} else
			return -1;
		return -1;
	}

	private class PhotoPagerAdapter extends PagerAdapter {

		// ArrayList<StudioPhoto> images;
		private LayoutInflater inflater;

		PhotoPagerAdapter() {
			// this.images = images;
			inflater = getActivity().getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return photos.size();
		}

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			View imageLayout = null;

			if (imageLayout == null) {
				imageLayout = inflater.inflate(R.layout.photo_popup_item, view,
						false);
			}
			final ProgressBar bar = (ProgressBar) imageLayout
					.findViewById(R.id.progressBar1);
			imageView = (ImageView) imageLayout.findViewById(R.id.imageView1);
			App.getInstance()
					.getImageLoader()
					.displayImage(photos.get(position).getPhotoURL(),
							imageView, App.getInstance().getDisplayOption(),
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingStarted(String imageUri,
										View view) {
									bar.setVisibility(View.VISIBLE);
									System.out.println("Loading");
								}

								@Override
								public void onLoadingFailed(String imageUri,
										View view, FailReason failReason) {
									String message = null;
									switch (failReason.getType()) {
									case IO_ERROR:
										message = "Input/Output error";
										break;
									case DECODING_ERROR:
										message = "Image can't be decoded";
										break;
									case NETWORK_DENIED:
										message = "Downloads are denied";
										break;
									case OUT_OF_MEMORY:
										message = "Out Of Memory error";
										break;
									case UNKNOWN:
										message = "Unknown error";
										break;
									}
									try{
										if(message != null)
											UIUtils.showToast(getActivity(), message);
//										else 
//											UIUtils.showToast(getActivity(), message);
									}
									catch (Exception e){
										
									}

									bar.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingComplete(String imageUri,
										View view, Bitmap loadedImage) {
									bar.setVisibility(View.GONE);
								}
							});

			view.addView(imageLayout);

			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if(UserManager.getInstance().getCurrentUserId() != 0 )
		ShowPhotosPopUp(position);
		else {
			startActivity(LogInActivity.getActivityIntent(getActivity(), UserManager.LOGIN_WITH_INTENT,StudioPhotosActivity.getActivityIntent(getActivity(), folder)));
		getActivity().finish();
		}
			/*getActivity().startActivityForResult(LogInActivity.getActivityIntent(getActivity(),
					UserManager.LOGIN_FOR_RESULT,
					new Intent().putExtra(Params.Studio.FOLDER_ID, folder.getFolderId())
					//.putExtra(Params.Studio.STUDIO_PHOTO_ID, selectedPhoto.getStudioPhotoId())
					),
					ACTION_REQUEST_LOGIN);*/
	}

	@Override
	public void onResume() {
		super.onResume();
		StudioManager.getInstance().addListener(this);
		UserManager.getInstance().addListener(this);
		pageNo = 1;
		
		// call get user credit
				if (UserManager.getInstance().getCurrentUserId() != 0)
					UserManager.getInstance().getUserCredit();

	}

	@Override
	public void onPause() {

		super.onPause();
		StudioManager.getInstance().removeListener(this);
		UserManager.getInstance().removeListener(this);
	}

	@Override
	public void onSuccess(List<StudioPhoto> objs) {
		hideLoadingDialog();
		gettinMoreData = false;
		if (objs != null) {
			if(pageNo == 1)
				photos.clear();
			this.photos.addAll(objs);
			adapter.notifyDataSetChanged();
			if (photospagerAdapter != null)
				photospagerAdapter.notifyDataSetChanged();

			// check if there is no more data set page no to -1 , else if there
			// was more data in server increment the page no
			if (objs.size() < 24)
				pageNo = -1;
			else
				pageNo++;
		}
		if(folder == null) // purchased
		{
			if(photos != null && ! photos.isEmpty())
			{
				noPhotosLayout.setVisibility(View.GONE);
				initialStateLayout.setVisibility(View.GONE);
				purchasedPhotosGV.setVisibility(View.VISIBLE);
			}
			else
			{

				noPhotosLayout.setVisibility(View.VISIBLE);
				initialStateLayout.setVisibility(View.GONE);
				purchasedPhotosGV.setVisibility(View.GONE);
			
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if (scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
			userScrolled = true;
		}

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		int lastInScreen = firstVisibleItem + visibleItemCount;
		if ((lastInScreen == photos.size()) && userScrolled && pageNo != -1) {
			if (!gettinMoreData) {
				gettinMoreData = true;
				showLoadingDialog();
				StudioManager.getInstance().getPurchasedPhotos(pageNo);
			}
		}

	}

	@Override
	public void onSuccess(String obj) {
		this.UserCredit = obj;
		if(creditPB != null){
		creditPB.setVisibility(View.GONE);
		
		if (userCreditTxt != null) {
			if (this.UserCredit != null)
				userCreditTxt.setText(getString(R.string.photoUse_unock_credit)
						+ " "
						+ this.UserCredit +" "
						+ getString(R.string.studio_xp));
			else
				userCreditTxt.setText(getString(R.string.photoUse_unock_credit)
						+ " "
						+ UserManager.getInstance()
						.getCurrentUser().getCredit() +" "
					    + getString(R.string.studio_xp));
		}}
	}

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		gettinMoreData = false;
		UIUtils.showToast(getActivity(), ex.getMessage());
		pageNo = -1;

		if(folder == null && (photos == null || photos.isEmpty())){
		noPhotosLayout.setVisibility(View.VISIBLE);
		initialStateLayout.setVisibility(View.GONE);
		purchasedPhotosGV.setVisibility(View.GONE);
		}
	
	}

}
