package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.facebook.Session.NewPermissionsRequest;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.Seasons;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.ui.activities.StudioFilterResultActivity;
import com.tawasol.barcelona.ui.activities.StudioPhotosActivity;
import com.tawasol.barcelona.ui.adapters.SeasonAdapter;
import com.tawasol.barcelona.utils.UIUtils;

public class FoldersFilterFragment extends BaseFragment implements OnClickListener {

	GridView seasonsGV;
	ProgressBar seasonsPB;
	RadioGroup sortRG;
	Button applyBtn;
	TextView noSeasonsTxt;
	ArrayList<Integer> SelectedSeasonsFilter = new ArrayList<Integer>();
	int filterBy = 0;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.folders_filter_fragment,
				container, false);
		
		initView(rootView);
		
		return rootView;
	}
	
	private void initView(View rootView)
	{
		 seasonsGV = (GridView) rootView.findViewById(R.id.photos_filter_season_list_id);
		 seasonsPB = (ProgressBar) rootView.findViewById(R.id.filter_season_dialog_progress_bar_id);
		 sortRG = (RadioGroup)rootView.findViewById(R.id.sort_folders_radio_group);
		 noSeasonsTxt = (TextView)rootView.findViewById(R.id.no_available_seasons_txt);
		 applyBtn = (Button)rootView.findViewById(R.id.folders_filter_apply_btn_id);
		seasonsGV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				CheckBox clickedCheckBox=(CheckBox)view.findViewById(R.id.season_checkbox);
				Seasons clickedseason = (Seasons)StudioManager.getInstance().getSeasonList().get(position);
				if(clickedCheckBox.isChecked())
				{
					clickedCheckBox.setChecked(false);
					if(SelectedSeasonsFilter.contains(clickedseason.getSeasonId()))
					{
						SelectedSeasonsFilter.remove(SelectedSeasonsFilter.indexOf(clickedseason.getSeasonId()));
							//remove(clickedcountry.getCountryId());
					}
				}
				else
				{
					clickedCheckBox.setChecked(true);
					SelectedSeasonsFilter.add((Integer) clickedseason.getSeasonId());
				}
				
			
			
				
			}
		});
		
		
		
		// radio group
		
		if(StudioManager.getInstance().getSeasonList()!=null)
		{
			if(StudioManager.getInstance().getSeasonList().size() !=0){
		seasonsPB.setVisibility(View.INVISIBLE);
		SeasonAdapter seasonsAdapter = new SeasonAdapter(getActivity(), StudioManager.getInstance().getSeasonList());
		seasonsGV.setAdapter(seasonsAdapter);
		seasonsGV.setVisibility(View.VISIBLE);
			}
			else
			{
			noSeasonsTxt.setVisibility(View.VISIBLE);
			seasonsPB.setVisibility(View.INVISIBLE);
			seasonsGV.setVisibility(View.INVISIBLE);
			}
		}
		
		applyBtn.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.folders_filter_apply_btn_id:
			int checkedSortBtn = sortRG.getCheckedRadioButtonId();
			if (checkedSortBtn == R.id.sort_latest_radiobtn)
				filterBy = 1;
			if (checkedSortBtn == R.id.sort_most_used_radio)
				filterBy = 2;
			startActivity(StudioFilterResultActivity.getActivityIntent(getActivity(), filterBy, SelectedSeasonsFilter));
			break;

		default:
			break;
		}
		
	}
}
