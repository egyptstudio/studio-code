package com.tawasol.barcelona.ui.fragments;
  
import java.util.ArrayList;
 
import com.tawasol.barcelona.R; 
import com.tawasol.barcelona.entities.SystemMessagesEntity;
import com.tawasol.barcelona.ui.adapters.SystemMessagesAdapter;
import com.tawasol.barcelona.utils.UIUtils;  
 
import android.os.Bundle; 
import android.view.LayoutInflater;
import android.view.View; 
import android.view.View.OnClickListener;
import android.view.ViewGroup; 
import android.widget.ListView;
import android.widget.RelativeLayout; 

/**
 * @author TURKI
 *  
 */
public class SystemMessagesFragment extends BaseFragment {

	ListView listView;
	SystemMessagesAdapter adapter;
	ArrayList<SystemMessagesEntity> list = new ArrayList<SystemMessagesEntity>();
	RelativeLayout deleteBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_system_messages, container, false);
		
		/** Initialize registration view **/
		init(rootView);

		return rootView;
	}

	/** Initialize registration view **/
	private void init(View rootView) {
		deleteBtn = (RelativeLayout) getActivity().findViewById(R.id.deleteRightBtnLayout);
		deleteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				getCheckedItem();
			}
		});
		listView  = (ListView) rootView.findViewById(R.id.system_msgs_list);
		adapter   = new SystemMessagesAdapter(getActivity(), list);
		listView.setAdapter(adapter);
				
	} 
	
	 public void getCheckedItem() {
		for (SystemMessagesEntity p : adapter.getCheckedSystemMessagesEntity()) {
		   if (p.isChecked()){
			 UIUtils.showToast(getActivity(), p.getMessageText());
		   }
		}
	}
}
