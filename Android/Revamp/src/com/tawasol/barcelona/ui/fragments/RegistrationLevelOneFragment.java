package com.tawasol.barcelona.ui.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.soundcloud.android.crop.Crop;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.customView.MaskedImageView;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.FCBPhoto;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.exception.NullIntentException;
import com.tawasol.barcelona.listeners.OnFCBPhotosResponseListener;
import com.tawasol.barcelona.listeners.OnLoginResponseListener;
import com.tawasol.barcelona.listeners.OnRegisterResponseListener;
import com.tawasol.barcelona.listeners.OnUpdateProfileListener;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.ui.activities.FCBPhotoActivity;
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.ui.activities.InviterActivity;
import com.tawasol.barcelona.ui.activities.RegistrationActivity;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.LocationUtil;
import com.tawasol.barcelona.utils.NetworkingUtils;
import com.tawasol.barcelona.utils.UIUtils;
import com.tawasol.barcelona.utils.Utils;
import com.tawasol.barcelona.utils.ValidatorUtils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * @author TURKI Registration Level One
 */
public class RegistrationLevelOneFragment extends BaseFragment implements
		OnClickListener, OnRegisterResponseListener,
		OnFCBPhotosResponseListener, OnLoginResponseListener, OnUpdateProfileListener {

	private EditText fullNameTxt, passwordTxt, confirmPasswordTxt, emailTxt;
	private String fullName, password, confirmPassword, email;
	private RelativeLayout loadingLayout;
	private ImageView profileImg;
	private static Uri cropPhotoURI;
	private boolean UPLOAD_IMAGE = false;
	private boolean GOTO_FCB_PHOTO_FRAGMENT = false;
	private String photoName;
	private ArrayList<FCBPhoto> fcbPhotoList = new ArrayList<FCBPhoto>();
	private static int FCB_IMAGE = 0;
	private static User socialMediaUser;
	private View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_registeration, container,
				false);

		/** Initialize registration view **/
		init(rootView);

		/** Set random profile picture **/
		// if(socialMediaUser == null)
		// setRandomImage();

		return rootView;
	}

	public static Fragment newFragment() {
		return new RegistrationLevelOneFragment();
	}

	/** Build user object **/
	private User buildUser() {

		/** Initialize user **/
		User user = new User();
		user.setFullName(fullName);
		user.setPassword(password);
		user.setEmail(email);
		if (UPLOAD_IMAGE)
			user.setProfilePic(cropPhotoURI.getPath());
		else
			user.setProfilePic(photoName);
		return user;
	}

	/** Handle OnClickListeners **/
	@Override
	public void onClick(View v) {
		/** Get view values **/
		getValues();

		switch (v.getId()) {

		case R.id.register_btn:
			/**
			 * Check if in Social Media Mode , then validate data and call
			 * LoginSocialMedia WB
			 **/
			if (socialMediaUser != null) {
				if (isValidSocialMediaFields()) {
					socialMediaUser.setEmail(emailTxt.getText().toString());
					socialMediaUser.setFullName(fullNameTxt.getText()
							.toString());
					UserManager.getInstance().loginWithSocialMedia(
							(BaseActivity) getActivity(), socialMediaUser,
							User.USER_TYPE_APP);
				}
			}
			/** Check if the data valid, then send it to register web service. **/
			else {
				if (ValidatorUtils.isRequired(photoName)
						&& fcbPhotoList.size() < 1) {
					setRandomImage();
				} else if (ValidatorUtils.isRequired(photoName)
						&& fcbPhotoList.size() > 0) {
					generateRandom(fcbPhotoList);
				} else if (!ValidatorUtils.isRequired(photoName)
						&& checkRegistrationField()) {
					User user = buildUser();
					UserManager.getInstance().register(
							(BaseActivity) getActivity(), user,
							LanguageUtils.getInstance().getDeviceLanguage(),
							UPLOAD_IMAGE);
					showLoadingDialog();
				}
			}
			break;

		case R.id.registeration_profile_img_id:
			showPhotoListDialog();
			break;
		}
	}

	/** Initialize registration view **/
	private void init(View rootView) {

		/** Initialize of image mask **/
		profileImg = (ImageView) rootView
				.findViewById(R.id.registeration_profile_img_id);
		profileImg.setImageDrawable(getResources().getDrawable(
				R.drawable.image_sample));
		profileImg.setOnClickListener(this);

		/** Initialize loading panel **/
		loadingLayout = (RelativeLayout) rootView
				.findViewById(R.id.loadingPanel);

		/** Initialize editText **/
		fullNameTxt = (EditText) rootView.findViewById(R.id.user_name_txt);
		passwordTxt = (EditText) rootView.findViewById(R.id.password_txt);
		confirmPasswordTxt = (EditText) rootView
				.findViewById(R.id.confirm_password_txt);
		emailTxt = (EditText) rootView.findViewById(R.id.email_txt);

		/** Initialize of register button **/
		Button registerActionBtn = (Button) rootView
				.findViewById(R.id.register_btn);
		registerActionBtn.setOnClickListener(this);

		if (socialMediaUser != null)
			handleSocialMediaMode();
		
		// handle white spaces of edit texts 
		// added by Mohga 
		textFilters();
	}

	/** Get registration fields value **/
	private void getValues() {
		/** Get all values from user **/
		fullName = fullNameTxt.getText().toString();
		password = passwordTxt.getText().toString();
		confirmPassword = confirmPasswordTxt.getText().toString();
		email = emailTxt.getText().toString();
	}

	/** Set random profile picture from returned FCB photos list **/
	private void setRandomImage() {
		FCB_IMAGE = Params.UPLOAD_USER_PHOTO.FCB_RANDOM_IMAGE;
		UserManager.getInstance().callFCBPhoto(
				LanguageUtils.getInstance().getDeviceLanguage(), false);
		showLoadingDialog();
	}

	/** Validate registration fields **/
	private boolean checkRegistrationField() {

		/**
		 * Validate all fields to not be empty & password isEqualTo
		 * confirmPassword Password at least 5 char Email must be ***@***.***
		 * Username must be in alphabetic
		 **/
		if (ValidatorUtils.isRequired(fullName)
				|| ValidatorUtils.isRequired(password)
				|| ValidatorUtils.isRequired(confirmPassword)
				|| ValidatorUtils.isRequired(email)) {
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.reg_fill_data_validation));
			return false;
		} else if (!UIUtils.isFullNameAlphabtic(fullName)) {
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.reg_full_name_validation));
			return false;
		} else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email)
				.matches()) {
			UIUtils.showToast(
					getActivity(),
					getResources().getString(
							R.string.Registration_IncorrectEmailFormatAlert));
			return false;
		} else if (password.length() < 5) {
			UIUtils.showToast(
					getActivity(),
					getResources().getString(
							R.string.Registration_passwordLengthAlert));
			return false;
		} else if (!password.equals(confirmPassword) || password.equals("")) {
			UIUtils.showToast(
					getActivity(),
					getResources().getString(
							R.string.ForgotPassword_passwordAlert3));
			return false;
		} else {
			return true;
		}
	}

	/** Validate registration fields **/
	private boolean isValidSocialMediaFields() {
		if (ValidatorUtils.isRequired(fullNameTxt.getText().toString())) {
			UIUtils.showToast(getActivity(),
					getString(R.string.username_required));
			return false;
		} else if (ValidatorUtils.isRequired(emailTxt.getText().toString())) {
			UIUtils.showToast(getActivity(),
					getString(R.string.Login_emailMissingAlert));
			return false;
		} else if (!ValidatorUtils.isValidEmail(emailTxt.getText().toString())) {
			UIUtils.showToast(getActivity(),
					getString(R.string.email_formate_error));
			return false;
		} else
			return true;
	}

	/** Initialize of PhotoList Dialog **/
	private void showPhotoListDialog() {

		/** Initialize of dialog **/
		final Dialog dialog = new Dialog(getActivity());

		/** remove dialog title **/
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		/** Include dialog.xml file **/
		dialog.setContentView(R.layout.photo_list_dialog);

		/** set dialog background transparent to use my own view instead **/
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		/** set dialog gravity **/
		dialog.getWindow().setGravity(Gravity.BOTTOM);

		/** Call pickImage method from Crop library to get image from camera **/
		Button cameraBtn = (Button) dialog.findViewById(R.id.cameraBtn);
		cameraBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Crop.pickImageFromCamera(getActivity());
			}
		});

		/** Call pickImage method from Crop library to get image from gallery **/
		Button photoAlbumBtn = (Button) dialog.findViewById(R.id.photoAlbumBtn);
		photoAlbumBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Crop.pickImage(getActivity());
			}
		});

		/**
		 * Call FCBPhoto web service to get FCB photos, then display it in
		 * FCBPhoto fragment
		 **/
		Button FCBPhotosBtn = (Button) dialog.findViewById(R.id.FCBPhotosBtn);
		FCBPhotosBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (fcbPhotoList.size() > 0 && GOTO_FCB_PHOTO_FRAGMENT) {
					goToFCBPhotoFragment(fcbPhotoList);
				} else {
					FCB_IMAGE = Params.UPLOAD_USER_PHOTO.FCB_SELECT_IMAGE;
					UserManager.getInstance().callFCBPhoto(
							LanguageUtils.getInstance().getDeviceLanguage(),
							false);
					showLoadingDialog();
				}
			}
		});

		/** Dismiss dialog on Cancel button pressed **/
		Button cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
		cancelBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});

		/** Show dialog **/
		dialog.show();
	}

	/** GoTo FCBPhotoFragment with FCB photos list **/
	private void goToFCBPhotoFragment(ArrayList<FCBPhoto> list) {
		/**
		 * Put FCB photos in list, then start FCBPhotoFragment to chose profile
		 * photo
		 **/
		Intent intent = FCBPhotoActivity.getActivityIntent(getActivity());
		Bundle bundle = new Bundle();
		bundle.putSerializable(Params.UPLOAD_USER_PHOTO.FCB_LIST_NAME,
				(Serializable) list);
		intent.putExtra(Params.UPLOAD_USER_PHOTO.FCB_BUNDLE, bundle);
		getActivity().startActivityForResult(intent,
				FCBPhotoActivity.REQUEST_FCB_PHOTO);
	}

	/** Check if request pick photo or request crop photo */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent result) {

		if (requestCode == Crop.REQUEST_PICK
				&& resultCode == Activity.RESULT_OK) {
			if (result.getData() != null)
				beginCrop(result.getData());
			else {
				Bitmap returnedBMP = (Bitmap) result.getExtras().get("data");
				beginCrop(getImageUri(returnedBMP));
			}

		} else if (requestCode == Crop.REQUEST_CROP) {
			handleCrop(resultCode, result);

		} else if (requestCode == FCBPhotoActivity.REQUEST_FCB_PHOTO
				&& result != null) {

			Bundle bundle = result.getExtras();
			FCBPhoto fcbPhotoModel = (FCBPhoto) bundle
					.getSerializable(Params.UPLOAD_USER_PHOTO.FCB_PHOTO_OBJECT);

			String url = fcbPhotoModel.getUrl();
			photoName = fcbPhotoModel.getPhotoName();
			profileImg.setTag(url);

			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				DownloadImagesTask task = new DownloadImagesTask();
				task.execute(profileImg);
			} else {
				UIUtils.showToast(
						getActivity(),
						getResources().getString(
								R.string.connection_noConnection));
			}
		} else if (requestCode == InviterActivity.REQUEST_INVITE_FRIEND
				&& result != null) {
			boolean inviteFlag = result.getBooleanExtra(
					Params.SMS_INVITATION.INVITATION_FLAG, false);
			if (inviteFlag)
				proceedNextScreen(RegistrationActivity.typeId,
						RegistrationActivity.returnedIntent);
		}
	}

	/** Get the URI path of Bitmap Image **/
	public Uri getImageUri(Bitmap inImage) {
		String path = Images.Media.insertImage(getActivity()
				.getContentResolver(), inImage, "tempImage", null);
		return Uri.parse(path);
	}

	/** Add Registration, FCBPhotos and UploadUserPhoto listeners onResume **/
	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	/** Remove Registration, FCBPhotos and UploadUserPhoto listeners onPause **/
	@Override
	public void onPause() {
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}

	/**
	 * Set typeID & intent that coming from previous activity to handle
	 * proceeding to the next screen
	 */
	public void populateLoginArgument(int typeID, Intent intent, User user) {
		RegistrationActivity.typeId = typeID;
		RegistrationActivity.returnedIntent = intent;
		socialMediaUser = user;
	}

	private void handleSocialMediaMode() {

		// hide the Password and confirm password fields
		passwordTxt.setVisibility(View.GONE);
		confirmPasswordTxt.setVisibility(View.GONE);

		// make the profile image not editable
		profileImg.setClickable(false);

		// set the data in fields (Full name field ,Profile Picture )
		fullNameTxt.setText(socialMediaUser.getFullName());
		profileImg.setTag(socialMediaUser.getProfilePic());

		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			DownloadImagesTask task = new DownloadImagesTask();
			task.execute(profileImg);
		}
		emailTxt.requestFocus();
		// App.getInstance().getImageLoader().displayImage(socialMediaUser.getProfilePic(),
		// profileImg);
	}

	/** Begin crop process */
	private void beginCrop(Uri source) {
		if (cropPhotoURI != null) {
			Utils.deleteFileNoThrow(cropPhotoURI.getPath());
		}

		ContextWrapper contextWrapper = new ContextWrapper(getActivity());

		// path to /data/data/yourapp/app_data/imageDir
		File directory = contextWrapper
				.getDir("imageDir", Context.MODE_PRIVATE);

		// Create imageDir
		File mypath = new File(directory, "tempImage");

		Uri outputUri = Uri.fromFile(mypath);
		cropPhotoURI = outputUri;

		new Crop(source).output(outputUri).asSquare().start(getActivity());
	}

	/**
	 * 
	 * @param resultCode
	 * @param result
	 *            Handle crop process and set the masked image with result
	 */
	private void handleCrop(int resultCode, Intent result) {
		if (resultCode == Activity.RESULT_OK) {
			Bitmap bmp = BitmapFactory.decodeFile(cropPhotoURI.getPath());
			Bitmap rotatedBMP = setOrientation();
			if (rotatedBMP != null) {
				saveToInternalSorageBitmap(rotatedBMP, "tempImage");
				profileImg.setImageBitmap(rotatedBMP);
				// profileImg.setBitmap(rotatedBMP);
				// saveToInternalSorageBitmap(rotatedBMP, "tempImage");
			} else {
				Bitmap decodedBMP = null;
				try {
					decodedBMP = BitmapFactory.decodeStream(
							new FileInputStream(
									new File(cropPhotoURI.getPath())), null,
							null);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (decodedBMP != null)
					profileImg.setImageBitmap(decodedBMP);
			}
			UPLOAD_IMAGE = true;
		} else if (resultCode == Crop.RESULT_ERROR) {
			UIUtils.showToast(getActivity(), Crop.getError(result).getMessage());
		}
	}

	private void generateRandom(List<FCBPhoto> list) {
		/** Set profile picture with random FCBPhoto url **/
		int postion = Utils.randomNumber(list.size());
		profileImg.setTag(list.get(postion).getUrl());
		photoName = list.get(postion).getPhotoName();

		if (checkRegistrationField() && !ValidatorUtils.isRequired(photoName)) {
			User user = buildUser();
			UserManager.getInstance().register((BaseActivity) getActivity(),
					user, LanguageUtils.getInstance().getDeviceLanguage(),
					UPLOAD_IMAGE);
			showLoadingDialog();
		}
	}

	/**
	 * @param loginType
	 * @param intent
	 *            Three levels of registration :- 1- Normal register, will go to
	 *            HomeActivity after register successfully. 2- Intent register,
	 *            will go to specific intent after register successfully. 3-
	 *            Intent With Action register, will will go to specific intent
	 *            and perform specific action after register successfully.
	 */
	public void proceedNextScreen(int loginType, Intent intent) {

		switch (loginType) {

		/** Normal Register **/
		case UserManager.NORMAL_LOGIN:
			startActivity(HomeActivity.getActivityIntent(getActivity())/*
																		 * new
																		 * Intent
																		 * (
																		 * getActivity
																		 * (),
																		 * HomeActivity
																		 * .
																		 * class
																		 * )
																		 */);
			break;

		/** Intent Register **/
		case UserManager.LOGIN_WITH_INTENT:
			if (intent != null) {
				startActivity(intent);
			} else
				throw new NullIntentException();

			/** Intent With Action Register **/
		case UserManager.LOGIN_FOR_RESULT:
			if (intent != null) {
				getActivity().setResult(Activity.RESULT_OK,
						new Intent().putExtras(intent));/*
														 * new
														 * Intent().putExtras
														 * (intent.getExtras)
														 * 22/1/2015
														 */
			} else
				throw new NullIntentException();
		default:
			getActivity().finish();
			break;
		}
	}

	/** Convert URL of image to bitmap, then set it as profile picture */
	public class DownloadImagesTask extends AsyncTask<ImageView, Void, Bitmap> {

		ImageView imageView = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			loadingLayout.setVisibility(View.VISIBLE);
		}

		@Override
		protected Bitmap doInBackground(ImageView... imageViews) {
			this.imageView = imageViews[0];
			return download_Image((String) imageView.getTag());
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			if (result != null) {
				loadingLayout.setVisibility(View.GONE);
				imageView.setImageBitmap(result);
				UPLOAD_IMAGE = false;
			} else
				DisplayImage();
		}

		private void DisplayImage() {
			App.getInstance()
					.getImageLoader()
					.displayImage((String) imageView.getTag(), this.imageView,
							App.getInstance().getDisplayOption(),
							new ImageLoadingListener() {

								@Override
								public void onLoadingStarted(String arg0,
										View arg1) {

								}

								@Override
								public void onLoadingFailed(String arg0,
										View arg1, FailReason arg2) {
									loadingLayout.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingComplete(String arg0,
										View arg1, Bitmap arg2) {
									loadingLayout.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingCancelled(String arg0,
										View arg1) {

								}
							});
		}

		private Bitmap download_Image(String url) {
			Bitmap bmp = null;
			try {
				URL ulrn = new URL(url);
				HttpURLConnection con = (HttpURLConnection) ulrn
						.openConnection();
				InputStream is = con.getInputStream();
				bmp = BitmapFactory.decodeStream(is);
				if (null != bmp)
					return bmp;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return bmp;
		}
	}

	/** Handle any exception in FCB-Photo or Registration */
	@Override
	public void onException(AppException ex) {
		// TODO Auto-generated method stub
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	/** Handle FCBPhoto response */
	@Override
	public void onSuccess(List<FCBPhoto> list) {
		// TODO Auto-generated method stub
		hideLoadingDialog();
		if (list.size() > 0
				&& FCB_IMAGE == Params.UPLOAD_USER_PHOTO.FCB_RANDOM_IMAGE) {
			/**
			 * Check if list > 0, if true set flag"GOTO_FCB_PHOTO_FRAGMENT"
			 * equal true then proceed to FCBPhotosFragment
			 **/
			GOTO_FCB_PHOTO_FRAGMENT = true;
			fcbPhotoList = (ArrayList<FCBPhoto>) list;

			/** Set profile picture with random FCBPhoto url **/
			generateRandom(list);
			// int postion = Utils.randomNumber(list.size());
			// profileImg.setTag(list.get(postion).getUrl());
			// photoName = list.get(postion).getPhotoName();
			//
			// if (checkRegistrationField() &&
			// !ValidatorUtils.isRequired(photoName)) {
			// User user = buildUser();
			// UserManager.getInstance().register(user,
			// LanguageUtils.getInstance().getDeviceLanguage(), UPLOAD_IMAGE);
			// showLoadingDialog();
			// }
			// /** Execute AsyncTask to download image from url and set the
			// profile image with returned bitmap **/
			// if(NetworkingUtils.isNetworkConnected(getActivity())){
			// DownloadImagesTask task = new DownloadImagesTask();
			// task.execute(profileImg);
			// }else{
			// UIUtils.showToast(getActivity(),getResources().getString(R.string.reg_no_internet));
			// }
		} else if (list.size() > 0
				&& FCB_IMAGE == Params.UPLOAD_USER_PHOTO.FCB_SELECT_IMAGE) {
			GOTO_FCB_PHOTO_FRAGMENT = true;
			fcbPhotoList = (ArrayList<FCBPhoto>) list;

			goToFCBPhotoFragment(fcbPhotoList);
		} else if (FCB_IMAGE == Params.UPLOAD_USER_PHOTO.FCB_SET_RANDOM_IMAGE_BEFORE_REGISTER) {

		}
	}

	/** Handle registration response */
	@Override
	public void onSuccess() {
		// TODO Auto-generated method stub
		hideLoadingDialog();
		/** Show congratulation message after register successfully **/
		int userID = UserManager.getInstance().getCurrentUser().getUserId();
		UIUtils.showToast(getActivity(),
				getResources().getString(R.string.Registration_Congratulations));

		// Added by Mohga
		// fire notification service
//		App.getInstance().startNotificationService();

		/** GOTO invite friends to download the app **/
//		Intent intent = InviterActivity.getActivityIntent(getActivity());
//		getActivity().startActivityForResult(intent,
//				InviterActivity.REQUEST_INVITE_FRIEND);
		
		
		LocationUtil loc = new LocationUtil(getActivity());
		List<Address> addresses;
		if(loc.getLocation() != null){
//			UIUtils.showToast(getActivity(), "Your Location is - \nLat: " + loc.getLatitude() 
//					+ "\nLong: " + loc.getLongitude());
			Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
			try {
				addresses = geoCoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
				String country  = addresses.get(0).getCountryName();
				String city     = addresses.get(0).getLocality();
				String state    = addresses.get(0).getAdminArea();
//				UIUtils.showToast(getActivity(), "Country "+country+" city "+city+" state "+state);
				User user = UserManager.getInstance().getCurrentUser();
				user.setCountryName(country);
				user.setDistrict(state);
				user.setCityName(city);
				
				user.setLatitude(String.valueOf(loc.getLatitude()));
				user.setLongitude(String.valueOf(loc.getLongitude()));
				UserManager.getInstance().udateProfile(user, false, false);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			if(RegistrationActivity.returnedIntent != null)
				proceedNextScreen(RegistrationActivity.typeId, 
						RegistrationActivity.returnedIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			else
				proceedNextScreen(RegistrationActivity.typeId, RegistrationActivity.returnedIntent);			
		}
	}

	/** Add red-color to confirm password layout if not match */
	public void reflectedEditTextColor(EditText editText, boolean match) {
		if (match) {
			editText.setBackgroundResource(R.drawable.not_matched_password_field);
			editText.setTextColor(Color.RED);
		} else {
			editText.setBackgroundResource(R.drawable.input_text);
			editText.setTextColor(Color.BLACK);
		}
	}

	/** handle On Login Success of Social Media */
	@Override
	public void onLoginSuccess(User user, int LoginState) {
		UserManager.getInstance().cacheUser(user);
		UIUtils.showToast(getActivity(), "Hello   " + user.getFullName());
		proceedNextScreen(RegistrationActivity.typeId,
				RegistrationActivity.returnedIntent);
		// getActivity().finish();
	}

	/** Do Nothing for this Response */
	@Override
	public void onLoginWithSocialMediaNavigation(User user) {
	}

	private Bitmap setOrientation() {
		Bitmap correctBmp = null;
		try {
			File file = new File(cropPhotoURI.getPath());
			ExifInterface exif = new ExifInterface(file.getPath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			int angle = 0;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
				angle = 90;
			} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
				angle = 180;
			} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
				angle = 270;
			}
			Matrix matrix = new Matrix();
			matrix.postRotate(angle);
			Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(file),
					null, null);
			correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
					bmp.getHeight(), matrix, true);
		} catch (IOException e) {
			Log.w("TAG", "-- Error in setting image");
		} catch (OutOfMemoryError oom) {
			Log.w("TAG", "-- OOM Error in setting image");
		}
		return correctBmp;
	}

	// save to internal storage
	private String saveToInternalSorageBitmap(Bitmap bitmapImage,
			String fileName) {
		// preview.setDrawingCacheEnabled(false);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmapImage.compress(CompressFormat.JPEG, 100, bos);
		byte[] bitmapdata = bos.toByteArray();

		ContextWrapper cw = new ContextWrapper(getActivity());
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		// Create imageDir
		File mypath = new File(directory, fileName);
		if (mypath.exists())
			mypath.delete();
		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(mypath);
			fos.write(bitmapdata);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		cropPhotoURI = Uri.fromFile(mypath);
		return directory.getAbsolutePath();
	}

	/** to handle the validation of white spaces for entered text */
	private void textFilters() {
		fullNameTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					fullNameTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		emailTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					emailTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		passwordTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					passwordTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		confirmPasswordTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					confirmPasswordTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

	}

	@Override
	public void onSuccess(User obj) {
		UserManager.getInstance().cacheUser(obj);
		if(RegistrationActivity.returnedIntent != null)
			proceedNextScreen(RegistrationActivity.typeId, 
					RegistrationActivity.returnedIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		else
			proceedNextScreen(RegistrationActivity.typeId, RegistrationActivity.returnedIntent);
	}
}
