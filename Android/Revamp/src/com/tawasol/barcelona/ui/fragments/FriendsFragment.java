package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.data.cache.FriendTable;
import com.tawasol.barcelona.entities.FriendsEntity;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnBackgroundTask;
import com.tawasol.barcelona.listeners.OnChatContactUpdatedListener;
import com.tawasol.barcelona.listeners.OnChatFriendsResponseListener;
import com.tawasol.barcelona.listeners.OnVerifyPhoneListener;
import com.tawasol.barcelona.managers.ChatManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.pushnotifications.BackgroundTask;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.ui.activities.ChatInviteActivity;
import com.tawasol.barcelona.ui.activities.VerifyPhoneActivity;
import com.tawasol.barcelona.ui.adapters.FriendsChatAdapter;
import com.tawasol.barcelona.utils.UIUtils;

/**
 * 
 * @author Turki
 *
 */
@SuppressLint("NewApi")
public class FriendsFragment extends BaseFragment implements
		OnChatFriendsResponseListener, OnChatContactUpdatedListener,
		SearchView.OnQueryTextListener, SearchView.OnCloseListener,
		OnVerifyPhoneListener {

	private GridView friendsGrid;
	private List<FriendsEntity> friendsList = new ArrayList<FriendsEntity>();
	private FriendsChatAdapter adapter;
	private boolean NoMoreFriendToLoading = true;
	private RelativeLayout loadingPanel;
	private boolean isLoading;
	private View verificationView;
	private Button verifyButton;
	private Button help;
	private Button inviteBtn;
	private SearchView mSearchView;
	private FrameLayout noFriendsMsgFrame;
	public static int mLastStartingLimit = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_friends, container,
				false);
		friendsList.clear();
		mLastStartingLimit = 0;
		/** Initialize registration view **/
		init(rootView);

		return rootView;
	}

	@SuppressLint("NewApi")
	private void init(View rootView) {
		loadingPanel = (RelativeLayout) rootView
				.findViewById(R.id.loadingPanel);
		friendsGrid = (GridView) rootView
				.findViewById(R.id.chat_friends_grid_view);

		noFriendsMsgFrame = (FrameLayout) rootView
				.findViewById(R.id.initial_no_friends_msg);
		inviteBtn = (Button) rootView.findViewById(R.id.invite_btn);
		if (!ChatManager.getInstance().isInviteFriendBefor()) {
			noFriendsMsgFrame.setVisibility(View.VISIBLE);
			friendsGrid.setVisibility(View.GONE);
		}
		inviteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getActivity().startActivity(
						ChatInviteActivity.getActivityIntent(getActivity()));
			}
		});

		adapter = new FriendsChatAdapter(getActivity(), friendsList,
				getActivity().getClass().getSimpleName()
						.equalsIgnoreCase("FriendsList") ? false : true);
		friendsGrid.setAdapter(adapter);
		friendsGrid.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (adapter == null) {
					return;
				}

				// if(adapter.getCount() == 0)
				// return;

				int l = visibleItemCount + firstVisibleItem;

				if (l >= totalItemCount && !isLoading && NoMoreFriendToLoading) {
					// set progress boolean
					isLoading = true;
					loadingPanel.setVisibility(View.VISIBLE);
					ChatManager.getInstance().callLoadingFriends();
				}
			}
		});

		/** For phone verification **/
		verificationView = rootView.findViewById(R.id.pop_up_phone_verify);
		verificationView.setVisibility(View.GONE);
		verifyButton = (Button) rootView.findViewById(R.id.verifytypebutton);
		help = (Button) rootView.findViewById(R.id.helpButton);
		handleVerficationView(verificationView);

		mSearchView = (SearchView) getActivity().findViewById(
				R.id.chat_search_friends);
		mSearchView.setOnSearchClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((BaseActivity) getActivity()).hideTitleBar();
				((BaseActivity) getActivity()).hideChatInviteFriends();
			}
		});
		((BaseActivity) getActivity()).getSearchBtn().setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						mSearchView.setIconified(false);
						((BaseActivity) getActivity()).hideChatSearchBtn();
					}
				});

		setupSearchView();
	}

	private void setupSearchView() {
		mSearchView.setOnQueryTextListener(this);
		mSearchView.setOnCloseListener(this);
		mSearchView.setQueryHint(" ");
	}

	/** Add ChatFriends listeners onResume **/
	@Override
	public void onResume() {
		showLoadingDialog();
		if (friendsList != null)
			friendsList.clear();
		if (adapter != null)
			adapter.notifyDataSetChanged();

		ChatManager.getInstance().addListener(this);
		if (FriendTable.getInstance().getCount() != 0)
			ChatManager.getInstance().loadCachedContactsAsync();
		else {
			if (!isLoading) {
				isLoading = true;
				loadingPanel.setVisibility(View.VISIBLE);
				ChatManager.getInstance().callLoadingFriends();
			}
		}
		super.onResume();
	}

	/** Remove ChatFriends listeners onPause **/
	@Override
	public void onPause() {
		ChatManager.getInstance().removeListener(this);
		super.onPause();
	}

	@Override
	public void onSuccess(List<FriendsEntity> friendsFilterEntity) {
		hideLoadingDialog();
		loadingPanel.setVisibility(View.INVISIBLE);
		isLoading = false;
		if (friendsFilterEntity != null && friendsFilterEntity.size() > 0) {
			noFriendsMsgFrame.setVisibility(View.GONE);
			if (mLastStartingLimit == 0) {
				friendsList.clear();
				saveFriendsToDatabase(friendsFilterEntity, true);
			} else
				saveFriendsToDatabase(friendsFilterEntity, false);

			mLastStartingLimit += friendsFilterEntity.size();
			friendsList.addAll(friendsFilterEntity);
			Collections.sort(friendsList);
			adapter.notifyDataSetChanged();
			friendsGrid.setVisibility(View.VISIBLE);
		} else {
			NoMoreFriendToLoading = false;
			if (friendsList != null && friendsList.isEmpty()) {
				noFriendsMsgFrame.setVisibility(View.VISIBLE);
				friendsGrid.setVisibility(View.GONE);
			}
		}

	}

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		isLoading = false;
		NoMoreFriendToLoading = false;
		loadingPanel.setVisibility(View.INVISIBLE);
		// if(ex.getErrorCode() == AppException.NO_DATA_EXCEPTION && friendsList
		// != null && friendsList.isEmpty()){
		// noFriendsMsgFrame.setVisibility(View.VISIBLE);
		// friendsGrid.setVisibility(View.GONE);
		// }else{
		UIUtils.showToast(getActivity(), ex.getMessage());
		// }
	}

	@Override
	public void onSuccess() {
		hideLoadingDialog();
		if (!isLoading)
			loadingPanel.setVisibility(View.INVISIBLE);
		List<FriendsEntity> list = ChatManager.getInstance().getContacts();
		if (list.size() > 0) {
			noFriendsMsgFrame.setVisibility(View.GONE);
			friendsList.clear();
			friendsList.addAll(list);
			Collections.sort(friendsList);
			adapter.notifyDataSetChanged();
		}
		// ChatManager.getInstance().callLoadingFriends();
	}

	private void handleVerficationView(View view) {
		if (!UserManager.getInstance().getCurrentUser().isPhoneVerified())
			view.setVisibility(View.GONE);
		else
			view.setVisibility(View.GONE);
		verifyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UserManager.getInstance().callVerifyPhone();
			}
		});
		help.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showHelpDialog(v);
			}
		});
	}

	/** Initialize of help PopUp **/
	private void showHelpDialog(View view) {
		final View popupView = LayoutInflater.from(getActivity()).inflate(
				R.layout.chat_help_dialog, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.setOutsideTouchable(true);
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(view, 0, 0/* , Gravity.LEFT */);
	}

	@Override
	public boolean onClose() {
		((BaseActivity) getActivity()).showTitleBar();
		((BaseActivity) getActivity()).showChatInviteFriends();
		((BaseActivity) getActivity()).showChatSearchBtn();
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		adapter.getFilter().filter(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	private void saveFriendsToDatabase(final List<FriendsEntity> friends,
			final boolean clearFriendDB) {
		OnBackgroundTask backgroundTaskListener = new OnBackgroundTask() {

			@Override
			public void onTaskCompleted() {
				// TODO Auto-generated method stub
				FriendTable.getInstance().insert(friends);
			}

			@Override
			public void doTaskInBackground() {
				// TODO Auto-generated method stub
				if (clearFriendDB) {
					FriendTable.getInstance().deleteAll();
				}
			}
		};
		new BackgroundTask(backgroundTaskListener).execute();
	}

	@Override
	public void onSuccess(User userPhoneVerified) {
		getActivity().startActivity(
				VerifyPhoneActivity.getActivityIntent(getActivity()));
	}
}