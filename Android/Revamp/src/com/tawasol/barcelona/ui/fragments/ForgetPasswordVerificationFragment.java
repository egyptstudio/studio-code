package com.tawasol.barcelona.ui.fragments;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.exception.NullIntentException;
import com.tawasol.barcelona.listeners.OnSendVerificationListener;
import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.ForgetPassowrdVerificationActivity;
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.UIUtils;
import com.tawasol.barcelona.utils.ValidatorUtils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author Mohga
 * */

public class ForgetPasswordVerificationFragment extends BaseFragment implements
		OnSuccessVoidListener,OnSendVerificationListener, OnClickListener {

	/* Declarations */
	private Button sendBtn, resendEmailBtn;
	private EditText verificationTxt, newPasswordTxt, confirmPasswordTxt;
	private String UserId , Email;
	int navigationTypeId;
	Intent intentAction;

	/* End Of Declarations */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_forget_password_verification, container,
				false);
		ViewByID(rootView);
		initailizeViewControls();
		getIntentData();

		return rootView;
	}

	/** Initializing Controls from XML file ! */
	private void ViewByID(View rootView) {
		sendBtn = (Button) rootView.findViewById(R.id.send_new_password_btn_id);
		resendEmailBtn = (Button) rootView
				.findViewById(R.id.resend_email_btn_id);
		verificationTxt = (EditText) rootView
				.findViewById(R.id.forget_password_verification_txt);
		newPasswordTxt = (EditText) rootView
				.findViewById(R.id.forget_password_new_password_txt);
		confirmPasswordTxt = (EditText) rootView
				.findViewById(R.id.forget_password_confirm_password_txt);
	}
	
	/**
	 * this method should be called when starting this fragment to specify the
	 * loginType and Intent action if exists ! so it will handled on
	 * loginSuccess method :)
	 **/
	public void HandleLoginType_And_Intent() {
		this.navigationTypeId =getActivity().getIntent().getExtras().getInt(LogInActivity.LOGIN_TYPE_KEY);
		this.intentAction = getActivity().getIntent().getExtras().getParcelable(LogInActivity.INTENT_NAVIGATION_KEY);
	}
	

	/** set the actions of the defined Controls */
	private void initailizeViewControls() {
		sendBtn.setOnClickListener(this);
		resendEmailBtn.setOnClickListener(this);
		// set the under line text of Resend Email button
		resendEmailBtn.setText(Html.fromHtml(getString(R.string.Resend_Email)));
		
		
		/*************** Password Text SetUp **************/
		newPasswordTxt.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable mEdit) {
				if (newPasswordTxt.getText().length() != 0) {
					if (!mEdit.toString().equals(confirmPasswordTxt.getText().toString())) {
						confirmPasswordTxt
								.setBackgroundResource(R.drawable.not_matched_password_field);
						confirmPasswordTxt.setTextColor(Color.RED);
					} else {
						confirmPasswordTxt
								.setBackgroundResource(R.drawable.input_text);
						confirmPasswordTxt.setTextColor(Color.BLACK);
					}
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
		/************************************************/

		/*************** Confirm Password Text SetUp **************/
		confirmPasswordTxt.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable mEdit) {
			
				if (!mEdit.toString().equals(newPasswordTxt.getText().toString())) {
					confirmPasswordTxt
							.setBackgroundResource(R.drawable.not_matched_password_field);
					confirmPasswordTxt.setTextColor(Color.RED);
				} else {
					confirmPasswordTxt
							.setBackgroundResource(R.drawable.input_text);
					confirmPasswordTxt.setTextColor(Color.BLACK);
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
		/************************************************/
		
		
	}

	/* to get the UserId and email Keys from the bundle */
	private void getIntentData() {
		this.UserId = (String) getActivity().getIntent().getExtras()
				.getString(ForgetPassowrdVerificationActivity.USERID_KEY);
		this.Email = (String) getActivity().getIntent().getExtras()
				.getString(ForgetPassowrdVerificationActivity.EMAIL_KEY);
	}

	public static Fragment newFragment() {
		return new ForgetPasswordVerificationFragment();
	}

	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}

	/** To Client-Validate Data of Forget Password */
	private boolean IsValidFields() {
		// check required verification code field
		if (ValidatorUtils.isRequired(verificationTxt.getText().toString()))
		{
			UIUtils.showToast(
					getActivity(),
					getResources().getString(
							R.string.ForgotPassword_VerificationCodeAlert1));
			return false;
		}
		// check required new password field
		else if (ValidatorUtils.isRequired(newPasswordTxt.getText().toString())) {
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.ForgotPassword_passwordAlert1));
			return false;
		}
		// check required confirm password field
		else if (ValidatorUtils.isRequired(confirmPasswordTxt.getText().toString()) ) {
			UIUtils.showToast(getActivity(),
					getResources()
							.getString(R.string.ForgotPassword_passwordAlert2));
			return false;
		}
		// check password and confirmation
		else if (!newPasswordTxt.getText().toString()
				.equals(confirmPasswordTxt.getText().toString())) {
			UIUtils.showToast(getActivity(),
					getResources()
							.getString(R.string.ForgotPassword_passwordAlert3));
			return false;
		} else
			return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.send_new_password_btn_id:
			showLoadingDialog();
			if (IsValidFields())
				UserManager.getInstance().changePassword( UserId,
					verificationTxt.getText().toString()/*"KDWI6ahZFFN4"*/, newPasswordTxt.getText().toString());
			break;
		case R.id.resend_email_btn_id:
			showLoadingDialog();
UserManager.getInstance().sendVerifcationCode( Email);
			break;

		default:
			break;
		}

	}

	@Override
	public void onException(AppException ex) {

		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());

	}

	/**handle the on success of Change password */
	@Override
	public void onSuccess() {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), "Password was changed successfully !");
		//getActivity().finish();
		//procedNextScreen(navigationTypeId, intentAction);
		startActivity(LogInActivity.getActivityIntent(getActivity(), Email, navigationTypeId, intentAction));
		getActivity().finish();
	}

	/**handle the on success of Send verification */
	@Override
	public void onSuccess(String userId) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), "Resent !");
		this.UserId = userId;
	}
	/** Basyouni's Method to handle pending actions and navigations */
	void procedNextScreen(int login_Type, Intent intent) {
		switch (login_Type) {
		case UserManager.NORMAL_LOGIN:
			startActivity( HomeActivity.getActivityIntent(getActivity())/*new Intent(getActivity(), HomeActivity.class)*/);
			break;
		case UserManager.LOGIN_WITH_INTENT:
			if (intent != null) {
				startActivity(intent);
				getActivity().finish();
			} else
				throw new NullIntentException();
			break;
		case UserManager.LOGIN_FOR_RESULT:
			if (intent != null) {
				getActivity().setResult(Activity.RESULT_OK, intent);
				getActivity().finish();
			} else
				throw new NullIntentException();
			break;
		default:
			getActivity().finish();
			break;
		}
	}
}
