package com.tawasol.barcelona.ui.fragments;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.managers.FanManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.ui.activities.ContestActivity;
import com.tawasol.barcelona.ui.activities.FansGridActivity;
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

/**
 * 
 * @author Mohga
 *
 */
public class MenuAnonymousFragment extends BaseFragment implements
		OnClickListener {

	RelativeLayout profileBtn, studioBtn, fansBtn, contestBtn, friendsBtn,
			sponsersBtn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.menu_anonymous_fragment,
				container, false);
		/* find the controls of the XML */
		findViewByID(rootView);

		/* set the info in controls */
		InitViewControls();

		return rootView;
	}

	/** get the Controls from XML file */
	private void findViewByID(View rootView) {
		profileBtn = (RelativeLayout) rootView
				.findViewById(R.id.menu_profile_btn);
		studioBtn = (RelativeLayout) rootView
				.findViewById(R.id.anonymous_menu_fcb_btn);
		fansBtn = (RelativeLayout) rootView
				.findViewById(R.id.anonymous_menu_fans_btn);
		contestBtn = (RelativeLayout) rootView
				.findViewById(R.id.anonymous_menu_contest_btn);
		friendsBtn = (RelativeLayout) rootView
				.findViewById(R.id.anonymous_menu_friends_btn);
		sponsersBtn = (RelativeLayout) rootView
				.findViewById(R.id.anonymous_menu_sponsers_btn);
	}

	/** Initialize the controls of the view */
	private void InitViewControls() {

		// set on click listener of buttons
		profileBtn.setOnClickListener(this);
		studioBtn.setOnClickListener(this);
		fansBtn.setOnClickListener(this);
		contestBtn.setOnClickListener(this);
		friendsBtn.setOnClickListener(this);
		sponsersBtn.setOnClickListener(this);
	}

	/** handle the on click listener of the fragment */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.menu_profile_btn:
			startActivity(LogInActivity.getActivityIntent(getActivity(),
					UserManager.NORMAL_LOGIN, null));
			break;
		case R.id.anonymous_menu_fcb_btn:
			startActivity(HomeActivity.getActivityIntent(getActivity()));
			break;
		case R.id.anonymous_menu_fans_btn:
			FanManager.getInstance().clearFans();
			startActivity(FansGridActivity.getIntent(getActivity(),
					FansGridActivity.LOADFANS).setFlags(
					Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
		case R.id.anonymous_menu_contest_btn:
			startActivity(ContestActivity.getActivityIntent(getActivity()));
			break;
		case R.id.anonymous_menu_friends_btn:
			startActivity(LogInActivity.getActivityIntent(getActivity(),
					UserManager.NORMAL_LOGIN, null));
			break;
		case R.id.anonymous_menu_sponsers_btn:
			break;

		default:
			break;
		}

	}
}
