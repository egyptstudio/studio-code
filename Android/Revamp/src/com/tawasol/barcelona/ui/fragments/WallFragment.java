package com.tawasol.barcelona.ui.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.commonsware.cwac.endless.EndlessAdapter;
import com.etiennelawlor.quickreturn.library.enums.QuickReturnType;
import com.etiennelawlor.quickreturn.library.listeners.OnViewStateChanged;
import com.etiennelawlor.quickreturn.library.listeners.QuickReturnListViewOnScrollListener;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.customView.ActionItem;
import com.tawasol.barcelona.customView.PostViewFactory;
import com.tawasol.barcelona.customView.QuickAction;
import com.tawasol.barcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.barcelona.data.cache.WallTable;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.entities.WebServiceRequestInfo;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnEmailVerify;
import com.tawasol.barcelona.listeners.OnPostsRecieved;
import com.tawasol.barcelona.listeners.OnUpdateFinished;
import com.tawasol.barcelona.listeners.UpdateDialogListener;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.ui.activities.ChatActivity;
import com.tawasol.barcelona.ui.activities.FilterActivity;
import com.tawasol.barcelona.ui.activities.HomeActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.ShopActivity;
import com.tawasol.barcelona.ui.activities.ShopActivityTabs;
import com.tawasol.barcelona.ui.activities.StudioFoldersActivity;
import com.tawasol.barcelona.ui.activities.VerifyPhoneActivity;
import com.tawasol.barcelona.utils.NetworkingUtils;
import com.tawasol.barcelona.utils.UIUtils;

public class WallFragment extends Fragment implements OnPostsRecieved,
		OnRefreshListener, OnUpdateFinished, OnViewStateChanged, OnEmailVerify /*
																				 * ,
																				 * UpdateDialogListener
																				 * ,
																				 * OnPostDeletedListener
																				 * ,
																				 * OnFollowSuccess
																				 */{
	public static final int FILTER_REQUEST = 454;
	public static final String COUNTRY_LIST = "com.barchelona.wallfragment.countryList";
	public static final String PopUPInitialState = "popUpInitialState";
	public static PostViewFactory viewFactory;
	public static final String refresh = "refresh";

	private boolean isNotEmpty = true;
	ListView wallList;
	PostViewModel post;
	TextView verifyText;
	Button verifyButton;
	Button resend;
	Button help;
	List<PostViewModel> posts = new ArrayList<PostViewModel>();
	LinearLayout initial_state_layout;
	TextView initialStateText;
	Button LoginButton;
	private Button captureBtn;
	RelativeLayout bottomBar;
	View VerificationView;
	FragmentInfo info;
	SwipeRefreshLayout swipeView;
	ListAdapter adapter;
	static Dialog dialog;
	boolean firstTime = false;
	LinearLayout studioPopUp;
	boolean chooseFavo = false;
	public boolean filtered;
	private ImageButton filterButton;
	private ProgressBar bar;
	private static ArrayList<Integer> countries;
	LinearLayout email_popUp;
	private QuickAction mQuickAction;
	private ActionItem itemHelp = new ActionItem();
	private TextView applyText;
	private static HomeActivity activity;
	/**
	 * this layout is no more than a transparent layer over listView it's only
	 * function is to block user touch when trying to change data inside list
	 * view adaptor to prevent listView crash specified in android issue num
	 * 71936 (Jun 19, 2014) in following link {@link https
	 * ://code.google.com/p/android/issues/detail?id=71936}
	 */
	LinearLayout cancelClick;
	private QuickReturnListViewOnScrollListener scrollListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = null;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = (HomeActivity) activity;
	}

	public static PostViewFactory getFactory() {
		return viewFactory;
	}

	@SuppressLint({ "InlinedApi", "NewApi" })
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_wall_new, container,
				false);
		((BaseActivity) getActivity()).setIsWall();
		User currentUser = UserManager.getInstance().getCurrentUser();
		if (!currentUser.isEmailVerified()) {
			itemHelp.setTitle(getActivity().getResources().getString(
					R.string.HomeUnconfirmedEmailAndPhone_emailWhy_one));
			itemHelp.setContent(getActivity().getResources().getString(
					R.string.HomeUnconfirmedEmailAndPhone_emailWhy_two));
		} else {
			itemHelp.setTitle(getActivity().getResources().getString(
					R.string.HomeUnconfirmedEmailAndPhone_phoneWhy_one));
			itemHelp.setContent(getActivity().getResources().getString(
					R.string.HomeUnconfirmedEmailAndPhone_phoneWhy_two));
		}
		mQuickAction = new QuickAction(getActivity());
		mQuickAction.setBackground(R.drawable.verify_phone_tooltip);
		mQuickAction.addActionItem(itemHelp);
		info = (FragmentInfo) getArguments().getSerializable(
				FragmentInfo.FRAGMENT_INFO_KEY);
		WallManager.setWHICH_TAB(info.getType());
		adapter = null;
		firstTime = false;
		wallList = (ListView) rootView.findViewById(R.id.listView1);
		wallList.setVisibility(View.GONE);
		PauseOnScrollListener listener = new PauseOnScrollListener(App
				.getInstance().getImageLoader(), true, true);
		wallList.setOnScrollListener(listener);
		// wallList.setOverScrollMode(View.OVER_SCROLL_NEVER);
		initial_state_layout = (LinearLayout) rootView
				.findViewById(R.id.initial_state_layout);
		swipeView = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
		swipeView.setEnabled(false);
		swipeView.setColorSchemeResources(R.color.imageFooter);
		cancelClick = (LinearLayout) rootView.findViewById(R.id.clickCancell);
		cancelClick.setVisibility(View.GONE);
		// swipeView.setColorSchemeResources(R.color.imageFooter);
		App.getInstance().getApplicationContext().getResources()
				.getColor(R.color.imageFooter);
		swipeView.setOnRefreshListener(this);
		initialStateText = (TextView) rootView
				.findViewById(R.id.initial_state_text);
		filterButton = (ImageButton) rootView.findViewById(R.id.filterButton);
		filterButton.setEnabled(false);
		filterButton.setVisibility(View.GONE);
		bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
		if (info.getType() == FragmentInfo.TOP_TEN
				|| info.getType() == FragmentInfo.MY_PIC
				|| ((UserManager.getInstance().getCurrentUserId() == 0 && info
						.getType() == FragmentInfo.WALL))) // final check added
															// by mohga
			filterButton.setVisibility(View.GONE);
		else
			filterButton.setVisibility(View.VISIBLE);
		filterButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (filtered) {
					filterButton.setBackgroundResource(R.drawable.filter);
					onFilterDisSelect();
					WallManager.getInstance().filterIsDiselected();
					filtered = false;
				} else {
					showDialog();
				}
			}
		});
		LoginButton = (Button) rootView.findViewById(R.id.loginButton);

		studioPopUp = (LinearLayout) rootView.findViewById(R.id.studioPopUp);
		studioPopUp.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				hideStudioPopUp();
			}
		});
		VerificationView = rootView.findViewById(R.id.pop_up_email_verify);
		verifyText = (TextView) rootView.findViewById(R.id.verifytypeText);
		verifyButton = (Button) rootView.findViewById(R.id.verifytypebutton);
		resend = (Button) rootView.findViewById(R.id.resendButton);
		help = (Button) rootView.findViewById(R.id.helpButton);
		help.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// User currentUser =
				// UserManager.getInstance().getCurrentUser();
				// if (!currentUser.isEmailVerified()) {
				// // showHelpDialog(v , UserManager.TYPE_EMAIL);
				// }
				mQuickAction.show(v, VerificationView.getLeft(), false);
			}
		});
		resend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO check type
				handleVerification(UserManager.TYPE_EMAIL);
			}
		});
		bottomBar = (RelativeLayout) rootView.findViewById(R.id.bottom_bar);
		ViewTreeObserver observer = bottomBar.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@SuppressLint("NewApi")
			@Override
			public void onGlobalLayout() {
				// menuBtnRight.setPadding(0, titleView.getHeight() / 2, 0, 0);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					bottomBar.getViewTreeObserver()
							.removeOnGlobalLayoutListener(this);
				} else {
					bottomBar.getViewTreeObserver()
							.removeGlobalOnLayoutListener(this);
				}
				((BaseActivity) getActivity()).setBottomBarHeight(bottomBar
						.getHeight());
			}
		});
		activityResumed();
		setupBottomBar(rootView);
		return rootView;
	}

	/**
	 * 
	 */
	protected void showDialog() {
		dialog = new Dialog(getActivity());
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.filter_dialog_new);
		email_popUp = (LinearLayout) dialog.findViewById(R.id.popUp_background);
		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				// apdateTags();
			}
		});

		if (info.getType() == FragmentInfo.LATEST_TAB)
			email_popUp.setBackgroundResource(R.drawable.popup_bg_small);
		else
			email_popUp.setBackgroundResource(R.drawable.popup_bg);
		final ImageButton close_button = (ImageButton) dialog
				.findViewById(R.id.pop_up_close_btn);
		close_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		LinearLayout favLayout = (LinearLayout) dialog
				.findViewById(R.id.add_fav_layout);
		LinearLayout apply_btn = (LinearLayout) dialog
				.findViewById(R.id.apply_btn);
		applyText = (TextView)dialog.findViewById(R.id.applyText);
		applyText.setTextColor(getActivity().getResources().getColor(R.color.gray));
		final TextView selectedCountries = (TextView) dialog.findViewById(R.id.choose_contry_btn);
		final CheckBox choose_favo = (CheckBox) dialog
				.findViewById(R.id.search_check_box);
		if (info.getType() == FragmentInfo.LATEST_TAB) {
			favLayout.setVisibility(View.GONE);
		}
		choose_favo
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							isNotEmpty = true;
							chooseFavo = true;
							applyText.setTextColor(getActivity().getResources().getColor(android.R.color.white));
						} else {
							if (WallFragment.countries != null)
								if (WallFragment.countries.isEmpty()){
									isNotEmpty = false;
									applyText.setTextColor(getActivity().getResources().getColor(R.color.gray));
								}
							chooseFavo = false;
						}
						WallManager.getInstance().setFilterWithFavo(chooseFavo);
					}
				});

		RelativeLayout chooseCountryBtn = (RelativeLayout) dialog
				.findViewById(R.id.countrySelectLayout);

		chooseCountryBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(WallFragment.countries != null)
					WallFragment.countries.clear();
				selectedCountries.setText("");
				getActivity()
						.startActivityForResult(
								FilterActivity.getIntent(getActivity()),
								FILTER_REQUEST);
			}
		});

		apply_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				WallManager.getInstance().clearFiltered();
				if (WallFragment.countries != null
						&& !WallFragment.countries.isEmpty() && isNotEmpty) {
					filterButton.setBackgroundResource(R.drawable.filtered);
					filtered = true;
					if (adapter != null) {
						adapter = null;
					}
					bar.setVisibility(View.VISIBLE);
					wallList.setVisibility(View.GONE);
					WallManager.getInstance().applyFilter(
							WallFragment.this.countries, chooseFavo, info);
					dialog.dismiss();
				} else {
					UIUtils.showToast(getActivity(), "Please choose a filter");
				}
			}
		});

		dialog.show();

	}

	private void setupBottomBar(View rootView) {
		captureBtn = (Button) rootView.findViewById(R.id.capture_btn);
		captureBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hideStudioPopUp();
				startActivity(StudioFoldersActivity
						.getActivityIntent(getActivity()));
			}
		});

		TextView storeBtn = (TextView) rootView.findViewById(R.id.home_btn);
		storeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(ShopActivityTabs.getIntent(getActivity()));
			}
		});

		TextView chatBtn = (TextView) rootView.findViewById(R.id.chat_btn);
		chatBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// UIUtils.showToast(getActivity(),
				// getResources().getString(R.string.demo_version));
				// getActivity().startActivity(SettingsActivity.getActivityIntent(getActivity()));
				/** Add by Turki **/
				if (UserManager.getInstance().getCurrentUser() != null
						&& UserManager.getInstance().getCurrentUserId() > 0) {
					getActivity().startActivity(
							ChatActivity.getActivityIntent(getActivity()));
				} else {
					getActivity().startActivity(
							LogInActivity.getActivityIntent(getActivity(),
									UserManager.LOGIN_WITH_INTENT, ChatActivity
											.getActivityIntent(getActivity())));
				}
			}
		});

	}

	/**
	 * @param info
	 */
	private void checkInitialState(FragmentInfo info) {
		showStudioPopUp();
		if (info.getType() == FragmentInfo.LATEST_TAB)
			WallManager.getInstance().fireMenuInitialState(getActivity());
		if (info.getType() == FragmentInfo.WALL)
			fireWallInitialState();
		if (info.getType() == FragmentInfo.MY_PIC)
			fireWallInitialState();
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			User currentUser = UserManager.getInstance().getCurrentUser();
			if (!currentUser.isEmailVerified()) {
				VerificationView.setVisibility(View.VISIBLE);
				verifyText
						.setText(R.string.HomeUnconfirmedEmailAndPhone_emailMessage);
				verifyButton
						.setText(R.string.HomeUnconfirmedEmailAndPhone_Verify);
				resend.setText(R.string.HomeUnconfirmedEmailAndPhone_ResendEmail);
			} else if (!currentUser.isPhoneVerified()) {
				VerificationView.setVisibility(View.VISIBLE);
				verifyText
						.setText(R.string.HomeUnconfirmedEmailAndPhone_phoneMessage);
				verifyButton
						.setText(R.string.HomeUnconfirmedEmailAndPhone_Verify);
				resend.setVisibility(View.GONE);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == FILTER_REQUEST) {
				ArrayList<Integer> countries = data
						.getIntegerArrayListExtra(COUNTRY_LIST);
				if (!countries.isEmpty()){
					((TextView) dialog.findViewById(R.id.choose_contry_btn))
							.setText(countries.size() + " selected");
					((TextView) dialog.findViewById(R.id.applyText)).setTextColor(activity.getResources().getColor(android.R.color.white));
				}
				else
					((TextView) dialog.findViewById(R.id.applyText)).setTextColor(activity.getResources().getColor(R.color.gray));
				this.countries = countries;
			} else if (requestCode == PostViewFactory.INTENT_REQUEST_CODE) {
				viewFactory.handelaction(data);
			}

		}
	}

	/**
	 * 
	 */
	private void fireMyPicInitialState() {
		initial_state_layout.setVisibility(View.VISIBLE);
		wallList.setVisibility(View.GONE);
		initialStateText.setText(getActivity().getResources().getString(
				R.string.HomeInitialState_MypicsNoPhotosMessage));
		LoginButton.setVisibility(View.GONE);
	}

	/**
	 * 
	 */
	private void fireWallInitialState() {
		if (UserManager.getInstance().getCurrentUserId() == 0) {
			initial_state_layout.setVisibility(View.VISIBLE);
			wallList.setVisibility(View.GONE);
			initialStateText.setText(getActivity().getResources().getString(
					R.string.HomeInitialState_WallLoginMessage));
			LoginButton.setVisibility(View.VISIBLE);
			LoginButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					startActivity(LogInActivity.getActivityIntent(
							getActivity(), UserManager.NORMAL_LOGIN, null));
				}
			});
		}
	}

	/**
	 * @param typeEmail
	 */
	protected void handleVerification(int type) {
		if (type == UserManager.TYPE_EMAIL)
			UserManager.getInstance().verify(
					UserManager.getInstance().getCurrentUser().getEmail(),
					UserManager.getInstance().getCurrentUserId());
		else
			getActivity().startActivity(
					VerifyPhoneActivity.getActivityIntent(getActivity()));

	}

	void onFilterDisSelect() {
		initial_state_layout.setVisibility(View.GONE);
		if (adapter != null) {
			adapter = null;
			wallList.invalidateViews();
		}
		bar.setVisibility(View.VISIBLE);
		wallList.setVisibility(View.GONE);
		activityResumed();
	}

	public void activityResumed() {
		long lastUpdateTime = 0;
		if (this.post != null && !this.posts.isEmpty())
			lastUpdateTime = this.posts.get(this.posts.size() - 1)
					.getLastUpdateTime();
		List<PostViewModel> posts = null;
		switch (info.getType()) {
		case FragmentInfo.LATEST_TAB:
			posts = WallManager.getInstance().getLatest();
			break;
		case FragmentInfo.WALL:
			posts = WallManager.getInstance().getWall();
			break;
		case FragmentInfo.MY_PIC:
			posts = WallManager.getInstance().getMyPics();
			break;
		default:
			break;
		}
		if (posts != null && !posts.isEmpty()) {
			if (bar.getVisibility() == View.VISIBLE)
				bar.setVisibility(View.GONE);
			ViewTreeObserver observer = bottomBar.getViewTreeObserver();
			observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

				@SuppressLint("NewApi")
				@SuppressWarnings("deprecation")
				@Override
				public void onGlobalLayout() {

					if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.JELLY_BEAN) {
						bottomBar.getViewTreeObserver()
								.removeGlobalOnLayoutListener(this);
					} else {
						bottomBar.getViewTreeObserver()
								.removeOnGlobalLayoutListener(this);
					}
					QuickReturnListViewOnScrollListener scrollListener = new QuickReturnListViewOnScrollListener(
							QuickReturnType.FOOTER, null, 0, bottomBar,
							bottomBar.getHeight());
					// Setting to true will slide the header and/or footer into
					// view or slide out of view based
					// on what is visible in the idle scroll state
					// scrollListener.setCanSlideInIdleScrollState(true);
					wallList.setOnScrollListener(scrollListener);
					filterButton.setEnabled(true);
					// filterButton.setVisibility(View.VISIBLE);
					if (info.getType() == FragmentInfo.TOP_TEN
							|| info.getType() == FragmentInfo.MY_PIC
							|| ((UserManager.getInstance().getCurrentUserId() == 0 && info
									.getType() == FragmentInfo.WALL))) // final
																		// check
																		// added
																		// by
																		// mohga
						filterButton.setVisibility(View.GONE);
				}
			});
			wallList.setVisibility(View.VISIBLE);
			adapter = new ListAdapter(posts);
			wallList.setAdapter(new updatePosts(getActivity(), new ListAdapter(
					posts), R.layout.progress_bar));
			if (getActivity().getIntent().getBooleanExtra(refresh, false)
					&& info.getType() == FragmentInfo.MY_PIC) {
				WebServiceRequestInfo requestInfo = null;
				requestInfo = new WebServiceRequestInfo(
						FragmentInfo.ACTION_UPDATE, UserManager.getInstance()
								.getCurrentUserId(), WallManager.NUM_OF_POSTS,
						adapter.getPosts().get(0).getCreationTime(),
						WebServiceRequestInfo.TIME_FILTER_NEW, 0, 0,
						WebServiceRequestInfo.GET_POSTS,
						FragmentInfo.getMethodName(info));
				WallManager.getInstance().handlePosts(requestInfo,
						info.getType(), cancelClick);
			}
			swipeView.setEnabled(true);
		} else {
			if (info.getType() != FragmentInfo.MY_PIC
					&& info.getType() != FragmentInfo.WALL) {
				bar.setVisibility(View.VISIBLE);
				initial_state_layout.setVisibility(View.GONE);
				wallList.setVisibility(View.VISIBLE);
				WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
						info.getAction(), UserManager.getInstance()
								.getCurrentUserId(), WallManager.NUM_OF_POSTS,
						lastUpdateTime, WebServiceRequestInfo.TIME_FILTER_OLD,
						0, 0, WebServiceRequestInfo.GET_POSTS,
						FragmentInfo.getMethodName(info));
				WallManager.getInstance().handlePosts(requestInfo,
						info.getType(), cancelClick);
			} else {
				if (UserManager.getInstance().getCurrentUserId() != 0) {
					bar.setVisibility(View.VISIBLE);
					WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
							info.getAction(), UserManager.getInstance()
									.getCurrentUserId(),
							WallManager.NUM_OF_POSTS, lastUpdateTime,
							WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
							WebServiceRequestInfo.GET_POSTS,
							FragmentInfo.getMethodName(info));
					WallManager.getInstance().handlePosts(requestInfo,
							info.getType(), cancelClick);
				}
			}
		}
	}

	@Override
	public void onResume() {
		WallManager.getInstance().addListener(this);
		checkInitialState(info);
		super.onResume();
		QuickReturnListViewOnScrollListener.registerToUiListeners(this);
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		wallList.invalidateViews();
		super.onResume();
		QuickReturnListViewOnScrollListener.registerToUiListeners(this);
		Log.i("Inside On Resume Count",
				String.valueOf(WallTable.getInstance().getCount()));
		if (scrollListener != null) {
			if (QuickReturnListViewOnScrollListener.getFooterState()) {
				hideStudioPopUp();
			} else {
				showStudioPopUp();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		QuickReturnListViewOnScrollListener.unRegisterToUiListeners(this);
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroy() {
		WallManager.getInstance().removeListener(this);
		super.onDestroy();
	}

	private class ListAdapter extends BaseAdapter {
		List<PostViewModel> posts;

		public ListAdapter(List<PostViewModel> posts) {
			this.posts = posts;
			viewFactory = new PostViewFactory(getActivity(), info.getType());
		}

		List<PostViewModel> getPosts() {
			return this.posts;
		}

		void addToList(List<PostViewModel> posts) {
			this.posts.addAll(posts);
			cancelClick.setVisibility(View.GONE);
		}

		@Override
		public int getCount() {
			return this.posts.size();
		}

		@Override
		public Object getItem(int position) {
			return this.posts.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View rootView = null;
			if (!this.posts.isEmpty())
				rootView = viewFactory.getView(convertView, parent,
						this.posts.get(position), wallList, position, 0);
			return rootView;
		}

	}

	public void getToFirst() {
		wallList.smoothScrollToPosition(0);
	}

	@Override
	public void onSuccess(final List<PostViewModel> posts) {
		swipeView.setRefreshing(false);
		// if (adapter == null)
		handleFirstTimeRecieved(posts);
		cancelClick.setVisibility(View.GONE);
	}

	void showVirificationPopUp(int type) {
		if (type == UserManager.TYPE_EMAIL) {
			verifyText.setText(getActivity().getResources().getString(
					R.string.HomeUnconfirmedEmailAndPhone_emailMessage));
			verifyButton.setText(getActivity().getResources().getString(
					R.string.HomeUnconfirmedEmailAndPhone_ResendEmail));
		} else if (type == UserManager.TYPE_PHONE) {
			verifyText.setText(getActivity().getResources().getString(
					R.string.HomeUnconfirmedEmailAndPhone_phoneMessage));
			verifyButton.setText(getActivity().getResources().getString(
					R.string.HomeUnconfirmedEmailAndPhone_Verify));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener#onRefresh
	 * ()
	 */
	@Override
	public void onRefresh() {
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			if (!adapter.getPosts().isEmpty()) {
				WebServiceRequestInfo requestInfo = null;
				long currentTime = 0;
				if (adapter != null && !adapter.getPosts().isEmpty()
						&& adapter.getPosts().get(0) != null) {
					currentTime = adapter.getPosts().get(0).getCreationTime();
				}
				switch (info.getType()) {
				case FragmentInfo.LATEST_TAB:
					currentTime = WallManager.getInstance()
							.getLatestCurrentTime();
					break;
				case FragmentInfo.WALL:
					currentTime = WallManager.getInstance()
							.getWallCurrentTime();
					break;
				case FragmentInfo.MY_PIC:
					currentTime = WallManager.getInstance()
							.getMyPicsCurrentTime();
					break;
				default:
					break;
				}
				if (filtered) {
					requestInfo = new WebServiceRequestInfo(
							FragmentInfo.ACTION_UPDATE, UserManager
									.getInstance().getCurrentUserId(),
							WallManager.NUM_OF_POSTS, currentTime,
							WebServiceRequestInfo.TIME_FILTER_NEW, 0, 0,
							WebServiceRequestInfo.GET_POSTS,
							FragmentInfo.getMethodName(info), countries,
							chooseFavo ? WebServiceRequestInfo.TURN_ON_FILTER
									: WebServiceRequestInfo.TURN_OFF_FILTER);
				} else {
					requestInfo = new WebServiceRequestInfo(
							FragmentInfo.ACTION_UPDATE, UserManager
									.getInstance().getCurrentUserId(),
							WallManager.NUM_OF_POSTS, currentTime,
							WebServiceRequestInfo.TIME_FILTER_NEW, 0, 0,
							WebServiceRequestInfo.GET_POSTS,
							FragmentInfo.getMethodName(info));
				}
				WallManager.getInstance().handlePosts(requestInfo,
						info.getType(), cancelClick);
			}
		} else {
			UIUtils.showToast(getActivity(), getActivity().getResources()
					.getString(R.string.connection_noConnection));
		}
	}

	private class updatePosts extends EndlessAdapter {

		List<PostViewModel> tempList;

		public updatePosts(Context context, ListAdapter wrapped,
				int pendingResource) {
			super(context, wrapped, pendingResource);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.commonsware.cwac.endless.EndlessAdapter#appendCachedData()
		 */
		@Override
		protected void appendCachedData() {
			if (getWrappedAdapter() != null) {
				swipeView.setEnabled(true);
				((ListAdapter) getWrappedAdapter()).addToList(tempList);
				WallManager.getInstance().saveToDataBase(tempList,
						info.getType());
			}
			// tempList = null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.commonsware.cwac.endless.EndlessAdapter#cacheInBackground()
		 */
		@Override
		protected boolean cacheInBackground() throws Exception {
			WebServiceRequestInfo requestInfo = null;
			long updateTime = 0;
			if (info.getType() != FragmentInfo.LATEST_TAB)
				updateTime = getLastUpdateTime();
			else {
				updateTime = getCreationTime();
			}
			if (filtered) {
				requestInfo = new WebServiceRequestInfo(
						FragmentInfo.ACTION_GETMORE, UserManager.getInstance()
								.getCurrentUserId(), WallManager.NUM_OF_POSTS,
						updateTime, WebServiceRequestInfo.TIME_FILTER_OLD, 0,
						0, WebServiceRequestInfo.GET_POSTS,
						FragmentInfo.getMethodName(info), countries,
						chooseFavo ? WebServiceRequestInfo.TURN_ON_FILTER
								: WebServiceRequestInfo.TURN_OFF_FILTER);
			} else {
				requestInfo = new WebServiceRequestInfo(
						WebServiceRequestInfo.ACTION_GETMORE, UserManager
								.getInstance().getCurrentUserId(),
						WallManager.NUM_OF_POSTS, updateTime,
						WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
						WebServiceRequestInfo.GET_POSTS,
						FragmentInfo.getMethodName(info));
			}
			tempList = WallManager.getInstance().getPosts(requestInfo,
					info.getType());
			return !tempList.isEmpty();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tawasol.barcelona.listeners.OnUpdateFinished#onUpdateFinished()
	 */
	@Override
	public void onUpdateFinished() {
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		swipeView.setRefreshing(false);
		wallList.invalidateViews();
		cancelClick.setVisibility(View.GONE);
	}

	public long getLastUpdateTime() {
		if (adapter != null
				&& !adapter.getPosts().isEmpty()
				&& adapter.getPosts().get(adapter.getPosts().size() - 1) != null)
			return adapter.getPosts().get(adapter.getPosts().size() - 1)
					.getLastUpdateTime();
		else
			return 0;
	}

	public long getCreationTime() {
		if (adapter != null
				&& !adapter.getPosts().isEmpty()
				&& adapter.getPosts().get(adapter.getPosts().size() - 1) != null)
			return adapter.getPosts().get(adapter.getPosts().size() - 1)
					.getCreationTime();
		else
			return 0;
	}

	public void showStudioPopUp() {
		if (!checkIfInitialState())
			studioPopUp.setVisibility(View.VISIBLE);
	}

	public void hideStudioPopUp() {
		studioPopUp.setVisibility(View.GONE);
		saveInitialPopUpStateEnded();
	}

	public boolean checkIfInitialState() {
		if (getActivity() != null)
			return new File(getActivity().getFilesDir() + "/"
					+ PopUPInitialState).exists();
		else
			return false;
	}

	public void saveInitialPopUpStateEnded() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(getActivity(),
							PopUPInitialState, "");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onException(AppException ex) {
		cancelClick.setVisibility(View.GONE);
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		if(ex.getErrorCode() != AppException.NO_DATA_EXCEPTION)
		Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT)
				.show();
	}

	@Override
	public void viewDisappeared() {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (!checkIfInitialState()) {
					studioPopUp.setVisibility(View.GONE);
				}
			}
		});
	}

	@Override
	public void viewAppeared() {
		if (!checkIfInitialState()) {
			studioPopUp.setVisibility(View.VISIBLE);
		}
	}

	private void handleFirstTimeRecieved(final List<PostViewModel> posts) {
		if (filtered && posts.isEmpty())
			fireNoPostsFoundByFilter();
		if (info.getType() == FragmentInfo.MY_PIC && posts.isEmpty())
			fireMyPicInitialState();
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		ViewTreeObserver observer = bottomBar.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressLint("NewApi")
			@SuppressWarnings("deprecation")
			@Override
			public void onGlobalLayout() {

				if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.JELLY_BEAN) {
					bottomBar.getViewTreeObserver()
							.removeGlobalOnLayoutListener(this);
				} else {
					bottomBar.getViewTreeObserver()
							.removeOnGlobalLayoutListener(this);
				}
				scrollListener = new QuickReturnListViewOnScrollListener(
						QuickReturnType.FOOTER, null, 0, bottomBar, bottomBar
								.getHeight());
				// Setting to true will slide the header and/or footer into
				// view or slide out of view based
				// on what is visible in the idle scroll state
				scrollListener.setCanSlideInIdleScrollState(true);
				wallList.setOnScrollListener(scrollListener);
				if (!posts.isEmpty()) {
					if (info.getType() == FragmentInfo.TOP_TEN
							|| info.getType() == FragmentInfo.MY_PIC
							|| ((UserManager.getInstance().getCurrentUserId() == 0 && info
									.getType() == FragmentInfo.WALL))) // final
																		// check
																		// added
																		// by
																		// mohga
						filterButton.setVisibility(View.GONE);
					else {
						filterButton.setEnabled(true);
						filterButton.setVisibility(View.VISIBLE);
					}

				}
			}
		});
		swipeView.setEnabled(true);
		wallList.setVisibility(View.VISIBLE);
		adapter = new ListAdapter(posts);
		wallList.setAdapter(new updatePosts(getActivity(), new ListAdapter(
				posts), R.layout.progress_bar));
		cancelClick.setVisibility(View.GONE);
	}

	private void fireNoPostsFoundByFilter() {
		initial_state_layout.setVisibility(View.VISIBLE);
		wallList.setVisibility(View.GONE);
		initialStateText.setText(getActivity().getResources().getString(
				R.string.filterResult_NoResults));
		initialStateText.setCompoundDrawablesWithIntrinsicBounds(0,
				R.drawable.noimage_results, 0, 0);
		LoginButton.setVisibility(View.GONE);
	}

	@Override
	public void onSuccess() {
		VerificationView.setVisibility(View.GONE);
		wallList.invalidateViews();
	}

	// @Override
	// public void countryList(List<Integer> countries) {
	// if (countries.size() > 0) {
	// // ((TextView) dialog.findViewById(R.id.choose_contry_btn))
	// // .setVisibility(View.VISIBLE);
	// ((TextView) dialog.findViewById(R.id.choose_contry_btn))
	// .setText(countries.size() + " selected");
	// this.countries = countries;
	// }
	// }

}
