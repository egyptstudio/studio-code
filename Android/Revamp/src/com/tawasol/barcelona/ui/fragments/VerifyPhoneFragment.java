package com.tawasol.barcelona.ui.fragments;
  
import com.tawasol.barcelona.R; 
import com.tawasol.barcelona.entities.SMSVerificationModel;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnSMSVerificationCodeListener;
import com.tawasol.barcelona.listeners.OnVerifyPhoneListener;
import com.tawasol.barcelona.managers.ChatManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.utils.UIUtils; 
import com.tawasol.barcelona.utils.ValidatorUtils;
 
import android.os.Bundle; 
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText; 

/**
 * @author TURKI
 * Registration Level One
 */
public class VerifyPhoneFragment extends BaseFragment implements OnClickListener, OnSMSVerificationCodeListener, OnVerifyPhoneListener{

	private EditText validationCodeTxt;
	private String validationCode;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_verify_phone, container, false);
		
		/** Initialize registration view **/
		init(rootView);

		return rootView;
	}

	/** Handle OnClickListeners **/
	@Override
	public void onClick(View v) {
		/** Get view values **/
		getValues();

		switch (v.getId()) {
		case R.id.send_btn:
			if(checkValidationField())
				UserManager.getInstance().callSMSVerification(new SMSVerificationModel(validationCode));
			break;
			
		case R.id.resend_btn:
			if(checkValidationField())
				UserManager.getInstance().callVerifyPhone();
			break;
		}
	}

	/** Initialize registration view **/
	private void init(View rootView) {

		validationCodeTxt  = (EditText) rootView.findViewById(R.id.validation_code_Txt);
		
		/** Initialize of register button **/
		Button sendButton   = (Button) rootView.findViewById(R.id.send_btn);
		Button resendButton = (Button) rootView.findViewById(R.id.resend_btn);
		sendButton.setOnClickListener(this);
		resendButton.setOnClickListener(this);
		
	}

	/**  Get registration fields value **/
	private void getValues() {
		/** Get all values from user **/
		validationCode        = validationCodeTxt.getText().toString();
	}


	/**  Validate validation fields **/
	private boolean checkValidationField() {

		if (ValidatorUtils.isRequired(validationCode)) {
			UIUtils.showToast(getActivity(), getResources().getString(R.string.reg_fill_data_validation));
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void onSuccess() {
		User userPhoneVerified = UserManager.getInstance().getCurrentUser();
		userPhoneVerified.setPhoneVerified(true);
		UserManager.getInstance().cacheUser(userPhoneVerified);
	}

	@Override
	public void onException(AppException ex) {
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onSuccess(User userPhoneVerified) {
		
	}
	
	@Override
	public void onResume() {
		ChatManager.getInstance().addListener(this); 
		super.onResume();
	}
 
	@Override
	public void onPause() {
		ChatManager.getInstance().removeListener(this);
		super.onPause();
	}
}
