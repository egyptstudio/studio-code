package com.tawasol.barcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.StudioFolder;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnFoldersReceivedListener;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.ui.activities.StudioPhotosActivity;
import com.tawasol.barcelona.ui.adapters.TeamAdapter;
import com.tawasol.barcelona.utils.UIUtils;



public class TeamFragment extends BaseFragment implements OnFoldersReceivedListener ,OnItemClickListener{

	ListView teamLV;
	TeamAdapter teamAdapter;
	List<StudioFolder> folders;
	
	private static TeamFragment teamFragment;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_team, container,
				false);
		initView(rootView);
		
		return rootView;
	}
	private void initView(View rootView)
	{
		//showLoadingDialog();
		teamLV = (ListView)rootView.findViewById(R.id.team_listview);
		teamLV.setPadding(0, 0, 0,((BaseActivity)getActivity()).getBottomBarHeight());
		
		// initialize the list
		folders = new ArrayList<StudioFolder>();
		
		// initialize the adapter  
		teamAdapter = new TeamAdapter(getActivity(), (ArrayList<StudioFolder>)folders);
		//teamLV.setPadding(0, 0, 0, BaseActivity.bottomBarHeight);
		teamLV.setAdapter(teamAdapter);
		
		teamLV.setOnItemClickListener(this);
		
		StudioManager.getInstance().getStudioFolders(StudioFolder.FOLDER_TYPE_TEAM);
		/*if(StudioManager.getInstance().getFolders() != null && StudioManager.getInstance().getFolders().size() != 0)
		{
			folders.clear();
			folders.addAll(getTeamList());
			teamAdapter.notifyDataSetChanged();
		}*/
		
	}
	

	
	public static TeamFragment newInstance() {
		if (teamFragment == null)
			teamFragment = new TeamFragment();
		return teamFragment;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		StudioManager.getInstance().addListener(this);
	}
	
	@Override
	public void onPause() {
		
		super.onPause();
		StudioManager.getInstance().removeListener(this);
	}
	@Override
	public void onSuccess(List<StudioFolder> objs) {
		hideLoadingDialog();
		
		folders.clear();
		folders.addAll(objs);
		teamAdapter.notifyDataSetChanged();
		
	}
	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		startActivity(StudioPhotosActivity.getActivityIntent(getActivity(),folders.get(position)));
		
	}


}
