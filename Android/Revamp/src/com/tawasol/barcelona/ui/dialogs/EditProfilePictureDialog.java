package com.tawasol.barcelona.ui.dialogs;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.soundcloud.android.crop.Crop;
import com.tawasol.barcelona.R;

@SuppressLint("NewApi")
public class EditProfilePictureDialog extends DialogFragment implements OnClickListener {
	Button myPicsBtn;
	Button cameraBtn;
	Button photoAlbumBtn;
	Button fcbPhotosBtn;
	Button cancelBtn;
	public static EditProfilePictureDialog new_Instance() {
		EditProfilePictureDialog editphoto = new EditProfilePictureDialog();
		editphoto.setStyle(DialogFragment.STYLE_NO_FRAME,
				android.R.style.Theme_DeviceDefault_Dialog);
		return editphoto;
	}
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
	getDialog().getWindow().setBackgroundDrawable(
			new ColorDrawable(Color.TRANSPARENT));
	View rootView = inflater.inflate(R.layout.edit_profile_photo_dialog, container,
			false);

	ViewByID(rootView);
	InitViewControls();
	return rootView;
}
	private void ViewByID(View rootView)
	{
		myPicsBtn=(Button)rootView.findViewById(R.id.MyPicsBtn);
		cameraBtn = (Button)rootView.findViewById(R.id.CameraBtn);
		photoAlbumBtn=(Button)rootView.findViewById(R.id.PhotoAlbumBtn);
		fcbPhotosBtn=(Button)rootView.findViewById(R.id.FcbPhotosBtn);
		cancelBtn=(Button)rootView.findViewById(R.id.CancelBtn);
	}
	private void InitViewControls()
	{
		myPicsBtn.setOnClickListener(this);
		cameraBtn.setOnClickListener(this);
		photoAlbumBtn.setOnClickListener(this);
		fcbPhotosBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.MyPicsBtn:
			// TODO camera on click
			break;
		case R.id.CameraBtn:
			Crop.pickImageFromCamera(getActivity());
			break;
		case R.id.PhotoAlbumBtn:
			Crop.pickImage(getActivity());
			break;
		case R.id.FcbPhotosBtn:
			// TODO FcbPhotos on click
			break;
		case R.id.CancelBtn:
			getDialog().dismiss();
			break;
		default:
			break;
		}
		
	}
	
}
