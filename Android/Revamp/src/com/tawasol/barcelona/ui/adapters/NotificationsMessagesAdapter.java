package com.tawasol.barcelona.ui.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.Notification;

/**
 * @author Mohga 
 * **/

public class NotificationsMessagesAdapter extends BaseAdapter{

	private Activity activity;
	private ArrayList<Notification> notifications;
	
	
	public NotificationsMessagesAdapter(Activity activity,ArrayList<Notification> notifications)
	{
		this.activity = activity;
		this.notifications = notifications;
	}
	
	@Override
	public int getCount() {
		return notifications.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final notificationsMessagesViewHolder holder;
		if(convertView ==  null)
		{
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new notificationsMessagesViewHolder();
			convertView = inflater.inflate(
					R.layout.chat_recent_item, parent,
					false);
			
			holder.messageUserNameTxt = (TextView)convertView.findViewById(R.id.user_name);
			holder.messageTxt = (TextView)convertView.findViewById(R.id.last_message);
			holder.userImg = (ImageView)convertView.findViewById(R.id.profile_Image);
			holder.messagePB=(ProgressBar)convertView.findViewById(R.id.contact_progress_bar);
			holder.messageDateTxt = (TextView)convertView.findViewById(R.id.date);
			
			convertView.setTag(holder);
		}
		else
			holder = (notificationsMessagesViewHolder) convertView.getTag();
		
		// set the user name 
		holder.messageUserNameTxt.setText(notifications.get(position).getSenderFullName());
		
		// set the message text 
		//holder.messageTxt.setText(notifications.get(position).); // TODO ask hamed :) 
		
		// set the user image 
		App.getInstance().getImageLoader().displayImage(notifications.get(position).getSenderImgUrl(), holder.userImg, App.getInstance().getDisplayOption(), new ImageLoadingListener() {
			
			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				
			}
			
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				holder.messagePB.setVisibility(View.GONE);
				holder.userImg.setImageResource(R.drawable.profile_icon);
			}
			
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				holder.messagePB.setVisibility(View.GONE);
			}
			
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
				
			}
		});
		
		// set the date
		holder.messageDateTxt.setText(setTime(notifications.get(position).getTime()));
			
			return convertView;
	}

	private String setTime(long date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a",
				Locale.getDefault());
		dateFormat.setTimeZone(TimeZone.getDefault());
		return dateFormat.format(new Date(date * 1000));
	}
	
	private class notificationsMessagesViewHolder
	{
		TextView messageUserNameTxt;
		TextView messageTxt;
		ImageView userImg;
		ProgressBar messagePB;
		TextView messageDateTxt;
	}
}
