package com.tawasol.barcelona.ui.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView; 

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.FriendsEntity;
import com.tawasol.barcelona.entities.Message;  
import com.tawasol.barcelona.managers.ChatManager;
import com.tawasol.barcelona.ui.activities.ChatViewActivity;

/**
 * 
 * @author Turki
 *
 */
public class RecentChatAdapter  extends BaseAdapter {

	private Context context; 
	private List<Message> mLatestMessages;
	private List<Message> mFilteredLatestMessages;
    private MessageFilter messageFilter;
	private ImageLoader loader;
	private DisplayImageOptions options;
	private SimpleDateFormat dateFormat; 
	public RecentChatAdapter(Context context, List<Message> mLatestMessages) {
		this.context                 = context;
		this.mLatestMessages         = mLatestMessages;
		this.mFilteredLatestMessages = mLatestMessages;
		if(dateFormat == null){
			dateFormat = new SimpleDateFormat("MM/d/yyyy",Locale.getDefault());
			dateFormat.setTimeZone(TimeZone.getDefault());
		}
		loader = App.getInstance().getImageLoader();
		
		/** Initialize Options to display RoundedImage **/
		options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.contact_img)
				.showImageOnFail(R.drawable.contact_img)
				.showImageOnLoading(R.drawable.contact_img)
				.resetViewBeforeLoading(true)
				.cacheOnDisc(true)
				.cacheInMemory(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1000)).build();
        getFilter();
	}
	
	public Filter getFilter() {
		if(messageFilter == null)
			messageFilter = new MessageFilter();
		return messageFilter;
	}

	@Override
	public int getCount() {
		return mFilteredLatestMessages.size();
	}

	@Override
	public Object getItem(int position) {
		return mFilteredLatestMessages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.chat_recent_item, parent, false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		final Message lastMsg       = (Message) getItem(position);
		final FriendsEntity conatct = new FriendsEntity(lastMsg.getFriendId(), lastMsg.getSenderName()); 
		
		if (lastMsg.getSenderName() != null) {
			holder.userName.setText(lastMsg.getSenderName());
		} else {
			holder.userName.setText("");
		}
		
		if (lastMsg.getSenderPicture() != null)
			loader.displayImage(lastMsg.getSenderPicture(), holder.image, options, new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					// TODO Auto-generated method stub
					holder.progressBar.setVisibility(View.GONE);
					holder.image.setImageResource(R.drawable.contact_img);
				}
				
				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					// TODO Auto-generated method stub
					holder.progressBar.setVisibility(View.GONE);
				}
				
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}
			});
		else
			holder.image.setImageResource(R.drawable.contact_img);
		
		if (lastMsg.getMessageTime() > 0){
			if(lastMsg.getReceived() == 1)
				holder.date.setText(dateFormat.format(new Date(lastMsg.getMessageTime())));
			else
				holder.date.setText(dateFormat.format(new Date(lastMsg.getMessageTime() * (long) 1000)));
		}else{
			holder.date.setText("");
		}
		
		if (lastMsg.getText() != null)
			holder.message.setText(lastMsg.getText());
		else
			holder.message.setText("");
		
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				context.startActivity(ChatViewActivity.getActivityIntent(context, conatct));
			}
		});
		
		return convertView;
	}
	
	static class ViewHolder {
		ImageView image;
		TextView userName, message, date;
		ProgressBar progressBar;
		
		
		public ViewHolder(View view) {
			image        = (ImageView) view.findViewById(R.id.profile_Image);
			userName     = (TextView) view.findViewById(R.id.user_name);
			date         = (TextView) view.findViewById(R.id.date);
			message      = (TextView) view.findViewById(R.id.last_message);
			progressBar  = (ProgressBar) view.findViewById(R.id.contact_progress_bar);
		}	
	}
	
	private class MessageFilter extends Filter {
	    @Override
	    protected FilterResults performFiltering(CharSequence constraint) {
	       FilterResults filterResults = new FilterResults();
	       if (constraint!=null && constraint.length() > 0) {
	            ArrayList<Message> tempList          = new ArrayList<Message>();
	            ArrayList<Message> allMsgList        = new ArrayList<Message>();
                // search content in friend list
            for (Message msg : mLatestMessages) {
            	 allMsgList = (ArrayList<Message>) ChatManager.getInstance().getMessagesToFiltered(msg.getFriendId());
            	 if(allMsgList != null){
            		 for (Message allMsg : allMsgList) {
            			 if (allMsg.getText().toLowerCase().contains(constraint.toString().toLowerCase())) {
            				 tempList.add(msg);
            				 break;
            			 }
            		 }
            	 }
            }

            filterResults.count  = tempList.size();
            filterResults.values = tempList;
            } else {
                filterResults.count  = mLatestMessages.size();
                filterResults.values = mLatestMessages;
            }

            return filterResults;
	    }

	    /**
	    * Notify about filtered list to ui
	    * @param constraint text
	    * @param results filtered result
	    */
	    @SuppressWarnings("unchecked")
	    @Override
	    protected void publishResults(CharSequence constraint, FilterResults results) {
	        mFilteredLatestMessages = (ArrayList<Message>) results.values;
	        notifyDataSetChanged();
	    }
	}
}

