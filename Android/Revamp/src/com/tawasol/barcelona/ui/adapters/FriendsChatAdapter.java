package com.tawasol.barcelona.ui.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context; 
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView; 
 

import com.nostra13.universalimageloader.core.ImageLoader; 
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.FriendsEntity;  
import com.tawasol.barcelona.ui.activities.ChatViewActivity; 
import com.tawasol.barcelona.ui.activities.FanProfileActivity;

/**
 * 
 * @author Turki
 *
 */
public class FriendsChatAdapter  extends BaseAdapter{

		private Context context;
		private List<FriendsEntity> friends;
		private List<FriendsEntity> filteredFriends;
	    private FriendsFilter friendFilter;
		private ImageLoader mLoader;
		private boolean isChat;

		public FriendsChatAdapter(Context context, List<FriendsEntity> friends , boolean isChat) {
			this.context = context;
			this.friends = friends;
			this.filteredFriends = friends;
			this.isChat = isChat;
			mLoader =  App.getInstance().getImageLoader();
			getFilter();
		}

		public Filter getFilter() {
			if(friendFilter == null)
				friendFilter = new FriendsFilter();
			return friendFilter;
		}
		
		@Override
		public int getCount() {
			return filteredFriends.size();
		}

		@Override
		public Object getItem(int position) {
			return filteredFriends.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(R.layout.chat_friends_grid_item, parent, false);
				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final FriendsEntity friend = (FriendsEntity) getItem(position);
			
			// Contact Name
			if (friend.getFanName() != null)
				holder.friendName.setText(friend.getFanName());
			else
				holder.friendName.setText("");

			// Number of posts
			if (friend.getFanPicNumber() > 0)
				holder.numberOfPic.setText(""+friend.getFanPicNumber());
			else
				holder.numberOfPic.setText("");
						
			// Favorite
			if (friend.getFanOnline() == 1)
				holder.onlineImg.setVisibility(View.VISIBLE);
			else
				holder.onlineImg.setVisibility(View.GONE);
			
			// Followed
			if (friend.getIsFollowed() == 1)
				holder.followedImg.setImageResource(R.drawable.followed);
			else
				holder.followedImg.setImageResource(R.drawable.unfollowed);
				
			// Online
			if (friend.getIsPremium() == 1)
				holder.premiumFrame.setBackgroundResource(R.drawable.premium_frame);
			else
				holder.premiumFrame.setBackgroundResource(R.drawable.basic_frame);
						
			// Contact Profile Img
			if (friend.getFanPicture() != null)
				mLoader.displayImage(friend.getFanPicture(), holder.profilePic, App.getInstance().getDisplayOption(), new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String arg0, View arg1) {
						// TODO Auto-generated method stub	
					}
					
					@Override
					public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
						// TODO Auto-generated method stub
						holder.progressBar.setVisibility(View.GONE);
						holder.profilePic.setImageResource(R.drawable.image_sample);
					}
					
					@Override
					public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
						// TODO Auto-generated method stub
						holder.progressBar.setVisibility(View.GONE);
					}
					
					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
						// TODO Auto-generated method stub	
					}
				});
			else
				holder.profilePic.setImageResource(R.drawable.image_sample);

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(isChat)
						context.startActivity(ChatViewActivity.getActivityIntent(context, friend));
					else
						context.startActivity(FanProfileActivity.getIntent(context, friend.getFanID(), friend.getFanName()));
				}
			});
			
			return convertView;
		}

		private static class ViewHolder {
			TextView friendName, numberOfPic;
			ImageView profilePic, onlineImg, followedImg, numberOfPicImg;
			FrameLayout premiumFrame;
			ProgressBar progressBar;

			public ViewHolder(View view) {
				friendName        = (TextView) view.findViewById(R.id.friend_name);
				numberOfPic       = (TextView) view.findViewById(R.id.number_of_pic_txt);
				profilePic        = (ImageView) view.findViewById(R.id.profile_pic);
				onlineImg         = (ImageView) view.findViewById(R.id.favorite_img);
				followedImg       = (ImageView) view.findViewById(R.id.followed_img);
				numberOfPicImg    = (ImageView) view.findViewById(R.id.number_of_pic_img);
				premiumFrame      = (FrameLayout) view.findViewById(R.id.status_frame);
				progressBar       = (ProgressBar) view.findViewById(R.id.friend_progress_bar);
				
			}
		}
		
		private class FriendsFilter extends Filter {
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {
		       FilterResults filterResults = new FilterResults();
		       if (constraint!=null && constraint.length() > 0) {
		            ArrayList<FriendsEntity> tempList = new ArrayList<FriendsEntity>();
	                // search content in friend list
	                for (FriendsEntity friend : friends) {
	                   if (friend.getFanName().toLowerCase().contains(constraint.toString().toLowerCase())) {
	                       tempList.add(friend);
	                    }
	                }

	            filterResults.count  = tempList.size();
	            filterResults.values = tempList;
	            } else {
	                filterResults.count  = friends.size();
	                filterResults.values = friends;
	            }

	            return filterResults;
		    }

		    /**
		    * Notify about filtered list to ui
		    * @param constraint text
		    * @param results filtered result
		    */
		    @SuppressWarnings("unchecked")
		    @Override
		    protected void publishResults(CharSequence constraint, FilterResults results) {
		    	filteredFriends = (ArrayList<FriendsEntity>) results.values;
		        notifyDataSetChanged();
		    }
		}
	}	