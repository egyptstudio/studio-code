package com.tawasol.barcelona.ui.adapters;

import java.util.List;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.KeyValuePairs;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SpinnerAdapter extends BaseAdapter {
	private Activity activity;
	private List<KeyValuePairs> countries_Cities_List;

	public SpinnerAdapter(Activity activity, List<KeyValuePairs> countries_Cities_List) {
		this.activity = activity;
		this.countries_Cities_List = countries_Cities_List;
	}

	@Override
	public int getCount() {
		return countries_Cities_List.size();
	}

	@Override
	public KeyValuePairs getItem(int position) {
		return countries_Cities_List.get(position);
	}

	@Override
	public View getView(int arg0, View contentView, ViewGroup arg2) {
		CountryViewHolder countryViewHolder = null;
		if(contentView == null){
			LayoutInflater inflater = activity.getLayoutInflater();
			contentView = inflater.inflate(R.layout.popup_list_item, null);
			countryViewHolder = new CountryViewHolder();
			countryViewHolder.nameTxt = (TextView) contentView.findViewById(R.id.name_txt);
			contentView.setTag(countryViewHolder);
		}else{
			countryViewHolder = (CountryViewHolder) contentView.getTag();
		}
		countryViewHolder.nameTxt.setText((String)countries_Cities_List.get(arg0).getText());
		return contentView;
	}

	@Override
	public long getItemId(int position) {
		return countries_Cities_List.get(position).get_Id();
	}

	private class CountryViewHolder {
		public TextView nameTxt;
	}
}

