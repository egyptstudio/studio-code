/**
 * 
 */
package com.tawasol.barcelona.ui.adapters;

import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.customView.PostViewFactory;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.entities.PostViewModel;

/**
 * 
 * @author Basyouni
 *
 */
public class TopTenAdapter extends BaseExpandableListAdapter {

	private FragmentActivity _context;
	private List<String> _listDataHeader;
	// child data in format of header title, child title
	private HashMap<String, List<PostViewModel>> _listDataChild;
	ProgressBar profile_image_loading;
	PostViewFactory viewFactory;
	ExpandableListView list;

	public TopTenAdapter(FragmentActivity context, List<String> listDataHeader,
			HashMap<String, List<PostViewModel>> listChildData,
			ExpandableListView list) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
		this.list = list;
		viewFactory = new PostViewFactory(context , FragmentInfo.TOP_TEN);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if (!_listDataChild.isEmpty())
			return this._listDataChild.get(
					this._listDataHeader.get(groupPosition)).get(childPosition);
		else
			return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (groupPosition == 0){
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.top_ten_empty_header,
					null);
		}else{
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.top_ten_list_header,
					null);
		}
		LinearLayout groupHeaderContainer = (LinearLayout) convertView
				.findViewById(R.id.groupHeaderContainer);
		if(groupPosition == 0 )
			groupHeaderContainer.setVisibility(View.GONE);
		else
			groupHeaderContainer.setVisibility(View.VISIBLE);
		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.listHeader);
		if(groupPosition == 0 ){
			convertView.setVisibility(View.GONE);
			lblListHeader.setVisibility(View.GONE);
		}
		else{
			convertView.setVisibility(View.VISIBLE);
			lblListHeader.setVisibility(View.VISIBLE);
			lblListHeader.setText(headerTitle);
		}
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final PostViewModel member = (PostViewModel) getChild(
				groupPosition, childPosition);

		View rootView = viewFactory.getView(convertView, parent, member, list, childPosition  , groupPosition);

		return rootView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
