/**
 * 
 */
package com.tawasol.barcelona.ui.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.StudioPhoto;

/**
 * @author Mohga
 * 
 */
public class StudioPhotosAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<StudioPhoto> list;
	DisplayImageOptions displayOption;
	
	public StudioPhotosAdapter(Context context, ArrayList<StudioPhoto> list) {
		this.context = context;
		this.list    = list;
		
		displayOption = App.getInstance().getDisplayOption();/* new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.resetViewBeforeLoading(true)
		.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
		.cacheInMemory(true)
		.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(5)).build();*/
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final PurchasedViewHolder holder;
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new PurchasedViewHolder();
			convertView = inflater.inflate(R.layout.purchased_list_item, parent , false);
			holder.purchasedImg = (ImageView) convertView.findViewById(R.id.purchased_image);
			holder.useCountTxt = (TextView)convertView.findViewById(R.id.purchased_num_of_photos_txt);
			holder.newImg = (ImageView)convertView.findViewById(R.id.purchased_new_img);
			holder.premiumImg = (ImageView)convertView.findViewById(R.id.purchased_premium_img);
			holder.purchasedPB = (ProgressBar)convertView.findViewById(R.id.purchased_progress_bar);
			convertView.setTag(holder);
		} else {
			holder = (PurchasedViewHolder) convertView.getTag();
		}
		
		// set the purchased Image
		App.getInstance()
		   .getImageLoader()
		   .displayImage(list.get(position).getPhotoURL(), holder.purchasedImg,
			displayOption,new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					
				}
				
				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					holder.purchasedPB.setVisibility(View.GONE);
					holder.purchasedImg.setImageResource(R.drawable.ic_launcher);
					
				}
				
				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					holder.purchasedPB.setVisibility(View.GONE);
				}
				
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
				}
			});
		
		
		// set the user count for each photo 
		holder.useCountTxt.setText(list.get(position).getUseCount()+"");
		
		// check if premium , show the premium image
		if(list.get(position).getIsPermium() == 1)
		holder.premiumImg.setVisibility(View.VISIBLE);
		
		
		// check if less than week has been passed , show the new image
		if(list.get(position).getUploadDate() != 0){
		Date date = new Date(list.get(position).getUploadDate()*(long)1000);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy",
				Locale.getDefault());
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, 7);
		Date futureDate = cal.getTime();

		if (!date.after(futureDate)) {
			holder.newImg.setVisibility(View.VISIBLE);
		}
		}
		
		
		
		return convertView;
	}
	private class PurchasedViewHolder {
		ImageView purchasedImg;
		TextView useCountTxt;
		ImageView newImg;
		ImageView premiumImg;
		ProgressBar purchasedPB;
		
	}
	
}
