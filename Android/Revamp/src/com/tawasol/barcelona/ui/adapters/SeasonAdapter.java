package com.tawasol.barcelona.ui.adapters;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.Seasons;

public class SeasonAdapter extends BaseAdapter {

    Activity activity;
    List<Seasons> seasons;

    public SeasonAdapter(Activity activity,
    		List<Seasons> seasons_) {
        this.activity = activity;
        this.seasons =  new ArrayList<Seasons>();
        this.seasons = seasons_;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

       
            LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
            row = inflater.inflate(R.layout.season_row, parent, false);
            CheckBox seasonCheckBox = (CheckBox)row.findViewById(R.id.season_checkbox);
            seasonCheckBox.setText(seasons.get(position).getSeasonName().toString());
        return row;
    }

    

	@Override
	public int getCount() {
		return seasons.size();
	}

	@Override
	public Object getItem(int position) {
		return seasons.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
}
