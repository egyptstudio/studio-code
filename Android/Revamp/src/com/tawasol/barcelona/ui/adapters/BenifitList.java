package com.tawasol.barcelona.ui.adapters;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.ui.activities.BaseActivity;

public class BenifitList extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.benifit_list_fragment);
		showBackBtn();
		ListView benifitList = (ListView)findViewById(R.id.benifitList);
		
	}
	
	private class BenifitAdapter extends BaseAdapter{

		String [] benifits;
		
		public BenifitAdapter(){
			benifits = getResources().getStringArray(R.array.benifitList);
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return benifits.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return benifits[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(App.getInstance()
					.getApplicationContext());
			View rootView = convertView;
			if(rootView == null){
				rootView = inflater.inflate(R.layout.benifit_list_item, parent, false);
			}
			return rootView;
		}
		
		
		private class ViewHolder{
			TextView benifitNum;
			TextView benifitContent;
		}
	}
}
