package com.tawasol.barcelona.ui.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList; 
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.tawasol.barcelona.R; 
import com.tawasol.barcelona.customView.ActionItem;
import com.tawasol.barcelona.customView.QuickAction;
import com.tawasol.barcelona.customView.QuickAction.OnActionItemClickListener;
import com.tawasol.barcelona.entities.Message;  
import com.tawasol.barcelona.utils.UIUtils;

import android.content.Context;  
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter; 
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

/**
 * @author Turki
 * 
 */
public class ChatViewAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<Message> mMessages; 
	private SimpleDateFormat dateFormat;
	private QuickAction mQuickAction;
	private ActionItem itemCopy   = new ActionItem();
	private ActionItem itemDelete = new ActionItem();
	
	public ChatViewAdapter(Context context, ArrayList<Message> messages) {
		super();
		this.mContext    = context;
		this.mMessages   = messages;
		
		if(dateFormat == null){
			dateFormat = new SimpleDateFormat("h:mm a",Locale.getDefault());
			dateFormat.setTimeZone(TimeZone.getDefault());
		}
		
		itemCopy.setTitle(mContext.getResources().getString(R.string.copy));
		itemDelete.setTitle(mContext.getResources().getString(R.string.DeleteMsg_Delete));
		mQuickAction = new QuickAction(mContext);
		mQuickAction.addActionItem(itemCopy);
//		mQuickAction.addActionItem(itemDelete);
	}

	@Override
	public int getCount() {
		return mMessages.size();
	}

	@Override
	public Object getItem(int position) {
		return mMessages.get(position);
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		int colorBlack = mContext.getResources().getColor(R.color.black);
		int colorWhite = mContext.getResources().getColor(R.color.white);
		final Message message = (Message) this.getItem(position);
		ViewHolder holder;
		 
		if(convertView == null){ 
			holder          = new ViewHolder();
			convertView     = LayoutInflater.from(mContext).inflate(R.layout.sms_row, parent, false); 
			holder.message  = (TextView) convertView.findViewById(R.id.message_text); 
			holder.date     = (TextView) convertView.findViewById(R.id.data_text); 
			holder.smsFrame = (LinearLayout) convertView.findViewById(R.id.frame); 
			convertView.setTag(holder);
			 
		}else  
			holder = (ViewHolder) convertView.getTag();

		holder.message.setText(message.getText());
		if(message.getReceived() == 1)
			holder.date.setText(dateFormat.format(new Date(message.getMessageTime())));
		else
			holder.date.setText(dateFormat.format(new Date(message.getMessageTime() * 1000)));
		LayoutParams lp = (LayoutParams) holder.smsFrame.getLayoutParams();
		
		if (message.getReceived() == 1) {
			holder.smsFrame.setBackgroundResource(R.drawable.chat_bubble_send);
			holder.message.setTextColor(colorWhite);
			holder.date.setTextColor(colorWhite);
			lp.gravity = Gravity.RIGHT;
		}
		// If not mine then it is from sender to show blue background and align to left
		else {
			
			holder.smsFrame.setBackgroundResource(R.drawable.chat_bubble_recieved);
			holder.message.setTextColor(colorBlack);
			holder.date.setTextColor(colorBlack);
			lp.gravity = Gravity.LEFT;
		}
		holder.smsFrame.setLayoutParams(lp); 
		
		holder.message.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mQuickAction.show(v , true);
				mQuickAction.setOnActionItemClickListener(new OnActionItemClickListener() {
					@Override
					public void onItemClick(int pos) {
						// TODO Auto-generated method stub
						if(pos == 0)
							copyText(message.getText());
						else if(pos == 1)
							UIUtils.showToast(mContext, "Delete not implemented yet");
					}
				});
			}
		});
		return convertView;
	}

	private static class ViewHolder {
		TextView message; 
		TextView date; 
		LinearLayout smsFrame; 
	}
 
	@Override
	public long getItemId(int position) {
		// Unimplemented, because we aren't using Sqlite.
		return position;
	}
	
	private void copyText(String text) {
		// TODO Auto-generated method stub
		int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
		    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
		    clipboard.setText(text);
		} else {
		    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE); 
		    android.content.ClipData clip = android.content.ClipData.newPlainText("label", text);
		    clipboard.setPrimaryClip(clip);
		}
	}
}
