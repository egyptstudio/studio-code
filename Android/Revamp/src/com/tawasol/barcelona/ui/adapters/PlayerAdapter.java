/**
 * 
 */
package com.tawasol.barcelona.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.StudioFolder;

/**
 * @author Mohga
 * 
 */
public class PlayerAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<StudioFolder> list;
	DisplayImageOptions displayOption;
	Typeface tf;

	
	public PlayerAdapter(Context context, ArrayList<StudioFolder> list) {
		this.context = context;
		this.list    = list;
		displayOption = App.getInstance().getDisplayOption();/*new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.resetViewBeforeLoading(true)
		.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
		.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(5)).build();*/
		
		// Loading Font Face
		 tf = Typeface.createFromAsset(context.getAssets(),
				"fonts/FCBarcelonaFont2013ByHD.ttf");
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final playerViewHolder holder;
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new playerViewHolder();
			convertView = inflater.inflate(R.layout.player_list_item, parent , false);
			holder.playerImg = (ImageView) convertView.findViewById(R.id.player_image);
			holder.playerNoTxt = (TextView)convertView.findViewById(R.id.player_No_txt);
			holder.playerNameTxt = (TextView)convertView.findViewById(R.id.player_name_txt);
			holder.photosNumTxt = (TextView)convertView.findViewById(R.id.player_num_of_photos_txt);
			holder.playerPB = (ProgressBar)convertView.findViewById(R.id.player_progress_bar);
			convertView.setTag(holder);
		} else {
			holder = (playerViewHolder) convertView.getTag();
		}
		
				
				
		// set the image of the player
		App.getInstance()
		   .getImageLoader()
		   .displayImage(list.get(position).getFolderPicURL(), holder.playerImg,
			displayOption, new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					
				}
				
				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					holder.playerPB.setVisibility(View.GONE);
					holder.playerImg.setImageResource(R.drawable.ic_launcher);
					
				}
				
				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					holder.playerPB.setVisibility(View.GONE);
					
				}
				
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					
				}
			});
		
		
		// set the player number text  
		if(list.get(position).getRank() !=0)
		holder.playerNoTxt.setText(list.get(position).getRank()+"");
		else
			holder.playerNoTxt.setVisibility(View.GONE);
		
		
		// Applying font
		holder.playerNameTxt.setTypeface(tf);
		holder.photosNumTxt.setTypeface(tf);
		
		// set the player name
		holder.playerNameTxt.setText(list.get(position).getFolderName());
		
		// set the total number of pictures 
		holder.photosNumTxt.setText(list.get(position).getNoOfPics()+"");
		
		return convertView;
	}
	private class playerViewHolder {
		ImageView playerImg;
		TextView playerNoTxt;
		TextView photosNumTxt;
		TextView playerNameTxt;
		ProgressBar playerPB;
		
	}
	
}
