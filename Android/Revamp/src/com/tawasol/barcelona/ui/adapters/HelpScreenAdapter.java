package com.tawasol.barcelona.ui.adapters;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.HelpScreen;
import com.tawasol.barcelona.utils.ValidatorUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class HelpScreenAdapter extends PagerAdapter{
	
	private Context context;
	private ArrayList<HelpScreen> helpScreenList;

	public HelpScreenAdapter(Context context, ArrayList<HelpScreen> helpScreenList) {
		this.context = context;
		this.helpScreenList = helpScreenList;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public int getCount() {
		return helpScreenList.size();
	}

	@Override
	public Object instantiateItem(ViewGroup view, final int position) {
		View helpView = null;
		final HelpScreenViewHolder holder;

		
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			helpView = inflater.inflate(R.layout.help_screen_list_item, view , false);
			holder = new HelpScreenViewHolder();
			holder.helpScrnImg = (ImageView)helpView.findViewById(R.id.help_screen_img);
			holder.helpScrnIconImg =(ImageView)helpView.findViewById(R.id.help_screen_icon_img);
			holder.helpScrnTitleTxt =(TextView) helpView.findViewById(R.id.help_screen_title_txt);
			holder.helpScrnDescTxt=(TextView)helpView.findViewById(R.id.help_screen_description_txt);
			holder.helpScrnImgPB=(ProgressBar)helpView.findViewById(R.id.help_screen_img_pb);
			holder.helpScrnIconPB=(ProgressBar)helpView.findViewById(R.id.help_screen_icon_pb);
			
			helpView.setTag(holder);
		
		
		
		
		// set the Image 
		App.getInstance().getImageLoader().displayImage(helpScreenList.get(position).getScreenImageUrl(), holder.helpScrnImg, App.getInstance().getDisplayOption(), new ImageLoadingListener() {
			
			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				holder.helpScrnImgPB.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				holder.helpScrnImgPB.setVisibility(View.GONE);
				holder.helpScrnImg.setImageResource(R.drawable.ic_launcher);
			}
			
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				holder.helpScrnImgPB.setVisibility(View.GONE);
			}
			
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
			}
		});
		
		
		// set the Icon image 
		if(!ValidatorUtils.isRequired(helpScreenList.get(position).getScreenIconUrl())){
				App.getInstance().getImageLoader().displayImage(helpScreenList.get(position).getScreenIconUrl(), holder.helpScrnIconImg, App.getInstance().getDisplayOption(), new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String arg0, View arg1) {
						holder.helpScrnIconPB.setVisibility(View.VISIBLE);
					}
					
					@Override
					public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
						holder.helpScrnIconPB.setVisibility(View.GONE);
						holder.helpScrnIconImg.setImageResource(R.drawable.ic_launcher);
					}
					
					@Override
					public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
						holder.helpScrnIconPB.setVisibility(View.GONE);
					}
					
					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
					}
				});
		}
		else
			holder.helpScrnIconImg.setVisibility(View.GONE);
				
		// set the title 
		holder.helpScrnTitleTxt.setText(helpScreenList.get(position).getScreenTitle());
		
		// set the description 
		holder.helpScrnDescTxt.setText(helpScreenList.get(position).getDescription());

		view.addView(helpView);

		return helpView;
	}
	
	private class HelpScreenViewHolder
	{
		ImageView helpScrnImg;
		ImageView helpScrnIconImg;
		TextView helpScrnTitleTxt;
		TextView helpScrnDescTxt;
		ProgressBar helpScrnImgPB;
		ProgressBar helpScrnIconPB;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view.equals(object);
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}


}
