package com.tawasol.barcelona.ui.adapters;

import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox; 
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader; 
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer; 
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.ContactEntity;
import com.tawasol.barcelona.utils.ValidatorUtils;

/**
 * 
 * @author Turki
 *
 */
public class ContactListAdapter extends BaseExpandableListAdapter implements SectionIndexer {

	private Context context;
	public static ImageLoader imageLoader;
	private DisplayImageOptions options;
	private static String sections;
	private ProgressBar profileImageLoading;
	
	/** Define list of A-Z char **/
	private List<String> listDataHeader; 
	/** child data in format of Header Title, Child Title **/
	private HashMap<String, List<ContactEntity>> listDataChild;
	
	/** Initialize ContactListAdapter constructor **/
	@SuppressWarnings("deprecation")
	public ContactListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<ContactEntity>> listChildData) {
		this.context = context;
		this.listDataHeader = listDataHeader;
		this.listDataChild  = listChildData;
		imageLoader          = App.getInstance().getImageLoader();

		/** Initialize Options to display RoundedImage **/
		options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error)
				.showImageOnLoading(R.drawable.ic_empty)
				.resetViewBeforeLoading(true).cacheOnDisc(true)
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1000)).build();
		
		String section = null;
		for (int x = 0; x < listDataHeader.size(); x++) {
			section += listDataHeader.get(x);
		}
		
		if (section != null)
			sections = section.substring(4);
	}

	/** Get Group Count **/
	@Override
	public int getGroupCount() {
		return this.listDataHeader.size();
	}

	/** Get Children Count **/
	@Override
	public int getChildrenCount(int groupPosition) {
		return this.listDataChild.get(this.listDataHeader.get(groupPosition)).size();
	}

	/** Get Group Object **/
	@Override
	public Object getGroup(int groupPosition) {
		return this.listDataHeader.get(groupPosition);
	}

	/** Get Child Object from specific group&child position **/
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if (!listDataChild.isEmpty())
			return this.listDataChild.get(this.listDataHeader.get(groupPosition)).get(childPosition);
		else
			return null;
	}

	/** Get Group ID from specific group position **/
	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	/** Get Child ID from specific child position **/
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		/** Get header title **/
		String headerTitle = (String) getGroup(groupPosition);
		
		/** Initialize test_expandable_header.xml **/
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.test_expandable_header, null);
		}

		/** Initialize group section **/
		TextView lblListHeader   = (TextView) convertView.findViewById(R.id.groupName);
		LinearLayout groupLayout = (LinearLayout) convertView.findViewById(R.id.groupLayout);
		
		/** Hide Group if has no child **/
		hideGroupIfNoChild(groupPosition, groupLayout, lblListHeader, headerTitle);
		
		return convertView;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		final ContactEntity member = (ContactEntity) getChild(groupPosition, childPosition);
		
		View rootView     = convertView;
		ViewHolder holder = null;
		
		/** Initialize contact_item.xml **/
		if (rootView == null) {
			rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item, parent, false);
			holder = new ViewHolder();
			holder.childLayout  = (RelativeLayout) rootView.findViewById(R.id.childLayout);
			holder.profileImage = (ImageView) rootView.findViewById(R.id.profile_Image);
			holder.name         = (TextView) rootView.findViewById(R.id.name);
			holder.phone        = (TextView) rootView.findViewById(R.id.phone);
			holder.inviteCB     = (CheckBox) rootView.findViewById(R.id.invite_check_box);
			rootView.setTag(holder);
		} else {
			holder = (ViewHolder) rootView.getTag();
		}

		/** Hide Child if has Group is GONE **/
		if(parent.getVisibility() == View.VISIBLE)
			hideChildIfGroupIsGone(groupPosition, childPosition, holder, member, true);
		else
			hideChildIfGroupIsGone(groupPosition, childPosition, holder, member, false);
		return rootView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	/** Get all group sections **/
	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		String[] sectionsArr = new String[sections.length()];
		for (int i = 0; i < sections.length(); i++)
			sectionsArr[i] = "" + sections.charAt(i);
		return sectionsArr;

	}

	@SuppressLint("DefaultLocale")
	@Override
	public int getPositionForSection(int section) {
		if (section == 35) {
			return 0;
		}
		for (int i = 0; i < listDataHeader.size(); i++) {
			String l = listDataHeader.get(i);
			l.toUpperCase().charAt(0);
			char firstChar = sections.toUpperCase().charAt(i);
			if (firstChar == section) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/** Define ViewHolder **/
	class ViewHolder {
		ImageView profileImage;
		TextView name;
		TextView phone;
		CheckBox inviteCB;
		RelativeLayout childLayout;
	}
	
	private void hideGroupIfNoChild(int groupPosition, LinearLayout groupLayout,TextView lblListHeader, String headerTitle){
		/** Hide Group if has no child **/
		if(getChildrenCount(groupPosition) == 0){
			groupLayout.setVisibility(View.GONE);
		}else{
			groupLayout.setVisibility(View.VISIBLE);
			lblListHeader.setTypeface(null, Typeface.BOLD);
			lblListHeader.setText(headerTitle);
		}
	}
	
	private void hideChildIfGroupIsGone(final int groupPosition, final int childPosition, ViewHolder holder, ContactEntity member, boolean isVisible ){
		/** Hide Child if has Group is GONE **/
		if(isVisible){
			holder.childLayout.setVisibility(View.VISIBLE);
			holder.name.setText(member.getUserName());
			holder.phone.setText(member.getPhone());
		
			/** Check if item is selected or not to change CheckBox status **/
			if(member.isSelected())
				holder.inviteCB.setChecked(true);
			else
				holder.inviteCB.setChecked(false);
		
			/** Handle CheckBox status and set isSelected variable of ConatctEntity with result **/
			holder.inviteCB.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					// TODO Auto-generated method stub
					if (((CheckBox) view).isChecked()){
						listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).setSelected(true);
					}else{
						listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).setSelected(false);
					}
				}
			});
		
			/** Check if contact has photo or not to set it in ImageView **/
			if(!ValidatorUtils.isRequired(member.getContactImageURI())){
				imageLoader.displayImage(member.getContactImageURI(), holder.profileImage, options);
			}
		}else{
			holder.childLayout.setVisibility(View.INVISIBLE);
		}
	}
}
