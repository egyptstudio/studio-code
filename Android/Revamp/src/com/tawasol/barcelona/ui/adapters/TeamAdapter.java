/**
 * 
 */
package com.tawasol.barcelona.ui.adapters;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.StudioFolder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author Mohga
 * 
 */
public class TeamAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<StudioFolder> list;
	DisplayImageOptions displayOption;
	Typeface tf;

	
	public TeamAdapter(Context context, ArrayList<StudioFolder> list) {
		this.context = context;
		this.list    = list;
		displayOption = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.resetViewBeforeLoading(true)
		.showImageOnFail(R.drawable.ic_launcher)
		.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(5)).build();
		
		tf = Typeface.createFromAsset(context.getAssets(),
				"fonts/FCBarcelonaFont2013ByHD.ttf");
	}
	

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final TeamViewHolder holder;
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new TeamViewHolder();
			convertView = inflater.inflate(R.layout.team_list_item, parent , false);
			holder.teamImg = (ImageView) convertView.findViewById(R.id.team_image);
			holder.photosNumTxt = (TextView)convertView.findViewById(R.id.team_num_of_photos_txt);
			holder.teamPhotoNameTxt = (TextView)convertView.findViewById(R.id.team_name_txt);
			holder.teamPB = (ProgressBar)convertView.findViewById(R.id.team_progress_bar);
			
			convertView.setTag(holder);
		} else {
			holder = (TeamViewHolder) convertView.getTag();
		}
		
		// set the image of the team
		App.getInstance()
		.getImageLoader()
		.displayImage(
				list.get(position).getFolderPicURL(),
				holder.teamImg, displayOption,new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String arg0, View arg1) {						
					}
					
					@Override
					public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
						holder.teamPB.setVisibility(View.GONE);
						holder.teamImg.setImageResource(R.drawable.ic_launcher);
					}
					
					@Override
					public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
						holder.teamPB.setVisibility(View.GONE);
					}
					
					@Override
					public void onLoadingCancelled(String arg0, View arg1) {

					}
				});
		
		// Applying font
		holder.teamPhotoNameTxt.setTypeface(tf);
		holder.photosNumTxt.setTypeface(tf);
				
		// set the team Image name
		holder.teamPhotoNameTxt.setText(list.get(position).getFolderName());
		
		// set the total number of pictures 
		holder.photosNumTxt.setText(list.get(position).getNoOfPics()+"");
		
		return convertView;
	}
	private class TeamViewHolder {
		ImageView teamImg;
		TextView photosNumTxt;
		TextView teamPhotoNameTxt;
		ProgressBar teamPB;
		
	}
	
}
