/**
 * 
 */
package com.tawasol.barcelona.ui.adapters;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.ContestPrize;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author Mohga
 * 
 */
public class ContestPrizesAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<ContestPrize> list;
	DisplayImageOptions displayOption;

	
	public ContestPrizesAdapter(Context context, ArrayList<ContestPrize> list) {
		this.context = context;
		this.list    = list;
		displayOption = App.getInstance().getDisplayOption();
	}
	

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ContestPrizeViewHolder holder;
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new ContestPrizeViewHolder();
			convertView = inflater.inflate(R.layout.contest_prize_list_item, parent , false);
			holder.prizeTxt=(TextView)convertView.findViewById(R.id.contest_prize_txt);
			holder.prizeImg=(ImageView)convertView.findViewById(R.id.contest_prize_img);
			holder.prizePB=(ProgressBar)convertView.findViewById(R.id.contest_prize_pb);
			
			convertView.setTag(holder);
		} else {
			holder = (ContestPrizeViewHolder) convertView.getTag();
		}
		
		// set the image of the prize
		App.getInstance()
		.getImageLoader()
		.displayImage(
				list.get(position).getProzeImageUrl(),
				holder.prizeImg, displayOption,new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String arg0, View arg1) {						
					}
					
					@Override
					public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
						holder.prizePB.setVisibility(View.GONE);
						holder.prizeImg.setImageResource(R.drawable.ic_launcher);
					}
					
					@Override
					public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
						holder.prizePB.setVisibility(View.GONE);
					}
					
					@Override
					public void onLoadingCancelled(String arg0, View arg1) {

					}
				});
		
		
		// set the prize text name
		holder.prizeTxt.setText(list.get(position).getPrizeText());
		
		
		
		return convertView;
	}
	private class ContestPrizeViewHolder {
		TextView prizeTxt;
		ImageView prizeImg;
		ProgressBar prizePB;
		
	}
	
}
