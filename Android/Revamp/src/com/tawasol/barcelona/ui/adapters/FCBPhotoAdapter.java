/**
 * 
 */
package com.tawasol.barcelona.ui.adapters;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.FCBPhoto;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

/**
 * @author Turki
 * 
 */
public class FCBPhotoAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<FCBPhoto> list;

	static class FCBViewHolder {
		ImageView fcbPhotoImageView;
		ProgressBar progressBar;	 
	}
	
	public FCBPhotoAdapter(Context context, ArrayList<FCBPhoto> list) {
		this.context = context;
		this.list    = list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final FCBViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new FCBViewHolder();
			convertView = inflater.inflate(R.layout.fcb_photo_grid, parent , false);
			holder.fcbPhotoImageView = (ImageView) convertView.findViewById(R.id.grid_image);
			holder.progressBar       = (ProgressBar) convertView.findViewById(R.id.fcb_progress_bar);
			convertView.setTag(holder);
		} else {
			holder = (FCBViewHolder) convertView.getTag();
		}
		App.getInstance()
		   .getImageLoader()
		   .displayImage(list.get(position).getUrl(), holder.fcbPhotoImageView,
			App.getInstance().getDisplayOption(), new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					// TODO Auto-generated method stub
					holder.progressBar.setVisibility(View.GONE);
					holder.fcbPhotoImageView.setImageResource(R.drawable.image_sample);
				}
				
				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					// TODO Auto-generated method stub
					holder.progressBar.setVisibility(View.GONE);
				}
				
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}
			});
		return convertView;
	}
}
