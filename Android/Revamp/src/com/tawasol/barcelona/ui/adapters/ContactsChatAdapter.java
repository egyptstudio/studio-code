package com.tawasol.barcelona.ui.adapters;

import java.util.List;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView; 
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType; 
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.ChatContactsEntity; 

/**
 * 
 * @author Turki
 *
 */
public class ContactsChatAdapter  extends BaseAdapter{

	private Context context;
	private List<ChatContactsEntity> contacts;

	private ImageLoader mLoader;
	DisplayImageOptions options;

	public ContactsChatAdapter(Context context, List<ChatContactsEntity> contacts) {
		this.context  = context;
		this.contacts = contacts;
		mLoader =  App.getInstance().getImageLoader();
		
		/** Initialize Options to display RoundedImage **/
		options = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.fan_no_image)
			.showImageOnFail(R.drawable.fan_no_image)
			.showImageOnLoading(R.drawable.fan_no_image)
			.resetViewBeforeLoading(true)
			.cacheOnDisc(true)
			.cacheInMemory(true)
			.imageScaleType(ImageScaleType.EXACTLY)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.considerExifParams(true)
			.build();
	}

	@Override
	public int getCount() {
		return contacts.size();
	}

	@Override
	public Object getItem(int position) {
		return contacts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.chat_contacts_grid_item, parent, false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final ChatContactsEntity friend = (ChatContactsEntity) getItem(position);
		
		// Contact Name
		if (friend.getFanName() != null)
			holder.friendName.setText(friend.getFanName());
		else
			holder.friendName.setText("");

		// Number of posts
		if (friend.getFanPicNumber() > 0)
			holder.numberOfPic.setText(""+friend.getFanPicNumber());
		else
			holder.numberOfPic.setText("");
					
//		// Favorite
//		if (friend.isFavorite())
//			holder.favoriteImg.setImageResource(R.drawable.favourate);
//		else
//			holder.favoriteImg.setImageResource(R.drawable.unfavourate);
		
		// Followed
		if (friend.isFollowed())
			holder.followedImg.setImageResource(R.drawable.followed);
		else
			holder.followedImg.setImageResource(R.drawable.unfollowed);
			
		// Online
		if (friend.isFanOnline())
			holder.onlineImg.setVisibility(View.VISIBLE);
		else
			holder.onlineImg.setVisibility(View.GONE);
				
		// Online
		if (friend.isPremium())
			holder.premiumFrame.setBackgroundResource(R.drawable.premium_frame);
		else
			holder.premiumFrame.setBackgroundResource(R.drawable.basic_frame);

		// Contact Profile Img
		if (friend.getFanPicture() != null)
			mLoader.displayImage(friend.getFanPicture(), holder.profilePic, App.getInstance().getDisplayOption(), new ImageLoadingListener() {
				
			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				// TODO Auto-generated method stub	
			}
							
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				// TODO Auto-generated method stub
				holder.progressBar.setVisibility(View.GONE);
				holder.profilePic.setImageResource(R.drawable.image_sample);
			}
							
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				// TODO Auto-generated method stub
				holder.progressBar.setVisibility(View.GONE);
			}
							
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub	
			}
		});
		else
			holder.profilePic.setImageResource(R.drawable.image_sample);


		return convertView;
	}

	private static class ViewHolder {
		TextView friendName, numberOfPic;
		ImageView profilePic, onlineImg, followedImg, numberOfPicImg;
		FrameLayout premiumFrame;
		ProgressBar progressBar;

		public ViewHolder(View view) {
			friendName     = (TextView) view.findViewById(R.id.friend_name);
			numberOfPic    = (TextView) view.findViewById(R.id.number_of_pic_txt);
			profilePic     = (ImageView) view.findViewById(R.id.profile_pic);
			onlineImg      = (ImageView) view.findViewById(R.id.favorite_img);
			followedImg    = (ImageView) view.findViewById(R.id.followed_img);
			numberOfPicImg = (ImageView) view.findViewById(R.id.number_of_pic_img);
			premiumFrame   = (FrameLayout) view.findViewById(R.id.status_frame);
			progressBar    = (ProgressBar) view.findViewById(R.id.friend_progress_bar);
		}
	}
}

