package com.tawasol.barcelona.business.validation;

import com.tawasol.barcelona.exception.AppException;


public abstract class ValidationRule {

	public String ruleName;
	public String errorMessage;
	public boolean passed; 
	
	public abstract void validateRule() throws AppException; 
	
}