package com.tawasol.barcelona.business;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.connection.ParameterGenerator;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.data.connection.RestServiceAgent;
import com.tawasol.barcelona.data.helper.DataHelper;
import com.tawasol.barcelona.data.helper.VersionController;
import com.tawasol.barcelona.data.parsing.ParsingManager;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnEntityListReceivedListener;
import com.tawasol.barcelona.listeners.OnEntityReceivedListener;
import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
import com.tawasol.barcelona.listeners.UiListener;
import com.tawasol.barcelona.responses.BaseResponse;

/**
 * An abstraction of any business manager which should be parameterized with the
 * type of entity this manager deals with, this entity should be a subclass of
 * {@linkplain BusinessObject}.
 * 
 * @author Morabea
 * 
 * @param <T>
 *            An entity type with an "is-a" relationship with
 *            {@linkplain BusinessObject}.
 */
public abstract class BusinessManager<T extends BusinessObject> {

	private Class<T> mClass;
	
	/** A list of all ongoing actions that are still being processed, no action can be run twice at the same time
	 * At any time any modification or check operation must be atomic */
	private List<Action> mActionsList;
	
	/**
	 * list of all listener used across the application as a centralized place
	 * add your listener to this list
	 */
	private List<UiListener> Uilisteners;

	/**
	 * Acts as a default constructor which should be called in all constructors
	 * of children managers.
	 * 
	 * @param clazz Class instance of the entity upon which the manager is based on.
	 */
	protected BusinessManager(Class<T> clazz) {
		this.mClass = clazz;
		mActionsList = new ArrayList<Action>();
		Uilisteners = new ArrayList<UiListener>();
	}
	
	/**
	 * add your listener to UiListeners list using this method
	 * @param listener
	 */
	public void addListener(UiListener listener) {
		if (!Uilisteners.contains(listener))
			Uilisteners.add(listener);
	}
	
	/**
	 * remove your listener from UiListeners using this method
	 * @param listener
	 */
	public void removeListener(UiListener listener) {
		if (Uilisteners.contains(listener))
			Uilisteners.remove(listener);
	}
	
	/**
	 * A method to get UiListeners list
	 * @return
	 */
	protected List<UiListener> getListeners() {
		return Uilisteners;
	}
	
	
	protected <D extends OnSuccessVoidListener> void notifyVoidSuccess(final Class<D> listenerClass) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener listener : getListeners())
					if (listenerClass.isInstance(listener))
						((OnSuccessVoidListener) listener).onSuccess();
			}
		});
	}
	
	@SuppressWarnings("hiding")
	protected <T, D extends OnEntityReceivedListener<?>> void notifyEntityReceviedSuccess(final T entity,
			final Class<D> listenerClass) {
		App.getInstance().runOnUiThread(new Runnable() {
			// Added by Mahmoud Elmorabea
			// A warning of unchecked casting is suppressed here
			// because the compiler cannot know for sure that the passed listener type's generic T
			// is of the same one that is being cast to
			// So you're just gonna have to trust me on that one :)
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				for (UiListener listener : getListeners())
					if (listenerClass.isInstance(listener))
						((OnEntityReceivedListener<T>) listener).onSuccess(entity);
			}
		});
	}
	
	/**
	 * Same as {@link #notifyEntityReceviedSuccess(Object, Class)} but instead of an entity object,
	 * it returns a {@link List} of entities.
	 * @param entityList The received entities list.
	 * @param listenerClass {@link Class} object of the desired listeners to be notified.
	 */
	@SuppressWarnings("hiding")
	protected <T, D extends OnEntityListReceivedListener<?>> void notifyEntityListReceviedSuccess(final List<T> entityList, 
			final Class<D> listenerClass) {
		App.getInstance().runOnUiThread(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				for (UiListener listener : getListeners())
					if (listenerClass.isInstance(listener))
						((OnEntityListReceivedListener<T>) listener).onSuccess(entityList);
			}
		});
	}
	
	/**
	 * Same as {@link #notifyEntityReceviedSuccess(Object, Class)} but notifies the listeners of an exception
	 * that has occurred.
	 * @param e
	 * @param listenerClass
	 */
	@SuppressWarnings("hiding")
	protected <T extends UiListener> void notifyRetrievalException(final AppException e, 
			final Class<T> listenerClass) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener listener : getListeners())
					if (listenerClass.isInstance(listener))
						((UiListener) listener).onException(e);
			}
		});
	}
	
	/**
	 * Indicates whether this action needs processing or not.<br>
	 * If this method returns false, no further action needs to be taken.
	 * @param action 
	 * @return True if the action isn't being processed, false otherwise.
	 */
	protected boolean needsProcessing(Action action) {
		synchronized (mActionsList) { return !mActionsList.contains(action); }
	}
	
	/**
	 * Should be called at the start of any action [Like, Repost, Follow]
	 * @param action Action that is being processed.
	 */
	protected void notifyActionStarted(Action action) {
		if (mActionsList.contains(action))
			throw new RuntimeException("This BusinessManager#Action is already being processed");
		
		synchronized (mActionsList) { mActionsList.add(action); }
	}

	/**
	 * Should be called after an action is done being processed by either success or failure.
	 * @param action Action that is done being processed.
	 */
	protected void notifyActionDone(Action action) {
		if (!mActionsList.contains(action))
			throw new RuntimeException("Ending a BusinessManager#Action that isn't being processed");
		
		synchronized (mActionsList) { mActionsList.remove(action); }
	}

	/**
	 * Returns an entity object by id.
	 * 
	 * @param id
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public T getObject(int id) throws AppException,
			InstantiationException, IllegalAccessException {
		return getObjectFromServer(id);
	}

	public <D extends BusinessObject> D getObject(Map<String, Object> params,
			String method, Class<D> clazz) throws AppException {
		return getObjectFromServer(params, method, clazz);
	}

	private T getObjectFromServer(int id) throws AppException,
			InstantiationException, IllegalAccessException {
		// Constructing the map with id parameter to generate JSON
		Map<String, Object> paramMap = new HashMap<String, Object>(1);
		paramMap.put(Params.BusinessObject.ID, String.valueOf(id));
		return getObjectFromServer(paramMap, mClass.newInstance().GET_METHOD,
				mClass);
	}

	private <D extends BusinessObject> D getObjectFromServer(
			Map<String, Object> params, String method, Class<D> clazz)
			throws AppException {
		try {
			String result = RestServiceAgent
					.post(getBaseUrl(), method, ParameterGenerator.getParamMap(DataHelper
							.formatAsJSON(params)));
			BaseResponse response = ParsingManager.parseServerResponse(result);
			return handleServerResponse(response, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}

	private <D extends BusinessObject> D handleServerResponse(
			BaseResponse response, Class<D> clazz) throws AppException {
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA)
			return DataHelper.deserialize(response.getData(), clazz);
		else if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
			return null;
		else if(response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
			throw new AppException(response.getValidationRule().errorMessage);
		else
			throw new AppException(BaseResponse.getExceptionType(response.getStatus()));
	}

	/**
	 * Returns a list of all entity objects.
	 * 
	 * @param methodName
	 *            Target resource at server.
	 * @return
	 * @throws AppException
	 */
	public List<T> getObjectList(String methodName) throws AppException {
		return getObjectList(methodName, null);
	}

	/**
	 * Returns a list of all entity objects for web services that need extra
	 * parameters.
	 * 
	 * @param methodName
	 * @param params
	 * @return
	 * @throws AppException
	 */
	public List<T> getObjectList(String methodName, Map<String, Object> params)
			throws AppException {
		return getObjectList(methodName, params, mClass);
	}

	/**
	 * Returns a list of all entity objects for web services that need extra
	 * parameters.
	 * 
	 * @param methodName
	 * @param params
	 * @param class type
	 * @return
	 * @throws AppException
	 */
	public <D extends BusinessObject> List<D> getObjectList(String methodName,
			Map<String, Object> params, Class<D> clazz)
			throws AppException {
		try {
			String result = RestServiceAgent.post(getBaseUrl(), methodName,
					ParameterGenerator.getParamMap(DataHelper
							.formatAsJSON(params)));
			BaseResponse response = ParsingManager.parseServerResponse(result);
			return handleObjectListServerResponse(response, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}
	
	private <D extends BusinessObject> List<D> handleObjectListServerResponse(
			BaseResponse response, Class<D> clazz) throws AppException {
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA){
			if(response.getData().equals("[]")){//TODO remove
				throw new AppException(AppException.NO_DATA_EXCEPTION);
			}
			return (List<D>) DataHelper.deserializeList(response.getData(),
					clazz);
		}
		else if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
			throw new AppException(
					response.getValidationRule().errorMessage);
		else
			throw new AppException(
					BaseResponse.getExceptionType(response.getStatus()));
	}

	/**
	 * Saves an entity object to server which is of the same type as the manager
	 * was parameterized.
	 * 
	 * @param obj
	 */
	public void saveObject(T obj) throws AppException {
		saveObjectToServer(obj, mClass);
	}

	/**
	 * Saves an entity object to server which is of a type different from the
	 * type the manager was parameterized.
	 * 
	 * @param obj
	 * @param clazz
	 * @throws AppException
	 */
	public <D extends BusinessObject> void saveObject(D obj, Class<D> clazz)
			throws AppException {
		saveObjectToServer(obj, clazz);
	}

	private <D extends BusinessObject> void saveObjectToServer(D obj,
			Class<D> clazz) throws AppException {
		BaseResponse response = submit(obj, obj.SAVE_METHOD, clazz);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(
						BaseResponse.getExceptionType(response.getStatus()));
		}
	}

	/**
	 * Deletes an entity object.
	 * 
	 * @param id
	 */
	public void deleteObject(int id) throws AppException {
		// TODO
	}

	/**
	 * Submits an entity object to server which is of a type different from the
	 * type the manager was parameterized.
	 * 
	 * @param obj
	 * @param method
	 * @param clazz
	 * @return
	 * @throws AppException
	 */
	public <D extends BusinessObject> BaseResponse submit(D obj, String method,
			Class<D> clazz) throws AppException {
		obj.validateObject(App.getInstance().getApplicationContext());
		return submit(method, DataHelper.serialize(obj, clazz));
	}
	
	public <D extends BusinessObject> BaseResponse submitList(List<D> list, String method,
			Class<D> clazz) throws AppException {
		return submit(method, DataHelper.serializeList(list, clazz));
	}

	/**
	 * Saves an entity object to server which is of a type different from the
	 * type the manager was parameterized with a file attachment.
	 * 
	 * @param obj
	 * @param file
	 * @param method
	 * @param clazz
	 * @return
	 * @throws AppException
	 */
	public <D extends BusinessObject> BaseResponse submit(D obj, File file,
			String method, Class<D> clazz) throws AppException {
		obj.validateObject(App.getInstance().getApplicationContext());
		return submit(method, DataHelper.serialize(obj, clazz), file);
	}
	/**
	 * Saves an entity object to server which is of a type different from the
	 * type the manager was parameterized with a file attachment and params list.
	 * 
	 * @param obj
	 * @param file
	 * @param params
	 * @param method
	 * @param clazz
	 * @return
	 * @throws AppException
	 */
	public <D extends BusinessObject> BaseResponse submit(D obj, File file,Map<String, Object> params,
			String method, Class<D> clazz) throws AppException {
		obj.validateObject(App.getInstance().getApplicationContext());
		
		try {
			return submit(method, DataHelper.serialize(obj, clazz), file,DataHelper.formatAsJSON(params));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new AppException(
					AppException.JSON_PARSING_EXCEPTION);
		}
	}

	/**
	 * Saves an entity object to server which is of the type the manager was
	 * parameterized with.
	 * 
	 * @param obj
	 * @param method
	 * @return
	 * @throws AppException
	 */
	public BaseResponse submit(T obj, String method)
			throws AppException {
		return submit(obj, method, mClass);
	}

	/**
	 * Saves an entity object to server which is of the type the manager was
	 * parameterized with, with a file attachment.
	 * 
	 * @param obj
	 * @param method
	 * @return
	 * @throws AppException
	 */
	public BaseResponse submit(T obj, File file, String method)
			throws AppException {
		return submit(obj, file, method, mClass);
	}

	/**
	 * Submits a request to server with parameters supplied in the map.<br>
	 * <br>
	 * <b>NOTE:</b> <i> If the value of any parameter is an entity object
	 * itself, it must be passed as JSON string formatted using
	 * {@linkplain DataHelper#serialize(BusinessObject, Class)} </i>
	 * 
	 * @param params
	 * @param method
	 * @return
	 * @throws AppException
	 */
	public BaseResponse submit(Map<String, Object> params, String method)
			throws AppException {
		try {
			return submit(method, DataHelper.formatAsJSON(params));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new AppException(
					AppException.JSON_PARSING_EXCEPTION);
		}
	}

	/**
	 * Submits a request to server with parameters supplied in the map with a
	 * file attachment.<br>
	 * <br>
	 * <b>NOTE:</b> <i> If the value of any parameter is an entity object
	 * itself, it must be passed as JSON string formatted using
	 * {@linkplain DataHelper#serialize(BusinessObject, Class)} </i>
	 * 
	 * @param params
	 * @param file
	 * @param method
	 * @return
	 * @throws AppException
	 */
	public BaseResponse submit(Map<String, Object> params, File file,
			String method) throws AppException {
		try {
			return submit(method, DataHelper.formatAsJSON(params), file);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new AppException(
					AppException.JSON_PARSING_EXCEPTION);
		}
	}
	
	public BaseResponse submitPosterFiles(Map<String, Object> params, File file,File file2,
			String method) throws AppException {
		try {
			return submitFiles(method, DataHelper.formatAsJSON(params), file,file2);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new AppException(
					AppException.JSON_PARSING_EXCEPTION);
		}
	}
	private BaseResponse submitFiles(String method, Object... params)
			throws AppException {
		try {
			String result = RestServiceAgent.post(getBaseUrl(), method,
					ParameterGenerator.getPosterParamMap(params));
			return ParsingManager.parseServerResponse(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}
	

	private BaseResponse submit(String method, Object... params)
			throws AppException {
		try {
			String result = RestServiceAgent.post(getBaseUrl(), method,
					ParameterGenerator.getParamMap(params));
			return ParsingManager.parseServerResponse(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}
	
	private static String getBaseUrl() throws AppException {
		return VersionController.getBaseUrl();
	}
	
	/**
	 * An action that's being processed by a BusinessManager
	 * @author Morabea
	 */
	public static class Action {
		
		public static enum ActionType {
			LIKE , REPOST , FOLLOW, GET_POSTS, GET_CHAT_MESSAGES, GET_NOTIFICATIONS;
		}
		
		private ActionType type;
		private Object associatedObj;
		
		/**
		 * @param type {@link ActionType#LIKE}, {@link ActionType#REPOST}, {@link ActionType#FOLLOW}
		 * @param associatedId
		 */
		public Action(ActionType type, Object associatedObj) {
			this.type = type;
			this.associatedObj = associatedObj;
		}
		
		public ActionType getType() {
			return type;
		}
		
		public Object getAssociatedObj() {
			return associatedObj;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((associatedObj == null) ? 0 : associatedObj.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Action other = (Action) obj;
			if (associatedObj == null) {
				if (other.associatedObj != null)
					return false;
			} else if (!associatedObj.equals(other.associatedObj))
				return false;
			if (type != other.type)
				return false;
			return true;
		}
		
	}

}
