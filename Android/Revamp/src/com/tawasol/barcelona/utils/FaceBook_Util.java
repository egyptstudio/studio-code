package com.tawasol.barcelona.utils;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.OnErrorListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.exception.LinkMissingException;
import com.tawasol.barcelona.listeners.OnLoginResponseListener;

/**
 * A class that handle both sharing a picture ot status on facebook using
 * postImage and postStatusUpdate -------------------------------- in your
 * activity in on create make an object of this class and give it (activity
 * object , savedInstanceState bundle) then in on activity result put this line
 * faceBook.result(requestCode, resultCode, data); then call postImage(give it
 * pic path on sd , or resource file i.e R.drawable.ic_launcher , and a required
 * type (from_sd or from_resources)) to post images or call
 * PostStatusUpdate(title , caption , description , and a required link)
 * 
 * @author Basyouni
 * 
 */
public class FaceBook_Util {

	private static Activity activity;
	public static final int FROM_SD = 1;
	public static final int FROM_RESOURCES = 2;
	private static String path;
	// define the publish permission and life-cycle helper
	private static final List<String> PERMISSIONS = Arrays
			.asList("user_birthday","user_hometown","user_location");
	private static final String PERMISSION = "publish_actions";
	private final String PENDING_ACTION_BUNDLE_KEY = "com.tawasol.share:PendingAction";
	private PendingAction pendingAction = PendingAction.NONE;
	private boolean canPresentShareDialogWithPhotos;
	private boolean canPresentShareDialog;
	private String title;
	private String caption;
	private String description;
	private String link;
	private static int type;
	private static int resource;
	static Session session;
	

	private enum PendingAction {
		NONE, POST_PHOTO, POST_STATUS_UPDATE
	}

	private static UiLifecycleHelper uiHelper;

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall,
				Exception error, Bundle data) {
			Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall,
				Bundle data) {
			Log.d("HelloFacebook", "Success!");
		}
	};

	public FaceBook_Util(Activity activity, Bundle savedInstanceState) {
		FaceBook_Util.activity = activity;
		uiHelper = new UiLifecycleHelper(activity, callback);
		uiHelper.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			String name = savedInstanceState
					.getString(PENDING_ACTION_BUNDLE_KEY);
			pendingAction = PendingAction.valueOf(name);
		}

		canPresentShareDialogWithPhotos = FacebookDialog.canPresentShareDialog(
				activity, FacebookDialog.ShareDialogFeature.PHOTOS);
		canPresentShareDialog = FacebookDialog.canPresentShareDialog(activity,
				FacebookDialog.ShareDialogFeature.SHARE_DIALOG);
	}

	public FaceBook_Util() {
	}

	/**
	 * call this method when you want to share image to facebook you got two
	 * options if you want to share using image path from your phone give it the
	 * path and give resource = zero and type FROM_SD --------------------------
	 * option two if you want to share from drawable folder give string = null
	 * and give id of resource file (i.e R.id.ic_launcher) and give type
	 * FROM_RESOURCE
	 * 
	 * @param path
	 * @param resource
	 * @param type
	 */
	public void postImage(String path, int resource, int type) {
		FaceBook_Util.type = type;
		FaceBook_Util.path = path;
		FaceBook_Util.resource = resource;
		performPublish(PendingAction.POST_PHOTO,
				canPresentShareDialogWithPhotos);
	}

	private interface GraphObjectWithId extends GraphObject {
		String getId();
	}

	public void result(int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	}

	/*
	 * public void loginresult(int requestCode, int resultCode, Intent data) {
	 * uiHelper.onActivityResult(requestCode, resultCode, data); }
	 */

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {

			Log.i("bb", "Logged in...");
		} else if (state.isClosed()) {
			Log.i("bb", "Logged out...");
		}
		if (pendingAction != PendingAction.NONE
				&& (exception instanceof FacebookOperationCanceledException || exception instanceof FacebookAuthorizationException)) {
			new AlertDialog.Builder(activity).setTitle(R.string.cancelled)
					.setMessage(R.string.permission_not_granted)
					.setPositiveButton(R.string.PhotoDetails_OK, null).show();
			pendingAction = PendingAction.NONE;
		} else if (state == SessionState.OPENED_TOKEN_UPDATED) {
			handlePendingAction();
		}
	}

	@SuppressWarnings("incomplete-switch")
	private void handlePendingAction() {
		PendingAction previouslyPendingAction = pendingAction;
		// These actions may re-set pendingAction if they are still pending, but
		// we assume they
		// will succeed.
		pendingAction = PendingAction.NONE;

		switch (previouslyPendingAction) {
		case POST_PHOTO:
			postPhoto();
			break;
		case POST_STATUS_UPDATE:
			postStatusUpdate();
			break;
		}
	}

	private FacebookDialog.ShareDialogBuilder createShareDialogBuilderForLink() {
		return new FacebookDialog.ShareDialogBuilder(activity).setName(title)
				.setCaption(caption).setDescription(description).setLink(link);
	}

	private void postStatusUpdate() {
		if (canPresentShareDialog) {
			FacebookDialog shareDialog = createShareDialogBuilderForLink()
					.build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		} else if (hasPublishPermission()) {
			Request request = Request.newStatusUpdateRequest(
					Session.getActiveSession(), "status share",
					new Request.Callback() {
						@Override
						public void onCompleted(Response response) {
							System.out.println("success");
							showPublishResult("Stauts Share",
									response.getGraphObject(),
									response.getError());
						}
					});
			request.executeAsync();

		} else {
			pendingAction = PendingAction.POST_STATUS_UPDATE;
		}
	}

	private void showPublishResult(String message, GraphObject result,
			FacebookRequestError error) {
		String title = null;
		String alertMessage = null;
		if (error == null) {
			title = "success";
			String id = result.cast(GraphObjectWithId.class).getId();
			alertMessage = activity.getString(
					R.string.successfully_posted_post, message, id);
		} else {
			title = "error";
			alertMessage = error.getErrorMessage();
		}

		new AlertDialog.Builder(activity).setTitle(title)
				.setMessage(alertMessage).setPositiveButton(R.string.PhotoDetails_OK, null)
				.show();
	}

	private FacebookDialog.PhotoShareDialogBuilder createShareDialogBuilderForPhoto(
			Bitmap... photos) {
		return new FacebookDialog.PhotoShareDialogBuilder(activity)
				.addPhotos(Arrays.asList(photos));
	}

	/**
	 * call this method when you want to share status on facebook you have to
	 * provide optional title , optional caption , optional description and a
	 * required link in case you forgot to provide it it will throw
	 * linkMissingException
	 * 
	 * @param title
	 * @param caption
	 * @param description
	 * @param link
	 * @throws LinkMissingException
	 */
	public void postStatusUpdate(String title, String caption,
			String description, String link) throws LinkMissingException {
		this.title = title;
		this.caption = caption;
		this.description = description;
		if (link == null)
			throw new LinkMissingException();
		this.link = link;
		performPublish(PendingAction.POST_STATUS_UPDATE, canPresentShareDialog);
	}

	private void postPhoto() {
		Bitmap image = null;
		if (type == FaceBook_Util.FROM_SD && path != null)
			image = BitmapFactory.decodeFile(path);
		else if (type == FaceBook_Util.FROM_RESOURCES && resource != 0) {
			image = BitmapFactory.decodeResource(activity.getResources(),
					resource);
		}

		if (canPresentShareDialogWithPhotos) {
			FacebookDialog shareDialog = createShareDialogBuilderForPhoto(image)
					.build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		} else if (hasPublishPermission()) {
			Request request = Request.newUploadPhotoRequest(
					Session.getActiveSession(), image, new Request.Callback() {
						@Override
						public void onCompleted(Response response) {
							showPublishResult("Photo Post",
									response.getGraphObject(),
									response.getError());
						}
					});
			request.executeAsync();
		} else {
			pendingAction = PendingAction.POST_PHOTO;
		}
	}

	private boolean hasPublishPermission() {
		Session session = Session.getActiveSession();
		return session != null
				&& session.getPermissions().contains("publish_actions");
	}

	private void performPublish(PendingAction action, boolean allowNoSession) {
		Session session = Session.getActiveSession();
		if (session != null) {
			pendingAction = action;
			if (hasPublishPermission()) {
				// We can do the action right away.
				handlePendingAction();
				return;
			} else if (session.isOpened()) {
				// We need to get new permissions, then complete the action when
				// we get called back.
				session.requestNewPublishPermissions(new Session.NewPermissionsRequest(
						activity, PERMISSION));
				return;
			}
		}

		if (allowNoSession) {
			pendingAction = action;
			handlePendingAction();
		}
	}

	/*** LOG IN Part ***/
	/**************************************************************/
	private WeakReference<OnLoginResponseListener> faceListener;

	/*
	 * this method take LoginButton of faceBook and faceBook listeners it handle
	 * both login successful and login fail
	 */
	public void faceBookLogin(final Activity activity , LoginButton authButton,
			OnLoginResponseListener listener) {
		
//		faceBookLogout();
		faceListener = new WeakReference<OnLoginResponseListener>(listener);
		//authButton.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
		authButton.setOnErrorListener(new OnErrorListener() {

			@Override
			public void onError(FacebookException error) {
				if (faceListener.get() != null)
					faceListener.get().onException(new AppException(error.getMessage()));//.onFaceBookLoginFailed(error);
			}
		});
		// set permission list, Don't forget to add email
		authButton.setReadPermissions(Arrays.asList("email"));
		
		// session state call back event
		authButton.setSessionStatusCallback(new Session.StatusCallback() {
			@Override
			public void call(final Session session, SessionState state,
					Exception exception) {

				FaceBook_Util.session = session;
				//Session session = Session.getActiveSession();
				if (session != null &&  session.isOpened() ) {
					List<String> permissions = session.getPermissions();
				if (!isSubsetOf(PERMISSIONS, permissions)){
					addLoginPermissions(activity , session);
					return;
				}
				}
				/*
				 * if(!session.isOpened()) session =
				 * Session.openActiveSession(activity, true, this);
				 */
				if (session.isOpened()) {
					Request.newMeRequest(session,
							new Request.GraphUserCallback() {
								@Override
								public void onCompleted(GraphUser user,
										Response response) {
									if (user != null) {

										User faceBookUser = new User();
									
										faceBookUser.setPhone("");
										faceBookUser.setDistrict("");
										faceBookUser.setCountryName("");
										
										// set the user full name 
										faceBookUser.setFullName(user
												.getFirstName()
												+ " "
												+ user.getLastName());
										// set the user email
										if(user.getProperty("email") != null)
											faceBookUser.setEmail(user.getProperty(
												"email").toString());
										else 
											faceBookUser.setEmail("");
										
									
										// set the user profile pic url
										faceBookUser
												.setProfilePic("http://graph.facebook.com/"
														+ user.getId()
														+ "/picture?=large");
										
										// set social media id 
										faceBookUser.setSocialMediaID(user
												.getId());
										
										// set user type
										faceBookUser
												.setUserType(User.USER_TYPE_SOCIAL_MEDIA);
										
										// ------- Setting  Gender -----
										try{
										if(user.getProperty("gender").toString().toLowerCase().equals("female"))
											faceBookUser.setGender(2);
								 		else if(user.getProperty("gender").toString().toLowerCase().equals("male"))
										faceBookUser.setGender(1);
										else
											faceBookUser.setGender(0);
										}
										catch (Exception e) {
											faceBookUser.setGender(0);
										}
										// ------- end of setting gender ------
										
										
										// set user BD
										try{
										faceBookUser.setDateOfBirth(Utils.dateStringToLong(user.getBirthday()));
										}
										catch (Exception e) {
											faceBookUser.setDateOfBirth(0);
										}
										// ------ setting user location -------
										try {
											
											
										if(user.asMap().get("hometown") != null) // first through home town
										{
											JSONObject obj = new JSONObject(user.asMap().get("hometown").toString());
											
											String homeTown = obj.getString("name");
											
											String[] places = homeTown.trim().split(",");
											if(places != null && places.length >=2)
											{
												faceBookUser.setCountryName(places[places.length-1]);
												 faceBookUser.setCityName(places[places.length-2]);
											}
											else 
											{
												if(user.getLocation() != null && user.getLocation().getCountry() != null  ){
													faceBookUser.setCountryName(user.getLocation().getCountry());
													if(user.getLocation().getCity() != null)
														faceBookUser.setCityName(user.getLocation().getCity());
												}
												else if(user.getLocation() != null )
												{
													String locationDetails = (String) user.getLocation().getProperty("name");
													if(! ValidatorUtils.isRequired(locationDetails))
													{
														places = locationDetails.trim().split(",");
														if(places != null && places.length >=2)
														{
															faceBookUser.setCountryName(places[places.length-1]);
															 faceBookUser.setCityName(places[places.length-2]);
														}
													}
															
												}
											}
										}
										else // if couldn't set the location from hometown , set it through user location
										{

											
											if(user.getLocation() != null && user.getLocation().getCountry() != null  ){
												faceBookUser.setCountryName(user.getLocation().getCountry());
												if(user.getLocation().getCity() != null)
													faceBookUser.setCityName(user.getLocation().getCity());
											}
											else if(user.getLocation() != null )
											{
												String locationDetails = (String) user.getLocation().getProperty("name");
												if(! ValidatorUtils.isRequired(locationDetails))
												{
													String[]places = locationDetails.trim().split(",");
													if(places != null && places.length >=2)
													{
														faceBookUser.setCountryName(places[places.length-1]);
														 faceBookUser.setCityName(places[places.length-2]);
													}
												}
														
											}
										}
										} catch (JSONException e) {
											
											if(user.getLocation() != null && user.getLocation().getCountry() != null  ){
												faceBookUser.setCountryName(user.getLocation().getCountry());
												if(user.getLocation().getCity() != null)
													faceBookUser.setCityName(user.getLocation().getCity());
											}
											else if(user.getLocation() != null )
											{
												String locationDetails = (String) user.getLocation().getProperty("name");
												if(! ValidatorUtils.isRequired(locationDetails))
												{
													String[]places = locationDetails.trim().split(",");
													if(places != null && places.length >=2)
													{
														faceBookUser.setCountryName(places[places.length-1]);
														 faceBookUser.setCityName(places[places.length-2]);
													}
												}
														
											}
										
										}
										
										// ------ End Of Setting location ----- 
										
										if (faceListener.get() != null)
											faceListener.get().onLoginSuccess(
													faceBookUser,User.USER_TYPE_SOCIAL_MEDIA);
										session.close();
				                        session.closeAndClearTokenInformation();

									}
								}
							}).executeAsync();
				} 
//				else
//					faceBookLogout(FaceBook_Util.LOG_OUT_EX);

			}
		});
	}

	
	public static void faceBookLogout() {
//		if (LogoutType == FaceBook_Util.LOG_OUT_EX) {
//			if (session != null)
//				if (session.isOpened()) {
//					session.closeAndClearTokenInformation();
//					session = null;
//				}
//		} else if (LogoutType == FaceBook_Util.LOG_OUT_WALL)
//			if (Session.getActiveSession() != null)
//				Session.getActiveSession().closeAndClearTokenInformation();
//		Utility.clearFacebookCookies(activity);
		
		Session session = Session.getActiveSession();
	    if (session != null) {

	        if (!session.isClosed()) {
	            session.closeAndClearTokenInformation();
	            //clear your preferences if saved
	        }
	    }
	}
	
	private void addLoginPermissions(Activity activity , Session session) {
		Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
				activity, PERMISSIONS);
		try {
			session.requestNewReadPermissions(newPermissionsRequest);//NewPublishPermissions(newPermissionsRequest);
		} catch (UnsupportedOperationException e) {
			
		}
	}

	
	private boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	/**************************************************************/
	/*** End Of LOG IN Part ***/

	/**
	 * Logout From Facebook
	 */
	/*
	 * public void FacebookLogout(Context context) { Session session =
	 * Session.getActiveSession(); if (session != null) {
	 * 
	 * if (!session.isClosed()) { session.closeAndClearTokenInformation();
	 * //clear your preferences if saved } } else {
	 * 
	 * session = new Session(context); Session.setActiveSession(session);
	 * 
	 * session.closeAndClearTokenInformation(); //clear your preferences if
	 * saved
	 * 
	 * }
	 * 
	 * }
	 */
}
