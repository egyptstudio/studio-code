package com.tawasol.barcelona.utils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnLoginResponseListener;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.ui.activities.ShareActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * A class that handle twitter Log In 
 * it includes getting Authorization verifier 
 * and User data of specific verifier as well
 * @author Mohga 
 * 
 */
public class Twitter_Util {

	// twitter listener
	private WeakReference<OnLoginResponseListener> twitterListener;
	
	// Twitter Variables 
	private Context context;
	
	// authriozations keys 
	private static final String TWITTER_CONSUMER_KEY = "0aae8rnhU8FMHMWbqBu3d702m";
	private static final String TWITTER_CONSUMER_SECRET="vuq7zRZB22jqxigrCNjNNU1CROWX3psveukNSHec5U74KeECFZ";
	// twitter declarations 
		String oauth_verifier;
		    static final String TWITTER_CALLBACK_URL = "api.twitter.com/oauth/authorize?force_login=true";//oauth://t4jsample //T4J_OAuth://callback_main
	    // Twitter oauth urls
	    static final String URL_TWITTER_AUTH = "http://twitter.com/oauth/authorize?force_login=true";
	    static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	    static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
	    // Preference Constants
	    static String PREFERENCE_NAME = "twitter_oauth";
	    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	    static final String PREF_KEY_OAUTH_USER_ID="oauth_token_user_id";
	    static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
	   // Shared Preferences
	    private static SharedPreferences mSharedPreferences;
	    
	    WebView twitterLoginWebView;
	    private static Twitter twitter;
	    private static RequestToken requestToken;
	    Dialog auth_dialog;
	
	public Dialog getAuth_dialog() {
			return auth_dialog;
		}
		public void setAuth_dialog(Dialog auth_dialog) {
			this.auth_dialog = auth_dialog;
		}
	// Constructor
	public Twitter_Util(Context context) {
		this.context=context;
		// prepare twitter login
					// Shared Preferences
		twitter = null;
			        mSharedPreferences = context.getSharedPreferences(
			                "MyPref", 0);
			        
			        ConfigurationBuilder builder = new ConfigurationBuilder();
		            builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
		            builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
		            builder.setOAuthAuthorizationURL(URL_TWITTER_AUTH);
		           // builder.setOAuthAccessToken(null);
		            Configuration configuration = builder.build();
		             
		            Log.v("test: ", configuration.getOAuthAuthorizationURL());
		            TwitterFactory factory = new TwitterFactory(configuration);
		            twitter = factory.getInstance();
		            
		            auth_dialog = new Dialog(context);
		       		auth_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	}
	
	/**
     * Function to login twitter   
     * */
    public void loginToTwitter(OnLoginResponseListener twitter_Listener,boolean saveCredintials) {
        // Check if already logged in
     //   if (!isTwitterLoggedInAlready())
        {
        	twitterListener = new WeakReference<OnLoginResponseListener>(twitter_Listener);
            new get_outh().execute(saveCredintials);
            
        } 
//        else
//        {
//        	// get the saved data !
//        	String outh_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, null);
//        	String outh_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, null);
//        	String outh_user_id = mSharedPreferences.getString(PREF_KEY_OAUTH_USER_ID, null);
//        	Toast.makeText(context, "loggedin", Toast.LENGTH_LONG).show();
//        	if(outh_token != null && outh_secret != null && outh_user_id != null)
//        	{
//        		AccessToken accessToken = new AccessToken(outh_token,outh_secret,(long)Integer.valueOf(outh_user_id));
//        		new get_User_Data().execute(accessToken);
//        	}
//        }
       
    }
    
    private class get_outh extends  AsyncTask<Boolean, Void, Object>
    {
    	boolean saveCredintials=false;
    	TwitterException exception = null;
   @Override
   protected void onPostExecute(Object result) {
	   
   	super.onPostExecute(result);
   	if(exception != null)
	   {
		   if (twitterListener.get() != null)
				twitterListener.get().onException(new AppException(exception.getErrorMessage()));//.onTwitterLoginFailed(e);
	   }
   	if(requestToken !=null)
   	{
   		
          // auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            auth_dialog.setContentView(R.layout.twitterwebview);
            twitterLoginWebView = (WebView)auth_dialog.findViewById(R.id.loginView);
            twitterLoginWebView.getSettings().setJavaScriptEnabled(true);
            twitterLoginWebView.getSettings().setSaveFormData(false);
            twitterLoginWebView.getSettings().setSavePassword(false);
            twitterLoginWebView.setWebViewClient(new WebViewClient() {
                  boolean authComplete = false;
//                  @Override
//                  public void onPageStarted(WebView view, String url, Bitmap favicon){
//                   super.onPageStarted(view, url, favicon);
//                   twitterLoginWebView.stopLoading();
//                  }
                  
                  
                  @Override
               public boolean shouldOverrideUrlLoading(WebView view, String url) {
               	
               	//return super.shouldOverrideUrlLoading(view, url);
                   if (url.contains("oauth_verifier") && authComplete == false){
                     authComplete = true;
                    Log.e("Url",url);
                    Uri uri = Uri.parse(url);
                       oauth_verifier = uri.getQueryParameter("oauth_verifier");
                          auth_dialog.dismiss();
                          android.webkit.CookieManager.getInstance().removeAllCookie();
                          // Hide login button
                         // twitter_img.setEnabled(false);

               new get_Data().execute(saveCredintials);
               }else if(url.contains("denied")){
                   auth_dialog.dismiss();
                   Toast.makeText(context, "Sorry !, Permission Denied", Toast.LENGTH_SHORT).show();
               }
                 return false;  
               }
                  
              });
            twitterLoginWebView.loadUrl(requestToken.getAuthenticationURL());
          //  if(!isTwitterLoggedInAlready())
            if(!saveCredintials || (saveCredintials && !isTwitterLoggedInAlready()))
            auth_dialog.show(); 
            
            auth_dialog.setCancelable(true);
         
   	}
   	
   	

   }
  
   	@Override
   	protected Object doInBackground(Boolean... params) {
   		try {
   			 saveCredintials = params[0];
   			requestToken = twitter
   			        .getOAuthRequestToken(TWITTER_CALLBACK_URL);
   		} catch (TwitterException e) {
   			
   			e.printStackTrace();
   			exception = e;
   		}
   		return null;
   	}
   	 
    }
    
    private class get_Data extends AsyncTask<Boolean, Void, Object>
    {
    	boolean saveCredintials = false;
   	 AccessToken accessToken;
   	 @Override
   	protected void onPostExecute(Object result) {
   		
   		super.onPostExecute(result);
   		if(accessToken != null)
   		new get_User_Data().execute(accessToken);
   	}

   	@Override
   	protected Object doInBackground(Boolean... params) {
   		try {
   			saveCredintials = params[0];
               // Get the access token
                accessToken = twitter.getOAuthAccessToken(
                       requestToken, oauth_verifier);
                
                // Shared Preferences
                if(saveCredintials){
                Editor e = mSharedPreferences.edit();

                // After getting access token, access token secret
                // store them in application preferences
                e.putString(PREF_KEY_OAUTH_USER_ID, accessToken.getUserId()+"");
                e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
                e.putString(PREF_KEY_OAUTH_SECRET,
                        accessToken.getTokenSecret());
                // Store login status - true
                e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
                e.commit(); // save changes
                }
                Log.e("Twitter OAuth Token", "> " + accessToken.getToken());

                
           } catch (Exception e) {
               // Check log for login errors
               Log.e("Twitter Login Error", "> " + e.getMessage());
           }

   		return null;
   	}
   	 
    }
    
   private class get_User_Data extends AsyncTask<Object, Void, Object>
    {
   	 twitter4j.User user;
   	 TwitterException exception=null;
   @Override
   	protected void onPostExecute(Object result) {
   		
   		super.onPostExecute(result);
   		if(exception != null){
   		if (twitterListener.get() != null)
				twitterListener.get().onException(new AppException(exception.getErrorMessage()));
   		}
   		if(user != null){
   			//String username = user.getName();
   	         
   		        // Displaying in  ui
   		      //  Toast.makeText(context, username, Toast.LENGTH_LONG).show();
   		     User twitterUser = new User();
   		     twitterUser.setId((int)user.getId());
   		     twitterUser.setFullName(user.getName());
   		     twitterUser.setProfilePic(user.getProfileImageURL());
   		     twitterUser.setUserType(User.USER_TYPE_SOCIAL_MEDIA);
   		     twitterUser.setSocialMediaID(user.getId()+"");
   		     twitterUser.setDistrict("");
   		     twitterUser.setDateOfBirth(0);
   		     twitterUser.setPhone("");
   		     twitterUser.setEmail("");
   		    // twitterUser.setGender(true);
   		     twitterUser.setCountryName("");
   		    
   		    // twitterUser.setDateOfBirth(user.get)
   		 
   		     if(twitterListener.get() != null)
   		    	 twitterListener.get().onLoginSuccess(twitterUser,User.USER_TYPE_SOCIAL_MEDIA);
   		     
   			}
   	}
   	@Override
   	protected Object doInBackground(Object... params) {
   		AccessToken accessToken  = (AccessToken) params[0];
           // Getting user details from twitter
           // For now i am getting his name only
           long userID = accessToken.getUserId();
           
   		try {
   			user = twitter.showUser((int)userID);
   			
   		} catch (TwitterException e1) {
   			
   			e1.printStackTrace();
   			exception = e1;

   			
   		}
   		return null;
   	}
   	 
    }
   /**
    * Check user already logged in your application using twitter Login flag is
    * fetched from Shared Preferences
    * */
   public boolean isTwitterLoggedInAlready() {
       // return twitter login status from Shared Preferences
       return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
   }
   
   public void LogOut()
   {
	   twitter.setOAuthAccessToken(null);
	   
	   SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.remove(PREF_KEY_OAUTH_TOKEN);
		editor.remove(PREF_KEY_OAUTH_SECRET);

		editor.commit();
		
		
	 //  twitter..shutdown();
   }
   
  
   
   public void shareLink( final String message,final String PostId,final boolean isNewPost) throws Exception {
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			boolean flag = false;

			@Override
			protected Void doInBackground(Void... params) {
				Log.d("in", "in");
				String token = mSharedPreferences.getString(
						PREF_KEY_OAUTH_TOKEN, "");
				String secret = mSharedPreferences.getString(
						PREF_KEY_OAUTH_SECRET, "");

				try {
					
					// initialize the status object of Twitter 
					StatusUpdate status = new StatusUpdate(message);
					
					// initialize twitter variable 
					ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
					configurationBuilder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
					configurationBuilder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
					configurationBuilder.setOAuthAccessToken(token);
					configurationBuilder.setOAuthAccessTokenSecret(secret);
					Configuration configuration = configurationBuilder.build();
					 twitter = new TwitterFactory(configuration).getInstance();
					
					/**share Bitmap Image */
					 /* Bitmap image = BitmapFactory.decodeResource(getResources(),
								R.drawable.ic_launcher);
					ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
					image.compress(CompressFormat.PNG, 0, bos); 
					byte[] bitmapdata = bos.toByteArray();
					ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
					status.setMedia(message, bs);*/
	
					
					/**Share Image URL */
					 
					if(!PostId.equals("")){
						
//					InputStream	in = new  ImageBitmap().OpenHttpConnection("http://FCBSite.com/photos.php?postId=" + PostId);
//						status.setMedia(message, in);
					status =new StatusUpdate(message +"  "+ShareActivity.shareLink + PostId);
					}
					
					try{
				twitter4j.Status s	 = twitter.updateStatus(status);
				
				Log.d("ok", s.toString());
				flag = true;
		}
		catch (TwitterException e) {
			e.printStackTrace();
			
		}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				if (flag && isNewPost) {
					StudioManager.getInstance().shareToSocilaMedia();
					/*Toast.makeText(context,
							"Shared Successfully",
							Toast.LENGTH_LONG).show();*/
				} else {
					/*Toast.makeText(context,
							"Shared Failed !", Toast.LENGTH_LONG)
							.show();*/
				}
			}
		};
		task.execute();
	}

	
	/*
	 * This Class used to accept URL and return BitmapImage  
	 * */
	public class ImageBitmap {
	    
	    private DefaultHttpClient client = new DefaultHttpClient();
	    
	    public Bitmap getBitmapImage(String url) {
	        Bitmap bitmap = null;
	        try {
	            if (url == null || url.equals("")) {
	                bitmap = null;
	                Log.e("Bitmap Error",url);
	            } else {
	                InputStream is = OpenHttpConnection(url);
	                bitmap = BitmapFactory.decodeStream(is);
	            }
	        } catch (Exception e) {
	            // TODO: handle exception
	        }
	        
	        return bitmap;
	    }
	    
	    public InputStream OpenHttpConnection(String url) {
	    	Log.d("url", url);
	        HttpGet getRequest = new HttpGet(url);
	        try {
	            HttpResponse getResponse = client.execute(getRequest);
	            final int statusCode = getResponse.getStatusLine().getStatusCode();
	            if (statusCode != HttpStatus.SC_OK) {
	                return null;
	            }
	            
	            HttpEntity getResponseEntity = getResponse.getEntity();
	            if (getResponseEntity != null) {
	                return getResponseEntity.getContent();
	            }
	            
	        } catch (IOException e) {
	            getRequest.abort();
	            Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
	        }
	        return null;
	        
	    }
	}


}
