package com.tawasol.barcelona.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.facebook.DialogErrorCustom;
import com.tawasol.barcelona.facebook.FacebookCustom;
import com.tawasol.barcelona.facebook.FacebookCustom.DialogListener;
import com.tawasol.barcelona.facebook.FacebookErrorCustom;
import com.tawasol.barcelona.managers.StudioManager;
import com.tawasol.barcelona.ui.activities.ShareActivity;



/**
 * handle Silent Share in background if , specifically if Facebook 
 * application wasn't installed on device 
 */
public class Custom_FB_Util {

	// declare variables 
	Activity activity;
	private FacebookCustom facebook;
	private static final String[] PERMISSIONS = new String[] { "publish_stream" };
	private static final String TOKEN = "access_token";
	private static final String EXPIRES = "expires_in";
	private static final String KEY = "facebook-credentials";
	private boolean isNewPost;
	
	
	public Custom_FB_Util(Activity activity) {
		//this.context = context;
		this.activity = activity;
		
		facebook = new FacebookCustom(activity.getResources().getString(R.string.app_id));
		restoreCredentials(facebook);
	}
	
	public void PublishStory(String Caption, String PostId,boolean isNewPost)
	{
		this.isNewPost = isNewPost;
		new request().execute( Caption,  PostId);
	}
	public boolean saveCredentials(/*FacebookCustom facebook*/) {
		Editor editor = activity.getSharedPreferences(KEY,
				Context.MODE_PRIVATE).edit();
		editor.putString(TOKEN, facebook.getAccessToken());
		editor.putLong(EXPIRES, facebook.getAccessExpires());
		return editor.commit();
	}

	public boolean restoreCredentials(FacebookCustom facebook) {
		SharedPreferences sharedPreferences = activity
				.getSharedPreferences(KEY, Context.MODE_PRIVATE);
		facebook.setAccessToken(sharedPreferences.getString(TOKEN, null));
		facebook.setAccessExpires(sharedPreferences.getLong(EXPIRES, 0));
		return facebook.isSessionValid();
	}
	public boolean isSessionValid()
	{
		return facebook.isSessionValid();
	}
	
	public void loginAndAutherize(DialogListener listener ) {
		facebook.authorize(activity, PERMISSIONS, FacebookCustom.FORCE_DIALOG_AUTH,
				/*new LoginDialogListener()*/listener);
	}
private class getresponse extends AsyncTask<String, Void, Object>
{
	String response;
	String Caption,  PostId;
	@Override
	protected void onPostExecute(Object result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
		Log.d("Tests", "got response: " + response);
		if (response == null || response.equals("")
				|| response.equals("false")) {
		//	UIUtils.showToast(activity,"Blank response.");
		} else {
			if(isNewPost)
			StudioManager.getInstance().shareToSocilaMedia();
			//UIUtils.showToast(activity,"Message posted to your facebook wall!");
		}
	}
	@Override
	protected Object doInBackground(String... params) {
		Caption = params[0];
		PostId = params[1];
		try {
			Bundle parameters = new Bundle();
//			parameters.putString("message", cap);
//			parameters.putString("description", "topic share");
			parameters.putString("caption", Caption);
			parameters.putString("link",
					ShareActivity.shareLink + PostId);
			 response = facebook.request("me/feed", parameters, "POST");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
	private class request extends AsyncTask<String, Void, Object>
	{
		String Caption,  PostId;
		@Override
		protected void onPostExecute(Object result) {
			// TODO Auto-generated method stub
			new getresponse().execute(Caption,PostId);
			
			
		}
		@Override
		protected Object doInBackground(String... params) {
			Caption = params[0];
			PostId = params[1];
			try {
				facebook.request("me");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
	}
	/*
	public void postToWall(String Caption , String postId) {
		Bundle parameters = new Bundle();
		parameters.putString("caption", Caption);
		parameters.putString("link",
				"http://FCBSite.com/photos.php?postId=" + PostId);
//		parameters.putString("message", message);
//		parameters.putString("description", "topic share");
		try {
			
			
			facebook.request("me");
			String response = facebook.request("me/feed", parameters, "POST");
			Log.d("Tests", "got response: " + response);
			if (response == null || response.equals("")
					|| response.equals("false")) {
				UIUtils.showToast(activity,"Blank response.");
			} else {
				UIUtils.showToast(activity,"Message posted to your facebook wall!");
			}
		} catch (Exception e) {
			UIUtils.showToast(activity,"Failed to post to wall!");
			e.printStackTrace();
		}
	}

*/
	class LoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			saveCredentials(/*facebook*/);
//			if (ValidatorUtils.isRequired(Caption) && ValidatorUtils.isRequired(PostId)) {
//				postToWall(Caption,PostId);
//			}
		}

		public void onFacebookError(FacebookErrorCustom error) {
			UIUtils.showToast(activity,"Authentication with Facebook failed!");
		}

		public void onError(DialogErrorCustom error) {
			UIUtils.showToast(activity,"Authentication with Facebook failed!");
		}

		public void onCancel() {
			UIUtils.showToast(activity,"Authentication with Facebook cancelled!");
		}
	}



}
