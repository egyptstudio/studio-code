package com.tawasol.barcelona.utils;

import java.util.Locale;

import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.SharedPrefrencesDataLayer;

import android.content.res.Configuration;

/**
 * 
 * @author Basyouni
 *
 */
public class LanguageUtils {

	/*
	 * @added by Turki
	 * 
	 */
	 
	public static final String LANGUAGE_ENGLISH = "en";

	public static final String APP_LANGUAGE_CODE_KEY = "com.tawasol.barcelona.languageCode";
	public static final String APP_LANGUAGE_KEY = "com.tawasol.barcelona.language";
	public static final String APP_INIT = "com.tawasol.barcelona.init.defaultLanguage";
	public static String LANGUAGE = LANGUAGE_ENGLISH;
	
	private static LanguageUtils instance;

	public static LanguageUtils getInstance() {
		if (instance == null)
			instance = new LanguageUtils();
		return instance;
	}

	private LanguageUtils(){
		String savedLanguage = SharedPrefrencesDataLayer.getStringPreferences(
				App.getInstance().getApplicationContext(), APP_LANGUAGE_CODE_KEY, LANGUAGE);
		if (savedLanguage != null) {
			LANGUAGE = savedLanguage;
		}
	}

	public String getLanguage() {
		return LANGUAGE;
	}
	
	public void setLanguage(String language) {
		LanguageUtils.LANGUAGE = language;
	}
	
	public static void changeApplicationLanguage(String abbreviation) {
		Configuration config = new Configuration();
		config.locale = new Locale(abbreviation);
		App.getInstance().getBaseContext().getResources()
		.updateConfiguration(config, App.getInstance().getBaseContext().getResources().getDisplayMetrics());
	}

	public String getDeviceLanguage() {
		// added by Mohga 
		System.out.println("App lang"+SharedPrefrencesDataLayer.getStringPreferences(
				App.getInstance().getApplicationContext(), APP_LANGUAGE_CODE_KEY, LANGUAGE));
		return SharedPrefrencesDataLayer.getStringPreferences(
				App.getInstance().getApplicationContext(), APP_LANGUAGE_CODE_KEY, LANGUAGE);
	}

	public boolean isRTL(String abbreviation) {
		return isRTL(new Locale(abbreviation));
	}

	private boolean isRTL(Locale locale) {
		String lang = locale.getLanguage();
        if( "iw".equals(lang) || "ar".equals(lang)
            || "fa".equals(lang) || "ur".equals(lang) )
        {
            return true;
        } else {
            return false;
        }
	}
}
