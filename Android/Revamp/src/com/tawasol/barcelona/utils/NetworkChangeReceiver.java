package com.tawasol.barcelona.utils;

import com.android.vending.billing.util.BarcaIabHelper;
import com.android.vending.billing.util.BarcaIabHelper.OnIabSetupFinishedListener;
import com.android.vending.billing.util.BarcaIabResult;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.entities.WebServiceRequestInfo;
import com.tawasol.barcelona.managers.ShopManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkChangeReceiver extends BroadcastReceiver implements
		OnIabSetupFinishedListener {

	private BarcaIabHelper billingHelper;
	boolean isConnected;

	@Override
	public void onReceive(Context context, Intent intent) {
		ConnectivityManager connMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		isConnected = wifi != null && wifi.isConnectedOrConnecting()
				|| mobile != null && mobile.isConnectedOrConnecting();
		if(isConnected){
			hanldeWall();
			billingHelper = new BarcaIabHelper(context, Params.Common.BASE_64_KEY);
			billingHelper.startSetup(this);
		}
	}

	private void hanldeWall() {
		int type = WallManager.getWHICH_TAB();
		switch (type) {
		case FragmentInfo.LATEST_TAB:
			if(WallManager.isLatestDataCached() || WallManager.getInstance().getLatest().isEmpty()){
				WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
						FragmentInfo.ACTION_FIRST_TIME, UserManager.getInstance()
								.getCurrentUserId(),
						WallManager.NUM_OF_POSTS, 0,
						WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
						WebServiceRequestInfo.GET_POSTS,
						"wall/getLatest");
				WallManager.getInstance().handlePosts(requestInfo,
						FragmentInfo.LATEST_TAB, null);
			}
			break;
		case FragmentInfo.WALL:
			if(WallManager.isWallDataCached() || WallManager.getInstance().getWall().isEmpty()){
				WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
						FragmentInfo.ACTION_FIRST_TIME, UserManager.getInstance()
								.getCurrentUserId(),
						WallManager.NUM_OF_POSTS, 0,
						WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
						WebServiceRequestInfo.GET_POSTS,
						"wall/getWall");
				WallManager.getInstance().handlePosts(requestInfo,
						FragmentInfo.WALL, null);
			}
			break;
		case FragmentInfo.MY_PIC:
			if(WallManager.isMyPicsDataCached() || WallManager.getInstance().getMyPics().isEmpty()){
				WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
						FragmentInfo.ACTION_FIRST_TIME, UserManager.getInstance()
								.getCurrentUserId(),
						WallManager.NUM_OF_POSTS, 0,
						WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
						WebServiceRequestInfo.GET_POSTS,
						"wall/getMyPosts");
				WallManager.getInstance().handlePosts(requestInfo,
						FragmentInfo.MY_PIC, null);
			}
			break;
		case FragmentInfo.TOP_TEN:
			if(WallManager.isTopTenDataCached() || WallManager.getInstance().getTopTenSessons().isEmpty()){
				WallManager.getInstance().getTopTen(
						UserManager.getInstance().getCurrentUserId(),
						"wall/getTopTen",0);
			}
			break;
			
		default:
			break;
		}
	}

	@Override
	public void onIabSetupFinished(BarcaIabResult result) {
		if (isConnected) {
			if (ShopManager.getInstance().checkPointsFile()) {
				ShopManager.getInstance().addPointsForUser(UserManager.getInstance().getCurrentUserId(), ShopManager.getInstance().getSavedPoints(), ShopManager.getInstance().getSavedPurchase(), billingHelper , null , false);
			}else if(ShopManager.getInstance().checkPurchaseObject()){
				ShopManager.getInstance().consumePoints(billingHelper, ShopManager.getInstance().getSavedPurchase());
			}
			
			if(ShopManager.getInstance().checkSubscriptionFile()){
				ShopManager.getInstance().subscripeInPremium(UserManager.getInstance().getCurrentUserId(), ShopManager.getInstance().getSvaedSubscription());
			}
		}
	}
}
