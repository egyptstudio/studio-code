package com.tawasol.barcelona.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ScalingUtils {


	private static Bitmap getScaledBitmapWithWidth(String path, float targetWidth) {
		Bitmap b = BitmapFactory.decodeFile(path);
		
		float aspectRatio = (float) b.getWidth() / (float) b.getHeight();
		float targetHeight = targetWidth / aspectRatio;
		
		return Bitmap.createScaledBitmap(b, (int) targetWidth, (int) targetHeight, false);
		
	}
	
	private static Bitmap getScaledBitmapWithHeight(String path, float targetHeight) {
		Bitmap b = BitmapFactory.decodeFile(path);
		
		float aspectRatio = (float) b.getWidth() / (float) b.getHeight();
		float targetWidth = targetHeight * aspectRatio;
		
		return Bitmap.createScaledBitmap(b, (int) targetWidth, (int) targetHeight, false);
	}
	
	public static Bitmap getScaledBitmap(String path, int targetWidth, int targetHeight) {
		if (targetWidth > targetHeight) {
			return getScaledBitmapWithWidth(path, targetWidth);
		} else {
			return getScaledBitmapWithHeight(path, targetHeight);
		}
	}
	
	
}
