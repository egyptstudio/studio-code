package com.tawasol.barcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose; 
import com.tawasol.barcelona.business.BusinessObject;
/**
 * @author Turki
 * 
 */
public class SendMessage extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8772568124029636733L;
	@Expose
	private int storedMessageID;

	public SendMessage() {
		super(null, null, null);
	}

	@Override
	public void addValidationRules(Context context) {
	}

	/**
	 * @return the storedMessageID
	 */
	public int getStoredMessageID() {
		return storedMessageID;
	}

	/**
	 * @param storedMessageID the storedMessageID to set
	 */
	public void setStoredMessageID(int storedMessageID) {
		this.storedMessageID = storedMessageID;
	}

	
}
