package com.tawasol.barcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class InAppPointsEntity extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -11143597166549609L;

	@Expose
	String inAppId;
	@Expose
	int price;
	@Expose
	String description;
	String title;

	public InAppPointsEntity() {
		super(null, null, null);
	}

	public InAppPointsEntity(String inAppId, int price) {
		this();
		this.inAppId = inAppId;
		this.price = price;
	}

	public InAppPointsEntity(String title) {
		this();
		this.title = title;
	}

	public InAppPointsEntity(String inAppId, int price, String description) {
		this();
		this.inAppId = inAppId;
		this.price = price;
		this.description = description;
	}

	public InAppPointsEntity(String inAppId, int price, String description,
			String title) {
		this();
		this.inAppId = inAppId;
		this.price = price;
		this.description = description;
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getInAppId() {
		return inAppId;
	}

	public void setInAppId(String inAppId) {
		this.inAppId = inAppId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((inAppId == null) ? 0 : inAppId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InAppPointsEntity other = (InAppPointsEntity) obj;
		if (inAppId == null) {
			if (other.inAppId != null)
				return false;
		} else if (!inAppId.equals(other.inAppId))
			return false;
		return true;
	}

}
