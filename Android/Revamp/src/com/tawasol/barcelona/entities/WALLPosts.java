/**
 * 
 */
package com.tawasol.barcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

/**
 * @author Basyouni
 *
 */
public class WALLPosts extends BusinessObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6166171321386671164L;

	@Expose
	long currentRequestTime;
	@Expose
	List<PostViewModel> posts;

	public long getCurrentRequestTime() {
		return currentRequestTime;
	}

	public void setCurrentRequestTime(long currentRequestTime) {
		this.currentRequestTime = currentRequestTime;
	}

	public List<PostViewModel> getPosts() {
		return posts;
	}

	public void setPosts(List<PostViewModel> posts) {
		this.posts = posts;
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public WALLPosts(String saveMethod, String deleteMethod, String getMethod) {
		super(saveMethod, deleteMethod, getMethod);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tawasol.barcelona.business.BusinessObject#addValidationRules(android
	 * .content.Context)
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
