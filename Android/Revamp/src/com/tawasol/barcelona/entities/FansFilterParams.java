package com.tawasol.barcelona.entities;

import java.io.Serializable;
import java.util.List;

public class FansFilterParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2964834665287009061L;
	public static final int FILTER_MALE_ONLY = 1;
	public static final int FILTER_FEMALE_ONLY = 2;
	public static final int FILTER_ANY_GENDER = 3;

	public static final int FILTER_ONLINE_ONLY = 1;
	public static final int FILTER_OFFLINE_ONLY = 2;
	public static final int FILTER_ONLINE_AND_OFFLINE = 3;

	public static final int FILTER_FAVOURATE_ONLY = 1;
	public static final int FILTER_FAVOURATE_OR_NOT = 2;

	public static final int SORT_BY_LOCATION_AND_ONLINE = 1;
	public static final int SORT_BY_NUMBER_OF_POSTS = 2;

	public static final int PREMIUM_USERS_ONLY = 1;
	public static final int NOT_PREMIUM_USER = 0;

	public static final int ANY_USER = 0;
	public static final int FOLLOWING = 1;
	public static final int FOLLOWERS = 2;
	public static final int FOLLOWING_OR_FOLLOWERS = 3;

	int userId;
	int startingLimit;
	int gender;
	int userState;
	int favoritesFilter;
	int followersOrFollwong;
	List<Integer> countryList;
	int onlyPremuim;
	int sortedBy;
	String keyword;

	public FansFilterParams() {

	}

	public FansFilterParams(int userId, int startingLimit, int gender,
			int userState, int favoritesFilter, int followersOrFollwong,
			List<Integer> countryList, int onlyPremuim, int sortedBy,
			String keyword) {
		super();
		this.userId = userId;
		this.startingLimit = startingLimit;
		this.gender = gender;
		this.userState = userState;
		this.favoritesFilter = favoritesFilter;
		this.followersOrFollwong = followersOrFollwong;
		this.countryList = countryList;
		this.onlyPremuim = onlyPremuim;
		this.sortedBy = sortedBy;
		this.keyword = keyword;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getStartingLimit() {
		return startingLimit;
	}

	public void setStartingLimit(int startingLimit) {
		this.startingLimit = startingLimit;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getUserState() {
		return userState;
	}

	public void setUserState(int userState) {
		this.userState = userState;
	}

	public int getFavoritesFilter() {
		return favoritesFilter;
	}

	public void setFavoritesFilter(int favoritesFilter) {
		this.favoritesFilter = favoritesFilter;
	}

	public List<Integer> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<Integer> countryList) {
		this.countryList = countryList;
	}

	public int isOnlyPremuim() {
		return onlyPremuim;
	}

	public void setOnlyPremuim(int onlyPremuim) {
		this.onlyPremuim = onlyPremuim;
	}

	public int getSortedBy() {
		return sortedBy;
	}

	public void setSortedBy(int sortedBy) {
		this.sortedBy = sortedBy;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getFollowersOrFollwong() {
		return followersOrFollwong;
	}

	public void setFollowersOrFollwong(int followersOrFollwong) {
		this.followersOrFollwong = followersOrFollwong;
	}

}
