/**
 * 
 */
package com.tawasol.barcelona.entities;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.barcelona.business.BusinessObject;
import com.tawasol.barcelona.data.connection.URLs;

/**
 * @author Turki
 * 
 */
public class InvitationEntity extends BusinessObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1611312981504672385L;
	private int userId;
	private String invitationText;
	private ArrayList<String> phoneNumbers;
	
	public InvitationEntity(String saveMethod, String deleteMethod,	String getMethod) {
		super(saveMethod, deleteMethod, getMethod);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
		
	}

	
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public InvitationEntity() {
		super(null, null, null);
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param userId
	 * @param invitationText
	 * @param phoneNumbers
	 */
	public InvitationEntity(String saveMethod, String deleteMethod,	String getMethod, int userId, String invitationText,
			ArrayList<String> phoneNumbers) {
		super(saveMethod, deleteMethod, getMethod);
		this.userId = userId;
		this.invitationText = invitationText;
		this.phoneNumbers = phoneNumbers;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the invitationText
	 */
	public String getInvitationText() {
		return invitationText;
	}

	/**
	 * @param invitationText the invitationText to set
	 */
	public void setInvitationText(String invitationText) {
		this.invitationText = invitationText;
	}

	/**
	 * @return the phoneNumbers
	 */
	public ArrayList<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	/**
	 * @param phoneNumbers the phoneNumbers to set
	 */
	public void setPhoneNumbers(ArrayList<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	
}
