package com.tawasol.barcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class FriendsFilterEntity extends BusinessObject implements Serializable{
	
/**
 * @author Turki
 * 
 */
private static final long serialVersionUID = -7156499329798553652L;
	
	public FriendsFilterEntity(String saveMethod, String deleteMethod,	String getMethod) {
		super(saveMethod, deleteMethod, getMethod);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
		
	}
	
	@Expose
	int status;
	
	@Expose
	int friendRequests;
	
	@Expose
	List<FriendsEntity> data;

	
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public FriendsFilterEntity() {
		super(null, null, null);
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param status
	 * @param friendRequests
	 * @param nextStartingLimit
	 * @param nextFilter
	 * @param data
	 */
	public FriendsFilterEntity(int friendRequests, List<FriendsEntity> data) {
		this();
		this.friendRequests = friendRequests;
		this.data = data;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the friendRequests
	 */
	public int getFriendRequests() {
		return friendRequests;
	}

	/**
	 * @param friendRequests the friendRequests to set
	 */
	public void setFriendRequests(int friendRequests) {
		this.friendRequests = friendRequests;
	}

	/**
	 * @return the data
	 */
	public List<FriendsEntity> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<FriendsEntity> data) {
		this.data = data;
	}
	
}
