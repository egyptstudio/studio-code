package com.tawasol.barcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class ContestPrize extends BusinessObject implements Comparable<ContestPrize> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6073270428506571508L;

	@Expose
	private int contestPrizeId;
	
	@Expose
	private Integer prizeRank;
	
	@Expose
	private String prizeText;
	
	@Expose
	private String prizeImageUrl;
	
	@Expose
	private int contestId;
	
	@Expose
	private int winnerId;
	
	
	
	public ContestPrize() {
		super(null, null, null);
	}


	public String getPrizeImageUrl() {
		return prizeImageUrl;
	}


	public void setPrizeImageUrl(String prizeImageUrl) {
		this.prizeImageUrl = prizeImageUrl;
	}


	public int getContestId() {
		return contestId;
	}


	public void setContestId(int contestId) {
		this.contestId = contestId;
	}


	public ContestPrize( int contestPrizeId, Integer prizeRank,
			String prizeText, String prizeImageUrl, int contestId, int winnerId) {
		this();
		this.contestPrizeId = contestPrizeId;
		this.prizeRank = prizeRank;
		this.prizeText = prizeText;
		this.prizeImageUrl = prizeImageUrl;
		this.contestId = contestId;
		this.winnerId = winnerId;
	}


	public int getContestPrizeId() {
		return contestPrizeId;
	}



	public void setContestPrizeId(int contestPrizeId) {
		this.contestPrizeId = contestPrizeId;
	}



	public Integer getPrizeRank() {
		return prizeRank;
	}



	public void setPrizeRank(Integer prizeRank) {
		this.prizeRank = prizeRank;
	}



	public String getPrizeText() {
		return prizeText;
	}



	public void setPrizeText(String prizeText) {
		this.prizeText = prizeText;
	}



	public String getProzeImageUrl() {
		return prizeImageUrl;
	}



	public void setProzeImageUrl(String prozeImageUrl) {
		this.prizeImageUrl = prozeImageUrl;
	}



	@Override
	public void addValidationRules(Context context) {
		
	}

	public int getWinnerId() {
		return winnerId;
	}


	public void setWinnerId(int winnerId) {
		this.winnerId = winnerId;
	}


	@Override
	public int compareTo(ContestPrize another) {
		// TODO Auto-generated method stub
		return 0;
	}

}
