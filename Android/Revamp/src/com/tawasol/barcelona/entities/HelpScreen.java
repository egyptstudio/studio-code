package com.tawasol.barcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class HelpScreen extends BusinessObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2525471363428563024L;

	@Expose 
	private int screenId;
	
	@Expose
	private String screenTitle;
	
	@Expose
	private String screenImageUrl;
	
	@Expose
	private String screenIconUrl;
	
	@Expose
	private String description;
	
	private String LanguageCode;
	
	public HelpScreen() {
		super(null, null, null);
	}



	public HelpScreen(int screenId, String screenTitle, String screenImageUrl,
			String screenIconUrl, String description, String languageCode) {
		this();
		this.screenId = screenId;
		this.screenTitle = screenTitle;
		this.screenImageUrl = screenImageUrl;
		this.screenIconUrl = screenIconUrl;
		this.description = description;
		LanguageCode = languageCode;
	}



	public int getScreenId() {
		return screenId;
	}



	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}



	public String getScreenTitle() {
		return screenTitle;
	}



	public void setScreenTitle(String screenTitle) {
		this.screenTitle = screenTitle;
	}



	public String getScreenImageUrl() {
		return screenImageUrl;
	}



	public void setScreenImageUrl(String screenImageUrl) {
		this.screenImageUrl = screenImageUrl;
	}



	public String getScreenIconUrl() {
		return screenIconUrl;
	}

	
	public void setScreenIconUrl(String screenIconUrl) {
		this.screenIconUrl = screenIconUrl;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getLanguageCode() {
		return LanguageCode;
	}







	public void setLanguageCode(String languageCode) {
		LanguageCode = languageCode;
	}







	@Override
	public void addValidationRules(Context context) {
		
	}

}
