package com.tawasol.barcelona.entities;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.barcelona.business.BusinessObject;

//key value pairs Class
public class KeyValuePairs extends BusinessObject implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = -5462042656282648676L;
	public static final int TAG_TYPE_STUDIOPHOTO = 1;
	public static final int TAG_TYPE_USERPHOTO = 2;

	@Expose
	@SerializedName("tagId")
	int id_;
	@Expose
	@SerializedName("tagName")
	Object Text;
	@Expose
	@SerializedName("tagType")
	int type;
	@Expose
	int postId;

	public KeyValuePairs() {
		super(null, null, null);
	}

	public KeyValuePairs(int id_, Object Text) {
		this();
		this.id_ = id_;
		this.Text = Text;
	}

	public KeyValuePairs(int id_, Object text, int type) {
		this();
		this.id_ = id_;
		Text = text;
		this.type = type;
	}

	public KeyValuePairs( int id_, Object text, int type, int postId) {
		this();
		this.id_ = id_;
		Text = text;
		this.type = type;
		this.postId = postId;
	}

	public int get_Id() {
		return id_;
	}

	public void set_Id(int id) {
		this.id_ = id;
	}

	// public KeyValuePairs(Object id, Object text) {
	// this.id = id;
	// Text = text;
	// }
	public Object getText() {
		return Text;
	}

	public void setText(Object text) {
		Text = text;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}
}
