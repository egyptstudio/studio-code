package com.tawasol.barcelona.entities;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class ContestWinner extends  BusinessObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1344307067066925177L;
	
	@Expose
	private int winnerId;
	
	@Expose
	private String winnerName;
	
	@Expose
	private boolean isPremium;
	
	@Expose
	private boolean isFollowed;
	
	@Expose
	private boolean isOnline;
	
	@Expose
	private boolean isFavorite;
	
	@Expose
	private boolean isFriend;
	
	@Expose
	private String profilePicUrl;
	
	@Expose
	private int noOfPics;

	
	
	public ContestWinner() {
		super(null, null, null);
	}

	
	
	public ContestWinner(int winnerId, String winnerName,
			boolean isPremium, boolean isFollowed, boolean isOnline,
			boolean isFavorite, boolean isFriend, String profilePicUrl,
			int noOfPics) {
		this();
		this.winnerId = winnerId;
		this.winnerName = winnerName;
		this.isPremium = isPremium;
		this.isFollowed = isFollowed;
		this.isOnline = isOnline;
		this.isFavorite = isFavorite;
		this.isFriend = isFriend;
		this.profilePicUrl = profilePicUrl;
		this.noOfPics = noOfPics;
	}


	public int getWinnerId() {
		return winnerId;
	}




	public void setWinnerId(int winnerId) {
		this.winnerId = winnerId;
	}




	public String getWinnerName() {
		return winnerName;
	}




	public void setWinnerName(String winnerName) {
		this.winnerName = winnerName;
	}




	public boolean isPremium() {
		return isPremium;
	}




	public void setPremium(boolean isPremium) {
		this.isPremium = isPremium;
	}




	public boolean isFollowed() {
		return isFollowed;
	}




	public void setFollowed(boolean isFollowed) {
		this.isFollowed = isFollowed;
	}




	public boolean isOnline() {
		return isOnline;
	}




	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}




	public boolean isFavorite() {
		return isFavorite;
	}




	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}




	public boolean isFriend() {
		return isFriend;
	}




	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}




	public String getProfilePicUrl() {
		return profilePicUrl;
	}




	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}




	public int getNoOfPics() {
		return noOfPics;
	}




	public void setNoOfPics(int noOfPics) {
		this.noOfPics = noOfPics;
	}




	@Override
	public void addValidationRules(Context context) {
		
	}

}
