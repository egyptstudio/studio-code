package com.tawasol.barcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class City extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6244411682118224684L;
	@Expose
	int cityId;
	@Expose
	String cityName;

	public City() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	public City(String saveMethod, String deleteMethod, String getMethod,
			int cityId, String cityName) {
		super(saveMethod, deleteMethod, getMethod);
		this.cityId = cityId;
		this.cityName = cityName;
	}

	

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

	

}
