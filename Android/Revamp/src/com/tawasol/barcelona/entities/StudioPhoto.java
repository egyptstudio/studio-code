/**
 * 
 */
package com.tawasol.barcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

/**
 * @author Mohga
 *
 */
public class StudioPhoto extends BusinessObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8561865154465023136L;

	// define Photo Orientation Variables 
	public static final int STUDIO_PHOTO_ORIENTATION_PORTRAIT = 1;
	public static final int STUDIO_PHOTO_ORIENTATION_LANDSCAPE = 2;
	
	// define Photo Capture Type Variables
	public static final int PHOTO_CAPTURE_TYPE_MASK_SHOT = 1;
	public static final int PHOTO_CAPTURE_TYPE_SCREEN_SHOT = 2;
	
	// define privacy types 
	public static final int PHOTO_PRIVACY_CONTEST =1;
	public static final int PHOTO_PRIVACY_PUBLIC =2;
	public static final int PHOTO_PRIVACY_FRIENDS =3;
	public static final int PHOTO_PRIVACY_ONLY_ME =4;
	
	@Expose
	private int studioPhotoId;
	
	@Expose
	private int folderId;
	
	@Expose
	private String photoUrl;
	
	@Expose
	private int photoOrientation;
	
	@Expose
	private int useCount;
	
	@Expose
	private int captureType;
	
	@Expose
	private String photoName;
	
	@Expose
	private long uploadDate;
	
	@Expose
	private int requiredPoints;
	
	@Expose
	private int isPremium; 
	
	public String getPhotoUrl() {
		return photoUrl;
	}



	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}



	public int getIsPermium() {
		return isPremium;
	}



	public void setIsPermium(int isPermium) {
		this.isPremium = isPermium;
	}



	public int getIsPurchased() {
		return isPurchased;
	}



	public void setIsPurchased(int isPurchased) {
		this.isPurchased = isPurchased;
	}






	@Expose
	private int isPurchased;
	
	@Expose
	private List<KeyValuePairs> tags;
	
	
	
	public StudioPhoto() {
		super(null, null, null);
	}
	
	

	public StudioPhoto(int studioPhotoId, int folderId, String photoURL,
			int photoOrientation, int useCount, int captureType,
			String photoName, long uploadDate, int requiredPoints,
			int isPermium, int isPurchased, List<KeyValuePairs> tags) {
		this();
		this.studioPhotoId = studioPhotoId;
		this.folderId = folderId;
		this.photoUrl = photoURL;
		this.photoOrientation = photoOrientation;
		this.useCount = useCount;
		this.captureType = captureType;
		this.photoName = photoName;
		this.uploadDate = uploadDate;
		this.requiredPoints = requiredPoints;
		this.isPremium = isPermium;
		this.isPurchased = isPurchased;
		this.tags = tags;
	}






	public int getStudioPhotoId() {
		return studioPhotoId;
	}






	public void setStudioPhotoId(int studioPhotoId) {
		this.studioPhotoId = studioPhotoId;
	}






	public int getFolderId() {
		return folderId;
	}






	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}






	public String getPhotoURL() {
		return photoUrl;
	}






	public void setPhotoURL(String photoURL) {
		this.photoUrl = photoURL;
	}






	public int getPhotoOrientation() {
		return photoOrientation;
	}






	public void setPhotoOrientation(int photoOrientation) {
		this.photoOrientation = photoOrientation;
	}



	public int getUseCount() {
		return useCount;
	}






	public void setUseCount(int userCount) {
		this.useCount = userCount;
	}






	public int getCaptureType() {
		return captureType;
	}






	public void setCaptureType(int captureType) {
		this.captureType = captureType;
	}






	public String getPhotoName() {
		return photoName;
	}






	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}






	public long getUploadDate() {
		return uploadDate;
	}






	public void setUploadDate(long uploadDate) {
		this.uploadDate = uploadDate;
	}






	public int getRequiredPoints() {
		return requiredPoints;
	}






	public void setRequiredPoints(int requiredPoints) {
		this.requiredPoints = requiredPoints;
	}






	





	public List<KeyValuePairs> getTags() {
		return tags;
	}






	public void setTags(List<KeyValuePairs> tags) {
		this.tags = tags;
	}






	/* (non-Javadoc)
	 * @see com.tawasol.barcelona.business.BusinessObject#addValidationRules(android.content.Context)
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}

	
	
	

	
}
