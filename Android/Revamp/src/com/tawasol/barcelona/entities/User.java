package com.tawasol.barcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

/**
 * 
 * @author Basyouni
 *
 */
public class User extends BusinessObject {

	// Device type : Android
	public static final int ANDROID_DEVICE_TYPE=1;
	
	// User Login Type
	public static final int USER_TYPE_APP = 1;
	public static final int USER_TYPE_SOCIAL_MEDIA = 2;
	public static final int USER_TYPE_DUMMY = 3;
	public static final int USER_TYPE_FCB_STUDIO = 4;
	public static final int USER_TYPE_ADMIN = 5;
	public static final int USER_TYPE_SPONSER = 6;
	
	// Gender Type
	public static final int GENDER_TYPE_MALE =1;
	public static final int GENDER_TYPE_FEMALE =2;
	
	//Relationship type
	public static final int RELATIONSHIP_SINGLE = 1;
	public static final int RELATIONSHIP_MARRIED = 2;
	public static final int RELATIONSHIP_ENGAGED = 3;
	public static final int RELATIONSHIP_IN_RELATIONSHIP = 4;
	
	// Income type
	public static final int INCOME_LOW = 1;
	public static final int INCOME_AVERAGE = 2;
	public static final int INCOME_HIGH = 3;
	/**
	 * 
	 */
	private static final long serialVersionUID = 9111039564399756210L;

	@Expose
	private int userId;
	@Expose
	private String profilePic;
	@Expose
	private String fullName;
	@Expose
	private String password;
	@Expose
	private String email;
	@Expose
	private long dateOfBirth;
	@Expose
	private int gender;
	@Expose
	private int countryId;
	@Expose
	private String latitude;
	@Expose
	private String longitude;
	@Expose
	private boolean isOnline;
	@Expose
	private boolean isPremium;
	@Expose
	private long registerationDate;
	@Expose
	private long lastLoginDate;
	@Expose
	private int noOfPics;
	@Expose
	private int userType;
	@Expose
	private String socialMediaID;
	@Expose
	private String brief;
	@Expose
	private int credit;
	@Expose
	private boolean allowLocation;
	@Expose
	private String countryName;
	@Expose
	private int followingCount;
	@Expose
	private int followersCount;
	@Expose
	private String phone;
	@Expose
	private String district;
	@Expose
	private int cityId;
	@Expose
	private boolean emailVerified;
	@Expose
	private boolean phoneVerified;
	@Expose
	private String status;
	@Expose
	private String aboutMe;
	@Expose
	private int relationship;
	@Expose
	private String religion;
	@Expose
	private String education;
	@Expose
	private String job;
	@Expose
	private String company;
	@Expose
	private int income;
	@Expose
	private String cityName;
	@Expose 
	private int receiveFriendRequest;
	@Expose
	private int viewPersonalInfo;
	@Expose
	private int viewContactsInfo;
	@Expose
	private boolean receivePushNotification;
	@Expose
	private String deviceToken;
	@Expose 
	private int deviceType;
	@Expose 
	private String countryCode;
	@Expose
	private int registrationLevelOnePoints;
	@Expose
	private int registrationLevelTwoPoints;
	@Expose
	private int registrationLevelThreePoints;
	/*@Expose
	private boolean blocked;
	@Expose
	private String keywords;
	@Expose
	private boolean isBasic;
	@Expose
	private boolean isBronze;
	@Expose
	private boolean isSilver;
	@Expose
	private boolean isGold;*/
	
	
	
	

	
	public User() {
		super(null, null, null);
	}
	

	

	public User(int userId, String profilePic, String fullName, String password,
			String email, long dateOfBirth, int gender, int countryId,
			String latitude, String longitude, boolean isOnline,
			boolean isPremium, long registerationDate, long lastLoginDate,
			int noOfPics, int userType, String socialMediaID, String brief,
			int credit, boolean allowLocation, String countryName,
			int followingCount, int followersCount, String phone,
			String district, int cityId, boolean emailVerified,
			boolean phoneVerified, String status, String aboutMe,
			int relationship, String religion, String education, String job,
			String company, int income, String cityName,
			int receiveFriendRequest, int viewPersonalInfo,
			int viewContactsInfo, boolean receivePushNotification,
			String deviceToken, int deviceType, String countryCode,
			int regestrationLeveOnePoints, int regestrationLeveTwoPoints,
			int regestrationLeveThreePoints/*, boolean blocked, String keywords,
			boolean isBasic, boolean isBronze, boolean isSilver, boolean isGold*/) {
		this();
		this.userId = userId;
		this.profilePic = profilePic;
		this.fullName = fullName;
		this.password = password;
		this.email = email;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.countryId = countryId;
		this.latitude = latitude;
		this.longitude = longitude;
		this.isOnline = isOnline;
		this.isPremium = isPremium;
		this.registerationDate = registerationDate;
		this.lastLoginDate = lastLoginDate;
		this.noOfPics = noOfPics;
		this.userType = userType;
		this.socialMediaID = socialMediaID;
		this.brief = brief;
		this.credit = credit;
		this.allowLocation = allowLocation;
		this.countryName = countryName;
		this.followingCount = followingCount;
		this.followersCount = followersCount;
		this.phone = phone;
		this.district = district;
		this.cityId = cityId;
		this.emailVerified = emailVerified;
		this.phoneVerified = phoneVerified;
		this.status = status;
		this.aboutMe = aboutMe;
		this.relationship = relationship;
		this.religion = religion;
		this.education = education;
		this.job = job;
		this.company = company;
		this.income = income;
		this.cityName = cityName;
		this.receiveFriendRequest = receiveFriendRequest;
		this.viewPersonalInfo = viewPersonalInfo;
		this.viewContactsInfo = viewContactsInfo;
		this.receivePushNotification = receivePushNotification;
		this.deviceToken = deviceToken;
		this.deviceType = deviceType;
		this.countryCode = countryCode;
		this.registrationLevelOnePoints = regestrationLeveOnePoints;
		this.registrationLevelTwoPoints= regestrationLeveTwoPoints;
		this.registrationLevelThreePoints = regestrationLeveThreePoints;
		/*this.blocked = blocked;
		this.keywords = keywords;
		this.isBasic = isBasic;
		this.isBronze = isBronze;
		this.isSilver = isSilver;
		this.isGold = isGold;*/
	}


public  User(String profilePic, String fullName,
			String email, long dateOfBirth, int countryId, int cityId,String district,String phone,int gender)
{
	this();
	this.profilePic = profilePic;
	this.fullName=fullName;
	this.email = email;
	this.dateOfBirth = dateOfBirth;
	this.gender=gender;
	this.countryId=countryId;
	this.cityId=cityId;
	this.district=district;
	this.phone = phone;
	this.gender=gender;
			
}
public  User(String profilePic, String fullName,
		String email, long dateOfBirth, int countryId, int cityId,String district,String phone,int gender
		,String status , String aboutMe,int relationShip,int income,String education , String job)
{
this();
this.profilePic = profilePic;
this.fullName=fullName;
this.email = email;
this.dateOfBirth = dateOfBirth;
this.gender=gender;
this.countryId=countryId;
this.cityId=cityId;
this.district=district;
this.phone = phone;
this.gender=gender;
this.status = status;
this.aboutMe = aboutMe;
this.relationship = relationShip;
this.income=income;
this.education=education;
this.job = job;
		
}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(long dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	
	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public boolean isOnline() {
		return isOnline;
	}

	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}

	public boolean isPremium() {
		return isPremium;
	}

	public void setPremium(boolean isPremium) {
		this.isPremium = isPremium;
	}

	public long getRegisterationDate() {
		return registerationDate;
	}

	public void setRegisterationDate(long registerationDate) {
		this.registerationDate = registerationDate;
	}

	public long getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(long lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public int getNoOfPics() {
		return noOfPics;
	}

	public void setNoOfPics(int noOfPics) {
		this.noOfPics = noOfPics;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getSocialMediaID() {
		return socialMediaID;
	}

	public void setSocialMediaID(String socialMediaID) {
		this.socialMediaID = socialMediaID;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public boolean isAllowLocation() {
		return allowLocation;
	}

	public void setAllowLocation(boolean allowLocation) {
		this.allowLocation = allowLocation;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getFollowingCount() {
		return followingCount;
	}

	public void setFollowingCount(int followingCount) {
		this.followingCount = followingCount;
	}

	public int getFollowersCount() {
		return followersCount;
	}

	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public boolean isPhoneVerified() {
		return phoneVerified;
	}

	public void setPhoneVerified(boolean phoneVerified) {
		this.phoneVerified = phoneVerified;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public int getRelationship() {
		return relationship;
	}

	public void setRelationship(int relationship) {
		this.relationship = relationship;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}
	

	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public int getIncome() {
		return income;
	}
	public void setIncome(int income) {
		this.income = income;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	
	public int getReceiveFriendRequest() {
		return receiveFriendRequest;
	}
	public void setReceiveFriendRequest(int receiveFriendRequest) {
		this.receiveFriendRequest = receiveFriendRequest;
	}
	public int getViewPersonalInfo() {
		return viewPersonalInfo;
	}
	public void setViewPersonalInfo(int viewPersonalInfo) {
		this.viewPersonalInfo = viewPersonalInfo;
	}
	public int getViewContactsInfo() {
		return viewContactsInfo;
	}
	public void setViewContactsInfo(int viewContactsInfo) {
		this.viewContactsInfo = viewContactsInfo;
	}
	public boolean isReceivePushNotification() {
		return receivePushNotification;
	}
	public void setReceivePushNotification(boolean receivePushNotification) {
		this.receivePushNotification = receivePushNotification;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public int getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(int deviceType) {
		this.deviceType = deviceType;
	}
	
	public String getCountryCode() {
		return countryCode;
	}



	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	
	



	public int getRegestrationLeveOnePoints() {
		return registrationLevelOnePoints;
	}


	public void setRegestrationLeveOnePoints(int regestrationLeveOnePoints) {
		this.registrationLevelOnePoints = regestrationLeveOnePoints;
	}



	public int getRegestrationLeveTwoPoints() {
		return registrationLevelTwoPoints;
	}






	public void setRegestrationLeveTwoPoints(int regestrationLeveTwoPoints) {
		this.registrationLevelTwoPoints = regestrationLeveTwoPoints;
	}






	public int getRegestrationLeveThreePoints() {
		return registrationLevelThreePoints;
	}



	public void setRegestrationLeveThreePoints(int regestrationLeveThreePoints) {
		this.registrationLevelThreePoints = regestrationLeveThreePoints;
	}





/*
	public boolean isBlocked() {
		return blocked;
	}




	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}




	public String getKeywords() {
		return keywords;
	}




	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}*/


/*

	public boolean isBasic() {
		return isBasic;
	}




	public void setBasic(boolean isBasic) {
		this.isBasic = isBasic;
	}




	public boolean isBronze() {
		return isBronze;
	}




	public void setBronze(boolean isBronze) {
		this.isBronze = isBronze;
	}




	public boolean isSilver() {
		return isSilver;
	}




	public void setSilver(boolean isSilver) {
		this.isSilver = isSilver;
	}




	public boolean isGold() {
		return isGold;
	}




	public void setGold(boolean isGold) {
		this.isGold = isGold;
	}


*/

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}
	
	

}