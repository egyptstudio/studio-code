package com.tawasol.barcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class Fan extends BusinessObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 545554L;
	@Expose
	private int fanID;
	@Expose
	private String fanName;
	@Expose
	private String fanPicture;
	@Expose
	private int fanPicNumber;
	@Expose
	private boolean isPremium;
	@Expose
	private boolean isFollowed;
	@Expose
	private boolean fanOnline;
	@Expose
	private boolean isFavorite;
	@Expose
	private boolean isFriend;
	@Expose
	private boolean isBlocked;
	@Expose
	List<MoreInfo> moreInfo;
	@Expose
	String fanCountry;
	@Expose
	String fanCity;
	@Expose
	String fanDistrict;
	@Expose
	String fanBrief;
	@Expose
	int fanFollowers;
	@Expose
	int fanFollows;

	public Fan() {
		super(null, null, null);
	}
	
	public Fan(int fanID) {
		super(null, null, null);
		this.fanID = fanID;
	}

	public Fan(String saveMethod, String deleteMethod, String getMethod,
			int fanID, String fanName, String fanPicture, int fanPicNumber,
			boolean isPremium, boolean isFollowed, boolean fanOnline,
			boolean isFavorite, boolean isFriend) {
		super(saveMethod, deleteMethod, getMethod);
		this.fanID = fanID;
		this.fanName = fanName;
		this.fanPicture = fanPicture;
		this.fanPicNumber = fanPicNumber;
		this.isPremium = isPremium;
		this.isFollowed = isFollowed;
		this.fanOnline = fanOnline;
		this.isFavorite = isFavorite;
		this.isFriend = isFriend;
	}

	public Fan(String saveMethod, String deleteMethod, String getMethod,
			int fanID, String fanName, String fanPicture, int fanPicNumber,
			boolean isPremium, boolean isFollowed, boolean fanOnline,
			boolean isFavorite, boolean isFriend, boolean isBlocked,
			List<MoreInfo> moreInfo, String fanCountry, String fanCity,
			String fanDistrict, String fanBrief, int fanFollowers,
			int fanFollows) {
		super(saveMethod, deleteMethod, getMethod);
		this.fanID = fanID;
		this.fanName = fanName;
		this.fanPicture = fanPicture;
		this.fanPicNumber = fanPicNumber;
		this.isPremium = isPremium;
		this.isFollowed = isFollowed;
		this.fanOnline = fanOnline;
		this.isFavorite = isFavorite;
		this.isFriend = isFriend;
		this.isBlocked = isBlocked;
		this.moreInfo = moreInfo;
		this.fanCountry = fanCountry;
		this.fanCity = fanCity;
		this.fanDistrict = fanDistrict;
		this.fanBrief = fanBrief;
		this.fanFollowers = fanFollowers;
		this.fanFollows = fanFollows;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public List<MoreInfo> getMoreInfo() {
		return moreInfo;
	}

	public void setMoreInfo(List<MoreInfo> moreInfo) {
		this.moreInfo = moreInfo;
	}

	public String getFanCountry() {
		return fanCountry;
	}

	public void setFanCountry(String fanCountry) {
		this.fanCountry = fanCountry;
	}

	public String getFanCity() {
		return fanCity;
	}

	public void setFanCity(String fanCity) {
		this.fanCity = fanCity;
	}

	public String getFanDistrict() {
		return fanDistrict;
	}

	public void setFanDistrict(String fanDistrict) {
		this.fanDistrict = fanDistrict;
	}

	public String getFanBrief() {
		return fanBrief;
	}

	public void setFanBrief(String fanBrief) {
		this.fanBrief = fanBrief;
	}

	public int getFanFollowers() {
		return fanFollowers;
	}

	public void setFanFollowers(int fanFollowers) {
		this.fanFollowers = fanFollowers;
	}

	public int getFanFollows() {
		return fanFollows;
	}

	public void setFanFollows(int fanFollows) {
		this.fanFollows = fanFollows;
	}

	public int getFanID() {
		return fanID;
	}

	public void setFanID(int fanID) {
		this.fanID = fanID;
	}

	public String getFanName() {
		return fanName;
	}

	public void setFanName(String fanName) {
		this.fanName = fanName;
	}

	public String getFanPicture() {
		return fanPicture;
	}

	public void setFanPicture(String fanPicture) {
		this.fanPicture = fanPicture;
	}

	public int getFanPicNumber() {
		return fanPicNumber;
	}

	public void setFanPicNumber(int fanPicNumber) {
		this.fanPicNumber = fanPicNumber;
	}

	public boolean isPremium() {
		return isPremium;
	}

	public void setPremium(boolean isPremium) {
		this.isPremium = isPremium;
	}

	public boolean isFollowed() {
		return isFollowed;
	}

	public void setFollowed(boolean isFollowed) {
		this.isFollowed = isFollowed;
	}

	public boolean isFanOnline() {
		return fanOnline;
	}

	public void setFanOnline(boolean fanOnline) {
		this.fanOnline = fanOnline;
	}

	public boolean isFavorite() {
		return isFavorite;
	}

	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public boolean isFriend() {
		return isFriend;
	}

	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fanID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fan other = (Fan) obj;
		if (fanID != other.fanID)
			return false;
		return true;
	}

}