package com.tawasol.barcelona.entities;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.barcelona.business.BusinessObject;

/**
 * @author Turki
 * 
 */
public class MessagesUsersEntity extends BusinessObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8339461379496937913L;

	public MessagesUsersEntity(String saveMethod, String deleteMethod, String getMethod) {
		super(saveMethod, deleteMethod, getMethod);
		// TODO Auto-generated constructor stub
	}
	
	@Expose @SerializedName("friendID")
	int fanID;
	
	@Expose @SerializedName("friendName")
	String fanName;
	
	@Expose @SerializedName("friendPicture")
	String fanPicture;
	
	@Expose
	int type;

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}

	
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public MessagesUsersEntity() {
		super(null, null, null);
	}


	/**
	 * @return the fanID
	 */
	public int getFanID() {
		return fanID;
	}


	/**
	 * @param fanID the fanID to set
	 */
	public void setFanID(int fanID) {
		this.fanID = fanID;
	}


	/**
	 * @return the fanName
	 */
	public String getFanName() {
		return fanName;
	}


	/**
	 * @param fanName the fanName to set
	 */
	public void setFanName(String fanName) {
		this.fanName = fanName;
	}


	/**
	 * @return the fanPicture
	 */
	public String getFanPicture() {
		return fanPicture;
	}


	/**
	 * @param fanPicture the fanPicture to set
	 */
	public void setFanPicture(String fanPicture) {
		this.fanPicture = fanPicture;
	}


	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}	

}
