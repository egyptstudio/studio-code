package com.tawasol.barcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class WhoInstalledAppObject extends BusinessObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5101170246247734213L;

	@Expose
	List<Fan> installed;
	@Expose
	List<Fan> nonInstalled;

	public WhoInstalledAppObject(String saveMethod, String deleteMethod,
			String getMethod, List<Fan> installed, List<Fan> nonInstalled) {
		super(saveMethod, deleteMethod, getMethod);
		this.installed = installed;
		this.nonInstalled = nonInstalled;
	}

	public List<Fan> getInstalled() {
		return installed;
	}

	public void setInstalled(List<Fan> installed) {
		this.installed = installed;
	}

	public List<Fan> getNonInstalled() {
		return nonInstalled;
	}

	public void setNonInstalled(List<Fan> nonInstalled) {
		this.nonInstalled = nonInstalled;
	}

	public WhoInstalledAppObject() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
