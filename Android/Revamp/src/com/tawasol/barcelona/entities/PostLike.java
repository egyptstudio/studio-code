package com.tawasol.barcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class PostLike extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7478213555921575297L;
	@Expose
	int likeId;
	@Expose
	int userId;
	@Expose
	String fullName;
	@Expose
	String profilePicURL;
	@Expose
	boolean isFollowing;

	public PostLike(){
		super(null, null, null);
	}
	
	public PostLike(int commentId, int userId, String fullName,
			String profilePicURL, boolean isFollowing) {
		super(null, null, null);
		this.likeId = commentId;
		this.userId = userId;
		this.fullName = fullName;
		this.profilePicURL = profilePicURL;
		this.isFollowing = isFollowing;
	}

	public int getLikeId() {
		return likeId;
	}

	public void setLikeId(int commentId) {
		this.likeId = commentId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getProfilePicURL() {
		return profilePicURL;
	}

	public void setProfilePicURL(String profilePicURL) {
		this.profilePicURL = profilePicURL;
	}

	public boolean isFollowing() {
		return isFollowing;
	}

	public void setFollowing(boolean isFollowing) {
		this.isFollowing = isFollowing;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tawasol.barcelona.business.BusinessObject#addValidationRules(android
	 * .content.Context)
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
