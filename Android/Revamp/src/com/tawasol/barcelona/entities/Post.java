package com.tawasol.barcelona.entities;

import android.content.Context;

import com.tawasol.barcelona.business.BusinessObject;

public class Post extends BusinessObject {
	
	public static final int POST_TYPE_NORMAL = 1;
	public static final int POST_TYPE_AD = 2;

	private static final long serialVersionUID = 2162980008318139039L;

	public Post() {
		super(null, null, null);
	}

	@Override
	public void addValidationRules(Context context) {
		// Nothing at this level of post
	}

}
