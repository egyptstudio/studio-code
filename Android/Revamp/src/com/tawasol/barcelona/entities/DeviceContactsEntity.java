package com.tawasol.barcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

/**
 * @author Turki
 * 
 */
public class DeviceContactsEntity extends BusinessObject{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = -8835857772828697608L;

	public DeviceContactsEntity(int userID, List<DeviceContactModel> contacts) {
		super(null, null, null);
		this.userID = userID;
		this.contacts = contacts;
	}

	@Expose
	int userID; 
	
	@Expose
	List<DeviceContactModel> contacts;
	
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}

	/**
	 * @param userID the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	/**
	 * @return the contacts
	 */
	public List<DeviceContactModel> getContacts() {
		return contacts;
	}

	/**
	 * @param contacts the contacts to set
	 */
	public void setContacts(List<DeviceContactModel> contacts) {
		this.contacts = contacts;
	}	
	
	
}
