package com.tawasol.barcelona.entities;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.annotations.Expose; 
import com.tawasol.barcelona.business.BusinessObject;

public class LanguageEntity extends BusinessObject implements Serializable{

/**
 * @author Turki
 * 
 */
private static final long serialVersionUID = 4680395092864462727L;

	public LanguageEntity(String saveMethod, String deleteMethod, String getMethod) {
		super(saveMethod, deleteMethod, getMethod);
		// TODO Auto-generated constructor stub
	}
	
	@Expose
	int languageId;
	
	@Expose
	String languageName;
	
	@Expose
	String languageCode;
	
	
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public LanguageEntity() {
		super(null, null, null);
	}

	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the languageName
	 */
	public String getLanguageName() {
		return languageName;
	}

	/**
	 * @param languageName the languageName to set
	 */
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	/**
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * @param languageCode the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
}
