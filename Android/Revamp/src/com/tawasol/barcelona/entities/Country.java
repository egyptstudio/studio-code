package com.tawasol.barcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class Country extends BusinessObject implements Comparable<Country>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6244411682118224684L;
	@Expose
	int countryId;
	@Expose
	String countryName;

	public Country() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	public Country(String saveMethod, String deleteMethod, String getMethod,
			int countryId, String countryName) {
		super(saveMethod, deleteMethod, getMethod);
		this.countryId = countryId;
		this.countryName = countryName;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

	@Override
	public int compareTo(Country another) {
		// TODO Auto-generated method stub
		return 0;
	}

}
