package com.tawasol.barcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

public class UserContacts extends BusinessObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -200037038512723281L;

	@Expose
	private String name;
	@Expose
	private List<String> phones;
	@Expose
	private List<String> emails;

	public UserContacts(String name, List<String> phones,
			List<String> emails) {
		this();
		this.name = name;
		this.phones = phones;
		this.emails = emails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getPhones() {
		return phones;
	}

	public void setPhones(List<String> phones) {
		this.phones = phones;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}

	public UserContacts() {
		super(null, null, null);
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}
}
