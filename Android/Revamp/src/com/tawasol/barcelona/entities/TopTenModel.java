/**
 * 
 */
package com.tawasol.barcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.barcelona.business.BusinessObject;

/**
 * @author Basyouni
 *
 */
public class TopTenModel extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1287724676228856958L;

	@Expose
	private int seasonId;
	@Expose
	private String seasonName;
	@Expose
	List<PostViewModel> posts;


	public TopTenModel(String saveMethod, String deleteMethod,
			String getMethod, int seasonId, String seasonName,
			List<PostViewModel> posts) {
		super(saveMethod, deleteMethod, getMethod);
		this.seasonId = seasonId;
		this.seasonName = seasonName;
		this.posts = posts;
	}

	public int getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(int seasonId) {
		this.seasonId = seasonId;
	}

	public String getSeasonName() {
		return seasonName;
	}

	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}

	public List<PostViewModel> getPosts() {
		return posts;
	}

	public void setPosts(List<PostViewModel> posts) {
		this.posts = posts;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tawasol.barcelona.business.BusinessObject#addValidationRules(android
	 * .content.Context)
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
