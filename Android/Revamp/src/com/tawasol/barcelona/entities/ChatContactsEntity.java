package com.tawasol.barcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.barcelona.business.BusinessObject;

public class ChatContactsEntity extends BusinessObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8256052771987261357L;

	/**
	 * @author Turki
	 * 
	 */
	public ChatContactsEntity(String saveMethod, String deleteMethod,
			String getMethod) {
		super(saveMethod, deleteMethod, getMethod);
		// TODO Auto-generated constructor stub
	}

	@Expose 
	int fanID;

	@Expose
	boolean isFriend;
	
	@Expose
	boolean isPremium;

	@Expose
	boolean isFollowed;

	@Expose 
	String fanName;

	@Expose 
	boolean fanOnline;

	@Expose
	boolean isFavorite;

	@Expose
	String fanPicture;

	@Expose 
	int fanPicNumber;

	@Expose
	String contactName;

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public ChatContactsEntity() {
		super(null, null, null);
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param fanID
	 * @param fanName
	 * @param fanOnline
	 */
	public ChatContactsEntity(int fanID, String fanName) {
		super(null, null, null);
		this.fanID = fanID;
		this.fanName = fanName;
	}

	/**
	 * @return the fanID
	 */
	public int getFanID() {
		return fanID;
	}

	/**
	 * @param fanID the fanID to set
	 */
	public void setFanID(int fanID) {
		this.fanID = fanID;
	}

	/**
	 * @return the isFriend
	 */
	public boolean isFriend() {
		return isFriend;
	}

	/**
	 * @param isFriend the isFriend to set
	 */
	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

	/**
	 * @return the isPremium
	 */
	public boolean isPremium() {
		return isPremium;
	}

	/**
	 * @param isPremium the isPremium to set
	 */
	public void setPremium(boolean isPremium) {
		this.isPremium = isPremium;
	}

	/**
	 * @return the isFollowed
	 */
	public boolean isFollowed() {
		return isFollowed;
	}

	/**
	 * @param isFollowed the isFollowed to set
	 */
	public void setFollowed(boolean isFollowed) {
		this.isFollowed = isFollowed;
	}

	/**
	 * @return the fanName
	 */
	public String getFanName() {
		return fanName;
	}

	/**
	 * @param fanName the fanName to set
	 */
	public void setFanName(String fanName) {
		this.fanName = fanName;
	}

	/**
	 * @return the fanOnline
	 */
	public boolean isFanOnline() {
		return fanOnline;
	}

	/**
	 * @param fanOnline the fanOnline to set
	 */
	public void setFanOnline(boolean fanOnline) {
		this.fanOnline = fanOnline;
	}

	/**
	 * @return the isFavorite
	 */
	public boolean isFavorite() {
		return isFavorite;
	}

	/**
	 * @param isFavorite the isFavorite to set
	 */
	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	/**
	 * @return the fanPicture
	 */
	public String getFanPicture() {
		return fanPicture;
	}

	/**
	 * @param fanPicture the fanPicture to set
	 */
	public void setFanPicture(String fanPicture) {
		this.fanPicture = fanPicture;
	}

	/**
	 * @return the fanPicNumber
	 */
	public int getFanPicNumber() {
		return fanPicNumber;
	}

	/**
	 * @param fanPicNumber the fanPicNumber to set
	 */
	public void setFanPicNumber(int fanPicNumber) {
		this.fanPicNumber = fanPicNumber;
	}

	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	
}
