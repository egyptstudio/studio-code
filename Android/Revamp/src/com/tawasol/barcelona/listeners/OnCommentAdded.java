/**
 * 
 */
package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.UserComment;

/**
 * @author Basyouni
 *
 */
public interface OnCommentAdded extends UiListener{
//	void getCount(int count);
	void commentAdded(UserComment comment);
}
