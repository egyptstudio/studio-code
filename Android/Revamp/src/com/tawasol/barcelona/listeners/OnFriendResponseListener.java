package com.tawasol.barcelona.listeners;

public interface OnFriendResponseListener extends UiListener{
	void onSuccess(int status , boolean friend);
}
