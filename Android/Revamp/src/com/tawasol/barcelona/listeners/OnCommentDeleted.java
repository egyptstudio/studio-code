package com.tawasol.barcelona.listeners;

public interface OnCommentDeleted extends UiListener{

	void onCommentDeleted(int postId , int commentId);
}
