package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.StudioPhoto;


public interface OnStudioPhotoReceived
extends OnEntityReceivedListener<StudioPhoto> {

}
