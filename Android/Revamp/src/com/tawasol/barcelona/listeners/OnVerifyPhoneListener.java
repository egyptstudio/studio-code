package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.User;

/**
 * @author Turki
 */
public interface OnVerifyPhoneListener extends OnEntityReceivedListener<User> {	
}
