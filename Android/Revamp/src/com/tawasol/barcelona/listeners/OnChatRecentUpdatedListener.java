package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.Message;


/**
 * @author Turki
 * 
 */
public interface OnChatRecentUpdatedListener extends OnEntityListReceivedListener<Message> {

}
