package com.tawasol.barcelona.listeners;

/**
 * 
 * @author Turki
 *
 */
public interface OnBackgroundTask {
	
	void onTaskCompleted();
	
	void doTaskInBackground();

}
