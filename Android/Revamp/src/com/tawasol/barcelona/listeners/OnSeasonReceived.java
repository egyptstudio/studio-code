/**
 * 
 */
package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.TopTenModel;

/**
 * @author Basyouni
 *
 */
public interface OnSeasonReceived extends
		OnEntityListReceivedListener<TopTenModel> {

}
