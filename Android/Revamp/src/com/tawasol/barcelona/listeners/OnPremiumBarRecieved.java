package com.tawasol.barcelona.listeners;

import java.util.List;

import com.tawasol.barcelona.entities.Fan;

public interface OnPremiumBarRecieved extends UiListener {
	void onPremiumListRecieved(List<Fan> fans);
}
