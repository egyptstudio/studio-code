package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.FriendsEntity;
import com.tawasol.barcelona.entities.FriendsFilterEntity;

/**
 * @author Turki
 */
public interface OnChatFriendsResponseListener extends OnEntityListReceivedListener<FriendsEntity>{

}
