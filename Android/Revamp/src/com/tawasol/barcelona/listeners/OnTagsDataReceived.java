package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.TagsData;


public interface OnTagsDataReceived
extends OnEntityReceivedListener<TagsData> {

}
