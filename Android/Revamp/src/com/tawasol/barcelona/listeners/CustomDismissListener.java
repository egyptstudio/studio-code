/**
 * 
 */
package com.tawasol.barcelona.listeners;

/**
 * @author Basyouni
 *
 */
public interface CustomDismissListener {

	void onDismiss(int commentsCount);
	
}
