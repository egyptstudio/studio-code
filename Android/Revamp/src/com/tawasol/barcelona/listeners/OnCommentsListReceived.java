/**
 * 
 */
package com.tawasol.barcelona.listeners;

import java.util.List;

import com.tawasol.barcelona.entities.UserComment;

/**
 * @author Areeg
 *
 */
public interface OnCommentsListReceived extends OnEntityListReceivedListener<UserComment>{

	<T> void onEntitiesListReceived(List<T> list, int pageNo);
	
}
