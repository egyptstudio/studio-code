package com.tawasol.barcelona.listeners;

import java.util.List;

import com.tawasol.barcelona.entities.PostViewModel;

public interface OnListUpdateFinished extends UiListener{

	void onUpdatedListRecieved(List<PostViewModel> posts);
}
