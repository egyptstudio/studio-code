/**
 * 
 */
package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.User;


/**
 * @author Mohga
 *
 */
public interface OnUpdateProfileListener extends
		OnEntityReceivedListener<User> {

}
