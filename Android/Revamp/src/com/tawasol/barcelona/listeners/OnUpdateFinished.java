/**
 * 
 */
package com.tawasol.barcelona.listeners;

/**
 * @author Basyouni
 *
 */
public interface OnUpdateFinished extends UiListener{
	void onUpdateFinished();
}
