package com.tawasol.barcelona.listeners;

/**
 * @author Turki
 */
public interface OnChatDeleteMessageListener extends UiListener {	
	void onDeleteSuccess();
}
