package com.tawasol.barcelona.listeners;

import java.util.List;

import com.tawasol.barcelona.entities.Fan;

public interface OnSuggestionListRecieved extends UiListener{

	void onSuccess(List<Fan> fans);
}
