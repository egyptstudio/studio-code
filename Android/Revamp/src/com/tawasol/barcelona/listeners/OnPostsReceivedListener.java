package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.PostViewModel;

public interface OnPostsReceivedListener extends OnEntityListReceivedListener<PostViewModel> {

}
