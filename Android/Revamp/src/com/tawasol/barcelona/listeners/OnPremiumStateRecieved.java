package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.PremiumStateObject;

public interface OnPremiumStateRecieved extends UiListener{

	void onPremiumStateRecieved(PremiumStateObject premium , String productID);
}
