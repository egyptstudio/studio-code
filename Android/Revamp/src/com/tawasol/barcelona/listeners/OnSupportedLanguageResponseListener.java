package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.LanguageEntity;

/**
 * @author Turki
 * 
 */
public interface OnSupportedLanguageResponseListener extends OnEntityListReceivedListener<LanguageEntity>{

}
