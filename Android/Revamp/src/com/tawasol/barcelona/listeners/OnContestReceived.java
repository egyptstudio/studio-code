/**
 * 
 */
package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.ContestData;

/**
 * @author Mohga
 *
 */
public interface OnContestReceived extends OnEntityReceivedListener<ContestData>{
}
