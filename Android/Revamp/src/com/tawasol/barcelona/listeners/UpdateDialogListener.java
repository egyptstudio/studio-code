package com.tawasol.barcelona.listeners;

import java.util.List;

public interface UpdateDialogListener extends UiListener {
	void countryList(List<Integer> countries);
}
