package com.tawasol.barcelona.listeners;

import java.util.List;

public interface OnEntityListReceivedListener<T> extends UiListener {

	void onSuccess(List<T> objs);
	
}
