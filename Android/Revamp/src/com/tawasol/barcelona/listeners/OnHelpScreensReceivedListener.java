package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.HelpScreen;

public interface OnHelpScreensReceivedListener
extends OnEntityListReceivedListener<HelpScreen> {

}
