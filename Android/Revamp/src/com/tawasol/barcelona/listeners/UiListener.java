package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.exception.AppException;

public interface UiListener {

	void onException(AppException ex);
}
