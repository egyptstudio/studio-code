package com.tawasol.barcelona.listeners;

public interface OnFriendRequestResponseListener extends UiListener{
	void onSuccess(int senderId);
}
