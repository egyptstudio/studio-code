package com.tawasol.barcelona.listeners;

public interface OnPostDeletedListener extends UiListener {

	void onPostDeleted(int postId);
}
