package com.tawasol.barcelona.listeners;


import com.tawasol.barcelona.entities.FCBPhoto;


/**
 * @author Turki
 * 
 */
public interface OnFCBPhotosResponseListener extends OnEntityListReceivedListener<FCBPhoto>{

}
