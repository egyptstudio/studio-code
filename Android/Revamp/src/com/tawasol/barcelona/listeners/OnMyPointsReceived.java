package com.tawasol.barcelona.listeners;

public interface OnMyPointsReceived extends UiListener{

	public void onPointsRecieved(int points);
}
