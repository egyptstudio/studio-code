package com.tawasol.barcelona.listeners;

import java.util.List;

import com.tawasol.barcelona.entities.PostViewModel;

public interface PostFullScreenListener extends UiListener {

	void handle(List<Integer> actions , PostViewModel post);
}
