/**
 * 
 */
package com.tawasol.barcelona.listeners;

/**
 * @author Basyouni
 *
 */
public interface OnCommentScreenFinish extends UiListener{
	void getCount(int count);
}
