package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.StudioFolder;

public interface OnFoldersReceivedListener
extends OnEntityListReceivedListener<StudioFolder> {

}
