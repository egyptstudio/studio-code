package com.tawasol.barcelona.listeners;


public interface OnEntityReceivedListener<T> extends UiListener {

	void onSuccess(T obj);
	
}
