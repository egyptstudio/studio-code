/**
 * 
 */
package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.PostViewModel;


/**
 * @author Mohga
 *
 */
public interface OnShareListener extends
		OnEntityReceivedListener<PostViewModel> {

}
