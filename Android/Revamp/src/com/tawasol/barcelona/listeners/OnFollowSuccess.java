package com.tawasol.barcelona.listeners;

public interface OnFollowSuccess extends UiListener{

	void onFollowSuccess(int userId);
}
