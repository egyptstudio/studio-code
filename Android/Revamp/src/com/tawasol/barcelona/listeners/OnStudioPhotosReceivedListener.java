package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.StudioPhoto;

public interface OnStudioPhotosReceivedListener
extends OnEntityListReceivedListener<StudioPhoto> {

}
