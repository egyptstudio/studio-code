package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.Notification;


public interface OnNotificationsReceivedListener
extends OnEntityListReceivedListener<Notification> {

}
