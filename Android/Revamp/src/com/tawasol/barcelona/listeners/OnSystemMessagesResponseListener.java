package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.SystemMessagesEntity;

/**
 * @author Turki
 * 
 */
public interface OnSystemMessagesResponseListener extends OnEntityListReceivedListener<SystemMessagesEntity>{

}
