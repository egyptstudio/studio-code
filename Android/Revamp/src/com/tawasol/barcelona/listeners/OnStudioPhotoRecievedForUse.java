package com.tawasol.barcelona.listeners;

import com.tawasol.barcelona.entities.StudioPhoto;

public interface OnStudioPhotoRecievedForUse extends UiListener{
	
	void onStudioPhotoRecieved(StudioPhoto photo);
	void onStudioPhotoRecievedFailed(String msg);
}
