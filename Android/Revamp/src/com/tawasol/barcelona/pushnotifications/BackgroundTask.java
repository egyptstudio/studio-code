package com.tawasol.barcelona.pushnotifications;

import com.tawasol.barcelona.listeners.OnBackgroundTask;

import android.os.AsyncTask;

/**
 * @author Turki
 * 
 */
public class BackgroundTask extends AsyncTask<Object, Object, Object>{

	 private OnBackgroundTask listener;
	 

	    public BackgroundTask(OnBackgroundTask listener){
	        this.listener=listener;
	    }
	    
	    @Override
	    protected Object doInBackground(Object... params) {
	    	
	    	listener.doTaskInBackground();
	    	return null;
	    }

	    protected void onPostExecute(Object o){

	        listener.onTaskCompleted();
	    }
	 
}
