package com.tawasol.barcelona.pushnotifications;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.entities.FriendsEntity;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.managers.ChatManager;
import com.tawasol.barcelona.managers.NotificationsManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.ChatActivity;
import com.tawasol.barcelona.ui.activities.ChatViewActivity;

public class PushNotificationsService extends IntentService {

	private static final String NOTIFICATION_MESSAGE_KEY = "type"; // TODO:
																	// update
																	// value
																	// when
																	// added to
																	// SDS

	public PushNotificationsService() {
		super("PushNotificationsService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageGcmType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageGcmType)) {
				// Error
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageGcmType)) {
				// Delete
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageGcmType)) {
				handleMessage(intent.getStringExtra(NOTIFICATION_MESSAGE_KEY),
						intent);
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		PushNotificationsReceiver.completeWakefulIntent(intent);
	}

	private void handleMessage(String messageType, Intent intent) {
		// TODO add switch for types when added to SDS, for now all we have is
		// chat so ...
		// Load new chat messages from server
		/** Regarding to sob7i request comment email verified **/
		if (messageType != null) {
			switch (messageType) {
			case "1":
				notifyWithMsg(intent);
				ChatManager.getInstance().loadMessagesFromServer();
				break;
			case "2":
				WallManager.getInstance().EmailVerified();
				User user = UserManager.getInstance().getCurrentUser();
				user.setEmailVerified(true);
				UserManager.getInstance().cacheUser(user);
				break;
			case "10":
				NotificationsManager.getInstance().getNotificationsFromServer();
				break;
			default:
				notifyWithMsg(intent);
				ChatManager.getInstance().loadMessagesFromServer();
				// notifyWithMsg(intent);
				break;
			}
		}
	}

	public void notifyWithMsg(Intent intent) {
		String name = intent.getStringExtra("sName");
		String msg  = intent.getStringExtra("message");
		String sID  = intent.getStringExtra("sID");
		int id      = Integer.parseInt(sID);
		FriendsEntity friendObj = new FriendsEntity(id, name);
		NotificationManager NM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notify = new Notification(R.drawable.ic_launcher, name
				+ " Sent you a message", System.currentTimeMillis());
		notify.flags = Notification.FLAG_AUTO_CANCEL;
		
		PendingIntent pending = PendingIntent.getActivity(
				getApplicationContext(), 0, 
						ChatViewActivity.getActivityIntent(getApplicationContext(), friendObj), PendingIntent.FLAG_UPDATE_CURRENT);
		notify.setLatestEventInfo(getApplicationContext(), name, msg, pending);
		notify.contentIntent = pending;
		NM.notify(0, notify);
	}

}
