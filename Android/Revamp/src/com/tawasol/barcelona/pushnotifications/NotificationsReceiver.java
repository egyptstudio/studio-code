package com.tawasol.barcelona.pushnotifications;


import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.managers.NotificationsManager;
import com.tawasol.barcelona.managers.UserManager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationsReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Call Get Notifications :))
		//if(UserManager.getInstance().getCurrentUser().getUserId() !=0){
               doPeriodicTask(context, App.getInstance());
		//}
        }

	 public  void doPeriodicTask(Context context, App myApplication) {
	        NotificationsManager.getInstance().getNotificationsFromServer();
	        if(UserManager.getInstance().getCurrentUser().getUserId() !=0)
	        	UserManager.getInstance().getUser();
	    }
public void restartPeriodicTask(Context context)
{
	 AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
     Intent alarmIntent = new Intent(context, NotificationsReceiver.class);
     
	PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
    alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,0/*SystemClock.elapsedRealtime()*/,AlarmManager.INTERVAL_HALF_HOUR, pendingIntent);
}

public void cancelPeriodicTask(Context context)
{
	AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    Intent alarmIntent = new Intent(context, NotificationsReceiver.class);
   // alarmIntent.setAction(INTENT_ACTION);
    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
    alarmManager.cancel(pendingIntent);
    
   
}
}
