package com.tawasol.barcelona.pushnotifications;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.tawasol.barcelona.ui.activities.BaseActivity;

public class PushNotificationHelper {
	
	public static final int STATUS_SUCCESS = 0x01;
	public static final int STATUS_FAILURE = 0x02;
	
	private final static String TAG = "PushNotificationHelper";
	
	private final static String PROPERTY_APP_VERSION = "com.tawasol.barcelona.pushnotifications.AppVersion";
	private final static String PROPERTY_REG_ID = "com.tawasol.barcelona.pushnotifications.RegId";
	private final static String PROPERTY_REGISTER_WITH_SERVER = "com.tawasol.barcelona.pushnotifications.RegWithServer";
	private final static String SHARED_PREFS = "com.tawasol.barcelona.pushnotifications.Prefs";
	
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private final static String SENDER_ID = "294952364921";
	
	private GoogleCloudMessaging gcm;
	
	public int register(BaseActivity context) {
		if (checkPlayServices(context)) {
            gcm = GoogleCloudMessaging.getInstance(context);
            String regId = getRegistrationId(context);

            if (TextUtils.isEmpty(regId)) {
                return registerWithServer(context);
            } else {
            	return STATUS_SUCCESS;
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
            return STATUS_FAILURE;
        }
	}
	
	public static void notifyRegistrationWithServerSuccessful(Context context) {
		SharedPreferences prefs = getGCMPreferences(context);
		prefs.edit()
				.putBoolean(PROPERTY_REGISTER_WITH_SERVER, true)
				.commit();
	}
	
	public static boolean isRegisteredWithServer(Context context) {
		return getGCMPreferences(context).getBoolean(PROPERTY_REGISTER_WITH_SERVER, false);
	}
	
	public static String getStoredRegId(Context context) {
		return getGCMPreferences(context).getString(PROPERTY_REG_ID, "");
	}
	
	private int registerWithServer(Context context) {
		try {
			if (gcm == null)
				gcm = GoogleCloudMessaging.getInstance(context);
			
			String regId = gcm.register(SENDER_ID);
			
			storeRegistrationId(context, regId);
			
			return STATUS_SUCCESS;
		} catch (IOException ex) {
			Log.i(TAG, "Error registering with server.");
			return STATUS_FAILURE;
		}
	}
	
	private String getRegistrationId(Context context) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    if (TextUtils.isEmpty(registrationId)) {
	        Log.i(TAG, "Registration not found.");
	        return "";
	    }
	    // Check if app was updated; if so, it must clear the registration ID
	    // since the existing regID is not guaranteed to work with the new
	    // app version.
	    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
	    int currentVersion = getAppVersion(context);
	    if (registeredVersion != currentVersion) {
	        // Application version changed
	        return "";
	    }
	    return registrationId;
	}
	
	private static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        // Couldn't find the name of package of our app, whose context is used to get the package info
	    	// Long shot, should never happen
	        throw new RuntimeException("Can't find your package name!!");
	    }
	}
	
	private void storeRegistrationId(Context context, String regId) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    int appVersion = getAppVersion(context);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_REG_ID, regId);
	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	private static SharedPreferences getGCMPreferences(Context context) {
	    return context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
	}

	private boolean checkPlayServices(Context cxt) {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(cxt);
	    if (resultCode != ConnectionResult.SUCCESS) {
	    	/**Added by mohga to avoid exception when showing google play error dialog .. */
	        /*if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) cxt,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            Log.i(TAG, "Play services are not supported.");
	        }*/
	    	Log.i(TAG, "Play services are not supported.");
	        return false;
	    }
	    return true;
	}
	
}
