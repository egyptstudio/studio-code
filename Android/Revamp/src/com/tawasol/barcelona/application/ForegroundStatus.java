package com.tawasol.barcelona.application;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import android.app.Activity;
import android.app.Application;
import android.os.Handler;
import android.util.Log;

public class ForegroundStatus {
	
	public static final long CHECK_DELAY = 2000;
	public static final String TAG = ForegroundStatus.class.getName();

	public interface Listener {

		public void onBecameForeground();

		public void onBecameBackground();

	}

	private static ForegroundStatus instance;

	private boolean foreground = false, paused = true;
	private Handler handler = new Handler();
	private List<Listener> listeners = new CopyOnWriteArrayList<Listener>();
	private Runnable check;

	/**
	 * Its not strictly necessary to use this method - _usually_ invoking get
	 * with a Context gives us a path to retrieve the Application and
	 * initialise, but sometimes (e.g. in test harness) the ApplicationContext
	 * is != the Application, and the docs make no guarantees.
	 *
	 * @param application
	 * @return an initialised Foreground instance
	 */
	public static ForegroundStatus init(Application application) {
		if (instance == null) {
			instance = new ForegroundStatus();
		}
		return instance;
	}

	protected static ForegroundStatus get(Application application) {
		if (instance == null) {
			init(application);
		}
		return instance;
	}

	public static ForegroundStatus get() {
		if (instance == null) {
			throw new IllegalStateException(
					"Foreground is not initialised - invoke "
							+ "at least once with parameterised init/get");
		}
		return instance;
	}

	public boolean isForeground() {
		return foreground;
	}

	public boolean isBackground() {
		return !foreground;
	}

	public void addListener(Listener listener) {
		listeners.add(listener);
	}

	public void removeListener(Listener listener) {
		listeners.remove(listener);
	}

	public void onActivityResumed(Activity activity) {
		paused = false;
		boolean wasBackground = !foreground;
		foreground = true;

		if (check != null)
			handler.removeCallbacks(check);

		if (wasBackground) {
			Log.i(TAG, "went foreground");
			for (Listener l : listeners) {
				try {
					l.onBecameForeground();
				} catch (Exception exc) {
					Log.e(TAG, "Listener threw exception!", exc);
				}
			}
		} else {
			Log.i(TAG, "still foreground");
		}
	}

	public void onActivityPaused(Activity activity) {
		paused = true;

		if (check != null)
			handler.removeCallbacks(check);

		handler.postDelayed(check = new Runnable() {
			@Override
			public void run() {
				if (foreground && paused) {
					foreground = false;
					Log.i(TAG, "went background");
					for (Listener l : listeners) {
						try {
							l.onBecameBackground();
						} catch (Exception exc) {
							Log.e(TAG, "Listener threw exception!", exc);
						}
					}
				} else {
					Log.i(TAG, "still foreground");
				}
			}
		}, CHECK_DELAY);
	}

}
