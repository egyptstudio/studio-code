package com.tawasol.barcelona.application;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;

import com.nostra13.universalimageloader.cache.disc.impl.ext.LruDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.utils.L;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.data.cache.DatabaseManager;
import com.tawasol.barcelona.managers.ChatManager;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.pushnotifications.NotificationsReceiver;
import com.tawasol.barcelona.pushnotifications.NotificationsService;
import com.tawasol.barcelona.utils.TypefaceUtil;

public class App extends Application {

	private static final int THREAD_POOL_SIZE = 3;

	private ImageLoader imageLoader = ImageLoader.getInstance();
	private DisplayImageOptions option;
	private Handler mHandler;
	private ExecutorService mExecutor;

	ForegroundStatus.Listener backgroundListener = new ForegroundStatus.Listener() {
		@Override
		public void onBecameForeground() {
			changeChatStatus(true);
		}

		@Override
		public void onBecameBackground() {
			changeChatStatus(false);
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();
		barchelona = this;
		mHandler = new Handler();
//		boolean languageInt = SharedPrefrencesDataLayer.getBooleanPreferences(this, LanguageUtils.APP_INIT, true);
//		if(languageInt){
//			SharedPrefrencesDataLayer.saveBooleanPreferences(this, LanguageUtils.APP_INIT, false);
//			String local = Locale.getDefault().getLanguage();
//			final String langCode[] = this.getResources().getStringArray(R.array.languages_code_list_items);
//			for(int i=0; i< langCode.length; i++){
//				if(local.equalsIgnoreCase(langCode[i])){
//					SharedPrefrencesDataLayer.saveStringPreferences(this, LanguageUtils.APP_LANGUAGE_CODE_KEY, langCode[i]);
//					break;
//				}
//			}
//		} 
//		
//		// Language init 
//        String language = SharedPrefrencesDataLayer.getStringPreferences(this, 
//        		LanguageUtils.APP_LANGUAGE_CODE_KEY, LanguageUtils.LANGUAGE_ENGLISH);
//        LanguageUtils.changeApplicationLanguage(language); 
        
		// Foreground init
		ForegroundStatus.init(this);
		ForegroundStatus.get().addListener(backgroundListener);
//		Log.i("Inside App Count",String.valueOf(WallTable.getInstance().getCount()));
		// Database manager init
		DatabaseManager.getInstance(getApplicationContext()).initializeTables();
		// Image loader init
		initImageLoader(getApplicationContext());

		// Thread pool init
		mExecutor = Executors.newScheduledThreadPool(THREAD_POOL_SIZE,
				new ThreadFactory() {
					@Override
					public Thread newThread(Runnable runnable) {
						Thread thread = new Thread(runnable,
								"Background executor");
						thread.setPriority(Thread.MIN_PRIORITY);
						thread.setDaemon(true);
						return thread;
					}
				});

		// set the Typeface font for textViews , added by Mohga
		TypefaceUtil.overrideFont(getApplicationContext(), "SANS",
				"fonts/ufonts_com_interstate_light.ttf");
		
		//if(UserManager.getInstance().getCurrentUserId() !=0)
			startNotificationService();
	}
	
	
	/**
	 * Fire a service to schedule get notifications feature 
	 * Added by Mohga
	 * **/
	public void startNotificationService()
	{
		Intent startNotificatoinService = new Intent(this, NotificationsService.class);
        startService(startNotificatoinService);
	}
	public void stopNotificationService()
	{
		new NotificationsReceiver().cancelPeriodicTask(this);
	}

	protected void changeChatStatus(boolean online) {
		// Only change status if the user is logged in
		if (UserManager.getInstance().getCurrentUserId() != 0)
			ChatManager.getInstance().changeStatusAsync(online ? 1 : 0);
	}

	@SuppressWarnings("deprecation")
	public void initImageLoader(Context context) {
		LruDiscCache cache = null;
		try {
			cache = new LruDiscCache(getApplicationContext().getCacheDir(), new Md5FileNameGenerator(), 50 * 1024 * 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		option = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error)
				.resetViewBeforeLoading(true).cacheOnDisc(true)
//				.cacheOnDisk(true)
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300)).build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).threadPriority(Thread.NORM_PRIORITY)
				.denyCacheImageMultipleSizesInMemory()
//				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.FIFO)
				.diskCache(cache)
//				.diskCacheSize(50 * 1024 * 1024)
//				.diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
				// .diskCache(new
				// LruDiscCache(getApplicationContext().getCacheDir(), new
				// Md5FileNameGenerator(), 50))//TODO
				.defaultDisplayImageOptions(option)
				.build();
		L.writeLogs(true);
		L.writeDebugLogs(true);
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
		
	}

	public DisplayImageOptions getDisplayOption() {
		return option;
	}

	public ImageLoader getImageLoader() {
		return imageLoader;
	}

	public void runOnUiThread(Runnable runnable) {
		mHandler.post(runnable);
	}

	public void runInBackground(final Runnable runnable) {
		mExecutor.submit(new Runnable() {
			@Override
			public void run() {
				try {
					runnable.run();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static App barchelona;

	public static App getInstance() {
		return barchelona;
	}
	
	public void clearApplicationData() {
		System.out.println("Clear Data");
		DatabaseManager.getInstance(barchelona).clearAppDatabase();
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());
		if(appDir.exists()){
			String[] children = appDir.list();
			for(String s : children){
	        	 System.out.println("- "+s);
				if(!s.equalsIgnoreCase("lib") && !s.equalsIgnoreCase("databases")){
					deleteDir(new File(appDir, s));
					Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
				}
			}
		}
	}
	
	private static boolean deleteDir(File dir) {
	    if (dir != null && dir.isDirectory()) {
	        String[] children = dir.list();
	       
	        for (int i = 0; i < children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
	    return dir.delete();
	}
	
}
