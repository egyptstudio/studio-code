package com.tawasol.barcelona.customView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.FragmentInfo;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.managers.UserManager;
import com.tawasol.barcelona.managers.WallManager;
import com.tawasol.barcelona.ui.activities.CommentActivity;
import com.tawasol.barcelona.ui.activities.FanProfileActivity;
import com.tawasol.barcelona.ui.activities.ImageDetailsActivity;
import com.tawasol.barcelona.ui.activities.LikeActivity;
import com.tawasol.barcelona.ui.activities.LogInActivity;
import com.tawasol.barcelona.ui.activities.PostFullScreenDetailsActivity;
import com.tawasol.barcelona.ui.activities.UserProfileActivity;
import com.tawasol.barcelona.utils.NetworkingUtils;
import com.tawasol.barcelona.utils.UIUtils;

//import com.tawasol.barcelona.ui.activities.LogInActivity;

/**
 * 
 * @author Basyouni
 *
 */
public class PostViewFactory {

	public static final int ACTION_LIKe = 1;
	public static final int ACTION_FOLLOW = 2;
	public static final int ACTION_REPOST = 3;
	public static final int ACTION_DISLIKe = 4;
	public static final int ACTION_COMMENT = 5;
	public static final int ACTION_OPEN_LIKE = 6;
	public static final int ACTION_COMMENT_COUNT = 7;
	public static final int ACTION_REPORT = 8;
	public static final int ACTION_FOLLOW_REPOSTER = 9;
	public static final int ACTION_DELETE = 10;
	public static final int INTENT_REQUEST_CODE = 2654;

	public final static String INTENT_EXTRA_POST = "post";
	public final static String INTENT_EXTRA_ACTION = "action";
	public final static String INTENT_EXTRA_POSITION = "position";
	public final String INTENT_EXTRA_COMMENTS_COUNT = "commentsCount";
	public final String INTENT_EXTRA_USER_ID = "userId";

	// private PostViewModel post;
	private DisplayImageOptions profileOption;
	// private Holder holder;
	private FragmentActivity activity;
	private LayoutInflater inflater;
	private LayoutParams layout;
	private static AbsListView list;
	private int whichTab;
	Display display;

	@SuppressWarnings("deprecation")
	public PostViewFactory(FragmentActivity activity, int whichTab) {
		this.whichTab = whichTab;
		this.activity = activity;
		this.inflater = LayoutInflater.from(App.getInstance()
				.getApplicationContext());// activity.getLayoutInflater();
		// holder = new Holder();
		profileOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1000)).build();

		WindowManager window = (WindowManager) activity
				.getSystemService(Context.WINDOW_SERVICE);
		display = window.getDefaultDisplay();
	}

	@SuppressWarnings("deprecation")
	public PostViewFactory(FragmentActivity activity) {
		this.activity = activity;
		this.inflater = LayoutInflater.from(App.getInstance()
				.getApplicationContext());// activity.getLayoutInflater();
		// holder = new Holder();
		profileOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.ic_launcher).cacheOnDisc(true)
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1000)).build();
	}

	@SuppressWarnings("deprecation")
	public View getView(View view, ViewGroup container,
			final PostViewModel post, final AbsListView list,
			final int position, final int groupPosition) {
		PostViewFactory.list = list;
		final Holder holder;
		View rootView = view;

		if (rootView == null)
			rootView = inflater.inflate(R.layout.post_body_new, container,
					false);
		rootView.setTag(post.getPostId());
		holder = new Holder();
		// this.holder = holder;
		holder.repostCount = (TextView) rootView
				.findViewById(R.id.repost_count);
		holder.likeCount = (TextView) rootView.findViewById(R.id.likes_num_txt);
		holder.profileImageView = (ImageView) rootView
				.findViewById(R.id.profile_Image);
		holder.userName = (TextView) rootView.findViewById(R.id.userName);
		holder.repostedBy = (TextView) rootView.findViewById(R.id.reposted_by);
		holder.follow = (ImageView) rootView.findViewById(R.id.follow);
		holder.topTenIndicator = (LinearLayout) rootView
				.findViewById(R.id.topTenIndicator);
		holder.topTenRank = (TextView) rootView.findViewById(R.id.topTenRank);
		holder.date = (TextView) rootView.findViewById(R.id.date);
		holder.postBody = (ImageView) rootView.findViewById(R.id.postImage);
		holder.like = (ImageButton) rootView.findViewById(R.id.likeButton);
		holder.comment = (TextView) rootView.findViewById(R.id.commentButton);
		holder.repost = (ImageButton) rootView.findViewById(R.id.repostButton);
		holder.more = (ImageButton) rootView.findViewById(R.id.moreButton);
		holder.postLoading = (ProgressBar) rootView
				.findViewById(R.id.postLoading);
		// rootView.setTag(holder);

		// } else {
		// holder = (Holder) rootView.getTag();
		// }
		//

		layout = rootView.getLayoutParams();
		if (layout instanceof AbsListView.LayoutParams) {
			rootView.setLayoutParams(new AbsListView.LayoutParams(
					AbsListView.LayoutParams.MATCH_PARENT, display.getHeight()
							- (display.getHeight() / 3)));
		}
		populateData(post, holder);

		holder.profileImageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (post.getUserId() == UserManager.getInstance()
						.getCurrentUserId())
					activity.startActivity(UserProfileActivity
							.getActivityIntent(activity));
				else
					activity.startActivity(FanProfileActivity.getIntent(
							activity, post.getUserId(), post.getUserName()));
			}
		});

		holder.userName.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (post.getUserId() == UserManager.getInstance()
						.getCurrentUserId())
					activity.startActivity(UserProfileActivity
							.getActivityIntent(activity));
				else
					activity.startActivity(FanProfileActivity.getIntent(
							activity, post.getUserId(), post.getUserName()));
			}
		});

		holder.likeCount.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (holder.likeCount != null && post.getLikeCount() > 0) {
					handleLikeCount(post, post.getLikeCount(), position);
				}
			}
		});

		if (holder.like != null)
			holder.like.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (NetworkingUtils.isNetworkConnected(activity)) {
						activity.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								if (UserManager.getInstance()
										.getCurrentUserId() != 0) {
									System.out.println(post.getLikeCount());
									if (post.isLiked()) {
										((ImageButton) getRowFromList(list,
												post.getPostId()).findViewById(
												R.id.likeButton))
												.setImageResource(R.drawable.like);
										WallManager.getInstance().handleLike(
												WallManager.DISLIKE_POST,
												UserManager.getInstance()
														.getCurrentUserId(),
												post.getPostId());
										if (post.getLikeCount() > 1)
											((TextView) getRowFromList(list,
													post.getPostId())
													.findViewById(
															R.id.likes_num_txt)).setText(String.valueOf(post
													.getLikeCount() - 1));
										else
											((TextView) getRowFromList(list,
													post.getPostId())
													.findViewById(
															R.id.likes_num_txt))
													.setText("");
									} else if (!post.isLiked()) {
										((ImageButton) getRowFromList(list,
												post.getPostId()).findViewById(
												R.id.likeButton))
												.setImageResource(R.drawable.like_sc);
										WallManager.getInstance().handleLike(
												WallManager.LIKE_POST,
												UserManager.getInstance()
														.getCurrentUserId(),
												post.getPostId());
										if (post.getLikeCount() >= 0)
											((TextView) getRowFromList(list,
													post.getPostId())
													.findViewById(
															R.id.likes_num_txt)).setText(String.valueOf(post
													.getLikeCount() + 1));
										else
											((TextView) getRowFromList(list,
													post.getPostId())
													.findViewById(
															R.id.likes_num_txt))
													.setText("");
									}
								} else {
									activity.startActivityForResult(
											LogInActivity
													.getActivityIntent(
															activity,
															UserManager.LOGIN_FOR_RESULT,
															intentBuilder(
																	post,
																	post.isLiked() ? ACTION_DISLIKe
																			: ACTION_LIKe,
																	position)),
											INTENT_REQUEST_CODE);
								}
							}
						});
					} else {
						UIUtils.showToast(activity,
								"Please make sure you are connected to Internet");
					}
				}

			});

		if (holder.comment != null)
			holder.comment.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						activity.startActivity(CommentActivity.getIntent(
								activity, post.getPostId(), post
										.getCommentCount(), whichTab,
								post.getUserId() == UserManager.getInstance()
										.getCurrentUserId() ? true : false));
					} else {
						activity.startActivityForResult(LogInActivity
								.getActivityIntent(
										activity,
										UserManager.LOGIN_FOR_RESULT,
										intentBuilder(post, ACTION_COMMENT,
												position)), INTENT_REQUEST_CODE);
					}
				}
			});

		if (holder.repost != null)
			holder.repost.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (UserManager.getInstance().getCurrentUserId() == post
							.getUserId())
						UIUtils.showToast(activity, activity.getResources()
								.getString(R.string.cantRepost));
					else {
						if (NetworkingUtils.isNetworkConnected(activity)) {
							if (!post.isRePosted())
								if (UserManager.getInstance()
										.getCurrentUserId() != 0) {
									WallManager.getInstance().handleRepost(
											UserManager.getInstance()
													.getCurrentUserId(),
											post.getPostId());
									((ImageButton) getRowFromList(
											PostViewFactory.list,
											post.getPostId()).findViewById(
											R.id.repostButton))
											.setImageResource(R.drawable.repost_sc);
									UIUtils.showToast(
											activity,
											activity.getResources().getString(
													R.string.Home_repostAlert));
								} else {
									activity.startActivityForResult(
											LogInActivity
													.getActivityIntent(
															activity,
															UserManager.LOGIN_FOR_RESULT,
															intentBuilder(
																	post,
																	ACTION_REPOST,
																	position)),
											INTENT_REQUEST_CODE);
								}
						} else {
							UIUtils.showToast(activity,
									"Please make sure you are connected to Internet");
						}
					}
				}
			});
		if (holder.more != null)
			holder.more.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					activity.startActivity(ImageDetailsActivity
							.getActivityIntent(activity, post, whichTab,
									position, false, 0, 0));
				}
			});
		if (holder.follow != null)
			holder.follow.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (NetworkingUtils.isNetworkConnected(activity)) {
						activity.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								if (UserManager.getInstance()
										.getCurrentUserId() != 0) {
									if (!post.isFollowing()) {
										((ImageView) getRowFromList(list,
												post.getPostId()).findViewById(
												R.id.follow))
												.setImageResource(R.drawable.followed);
										WallManager.getInstance().handleFollow(
												UserManager.getInstance()
														.getCurrentUserId(),
												post.getUserId(),
												post.getPostId(), activity);
									}
								} else {
									activity.startActivityForResult(
											LogInActivity
													.getActivityIntent(
															activity,
															UserManager.LOGIN_FOR_RESULT,
															intentBuilder(
																	post,
																	ACTION_FOLLOW,
																	position)),
											INTENT_REQUEST_CODE);
								}
							}
						});
					} else {
						UIUtils.showToast(activity,
								"Please make sure you are connected to Internet");
					}
				}
			});
		if (holder.postBody != null)
			holder.postBody.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					activity.startActivity(PostFullScreenDetailsActivity
							.getIntent(activity, position, whichTab,
									groupPosition, WallManager.getInstance()
											.isFiltered(), WallManager
											.getInstance().isFilteredUsingFav()));
				}
			});
		return rootView;
	}

	/**
	 * 
	 */
	private void handleLikeCount(PostViewModel post, int likeCount, int position) {
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			activity.startActivity(LikeActivity.getIntent(activity,
					post.getPostId(), likeCount));
		} else {
			activity.startActivityForResult(LogInActivity.getActivityIntent(
					activity, UserManager.LOGIN_FOR_RESULT,
					intentBuilder(post, ACTION_OPEN_LIKE, position)),
					INTENT_REQUEST_CODE);
		}
	}

	public View getRowFromList(AbsListView list, int id) {
		// if (whichTab == FragmentInfo.TOP_TEN) {
		// View itemView = null;
		// int nFirst = list.getFirstVisiblePosition();
		// final int nLast = list.getLastVisiblePosition();
		// for (int position = nFirst; position <= nLast; ++position) {
		// itemView = list.getChildAt(position - nFirst);
		// int top = (itemView == null) ? 0 : (itemView.getTop() -
		// list.getPaddingTop());
		// if(top > 20)
		// itemView = list.getChildAt((position - nFirst) - 1);
		// if (itemView instanceof RelativeLayout)
		// if (itemView.getId() == R.id.postMainView)
		// break;
		// }
		// return itemView;
		// } else {
		// return list.getChildAt(id - list.getFirstVisiblePosition());
		// }

		return list.findViewWithTag(id);

	}

	private void populateData(PostViewModel post, Holder holder) {
		if (holder.repostCount != null) {
			if (post.getReportCount() > 0) {
				holder.repostCount
						.setText(String.valueOf(post.getReportCount()));
			}
		}
		if (holder.comment != null) {
			if (post.getCommentCount() > 0) {
				holder.comment.setText(String.valueOf(post.getCommentCount()));
			} else {
				holder.comment.setText("");
			}
		}
		if (holder.likeCount != null) {
			if (post.getLikeCount() > 0) {
				holder.likeCount.setText(String.valueOf(post.getLikeCount()));
			} else {
				holder.likeCount.setText(String.valueOf(""));
			}
		}
		if (holder.repost != null) {
			if (post.isRePosted())
				holder.repost.setBackgroundResource(R.drawable.repost_sc);
			else
				holder.repost.setBackgroundResource(R.drawable.repost);
		}
		if (holder.profileImageView != null) {
			App.getInstance()
					.getImageLoader()
					.displayImage(post.getUserProfilePicUrl(),
							holder.profileImageView, profileOption);
			holder.profileImageView.setBackgroundResource(R.drawable.user_pic);
		}
		if (holder.userName != null)
			holder.userName.setText(post.getUserName());
		if (post.getOriginalPostId() > 0) {
			if (whichTab != FragmentInfo.MY_PIC) {
				if (holder.repostedBy != null)
					if (post.getRepostedBy() != null
							&& !post.getRepostedBy().equalsIgnoreCase("")) {
						holder.repostedBy.setVisibility(View.VISIBLE);
						holder.repostedBy.setText(activity.getResources()
								.getString(R.string.reposted_by)
								+ " "
								+ post.getRepostedBy());
					} else {
						holder.repostedBy.setVisibility(View.GONE);
					}
			} else {
				holder.repostedBy.setVisibility(View.GONE);
			}
		} else {
			holder.repostedBy.setVisibility(View.GONE);
		}
		if (UserManager.getInstance().getCurrentUserId() != post.getUserId()) {
			if (holder.follow != null)
				if (post.isFollowing())
					holder.follow.setVisibility(View.GONE);
				// .setImageBitmap(BitmapFactory.decodeResource(
				// activity.getResources(), R.drawable.followed));

				else {
					holder.follow.setVisibility(View.VISIBLE);
					holder.follow.setImageBitmap(BitmapFactory.decodeResource(
							activity.getResources(), R.drawable.unfollowed));
				}
		} else {
			holder.follow.setVisibility(View.GONE);
		}
		if (holder.topTenIndicator != null && holder.topTenRank != null)
			if (post.getContestRank() > 0) {
				holder.topTenIndicator.setVisibility(View.VISIBLE);
				holder.topTenRank
						.setText(String.valueOf(post.getContestRank()));
			} else {
				holder.topTenIndicator.setVisibility(View.GONE);
			}
		if (holder.date != null)
			holder.date.setText(formatedate(post.getCreationTime()));
		if (holder.postBody != null)
			display(holder.postBody, post.getPostPhotoUrl(), holder.postLoading);
		// App.getInstance()
		// .getImageLoader()
		// .displayImage(post.getPostPhotoUrl(), holder.postBody,
		// App.getInstance().getDisplayOption());
		if (holder.like != null)
			if (post.isLiked()) {
				holder.like.setImageBitmap(BitmapFactory.decodeResource(
						activity.getResources(), R.drawable.like_sc));
			} else {
				holder.like.setImageBitmap(BitmapFactory.decodeResource(
						activity.getResources(), R.drawable.like));
			}
	}

	public void display(ImageView img, String url, final ProgressBar spinner) {
		App.getInstance()
				.getImageLoader()
				.displayImage(url, img, App.getInstance().getDisplayOption(),
						new ImageLoadingListener() {
							@Override
							public void onLoadingStarted(String imageUri,
									View view) {
								spinner.setVisibility(View.VISIBLE);
							}

							@Override
							public void onLoadingFailed(String imageUri,
									View view, FailReason failReason) {
								spinner.setVisibility(View.GONE);

							}

							@Override
							public void onLoadingComplete(String imageUri,
									View view, Bitmap loadedImage) {
								spinner.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingCancelled(String imageUri,
									View view) {

							}

						});
	}

	// @SuppressWarnings("unused")
	// private Period calcDiff(Date startDate, Date endDate) {
	// DateTime START_DT = (startDate == null) ? null
	// : new DateTime(startDate);
	// DateTime END_DT = (endDate == null) ? null : new DateTime(endDate);
	//
	// Period period = new Period(START_DT, END_DT);
	//
	// return period;
	//
	// }

	private String formatedate(long date1) {

		long time = date1 * (long) 1000;
		// Date date = new Date(time);
		// SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm",
		// Locale.getDefault());
		// format.setTimeZone(TimeZone.getTimeZone("GMT"));
		// String dt = format.format(date);
		// ---------------ago------------------------
		// Period dateDiff = calcDiff(new Date(time),
		// new Date(System.currentTimeMillis()));
		// String d = PeriodFormat.wordBased().print(dateDiff);
		// String[] da = d.split(",");
		// return da[0] + " ago";
		// ------------------------------------------
		Date date = new Date(time);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm",
				Locale.getDefault());
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		String beforeSplit = format.format(date);
		String[] afterSplit = beforeSplit.split(" ");
		return afterSplit[0] + "\n" + afterSplit[1];
	}

	private class Holder {
		ImageView profileImageView;
		TextView userName;
		TextView repostedBy;
		ImageView follow;
		LinearLayout topTenIndicator;
		TextView topTenRank;
		TextView date;
		ImageView postBody;
		ImageButton like;
		TextView comment;
		ImageButton repost;
		ImageButton more;
		TextView likeCount;
		TextView repostCount;
		ProgressBar postLoading;
	}

	public void handelaction(Intent intent) {
		final PostViewModel post = (PostViewModel) intent.getExtras()
				.getSerializable(INTENT_EXTRA_POST);
		final int position = intent.getIntExtra(INTENT_EXTRA_POSITION, 0);
		final int action = intent.getIntExtra(INTENT_EXTRA_ACTION, 0);
		final int commentCount = intent.getIntExtra(
				CommentActivity.CommentsCount, 0);
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (post != null && post.getPostId() != 0 && action != 0) {
					if (action == ACTION_COMMENT) {
						activity.startActivity(CommentActivity.getIntent(
								activity, post.getPostId(), post
										.getCommentCount(), whichTab,
								post.getUserId() == UserManager.getInstance()
										.getCurrentUserId() ? true : false));
					} else if (action == ACTION_OPEN_LIKE) {
						activity.startActivity(LikeActivity.getIntent(activity,
								post.getPostId(), post.getLikeCount()));
					} else if (action == ACTION_LIKe) {
						WallManager.getInstance().handleLike(
								WallManager.LIKE_POST,
								UserManager.getInstance().getCurrentUserId(),
								post.getPostId());
					} else if (action == ACTION_DISLIKe) {
						WallManager.getInstance().handleLike(
								WallManager.DISLIKE_POST,
								UserManager.getInstance().getCurrentUserId(),
								post.getPostId());
					} else if (action == ACTION_FOLLOW) {
						WallManager.getInstance().handleFollow(
								UserManager.getInstance().getCurrentUserId(),
								post.getUserId(), post.getPostId(), activity);
					} else if (action == ACTION_REPOST) {
						if (UserManager.getInstance().getCurrentUserId() == post
								.getUserId())
							UIUtils.showToast(activity, activity.getResources()
									.getString(R.string.cantRepost));
						else {
							WallManager.getInstance().handleRepost(
									UserManager.getInstance()
											.getCurrentUserId(),
									post.getPostId());
							UIUtils.showToast(activity, activity.getResources()
									.getString(R.string.Home_repostAlert));
						}
					}
					if (getRowFromList(list, post.getPostId()) != null) {
						if (action == ACTION_LIKe) {
							((ImageButton) getRowFromList(list,
									post.getPostId()).findViewById(
									R.id.likeButton))
									.setImageResource(R.drawable.like_sc);
							((TextView) getRowFromList(list, post.getPostId())
									.findViewById(R.id.likes_num_txt)).setText(String
									.valueOf(post.getLikeCount() + 1));
						} else if (action == ACTION_DISLIKe) {
							((ImageButton) getRowFromList(list,
									post.getPostId()).findViewById(
									R.id.likeButton))
									.setImageResource(R.drawable.like);
							((TextView) getRowFromList(list, post.getPostId())
									.findViewById(R.id.likes_num_txt)).setText(String
									.valueOf(post.getLikeCount() - 1));
						} else if (action == ACTION_COMMENT) {
							activity.startActivity(CommentActivity.getIntent(
									activity,
									post.getPostId(),
									post.getCommentCount(),
									whichTab,
									post.getUserId() == UserManager
											.getInstance().getCurrentUserId() ? true
											: false));
						} else if (action == ACTION_FOLLOW) {
							((ImageView) getRowFromList(list, post.getPostId())
									.findViewById(R.id.follow))
									.setImageResource(R.drawable.followed);
						} else if (action == ACTION_REPOST) {
							if (UserManager.getInstance().getCurrentUserId() == post
									.getUserId())
								UIUtils.showToast(
										activity,
										activity.getResources().getString(
												R.string.cantRepost));
							else {
								((ImageButton) getRowFromList(list,
										post.getPostId()).findViewById(
										R.id.repostButton))
										.setImageResource(R.drawable.repost_sc);
								UIUtils.showToast(
										activity,
										activity.getResources().getString(
												R.string.Home_repostAlert));
							}
						} else if (action == ACTION_OPEN_LIKE) {
							activity.startActivity(LikeActivity.getIntent(
									activity, post.getPostId(),
									post.getLikeCount()));
						} else if (action == ACTION_COMMENT_COUNT) {
							if (commentCount > 0) {
								((TextView) getRowFromList(list,
										post.getPostId()).findViewById(
										R.id.commentButton)).setText(String
										.valueOf(commentCount));
							} else {
								((TextView) getRowFromList(list,
										post.getPostId()).findViewById(
										R.id.commentButton)).setText("");
							}
						}
					}
				} else if (post != null && post.getPostId() != 0
						&& action == ACTION_COMMENT_COUNT) {
					if (commentCount > 0) {
						((TextView) getRowFromList(list, post.getPostId())
								.findViewById(R.id.commentButton))
								.setText(String.valueOf(commentCount));
					} else {
						((TextView) getRowFromList(list, post.getPostId())
								.findViewById(R.id.commentButton)).setText("");
					}
				}
			}
		});

	}

	// public void showToast(final String msg) {
	// activity.runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
	// }
	// });
	// }

	private Intent intentBuilder(PostViewModel post, int action, int position) {
		return new Intent().putExtra(INTENT_EXTRA_POST, post)
				.putExtra(INTENT_EXTRA_ACTION, action)
				.putExtra(INTENT_EXTRA_POSITION, position);
	}

}
