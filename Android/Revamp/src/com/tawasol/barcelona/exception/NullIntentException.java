package com.tawasol.barcelona.exception;

/**
 * An exception throw in case you try to open login activity with intent or for result
 * and you mistakenly passed a null intent
 * 
 * @author Basyouni
 * 
 *
 */
public class NullIntentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2187442454884424444L;

	static String msg = "you have to give an intent for LOGIN_FOR_RESULT or LOGIN_WITH_INTENT";
	
	public NullIntentException(){
		super(msg);
	}
}
