/**
 * 
 */
package com.tawasol.barcelona.exception;

/**
 * @author Basyouni
 *
 */
public class LinkMissingException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static String msg = "link field is missing you have to provide at least one link in status update";

	public LinkMissingException(){
		super(msg);
	}
	

}
