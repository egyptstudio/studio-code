package com.tawasol.barcelona.data.connection;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.tawasol.barcelona.data.helper.DataHelper;

/**
 * This a factory class for all REST type connections [Post - Get] which is used to perform both post and get requests.<br>
 * This class basically works as a black box whose input are the target URL and parameters for the request and output is the
 * data returned from server in the form of String.
 * @author Morabea
 *
 */
public class RESTRequestFactory {
	
	private final String BASE_SERVICE_URL;
	private static int CONNECTION_TIME_OUT = 30 * 1000;  // 30 seconds is the default
	private static int SOCKET_TIME_OUT = 30 * 1000;  // 30 seconds is the default
	
	/* Constructors */
	private RESTRequestFactory(String baseUrl) {
		this(baseUrl, CONNECTION_TIME_OUT, SOCKET_TIME_OUT);
	}
	
	private RESTRequestFactory(String baseUrl, int connTimeout) {
		this(baseUrl, connTimeout, SOCKET_TIME_OUT);
	}
	
	private RESTRequestFactory(String baseUrl, int connTimeout, int socketTimeout) {
		BASE_SERVICE_URL = baseUrl;
		CONNECTION_TIME_OUT = connTimeout;
		SOCKET_TIME_OUT = socketTimeout;
	}
	
	/* Factory methods */
	/**
	 * Returns an object with default configurations.
	 * @param baseUrl
	 * @return
	 */
	public static RESTRequestFactory newInstance(String baseUrl) {
		return newInstance(baseUrl, CONNECTION_TIME_OUT, SOCKET_TIME_OUT);
	}
	
	public static RESTRequestFactory newInstance(String baseUrl, int connTimeout) {
		return newInstance(baseUrl, connTimeout, SOCKET_TIME_OUT);
	}
	
	public static RESTRequestFactory newInstance(String baseUrl, int connTimeout, int socketTimeout) {
		return new RESTRequestFactory(baseUrl, connTimeout, socketTimeout);
	}
	
	
	private static HttpClient newHttpClient() {
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIME_OUT);
		HttpConnectionParams.setSoTimeout(params, SOCKET_TIME_OUT); 
		return new DefaultHttpClient(params);
	}
	
	/* POST Request */
	
	@SuppressWarnings("deprecation")
	private static HttpPost getPostRequest(String baseUrl, String method, Map<String, Object> postParams) 
			throws UnsupportedEncodingException {
		// Obtaining a request object
		HttpPost mPost = new HttpPost(getFullPostUrl(baseUrl, method));
		// Adding parameters to request if any exist
		if (postParams != null) {
			MultipartEntityBuilder multipartEntity = newMultiPartEntityBuilder();
			for (Map.Entry<String, Object> entry : postParams.entrySet()) {
				if (entry.getValue() instanceof File)
					multipartEntity.addBinaryBody(entry.getKey(), (File) entry.getValue());
				else
					multipartEntity.addPart(entry.getKey(), 
							new StringBody((String) entry.getValue(), Charset.forName("UTF-8")));
			}
			mPost.setEntity(multipartEntity.build());
		}
		return mPost;
	}
	
	private static MultipartEntityBuilder newMultiPartEntityBuilder() {
		MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
//		multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//		multipartEntity.setCharset(Charset.forName("UTF-8"));
		return multipartEntity;
	}
	
	/**
	 * Performs a POST request synchronously and returns the {@linkplain String} value returned from server.<br>
	 * <b>NOTE:</b> This method needs to be invoked <b>off</b> the UI thread.
	 * @param method Target action/resource at server, if any.
	 * @param postParams Parameters of the POST request.
	 * @return String value of data.
	 * @throws ClientProtocolException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public String performPostRequest(String method, Map<String, Object> postParams) 
			throws ClientProtocolException, UnsupportedEncodingException, IOException {
		HttpResponse response = newHttpClient().execute(getPostRequest(BASE_SERVICE_URL, method, postParams));
		return EntityUtils.toString(response.getEntity());
	}
	
	private static String getFullPostUrl(String baseUrl, String method) {
		return getFullUrl(baseUrl, method);
	}
	
	/* GET Request */
	
	@SuppressWarnings("deprecation")
	private static String getFullGetUrl(String baseUrl, String method, Map<String, String> getParams) {
		String fullUrl = getFullUrl(baseUrl, method);
		if (getParams != null) {
			int index = 0;
			if (!getParams.isEmpty())
				fullUrl += DataHelper.URL_QUESTION_MARK;
			for (Map.Entry<String, String> entry : getParams.entrySet()) {
				if (index != 0)
					fullUrl += DataHelper.URL_AMPERSAND;
				fullUrl += entry.getKey() + DataHelper.URL_EQUAL + entry.getValue();
			}
		}
		// URL is encoded to make sure no characters that aren't allowed in a URL is present
		return URLEncoder.encode(fullUrl);
	}
	
	private static HttpGet getGetRequest(String baseUrl, String method, Map<String, String> getParams) {
		return new HttpGet(getFullGetUrl(baseUrl, method, getParams));
	}
	
	/**
	 * Performs a GET request synchronously and returns the {@linkplain String} value returned from server.<br>
	 * <b>NOTE:</b> 
	 * <ul>  
	 *  <li> This method needs to be invoked <b>off</b> the UI thread </li>
	 *  <li> The parameters of the request would be encoded,  </li>
	 * </ul>
	 * @param method Target action/resource at server, if any.
	 * @param getParams Parameters of the GET request.
	 * @return String value of data.
	 * @throws ClientProtocolException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public String performGetRequest(String method, Map<String, String> getParams) throws ClientProtocolException,
			UnsupportedEncodingException, IOException {
		HttpResponse response = newHttpClient().execute(getGetRequest(BASE_SERVICE_URL, method, getParams));
		return EntityUtils.toString(response.getEntity());
	}
	
	/**
	 * Returns a fully concatenated URL using a base URL and a target page.
	 * @param baseUrl Base URL of the service
	 * @param method Target page or resource at the server side.
	 * @return
	 */
	private static String getFullUrl(String baseUrl, String method) {
		return DataHelper.appendToUrl(baseUrl, method);
	}
}
