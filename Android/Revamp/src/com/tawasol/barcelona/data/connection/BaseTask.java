package com.tawasol.barcelona.data.connection;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;

public abstract class BaseTask extends AsyncTask<Object, Integer, Object> {

	/**
	 * Above API 11 the normal {@link AsyncTask#execute(Object...)} method processes threads serially,
	 * so instead we use {@link AsyncTask#executeOnExecutor(java.util.concurrent.Executor, Object...)} so that we can use
	 * the parallel behavior.
	 */
	@SuppressLint("NewApi")
	public void executePerApi() {
		if (Build.VERSION.SDK_INT >= 11) {
			executeOnExecutor(THREAD_POOL_EXECUTOR);
		} else {
			execute();
		}
	}
	
}
