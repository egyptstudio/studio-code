package com.tawasol.barcelona.data.connection;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

/**
 * An implementation of the REST web services to retreive data from server.<br>
 * If you need a more configurable versions of the requests you can use the factory methods to
 *  get a {@link RestRequestFactory} object with custom configurations.
 * @author Morabea
 */
public class RestServiceAgent {
	
	/**
	 * Performs a GET request to the server and returns a {@linkplain String} as a result
	 * @param baseUrl
	 * @param method
	 * @param getParams
	 * @return
	 * @throws ClientProtocolException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static String get(String baseUrl, String method, Map<String, String> getParams) 
			throws ClientProtocolException, UnsupportedEncodingException, IOException {
		return RESTRequestFactory.newInstance(baseUrl).performGetRequest(method, getParams);
	}
	
	/**
	 * Performs a POST request to the server and returns a {@linkplain String} as a result
	 * @param baseUrl
	 * @param method
	 * @param postParams
	 * @return
	 * @throws ClientProtocolException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static String post(String baseUrl, String method, Map<String, Object> postParams) 
			throws ClientProtocolException, UnsupportedEncodingException, IOException {
		return RESTRequestFactory.newInstance(baseUrl).performPostRequest(method, postParams);
	}
	
}
