package com.tawasol.barcelona.data.connection;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.tawasol.barcelona.entities.User;

public class ParameterGenerator {

	public static Map<String, Object> getRegisterParams(User user) {
		Map<String, Object> params = new HashMap<String, Object>(6);
		params.put(Params.User.USER_NAME, user.getFullName());
		params.put(Params.User.PASSWORD, user.getPassword());
		params.put(Params.User.EMAIL, user.getEmail());
		params.put(Params.User.DATE_OF_BIRTH, user.getDateOfBirth());
		params.put(Params.User.GENDER, user.getGender());
		params.put(Params.User.COUNTRY_ID, String.valueOf(user.getCountryId()));
		return params;
	}
	
	public static String getLoginJson(String username, String password) throws JSONException {
		return new JSONObject().put(Params.User.USER_NAME, username)
							.put(Params.User.PASSWORD, password).toString();
	}
	
	public static Map<String, Object> getParamMap(Object... json) {
		if (json == null || json[0] == null) { return null; }
		
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("data", String.valueOf(json[0]));
		if (json.length > 1 && json[1] != null)
			params.put("photo", json[1]);
		return params;
	}
	
	public static Map<String, Object> getPosterParamMap(Object... json) {
		if (json == null || json[0] == null) { return null; }
		
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("data", String.valueOf(json[0]));
		if (json.length > 1 && json[1] != null)
			params.put(Params.Studio.USER_PHOTO, json[1]);
		if(json.length > 2 && json[2] != null)
			params.put(Params.Studio.MASKED_PHOTO, json[1]);
		return params;
	}
	
}
