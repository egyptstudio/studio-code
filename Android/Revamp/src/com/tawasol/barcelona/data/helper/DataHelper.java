package com.tawasol.barcelona.data.helper;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.tawasol.barcelona.business.BusinessObject;
import com.tawasol.barcelona.entities.Post;

/**
 * Utility class to hold all data layer helper methods
 * @author Morabea
 *
 */
public class DataHelper {
	
	public static final String URL_SEPARATOR = "/";
	public static final String URL_AMPERSAND = "&";
	public static final String URL_QUESTION_MARK = "?";
	public static final String URL_EQUAL = "=";
	
	
	private static final Set<Class<?>> wrapperTypesMap = new HashSet<Class<?>>(8);
	static {
		wrapperTypesMap.add(Boolean.class);
		wrapperTypesMap.add(Byte.class);
		wrapperTypesMap.add(Character.class);
		wrapperTypesMap.add(Short.class);
		wrapperTypesMap.add(Integer.class);
		wrapperTypesMap.add(Long.class);
		wrapperTypesMap.add(Double.class);
		wrapperTypesMap.add(Float.class);
	}
	
	public static boolean isWrapperType(Class<?> cls) {
		if (cls == null)
			return false;
		return wrapperTypesMap.contains(cls);
	}

	/**
	 * Appends a resource or target to a URL, for example adding if the URL is <code>https://www.google.com.eg</code> and the appended part is
	 * <code>images</code>, the result would be <code>https://www.google.com.eg/images</code>
	 * @param url
	 * @param appended
	 * @return
	 */
	public static String appendToUrl(String url, String appended) {
		if (url == null)
			// TODO: Add exception handling
			throw new RuntimeException();
		
		if (appended == null)
			return url;
		
		if (!url.endsWith(URL_SEPARATOR) && !appended.startsWith(URL_SEPARATOR))
			return url + URL_SEPARATOR + appended;
		
		if (url.endsWith(URL_SEPARATOR) && appended.startsWith(URL_SEPARATOR)) {
			// Removing the extra URL separators
			String editedBaseUrl = url.replace(URL_SEPARATOR, "");
			return editedBaseUrl + appended;
		}
		
		return url + appended;
	}
	
	/**
	 * Custom {@linkplain Boolean} serializer/deserializer for Gson to map ints to booleans and vice versa
	 */
	private static TypeAdapter<Boolean> booleanAsIntAdapter = new TypeAdapter<Boolean>() {
		@Override
		public void write(JsonWriter out, Boolean value) throws IOException {
			if (value)
				out.value(1);
			else
				out.value(0);
		}
		@Override
		public Boolean read(JsonReader in) throws IOException {
			JsonToken peek = in.peek();
		    switch (peek) {
		    case BOOLEAN:
		      return in.nextBoolean();
		    case NUMBER:
		      return in.nextInt() != 0;
		    default:
		      throw new IllegalStateException("Expected BOOLEAN or NUMBER but was " + peek);
		    }
		}
	};
	
	/**
	 * Returns a configured instance of {@linkplain Gson} ready for use.
	 * @return
	 */
	private static Gson getGson() {
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation()
//				.registerTypeAdapter(Post.class, new PostTypeDeserializer())
				.registerTypeAdapter(Boolean.class, booleanAsIntAdapter.nullSafe())
				.registerTypeAdapter(boolean.class, booleanAsIntAdapter.nullSafe());
		return builder.create();
	}
	
	/**
	 * Converts a JSON formatted string to an object according to the passed class signature
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T extends BusinessObject> T deserialize(String json, Class<T> clazz) {
	    return getGson().fromJson(json, clazz);
	}
	
	
	/**
	 * Converts an object with the passed signature to a JSON formatted string.
	 * @param obj
	 * @param clazz
	 * @return
	 */
	public static <T extends BusinessObject> String serialize(T obj, Class<T> clazz) {
	    return getGson().toJson(obj, clazz);
	}
	
	/**
	 * Converts an object with the passed {@link Type} to a JSON formatted string.
	 * @param obj
	 * @param type
	 * @return
	 */
	public static String serialize(Object obj, Type type) {
	    return getGson().toJson(obj, type);
	}
	
	/**
	 * Converts a JSON formatted string to an object according to the passed type, this overloaded version is provided to enable
	 * deserializing structure that cannot be represented by a simple {@linkplain Class} 
	 * @param json
	 * @param type
	 * @return
	 */
	public static <T extends BusinessObject> List<T> deserializeList(String json, Class<T> clazz) {
	    Gson gson = getGson();
	    JsonElement jsonElement = new JsonParser().parse(json);
	    JsonArray array= jsonElement.getAsJsonArray();
	    Iterator<JsonElement> iterator = array.iterator();
	    List<T> list = new ArrayList<T>();
	     
	    while(iterator.hasNext()){
	        JsonElement tempJson = (JsonElement)iterator.next();
	        list.add(gson.fromJson(tempJson, clazz));
	    }
	    
	    return list;
	}
	
	public static <T extends BusinessObject> String serializeList(List<T> list, Class<T> clazz) {
	    JsonArray array = new JsonArray();
	    
	    for (T entity : list) {
	    	array.add(new JsonParser().parse(serialize(entity, clazz)));
	    }
	    
	    return array.getAsString();
	} 
	
	/**
	 * Formats the passed map into a JSON formatted string.
	 * @param params
	 * @return
	 * @throws JSONException
	 */
	public static String formatAsJSON(Map<String, Object> params) throws JSONException {
		if (params == null) { return null; }
		
		JSONObject obj = new JSONObject();
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			Object value = entry.getValue();
			if (value instanceof String || isWrapperType(value.getClass())) {
				obj.put(entry.getKey(), getJsonValue(value));
			} else {
				try {
					Class<?> cl = Class.forName(value.getClass().getName());
					obj.put(entry.getKey(), getJsonValue(serialize(value, cl)));
				} catch (ClassNotFoundException e) {
					// Do nothing, we can't even find the class, we ain't gonna serialize it
					e.printStackTrace();
				}
			}
		}
		return obj.toString();
	}

	/**
	 * Returns the JSON value of an {@linkplain Object}, if that object is already formatted
	 * as {@linkplain JSONObject} or {@linkplain JSONArray} then an instance of those classes are returned,
	 * other wise a {@linkplain String} representation is returned.
	 * @param value
	 * @return {@link String} or {@link JSONObject} in case the passed object is formatted in the form
	 * of a {@link JSONObject} or {@link JSONArray} in case the passed object is formatted in the form
	 * of {@link JSONArray}
	 */
	private static Object getJsonValue(Object value) {
		Object result = null;
		try {
			result = new JSONObject(value.toString());
		} catch (Exception e) {
			// Nothing
		}
		if (result == null) {
			try {
				result = new JSONArray(value.toString());
			} catch (Exception e) {
				// Nothing
			}
		}
		if (result == null)
			result = String.valueOf(value);
		return result;
	}
	
}
