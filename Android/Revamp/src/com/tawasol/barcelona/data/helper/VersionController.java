package com.tawasol.barcelona.data.helper;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.barcelona.data.connection.ParameterGenerator;
import com.tawasol.barcelona.data.connection.RestServiceAgent;
import com.tawasol.barcelona.data.connection.URLs;
import com.tawasol.barcelona.data.parsing.ParsingManager;
import com.tawasol.barcelona.entities.SimpleBusinessObject;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.responses.BaseResponse;

/**
 * Helper class to provide the base URL for the application based on its version [ Production - Staging ]
 * @author Morabea
 *
 */
public class VersionController {
	
	public static final class VersionInfo extends SimpleBusinessObject {
		
		private static final long serialVersionUID = 1799252155044611546L;
		
		public static final int UPDATE_STATUS_NONE = 1;
		public static final int UPDATE_STATUS_OPTIONAL = 2;
		public static final int UPDATE_STATUS_FORCE = 3;
		
		public VersionInfo(String url, int status) {
			baseUrl = url;
			updateStatus = status;
		}
		
		@Expose @SerializedName("baseURL")
		String baseUrl;
		@Expose 
		int updateStatus;
	}
	
	private static final String PREFS_BASE_URL_KEY = "com.tawasol.barcelona.data.helper.baseUrl";
	private static final String PREFS_UPDATE_STATUS_KEY = "com.tawasol.barcelona.data.helper.statusUpdate";
	
	private static final String BASE_URL = "http://barca.tawasoldev.com/barca/index.php/api/";

	public static String getBaseUrl() throws AppException {
		return getVersionInfo(false).baseUrl;
	}
	
	public static VersionInfo getVersionInfo(boolean forceFromServer) throws AppException {
		if (forceFromServer)
			clearVersionInfo();
		
		VersionInfo info = loadVersionInfo();
		if (info == null)
			info = getVersionInfoFromServer();
		return info;
	}
	
	public static boolean isUpdateRequired() {
		return VersionController.getUpdateStatus() == VersionInfo.UPDATE_STATUS_FORCE;
	}
	
	private static int getUpdateStatus() {
		return SharedPrefrencesDataLayer.getIntPreferences(App.getInstance().getApplicationContext(),
				PREFS_UPDATE_STATUS_KEY, VersionInfo.UPDATE_STATUS_NONE);
	}
	
	private static VersionInfo loadVersionInfo() {
		Context context = App.getInstance().getApplicationContext();
		String baseUrl = SharedPrefrencesDataLayer.getStringPreferences(context, PREFS_BASE_URL_KEY, "");
		int status = SharedPrefrencesDataLayer.getIntPreferences(context, PREFS_UPDATE_STATUS_KEY, VersionInfo.UPDATE_STATUS_NONE);
		return TextUtils.isEmpty(baseUrl) ? null : new VersionInfo(baseUrl, status);
	}

	private static VersionInfo getVersionInfoFromServer() throws AppException {
		try {
			String serverResponse = RestServiceAgent.post(BASE_URL, URLs.Methods.GET_BASE_URL, 
					ParameterGenerator.getParamMap(DataHelper
							.formatAsJSON(getParams())));
			BaseResponse baseResponse = ParsingManager.parseServerResponse(serverResponse);
			return getVersionInfoFromServerResponseOrThrow(baseResponse);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}
	
	private static VersionInfo getVersionInfoFromServerResponseOrThrow(BaseResponse response) throws AppException {
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			// An error has occurred, check if server returned a validation rule or this is a general failure
			if(response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(response.getValidationRule().errorMessage);
			else
				throw new IllegalStateException("ws_getBaseUrl failed to retireve data!");
		}
		// Parse version info
		VersionInfo info = DataHelper.deserialize(response.getData(), VersionInfo.class);
		
		// Save results for future look-up
		saveVersionInfo(info);
		
		// Throw if update status is "UPDATE_STATUS_FORCE"
		if (info.updateStatus == VersionInfo.UPDATE_STATUS_FORCE)
			throw new AppException(AppException.UPDATE_STATUS_FORCE_EXCEPTION);
		
		return info;
	}
	
	private static void clearVersionInfo() {
		saveVersionInfo(null);
	}
	
	private static void saveVersionInfo(VersionInfo info) {
		Context context = App.getInstance().getApplicationContext();
		String baseUrl = info == null ? "" : info.baseUrl;
		int updateStatus = info == null ? VersionInfo.UPDATE_STATUS_NONE : info.updateStatus;
		SharedPrefrencesDataLayer.saveStringPreferences(context, PREFS_BASE_URL_KEY, baseUrl);
		SharedPrefrencesDataLayer.saveIntPreferences(context, PREFS_UPDATE_STATUS_KEY, updateStatus);
	}

	private static String getVersionName() {
		try {
			Context cxt = App.getInstance().getApplicationContext();
			PackageManager manager = cxt.getPackageManager();
			PackageInfo info = manager.getPackageInfo(cxt.getPackageName(), 0);
			return info.versionName;
		} catch (NameNotFoundException e) {
			throw new IllegalStateException("No such package installed on the device: ", e);
		}
	}

	private static Map<String, Object> getParams() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("versionNumber", getVersionName());
		params.put("deviceType", "1" /* Server enumeration of Android type */);
		return params;
	}
	
	
}
