package com.tawasol.barcelona.data.helper;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.tawasol.barcelona.entities.AdPost;
import com.tawasol.barcelona.entities.Post;
import com.tawasol.barcelona.entities.PostViewModel;

class PostTypeDeserializer implements JsonDeserializer<Post> {

	@Override
	public Post deserialize(JsonElement json, Type type,
			JsonDeserializationContext context) throws JsonParseException {
		
		JsonObject jsonObject = json.getAsJsonObject();
		
		int postType = jsonObject.get("postType").getAsInt();
		
		Class<? extends Post> desiredClass = null;
		
		switch (postType) {
		case Post.POST_TYPE_NORMAL:
			desiredClass = PostViewModel.class;
			break;
		case Post.POST_TYPE_AD:
			desiredClass = AdPost.class;
			break;
		}
		
		return DataHelper.deserialize(json.toString(), desiredClass);
	}

}
