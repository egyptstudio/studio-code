package com.tawasol.barcelona.data.parsing;

import org.json.JSONException;
import org.json.JSONObject;

import com.tawasol.barcelona.business.validation.SimpleValidationRule;
import com.tawasol.barcelona.business.validation.ValidationRule;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.responses.BaseResponse;

public class ParsingManager {

	public static BaseResponse parseServerResponse(String result)
			throws JSONException {
		ValidationRule rule = null;
		JSONObject jsonObject = new JSONObject(result);

		int status = jsonObject.getInt(Params.STATUS);

		if (status == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR) {
			rule = new SimpleValidationRule();
			rule.ruleName = jsonObject
					.getString(Params.ValidationRule.RULE_NAME);
			rule.errorMessage = jsonObject
					.getString(Params.ValidationRule.ERROR_MESSAGE);
		}

		return new BaseResponse(status, jsonObject.optString(Params.DATA), rule);
	}

}
