package com.tawasol.barcelona.data.cache;



import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.StudioPhoto;

public class StudioPhotosTable extends AbstractTable<StudioPhoto> {

	private static StudioPhotosTable sInstance;
	
	// Table name
	private static final String TABLE_STUDIO_PHOTO = "Studio_Photos";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String PHT_PHOTO_ID      = "studioPhotoId";
		private static final String PHT_FOLDER_ID         = "folderId";
		private static final String PHT_PHOTO_URL         = "photoUrl";
		private static final String PHT_PHOTO_ORIENTATION = "photoOrientation";
		private static final String PHT_USE_COUNT = "useCount";
		private static final String PHT_CAPTURE_TYPE = "captureType";
		private static final String PHT_PHOTO_NAME = "photoName";
		private static final String PHT_UPLOAD_DATE = "uploadDate";
		private static final String PHT_REQUIRED_POINTS = "requiredPoints";
		private static final String PHT_IS_PREMIUM = "isPremium";
		private static final String PHT_IS_PURCHASED = "isPurchased";
	}
	
	// Create Table Folders
	private static final String CREATE_TABLE_STUDIO_PHOTOS  = "CREATE TABLE " + TABLE_STUDIO_PHOTO 
			+ "(" 
				+ Fields.PHT_PHOTO_ID + " INTEGER PRIMARY KEY," 
				+ Fields.PHT_FOLDER_ID      + " INTEGER," 
				+ Fields.PHT_PHOTO_URL         + " TEXT,"
				+ Fields.PHT_PHOTO_ORIENTATION         + " INTEGER,"
				+ Fields.PHT_USE_COUNT         + " INTEGER," 
				+ Fields.PHT_CAPTURE_TYPE         + " INTEGER," 
				+ Fields.PHT_PHOTO_NAME         + " TEXT," 
				+ Fields.PHT_UPLOAD_DATE         + " INTEGER," 
				+ Fields.PHT_REQUIRED_POINTS         + " INTEGER," 
				+ Fields.PHT_IS_PREMIUM         + " BOOLEAN," 
				+ Fields.PHT_IS_PURCHASED         + " BOOLEAN" 
				
			+ ")";
		
	public static StudioPhotosTable getInstance() {
		if (sInstance == null)
			sInstance = new StudioPhotosTable();
		return sInstance;
	}
	
	private StudioPhotosTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_STUDIO_PHOTOS);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		return TABLE_STUDIO_PHOTO;
	}

	@Override
	public String[] getProjection() {
		return new String[] {Fields.PHT_PHOTO_ID, Fields.PHT_FOLDER_ID, 
				Fields.PHT_PHOTO_URL , Fields.PHT_PHOTO_ORIENTATION,
				Fields.PHT_USE_COUNT , Fields.PHT_CAPTURE_TYPE,
				Fields.PHT_PHOTO_NAME , Fields.PHT_UPLOAD_DATE,
				Fields.PHT_REQUIRED_POINTS, Fields.PHT_IS_PREMIUM,Fields.PHT_IS_PURCHASED};
	}

	@Override
	protected ContentValues getContentValues(StudioPhoto entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.PHT_PHOTO_ID      , entity.getStudioPhotoId());
	    values.put(Fields.PHT_FOLDER_ID         , entity.getFolderId());
	    values.put(Fields.PHT_PHOTO_URL              , entity.getPhotoURL());
	    values.put(Fields.PHT_PHOTO_ORIENTATION              , entity.getPhotoOrientation());
	    values.put(Fields.PHT_USE_COUNT              , entity.getUseCount());
	    values.put(Fields.PHT_CAPTURE_TYPE              , entity.getCaptureType());
	    values.put(Fields.PHT_PHOTO_NAME              , entity.getPhotoName());
	    values.put(Fields.PHT_UPLOAD_DATE              , entity.getUploadDate());
	    values.put(Fields.PHT_REQUIRED_POINTS              , entity.getRequiredPoints());
	    values.put(Fields.PHT_IS_PREMIUM              , entity.getIsPermium());
	    values.put(Fields.PHT_IS_PURCHASED              , entity.getIsPurchased());
		return values;
	}

	@Override
	protected StudioPhoto getObjFromCursor(Cursor cursor) {
		StudioPhoto photo = new StudioPhoto();
		photo.setStudioPhotoId(cursor.getInt(cursor.getColumnIndex(Fields.PHT_PHOTO_ID)));
		photo.setFolderId(cursor.getInt(cursor.getColumnIndex(Fields.PHT_FOLDER_ID)));
		photo.setPhotoURL(cursor.getString(cursor.getColumnIndex(Fields.PHT_PHOTO_URL)));
		photo.setPhotoOrientation(cursor.getInt(cursor.getColumnIndex(Fields.PHT_PHOTO_ORIENTATION)));
		photo.setUseCount(cursor.getInt(cursor.getColumnIndex(Fields.PHT_USE_COUNT)));
		photo.setCaptureType(cursor.getInt(cursor.getColumnIndex(Fields.PHT_CAPTURE_TYPE)));
		photo.setPhotoName(cursor.getString(cursor.getColumnIndex(Fields.PHT_PHOTO_NAME)));
		photo.setUploadDate(cursor.getInt(cursor.getColumnIndex(Fields.PHT_UPLOAD_DATE)));
		photo.setRequiredPoints(cursor.getInt(cursor.getColumnIndex(Fields.PHT_REQUIRED_POINTS)));
		photo.setIsPermium(cursor.getInt(cursor.getColumnIndex(Fields.PHT_IS_PREMIUM)));
		photo.setIsPurchased(cursor.getInt(cursor.getColumnIndex(Fields.PHT_IS_PURCHASED)));

		return photo;
	}

	public List<StudioPhoto> getPhotos(int folderId){
		
		Cursor cursor = getDb().query(getTableName(), getProjection(), Fields.PHT_FOLDER_ID + " = " + folderId, null, null, null, null);
		
		List<StudioPhoto> photos = new ArrayList<StudioPhoto>(cursor.getCount() >= 0 ?
				cursor.getCount() : 10/* Normal initial capacity for ArrayList  */);
		if (cursor.moveToFirst()) {
			do {
				photos.add(getObjFromCursor(cursor));
			} while (cursor.moveToNext());
		}
		
		return photos;
		
	}
	
public List<StudioPhoto> getPurchasedPhotos(){
		
		Cursor cursor = getDb().query(getTableName(), getProjection(), Fields.PHT_IS_PURCHASED + " = " + 1, null, null, null, null);
		
		List<StudioPhoto> photos = new ArrayList<StudioPhoto>(cursor.getCount() >= 0 ?
				cursor.getCount() : 10/* Normal initial capacity for ArrayList  */);
		if (cursor.moveToFirst()) {
			do {
				photos.add(getObjFromCursor(cursor));
			} while (cursor.moveToNext());
		}
		
		return photos;
		
	}

public void setPurchased(int studioPhotoId )
{
		ContentValues values = new ContentValues();
		values.put(Fields.PHT_IS_PURCHASED, 1);
		DatabaseManager
				.getInstance(App.getInstance().getApplicationContext())
				.getDb()
				.update(TABLE_STUDIO_PHOTO,
						values,
					    Fields.PHT_PHOTO_ID + " = "+ studioPhotoId,
						null);

	}

public int getCount(){
	String query = "select * from "+TABLE_STUDIO_PHOTO;
	Cursor cursor = DatabaseManager.getInstance(App.getInstance()
			.getApplicationContext()).getReadableDatabase().rawQuery(query, null);
	return cursor.getCount();
}
	
}
