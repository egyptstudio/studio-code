package com.tawasol.barcelona.data.cache;

import android.content.Context;
import android.content.SharedPreferences;
import com.tawasol.barcelona.application.App;

public class SharedPrefsHelper {

	private static final String SHARED_PREFS_NAME = "com.tawasol.amcham.amchamprefs";

	private static SharedPreferences getSharedPreferences() {
		return App.getInstance().getSharedPreferences(
				SHARED_PREFS_NAME, Context.MODE_PRIVATE);
	}

	public static long getLongVal(String key) {
		return getSharedPreferences().getLong(key, -1);
	}

	public static void setLongVal(String key, long value) {
		getSharedPreferences().edit().putLong(key, value).commit();
	}

	public static int getIsChased(String key) {
		return getSharedPreferences().getInt(key, 0);

	}

	public static int getIntVal(String key) {
		return getSharedPreferences().getInt(key, -1);
	}

	public static void setIntVal(String key, int value) {
		getSharedPreferences().edit().putInt(key, value).commit();
	}

	public static void SetIsChashedVal(String key, int value) {
		getSharedPreferences().edit().putInt(key, value).commit();

	}
	
	public static void SetISRegisteres(String key,int value)
	{
		getSharedPreferences().edit().putInt(key, value).commit();

	}
	
	public static void setRegiterKey(String key ,String value)
	{
		getSharedPreferences().edit().putString(key, value).commit();

	}
	
	public static String getRegisterKey(String key)
	{
		return getSharedPreferences().getString(key, "");
	}
}
