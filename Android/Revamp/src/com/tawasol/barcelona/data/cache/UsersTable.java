package com.tawasol.barcelona.data.cache;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.User;

public class UsersTable extends AbstractTable<User> {
	
	public UsersTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()), App.getInstance().getApplicationContext());
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unused")
	private static final class Fields implements BaseColumns {
		/** Prevent class form being instantiated */
		private Fields() {}
		
		public static final String PROFILE_PICTURE = "profile_pic";
		
		public static final String USER_NAME = "user_name";
		
		public static final String PASSWORD = "password";
		
		public static final String EMAIL = "email";
		
		public static final String DATE_OF_BIRTH = "date_of_birth";
		
		public static final String GENDER = "gender";
		
		public static final String COUNTRY_ID = "country_id";
		
		public static final String IS_ONLINE = "is_online";
		
		public static final String LAST_LOGIN_DATE = "last_login_date";
	}
	
	private static final String TABLE_NAME = "users";

	@Override
	protected void create(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	@Override
	protected String[] getProjection() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ContentValues getContentValues(User entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected User getObjFromCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
