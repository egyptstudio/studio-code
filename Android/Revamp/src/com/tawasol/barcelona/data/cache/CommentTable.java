package com.tawasol.barcelona.data.cache;

import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.UserComment;

public class CommentTable extends AbstractTable<UserComment> {

	private static CommentTable instance;
	// Table name
	private static final String TABLE_Comment = "commentTable";

	public static final class Fields implements BaseColumns {
		// Column names
		private static final String CommentID = BaseColumns._ID;
		private static final String PostID = "postId";
		private static final String COMMENT_TEXT = "commentText";
		private static final String USER_ID = "userId";
		private static final String FULL_NAME = "fullName";
		private static final String PROFILE_PIC = "profilePic";
		private static final String DATE = "date";
		private static final String IS_FOLLOWING = "isFollowing";
	}

	// Create Table Message
	private static final String CREATE_TABLE_COMMENT = "CREATE TABLE "
			+ TABLE_Comment + "(" + Fields.CommentID + " INTEGER PRIMARY KEY,"
			+ Fields.PostID + " INTEGER," + Fields.COMMENT_TEXT + " TEXT,"
			+ Fields.USER_ID + " INTEGER," + Fields.FULL_NAME + " TEXT,"
			+ Fields.PROFILE_PIC + " TEXT," + Fields.DATE + " INTEGER,"
			+ Fields.IS_FOLLOWING + " INTEGER" // DATETIME
			+ ")";

	public static CommentTable getInstance() {
		if (instance == null)
			instance = new CommentTable();
		return instance;
	}

	public CommentTable() {
		super(DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()), App.getInstance()
				.getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_COMMENT);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		return TABLE_Comment;
	}

	@Override
	protected String[] getProjection() {
		return new String[] { Fields.CommentID, Fields.PostID,
				Fields.COMMENT_TEXT, Fields.USER_ID, Fields.FULL_NAME,
				Fields.PROFILE_PIC, Fields.DATE, Fields.IS_FOLLOWING };
	}

	public List<UserComment> getPostsComments(int postId) {
		return getBySelection(Fields.PostID + " = ?", new String[] { String.valueOf(postId) });
	}
	
	@Override
	protected ContentValues getContentValues(UserComment entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.CommentID, entity.getCommentId());
		values.put(Fields.PostID, entity.getPostId());
		values.put(Fields.COMMENT_TEXT, entity.getCommentText());
		values.put(Fields.USER_ID, entity.getUserId());
		values.put(Fields.FULL_NAME, entity.getUserName());
		values.put(Fields.PROFILE_PIC, entity.getProfilePic());
		values.put(Fields.DATE, entity.getCommentDate());
		values.put(Fields.IS_FOLLOWING, entity.isFollowing() ? 1 : 0);
		return values;
	}

	@Override
	protected UserComment getObjFromCursor(Cursor cursor) {
		UserComment comment = new UserComment();
		comment.setCommentId(cursor.getInt(cursor
				.getColumnIndex(Fields.CommentID)));
		comment.setPostId(cursor.getInt(cursor.getColumnIndex(Fields.PostID)));
		comment.setCommentText(cursor.getString(cursor
				.getColumnIndex(Fields.COMMENT_TEXT)));
		comment.setUserId(cursor.getInt(cursor.getColumnIndex(Fields.USER_ID)));
		comment.setUserName(cursor.getString(cursor
				.getColumnIndex(Fields.FULL_NAME)));
		comment.setProfilePic(cursor.getString(cursor
				.getColumnIndex(Fields.PROFILE_PIC)));
		comment.setDate(cursor.getInt(cursor.getColumnIndex(Fields.DATE)));
		comment.setFollowing(cursor.getInt(cursor
				.getColumnIndex(Fields.IS_FOLLOWING)) == 1);
		return comment;
	}

}
