package com.tawasol.barcelona.data.cache;



import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.KeyValuePairs;

public class TagsTable extends AbstractTable<KeyValuePairs> {

	private static TagsTable sInstance;
	
	// Table name
	private static final String TABLE_TAG = "Tags";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String TAG_ID      = "tagId";
		private static final String POST_ID      = "postId";
		private static final String TAG_NAME         = "tagName";
		private static final String TAG_TYPE = "tagType";
	}
	
	// Create Table Folders
	private static final String CREATE_TABLE_TAGS  = "CREATE TABLE " + TABLE_TAG 
			+ "(" 
				+ Fields.TAG_ID + " INTEGER," 
				+ Fields.POST_ID      + " INTEGER," 
				+ Fields.TAG_NAME  +" TEXT," 
				+ Fields.TAG_TYPE         + " INTEGER" 
			+ ")";
		
	public static TagsTable getInstance() {
		if (sInstance == null)
			sInstance = new TagsTable();
		return sInstance;
	}
	
	private TagsTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_TAGS);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		return TABLE_TAG;
	}

	@Override
	public String[] getProjection() {
		return new String[] {Fields.TAG_ID, Fields.POST_ID, Fields.TAG_NAME,Fields.TAG_TYPE};
	}

	@Override
	protected ContentValues getContentValues(KeyValuePairs entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.TAG_ID      , entity.get_Id());
	    values.put(Fields.POST_ID         , entity.getPostId());
	    values.put(Fields.TAG_NAME              , (String) entity.getText());
	    values.put(Fields.TAG_TYPE              , entity.getType());
		return values;
	}

	@Override
	protected KeyValuePairs getObjFromCursor(Cursor cursor) {
		KeyValuePairs tag = new KeyValuePairs();
		tag.set_Id(cursor.getInt(cursor.getColumnIndex(Fields.TAG_ID)));
		tag.setPostId(cursor.getInt(cursor.getColumnIndex(Fields.POST_ID)));
		tag.setText(cursor.getString(cursor.getColumnIndex(Fields.TAG_NAME)));
		tag.setType(cursor.getInt(cursor.getColumnIndex(Fields.TAG_TYPE)));		
		return tag;
	}
	
}
