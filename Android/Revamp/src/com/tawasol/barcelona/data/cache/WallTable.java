package com.tawasol.barcelona.data.cache;


import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.PostViewModel;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class WallTable extends AbstractTable<PostViewModel> {

	private static WallTable instance;
	// Table name
	private static final String TABLE_WALL = "wallTable";

	public static final class Fields implements BaseColumns {
		// Column names
		private static final String POST_ID = BaseColumns._ID;
		private static final String ORIGINAL_POST_ID = "originalPostId";
		private static final String LAST_UPDATE_TIME = "lastUpdateTime";
		private static final String USER_ID = "userId";
		private static final String STUDIO_PHOTO_ID = "studioPhotoId";
		private static final String POST_PHOTO_URL = "postPhotoUrl";
		private static final String USER_NAME = "userName";
		private static final String USER_COUNTRY = "userCountry";

		private static final String USER_PROFILE_PIC_URL = "userProfilePicUrl";
		private static final String CREATION_TIME = "creationTime";
		private static final String LIKED = "liked";
		private static final String COMMENT_COUNT = "commentCount";
		private static final String LIKE_COUNT = "likeCount";

		private static final String REPORT_COUNT = "reportCount";

		private static final String PHOTO_ORIANTATION = "photoOrientation";
		private static final String PRIVACY = "privacy";
		private static final String CONTEST_RANK = "contestRank";
		private static final String REPORTED = "reported";
		private static final String HIDDEN = "hidden";

		private static final String POST_PHOTO_CAPTION = "postPhotoCaption";
		private static final String REPOSTED = "rePosted";
		private static final String FOLLOWING = "following";
		private static final String FAVORUITE = "favorite";
		private static final String REPOSTED_BY = "repostedBy";

		private static final String REPOSTER_PROFILE_PIC = "rePosterProfilePic";

		private static final String REPOSTER_ID = "rePosterId";
		private static final String REPOSTER_IS_FOLLOWING = "rePosterIsFollowing";
		public static final String WALL_POST = "wallPost";
		public static final String LATEST_POST = "latestPost";
		public static final String TOPTEN = "topTen";
		public static final String MyPicsPost = "myPics";
	}

	// Create Table Message
	private static final String CREATE_TABLE_WALL = "CREATE TABLE "
			+ TABLE_WALL
			+ "("
			+ Fields.POST_ID
			+ " INTEGER PRIMARY KEY,"
			+ Fields.ORIGINAL_POST_ID
			+ " INTEGER,"
			+ Fields.LAST_UPDATE_TIME
			+ " INTEGER,"
			+ Fields.USER_ID
			+ " INTEGER,"
			+ Fields.STUDIO_PHOTO_ID
			+ " INTEGER,"
			+ Fields.POST_PHOTO_URL
			+ " TEXT," // DATETIME
			+ Fields.USER_NAME + " TEXT," + Fields.USER_COUNTRY + " TEXT,"
			+ Fields.USER_PROFILE_PIC_URL + " TEXT," + Fields.CREATION_TIME
			+ " INTEGER," + Fields.LIKED + " INTEGER," + Fields.COMMENT_COUNT
			+ " INTEGER," + Fields.LIKE_COUNT + " INTEGER ,"

			+ Fields.REPORT_COUNT + " INTEGER," + Fields.PHOTO_ORIANTATION
			+ " INTEGER," + Fields.PRIVACY + " INTEGER," + Fields.CONTEST_RANK
			+ " INTEGER," + Fields.REPORTED + " INTEGER," + Fields.HIDDEN
			+ " INTEGER," + Fields.POST_PHOTO_CAPTION + " TEXT ,"

			+ Fields.REPOSTED + " INTEGER," + Fields.FOLLOWING + " INTEGER,"
			+ Fields.FAVORUITE + " INTEGER," + Fields.REPOSTED_BY + " TEXT,"
			+ Fields.REPOSTER_PROFILE_PIC + " TEXT," + Fields.REPOSTER_ID
			+ " INTEGER," + Fields.REPOSTER_IS_FOLLOWING + " INTEGER ,"

			+ Fields.WALL_POST + " INTEGER," + Fields.LATEST_POST + " INTEGER,"
			+ Fields.TOPTEN + " INTEGER," + Fields.MyPicsPost + " INTEGER"
			+ ")";

	public static WallTable getInstance() {
		if (instance == null)
			instance = new WallTable();
		return instance;
	}

	public WallTable() {
		super(DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()), App.getInstance()
				.getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_WALL);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		return TABLE_WALL;
	}

	public int getCount(){
		String query = "select * from "+TABLE_WALL;
		Cursor cursor = DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()).getReadableDatabase().rawQuery(query, null);
		return cursor.getCount();
	}
	
	@Override
	protected String[] getProjection() {
		return new String[] { Fields.POST_ID, Fields.ORIGINAL_POST_ID,
				Fields.LAST_UPDATE_TIME, Fields.USER_ID,
				Fields.STUDIO_PHOTO_ID, Fields.POST_PHOTO_URL,
				Fields.USER_NAME, Fields.USER_COUNTRY,
				Fields.USER_PROFILE_PIC_URL, Fields.CREATION_TIME,
				Fields.LIKED, Fields.COMMENT_COUNT, Fields.LIKE_COUNT,
				Fields.REPORT_COUNT, Fields.PHOTO_ORIANTATION, Fields.PRIVACY,
				Fields.CONTEST_RANK, Fields.REPORTED, Fields.HIDDEN,
				Fields.POST_PHOTO_CAPTION, Fields.REPOSTED, Fields.FOLLOWING,
				Fields.FAVORUITE, Fields.REPOSTED_BY,
				Fields.REPOSTER_PROFILE_PIC, Fields.REPOSTER_ID,
				Fields.REPOSTER_IS_FOLLOWING, Fields.WALL_POST,
				Fields.LATEST_POST, Fields.TOPTEN, Fields.MyPicsPost };
	}

	@Override
	protected PostViewModel getObjFromCursor(Cursor cursor) {
		PostViewModel post = new PostViewModel();
		post.setPostId(cursor.getInt(cursor.getColumnIndex(Fields.POST_ID)));
		post.setOriginalPostId(cursor.getInt(cursor
				.getColumnIndex(Fields.ORIGINAL_POST_ID)));
		post.setLastUpdateTime(cursor.getInt(cursor
				.getColumnIndex(Fields.LAST_UPDATE_TIME)));
		post.setUserId(cursor.getInt(cursor.getColumnIndex(Fields.USER_ID)));
		post.setStudioPhotoId(cursor.getInt(cursor
				.getColumnIndex(Fields.STUDIO_PHOTO_ID)));
		post.setPostPhotoUrl(cursor.getString(cursor
				.getColumnIndex(Fields.POST_PHOTO_URL)));
		post.setUserName(cursor.getString(cursor
				.getColumnIndex(Fields.USER_NAME)));
		post.setUserCountry(cursor.getString(cursor
				.getColumnIndex(Fields.USER_COUNTRY)));
		post.setUserProfilePicUrl(cursor.getString(cursor
				.getColumnIndex(Fields.USER_PROFILE_PIC_URL)));
		post.setCreationTime(cursor.getInt(cursor
				.getColumnIndex(Fields.CREATION_TIME)));
		post.setLiked(cursor.getInt(cursor.getColumnIndex(Fields.LIKED)) == 1);
		post.setCommentCount(cursor.getInt(cursor
				.getColumnIndex(Fields.COMMENT_COUNT)));
		post.setLikeCount(cursor.getInt(cursor
				.getColumnIndex(Fields.LIKE_COUNT)));
		post.setReportCount(cursor.getInt(cursor
				.getColumnIndex(Fields.REPORT_COUNT)));
		post.setPhotoOrientation(cursor.getInt(cursor
				.getColumnIndex(Fields.PHOTO_ORIANTATION)));
		post.setPrivacy(cursor.getInt(cursor.getColumnIndex(Fields.PRIVACY)));
		post.setContestRank(cursor.getInt(cursor
				.getColumnIndex(Fields.CONTEST_RANK)));
		post.setReported(cursor.getInt(cursor.getColumnIndex(Fields.REPORTED)) == 1);
		post.setFollowing(cursor.getInt(cursor.getColumnIndex(Fields.FOLLOWING)) == 1);
		post.setFavorite(cursor.getInt(cursor.getColumnIndex(Fields.FAVORUITE)) == 1);
		post.setRepostedBy(cursor.getString(cursor
				.getColumnIndex(Fields.REPOSTED_BY)));
		post.setRePosterProfilePic(cursor.getString(cursor
				.getColumnIndex(Fields.REPOSTER_PROFILE_PIC)));
		post.setRePosterId(cursor.getInt(cursor
				.getColumnIndex(Fields.REPOSTER_ID)));
		post.setRePosterIsFollowing(cursor.getInt(cursor
				.getColumnIndex(Fields.REPOSTER_IS_FOLLOWING)) == 1);
		post.setWall(cursor.getInt(cursor.getColumnIndex(Fields.WALL_POST)) == 1);
		post.setLatest(cursor.getInt(cursor.getColumnIndex(Fields.LATEST_POST)) == 1);
		post.setTopTen(cursor.getInt(cursor.getColumnIndex(Fields.TOPTEN)) == 1);
		post.setMyPics(cursor.getInt(cursor.getColumnIndex(Fields.MyPicsPost)) == 1);
		return post;
	}

	@Override
	protected ContentValues getContentValues(PostViewModel entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.POST_ID, entity.getPostId());
		values.put(Fields.ORIGINAL_POST_ID, entity.getOriginalPostId());
		values.put(Fields.LAST_UPDATE_TIME, entity.getLastUpdateTime());
		values.put(Fields.USER_ID, entity.getUserId());
		values.put(Fields.STUDIO_PHOTO_ID, entity.getStudioPhotoId());
		values.put(Fields.POST_PHOTO_URL, entity.getPostPhotoUrl());
		values.put(Fields.USER_NAME, entity.getUserName());
		values.put(Fields.USER_COUNTRY, entity.getUserCountry());
		values.put(Fields.USER_PROFILE_PIC_URL, entity.getUserProfilePicUrl());
		values.put(Fields.CREATION_TIME, entity.getCreationTime());
		values.put(Fields.LIKED, entity.isLiked() ? 1 : 0);
		values.put(Fields.COMMENT_COUNT, entity.getCommentCount());

		values.put(Fields.LIKE_COUNT, entity.getLikeCount());
		values.put(Fields.REPORT_COUNT, entity.getReportCount());
		values.put(Fields.PHOTO_ORIANTATION, entity.getPhotoOrientation());
		values.put(Fields.PRIVACY, entity.getPrivacy());
		values.put(Fields.CONTEST_RANK, entity.getContestRank());
		values.put(Fields.REPORTED, entity.isReported() ? 1 : 0);
		values.put(Fields.HIDDEN, entity.isHidden() ? 1 : 0);
		values.put(Fields.POST_PHOTO_CAPTION, entity.getPostPhotoCaption());
		values.put(Fields.REPOSTED, entity.isRePosted());
		values.put(Fields.FOLLOWING, entity.isFollowing() ? 1 : 0);
		values.put(Fields.FAVORUITE, entity.isFavorite() ? 1 : 0);
		values.put(Fields.REPOSTED_BY, entity.getRepostedBy());

		values.put(Fields.REPOSTER_PROFILE_PIC, entity.getRePosterProfilePic());
		values.put(Fields.REPOSTER_ID, entity.getRePosterId());
		values.put(Fields.REPOSTER_IS_FOLLOWING,
				entity.isRePosterIsFollowing() ? 1 : 0);
		values.put(Fields.WALL_POST, entity.isWall() ? 1 : 0);
		values.put(Fields.LATEST_POST, entity.isLatest() ? 1 : 0);
		values.put(Fields.TOPTEN, entity.isTopTen() ? 1 : 0);
		values.put(Fields.MyPicsPost, entity.isMyPics() ? 1 : 0);
		return values;
	}

	// added by  Mohga
	public PostViewModel getPostById(int postId)
	{

		String whereQuery = Fields.POST_ID + " = "
				+ postId;
		Cursor cursor = getDb().query(getTableName(), getProjection(),
				whereQuery, null, null, null, null);

		PostViewModel post = null;
		
		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			post = getObjFromCursor(cursor);
		}
		cursor.close();
				
		

		return post;
	
	}
}
