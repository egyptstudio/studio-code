package com.tawasol.barcelona.data.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import android.os.Environment;
import android.os.Parcelable;

/**
 * 
 * @author Basyouni
 *
 */
public class ExternalFileSaveDataLayer {
	
	private static final String FILES_CACHE_DIRECTOTY = "MyFiles";
	
	public static void saveObject(String fileName,
			Object object) throws FileNotFoundException, IOException {
		if (!(object instanceof Serializable)
				&& !(object instanceof Parcelable))
			throw new RuntimeException(); // TODO
		if ((!isPresent()) || isReadOnly())
			throw new RuntimeException(); // TODO

		
		File file = new File(getFilesDirectory(), fileName);
		FileOutputStream out = new FileOutputStream(file);
		ObjectOutputStream writer = new ObjectOutputStream(out);
		writer.writeObject(object);
		writer.flush();
		writer.close();
	}

	public static Object getObject(String fileName)
			throws FileNotFoundException, IOException, ClassNotFoundException {
		Object returnObject = null;
		if ((!isPresent() || isReadOnly()))
			throw new RuntimeException(); // TODO

		FileInputStream stream =  new FileInputStream(new File(getFilesDirectory(), fileName));
		ObjectInputStream writer = new ObjectInputStream(stream);

		returnObject = writer.readObject();
		writer.close();

		return returnObject;
	}

	public static String getFilesDirectory() {
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File(sdCard.getAbsolutePath() + File.separator + FILES_CACHE_DIRECTOTY);
		if (!dir.exists())
			dir.mkdir();
		return dir.getAbsolutePath();
	}
	
	private static boolean isPresent() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
	}

	private static boolean isReadOnly() {
		return (Environment.getExternalStorageState()
				.equals(Environment.MEDIA_MOUNTED_READ_ONLY));
	}
}
