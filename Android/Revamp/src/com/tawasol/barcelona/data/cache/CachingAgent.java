package com.tawasol.barcelona.data.cache;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import android.content.Context;
import android.os.Parcelable;

/**
 * A helper caching utility used to save any object to internal storage, it makes more sense to save {@linkplain Object}s that
 * are specific to your application in a storage area that other applications can access [Security wise - Usability wise]
 * @author Morabea
 */
public class CachingAgent {

	/**
	 * Caches a java {@linkplain Object} internally within the application specific space.
	 * @param context
	 * @param fileName The name which the file will be saved on disk.
	 * @param obj The object to be saved, which must be {@linkplain Serializable} or {@linkplain Parcelable}
	 */
	public static void cacheObject(Context context, String fileName,
			Object obj) {
		try {
			InternalFileSaveDataLayer.saveObject(context, fileName, obj);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		}
	}
	
	/**
	 * Restores a java {@linkplain Object} which is saved internally in the application specific space.
	 * @param context
	 * @param fileName
	 * @return The specified object or null if it doesn't exist.
	 */
	public static Object restoreObject(Context context, String fileName) {
		Object restoredObj = null;
		try {
			restoredObj = InternalFileSaveDataLayer.getObject(context, fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		}
		return restoredObj;
	}
	
}
