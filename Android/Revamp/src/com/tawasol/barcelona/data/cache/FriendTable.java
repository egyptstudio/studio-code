package com.tawasol.barcelona.data.cache;
 
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase; 
import android.provider.BaseColumns;

import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.entities.FriendsEntity; 

/**
 * @author Turki
 * 
 */
public class FriendTable extends AbstractTable<FriendsEntity> {

	private static FriendTable sInstance;
	
	// Table name
	private static final String TABLE_FRIEND = "Friend";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String FRI_USER_ID     = BaseColumns._ID;
		private static final String FRI_USER_NAME   = "userName";
		private static final String FRI_PROFILE_PIC = "profilePic";
		private static final String FRI_ONLINE      = "online";
		private static final String FRI_POSTS       = "posts";
		private static final String FRI_PREMIUM     = "premium";
		private static final String FRI_FAVORITE    = "isFavorite";
		private static final String FRI_FOLLOWING   = "isFollowing";
		
		private static final String FRI_FOLLOWERS   = "followers";
		private static final String FRI_BIO         = "bio";
		private static final String FRI_COUNTRY     = "country";
		private static final String FRI_CITY        = "city";
		private static final String FRI_DISTRICT    = "district";
	}
	
	// Create Table Message
	private static final String CREATE_TABLE_FRIEND  = "CREATE TABLE " + TABLE_FRIEND 
			+ "(" 
				+ Fields.FRI_USER_ID       + " INTEGER PRIMARY KEY," 
				+ Fields.FRI_USER_NAME     + " TEXT," 
				+ Fields.FRI_PROFILE_PIC   + " TEXT," 
				+ Fields.FRI_ONLINE        + " INTEGER," 
				+ Fields.FRI_POSTS         + " INTEGER," 
				+ Fields.FRI_PREMIUM       + " INTEGER,"  // DATETIME
				+ Fields.FRI_FAVORITE      + " INTEGER," 
				+ Fields.FRI_FOLLOWERS     + " INTEGER," 
				+ Fields.FRI_BIO           + " TEXT," 
				+ Fields.FRI_COUNTRY       + " TEXT," 
				+ Fields.FRI_CITY          + " TEXT," 
				+ Fields.FRI_DISTRICT      + " TEXT," 
				+ Fields.FRI_FOLLOWING     + " INTEGER" 
			+ ")";
		
	public static FriendTable getInstance() {
		if (sInstance == null)
			sInstance = new FriendTable();
		return sInstance;
	}
	
	public int getCount() {
		String query = "select * from " + TABLE_FRIEND;
		Cursor cursor = DatabaseManager.getInstance(App.getInstance().getApplicationContext()).getReadableDatabase()
				.rawQuery(query, null);
		return cursor.getCount();
	}
	
	private FriendTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void create(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_FRIEND);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_FRIEND;
	}

	@Override
	protected String[] getProjection() {
		// TODO Auto-generated method stub
		return new String[] {Fields.FRI_USER_ID, Fields.FRI_USER_NAME, Fields.FRI_PROFILE_PIC,
				Fields.FRI_ONLINE, Fields.FRI_POSTS, Fields.FRI_PREMIUM, Fields.FRI_FAVORITE, Fields.FRI_FOLLOWING,
				Fields.FRI_FOLLOWERS, Fields.FRI_BIO, Fields.FRI_COUNTRY, Fields.FRI_CITY, Fields.FRI_DISTRICT};
	}

	@Override
	protected ContentValues getContentValues(FriendsEntity entity) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put(Fields.FRI_USER_ID      , entity.getFanID());
	    values.put(Fields.FRI_USER_NAME    , entity.getFanName());
	    values.put(Fields.FRI_PROFILE_PIC  , entity.getFanPicture());
	    values.put(Fields.FRI_ONLINE       , entity.getFanOnline());
	    values.put(Fields.FRI_POSTS        , entity.getFanPicNumber());
	    values.put(Fields.FRI_PREMIUM      , entity.getIsPremium());
	    values.put(Fields.FRI_FAVORITE     , entity.getIsFavorite());
	    values.put(Fields.FRI_FOLLOWERS    , entity.getFollowers());
	    values.put(Fields.FRI_CITY         , entity.getCity());
	    values.put(Fields.FRI_BIO          , entity.getBio());
	    values.put(Fields.FRI_COUNTRY      , entity.getCountry());
	    values.put(Fields.FRI_DISTRICT     , entity.getDistrict());
	    values.put(Fields.FRI_FOLLOWING    , entity.getIsFollowed());
		return values;
	}

	@Override
	protected FriendsEntity getObjFromCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		FriendsEntity friend = new FriendsEntity();
		friend.setFanID(cursor.getInt(cursor.getColumnIndex(Fields.FRI_USER_ID)));
		friend.setFanName(cursor.getString(cursor.getColumnIndex(Fields.FRI_USER_NAME)));
		friend.setFanPicture(cursor.getString(cursor.getColumnIndex(Fields.FRI_PROFILE_PIC)));
		friend.setFanOnline(cursor.getInt(cursor.getColumnIndex(Fields.FRI_ONLINE)));
		friend.setFanPicNumber(cursor.getInt(cursor.getColumnIndex(Fields.FRI_POSTS)));
		friend.setIsPremium(cursor.getInt(cursor.getColumnIndex(Fields.FRI_PREMIUM)));
		friend.setIsFavorite(cursor.getInt(cursor.getColumnIndex(Fields.FRI_FAVORITE)));
		friend.setIsFollowed(cursor.getInt(cursor.getColumnIndex(Fields.FRI_FOLLOWING)));
		friend.setFollowers(cursor.getInt(cursor.getColumnIndex(Fields.FRI_FOLLOWERS)));
		friend.setCity(cursor.getString(cursor.getColumnIndex(Fields.FRI_CITY)));
		friend.setCountry(cursor.getString(cursor.getColumnIndex(Fields.FRI_COUNTRY)));
		friend.setDistrict(cursor.getString(cursor.getColumnIndex(Fields.FRI_DISTRICT)));
		friend.setBio(cursor.getString(cursor.getColumnIndex(Fields.FRI_BIO)));
		return friend;
	}
}










