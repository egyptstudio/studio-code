package com.tawasol.barcelona.data.cache;
 
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase; 
import android.provider.BaseColumns;

import com.tawasol.barcelona.application.App;  
import com.tawasol.barcelona.entities.MessagesUsersEntity;
import com.tawasol.barcelona.entities.SystemMessagesEntity;

/**
 * @author Turki
 * 
 */
public class SystemMessagesTable extends AbstractTable<SystemMessagesEntity> {

	private static SystemMessagesTable sInstance;
	
	// Table name
	private static final String TABLE_SYSTEM_MESSAGES = "SystemMessages";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String SYS_MSG_ID   = BaseColumns._ID;
		private static final String SYS_MSG_TEXT = "messageText";
		private static final String SYS_MSG_TIME = "messageTime"; 
	}
	
	// Create Table Message
	private static final String CREATE_TABLE_SYSTEM_MESSAGES = "CREATE TABLE " + TABLE_SYSTEM_MESSAGES 
			+ "(" 
				+ Fields.SYS_MSG_ID    + " INTEGER PRIMARY KEY," 
				+ Fields.SYS_MSG_TEXT  + " TEXT," 
				+ Fields.SYS_MSG_TIME  + " INTEGER" 
			+ ")";
		
	public static SystemMessagesTable getInstance() {
		if (sInstance == null)
			sInstance = new SystemMessagesTable();
		return sInstance;
	}
	
	private SystemMessagesTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void create(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_SYSTEM_MESSAGES);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_SYSTEM_MESSAGES;
	}

	@Override
	protected String[] getProjection() {
		// TODO Auto-generated method stub
		return new String[] {Fields.SYS_MSG_ID, Fields.SYS_MSG_TEXT, Fields.SYS_MSG_TIME};
	}

	@Override
	protected ContentValues getContentValues(SystemMessagesEntity entity) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put(Fields.SYS_MSG_ID   , entity.getMessageId());
	    values.put(Fields.SYS_MSG_TEXT , entity.getMessageText());
	    values.put(Fields.SYS_MSG_TIME , entity.getMessageTime());
		return values;
	}

	@Override
	protected SystemMessagesEntity getObjFromCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		SystemMessagesEntity systemMsgsEntity = new SystemMessagesEntity();
		systemMsgsEntity.setMessageId(cursor.getInt(cursor.getColumnIndex(Fields.SYS_MSG_ID)));
		systemMsgsEntity.setMessageText(cursor.getString(cursor.getColumnIndex(Fields.SYS_MSG_TEXT)));
		systemMsgsEntity.setMessageTime(cursor.getLong(cursor.getColumnIndex(Fields.SYS_MSG_TIME)));
		return systemMsgsEntity;
	}
}










