package com.tawasol.barcelona.responses;

public class CommentResponse extends BaseResponse {

	int commentId;
	
	public CommentResponse(int status) {
		super(status);
		// TODO Auto-generated constructor stub
	}

	public CommentResponse(int status, int commentId) {
		super(status);
		this.commentId = commentId;
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	
}
