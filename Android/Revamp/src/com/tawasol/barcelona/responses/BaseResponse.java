package com.tawasol.barcelona.responses;

import com.tawasol.barcelona.business.validation.ValidationRule;
import com.tawasol.barcelona.exception.AppException;

public class BaseResponse {

	
	/* Response Status Reference */
	/** Parameter is missing or not entered correctly */
	public static final int STATUS_WEBSERVICE_PARAM_ERROR = -1;
	/** An exception occurred on the server side */
	public static final int STATUS_WEBSERVICE_SERVER_EXCEPTION = -2;
	/** A validation rule has been violated, server should return a list of those rules */
	public static final int STATUS_WEBSERVICE_VALIDATION_RULE_ERROR = -3;
	/** The request is a success but no data is available at server */
	public static final int STATUS_WEBSERVICE_SUCCES_VOID = 1;
	/** The request is a success with data is available at server returned */
	public static final int STATUS_WEBSERVICE_SUCCES_WITH_DATA = 2;
	
	private int status;
	private String data;
	private ValidationRule validationRule;
	
	public BaseResponse(int status) {
		this.status = status;
	}
	
	public BaseResponse(int status, String data, ValidationRule rule) {
		this(status);
		this.data = data;
		this.validationRule = rule;
	}
	
	/**
	 * @return One of the following values, {@linkplain BaseResponse#STATUS_WEBSERVICE_PARAM_ERROR}, {@linkplain BaseResponse#STATUS_WEBSERVICE_SERVER_EXCEPTION}
	 * , {@linkplain BaseResponse#STATUS_WEBSERVICE_SUCCES_VOID}, {@linkplain BaseResponse#STATUS_WEBSERVICE_VALIDATION_RULE_ERROR},
	 * {@linkplain BaseResponse#STATUS_SUCCESS} 
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * @return The violated validation rule, only in case of having status {@linkplain BaseResponse#STATUS_WEBSERVICE_VALIDATION_RULE_ERROR},
	 *  otherwise this will return <code>null</code>.
	 */
	public ValidationRule getValidationRule() {
		return validationRule;
	}
	
	/**
	 * Data is returned only in case of {@linkplain BaseResponse#STATUS_SUCCESS}
	 * @return A JSON formatted string.
	 */
	public String getData() {
		return data;
	}
	
	public static int getExceptionType(int status) {
		int type = AppException.UNKNOWN_EXCEPTION;
		switch (status) {
		case BaseResponse.STATUS_WEBSERVICE_PARAM_ERROR:
			type = AppException.WB_PARAM_EXCEPTION;
			break;
		case BaseResponse.STATUS_WEBSERVICE_SERVER_EXCEPTION:
			type = AppException.WB_SERVER_EXCEPTION;
			break;
		case BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR:
			type = AppException.WB_VALIDATION_RULE_EXCEPTION;
			break;
		}
		return type;
	}
	
}
