package com.tawasol.barcelona.managers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.business.BusinessManager;
import com.tawasol.barcelona.data.cache.HelpScreenTable;
import com.tawasol.barcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.barcelona.data.cache.LanguageTable;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.data.connection.URLs;
import com.tawasol.barcelona.data.helper.DataHelper;
import com.tawasol.barcelona.entities.ChangePassword;
import com.tawasol.barcelona.entities.City;
import com.tawasol.barcelona.entities.ContactUs;
import com.tawasol.barcelona.entities.Country;
import com.tawasol.barcelona.entities.FCBPhoto;
import com.tawasol.barcelona.entities.HelpScreen;
import com.tawasol.barcelona.entities.InvitationEntity;
import com.tawasol.barcelona.entities.LanguageEntity;
import com.tawasol.barcelona.entities.SMSVerificationModel;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.entities.WALLPosts;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnChangePasswordListener;
import com.tawasol.barcelona.listeners.OnContactUsListener;
import com.tawasol.barcelona.listeners.OnCountriesCodesRecieved;
import com.tawasol.barcelona.listeners.OnEntityListReceivedListener;
import com.tawasol.barcelona.listeners.OnFAQListener;
import com.tawasol.barcelona.listeners.OnFCBPhotosResponseListener;
import com.tawasol.barcelona.listeners.OnHelpScreensReceivedListener;
import com.tawasol.barcelona.listeners.OnInfoReceived;
import com.tawasol.barcelona.listeners.OnInviteResponseListener;
import com.tawasol.barcelona.listeners.OnLoginResponseListener;
import com.tawasol.barcelona.listeners.OnPrivacySettingsListener;
import com.tawasol.barcelona.listeners.OnRegisterResponseListener;
import com.tawasol.barcelona.listeners.OnSMSVerificationCodeListener;
import com.tawasol.barcelona.listeners.OnSendVerificationListener;
import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
import com.tawasol.barcelona.listeners.OnSupportedLanguageResponseListener;
import com.tawasol.barcelona.listeners.OnUpdateProfileListener;
import com.tawasol.barcelona.listeners.OnUserCreditReceived;
import com.tawasol.barcelona.listeners.OnVerifyPhoneListener;
import com.tawasol.barcelona.listeners.UiListener;
import com.tawasol.barcelona.pushnotifications.PushNotificationHelper;
import com.tawasol.barcelona.responses.BaseResponse;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.UIUtils;
import com.tawasol.barcelona.utils.ValidatorUtils;

/**
 * 
 * @author Turki
 *
 */
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class UserManager extends BusinessManager<User> {

	/**
	 * in case of normal login (i.e first time you open application and log in)
	 */
	public static final int NORMAL_LOGIN = -4569;
	/**
	 * in case you open screen that require user to be logged in and when login
	 * process finish you want it to continue to called screen
	 */
	public static final int LOGIN_WITH_INTENT = -48451;
	/**
	 * in case user need to make action in same screen which require him to be
	 * logged in
	 */
	public static final int LOGIN_FOR_RESULT = -54185;

	private static final String USER_CACHE_KEY = "com.barcelona.tawasol.entities.User";
	public static final String LOGOUT_USER_ID_KEY = "com.tawasol.barcelona.fragments.logout.userID";

	public static final String LOGIN_TYPE = "com.tawasol.barcelona.fragments.login.type";
	public static final int TYPE_EMAIL = 0;
	public static final int TYPE_PHONE = 1;

	private static UserManager instance;
	private User mUser;

	public static UserManager getInstance() {
		if (instance == null)
			instance = new UserManager();
		return instance;
	}

	private UserManager() {
		super(User.class);
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* FCBPhoto Level One Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Calling performGetFCBPhotos to get FCB photo list. **/
	public void callFCBPhoto(String language, boolean isFromEdit) {
		performGetFCBPhotos(language, isFromEdit);
	}

	/**
	 * Calling getFCBPhotos web service, then notify all with response list and
	 * exception if happened.
	 **/
	private void performGetFCBPhotos(final String language,
			final boolean isFromEdit) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					final List<FCBPhoto> list = getFCBPhotos(language);
					if (!isFromEdit)
						notifyEntityListReceviedSuccess(list,
								OnFCBPhotosResponseListener.class);
					else
						notifyEntityListReceviedSuccess(list,
								OnEntityListReceivedListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					if (!isFromEdit)
						notifyRetrievalException(
								AppException.getAppException(e),
								OnFCBPhotosResponseListener.class);
					else
						notifyRetrievalException(
								AppException.getAppException(e),
								OnEntityListReceivedListener.class);
				}
			}
		});
	}

	/** Build request parameter, the deserialize response to FCB Photo list. **/
	private List<FCBPhoto> getFCBPhotos(String language) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, language);
		List<FCBPhoto> list = getObjectList(URLs.Methods.GET_FCB_PHOTOS,
				params, FCBPhoto.class);
		return list;
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* Registration Level One Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Calling performRegister to register the user. **/
	public void register(BaseActivity context, User user, String language,
			boolean uploadProfilePictureFile) {
		performRegister(context, user, language, uploadProfilePictureFile);
	}

	/**
	 * Calling registerUser web service, then notify all with response user
	 * object and exception if happened.
	 **/
	private void performRegister(final BaseActivity context, final User user,
			final String language, final boolean uploadProfilePictureFile) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					/** Added By Morabea */
					if (!PushNotificationHelper.isRegisteredWithServer(context)) {
						PushNotificationHelper mHelper = new PushNotificationHelper();
						mHelper.register(context);
					}
					registerUser(context, user, language,
							uploadProfilePictureFile);
					PushNotificationHelper
							.notifyRegistrationWithServerSuccessful(context);
					onRegisterResponse();
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnRegisterResponseListener.class);
				}
			}
		});
	}

	/**
	 * Build request parameter, then deserialize response to User Object and
	 * Cache this User in App.
	 **/
	private void registerUser(Context context, User user, String language,
			boolean uploadProfilePictureFile) throws AppException {

		BaseResponse response = null;

		user.validateObject(App.getInstance().getApplicationContext());

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, language);
		params.put(Params.User.REGISTRATION_USER_NAME, user.getFullName());
		params.put(Params.User.EMAIL, user.getEmail());
		params.put(Params.User.PASSWORD, user.getPassword());
		params.put(Params.User.DEVICE_TYPE, User.ANDROID_DEVICE_TYPE);
		params.put(Params.User.DEVICE_TOKEN,
				PushNotificationHelper.getStoredRegId(context));
		System.out.println("FCB Name: " + user.getProfilePic());
		if (!uploadProfilePictureFile) {
			if (!ValidatorUtils.isRequired(user.getProfilePic())) {
				params.put(Params.User.PHOTO_URL, user.getProfilePic());
				response = submit(params, URLs.Methods.REGISTER_METHOD);
			}
		} else {
			params.put(Params.User.PHOTO_URL, "");
			response = submit(params, new File(user.getProfilePic()),
					URLs.Methods.REGISTER_METHOD);
		}

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			User userObject = DataHelper.deserialize(response.getData(),
					User.class);
			cacheUser(userObject);
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));

		}
	}

	/** Notify all with success response **/
	private void onRegisterResponse() {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnSuccessVoidListener) {
						((OnSuccessVoidListener) listener).onSuccess();
					}
				}
			}
		});
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* Cache User Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Cache User in file. **/
	public void cacheUser(final User user) {
		mUser = user;
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(App.getInstance()
							.getApplicationContext(), USER_CACHE_KEY, user);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/** Get cached User in file. **/
	public User getCurrentUser() {
		if (mUser == null) {
			try {
				mUser = (User) InternalFileSaveDataLayer.getObject(App
						.getInstance().getApplicationContext(), USER_CACHE_KEY);
			} catch (Exception e) {
				mUser = new User();
				e.printStackTrace();
			}
		}
		return mUser;
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* Invitation Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Calling performInvitation to invite friends download the app. **/
	public void callInviteFriend(InvitationEntity invitationObject,
			String language) {
		performInvitation(invitationObject, language);
	}

	/**
	 * Calling sendInviteSMS web service, then notify all with success response
	 * and exception if happened.
	 **/
	private void performInvitation(final InvitationEntity invitationObject,
			final String language) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					inviteFriends(invitationObject, language);
					onInvitationResponse();
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnInviteResponseListener.class);
				}
			}
		});
	}

	/** Build request parameter. **/
	private void inviteFriends(InvitationEntity invitationObject,
			String language) throws AppException {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, language);
		params.put(Params.SMS_INVITATION.USER_ID, invitationObject.getUserId());
		params.put(Params.SMS_INVITATION.SMS_NUMBER,
				invitationObject.getPhoneNumbers());
		params.put(Params.SMS_INVITATION.SMS_TEXT,
				invitationObject.getInvitationText());

		BaseResponse response = submit(params, URLs.Methods.SEND_SMS_INVITATION);

		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));

		}
	}

	/** Notify all with success response **/
	private void onInvitationResponse() {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnSuccessVoidListener) {
						((OnSuccessVoidListener) listener).onSuccess();
					}
				}
			}
		});
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* Settings Contact-Us Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Calling performContactUs to send . **/
	public void callContactUs(ContactUs contactEntity) {
		performContactUs(contactEntity);
	}

	/**
	 * Calling contactUs web service, then notify all with response and
	 * exception if happened.
	 **/
	private void performContactUs(final ContactUs contactEntity) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					contactUS(contactEntity);
					notifyVoidSuccess(OnContactUsListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnContactUsListener.class);
				}
			}
		});
	}

	/** Build request parameter **/
	private void contactUS(ContactUs contactEntity) throws AppException {
		BaseResponse response = null;
		contactEntity.validateObject(App.getInstance().getApplicationContext());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Settings.EMAIL, contactEntity.getEmail());
		params.put(Params.Settings.MESSAGE_TITLE,
				contactEntity.getMessageTitle());
		params.put(Params.Settings.MESSAGE_TEXT, contactEntity.getMessageText());

		response = submit(params, URLs.Methods.CONTACT_US);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {

		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* Settings Change Password Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Calling performContactUs to send . **/
	public void callChangePassword(ChangePassword changePasswordEntity) {
		performchangePassword(changePasswordEntity);
	}

	/**
	 * Calling contactUs web service, then notify all with response and
	 * exception if happened.
	 **/
	private void performchangePassword(final ChangePassword changePasswordEntity) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					ChangePassword(changePasswordEntity);
					notifyVoidSuccess(OnChangePasswordListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnChangePasswordListener.class);
				}
			}
		});
	}

	/** Build request parameter **/
	private void ChangePassword(ChangePassword changePasswordEntity)
			throws AppException {
		BaseResponse response = null;
		changePasswordEntity.validateObject(App.getInstance()
				.getApplicationContext());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Settings.USER_ID, getCurrentUserId());
		params.put(Params.Settings.CURRENT_PASSWORD,
				changePasswordEntity.getCurrentPassword());
		params.put(Params.Settings.OLD_PASSWORD,
				changePasswordEntity.getOldPassword());

		response = submit(params, URLs.Methods.CHANGE_PASSWORD);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {

		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* Change Privacy Settings Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Calling performChangePrivacySettings to send updated user. **/
	public void callChangePrivacySettings(User userEntity) {
		performChangePrivacySettings(userEntity);
	}

	/**
	 * Calling updateUserProfile web service, then notify all with response and
	 * exception if happened.
	 **/
	private void performChangePrivacySettings(final User userEntity) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					ChangePrivacySettings(userEntity);
					notifyVoidSuccess(OnPrivacySettingsListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnPrivacySettingsListener.class);
				}
			}
		});
	}

	/** Build request parameter **/
	private void ChangePrivacySettings(User userEntity) throws AppException {
		BaseResponse response = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.USER_MODEL,
				DataHelper.serialize(userEntity, User.class));
		params.put(Params.User.PHOTO_URL, userEntity.getProfilePic());

		response = submit(params, URLs.Methods.UPDATE_PROFILE);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			User returnedUser = DataHelper.deserialize(response.getData(),
					User.class);
			cacheUser(returnedUser);
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* Languages Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	private List<LanguageEntity> loadLanguagesFromDatabase() {
		return LanguageTable.getInstance().getAll();
	}

	private void saveLanguagesToDatabase(List<LanguageEntity> languages) {
		LanguageTable.getInstance().insert(languages);
	}

	/** Calling performGetFCBPhotos to get FCB photo list. **/
	public void callSupportedLanguage() {
		performGetSupportedLanguage();
	}

	/**
	 * Calling getFCBPhotos web service, then notify all with response list and
	 * exception if happened.
	 **/
	private void performGetSupportedLanguage() {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					final List<LanguageEntity> list = getSupportedLanguage();
					if (list != null && list.size() > 0)
						saveLanguagesToDatabase(list);
					notifyEntityListReceviedSuccess(list,
							OnSupportedLanguageResponseListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnSupportedLanguageResponseListener.class);
				}
			}
		});
	}

	/** Build request parameter, the deserialize response to FCB Photo list. **/
	private List<LanguageEntity> getSupportedLanguage() throws AppException {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		List<LanguageEntity> list = getObjectList(
				URLs.Methods.GET_SUPPORTED_LANGUAGE, params,
				LanguageEntity.class);
		return list;
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* Verify Phone Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Calling performPhoneVerify to verify phone **/
	public void callVerifyPhone() {
		performPhoneVerify();
	}

	/**
	 * Calling performPhoneVerify web service, then notify all with response and
	 * exception if happened.
	 **/
	private void performPhoneVerify() {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					verifyPhone();
					notifyEntityReceviedSuccess(User.class,
							OnVerifyPhoneListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnVerifyPhoneListener.class);
				}
			}
		});
	}

	/** Build request parameter **/
	private void verifyPhone() throws AppException {
		BaseResponse response = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.USER_ID, UserManager.getInstance()
				.getCurrentUserId());
		params.put(Params.User.PHONE, UserManager.getInstance()
				.getCurrentUser().getPhone());

		response = submit(params, URLs.Methods.VERIFY_PHONE);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {

		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	/*
	 * ==========================================================================
	 * ====================
	 */
	/* SMS Verification Methods */
	/*
	 * ==========================================================================
	 * ====================
	 */

	/** Calling performPhoneVerify to verify phone **/
	public void callSMSVerification(SMSVerificationModel smsVerificationEntity) {
		performSMSVerification(smsVerificationEntity);
	}

	/**
	 * Calling performPhoneVerify web service, then notify all with response and
	 * exception if happened.
	 **/
	private void performSMSVerification(
			final SMSVerificationModel smsVerificationEntity) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					smsVerification(smsVerificationEntity);
					notifyVoidSuccess(OnSMSVerificationCodeListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnSMSVerificationCodeListener.class);
				}
			}
		});
	}

	/** Build request parameter **/
	private void smsVerification(SMSVerificationModel smsVerificationEntity)
			throws AppException {
		BaseResponse response = null;
		smsVerificationEntity.validateObject(App.getInstance()
				.getApplicationContext());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.USER_ID, UserManager.getInstance()
				.getCurrentUserId());
		params.put(Params.SMSVerification.SMS_VER_CODE,
				smsVerificationEntity.getCode());

		response = submit(params, URLs.Methods.SMS_Verification);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {

		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	/* ============================================================================================== */
	/*                                 Info FAQ Methods                                       */
	/* ============================================================================================== */
	
	/** Calling performPhoneVerify to verify phone **/
	public void callFAQ(String name) {
		performFAQ(name);
	}

	/** Calling performPhoneVerify web service, then notify all with response and exception if happened. **/
	private void performFAQ(final String name) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					String FAQLink = sendFAQ(name);
					notifyEntityReceviedSuccess(FAQLink, OnFAQListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e), OnFAQListener.class);
				}
			}
		});
	}

	/** Build request parameter **/
	private String sendFAQ(String name) throws AppException {
		BaseResponse response = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG   , LanguageUtils.getInstance().getDeviceLanguage());
//		params.put(Params.User.USER_ID  , UserManager.getInstance().getCurrentUserId());
		params.put(Params.Common.NAME   , name);
		response = submit(params, URLs.Methods.FAQ_LINK);
		
		String FAQLink = " ";
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			try {
				JSONArray array = new JSONArray(response.getData());
				FAQLink = array.getJSONObject(0).getString("link");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response.getStatus()));
		}
		return FAQLink;
	}
	
	/**************************************************************************************/
	/**************************************************************************************/

	/**
	 * @author Mohga
	 */
	/*** Log In Part ***/
	public void login(BaseActivity context, String email, String password,
			int LoginState) {

		try {
			performLogin(context, email, password, LoginState);
		} catch (AppException e) {
			e.printStackTrace();
			notifyRetrievalException(AppException.getAppException(e),
					UiListener.class);
		}
	}

	/* to set the parameters of normal login method */
	private Map<String, Object> prepareLoginParamaters(Context context,
			String email, String password) {
		Map<String, Object> params = new HashMap<String, Object>(3);
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.EMAIL, email);
		params.put(Params.User.PASSWORD, password);
		params.put(Params.User.DEVICE_TYPE, User.ANDROID_DEVICE_TYPE);
		params.put(Params.User.DEVICE_TOKEN,
				PushNotificationHelper.getStoredRegId(context));
		return params;
	}

	/* get the logged in User object or notify exception */
	private void performLogin(final BaseActivity context, final String email,
			final String password, final int LoginState) throws AppException {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					/** Added By Morabea */
					if (!PushNotificationHelper.isRegisteredWithServer(context)) {
						PushNotificationHelper mHelper = new PushNotificationHelper();
						mHelper.register(context);
					}
					User loggedinUser = getObject(
							prepareLoginParamaters(context, email, password),
							URLs.Methods.LOGIN_METHOD, User.class);
					PushNotificationHelper
							.notifyRegistrationWithServerSuccessful(context);
					onLoginResponse(loggedinUser, LoginState);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							UiListener.class);
				}
			}
		});
	}

	private void onLoginResponse(final User user, final int LoginState) {
		NotificationsManager.getInstance().getNotificationsFromServer();
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener temp : getListeners())
					if (temp instanceof OnLoginResponseListener)
						((OnLoginResponseListener) temp).onLoginSuccess(user,
								LoginState);
			}
		});
	}

	/*** Log In With SocialMedia Part ***/
	public void loginWithSocialMedia(BaseActivity context, User user,
			int LoginState) {
		try {
			performLoginSocial(context, LoginState, user);
		} catch (AppException e) {
			e.printStackTrace();
			notifyRetrievalException(AppException.getAppException(e),
					UiListener.class);
		}

	}

	private void performLoginSocial(final BaseActivity context,
			final int LoginState, final User user) throws AppException {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					/** Added By Morabea */
					if (!PushNotificationHelper.isRegisteredWithServer(context)) {
						PushNotificationHelper mHelper = new PushNotificationHelper();
						mHelper.register(context);
					}

					executeLoginSocial(
							prepareLoginSocialParamaters(context, user),
							LoginState, user);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							UiListener.class);
				}
			}
		});
	}

	/* to set the parameters of Social media login method */
	private Map<String, Object> prepareLoginSocialParamaters(Context context,
			User user) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.SOCIALMEDIAID, user.getSocialMediaID());
		params.put(Params.User.EMAIL, user.getEmail());
		params.put(Params.User.USER_NAME, user.getFullName());
		params.put(Params.User.PHONE, user.getPhone());
		if (ValidatorUtils.isRequired(user.getCountryName()))
			params.put(Params.User.LOGIN_COUNTRY, "");
		else
			params.put(Params.User.LOGIN_COUNTRY, user.getCountryName());
		if (ValidatorUtils.isRequired(user.getCityName()))
			params.put(Params.User.LOGIN_CITY, "");
		else
			params.put(Params.User.LOGIN_CITY, user.getCityName());
		params.put(Params.User.DISTRICT, user.getDistrict());
		if (user.getDateOfBirth() != 0)
			params.put(Params.User.LOGIN_BIRTHDAY, user.getDateOfBirth());
		else
			params.put(Params.User.LOGIN_BIRTHDAY, "");
		params.put(Params.User.GENDER, user.getGender());
		params.put(Params.User.LOGIN_PROFILE_PIC_URL, user.getProfilePic());
		params.put(Params.User.DEVICE_TYPE, User.ANDROID_DEVICE_TYPE);
		params.put(Params.User.DEVICE_TOKEN,
				PushNotificationHelper.getStoredRegId(context));

		return params;
	}

	/* get the User Id field from WB or notify exception */
	private void executeLoginSocial(Map<String, Object> params, int LoginState,
			final User user) throws AppException {

		BaseResponse response = submit(params, URLs.Methods.LOGIN_SOCIAL_MEDIA);

		// if object returned then , deserialize the object and handle response
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			User loggedInUser = DataHelper.deserialize(response.getData(),
					User.class);
			onLoginResponse(loggedInUser, LoginState);

		} else if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			// handle listeners
			App.getInstance().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					for (UiListener temp : getListeners())
						if (temp instanceof OnLoginResponseListener)
							((OnLoginResponseListener) temp)
									.onLoginWithSocialMediaNavigation(user);
				}
			});
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}

	}

	/*** Send Verification Part ***/
	public void sendVerifcationCode(/* String lang, */String email) {
		// listeners.add(listener);
		performSendVerificationCode(/* lang, */email);
	}

	private void performSendVerificationCode(/* final String lang, */
	final String email) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					executeSendVerificationAction(/* lang, */email);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnSendVerificationListener.class);
				}
			}
		});
	}

	/* get the User Id field from WB or notify exception */
	private void executeSendVerificationAction(/* String lang, */String email)
			throws AppException {
		Map<String, Object> params = new HashMap<String, Object>(2);
		params.put(Params.User.EMAIL, email);
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		BaseResponse response = submit(params,
				URLs.Methods.SEND_VERIFICATION_METHOD);

		String UserID = "-1";
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			try {
				UserID = new JSONObject(response.getData()).getString("userId");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			NotifySendVerificationResponse(UserID);
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}

	}

	private void NotifySendVerificationResponse(final String userId) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener temp : getListeners())
					if (temp instanceof OnSendVerificationListener)
						((OnSendVerificationListener) temp).onSuccess(userId);
			}
		});
	}

	/*** Forget Password Part ***/
	public void changePassword(/* final String lang, */final String userId,
			final String verificationCode, final String newPassword) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					performChangePassowrd(prepareChangePAsswordParamaters(/* lang, */
					userId, verificationCode, newPassword));
					// handle listeners
					App.getInstance().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							for (UiListener temp : getListeners())
								if (temp instanceof OnSuccessVoidListener)
									((OnSuccessVoidListener) temp).onSuccess();
						}
					});
				} catch (AppException e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnSuccessVoidListener.class);
				}

			}
		});

	}

	/* set the parameters of change password method */
	private Map<String, Object> prepareChangePAsswordParamaters(/* String lang, */
	String userId, String verificationCode, String newPassword) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.USER_ID, userId);
		params.put(Params.User.VERIFICATION_CODE, verificationCode);
		params.put(Params.User.NEW_PASSOWRD, newPassword);
		return params;
	}

	/* call the WB and handle the response or notify exception */
	private void performChangePassowrd(Map<String, Object> params)
			throws AppException {
		BaseResponse response = submit(params,
				URLs.Methods.CHANGE_PASSWORD_METHOD);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}

	}

	/** Update Profile Part */
	public void udateProfile(final User user, final boolean isImageFile,
			final boolean isFromEdit) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					executeupdateProfile(user, isImageFile, isFromEdit);
				} catch (AppException e) {
					notifyRetrievalException(e, OnUpdateProfileListener.class);
					e.printStackTrace();
				}

			}
		});
	}

	/* update the user profile and cache the returned user object */
	private void executeupdateProfile(User user, boolean isImageFile,
			boolean isFromEdit) throws AppException {
		String path = "";
		if (isImageFile) {
			path = user.getProfilePic();
			user.setProfilePic("");

		}
		BaseResponse response;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.USER_MODEL,
				DataHelper.serialize(user, User.class));
		if (isFromEdit) {
			if (isImageFile) {
				params.put(Params.User.PHOTO_URL, "");
				response = submit(params, new File(path),
						URLs.Methods.UPDATE_PROFILE);
			} else {
				params.put(Params.User.PHOTO_URL, user.getProfilePic());
				response = submit(params, URLs.Methods.UPDATE_PROFILE);
			}

		} else
			response = submit(params, URLs.Methods.UPDATE_PROFILE);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			User returnedUser = DataHelper.deserialize(response.getData(),
					User.class);
			notifyUpdateProfile(returnedUser);
			cacheUser(returnedUser);
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));

		}
	}

	// notify the share listener
	private void notifyUpdateProfile(final User user) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnUpdateProfileListener) {
						((OnUpdateProfileListener) listener).onSuccess(user);
					}
				}
			}
		});
	}

	/** Get User Credit Part */
	public void getUserCredit() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					executeGetUserCredit();
				} catch (AppException e1) {

					e1.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e1),
							UiListener.class);
				}
			}

		});

	}

	/* get the user credit and notify listener */
	private void executeGetUserCredit() throws AppException {
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(Params.Common.LANG, LanguageUtils.getInstance()
					.getDeviceLanguage());
			params.put(Params.User.USER_ID, UserManager.getInstance()
					.getCurrentUserId());
			BaseResponse response = submit(params, URLs.Methods.GET_USER_CREDIT);

			String userCredit = "-1";
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
				try {
					userCredit = new JSONObject(response.getData())
							.getString("credit");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				NotifyUserCreditResponse(userCredit);
			} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
					throw new AppException(
							response.getValidationRule().errorMessage);
				else
					throw new AppException(
							BaseResponse.getExceptionType(response.getStatus()));
			}
		}

	}

	private void NotifyUserCreditResponse(final String userCdedit) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener temp : getListeners())
					if (temp instanceof OnUserCreditReceived)
						((OnUserCreditReceived) temp).onSuccess(userCdedit);
			}
		});
	}

	/** get Countries List Part **/
	public void getCountries() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(Params.Common.LANG, LanguageUtils.getInstance()
							.getDeviceLanguage());

					List<Country> countries = getObjectList(
							URLs.Methods.GET_COUNTRIES, params, Country.class);

					Collections.sort(countries, new Comparator<Country>() {

						@Override
						public int compare(Country lhs, Country rhs) {
							return lhs.getCountryName().compareTo(
									rhs.getCountryName());
						}
					});

					SortedSet<String> countriesCodes = new TreeSet<String>();

					for (int x = 0; x < countries.size(); x++) {
						countriesCodes.add(countries.get(x).getPhoneCode());
					}

					System.out.println(countriesCodes);
					// List<Integer> countriesCodeInt = new
					// ArrayList<Integer>();
					// List<String> countriesCodes = new ArrayList<String>();
					//
					// for (int x = 0; x < countries.size(); x++) {
					// countriesCodeInt.add(Integer.parseInt(countries.get(x).getPhoneCode().split("\\+")[1]));
					// }
					// Collections.sort(countriesCodeInt);
					//
					// for (int x = 0; x < countriesCodeInt.size(); x++) {
					// countriesCodes.add("+" + countriesCodeInt.get(x));
					// }

					notifyCodesRecieved(countriesCodes);
					notifyEntityListReceviedSuccess(countries,
							OnEntityListReceivedListener.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnEntityListReceivedListener.class);
					e.printStackTrace();
				}
			}
		});
	}

	protected void notifyCodesRecieved(final Set<String> countriesCodes) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnCountriesCodesRecieved) {
						((OnCountriesCodesRecieved) listener)
								.onPhonesCodesRecieved(countriesCodes);
					}
				}
			}
		});
	}

	/** get Cities List Part **/
	public void getCities(final int countryId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(Params.Common.LANG, LanguageUtils.getInstance()
							.getDeviceLanguage());
					params.put(Params.User.COUNTRY_ID, countryId);
					List<City> cities = getObjectList(URLs.Methods.GET_CITIES,
							params, City.class);
					notifyEntityListReceviedSuccess(cities,
							OnEntityListReceivedListener.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnEntityListReceivedListener.class);
					e.printStackTrace();
				}
			}
		});
	}

	/** get my pictures */
	public void getMyPics(final int PostsNum) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(Params.Common.LANG, LanguageUtils.getInstance()
							.getDeviceLanguage());
					params.put(Params.User.USER_ID, getCurrentUser()
							.getUserId());
					params.put(Params.Wall.NUM_OF_POSTS, PostsNum);
					WALLPosts myPics = getObject(params,
							URLs.Methods.GET_MY_POSTS, WALLPosts.class);
					notifyEntityListReceviedSuccess(myPics.getPosts(),
							OnEntityListReceivedListener.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnEntityListReceivedListener.class);
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * get Registration Level
	 * */
	public boolean canJoinContest() {
		User user = new User();
		user = getCurrentUser();

		// Check Phone, Country, City, District, DOB
		if ((user.getPhone() != null && !user.getPhone().isEmpty())
				&& (user.getCountryName() != null && !user.getCountryName()
						.isEmpty())
				&& (user.getCityName() != null && !user.getCityName().isEmpty())
				&& (user.getDistrict() != null && !user.getDistrict().isEmpty())
				&& (user.getGender() != 0) && (user.getDateOfBirth() != 0)) {

			// Check if email and phone are verified
			// if (user.isEmailVerified() && user.isPhoneVerified())
			return true;
			// else
			// return false;
		} else
			return false;
	}

	/**
	 * calculate profile completion percentage according to the filled fields of
	 * the logged in user
	 * */
	public int calculateProfilePercentage() {
		int fieldsCount = 0;
		if (getCurrentUserId() != 0) {
			User user = getCurrentUser();
			fieldsCount = (!user.getFullName().isEmpty() && user.getFullName() != null) ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = user.getCountryId() != 0 ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = user.getCityId() != 0 ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = (!user.getDistrict().isEmpty() && user.getDistrict() != null) ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = (!user.getPhone().isEmpty() && user.getPhone() != null) ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = (!user.getEmail().isEmpty() && user.getEmail() != null) ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = user.getDateOfBirth() != 0 ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = user.getGender() != 0 ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = (user.getProfilePic() != null && !user
					.getProfilePic().isEmpty()) ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = (user.getStatus() != null && !user.getStatus()
					.isEmpty()) ? fieldsCount += 1 : fieldsCount;
			fieldsCount = (user.getAboutMe() != null && !user.getAboutMe()
					.isEmpty()) ? fieldsCount += 1 : fieldsCount;
			fieldsCount = user.getRelationship() != 0 ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = user.getJobRole() != 0 ? fieldsCount += 1
					: fieldsCount;
			/*
			 * fieldsCount = (user.getReligion() != null && !user.getReligion()
			 * .isEmpty()) ? fieldsCount += 1 : fieldsCount;
			 */
			fieldsCount = user.getEducation() != 0 ? fieldsCount += 1
					: fieldsCount;
			fieldsCount = user.getJob() != 0 ? fieldsCount += 1 : fieldsCount;
			fieldsCount = (user.getCompany() != null && !user.getCompany()
					.isEmpty()) ? fieldsCount += 1 : fieldsCount;
			fieldsCount = user.getIncome() != 0 ? fieldsCount += 1
					: fieldsCount;
		}
		return fieldsCount;
	}

	/** Get Help Screens Part */
	public void getHelpScreens() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put(Params.Common.LANG, LanguageUtils.getInstance()
						.getDeviceLanguage());
				try {

					ArrayList<HelpScreen> helpScreens = new ArrayList<HelpScreen>();
					ArrayList<HelpScreen> cachedScreens = (ArrayList<HelpScreen>) getCachedHelpScreens();

					if (cachedScreens != null && cachedScreens.size() > 0)
						helpScreens.addAll(cachedScreens);
					else {
						helpScreens = (ArrayList<HelpScreen>) getObjectList(
								URLs.Methods.GET_HELP_SCREENS, params,
								HelpScreen.class);
						cacheHelpScreens(helpScreens);
					}
					notifyEntityListReceviedSuccess(helpScreens,
							OnHelpScreensReceivedListener.class);
				}

				catch (AppException e) {
					notifyRetrievalException(e,
							OnHelpScreensReceivedListener.class);
				}

			}
		});
	}

	/** get cached help screens method */
	private List<HelpScreen> getCachedHelpScreens() {
		return HelpScreenTable.getInstance().getLocalizedHelpScreens(
				LanguageUtils.getInstance().getDeviceLanguage());
	}

	/** Cache help screens method */
	private void cacheHelpScreens(ArrayList<HelpScreen> helpScreens) {
		HelpScreenTable.getInstance().insert(helpScreens);
	}

	private ArrayList<HelpScreen> fillHelpScreenData() {
		ArrayList<HelpScreen> helpScreens = new ArrayList<HelpScreen>();
		helpScreens
				.add(new HelpScreen(
						1,
						"Step 1",
						"http://barca.s3.amazonaws.com/third_party/uploads/users/954/1421591194.jpg",
						"http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg",
						"bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n ",
						"en"));
		helpScreens
				.add(new HelpScreen(
						1,
						"Step 1",
						"http://barca.s3.amazonaws.com/third_party/uploads/users/954/1421591132.jpg",
						"https://igcdn-photos-g-a.akamaihd.net//hphotos-ak-xpf1//10570155_806254719395518_1563946762_a.jpg",
						"bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \nbla2 bla2 bla2 \n bla2 bla2 bla2 \nbla2 bla2 bla2 \n",
						"en"));
		helpScreens
				.add(new HelpScreen(
						1,
						"Step 1",
						"http://barca.s3.amazonaws.com/third_party/uploads/users/954/1421591194.jpg",
						"http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg",
						"bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n ",
						"en"));
		helpScreens
				.add(new HelpScreen(
						1,
						"Step 1",
						"http://barca.s3.amazonaws.com/third_party/uploads/users/954/1421591132.jpg",
						"https://igcdn-photos-g-a.akamaihd.net//hphotos-ak-xpf1//10570155_806254719395518_1563946762_a.jpg",
						"bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \nbla2 bla2 bla2 \n bla2 bla2 bla2 \nbla2 bla2 bla2 \n",
						"en"));
		helpScreens
				.add(new HelpScreen(
						1,
						"Step 1",
						"http://barca.s3.amazonaws.com/third_party/uploads/users/954/1421591194.jpg",
						"http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg",
						"bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n ",
						"en"));
		helpScreens
				.add(new HelpScreen(
						1,
						"Step 1",
						"http://barca.s3.amazonaws.com/third_party/uploads/users/954/1421591132.jpg",
						"https://igcdn-photos-g-a.akamaihd.net//hphotos-ak-xpf1//10570155_806254719395518_1563946762_a.jpg",
						"bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \nbla2 bla2 bla2 \n bla2 bla2 bla2 \nbla2 bla2 bla2 \n",
						"en"));
		helpScreens
				.add(new HelpScreen(
						1,
						"Step 1",
						"http://barca.s3.amazonaws.com/third_party/uploads/users/954/1421591194.jpg",
						"http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg",
						"bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n bla1 bla1 bla1 \n ",
						"en"));
		helpScreens
				.add(new HelpScreen(
						1,
						"Step 1",
						"http://barca.s3.amazonaws.com/third_party/uploads/users/954/1421591132.jpg",
						"https://igcdn-photos-g-a.akamaihd.net//hphotos-ak-xpf1//10570155_806254719395518_1563946762_a.jpg",
						"bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \n bla2 bla2 bla2 \nbla2 bla2 bla2 \n bla2 bla2 bla2 \nbla2 bla2 bla2 \n",
						"en"));
		return helpScreens;

	}

	public int getCurrentUserId() {
		if (getCurrentUser() == null || getCurrentUser() == null)
			return 0;
		return getCurrentUser().getUserId();
	}

	/**** Info Part ****/

	public void getInfo(final String pageName) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {

				Map<String, Object> params = new HashMap<String, Object>();
				params.put(Params.Common.LANG, LanguageUtils.getInstance()
						.getLanguage());
				params.put(Params.Info.PAGE_NAME, pageName);

				BaseResponse response = null;
				try {
					response = submit(params, URLs.Methods.GET_INFO);

					String infoLink = "";
					if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
						try {
							infoLink = new JSONObject(response.getData())
									.getString("link");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						NotifyInfoResponse(infoLink);
					} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
						if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
							throw new AppException(
									response.getValidationRule().errorMessage);
						else
							throw new AppException(BaseResponse
									.getExceptionType(response.getStatus()));
					}
				} catch (AppException e1) {
					e1.printStackTrace();
					notifyRetrievalException(e1, OnInfoReceived.class);
				}
			}
		});
	}

	private void NotifyInfoResponse(final String infoLink) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener temp : getListeners())
					if (temp instanceof OnInfoReceived)
						((OnInfoReceived) temp).onSuccess(infoLink);
			}
		});
	}

	// ----------------verification part-----------------

	public void verify(final String email, final int userId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put(Params.Common.LANG, LanguageUtils.getInstance()
						.getDeviceLanguage());
				params.put(Params.User.USER_ID, userId);
				params.put(Params.User.EMAIL, email);
				try {
					verifyFromServer(params);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	protected void verifyFromServer(Map<String, Object> params)
			throws AppException {

		BaseResponse response = submit(params, URLs.Methods.VERIFY_EMAIL);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					UIUtils.showToast(
							App.getInstance().getApplicationContext(),
							App.getInstance().getApplicationContext().getString(R.string.verification_sent_to_email));
				}
			});
		else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));

		}
	}

	/**
	 * method that gets user object passing user id to it . runs every 30
	 * minutes to update the user periodically in case of logged in
	 * **/
	public void getUser() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(Params.Common.LANG, LanguageUtils.getInstance()
							.getDeviceLanguage());
					params.put(Params.User.USER_ID, getCurrentUser()
							.getUserId());

					User user = getObject(params, URLs.Methods.GET_USER,
							User.class);
					cacheUser(user);
				} catch (AppException e) {
					e.printStackTrace();

				}
			}
		});
	}

	public void updateUserCriedt(final int creditValue) {

		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					updateUserCreidtInServer(creditValue);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	private void updateUserCreidtInServer(int creditValue) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.USER_ID, getCurrentUser().getUserId());
		params.put(Params.Wall.ADS_UPDATE_CREDIT, creditValue);

		BaseResponse response = submit(params, URLs.Methods.UPDATE_USER_CREDIT);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA
				|| response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			StudioManager.getInstance().getUserCredit();
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}

	}

	public void activateUser() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					activateUserFromServer();
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	protected void activateUserFromServer() throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.User.USER_ID, getCurrentUser().getUserId());

		BaseResponse response = submit(params, URLs.Methods.ACTIVATE_USER);
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA
				|| response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			// cache credit

		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	public void deActivateUser() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					deActivateUserFromServer();
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	protected void deActivateUserFromServer() throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());

		BaseResponse response = submit(params, URLs.Methods.DEACTIVATE_USER);
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA
				|| response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			// cache credit

		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}
}
