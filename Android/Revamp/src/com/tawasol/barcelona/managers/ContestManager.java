package com.tawasol.barcelona.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.business.BusinessManager;
import com.tawasol.barcelona.business.BusinessManager.Action.ActionType;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.data.connection.URLs;
import com.tawasol.barcelona.entities.Contest;
import com.tawasol.barcelona.entities.ContestData;
import com.tawasol.barcelona.entities.ContestPrize;
import com.tawasol.barcelona.entities.ContestWinner;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnContestReceived;
import com.tawasol.barcelona.responses.BaseResponse;
import com.tawasol.barcelona.utils.LanguageUtils;

public class ContestManager extends BusinessManager<Contest> {
	
	private static ContestManager instance;
	
	// set the operations ID 
	public static final int ADD_FAV_WINNER = 1;
	public static final int REMOVE_FAV_WINNER = 2;
	public static final int FOLLOW_WINNER = 3;
	public static final int UNFOLLOW_WINNER = 4;
	
	// define the winner object bundle 
	public static final String WINNER_OBJ_KEY="WinnerKey";
	
	// define the index of the selected winner bundle
	public static final String WINNER_INDEX_KEY="WinnerIndexKey";
	
	// define the operation id bundle 
	public static final String WINNER_OPERATION_ID="WinnerOperationId";
	
	// define the row view of the list bundle
	public static final String WINNER_ROW_VIEW="WinnerRowView";
	
	 static ContestData contest;
	
	// constructor
	protected ContestManager() {
		super(Contest.class);
	}

	// get instance of the manager
	public static ContestManager getInstance() {
		if (instance == null)
			instance = new ContestManager();
		return instance;
	}
	
	

/*public void setContest(ContestData contest) {
		this.contest = contest;
	}*/
public ContestData getContest() {
	return contest;
}


/**Get Contest Data*/
public void getContestData()
{
	App.getInstance().runInBackground(new Runnable() {
		
		@Override
		public void run() {
			// set Language and User parameters
						Map<String, Object> params = new HashMap<String, Object>();
						params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
						params.put(Params.Contest.USER_ID,UserManager.getInstance().getCurrentUserId());
						
				try {
					contest = getObject(params, URLs.Methods.GET_CONTEST,
							ContestData.class);
					//fillContestData();
					
					// sort the prizes list according to prize rank ASC 
					if (contest != null && contest.getContestPrize() != null){
						Collections.sort(contest.getContestPrize(),
								new Comparator<ContestPrize>() {

									@Override
									public int compare(ContestPrize lhs,
											ContestPrize rhs) {
										return lhs.getPrizeRank().compareTo(
												rhs.getPrizeRank());
									}
								});
						
					}
					
					// then map the winners and prizes lists 
					if(contest != null)
						Map_Winners_And_Prizes();
					//notifyRetrievalException(new AppException("Error"),OnContestWinnersReceived.class);

					notifyEntityReceviedSuccess(contest,
							OnContestReceived.class);
				} catch (AppException e) {
							e.printStackTrace();
							notifyRetrievalException(AppException.getAppException(e), OnContestReceived.class);
						}
		}
	});
}


/** Handle Mapping between winners and prizes 
 * to map which winner got which prize  
 * **/

private void  Map_Winners_And_Prizes()
{
	List<ContestWinner> winners = new ArrayList<ContestWinner>();
		if (contest.getContestPrize() != null
				&& contest.getContestWinner() != null
				&& !contest.getContestPrize().isEmpty()
				&& !contest.getContestWinner().isEmpty()) {
for (ContestPrize prize : contest.getContestPrize()) {
	for(ContestWinner winner : contest.getContestWinner())
	{
		if(prize.getWinnerId()!=0 && winner.getWinnerId() != 0 && prize.getWinnerId() == winner.getWinnerId())
			winners.add(winner);
	}
}

contest.setContestWinners(winners);
		}
}


/**handle Winner operations */
public void handleWinnerOperations(Intent data)
{
	// get the winner object 
	final ContestWinner winner = (ContestWinner) data.getExtras()
			.getSerializable(WINNER_OBJ_KEY);
	
	// get the index 
	final int winnerIndex = (int)data.getExtras().getInt(WINNER_INDEX_KEY,-1);
	
	// get the operation id 
	final int operationId = (int)data.getExtras().getInt(WINNER_OPERATION_ID,0);
	
	// get the row view 
	final View rowView = (View)data.getExtras().getSerializable(WINNER_ROW_VIEW);
	
	App.getInstance().runOnUiThread(new Runnable() {
		
		@Override
		public void run() {
			// validate the intent data
			if(winner != null && winnerIndex != -1 && operationId !=0)
			{
				// case Add to Favorite
				/*if(operationId == ADD_FAV_WINNER)
				{
					((ImageButton)rowView getRowFromList(contest.getContestWinner(), winnerIndex)
							.findViewById(R.id.contest_winner_fav_btn))
							.setImageResource(R.drawable.favourate);
					handleFavo(UserManager.getInstance()
							.getCurrentUserId(),
							winner.getWinnerId(), true);
				}*/
				
//				 if(operationId == REMOVE_FAV_WINNER)
//				{
//					((ImageButton) rowView/* getRowFromList(contest.getContestWinner(), winnerIndex)*/
//							.findViewById(R.id.contest_winner_fav_btn))
//							.setImageResource(R.drawable.unfavourate);
//					handleFavo(UserManager.getInstance()
//							.getCurrentUserId(),
//							winner.getWinnerId(), false);
//				}
				
				 if(operationId == FOLLOW_WINNER)
				{
					((ImageButton) rowView/* getRowFromList(contest.getContestWinner(), winnerIndex)*/
							.findViewById(R.id.follow_winner_btn))
							.setImageResource(R.drawable.followed);
					handleFollow(UserManager.getInstance()
							.getCurrentUserId(),
							winner.getWinnerId(), true);
				}
				
				else if(operationId == UNFOLLOW_WINNER)
				{
					((ImageButton) rowView/* getRowFromList(contest.getContestWinner(), winnerIndex)*/
							.findViewById(R.id.follow_winner_btn))
							.setImageResource(R.drawable.unfollowed);
					handleFollow(UserManager.getInstance()
							.getCurrentUserId(),
							winner.getWinnerId(), false);
				}
			}
		}
	});
}

private void fillContestData()
{
	List<ContestPrize> prizes = new ArrayList<ContestPrize>();
	prizes.add(new ContestPrize(1, 1, "Hello", "http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg", 1, 1));
	
	List<ContestWinner> winners = new ArrayList<ContestWinner>();
	winners.add(new ContestWinner(1, "Mohamed ahmed Mahmoud", false, false, true, true, false, "http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg", 233));
	Contest contest1 = new Contest(1, "Contest Hello", 0, 0, 4);
	contest = new ContestData(contest1, prizes, winners, "https://www.google.com.eg/");
}


/*** Handle Cache Methods ****/
// not used after update SDS 

/* get cached rules*/
/*private ContestRules getCachedRules()
{
	return ContestRulesTable.getInstance().getLocalizedRulesUrl(LanguageUtils.getInstance().getDeviceLanguage());
}

 cache rules
private void cacheRules(ContestRules rule)
{
	ContestRulesTable.getInstance().insert(rule);
}

*/
//-------------------------------------------------------------------

	// --------------------------follow part-------------------------------

	public void handleFollow(final int userId, final int winnerId,
			final boolean follow) {
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, winnerId);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performFollow(userId, winnerId, follow);
						notifyActionDone(action);
						onFollowProcessSuccess(winnerId, follow);
						
					}
				} catch (Exception e) {
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	
	

	protected void onFollowProcessSuccess(int winnerId, boolean follow) {
		final int index = getWinnerPosition(winnerId);
		final ContestWinner winner = getWinnerForId(winnerId);
		if (winner != null) {
			if (follow) {
				winner.setFollowed(true);
			} else {
				winner.setFollowed(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					//UIUtils.showToast(App.getInstance().getApplicationContext(), "done");
					contest.getContestWinner().remove(index);
					contest.getContestWinner().add(index, winner);
				}
			});
		}
	}

	protected void performFollow(int userId, int fanId, boolean follow)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, fanId);
		if (follow)
			parametrs.put(Params.Fans.FOLLOW, 1);
		else
			parametrs.put(Params.Fans.FOLLOW, 0);

		BaseResponse response = submit(parametrs, URLs.Methods.CHANGE_FOLLOW);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	// -------------------------------------fav part-------------------

	public void handleFavo(final int userId, final int fanId,
			final boolean favourat) {
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, fanId);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performFavourat(userId, fanId, favourat);
						notifyActionDone(action);
						onFavoProcessSuccess(fanId, favourat);
					}
				} catch (Exception e) {
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	


	protected void onFavoProcessSuccess(int winnerId, boolean favourat) {
		final int index = getWinnerPosition(winnerId);
		final ContestWinner winner = getWinnerForId(winnerId);
		if (winner != null) {
			if (favourat) {
				winner.setFavorite(true);
			} else {
				winner.setFavorite(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					contest.getContestWinner().remove(index);
					contest.getContestWinner().add(index, winner);
					//UIUtils.showToast(App.getInstance().getApplicationContext(), "done");
				}
			});
		}
	}

	protected void performFavourat(int userId, int fanId, boolean favourate)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, fanId);
		if (favourate) {
			parametrs.put(Params.Fans.FAVOURATE, 1);
		} else {
			parametrs.put(Params.Fans.FAVOURATE, 0);
		}

		BaseResponse response = submit(parametrs, URLs.Methods.CHANGE_FAVOURATE);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}
	
	
	private int getWinnerPosition(int fanId) {
		for (int x = 0; x < contest.getContestWinner().size(); x++) {
			if (fanId == contest.getContestWinner().get(x).getWinnerId())
				return x;
		}
		return 0;
	}

	private ContestWinner getWinnerForId(int winnerId) {
		for (int x = 0; x < contest.getContestWinner().size(); x++) {
			if (winnerId == contest.getContestWinner().get(x).getWinnerId())
				return contest.getContestWinner().get(x);
		}
		return null;
	}
	
	
	/** get Prizes List*/
	/*public void getContestPrizes()
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				// set the language parameter 
				Map<String, Object> params = new HashMap<String, Object>();
				params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
				
				//try 
				{
					Prizes contestPrizes = fillPrizesData(); getObject(params, URLs.Methods.GET_CONTEST_PRIZES, Prizes.class);
					notifyEntityReceviedSuccess(contestPrizes, OnContestPrizesReceived.class);
				} 
//				catch (AppException e) {
//					e.printStackTrace();
//					notifyRetrievalException(AppException.getAppException(e), OnContestPrizesReceived.class);
//				}
			}
		});
	}
	
	*//** get Rules URL*//*
	public void getContestRules()
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				String rulesUrl = "";
				
				// get rule url from cache 
				try{
					if(getCachedRules() != null){
						rulesUrl = getCachedRules().getRuleUrl();
						NotifyRulesResponse(rulesUrl);
					}
					
				}
				catch (Exception e) {
					rulesUrl="";
				}
				
				if(ValidatorUtils.isRequired(rulesUrl)){
				// set the language parameter 
				Map<String, Object> params = new HashMap<String, Object>();
				params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
				
				BaseResponse response;
				try {
					response = submit(params, URLs.Methods.GET_CONTEST_RULES);
				

				
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
					try {
						rulesUrl = new JSONObject(response.getData())
								.getString("link");
						cacheRules(new ContestRules(LanguageUtils.getInstance().getDeviceLanguage(), rulesUrl));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					NotifyRulesResponse(rulesUrl);
				} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
					if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
						throw new AppException(
								response.getValidationRule().errorMessage);
					else
						throw new AppException(
								BaseResponse.getExceptionType(response.getStatus()));
				}
				} catch (AppException e1) {
					e1.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e1), OnContestRulesReceived.class);
				}
				}
			}
			
		});
	}
Notify the Rules response listener
private void NotifyRulesResponse(final String rulesLink)
{

	App.getInstance().runOnUiThread(new Runnable() {
		@Override
		public void run() {
			for (UiListener temp : getListeners())
				if (temp instanceof OnContestRulesReceived)
					((OnContestRulesReceived) temp).onSuccess(rulesLink);
		}
	});


}


*//** get Contest Winners List*//*
public void getContestWinners()
{
	App.getInstance().runInBackground(new Runnable() {
		
		@Override
		public void run() {
			// set the language parameter 
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
			params.put(Params.Contest.USER_ID,UserManager.getInstance().getCurrentUser().getUserId());
			
			//try 
			{
				 contestwinners = fillWinnerData(); getObject(params, URLs.Methods.GET_CONTEST_WINNERS, Winners.class);
				notifyEntityReceviedSuccess(contestwinners, OnContestWinnersReceived.class);
			} 
//			catch (AppException e) {
//				e.printStackTrace();
//				notifyRetrievalException(AppException.getAppException(e), OnContestWinnersReceived.class);
//			}
		}
	});
}*/

}
