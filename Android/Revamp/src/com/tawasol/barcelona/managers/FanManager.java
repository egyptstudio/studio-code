package com.tawasol.barcelona.managers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;

import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.business.BusinessManager;
import com.tawasol.barcelona.business.BusinessManager.Action;
import com.tawasol.barcelona.business.BusinessManager.Action.ActionType;
import com.tawasol.barcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.data.connection.URLs;
import com.tawasol.barcelona.data.helper.DataHelper;
import com.tawasol.barcelona.entities.ContactEntity;
import com.tawasol.barcelona.entities.Fan;
import com.tawasol.barcelona.entities.FansFilterParams;
import com.tawasol.barcelona.entities.GetFanModel;
import com.tawasol.barcelona.entities.SuggestObject;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.entities.UserContacts;
import com.tawasol.barcelona.entities.WhoInstalledAppObject;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnFanListRecieved;
import com.tawasol.barcelona.listeners.OnFanProfileRecieved;
import com.tawasol.barcelona.listeners.OnFriendResponseListener;
import com.tawasol.barcelona.listeners.OnPremiumBarRecieved;
import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
import com.tawasol.barcelona.listeners.OnSuggestionListRecieved;
import com.tawasol.barcelona.listeners.UiListener;
import com.tawasol.barcelona.responses.BaseResponse;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.UIUtils;

public class FanManager extends BusinessManager<GetFanModel> {

	public static String syncedContactBefore;
	private static String lastTimeContactsSynced;
	public static final int FOLLOWERS = 18484;
	public static final int FOLLOWING = 44354;
	public static final String STARTING_LIMIT = "starting_limit";
	private static FanManager instance;
	private Context context;
	private List<Fan> premiumFanBar = new ArrayList<Fan>();
	private List<Fan> fans = new ArrayList<Fan>();
	private List<Fan> premium = new ArrayList<Fan>();
	private List<Fan> filtered = new ArrayList<Fan>();
	private Fan fan;
	private List<Fan> FollowList = new ArrayList<Fan>();
	private List<Fan> RequestList = new ArrayList<Fan>();
	private List<Fan> contactsList = new ArrayList<Fan>();
	private List<Fan> suggestList = new ArrayList<Fan>();

	protected FanManager() {
		super(GetFanModel.class);
		context = App.getInstance().getApplicationContext();
	}

	public static FanManager getInstance() {
		if (instance == null)
			instance = new FanManager();
		return instance;
	}

	// ---------------------Premium Bar -------------------------
	public List<Fan> getPremiumBar() {
		return premiumFanBar;
	}

	public void clearFollowList() {
		FollowList.clear();
	}

	public void clearPremium() {
		premium.clear();
	}

	public List<Fan> getContactsList() {
		return contactsList;
	}

	public List<Fan> getSuggestList() {
		return suggestList;
	}

	public List<Fan> getRequestList() {
		return RequestList;
	}

	public List<Fan> getFans() {
		return fans;
	}

	public void getPremiumBarFromServer() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					List<Fan> fans = loadPremiumUsersFromServer();
					notifyPremiumListRecieved(fans);
				} catch (Exception e) {
					notifyPremiumUserException(AppException.getAppException(e));
					e.printStackTrace();
				}
			}
		});
	}

	protected void notifyPremiumUserException(final AppException appException) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnPremiumBarRecieved)
						((OnPremiumBarRecieved) listener)
								.onException(appException);
				}
			}
		});
	}

	protected void notifyPremiumListRecieved(final List<Fan> fans) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnPremiumBarRecieved)
						((OnPremiumBarRecieved) listener)
								.onPremiumListRecieved(fans);
				}
			}
		});
	}

	protected List<Fan> loadPremiumUsersFromServer() throws AppException {
		return getObjectList(URLs.Methods.GET_PREMIUM_BAR, null, Fan.class);
	}

	// -------------------------------------------------------------------

	private void saveStartingLimit(final int startingLimit) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(context,
							STARTING_LIMIT, startingLimit);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public int getStartingLimit() {
		int startingLimit = 0;
		try {
			startingLimit = (Integer) InternalFileSaveDataLayer.getObject(
					context, STARTING_LIMIT);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return startingLimit;
	}

	// ------------------------get fans----------------------------------

	private Map<String, Object> prepareGetFanParameters(FansFilterParams param) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Fans.USER_ID, param.getUserId());
		params.put(Params.Fans.STARTINGLIMIT, param.getStartingLimit());
		params.put(Params.Fans.MALE_FEMALE_FILTER, param.getGender());
		params.put(Params.Fans.ONLINE_OFFLINE_FILTER, param.getUserState());
		params.put(Params.Fans.FAVOURATE_FILTER, param.getFavoritesFilter());
		params.put(Params.Fans.COUNTRY_FILTER, param.getCountryList());
		params.put(Params.Fans.ONLY_PREMIUM, param.isOnlyPremuim());
		params.put(Params.Fans.SORT_BY, param.getSortedBy());
		params.put(Params.Fans.KEYWORD, param.getKeyword());
		params.put(Params.Fans.FOLLOWING_FILTER, param.getFollowersOrFollwong());
		return params;
	}

	// -----------------------------------------------------

	public void getFansSuggestions(final int StartingLimits) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					getFansSuggestionsFromServer(StartingLimits);
					notifyEntityListReceviedSuccess(FollowList,
							OnFanListRecieved.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFanListRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	public List<Fan> getFansSuggestionsFromServer(int startingLimits)
			throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Fans.STARTINGLIMIT, startingLimits);
		GetFanModel fan = getObject(params,
				URLs.Methods.LOAD_FOLLOWING_SUGGESTIONS, GetFanModel.class);
		addToFollowList(fan.getFans());
		return fan.getFans();
	}

	// -----------------------------------------------------

	public void getFans(final FansFilterParams param) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					getFansFromServer(param);
					if (param.isOnlyPremuim() == 1)
						notifyEntityListReceviedSuccess(premium,
								OnFanListRecieved.class);
					else
						notifyEntityListReceviedSuccess(fans,
								OnFanListRecieved.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFanListRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	public void getFollowList(final int userID, final int fanID,
			final int type, final int startingLimit) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {

					getFollowListFromServer(userID, fanID, type, startingLimit);
					notifyEntityListReceviedSuccess(FollowList,
							OnFanListRecieved.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFanListRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	public List<Fan> getFollowListFromServer(int userID, int fanID, int type,
			int startingLimit) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Fans.USER_ID, userID);
		params.put(Params.Fans.FAN_ID, fanID);
		params.put(Params.Fans.STARTINGLIMIT, startingLimit);
		GetFanModel fan = null;
		if (type == FOLLOWING)
			fan = getObject(params, URLs.Methods.LOAD_FOLLOWING_FOR_FAN,
					GetFanModel.class);
		else
			fan = getObject(params, URLs.Methods.LOAD_FOLLOWERS_FOR_FAN,
					GetFanModel.class);
		addToFollowList(fan.getFans());
		return fan.getFans();
	}

	public void resetFollowList() {
		FollowList.clear();
	}

	public void clearFans() {
		fans.clear();
	}

	private void addToFollowList(List<Fan> fans2) {
		FollowList.addAll(fans2);
	}

	public void getFanFiltered(final FansFilterParams param) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					getFansFromServerFiltered(param);
					notifyEntityListReceviedSuccess(filtered,
							OnFanListRecieved.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFanListRecieved.class);
					e.printStackTrace();
				}
			}
		});

	}

	protected List<Fan> getFansFromServerFiltered(FansFilterParams param)
			throws AppException {
		GetFanModel fan = getObject(prepareGetFanParameters(param),
				URLs.Methods.GET_FANS, GetFanModel.class);
		addToFiltered(fan.getFans());
		saveStartingLimit(fan.getNextStartingLimit());
		// TODO save starting limits
		return fan.getFans();
	}

	private void addToFiltered(List<Fan> fans2) {
		filtered.addAll(fans2);
	}

	public List<Fan> getFansFromServer(FansFilterParams param)
			throws AppException {
		GetFanModel fan = getObject(prepareGetFanParameters(param),
				URLs.Methods.GET_FANS, GetFanModel.class);
		// List<Fan> fans = getObjectList(URLs.Methods.GET_FANS,
		// prepareGetFanParameters(param));
		if (param.isOnlyPremuim() == 1)
			addToPremium(fan.getFans());
		else
			addtoFans(fan.getFans());
		saveStartingLimit(fan.getNextStartingLimit());
		// TODO save starting limits
		return fan.getFans();
	}

	private void addToPremium(List<Fan> fans2) {
		premium.addAll(fans2);
	}

	private void addtoFans(List<Fan> fansList) {
		fans.addAll(fansList);

	}

	// ---------------------------------------------------------------------

	// --------------------------friend Part------------------------------

	public void handleFriend(final int userId, final int fanId,
			final boolean friend) {
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, fanId);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						if (friend)
							performFriend(userId, fanId, friend);
						else
							declineFriend(userId, fanId);
						notifyActionDone(action);
					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnSuccessVoidListener.class);
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	protected void declineFriend(int userId, int fanId) throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, fanId);

		final BaseResponse response = submit(parametrs,
				URLs.Methods.DECLINE_FRIENDS);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			onFriendProcessSuccess(fanId, false);
			if (premium != null && !premium.isEmpty())
				onFriendProcessSuccessPremium(fanId, false);
			if (filtered != null && !filtered.isEmpty())
				onFriendProcessSuccessFiltered(fanId, false);
			if (RequestList != null && !RequestList.isEmpty())
				onFriendProcessSuccessRequest(fanId, false);
			if (fan != null)
					fan.setFriend(false);
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					for (UiListener listener : getListeners()) {
						if (listener instanceof OnFriendResponseListener) {
							((OnFriendResponseListener) listener)
									.onSuccess(response.getStatus());
						}
					}
				}
			});
		}

		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}

	}

	protected void onFriendProcessSuccessRequest(int fanId, boolean friend) {
		Fan markedFan = new Fan(fanId);
		final int index = RequestList.indexOf(markedFan);
		Fan fan = RequestList.get(index);
		if (friend)
			fan.setFriend(true);
		else
			fan.setFriend(false);
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				UIUtils.showToast(context, "done");
				RequestList.remove(index);
			}
		});
	}

	protected void performFriend(int userId, int fanId, boolean friend)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, fanId);
		if (friend)
			parametrs.put(Params.Fans.FRIEND, 1);
		else
			parametrs.put(Params.Fans.FRIEND, 0);

		final BaseResponse response = submit(parametrs,
				URLs.Methods.CHANGE_FRIEND);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA ||
				response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			onFriendProcessSuccess(fanId, friend);
			if (premium != null && !premium.isEmpty())
				onFriendProcessSuccessPremium(fanId, friend);
			if (filtered != null && !filtered.isEmpty())
				onFriendProcessSuccessFiltered(fanId, friend);
			if (RequestList != null && !RequestList.isEmpty())
				onFriendProcessSuccessRequest(fanId, friend);
			if (fan != null)
				if (friend)
					fan.setFriend(true);
				else
					fan.setFriend(false);
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					for (UiListener listener : getListeners()) {
						if (listener instanceof OnFriendResponseListener) {
							((OnFriendResponseListener) listener)
									.onSuccess(response.getStatus());
						}
					}
				}
			});
		}

		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	protected void onFriendProcessSuccess(int fanId, boolean follow) {
		final int index = getFantPosition(fanId);
		final Fan fan = getFanForId(fanId);
		if (fan != null) {
			if (follow) {
				fan.setFriend(true);
			} else {
				fan.setFriend(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					UIUtils.showToast(context, "done");
					fans.remove(index);
					fans.add(index, fan);
				}
			});
		}
	}

	protected void onFriendProcessSuccessPremium(int fanId, boolean follow) {
		final int index = getFantPositionPremium(fanId);
		final Fan fan = getFanForIdPremium(fanId);
		if (fan != null) {
			if (follow) {
				fan.setFollowed(true);
			} else {
				fan.setFollowed(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					premium.remove(index);
					premium.add(index, fan);
				}
			});
		}
	}

	protected void onFriendProcessSuccessFiltered(int fanId, boolean follow) {
		final int index = getFantPositionFiltered(fanId);
		final Fan fan = getFanForIdFiltered(fanId);
		if (fan != null) {
			if (follow) {
				fan.setFollowed(true);
			} else {
				fan.setFollowed(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					filtered.remove(index);
					filtered.add(index, fan);
				}
			});
		}
	}

	// -------------------------------------------------------------------

	// --------------------------follow part-------------------------------

	public void handleFollow(final int userId, final int fanId,
			final boolean follow, final Activity activity) {
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, fanId);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performFollow(userId, fanId, follow);
						notifyActionDone(action);
						User user = UserManager.getInstance().getCurrentUser();
						if (follow)
							user.setFollowingCount(user.getFollowingCount() + 1);
						else
							user.setFollowingCount(user.getFollowingCount() - 1);
						UserManager.getInstance().cacheUser(user);
						if (activity instanceof BaseActivity)
							((BaseActivity) activity).refreshMenu();
						onFollowProcessSuccess(fanId, follow);
						if (premium != null && !premium.isEmpty())
							onFollowProcessSuccessPremium(fanId, follow);
						if (filtered != null && !filtered.isEmpty())
							onFollowProcessSuccessFiltered(fanId, follow);
						// if (fan != null)
						// fan.setFollowed(true);
						if (!FollowList.isEmpty())
							onFollowProcessSuccessFollowList(fanId, follow);
					}
				} catch (Exception e) {
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	protected void onFollowProcessSuccessFollowList(int fanId, boolean follow) {
		final int index = getFantPositionFollow(fanId);
		final Fan fan = getFanForIdFollow(fanId);
		if (fan != null) {
			if (follow) {
				fan.setFollowed(true);
			} else {
				fan.setFollowed(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					FollowList.remove(index);
					FollowList.add(index, fan);
				}
			});
		}
	}

	protected void onFollowProcessSuccessFiltered(int fanId, boolean follow) {
		final int index = getFantPositionFiltered(fanId);
		final Fan fan = getFanForIdFiltered(fanId);
		if (fan != null) {
			if (follow) {
				fan.setFollowed(true);
			} else {
				fan.setFollowed(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					filtered.remove(index);
					filtered.add(index, fan);
				}
			});
		}
	}

	protected void onFollowProcessSuccessPremium(int fanId, boolean follow) {
		final int index = getFantPositionPremium(fanId);
		final Fan fan = getFanForIdPremium(fanId);
		if (fan != null) {
			if (follow) {
				fan.setFollowed(true);
			} else {
				fan.setFollowed(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					premium.remove(index);
					premium.add(index, fan);
				}
			});
		}
	}

	protected void onFollowProcessSuccess(int fanId, boolean follow) {
		final int index = getFantPosition(fanId);
		final Fan fan = getFanForId(fanId);
		if (fan != null) {
			if (follow) {
				fan.setFollowed(true);
			} else {
				fan.setFollowed(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
//					UIUtils.showToast(context, "done");
					fans.remove(index);
					fans.add(index, fan);
				}
			});
		}
	}

	protected void performFollow(int userId, int fanId, boolean follow)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, fanId);
		if (follow)
			parametrs.put(Params.Fans.FOLLOW, 1);
		else
			parametrs.put(Params.Fans.FOLLOW, 0);

		BaseResponse response = submit(parametrs, URLs.Methods.CHANGE_FOLLOW);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	// -------------------------------------fav part-------------------

	public void handleFavo(final int userId, final int fanId,
			final boolean favourat) {
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, fanId);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performFavourat(userId, fanId, favourat);
						notifyActionDone(action);
						onFavoProcessSuccess(fanId, favourat);
						if (premium != null && !premium.isEmpty())
							onFavoProcessSuccessPremium(fanId, favourat);
						if (filtered != null && !filtered.isEmpty())
							onFavoProcessSuccessFiltered(fanId, favourat);
						// if (fan != null)
						// fan.setFavorite(true);
						if (!FollowList.isEmpty())
							onFavoProcessSuccessFollow(fanId, favourat);
					}
				} catch (Exception e) {
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	protected void onFavoProcessSuccessFollow(int fanId, boolean favourat) {
		final int index = getFantPositionFollow(fanId);
		final Fan fan = getFanForIdFollow(fanId);
		if (fan != null) {
			if (favourat) {
				fan.setFavorite(true);
			} else {
				fan.setFavorite(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					UIUtils.showToast(context, "done");
					FollowList.remove(index);
					FollowList.add(index, fan);
				}
			});
		}
	}

	protected void onFavoProcessSuccessFiltered(int fanId, boolean favourat) {
		final int index = getFantPositionFiltered(fanId);
		final Fan fan = getFanForIdFiltered(fanId);
		if (fan != null) {
			if (favourat) {
				fan.setFavorite(true);
			} else {
				fan.setFavorite(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					UIUtils.showToast(context, "done");
					filtered.remove(index);
					filtered.add(index, fan);
				}
			});
		}
	}

	protected void onFavoProcessSuccessPremium(int fanId, boolean favourat) {
		final int index = getFantPositionPremium(fanId);
		final Fan fan = getFanForIdPremium(fanId);
		if (fan != null) {
			if (favourat) {
				fan.setFavorite(true);
			} else {
				fan.setFavorite(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					premium.remove(index);
					premium.add(index, fan);
				}
			});
		}
	}

	protected void onFavoProcessSuccess(int fanId, boolean favourat) {
		final int index = getFantPosition(fanId);
		final Fan fan = getFanForId(fanId);
		if (fan != null) {
			if (favourat) {
				fan.setFavorite(true);
			} else {
				fan.setFavorite(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					fans.remove(index);
					fans.add(index, fan);
				}
			});
		}
	}

	protected void performFavourat(int userId, int fanId, boolean favourate)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, fanId);
		if (favourate) {
			parametrs.put(Params.Fans.FAVOURATE, 1);
		} else {
			parametrs.put(Params.Fans.FAVOURATE, 0);
		}

		BaseResponse response = submit(parametrs, URLs.Methods.CHANGE_FAVOURATE);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	private Fan getFanForId(int fanId) {
		for (int x = 0; x < fans.size(); x++) {
			if (fanId == fans.get(x).getFanID())
				return fans.get(x);
		}
		return null;
	}

	private int getFantPosition(int fanId) {
		for (int x = 0; x < fans.size(); x++) {
			if (fanId == fans.get(x).getFanID())
				return x;
		}
		return 0;
	}

	private Fan getFanForIdPremium(int fanId) {
		for (int x = 0; x < premium.size(); x++) {
			if (fanId == premium.get(x).getFanID())
				return premium.get(x);
		}
		return null;
	}

	private int getFantPositionPremium(int fanId) {
		for (int x = 0; x < premium.size(); x++) {
			if (fanId == premium.get(x).getFanID())
				return x;
		}
		return 0;
	}

	private Fan getFanForIdFiltered(int fanId) {
		for (int x = 0; x < filtered.size(); x++) {
			if (fanId == filtered.get(x).getFanID())
				return filtered.get(x);
		}
		return null;
	}

	private int getFantPositionFiltered(int fanId) {
		for (int x = 0; x < filtered.size(); x++) {
			if (fanId == filtered.get(x).getFanID())
				return x;
		}
		return 0;
	}

	private Fan getFanForIdFollow(int fanId) {
		for (int x = 0; x < FollowList.size(); x++) {
			if (fanId == FollowList.get(x).getFanID())
				return FollowList.get(x);
		}
		return null;
	}

	private int getFantPositionFollow(int fanId) {
		for (int x = 0; x < FollowList.size(); x++) {
			if (fanId == FollowList.get(x).getFanID())
				return x;
		}
		return 0;
	}

	public void clearFiltered() {
		filtered.clear();
	}

	public void getSuggestedFans(final String keyWord) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					final List<Fan> fans = getSuggestedFansFromServer(keyWord);
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							for (UiListener listener : getListeners()) {
								if (listener instanceof OnSuggestionListRecieved) {
									((OnSuggestionListRecieved) listener)
											.onSuccess(fans);
								}
							}
						}
					});
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFanListRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	public List<Fan> getSuggestedFansFromServer(String keyWord)
			throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Fans.KEYWORD, keyWord);
		GetFanModel fans = getObject(params, URLs.Methods.GET_SUGGESTED_FANS,
				GetFanModel.class);
		return fans.getFans();
	}

	// ----------------------Block part

	public void handleBlock(final int userId, final int fanId,
			final boolean bloced, final FragmentActivity fragmentActivity) {
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, fanId);
			ProgressDialog dialog = new ProgressDialog(fragmentActivity,
					R.style.MyTheme);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						App.getInstance().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
								dialog.setCancelable(false);
								dialog.show();
							}
						});
						notifyActionStarted(action);
						performBlock(userId, fanId, bloced);
						notifyActionDone(action);
						onBlockProcessSuccess(fanId, bloced);
						if (premium != null && !premium.isEmpty())
							onBlockProcessSuccessPremium(fanId, bloced);
						if (filtered != null && !filtered.isEmpty())
							onBlockProcessSuccessFiltered(fanId, bloced);
						// if (fan != null)
						// fan.setBlocked(true);
						if (!FollowList.isEmpty())
							onBlockProcessSuccessFollow(fanId, bloced);
						if (bloced)
							fragmentActivity.finish();
						App.getInstance().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								dialog.dismiss();
							}
						});
					}
				} catch (Exception e) {
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							dialog.dismiss();
						}
					});
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	protected void onBlockProcessSuccessFollow(int fanId, boolean favourat) {
		final int index = getFantPositionFollow(fanId);
		final Fan fan = getFanForIdFollow(fanId);
		if (fan != null) {
			if (favourat) {
				fan.setBlocked(true);
			} else {
				fan.setBlocked(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					UIUtils.showToast(context, "done");
					FollowList.remove(index);
				}
			});
		}
	}

	protected void onBlockProcessSuccessFiltered(int fanId, boolean favourat) {
		final int index = getFantPositionFiltered(fanId);
		final Fan fan = getFanForIdFiltered(fanId);
		if (fan != null) {
			if (favourat) {
				fan.setBlocked(true);
			} else {
				fan.setBlocked(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					UIUtils.showToast(context, "done");
					filtered.remove(index);
				}
			});
		}
	}

	protected void onBlockProcessSuccessPremium(int fanId, boolean favourat) {
		final int index = getFantPositionPremium(fanId);
		final Fan fan = getFanForIdPremium(fanId);
		if (fan != null) {
			if (favourat) {
				fan.setBlocked(true);
			} else {
				fan.setBlocked(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					premium.remove(index);
				}
			});
		}
	}

	protected void onBlockProcessSuccess(int fanId, boolean favourat) {
		final int index = getFantPosition(fanId);
		final Fan fan = getFanForId(fanId);
		if (fan != null) {
			if (favourat) {
				fan.setBlocked(true);
			} else {
				fan.setBlocked(false);
			}
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					fans.remove(index);
				}
			});
		}
	}

	protected void performBlock(int userId, int fanId, boolean blocked)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, fanId);
		if (blocked) {
			parametrs.put(Params.Fans.BLOCK, 1);
		} else {
			parametrs.put(Params.Fans.BLOCK, 0);
		}

		BaseResponse response = submit(parametrs, URLs.Methods.CHANGE_BLOCK);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	// --------------------------------------------

	// ---------------------getProfile------------------
	public void getPublicProfile(final int fanId, final int userId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					fan = getProfileFromServer(fanId, userId);
					notifyEntityReceviedSuccess(fan, OnFanProfileRecieved.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFanProfileRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	protected Fan getProfileFromServer(int fanId, int userId)
			throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Fans.USER_ID, userId);
		params.put(Params.Fans.FAN_ID, fanId);

		Fan fan = getObject(params, URLs.Methods.GET_PUBLIC_PROFILE, Fan.class);
		return fan;
	}

	public void resetFan() {
		fan = null;
	}

	public void loadRequestList() {
		final Action action = new Action(ActionType.GET_POSTS, 1);
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						RequestList = loadRequestListFromServer();
						notifyEntityListReceviedSuccess(RequestList,
								OnFanListRecieved.class);
						notifyActionDone(action);
					}
				} catch (Exception e) {
					notifyActionDone(action);
					notifyRetrievalException(AppException.getAppException(e),
							OnFanListRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	protected List<Fan> loadRequestListFromServer() throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Fans.USER_ID, UserManager.getInstance()
				.getCurrentUserId());
		params.put(Params.Fans.STARTINGLIMIT, 0);
		List<Fan> fan = getObjectList(URLs.Methods.GET_FRIENDS_REQUEST, params,
				Fan.class);
		return fan;
	}

	public void declineFriendRequest(final int fanID) {
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, fanID);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performDeclineFriend(UserManager.getInstance()
								.getCurrentUserId(), fanID);
						notifyActionDone(action);
						onFriendProcessSuccess(fanID, false);
						if (premium != null && !premium.isEmpty())
							onFriendProcessSuccessPremium(fanID, false);
						if (filtered != null && !filtered.isEmpty())
							onFriendProcessSuccessFiltered(fanID, false);
						if (fan != null)
							fan.setFriend(false);
						notifyVoidSuccess(OnSuccessVoidListener.class);
					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnSuccessVoidListener.class);
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	protected void performDeclineFriend(int currentUserId, int fanID)
			throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Fans.USER_ID, UserManager.getInstance()
				.getCurrentUserId());

		BaseResponse response = submit(params, URLs.Methods.DECLINE_FRIENDS);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	public boolean checkSuggestions() {
		return new File(context.getFilesDir() + "/" + syncedContactBefore)
				.exists();
	}

	public void saveSuggestion() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(context,
							syncedContactBefore, "");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected void saveLastTimeContactsSynced(final long currentRequestTime) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(context,
							lastTimeContactsSynced, currentRequestTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public long getlastTimeContactsSynced() {
		long currentTime = 0;
		try {
			currentTime = (Long) InternalFileSaveDataLayer.getObject(context,
					lastTimeContactsSynced);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return currentTime;
	}

	public void loadContacts() {
		final Action action = new Action(ActionType.GET_POSTS, 5557);
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						contactsList = loadContactsFromServer();
						notifyEntityListReceviedSuccess(contactsList,
								OnFanListRecieved.class);
						notifyActionDone(action);
					}
				} catch (AppException e) {
					notifyActionDone(action);
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	protected List<Fan> loadContactsFromServer() throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Fans.USER_ID, UserManager.getInstance()
				.getCurrentUserId());
		List<Fan> fans = getObjectList(URLs.Methods.LOAD_CONTACTS, params,
				Fan.class);
		return fans;
	}

	public void sendContacts(final SuggestObject contacts) {
		final Action action = new Action(ActionType.GET_POSTS, 55848);
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						suggestList = sendContactsToServer(contacts);
						notifyEntityListReceviedSuccess(suggestList,
								OnFanListRecieved.class);
						saveSuggestion();
						notifyActionDone(action);
					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFanListRecieved.class);
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	protected List<Fan> sendContactsToServer(SuggestObject contacts)
			throws AppException {
		WhoInstalledAppObject fans = null;
		BaseResponse response = submit(contacts,
				URLs.Methods.WHO_INSTALLED_APP, SuggestObject.class);
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			fans = DataHelper.deserialize(response.getData(),
					WhoInstalledAppObject.class);
			if (fans.getInstalled().isEmpty())
				throw new AppException(AppException.NO_DATA_EXCEPTION);
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));

		}
		return fans.getInstalled();
	}
}
