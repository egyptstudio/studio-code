///**
// * 
// */
//package com.tawasol.barcelona.managers;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//import java.util.Timer;
//import java.util.TimerTask;
//
//import android.content.Context;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.google.android.gms.internal.ih;
//import com.tawasol.barcelona.application.App;
//import com.tawasol.barcelona.business.BusinessManager;
//import com.tawasol.barcelona.business.BusinessManager.Action.ActionType;
//import com.tawasol.barcelona.data.cache.InternalFileSaveDataLayer;
//import com.tawasol.barcelona.data.cache.SharedPrefrencesDataLayer;
//import com.tawasol.barcelona.data.connection.Params;
//import com.tawasol.barcelona.data.connection.URLs;
//import com.tawasol.barcelona.entities.Country;
//import com.tawasol.barcelona.entities.Fan;
//import com.tawasol.barcelona.entities.FragmentInfo;
//import com.tawasol.barcelona.entities.PostLike;
//import com.tawasol.barcelona.entities.PostViewModel;
//import com.tawasol.barcelona.entities.TopTenModel;
//import com.tawasol.barcelona.entities.UserComment;
//import com.tawasol.barcelona.entities.WALLPosts;
//import com.tawasol.barcelona.entities.WebServiceRequestInfo;
//import com.tawasol.barcelona.exception.AppException;
//import com.tawasol.barcelona.listeners.OnCommentAdded;
//import com.tawasol.barcelona.listeners.OnCommentDeleted;
//import com.tawasol.barcelona.listeners.OnCommentsListReceived;
//import com.tawasol.barcelona.listeners.OnCountriesRecieved;
//import com.tawasol.barcelona.listeners.OnFollowSuccess;
//import com.tawasol.barcelona.listeners.OnLikeRecieved;
//import com.tawasol.barcelona.listeners.OnPostDeletedListener;
//import com.tawasol.barcelona.listeners.OnPostsReceivedListener;
//import com.tawasol.barcelona.listeners.OnPostsRecieved;
//import com.tawasol.barcelona.listeners.OnSeasonReceived;
//import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
//import com.tawasol.barcelona.listeners.OnUpdateFinished;
//import com.tawasol.barcelona.listeners.UiListener;
//import com.tawasol.barcelona.listeners.UpdateDialogListener;
//import com.tawasol.barcelona.responses.BaseResponse;
//import com.tawasol.barcelona.ui.activities.BaseActivity;
//import com.tawasol.barcelona.ui.fragments.WallFragment;
//
///**
// * @author Basyouni
// *
// */
//public class HomeManager extends BusinessManager<WALLPosts> {
//
//	private static final String PHOTO_DETAILS_INITIAL_STATE_KEY = "com.tawasol.barcelona.managers.photo_details_init_state";
//
//	public static final int LIKE_POST = 0;
//	public static final int DISLIKE_POST = 1;
//	String methodName = null;
//	List<PostViewModel> posts = new ArrayList<PostViewModel>();
//	public static final String INITIAL_STATE_HOME = "initialState";
//	public static final String CURRENT_TIME = "currentrquestTime";
//	public static final String LastUpdateTime = "lastUpdateTime";
//	Context context;
//	public static final int GET_TAGS = 1;
//	public static final int GET_POSTS = 2;
//	public static final int GET_USER_POSTS = 3;
//	public static final int NUM_OF_POSTS = 10;
//	List<PostViewModel> Recievedposts = null;
//	private boolean found;
//	Action action;
//	HashMap<Fan, List<PostViewModel>> postsStorage = new LinkedHashMap<Fan, List<PostViewModel>>();
//	public static PostViewModel EMPTY_POST = new PostViewModel(-1);
//
//	/**
//	 * @param clazz
//	 */
//	protected HomeManager() {
//		super(WALLPosts.class);
//		context = App.getInstance().getApplicationContext();
//	}
//
//	public static HomeManager instance;
//
//	public static HomeManager getInstance() {
//		if (instance == null)
//			instance = new HomeManager();
//		return instance;
//	}
//
//	public List<PostViewModel> getPosts(WebServiceRequestInfo info)
//			throws AppException {
//		WALLPosts post = getPostsFromServer(info);
//		if (info.getMethodName().equalsIgnoreCase("wall/getLatest"))
//			addToStorage(post.getPosts());
//		if (info.getAction() == WebServiceRequestInfo.ACTION_FIRST_TIME) {
//			saveCurrentTime(post.getCurrentRequestTime());
//			getLastUpdateTime(post.getPosts());
//		}
//		if (info.getAction() == WebServiceRequestInfo.ACTION_GETMORE) {
//			getLastUpdateTime(post.getPosts());
//		}
//		handleDataRecieved(post.getPosts(), info.getAction());
//		return post.getPosts();
//	}
//
//	/**
//	 * @param posts
//	 */
//	private void addToStorage(List<PostViewModel> posts) {
//		for (int x = 0; x < posts.size(); x++) {
//			Fan fan = new Fan(posts.get(x).getUserId(), posts.get(x)
//					.getUserName(), posts.get(x).getUserProfilePicUrl(), posts
//					.get(x).isFollowing());
//			if (!checkFanExistanceInStorage(fan.getUserId())) {
//				List<PostViewModel> postsToSave = new ArrayList<PostViewModel>();
//				postsToSave.add(0, EMPTY_POST);
//				postsToSave.add(posts.get(x));
//				postsToSave.add(EMPTY_POST);
//				postsStorage.put(fan, postsToSave);
//			} else {
//				List<PostViewModel> postsForFan = getPostsForFan(fan
//						.getUserId());
//				postsForFan.remove(postsForFan.size() - 1);
//				for (int j = 0; j < postsForFan.size(); j++) {
//					if (posts.get(x).getPostId() == postsForFan.get(j)
//							.getPostId()) {
//						postsForFan.remove(j);
//						break;
//					}
//				}
//				postsForFan.add(posts.get(x));
//				Collections.sort(postsForFan);
//				postsForFan.add(EMPTY_POST);
//				Collections.reverse(postsForFan);
//				postsStorage.put(fan, postsForFan);
//			}
//		}
//	}
//
//	public int getPostPositionInPostsStorage(int postId, int userId) {
//		int position = 0;
//		List<PostViewModel> posts = getPostsForFan(userId);
//		for (int x = 0; x < posts.size(); x++) {
//			if (postId == posts.get(x).getPostId()) {
//				position = x;
//				break;
//			}
//		}
//		return position;
//	}
//
//	/**
//	 * @param fan
//	 * @return
//	 */
//	private List<PostViewModel> getPostsForFan(int userId) {
//		List<PostViewModel> posts = new ArrayList<PostViewModel>();
//		for (Map.Entry<Fan, List<PostViewModel>> value : postsStorage
//				.entrySet()) {
//			if (userId == value.getKey().getUserId())
//				posts.addAll(value.getValue());
//		}
//		return posts;
//	}
//
//	/**
//	 * @param fan
//	 * @return
//	 */
//	private boolean checkFanExistanceInStorage(int userId) {
//		boolean exist = false;
//		for (Map.Entry<Fan, List<PostViewModel>> value : postsStorage
//				.entrySet()) {
//			if (userId == value.getKey().getUserId()) {
//				exist = true;
//				break;
//			}
//		}
//		return exist;
//	}
//
//	public List<PostViewModel> getFanOlderPosts(final int fanId) {
//		// App.getInstance().runInBackground(new Runnable() {
//		// @Override
//		// public void run() {
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		List<PostViewModel> posts = new ArrayList<PostViewModel>(2);
//		posts.add(new PostViewModel(new Random().nextInt()));
//		posts.add(new PostViewModel(new Random().nextInt()));
//		notifyEntityListReceviedSuccess(posts, OnPostsReceivedListener.class);
//		return posts;
//	}
//
//	public List<PostViewModel> getFanNewerPosts(final int fanId) {
//		WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
//				WebServiceRequestInfo.ACTION_GETMORE, UserManager.getInstance()
//						.getCurrentUserId(), HomeManager.NUM_OF_POSTS,
//				HomeManager.getInstance().getLastUpdateTime(), 0, 0, fanId,
//				WebServiceRequestInfo.GET_USER_POSTS, "wall/getUserPosts");
//		Action action = new Action(ActionType.GET_POSTS, requestInfo);
//		try {
//			if (!needsProcessing(action))
//				return null;
//
//			notifyActionStarted(action);
//			List<PostViewModel> posts = getPosts(requestInfo);
//			notifyActionDone(action);
//			return posts;
//		} catch (Exception e) {
//			notifyActionDone(action);
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	public void handlePosts(final WebServiceRequestInfo requestInfo) {
//		final Action action = new Action(ActionType.FOLLOW, requestInfo);
//		// this.action = action;
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					System.out.println(needsProcessing(action));
//					if (needsProcessing(action)) {
//						notifyActionStarted(action);
//						Recievedposts = getPosts(requestInfo);
//						notifyActionDone(action);
//					}
//					// handleDataRecieved(Recievedposts,
//					// requestInfo.getAction());
//				} catch (Exception e) {
//					notifyActionDone(action);
//					notifyUpdateFinished();
//					notifyRetrievalException(AppException.getAppException(e),
//							OnPostsRecieved.class);
//					// notifyLikeRecieveException(AppException
//					// .getAppException(e));
//					// notifyPostsRecievedFailed(AppException
//					// .getAppException(e));
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	/**
//	 * @param recievedposts2
//	 */
//	protected void handleDataRecieved(List<PostViewModel> recievedposts,
//			int action) {
//		if (action == WebServiceRequestInfo.ACTION_FIRST_TIME) {
//			actiongetMore(recievedposts);
//			notifyEntityListReceviedSuccess(recievedposts,
//					OnPostsRecieved.class);
//		} else if (action == WebServiceRequestInfo.ACTION_UPDATE) {
//			addUpdatesToListDataStructure(recievedposts);
//			notifyEntityListReceviedSuccess(recievedposts,
//					OnPostsRecieved.class);
//		} else if (action == WebServiceRequestInfo.ACTION_GETMORE) {
//			actiongetMore(recievedposts);
//			notifyEntityListReceviedSuccess(recievedposts,
//					OnPostsRecieved.class);
//		}
//	}
//
//	void notifyUpdateFinished() {
//		App.getInstance().runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				for (UiListener listener : getListeners()) {
//					if (listener instanceof OnUpdateFinished)
//						((OnUpdateFinished) listener).onUpdateFinished();
//				}
//			}
//		});
//	}
//
//	// void notifyPostsRecievedFailed(final AppException exc) {
//	// App.getInstance().runOnUiThread(new Runnable() {
//	//
//	// @Override
//	// public void run() {
//	// for (UiListener listener : listeners) {
//	// if (listener instanceof OnPostsRecieved)
//	// ((OnPostsRecieved) listener).onFailed(exc);
//	// }
//	// }
//	// });
//	// }
//
//	// void notifyPostsRecieved(final List<PostViewModel> posts) {
//	// App.getInstance().runOnUiThread(new Runnable() {
//	//
//	// @Override
//	// public void run() {
//	// for (UiListener listener : listeners) {
//	// if (listener instanceof OnPostsRecieved)
//	// ((OnPostsRecieved) listener).onSuccess(posts);
//	// }
//	// }
//	// });
//	// }
//
//	/**
//	 * @param methodName
//	 * @param userId
//	 * @param n
//	 * @param lasrRequestTime
//	 * @param timeFilter
//	 */
//	private WALLPosts getPostsFromServer(WebServiceRequestInfo info)
//			throws AppException {
//		WALLPosts posts = getObject(preparePostsParameters(info),
//				info.getMethodName(), WALLPosts.class);
//		return posts;
//	}
//
//	/**
//	 * @param info
//	 * @return
//	 */
//	private Map<String, Object> preparePostsParameters(
//			WebServiceRequestInfo info) {
//		Map<String, Object> params = new HashMap<String, Object>();
//		if (info.getType() == GET_POSTS) {
//			params.put(Params.Wall.USER_ID, info.getUserId());
//			params.put(Params.Wall.NUM_OF_POSTS, info.getN());
//			params.put(Params.Wall.LAST_REQUEST_NUM, info.getLasrRequestTime());
//			params.put(Params.Wall.TIME_FILTER, info.getTimeFilter());
//			if (info.getCountryList() != null
//					&& !info.getCountryList().isEmpty())
//				params.put(Params.Wall.COUNTRYLIST, info.getCountryList());
//			if (info.getMethodName().equalsIgnoreCase("wall/getWall"))
//				params.put(Params.Wall.FAVO_POST_FLAG,
//						info.getFavoritePostsFlag());
//			return params;
//		} else if (info.getType() == GET_TAGS) {
//			params.put(Params.Wall.TAG_ID, info.getTagId());
//			params.put(Params.Wall.NUM_OF_POSTS, info.getN());
//			params.put(Params.Wall.LAST_REQUEST_NUM, info.getLasrRequestTime());
//			params.put(Params.Wall.TIME_FILTER, info.getTimeFilter());
//			return params;
//		} else if (info.getType() == GET_USER_POSTS) {
//			params.put(Params.Wall.USER_ID, info.getUserId());
//			params.put(Params.Wall.FAN_ID, info.getFanId());
//			params.put(Params.Wall.NUM_OF_POSTS, info.getN());
//			params.put(Params.Wall.LAST_REQUEST_NUM, info.getLasrRequestTime());
//			params.put(Params.Wall.TIME_FILTER, info.getTimeFilter());
//			return params;
//		} else
//			return null;
//	}
//
//	/**
//	 * @param recievedposts2
//	 */
//	protected void actiongetMore(final List<PostViewModel> tempPosts) {
//		App.getInstance().runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				if (posts.size() >= 1)
//					posts.remove(posts.size() - 1);
//				posts.addAll(tempPosts);
//				posts.add(EMPTY_POST);
//			}
//		});
//
//	}
//
//	/**
//	 * @param recievedposts
//	 */
//	protected void addUpdatesToListDataStructure(
//			final List<PostViewModel> recievedposts) {
//
//		if (recievedposts.size() >= NUM_OF_POSTS) {
//			App.getInstance().runOnUiThread(new Runnable() {
//
//				@Override
//				public void run() {
//					posts.clear();
//					posts.addAll(recievedposts);
//					posts.add(EMPTY_POST);
//				}
//			});
//		}
//		// check for duplicate
//		else if (recievedposts.size() < NUM_OF_POSTS
//				&& recievedposts.size() > 0) {
//			List<PostViewModel> copyOfHomePosts = new ArrayList<PostViewModel>();
//			copyOfHomePosts.addAll(posts);
//			final List<PostViewModel> afterUpdate = handleData(recievedposts,
//					copyOfHomePosts);
//			App.getInstance().runOnUiThread(new Runnable() {
//
//				@Override
//				public void run() {
//					posts.clear();
//					posts.addAll(afterUpdate);
//					posts.add(EMPTY_POST);
//				}
//			});
//
//		}
//
//	}
//
//	/**
//	 * 
//	 */
//	private List<PostViewModel> handleData(List<PostViewModel> recievedposts,
//			List<PostViewModel> copyOfHomePosts) {
//		copyOfHomePosts.remove(copyOfHomePosts.size() - 1);
//		for (int x = 0; x < recievedposts.size(); x++) {
//			for (int j = 0; j < posts.size() - 1; j++) {
//				if (recievedposts.get(x).getPostId() == copyOfHomePosts.get(j)
//						.getPostId()) {
//					found = true;
//					copyOfHomePosts.remove(j);
//					copyOfHomePosts.add(j, recievedposts.get(x));
//					break;
//				}
//			}
//			if (!found)
//				copyOfHomePosts.add(0, recievedposts.get(x));
//		}
//		return copyOfHomePosts;
//	}
//
//	/**
//	 * @param currentRequestTime
//	 */
//	protected void saveCurrentTime(final long currentRequestTime) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					InternalFileSaveDataLayer.saveObject(context, CURRENT_TIME,
//							currentRequestTime);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	public long getCurrentTime() {
//		long currentTime = 0;
//		try {
//			currentTime = (Long) InternalFileSaveDataLayer.getObject(context,
//					CURRENT_TIME);
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//		return currentTime;
//	}
//
//	public void clearTimes() {
//		InternalFileSaveDataLayer.deleteObjectFile(context.getFilesDir() + "/"
//				+ CURRENT_TIME);
//		InternalFileSaveDataLayer.deleteObjectFile(context.getFilesDir() + "/"
//				+ LastUpdateTime);
//	}
//
//	/**
//	 * @param lastUpdateTime
//	 */
//	private void saveLastUpdateTime(final long lastUpdateTime) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					InternalFileSaveDataLayer.saveObject(context,
//							LastUpdateTime, lastUpdateTime);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	public long getLastUpdateTime() {
//		long lastUpdateTime = 0;
//		try {
//			lastUpdateTime = (Long) InternalFileSaveDataLayer.getObject(
//					context, LastUpdateTime);
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//		return lastUpdateTime;
//	}
//
//	public void clearRecieved() {
//		if (Recievedposts != null)
//			Recievedposts.clear();
//	}
//
//	public void clearData() {
//		posts.clear();
//	}
//
//	/**
//	 * @param posts
//	 */
//	private void getLastUpdateTime(List<PostViewModel> posts) {
//		if (!posts.isEmpty()) {
//			PostViewModel post = posts.get(posts.size() - 1);
//			saveLastUpdateTime(post.getLastUpdateTime());
//		}
//	}
//
//	// --------------------RePost------------------------------------
//
//	public void handleRepost(final int userId, final int postId) {
//		final Action action = new Action(ActionType.REPOST, postId);
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					if (needsProcessing(action)) {
//						notifyActionStarted(action);
//						performRepost(userId, postId);
//						notifyActionDone(action);
//						onRepostSuccess(postId);
//					}
//				} catch (Exception e) {
//					notifyActionDone(action);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// onRepostFailed(AppException.getAppException(e));
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	/**
//	 * @param type
//	 * @param userId
//	 * @param postId
//	 */
//	protected void performRepost(int userId, int postId) throws AppException {
//		Map<String, Object> parametrs = new HashMap<String, Object>();
//		parametrs.put(Params.Wall.USER_ID, userId);
//		parametrs.put(Params.Wall.POST_ID, postId);
//
//		BaseResponse response;
//		try {
//			response = submit(parametrs, URLs.Methods.REPOST);
//			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
//					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
//				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
//					throw new AppException(
//							response.getValidationRule().errorMessage);
//				else
//					throw new AppException(
//							BaseResponse.getExceptionType(response.getStatus()));
//			}
//		} catch (AppException e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * @param appException
//	 */
//	protected void onRepostFailed(AppException appException) {
//		// TODO Auto-generated method stub
//
//	}
//
//	/**
//	 * 
//	 */
//	protected void onRepostSuccess(int postId) {
//		final int index = getpostPosition(postId);
//		final PostViewModel post = getPostForId(postId);
//		post.setRePosted(true);
//		App.getInstance().runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
//				posts.remove(index);
//				posts.add(index, post);
//			}
//		});
//	}
//
//	// -----------------------------------------------------------------
//
//	// --------------------------FOLLOW----------------------------------
//	public void handleFollow(final int currentUserId, final int otherUserId,
//			final int postId) {
//		final Action action = new Action(ActionType.FOLLOW, postId);
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					if (needsProcessing(action)) {
//						notifyActionStarted(action);
//						performFollow(currentUserId, otherUserId);
//						notifyActionDone(action);
//						onFollowSuccess(postId , otherUserId);
//					}
//				} catch (Exception e) {
//					notifyActionDone(action);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// onFollowFailed(AppException.getAppException(e));
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	/**
//	 * @param currentUserId
//	 * @param otherUserId
//	 */
//	protected void performFollow(int currentUserId, int otherUserId)
//			throws AppException {
//		Map<String, Object> parametrs = new HashMap<String, Object>();
//		parametrs.put(Params.Wall.USER_ID, currentUserId);
//		parametrs.put(Params.Wall.POST_ID, otherUserId);
//
//		BaseResponse response;
//		try {
//			response = submit(parametrs, URLs.Methods.REPOST);
//			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
//					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
//				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
//					throw new AppException(
//							response.getValidationRule().errorMessage);
//				else
//					throw new AppException(
//							BaseResponse.getExceptionType(response.getStatus()));
//			}
//		} catch (AppException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * @param appException
//	 */
//	protected void onFollowFailed(AppException appException) {
//		// TODO Auto-generated method stub
//
//	}
//
//	/**
//	 * 
//	 */
//	protected void onFollowSuccess(int postId , final int userId) {
//		final int index = getpostPosition(postId);
//		final PostViewModel post = getPostForId(postId);
//		post.setFollowing(true);
//		App.getInstance().runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				for(UiListener listener : getListeners()){
//					if(listener instanceof OnFollowSuccess){
//						((OnFollowSuccess)listener).onFollowSuccess(userId);
//					}
//				}
//				
//				Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
//				posts.remove(index);
//				posts.add(index, post);
//			}
//		});
//	}
//
//	// ---------------------------------------------------------
//
//	// -----------------------Like -----------------------------
//
//	public void handleLike(final int type, final int userId, final int postId) {
//		App.getInstance().runInBackground(new Runnable() {
//			final Action action = new Action(ActionType.LIKE, postId);
//
//			@Override
//			public void run() {
//				try {
//					if (needsProcessing(action)) {
//						notifyActionStarted(action);
//						performLike(type, userId, postId);
//						notifyActionDone(action);
//						onLikeProcessSuccess(postId, type);
//					}
//				} catch (Exception e) {
//					notifyActionDone(action);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// onLikeProcessFailed(AppException.getAppException(e));
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	/**
//	 * @param type
//	 */
//	private void performLike(int type, int userId, int postId)
//			throws AppException {
//		Map<String, Object> parametrs = new HashMap<String, Object>();
//		parametrs.put(Params.Wall.USER_ID, userId);
//		parametrs.put(Params.Wall.POST_ID, postId);
//
//		if (type == LIKE_POST) {
//			BaseResponse response = submit(parametrs, URLs.Methods.LIKE_POST);
//			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
//					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
//				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
//					throw new AppException(
//							response.getValidationRule().errorMessage);
//				else
//					throw new AppException(
//							BaseResponse.getExceptionType(response.getStatus()));
//			}
//		} else if (type == DISLIKE_POST) {
//			BaseResponse response = submit(parametrs, URLs.Methods.DISLIKE_POST);
//			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
//					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
//				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
//					throw new AppException(
//							response.getValidationRule().errorMessage);
//				else
//					throw new AppException(
//							BaseResponse.getExceptionType(response.getStatus()));
//			}
//		}
//	}
//
//	/**
//	 * @param e
//	 */
//	protected void onLikeProcessFailed(AppException e) {
//		// don't do anything
//
//	}
//
//	/**
//	 * 
//	 */
//	protected void onLikeProcessSuccess(int postId, int type) {
//		final int index = getpostPosition(postId);
//		final PostViewModel post = getPostForId(postId);
//		if (type == LIKE_POST) {
//			post.setLiked(true);
//			post.setLikeCount(post.getLikeCount() + 1);
//		} else {
//			post.setLiked(false);
//			post.setLikeCount(post.getLikeCount() - 1);
//		}
//		App.getInstance().runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
//				posts.remove(index);
//				posts.add(index, post);
//			}
//		});
//	}
//
//	// ---------------------------------------------------------------------
//
//	private int getpostPosition(int postId) {
//		for (int x = 0; x < posts.size() - 1; x++) {
//			if (postId == posts.get(x).getPostId())
//				return x;
//		}
//		return 0;
//	}
//
//	public PostViewModel getPostForId(int postId) {
//		for (int x = 0; x < posts.size() - 1; x++) {
//			if (postId == posts.get(x).getPostId())
//				return posts.get(x);
//		}
//		return null;
//	}
//
//	// -------------------------INITIALSTATE-------------------------------
//
//	public boolean checkIfInitialState() {
//		return new File(context.getFilesDir() + "/" + INITIAL_STATE_HOME)
//				.exists();
//	}
//
//	public void saveInitialHomeStateEnded() {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					InternalFileSaveDataLayer.saveObject(context,
//							INITIAL_STATE_HOME, "");
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	public void fireMenuInitialState() {
//		if (!checkIfInitialState()) {
//			Timer mTimer = new Timer();
//			mTimer.schedule(new TimerTask() {
//
//				@Override
//				public void run() {
//					BaseActivity.homeInitialMenuState();
//					saveInitialHomeStateEnded();
//				}
//			}, 1000);
//		}
//	}
//
//	// -------------------------GET_TOP_TEN-----------------
//
//	public void getTopTen(final int userId, final String methodName) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				Map<String, Object> params = new HashMap<String, Object>();
//				params.put(Params.User.USER_ID, userId);
//				try {
//					List<TopTenModel> seasons = getObjectList(methodName,
//							params, TopTenModel.class);
//					notifyEntityListReceviedSuccess(seasons,
//							OnSeasonReceived.class);
//					// onSeasonsRecieved(seasons);
//				} catch (Exception e) {
//					notifyRetrievalException(AppException.getAppException(e),
//							OnSeasonReceived.class);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// onSeasonsFailed(AppException.getAppException(e));
//					e.printStackTrace();
//				}
//			}
//		});
//
//	}
//
//	// /**
//	// * @param appException
//	// */
//	// protected void onSeasonsFailed(final AppException appException) {
//	// App.getInstance().runOnUiThread(new Runnable() {
//	//
//	// @Override
//	// public void run() {
//	// for (UiListener listener : listeners) {
//	// if (listener instanceof OnSeasonReceived)
//	// ((OnSeasonReceived) listener)
//	// .onException(appException);
//	// }
//	// }
//	// });
//	// }
//	//
//	// /**
//	// * @param seasons
//	// */
//	// protected void onSeasonsRecieved(final List<TopTenModel> seasons) {
//	// App.getInstance().runOnUiThread(new Runnable() {
//	//
//	// @Override
//	// public void run() {
//	// for (UiListener listener : listeners) {
//	// if (listener instanceof OnSeasonReceived)
//	// ((OnSeasonReceived) listener).onSuccess(seasons);
//	// }
//	// }
//	// });
//	// }
//
//	public void get_Tag_Posts(final int tagId, final int n,
//			final int lasrRequestTime, final int timeFilter) {
//		methodName = URLs.Methods.GET_TAG_POSTS;
//		App.getInstance().runInBackground(new Runnable() {
//			public void run() {
//				try {
//					WALLPosts posts = PerformGetTagList(methodName, tagId, n,
//							lasrRequestTime, timeFilter);
//					notifyEntityListReceviedSuccess(posts.getPosts(),
//							OnPostsRecieved.class);
//					// notifyPostsRecieved(posts.getPosts());
//				} catch (Exception e) {
//					notifyRetrievalException(AppException.getAppException(e),
//							OnPostsRecieved.class);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// notifyPostsRecievedFailed(AppException
//					// .getAppException(e));
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	private WALLPosts PerformGetTagList(String methodName, int tagId, int n,
//			int lasrRequestTime, int timeFilter) throws AppException {
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("tagId", tagId);
//		params.put("n", n);
//		params.put("lastRequestTime", lasrRequestTime);
//		params.put("timeFilter", timeFilter);
//		WALLPosts posts = getObject(params, methodName, WALLPosts.class);
//		return posts;
//	}
//
//	/** Comments Part **/
//	public void addComment(final UserComment comment) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					BaseResponse response = submit(comment, URLs.Methods.ADD_COMMENT, UserComment.class);
////					notifyCommentId(response.getData());
//					comment.setCommentId(Integer.valueOf(response.getCommentId()));
//					notifyCommentAdded(comment);
//					final int index = getpostPosition(comment.getPostId());
//					final PostViewModel temppost = getPostForId(comment
//							.getPostId());
//					temppost.setCommentCount(temppost.getCommentCount() + 1);
//					App.getInstance().runOnUiThread(new Runnable() {
//
//						@Override
//						public void run() {
//							Toast.makeText(context, "Done", Toast.LENGTH_SHORT)
//									.show();
//							posts.remove(index);
//							posts.add(index, temppost);
//						}
//					});
//
//				} catch (AppException e) {
//					notifyRetrievalException(AppException.getAppException(e),
//							OnCommentsListReceived.class);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// onException(e);
//					e.printStackTrace();
//				}
//			}
//
//		});
//	}
//
//	protected void notifyCommentAdded(final UserComment comment) {
//		App.getInstance().runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				for (UiListener temp : getListeners())
//					if (temp instanceof OnCommentsListReceived)
//						((OnCommentAdded) temp).commentAdded(comment);
//			}
//		});
//	}
//	// public void notifyCommentsCount(final int commentcount) {
//	// App.getInstance().runOnUiThread(new Runnable() {
//	// @Override
//	// public void run() {
//	// for (UiListener temp : getListeners())
//	// if (temp instanceof OnCommentScreenFinish)//TODO
//	// ((OnCommentScreenFinish) temp).getCount(commentcount);
//	// }
//	// });
//	// }
//
//	// private void onException(final AppException ex) {
//	// App.getInstance().runOnUiThread(new Runnable() {
//	// @Override
//	// public void run() {
//	// for (UiListener temp : listeners)
//	// if (temp instanceof OnCommentsListReceived)
//	// ((SimpleUIListsner) temp).onException(ex);
//	// }
//	// });
//	// }
//
//	// get comments
//
//
//	public List<UserComment> getCommentsFromServer(int postId, int pageNum)
//			throws AppException {
//		final Map<String, Object> params = prepareGetCommetnsParams(postId,
//				pageNum);
//		ArrayList<UserComment> comments = (ArrayList<UserComment>) getObjectList(
//				URLs.Methods.GET_COMMENTS, params, UserComment.class);
//		return comments;
//	}
//
//	public void getComments(final int photoId, final int pageNo) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					List<UserComment> comments = getCommentsFromServer(photoId,
//							pageNo);
//					handleCommentsResponse(comments, 0);
//				} catch (Exception e) {
//					// handle exception
//					notifyRetrievalException(AppException.getAppException(e),
//							OnCommentsListReceived.class);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// onException(AppException.getAppException(e));
//					e.printStackTrace();
//				}
//			}
//
//		});
//	}
//
//	/**
//	 * @param photoId
//	 * @param pageNo
//	 * @return
//	 */
//	private Map<String, Object> prepareGetCommetnsParams(int photoId, int pageNo) {
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put(Params.Wall.POST_ID, photoId);
//		params.put(Params.Wall.PAGE_NUM, pageNo);
//		return params;
//	}
//
//	private void handleCommentsResponse(final List<UserComment> comments,
//			final int pageNo) {
//		// if(comments == null || comments.size() ==0)
//		// return;
//		if (!comments.isEmpty()) {
//			App.getInstance().runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//					for (UiListener temp : getListeners())
//						if (temp instanceof OnCommentsListReceived)
//							((OnCommentsListReceived) temp)
//									.onEntitiesListReceived(comments, pageNo);
//				}
//			});
//		}
//	}
//
//	public void deleteComment(final int postid , final int commentId) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					performeDelete(postid , commentId);
//				} catch (Exception e) {
//					e.printStackTrace();
//					notifyRetrievalException(AppException.getAppException(e),
//							OnCommentDeleted.class);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// onDeleteException(AppException.getAppException(e));
//				}
//			}
//
//			private void performeDelete(int postid , int commentId) throws AppException {
//				Map<String, Object> parametrs = new HashMap<String, Object>();
//				parametrs.put(Params.Wall.POST_ID, postid);
//				parametrs.put(Params.Wall.COMMENT_ID, commentId);
//					BaseResponse response = submit(parametrs,
//							URLs.Methods.DELETECOMMENT);
//					if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
//						notifyCommentDeleted(postid , commentId);
//					if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
//						if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
//							throw new AppException(
//									response.getValidationRule().errorMessage);
//						else
//							throw new AppException(BaseResponse
//									.getExceptionType(response.getStatus()));
//					}
//			}
//		});
//	}
//
//	protected void notifyCommentDeleted(final int postid, final int commentId) {
//			App.getInstance().runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//					for (UiListener temp : getListeners())
//						if (temp instanceof OnCommentDeleted)
//							((OnCommentDeleted) temp).onCommentDeleted(postid, commentId);
//				}
//			});
//	}
//	/******* End Of Comments Part **********/
//
//
//	/*** Report Post ***/
//	public void ReportPost(final int userId, final int postId) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					performReport(userId, postId);
//					onReportSuccess();
//				} catch (Exception e) {
//					onReportFailed(AppException.getAppException(e));
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	/**
//	 * @param userId
//	 * @param postId
//	 */
//	protected void performReport(int userId, int postId) throws AppException {
//
//		Calendar c = Calendar.getInstance();
//		Date dateToSend = c.getTime();
//
//		Map<String, Object> parametrs = new HashMap<String, Object>();
//		parametrs.put(Params.Wall.USER_ID, userId);
//		parametrs.put(Params.Wall.POST_ID, postId);
//		parametrs.put(Params.Wall.DATE, dateToSend.getTime());
//
//		BaseResponse response;
//		try {
//			response = submit(parametrs, URLs.Methods.REPORT);
//			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
//					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
//				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
//					throw new AppException(
//							response.getValidationRule().errorMessage);
//				else
//					throw new AppException(
//							BaseResponse.getExceptionType(response.getStatus()));
//			}
//		} catch (AppException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//	protected void onReportFailed(AppException appException) {
//		// TODO Auto-generated method stub
//
//	}
//
//	protected void onReportSuccess() {
//		// TODO Auto-generated method stub
//
//	}
//
//	/**** End Of Report Post ****/
//
//	/*** Delete Post ***/
//	public void deletePost(final int post_id) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					performeDelete(post_id);
//					onDeleteResponse();
//				} catch (Exception e) {
//					e.printStackTrace();
//					notifyRetrievalException(AppException.getAppException(e),
//							OnSuccessVoidListener.class);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					// onDeleteException(AppException.getAppException(e));
//				}
//			}
//
//			private void onDeleteResponse() {
//				App.getInstance().runOnUiThread(new Runnable() {
//					@Override
//					public void run() {
//						for (UiListener temp : getListeners())
//							if (temp instanceof OnSuccessVoidListener)
//								((OnSuccessVoidListener) temp).onSuccess();
//					}
//				});
//			}
//
//			private void performeDelete(int post_id) {
//				Map<String, Object> parametrs = new HashMap<String, Object>();
//				parametrs.put(Params.IMAGE_DETAILS.POST_ID, post_id);
//				try {
//					BaseResponse response = submit(parametrs,
//							URLs.Methods.DELETEPOST);
//					if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
//						notifyPostDeleted(post_id);
//					if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
//						if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
//							throw new AppException(
//									response.getValidationRule().errorMessage);
//						else
//							throw new AppException(BaseResponse
//									.getExceptionType(response.getStatus()));
//					}
//				} catch (AppException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	public void notifyPostDeleted(final int post_id) {
//		App.getInstance().runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				for (UiListener listener : getListeners()) {
//					if (listener instanceof OnPostDeletedListener) {
//						((OnPostDeletedListener) listener)
//								.onPostDeleted(post_id);
//					}
//				}
//			}
//		});
//	}
//
//	/*** End Of Delete Post ***/
//
//	public List<PostViewModel> getFanPosts(PostViewModel post) {
//		int fanId = post.getUserId();
//		// Fan fan = null;
//		// for (Fan thisFan : postsStorage.keySet()) {
//		// if (fanId == thisFan.getUserId())
//		// fan = thisFan;
//		// }
//		// if (fan == null)
//		// return null;
//		// return postsStorage.get(fan);
//		List<PostViewModel> tempPosts = new ArrayList<PostViewModel>();
//		for (PostViewModel thisPost : posts) {
//			if (thisPost.getUserId() == fanId)
//				tempPosts.add(thisPost);
//		}
//		// Collections.sort(tempPosts);
//		return tempPosts;
//	}
//
//	public List<Fan> getFans() {
//		return new ArrayList<Fan>(postsStorage.keySet());
//	}
//
//	public static boolean isEmptyPost(PostViewModel post) {
//		return EMPTY_POST.equals(post);
//	}
//
//	public List<PostViewModel> getCurrentPosts() {
//		return new ArrayList<PostViewModel>(posts);
//	}
//
//	public PostViewModel getEmptyPost() {
//		return EMPTY_POST;
//	}
//
//	/**
//	 * @param object
//	 * @param chooseFavo
//	 */
//	public void applyFilter(List<Integer> countries, boolean chooseFavo,
//			int action, String methodName) {
//		WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(action,
//				UserManager.getInstance().getCurrentUserId(),
//				HomeManager.NUM_OF_POSTS, HomeManager.getInstance()
//						.getLastUpdateTime(),
//				WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
//				WebServiceRequestInfo.GET_POSTS, methodName, countries,
//				chooseFavo ? WebServiceRequestInfo.TURN_ON_FILTER
//						: WebServiceRequestInfo.TURN_OFF_FILTER);
//		HomeManager.getInstance().handlePosts(requestInfo);
//	}
//
//	public void getLikeData(final int userId, final int postId) {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				Map<String, Object> parametrs = new HashMap<String, Object>();
//				parametrs.put(Params.Wall.USER_ID, userId);
//				parametrs.put(Params.Wall.POST_ID, postId);
//
//				try {
//					ArrayList<PostLike> likes = (ArrayList<PostLike>) getObjectList(
//							URLs.Methods.GET_LIKE_DETAILS, parametrs,
//							PostLike.class);
//					notifyEntityListReceviedSuccess(likes, OnLikeRecieved.class);
//					// notifyLikesListRecieved(likes);
//				} catch (Exception e) {
//					notifyRetrievalException(AppException.getAppException(e),
//							OnLikeRecieved.class);
//					// notifyLikeRecieveException(AppException.getAppException(e));
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	public boolean isInitialPhotoDetails() {
//		return SharedPrefrencesDataLayer.getBooleanPreferences(App
//				.getInstance().getApplicationContext(),
//				PHOTO_DETAILS_INITIAL_STATE_KEY, false);
//	}
//
//	public void markPhotoDetailsInitialized() {
//		SharedPrefrencesDataLayer
//				.saveBooleanPreferences(App.getInstance()
//						.getApplicationContext(),
//						PHOTO_DETAILS_INITIAL_STATE_KEY, true);
//	}
//
//	public void updateDialog(final List<Integer> countries) {
//		App.getInstance().runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				for (UiListener listener : getListeners()) {
//					if (listener instanceof UpdateDialogListener) {
//						((UpdateDialogListener) listener)
//								.countryList(countries);
//					}
//				}
//			}
//		});
//	}
//
//	public void getCountryList() {
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					List<Country> countries = getObjectList(
//							URLs.Methods.GET_COUNTRIES,
//							new HashMap<String, Object>(), Country.class);
//					Collections.sort(countries, new Comparator<Country>() {
//
//						@Override
//						public int compare(Country lhs, Country rhs) {
//							// TODO Auto-generated method stub
//							return lhs.getCountryName().compareTo(
//									rhs.getCountryName());
//						}
//					});
//					notifyEntityListReceviedSuccess(countries,
//							OnCountriesRecieved.class);
//				} catch (Exception e) {
//					notifyRetrievalException(AppException.getAppException(e),
//							OnCountriesRecieved.class);
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//}
