package com.tawasol.barcelona.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.business.BusinessManager;
import com.tawasol.barcelona.business.BusinessManager.Action.ActionType;
import com.tawasol.barcelona.data.cache.NotificationsTable;
import com.tawasol.barcelona.data.cache.SeasonsTable;
import com.tawasol.barcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.barcelona.data.cache.StudioFoldersTable;
import com.tawasol.barcelona.data.cache.StudioPhotosTable;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.data.connection.URLs;
import com.tawasol.barcelona.entities.Notification;
import com.tawasol.barcelona.entities.WALLPosts;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnFriendRequestResponseListener;
import com.tawasol.barcelona.listeners.OnNotificationsReceivedListener;
import com.tawasol.barcelona.listeners.OnPostReceived;
import com.tawasol.barcelona.listeners.UiListener;
import com.tawasol.barcelona.responses.BaseResponse;
import com.tawasol.barcelona.ui.activities.BaseActivity;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.NetworkingUtils;

/**
 * @author Mohga
 * */

public class NotificationsManager extends BusinessManager<Notification> {

	private static final String LAST_REQUEST_TIME_NOTIFICATIONS_KEY = "com.tawasol.barcelona.managers.lastRequestTime";
	private static NotificationsManager instance;
	private long lastRequestTime = 0;
	ArrayList<Notification> requestsList = new ArrayList<Notification>();
	private static int pageNo = 1;
	ArrayList<Notification> notifications;

	// constructor
	protected NotificationsManager() {
		super(Notification.class);
	}

	// get instance of the manager
	public static NotificationsManager getInstance() {
		if (instance == null)
			instance = new NotificationsManager();
		return instance;
	}

	public List<Notification> getReqestsList() {
		return requestsList;
	}

	/** Get Notifications List */

	public void getNotificationsFromServer() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {

				Action notificationsAction = new Action(
						ActionType.GET_NOTIFICATIONS, getLastRequestTime());
				if (!needsProcessing(notificationsAction))
					return;

				try {
					// adding get notification action to actions list
					notifyActionStarted(notificationsAction);

					// get the list of notifications
					notifications = (ArrayList<Notification>) getObjectList(
							URLs.Methods.GET_NOTIFICATIONS,
							prepareNotificationsParams(pageNo));
//					notifications.add(new Notification(1111, 711, "", 0, "", 0,
//							0, 8, "", false));
					notifications = HandleStudioNotificationIFExist(notifications);
					// cache the returned notifications
					cachNotifications(notifications);

					// remove the notifications action from action list
					notifyActionDone(notificationsAction);

					// notify the UI if any listener registered
					notifyEntityListReceviedSuccess(notifications,
							OnNotificationsReceivedListener.class);

					pageNo += 1;
					getNotificationsFromServer();
				} catch (Exception e) {
					pageNo = 1;

					// validate the returned list and save the last request time
					if (notifications != null && notifications.size() > 0)
						saveLastRequestTime(notifications.get(
								notifications.size() - 1).getTime());
					notifyActionDone(notificationsAction);
					e.printStackTrace();
					if (!(e instanceof AppException))
						e = AppException.getAppException(e);
					notifyRetrievalException((AppException) e,
							OnNotificationsReceivedListener.class);
				}

			}
		});
	}

	private Map<String, Object> prepareNotificationsParams(int pageNo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Notifications.USER_ID,
				String.valueOf(UserManager.getInstance().getCurrentUserId()));
		params.put(Params.Notifications.LAST_REQUEST_TIME,
				String.valueOf(getLastRequestTime()));
		params.put(Params.Notifications.PAGE_NO, String.valueOf(pageNo));
		return params;
	}

	/** check if there is Notification of Type (Studio Library) "8" */
	public ArrayList<Notification> HandleStudioNotificationIFExist(
			ArrayList<Notification> notifications) {
		for (int i = 0; i < notifications.size(); i++) {
			if (notifications.get(i).getNotificationType() == Notification.STUDIO_NOTIFICATIONS) {
				/** reset tables */
				// 1- reset seasons table
				if (SeasonsTable.getInstance().getCount() > 0)
					SeasonsTable.getInstance().deleteAll();

				// 2- reset Photos table
				if (StudioPhotosTable.getInstance().getCount() > 0)
					StudioPhotosTable.getInstance().deleteAll();

				// 3- reset Folders table
				if (StudioFoldersTable.getInstance().getCount() > 0)
					StudioFoldersTable.getInstance().deleteAll();

				/** get Studio data from Server */
				if (NetworkingUtils.isNetworkConnected(App.getInstance()
						.getApplicationContext())) {
					// 4- get Folders from Server and cache it
					StudioManager.getInstance().getFoldersFromServer();

					// 5- get Seasons from Server and cache it
					StudioManager.getInstance().getSeasonsFromServer();

					// remove the notification from the list :)) no need for it
					// again
					notifications.remove(i);
					break;

				}

			}

		}
		return notifications;
	}

	private long getLastRequestTime() {
		long lastRequestTime_ = 0;

		lastRequestTime_ = (Long) SharedPrefrencesDataLayer.getLongPreferences(
				App.getInstance().getApplicationContext(),
				LAST_REQUEST_TIME_NOTIFICATIONS_KEY, 0);
		return lastRequestTime_;
	}

	public void saveLastRequestTime(long timeStamp) {
		lastRequestTime = timeStamp;
		SharedPrefrencesDataLayer.saveLongPreferences(App.getInstance()
				.getApplicationContext(), LAST_REQUEST_TIME_NOTIFICATIONS_KEY,
				lastRequestTime);
	}

	private void cachNotifications(List<Notification> notifications) {
		NotificationsTable.getInstance().insert(notifications);
	}

	public List<Notification> getCachedFilteredNotifications(
			int notificationType) {
		return NotificationsTable.getInstance().getFilteredNotifications(
				notificationType);
	}

	public List<Notification> getCachedFavoriteNotifications() {
		return NotificationsTable.getInstance().getFavoriteNotifications();
	}

	public List<Notification> getFriendRequestsNotifications() {
		requestsList = /* (ArrayList<Notification>) getrequested(); */(ArrayList<Notification>) getCachedFilteredNotifications(Notification.FRIEND_REQUEST_NOTIFICATIONS);
		return requestsList;
	}

	public void setNotificationRead(int notificationId) {
		NotificationsTable.getInstance().setRead(notificationId);
	}

	/*
	 * public void setFavTabReab() {
	 * NotificationsTable.getInstance().setFavRead(); }
	 */
	public List<Notification> getrequested() {
		List<Notification> nots = new ArrayList<Notification>();
		nots.add(new Notification(99, 1546, "Mohga", 711, "Osama", 1423481771,
				758, Notification.FRIEND_REQUEST_NOTIFICATIONS, "", false));
		nots.add(new Notification(100, 1547, "Mohga", 711, "Osama", 1423580553,
				759, Notification.FRIEND_REQUEST_NOTIFICATIONS, "", true));
		return nots;
	}

	// --------------------------friend Part------------------------------

	public void handleFriend(final int userId, final int fanId,
			final boolean friend) {
		// this.requestsList = requests;
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, fanId);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performFriend(userId, fanId, friend);
						notifyActionDone(action);
					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFriendRequestResponseListener.class);
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	protected void performFriend(int userId, final int senderId, boolean friend)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, senderId);
		if (friend)
			parametrs.put(Params.Fans.FRIEND, 1);
		else
			parametrs.put(Params.Fans.FRIEND, 0);

		final BaseResponse response = submit(parametrs,
				URLs.Methods.CHANGE_FRIEND);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			

			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					for (UiListener listener : getListeners()) {
						if (listener instanceof OnFriendRequestResponseListener) {
							((OnFriendRequestResponseListener) listener)
									.onSuccess(senderId);
						}
					}
				}
			});

		}

		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else {
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));

			}
		}
	}

	/**
	 * get Post Details
	 * 
	 * @param postId
	 * */
	public void getPostDetails(final int postId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					executeGetPost(postId);
				} catch (AppException e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnPostReceived.class);
				}
			}
		});
	}

	private void executeGetPost(int postId) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Wall.POST_ID, postId);
		params.put(Params.Wall.USER_ID, UserManager.getInstance()
				.getCurrentUserId());

		WALLPosts post = getObject(params, URLs.Methods.GET_POST,
				WALLPosts.class);
		notifyEntityReceviedSuccess(post, OnPostReceived.class);
	}

}
