package com.tawasol.barcelona.managers;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.business.BusinessManager;
import com.tawasol.barcelona.business.BusinessManager.Action.ActionType; 
import com.tawasol.barcelona.data.cache.FriendTable;
import com.tawasol.barcelona.data.cache.MessagesTable;
import com.tawasol.barcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.data.connection.URLs; 
import com.tawasol.barcelona.data.helper.DataHelper;
import com.tawasol.barcelona.entities.ChatContactsEntity;
import com.tawasol.barcelona.entities.ContactEntity;
import com.tawasol.barcelona.entities.DeviceContactModel;
import com.tawasol.barcelona.entities.DeviceContactsEntity;
import com.tawasol.barcelona.entities.FCBPhoto;
import com.tawasol.barcelona.entities.FriendsEntity; 
import com.tawasol.barcelona.entities.FriendsFilterEntity;
import com.tawasol.barcelona.entities.Message; 
import com.tawasol.barcelona.entities.SendMessage; 
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException; 
import com.tawasol.barcelona.listeners.OnChatChangeStatusListener;
import com.tawasol.barcelona.listeners.OnChatContactUpdatedListener;
import com.tawasol.barcelona.listeners.OnChatContactsResponseListener;
import com.tawasol.barcelona.listeners.OnChatDeleteMessageListener;
import com.tawasol.barcelona.listeners.OnChatFriendsResponseListener; 
import com.tawasol.barcelona.listeners.OnChatSendMessageListener;
import com.tawasol.barcelona.listeners.OnChatUpdatedListener;
import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
import com.tawasol.barcelona.listeners.UiListener; 
import com.tawasol.barcelona.responses.BaseResponse;
import com.tawasol.barcelona.ui.fragments.FriendsFragment;
import com.tawasol.barcelona.utils.LanguageUtils;
import com.tawasol.barcelona.utils.NetworkingUtils;
/**
 * @author Turki
 * 
 */
public class ChatManager extends BusinessManager<Message> {
	
	private static final String LAST_TIME_STAMP_CHECKED_KEY = "com.tawasol.barcelona.managers.lastTimeStampChecked";
	private static final String STARTING_LIMIT_CHECKED_KEY  = "com.tawasol.barcelona.managers.startingLimit";
	private static final String INVITE_FRIEND_CHECKED_KEY   = "com.tawasol.barcelona.managers.inviteFriend";
	private static final String RECENT_MSG_CHECKED_KEY      = "com.tawasol.barcelona.managers.recentMsg";
	
	private static ChatManager sInstance;
	private List<ChatContactsEntity> chatContacts;
	private Map<Integer/*Friend ID*/, List<Message>> mChatMap;
	private List<FriendsEntity> contacts;
	private List<Message> latestMessages;
	private long mLastTimeStampChecked; 
	private Map<Integer/*Friend ID*/, List<Message>> mFilterMap;	
	public static boolean noMsg;
	public static ChatManager getInstance() {
		if (sInstance == null)
			sInstance = new ChatManager();
		return sInstance;
	}

	private ChatManager() {
		super(Message.class);
		mChatMap = new HashMap<Integer, List<Message>>();
		contacts = new ArrayList<FriendsEntity>();
		latestMessages  = new ArrayList<Message>();
		mFilterMap = new HashMap<Integer, List<Message>>();
		chatContacts = new ArrayList<ChatContactsEntity>();
	}
	
	private long getLastTimeStampChecked() {
		if (mLastTimeStampChecked == 0)
			mLastTimeStampChecked = SharedPrefrencesDataLayer.getLongPreferences(App.getInstance().getApplicationContext(),
					LAST_TIME_STAMP_CHECKED_KEY, 0);
		return mLastTimeStampChecked;
	}
	
	private void saveLastTimeStampChecked(long timeStamp) {
		mLastTimeStampChecked = timeStamp;
		SharedPrefrencesDataLayer.saveLongPreferences(App.getInstance().getApplicationContext(),
				LAST_TIME_STAMP_CHECKED_KEY, mLastTimeStampChecked);
	}
	
	public List<Message> getMessages(int friendId) {
		if(noMsg){
			if (!mChatMap.containsKey(friendId)) {
				loadMessagesAsync();
			}
		}
		return mChatMap.get(friendId);
	}
	
	private void loadMessagesAsync() {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				List<Message> cachedMsgs = loadMessagesFromDatabase();
				if (cachedMsgs != null) {
					addMessagesToChatMap(cachedMsgs);
					notifyVoidSuccess(OnChatUpdatedListener.class);
				}
				if(NetworkingUtils.isNetworkConnected(App.getInstance().getApplicationContext()))
					loadMessagesFromServer();
			}
		});
	}
	
	private List<Message> loadMessagesFromDatabase() {
		return MessagesTable.getInstance().getAll(); 
	}
	
	private void saveMessagesToDatabase(List<Message> msgs) {
		MessagesTable.getInstance().insert(msgs);
	}
	
	private void saveMessagesToDatabase(Message msgs) {
		MessagesTable.getInstance().insert(msgs);
	}
	/**
	 * Called when no messages are found on the client side or a notification of new messages has arrived.
	 */
	public void loadMessagesFromServer() {
		Action msgRetreivingAction = new Action(ActionType.GET_CHAT_MESSAGES, getLastTimeStampChecked());
		if (!needsProcessing(msgRetreivingAction))
			return;
		
		try {
			notifyActionStarted(msgRetreivingAction);

			List<Message> msgs = getObjectList("chat/loadMessage", 
					getLoadingMessagesParams(UserManager.getInstance().getCurrentUserId()));

			noMsg=false;
			addMessagesToChatMap(msgs);
			saveMessagesToDatabase(msgs);
			/** Added by Turki**/
			if(msgs != null && msgs.size() > 0)
				saveLastTimeStampChecked(msgs.get(msgs.size()-1).getMessageTime());
			notifyActionDone(msgRetreivingAction);
			notifyVoidSuccess(OnChatUpdatedListener.class);
		} catch (Exception e) {
			notifyActionDone(msgRetreivingAction);
			e.printStackTrace();
			if (!(e instanceof AppException))
				e = AppException.getAppException(e);
			notifyRetrievalException((AppException) e, OnChatUpdatedListener.class);
		}
	}
	

	private void addMessagesToChatMap(List<Message> msgs) {
		if(msgs != null && msgs.size() >0){
			for (Message msg : msgs) {
				// False 0 = messages received, True 1 = messages sent
				if (mChatMap.containsKey(msg.getFriendId())) {
					mChatMap.get(msg.getFriendId()).add(msg);
				} else {
					List<Message> tmpList = new ArrayList<Message>();
					tmpList.add(msg);
					mChatMap.put(msg.getFriendId(), tmpList);
				}
			}
		}
	}
	
	private void addMessagesToChatMap(Message msg) {
			// False 0 = messages received, True 1 = messages sent
		if (mChatMap.containsKey(msg.getFriendId())) {
			mChatMap.get(msg.getFriendId()).add(msg);
		} else {
			List<Message> tmpList = new ArrayList<Message>();
			tmpList.add(msg);
			mChatMap.put(msg.getFriendId(), tmpList);
		}
	}
	private Map<String, Object> getLoadingMessagesParams(int currentUserId) {
		Map<String, Object> params = new HashMap<String, Object>(2);
		params.put(Params.Chat.USER_ID, String.valueOf(currentUserId));
		params.put(Params.Chat.LAST_TIME_STAMP_CHECKED, String.valueOf(getLastTimeStampChecked()));
		return params;
	}
	
	/* ============================================================================================== */
	/*								Loading Friends Methods                                           */
	/* ============================================================================================== */
	public boolean isInviteFriendBefor() {
		return SharedPrefrencesDataLayer.getBooleanPreferences(App.getInstance().getApplicationContext(),
					INVITE_FRIEND_CHECKED_KEY, false);
	}
	
	private void setInviteFriendBefor(boolean flag) {
		SharedPrefrencesDataLayer.saveBooleanPreferences(App.getInstance().getApplicationContext(),
				INVITE_FRIEND_CHECKED_KEY, flag);
	}
	
	public List<FriendsEntity> getContacts(){
		return contacts;
	}
	
	public void loadCachedContactsAsync() {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				List<FriendsEntity> cachedContact = loadFriendsFromDatabase();
				if (cachedContact != null) {
					contacts = cachedContact;
					notifyVoidSuccess(OnChatContactUpdatedListener.class);
				}
			}
		});
	}

	private List<FriendsEntity> loadFriendsFromDatabase() {
		return FriendTable.getInstance().getAll();
	}
	
	/** Calling performGetFriends to get chat Friends list. **/
	public void callLoadingFriends() {
		performGetFriends();
	}

	/** Calling loadFriends web service, then notify all with response list and exception if happened. **/
	private void performGetFriends() {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					List<FriendsEntity> list = getFriends();
					notifyEntityListReceviedSuccess(list, OnChatFriendsResponseListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e), OnChatFriendsResponseListener.class);
				}
			}
		});
	}

	/** Build request parameter, the deserialize response to FriendsEntity list. **/
	private List<FriendsEntity> getFriends() throws AppException {
		
		List<FriendsEntity> list = new ArrayList<FriendsEntity>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG                     ,	LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Chat.LOAD_FRIENDS_USER_ID       ,	UserManager.getInstance().getCurrentUserId());
		params.put(Params.Chat.LOAD_FRIENDS_FILTER        , "city");
		params.put(Params.Chat.LOAD_FRIENDS_STARTING_LIMIT, FriendsFragment.mLastStartingLimit);
		
		BaseResponse response = submit(params, URLs.Methods.GET_CHAT_FRIENDS);
		
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			
			FriendsFilterEntity object = DataHelper.deserialize(response.getData(), FriendsFilterEntity.class);
			list = object.getData();
			if(list != null && list.size() > 0){
				setInviteFriendBefor(true);
			}
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response.getStatus()));

		}
		return list;
	}
	
	/* ============================================================================================== */
	/*								Send Message Methods                                              */
	/* ============================================================================================== */


	/** Calling performSendMessage to send chat message. **/
	public void callSendMessage( Message message, FriendsEntity friendObj) {
		performSendMessage(message, friendObj);
	}

	/** Calling sendMessage web service, then notify all with response list and exception if happened. **/
	private void performSendMessage(final Message message, final FriendsEntity friendObj) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					Integer storedMessageID = sendMessage(message,friendObj);
					notifyEntityReceviedSuccess(storedMessageID, OnChatSendMessageListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e), OnChatSendMessageListener.class);
				}
			}
		});
	}

	/** Build request parameter, the deserialize response to storedMessageID. **/
	private Integer sendMessage(Message message, FriendsEntity friendObj) throws AppException {
		Integer storedMessageID = null;
		BaseResponse response;
		message.validateObject(App.getInstance().getApplicationContext());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Chat.SEND_MESSAGE_MY_ID   , UserManager.getInstance().getCurrentUserId());
		params.put(Params.Chat.SEND_MESSAGE_FAN_ID  , /**711**//** fanID**/ message.getFriendId());
		params.put(Params.Chat.SEND_MESSAGE_MSG_TEXT, /**messageText **/ message.getText());
		
		response = submit(params, URLs.Methods.GET_CHAT_MESSAGES);
	
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			
			SendMessage obj = DataHelper.deserialize(response.getData(), SendMessage.class);
			storedMessageID = obj.getStoredMessageID();
			message = new Message(storedMessageID, message.getText(), message.getFriendId(), 1, 1,
					new Date().getTime(), friendObj.getFanName(),
					friendObj.getFanPicture());
			addMessagesToChatMap(message);
			saveMessagesToDatabase(message);
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response.getStatus()));

		}
		return storedMessageID;
	}
	
	/* ============================================================================================== */
	/*								Delete Message Methods                                            */
	/* ============================================================================================== */
	
	
	public void deleteMessageAsync(final int msgID) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try{
					MessagesTable.getInstance().delete(msgID);
					deleteMessageFromServer(msgID);
					onDeleteResponse();
					
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e), OnChatDeleteMessageListener.class);
				}
			}
		});
	}
	
	private void deleteMessageFromServer(final int msgID) throws AppException{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG               , LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Chat.DELETED_MESSAGE_ID   , msgID);
		
		BaseResponse response = submit(params, URLs.Methods.GET_CHAT_DELETE_MESSAGES);

		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response.getStatus()));

		}
	}
	/** Notify all with success response **/
	private void onDeleteResponse() {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for(UiListener listener : getListeners()){
					if(listener instanceof OnSuccessVoidListener){
						((OnSuccessVoidListener)listener).onSuccess();
					}
				}
			}
		});
	}
	/* ============================================================================================== */
	/*								Recent Message Methods                                            */
	/* ============================================================================================== */
	
	public boolean isRecentMsgLoadedBefor() {
		return SharedPrefrencesDataLayer.getBooleanPreferences(App.getInstance().getApplicationContext(),
				RECENT_MSG_CHECKED_KEY, false);
	}
	
	private void setRecentMsgLoadedBefor(boolean flag) {
		SharedPrefrencesDataLayer.saveBooleanPreferences(App.getInstance().getApplicationContext(),
				RECENT_MSG_CHECKED_KEY, flag);
	}
	
	public void loadRecentMessagesAsync() {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try{
					List<Message> cachedMsgs = loadMessagesFromDatabase();
					if (cachedMsgs != null) {
						addMessagesToFilterMap(cachedMsgs);
					}
					List<Message> latestMsg      = MessagesTable.getInstance().getLatestMessages();
//					List<FriendsEntity> conatcts = FriendTable.getInstance().getAll();
					if(latestMsg != null){
//					if(latestMsg != null && conatcts != null){
//						for(int i=0 ; i< latestMsg.size(); i++){
//							for(int k=0; k< conatcts.size(); k++){
//								FriendsEntity mContact = conatcts.get(k);
//								if(latestMsg.get(i).getFriendId() == mContact.getFanID()){
//									latestMsg.get(i).setSenderName(mContact.getFanName());
//									latestMsg.get(i).setSenderPicture(mContact.getFanPicture());
//									break;
//								}
//							}
//						}
						latestMessages = latestMsg; 
						setRecentMsgLoadedBefor(true);
						notifyVoidSuccess(OnChatUpdatedListener.class);
					}
					else // there is error getting latest messages ! (added by mohga)
						notifyRetrievalException(new AppException(AppException.NO_DATA_EXCEPTION), OnChatUpdatedListener.class);
						
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException((AppException) e, OnChatUpdatedListener.class);
				}
			}
		});
	}

	public List<Message> getLatestMessages() {
		// TODO Auto-generated method stub
		return latestMessages;
	}
	
	/* ============================================================================================== */
	/*								Change Status Methods                                             */
	/* ============================================================================================== */
	
	
	public void changeStatusAsync(final int online) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try{
					changeStatusRequest(online);
					notifyVoidSuccess(OnChatChangeStatusListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e), OnChatChangeStatusListener.class);
				}
			}
		});
	}
	
	private void changeStatusRequest(final int online) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG            , LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Chat.CHANGE_STATUS_ID  , UserManager.getInstance().getCurrentUserId());
		params.put(Params.Chat.CHANGE_STATUS     , online);
		BaseResponse response = submit(params, URLs.Methods.GET_CHAT_CHANGE_STATUS);

		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response.getStatus()));

		}
	}
	
	/* ============================================================================================== */
	/*								Filter Methods                                                    */
	/* ============================================================================================== */
	
	public List<Message> getMessagesToFiltered(int friendId) {
		if (mFilterMap.containsKey(friendId))
			return mFilterMap.get(friendId);
		else
			return null;
	}
	
	private void addMessagesToFilterMap(List<Message> msgs) {
		for (Message msg : msgs) {
			// False 0 = messages received, True 1 = messages sent
			if (mFilterMap.containsKey(msg.getFriendId())) {
				mFilterMap.get(msg.getFriendId()).add(msg);
			} else {
				List<Message> tmpList = new ArrayList<Message>();
				tmpList.add(msg);
				mFilterMap.put(msg.getFriendId(), tmpList);
			}
		}
	}
	
	/* ============================================================================================== */
	/*								Chat Contact Methods                                                    */
	/* ============================================================================================== */
	
	/** Calling performGetFriends to get chat Friends list. **/
	public void callChatContacts(List<DeviceContactModel> deviceContactList) {
		performChatContacts(deviceContactList);
	}

	/** Calling loadFriends web service, then notify all with response list and exception if happened. **/
	private void performChatContacts(final List<DeviceContactModel> deviceContactList) {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					List<ChatContactsEntity> list = getChatContacts(deviceContactList);
					chatContacts = list;
					notifyEntityListReceviedSuccess(list, OnChatContactsResponseListener.class);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e), OnChatContactsResponseListener.class);
				}
			}
		});
	}

	/** Build request parameter, the deserialize response to FriendsEntity list. **/
	private List<ChatContactsEntity> getChatContacts(List<DeviceContactModel> deviceContactList) throws AppException {
		 
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG          , LanguageUtils.getInstance().getDeviceLanguage()); 
		params.put(Params.Chat.CONTACT_USER_ID , UserManager.getInstance().getCurrentUserId());
		params.put(Params.Chat.CONTACT_MODEL   , deviceContactList);
		
		List<ChatContactsEntity> list = getObjectList(URLs.Methods.LOAD_CHAT_CONTACT,params, ChatContactsEntity.class);
		return list;
	}
	
	public List<ChatContactsEntity> getChatContactsList() {
		return chatContacts;
	}
	
	
	/**handle the invite friends WB for Chat  and Friends Views */
	public void InviteFriends(final String invitationTxt) 
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				try {
					performInvite(invitationTxt);
				
				} catch (AppException e) {}
			}
		});
	}
	private void performInvite(final String invitationTxt)throws AppException
	{

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.SMS_INVITATION.USER_ID   , UserManager.getInstance().getCurrentUser().getUserId());
		params.put(Params.SMS_INVITATION.SMS_TEXT  , invitationTxt);
		 submit(params, URLs.Methods.INVITE_FRIENDS);

	}
}
