package com.tawasol.barcelona.managers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.android.vending.billing.util.BarcaIabHelper;
import com.android.vending.billing.util.BarcaIabHelper.OnConsumeFinishedListener;
import com.android.vending.billing.util.BarcaIabResult;
import com.android.vending.billing.util.BarcaPurchase;
import com.tawasol.barcelona.R;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.business.BusinessManager;
import com.tawasol.barcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.barcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.data.connection.URLs;
import com.tawasol.barcelona.entities.InAppPointsEntity;
import com.tawasol.barcelona.entities.InAppProductId;
import com.tawasol.barcelona.entities.MyPoints;
import com.tawasol.barcelona.entities.PremiumStateObject;
import com.tawasol.barcelona.entities.User;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnMyPointsReceived;
import com.tawasol.barcelona.listeners.OnNikeStoreLinkRecieved;
import com.tawasol.barcelona.listeners.OnPremiumStateRecieved;
import com.tawasol.barcelona.listeners.UiListener;
import com.tawasol.barcelona.responses.BaseResponse;
import com.tawasol.barcelona.ui.activities.SplashActivity;
import com.tawasol.barcelona.utils.LanguageUtils;

/**
 * 
 * @author Basyouni anyone need to use this class and using both submit or any
 *         method from BusinessManager u should pass the object returned or app
 *         will crash
 *
 */
public class ShopManager extends BusinessManager<MyPoints> {

	public static final int ONE_MONTH_SUBSCRIPTION = 30;
	public static final int THREE_MONTHS_SUBSCRIPTION = 90;
	public static final int SIX_MONTHS_SUBSCRIPTION = 180;
	public static final int ONE_YEAR_SUBSCRIPTION = 365;

	public static final String ONE_HUNDRED_POINTS = "100";
	public static final String THREE_HUNDRED_POINTS = "300";
	public static final String FIVE_HUNDRED_POINTS = "500";
	public static final String EIGHT_HUNDRED_POINTS = "800";

	public static final String ONE_MONTH = "1 MONTH";
	public static final String THREE_MONTHS = "3 MONTHS";
	public static final String SIX_MONTHS = "6 MONTHS";
	public static final String ONE_YEAR = "1 YEAR";

	public static final String PointsFILE = "com.tawasol.barca.ShopManager.P244O23453I2425N3545T35253S";
	public static final String purchaseObject = "com.tawasol.barca.ShopManager.P244U23453R2425C3545H35253S";
	public static final String SUBSCRIPTION_FILE = "com.tawasol.barca.ShopManager.S244U23453B2425S3545H35253S";
	public static final String BILING_IS_NOT_AVAILABLE = "Billing is not Unavailable";
	public static boolean BILING_NOT_AVAILABLE = false;
	private Context context;
	public static ShopManager instance;
	private static boolean dataAvailable;
	protected static boolean isDataAvailableAvailableException;
	protected static String dataAvailableAvailableException;

	public List<InAppPointsEntity> points = new ArrayList<InAppPointsEntity>();
	public List<InAppPointsEntity> premium = new ArrayList<InAppPointsEntity>();
	protected PremiumStateObject premiumState;
	protected boolean isPremiumStateAvailable;
	private Object syncObject = new Object();

	public boolean checkPointsFile() {
		return new File(context.getFilesDir() + "/" + PointsFILE).exists();
	}

	public boolean checkPurchaseObject() {
		return new File(context.getFilesDir() + "/" + purchaseObject).exists();
	}

	public boolean checkSubscriptionFile() {
		return new File(context.getFilesDir() + "/" + SUBSCRIPTION_FILE)
				.exists();
	}

	protected ShopManager() {
		super(null);
		context = App.getInstance().getApplicationContext();
	}

	public static ShopManager getInstance() {
		if (instance == null)
			instance = new ShopManager();
		return instance;
	}

	public boolean isDataAvailableException() {
		return isDataAvailableAvailableException;
	}

	public String dataRetrievalException() {
		return dataAvailableAvailableException;
	}

	public boolean isDataAvailable() {
		return dataAvailable;
	}

	public List<InAppPointsEntity> getPoints() {
		return points;
	}

	public void setPoints(List<InAppPointsEntity> points) {
		this.points = points;
	}

	public List<InAppPointsEntity> getPremium() {
		return premium;
	}

	public void setPremium(List<InAppPointsEntity> premium) {
		this.premium = premium;
	}

	public PremiumStateObject getPremiumState() {
		return premiumState;
	}

	public boolean isPremiumStateAvailable() {
		return isPremiumStateAvailable;
	}

	public void getMyPoints(final int userID) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					MyPoints points = getMyPointsFromServer(userID);
					notifyMyPointsSuccess(points);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnMyPointsReceived.class);
					e.printStackTrace();
				}
			}
		});
	}

	protected void notifyMyPointsSuccess(final MyPoints points) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnMyPointsReceived)
						((OnMyPointsReceived) listener).onPointsRecieved(points
								.getPoints());
				}
			}
		});
	}

	private MyPoints getMyPointsFromServer(int userID) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.User.USER_ID, userID);
		params.put(Params.Common.LANG, SharedPrefrencesDataLayer
				.getStringPreferences(context,
						LanguageUtils.APP_LANGUAGE_CODE_KEY,
						LanguageUtils.LANGUAGE_ENGLISH));
		return getObject(params, URLs.Methods.GET_USER_CREDIT, MyPoints.class);
	}

	// ---------------------------------------------------
	public void addPointsForUser(final int userid, final int pointsBought,
			final BarcaPurchase info, final BarcaIabHelper billingHelper,
			final ProgressDialog dialog, final boolean save) {
		synchronized (syncObject) {
			if (save) {
				SavePoints(pointsBought);
				SavePurchase(info);
			}
			App.getInstance().runInBackground(new Runnable() {

				@Override
				public void run() {
					try {
						addPointsToServer(userid, pointsBought, info,
								billingHelper);
						if (dialog != null)
							dialog.dismiss();
					} catch (final Exception e) {
						App.getInstance().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								if (dialog != null) {
									dialog.dismiss();
									Toast.makeText(
											context,
											AppException.getAppException(e)
													.getMessage(),
											Toast.LENGTH_LONG).show();
								}
							}
						});
						e.printStackTrace();
					}
				}
			});
		}
	}

	public void addPointsToServer(int userid, int pointsBought,
			BarcaPurchase info, BarcaIabHelper billingHelper)
			throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.User.USER_ID, userid);
		params.put(Params.User.USER_CREDIT, pointsBought);
		params.put(Params.Common.LANG, SharedPrefrencesDataLayer
				.getStringPreferences(context,
						LanguageUtils.APP_LANGUAGE_CODE_KEY,
						LanguageUtils.LANGUAGE_ENGLISH));
		BaseResponse response = submit(params, URLs.Methods.UPDATE_USER_CREDIT);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				|| response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			InternalFileSaveDataLayer.deleteObjectFile(PointsFILE);
			User user = UserManager.getInstance().getCurrentUser();
			user.setCredit(user.getCredit() + pointsBought);
			UserManager.getInstance().cacheUser(user);
			consumePoints(billingHelper, info);
		}
	}

	// -----------------------------------------------------

	public void consumePoints(BarcaIabHelper billingHelper, BarcaPurchase info) {
		OnConsumeFinishedListener listener = new OnConsumeFinishedListener() {

			@Override
			public void onConsumeFinished(BarcaPurchase purchase,
					BarcaIabResult result) {
				InternalFileSaveDataLayer.deleteObjectFile(purchaseObject);
			}
		};
		if (billingHelper != null)
			billingHelper.flagEndAsync();
		billingHelper.consumeAsync(info, listener);
	}

	public void subscripeInPremium(final int userID,
			final int subscriptionDuration) {
		synchronized (syncObject) {
			App.getInstance().runInBackground(new Runnable() {

				@Override
				public void run() {
					try {
						SaveSubscription(subscriptionDuration);
						subscripeInPremiumToServer(userID, subscriptionDuration);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
	}

	protected void subscripeInPremiumToServer(int userID,
			int subscriptionDuration) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Shop.USER_ID, userID);
		params.put(Params.Shop.SUBSCRIPTION_DURATION, subscriptionDuration);
		params.put(Params.Common.LANG, SharedPrefrencesDataLayer
				.getStringPreferences(context,
						LanguageUtils.APP_LANGUAGE_CODE_KEY,
						LanguageUtils.LANGUAGE_ENGLISH));
		BaseResponse response = submit(params,
				URLs.Methods.SUBSCRIPE_IN_PREMIUM);
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				|| response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			InternalFileSaveDataLayer.deleteObjectFile(SUBSCRIPTION_FILE);
		}
	}

	// ----------------------------------

	public void MyPremiumStatus(final int userID, final String productId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					premiumState = myPremiumStateFrmServer(userID);
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							for (UiListener listener : getListeners()) {
								if (listener instanceof OnPremiumStateRecieved) {
									((OnPremiumStateRecieved) listener)
											.onPremiumStateRecieved(
													premiumState, productId);
								}
							}
						}
					});
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnPremiumStateRecieved.class);
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	protected PremiumStateObject myPremiumStateFrmServer(int userID)
			throws AppException {
		PremiumStateObject premium = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Shop.USER_ID, userID);
		params.put(Params.Common.LANG, SharedPrefrencesDataLayer
				.getStringPreferences(context,
						LanguageUtils.APP_LANGUAGE_CODE_KEY,
						LanguageUtils.LANGUAGE_ENGLISH));
		BaseResponse response = submit(params, URLs.Methods.MY_PREMIUM_STATE);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
		try {
			JSONObject json = new JSONObject(response.getData());
			premium = new PremiumStateObject(json.getBoolean("premium")/*
																		 * == 1
																		 * ?
																		 * true
																		 * :
																		 * false
																		 */,
					json.getString("premiumEnd"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return premium;
	}

	// ----------------------------------------

	public void LoadInAppProductIDs() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					LoadInAppProductIDsFrmServer();
				} catch (Exception e) {
					isDataAvailableAvailableException = true;
					dataAvailableAvailableException = AppException
							.getAppException(e).getMessage();
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	protected void LoadInAppProductIDsFrmServer() throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, SharedPrefrencesDataLayer
				.getStringPreferences(context,
						LanguageUtils.APP_LANGUAGE_CODE_KEY,
						LanguageUtils.LANGUAGE_ENGLISH));
		params.put(Params.Common.DEVICE_TYPE, Params.Common.ANDROID);
		List<InAppProductId> products = getObjectList(
				URLs.Methods.LOAD_IN_APP_PRODUCTS, params, InAppProductId.class);/*
																				 * getObject
																				 * (
																				 * params
																				 * ,
																				 * URLs
																				 * .
																				 * Methods
																				 * .
																				 * TEST_SHOP
																				 * ,
																				 * InAppProductId
																				 * .
																				 * class
																				 * )
																				 * ;
																				 */
		points.clear();
		premium.clear();
		List<InAppPointsEntity> tempPoints = products.get(0).getPoints();
		Collections.reverse(tempPoints);
		points.add(new InAppPointsEntity(tempPoints.get(0).getInAppId(),
				tempPoints.get(0).getPrice(), tempPoints.get(0)
						.getDescription(), ONE_HUNDRED_POINTS));
		points.add(new InAppPointsEntity(tempPoints.get(1).getInAppId(),
				tempPoints.get(1).getPrice(), tempPoints.get(1)
						.getDescription(), THREE_HUNDRED_POINTS));
		points.add(new InAppPointsEntity(tempPoints.get(2).getInAppId(),
				tempPoints.get(2).getPrice(), tempPoints.get(2)
						.getDescription(), FIVE_HUNDRED_POINTS));
		points.add(new InAppPointsEntity(tempPoints.get(3).getInAppId(),
				tempPoints.get(3).getPrice(), tempPoints.get(3)
						.getDescription(), EIGHT_HUNDRED_POINTS));

		List<InAppPointsEntity> tempPremium = products.get(0).getPremium();
		Collections.reverse(tempPremium);

		premium.add(new InAppPointsEntity(tempPremium.get(0).getInAppId(),
				tempPremium.get(0).getPrice(), tempPremium.get(0)
						.getDescription(), context.getString(R.string.PremiumPackages_month)));
		premium.add(new InAppPointsEntity(tempPremium.get(1).getInAppId(),
				tempPremium.get(1).getPrice(), tempPremium.get(1)
						.getDescription(), context.getString(R.string.PremiumPackages_3month)));
		premium.add(new InAppPointsEntity(tempPremium.get(2).getInAppId(),
				tempPremium.get(2).getPrice(), tempPremium.get(2)
						.getDescription(), context.getString(R.string.PremiumPackages_6month)));
		premium.add(new InAppPointsEntity(tempPremium.get(3).getInAppId(),
				tempPremium.get(3).getPrice(), tempPremium.get(3)
						.getDescription(), context.getString(R.string.PremiumPackages_year)));

		dataAvailable = true;
	}

	private void SavePoints(int points) {
		try {
			InternalFileSaveDataLayer.saveObject(context, PointsFILE, points);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Integer getSavedPoints() {
		Integer points = 0;
		try {
			points = (Integer) InternalFileSaveDataLayer.getObject(context,
					PointsFILE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return points;
	}

	private void SavePurchase(BarcaPurchase info) {
		try {
			InternalFileSaveDataLayer.saveObject(context, purchaseObject, info);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public BarcaPurchase getSavedPurchase() {
		BarcaPurchase info = null;
		try {
			info = (BarcaPurchase) InternalFileSaveDataLayer.getObject(context,
					purchaseObject);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return info;
	}

	private void SaveSubscription(int subDuration) {
		try {
			InternalFileSaveDataLayer.saveObject(context, SUBSCRIPTION_FILE,
					subDuration);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Integer getSvaedSubscription() {
		Integer subscriptionDuration = 0;
		try {
			subscriptionDuration = (Integer) InternalFileSaveDataLayer
					.getObject(context, SUBSCRIPTION_FILE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return subscriptionDuration;
	}

	public void getShopUrl() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					final String url = getShopUrlFromServer();
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							for (UiListener listener : getListeners()) {
								if (listener instanceof OnNikeStoreLinkRecieved)
									((OnNikeStoreLinkRecieved) listener)
											.onLinkRecieved(url);
							}
						}
					});
				} catch (final Exception e) {
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							for (UiListener listener : getListeners()) {
								if (listener instanceof OnNikeStoreLinkRecieved)
									((OnNikeStoreLinkRecieved) listener)
											.onException(AppException
													.getAppException(e));
							}
						}
					});
					e.printStackTrace();
				}
			}
		});
	}

	protected String getShopUrlFromServer() throws AppException {
		String url = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, SharedPrefrencesDataLayer
				.getStringPreferences(context,
						LanguageUtils.APP_LANGUAGE_CODE_KEY,
						LanguageUtils.LANGUAGE_ENGLISH));
		BaseResponse response = submit(params, URLs.Methods.GET_SHOP_URL);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		} else {
			url = response.getData();
		}
		return url;
	}

	public void GetSurvey() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					final String url = getSurvyUrlFromServer();
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							for (UiListener listener : getListeners()) {
								if (listener instanceof OnNikeStoreLinkRecieved)
									((OnNikeStoreLinkRecieved) listener)
											.onLinkRecieved(url);
							}
						}
					});
				} catch (final Exception e) {
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							for (UiListener listener : getListeners()) {
								if (listener instanceof OnNikeStoreLinkRecieved)
									((OnNikeStoreLinkRecieved) listener)
											.onException(AppException
													.getAppException(e));
							}
						}
					});
					e.printStackTrace();
				}
			}
		});
	}

	protected String getSurvyUrlFromServer() throws AppException {
		String url = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, SharedPrefrencesDataLayer
				.getStringPreferences(context,
						LanguageUtils.APP_LANGUAGE_CODE_KEY,
						LanguageUtils.LANGUAGE_ENGLISH));
		params.put(Params.Shop.NAME, Params.Shop.SURVY);
		BaseResponse response = submit(params, URLs.Methods.FAQ_LINK);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		} else {
			url = response.getData();
		}
		return url;
	}

	public void rewardingPoints() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					final String url = getRewardingPointsUrlFromServer();
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							for (UiListener listener : getListeners()) {
								if (listener instanceof OnNikeStoreLinkRecieved)
									((OnNikeStoreLinkRecieved) listener)
											.onLinkRecieved(url);
							}
						}
					});
				} catch (final Exception e) {
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							for (UiListener listener : getListeners()) {
								if (listener instanceof OnNikeStoreLinkRecieved)
									((OnNikeStoreLinkRecieved) listener)
											.onException(AppException
													.getAppException(e));
							}
						}
					});
					e.printStackTrace();
				}
			}
		});
	}

	protected String getRewardingPointsUrlFromServer() throws AppException {
		String url = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, SharedPrefrencesDataLayer
				.getStringPreferences(context,
						LanguageUtils.APP_LANGUAGE_CODE_KEY,
						LanguageUtils.LANGUAGE_ENGLISH));
		params.put(Params.Shop.NAME, Params.Shop.POINTS);
		BaseResponse response = submit(params, URLs.Methods.FAQ_LINK);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		} else {
			url = response.getData();
		}
		return url;
	}
}
