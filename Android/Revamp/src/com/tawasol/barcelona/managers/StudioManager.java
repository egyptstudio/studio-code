package com.tawasol.barcelona.managers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tawasol.barcelona.application.App;
import com.tawasol.barcelona.business.BusinessManager;
import com.tawasol.barcelona.data.cache.SeasonsTable;
import com.tawasol.barcelona.data.cache.StudioFoldersTable;
import com.tawasol.barcelona.data.cache.StudioPhotosTable;
import com.tawasol.barcelona.data.cache.TagsTable;
import com.tawasol.barcelona.data.connection.Params;
import com.tawasol.barcelona.data.connection.URLs;
import com.tawasol.barcelona.data.helper.DataHelper;
import com.tawasol.barcelona.entities.KeyValuePairs;
import com.tawasol.barcelona.entities.PostViewModel;
import com.tawasol.barcelona.entities.Seasons;
import com.tawasol.barcelona.entities.StudioFolder;
import com.tawasol.barcelona.entities.StudioPhoto;
import com.tawasol.barcelona.exception.AppException;
import com.tawasol.barcelona.listeners.OnFoldersReceivedListener;
import com.tawasol.barcelona.listeners.OnShareListener;
import com.tawasol.barcelona.listeners.OnStudioPhotoReceived;
import com.tawasol.barcelona.listeners.OnStudioPhotosReceivedListener;
import com.tawasol.barcelona.listeners.OnSuccessVoidListener;
import com.tawasol.barcelona.listeners.UiListener;
import com.tawasol.barcelona.responses.BaseResponse;
import com.tawasol.barcelona.utils.LanguageUtils;

/**
 * @author Mohga
 * */
public class StudioManager extends BusinessManager<StudioFolder> {

	private static StudioManager instance;
	private static ArrayList<StudioFolder>folders;
	 ArrayList<Seasons> seasons;

	// constructor
	protected StudioManager() {
		super(StudioFolder.class);
	}

	// get instance of the manager
	public static StudioManager getInstance() {
		if (instance == null)
			instance = new StudioManager();
		return instance;
	}

	/** get Seasons List*/
	public ArrayList<Seasons> getSeasonList()
	{
			return this.seasons;
	}
	
	
	/**
	 * get studio folders List from back-end and notify result
	 * 
	 */
	
	public void getStudioFolders(final int folderType) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					ArrayList<StudioFolder> studioFolders = new ArrayList<StudioFolder>();
					folders = (ArrayList<StudioFolder>)getCahcedFolders();
					
					if(folders != null && folders.size() >0 )
						studioFolders.addAll(folders);
				else
				{
					studioFolders = getFoldersFromServer();
					
				}
					folders =filterFolders(studioFolders, folderType);
					
					// sort the prizes list according to prize rank ASC 
					if (folders != null ){
						Collections.sort(folders, new Comparator<StudioFolder>() {

							@Override
							public int compare(StudioFolder lhs,
									StudioFolder rhs) {
								
								return lhs.getRank().compareTo(rhs.getRank());
							}

							
						});
					}
					
					
					notifyEntityListReceviedSuccess(folders,
							OnFoldersReceivedListener.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFoldersReceivedListener.class);
					e.printStackTrace();
				}
			}
		});
	}
	
	
/**get Studio folders from Server **/
public ArrayList<StudioFolder> getFoldersFromServer()
{
	  ArrayList<StudioFolder> studioFolders =new ArrayList<StudioFolder>();
	
			// set the language parameter 
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
			// get the folders list
				try {
					studioFolders = (ArrayList<StudioFolder>) getObjectList(
							URLs.Methods.GET_STUDIO_FOLDERS, params);
				} catch (AppException e) {
					e.printStackTrace();
				}
			
			if(studioFolders != null && studioFolders.size() != 0){
				// save the folders 
				cacheFolders(studioFolders);
				//cachefolderImages(studioFolders);
				
		}
		
	return studioFolders;
}

	
	// filter player {Player , Team}
	private  ArrayList<StudioFolder> filterFolders(ArrayList<StudioFolder> folders ,int folderType)
	{
		ArrayList<StudioFolder> filteredFolders = new ArrayList<StudioFolder>();
		for (StudioFolder studioFolder : folders) {
			if(studioFolder.getFolderType() == folderType)
				filteredFolders.add(studioFolder);
		}
		return filteredFolders;
	}
	
	/**
	 * get purchased photos from back-end and notify result
	 * 
	 */
	public void getPurchasedPhotos(final int pageNo) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					
					ArrayList<StudioPhoto> purchasedPhotos ;
//					 purchasedPhotos = (ArrayList<StudioPhoto>)getCachedPurchasedPhotos();
//					
//					if(purchasedPhotos == null ||  purchasedPhotos.size() <= 0 || Math.ceil(purchasedPhotos.size() / 24.0) < pageNo)
//					{
					 purchasedPhotos = (ArrayList<StudioPhoto>) getObjectList(
							URLs.Methods.GET_PURCHASED_PHOTOS, preparePurchasedPhotosParamaters(pageNo),StudioPhoto.class);
					//}
					notifyEntityListReceviedSuccess(purchasedPhotos,
							OnStudioPhotosReceivedListener.class);
					
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnStudioPhotosReceivedListener.class);
					e.printStackTrace();
				}
			}
		});
	}
	
	/* set the parameters of pageNo and userId if logged in */
	private Map<String, Object> preparePurchasedPhotosParamaters(int PageNo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		
		if(UserManager.getInstance().getCurrentUserId() !=0)
			params.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUserId());
		
		params.put(Params.Studio.PAGE_NUMBER, PageNo);
		
		return params;
	}

	/** get the Studio Photos*/
	public void getStudioPhotos(final int folderId ,final int pageNo) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					ArrayList<StudioPhoto> studioPhotos = (ArrayList<StudioPhoto>)getCahcedPhotos(folderId);
					
					if(studioPhotos == null ||  studioPhotos.size() <= 0 || Math.ceil(studioPhotos.size() / 24.0) < pageNo)
					{
						studioPhotos = getPhotosFromServer(folderId,pageNo);
					}
					
					notifyEntityListReceviedSuccess(studioPhotos,
							OnStudioPhotosReceivedListener.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnStudioPhotosReceivedListener.class);
					e.printStackTrace();
				}
				
			}
		});
	}
	
	
	/**get the Studio photos from serevr */
	public ArrayList<StudioPhoto> getPhotosFromServer( int folderId , int pageNo)throws AppException
	{
		ArrayList<StudioPhoto> studioPhotos = (ArrayList<StudioPhoto>) getObjectList(
					URLs.Methods.GET_STUDIO_FOLDER_PHOTOS, prepareStudioPhotosParamaters(folderId, pageNo),StudioPhoto.class);
			 cachePhotos(studioPhotos);
			 return studioPhotos;
	}
	
	/* set the parameters of pageNo and userId if logged in */
	private Map<String, Object> prepareStudioPhotosParamaters(int folderId , int PageNo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUserId());
		params.put(Params.Studio.FOLDER_ID, folderId);
		params.put(Params.Studio.PAGE_NUMBER, PageNo);
		
		return params;
	}
	
	
	/** get the Seasons*/
	public void getSeasons() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					
					// get seasons from DB 
					seasons = (ArrayList<Seasons>) getCahcedSeasons();
					
					// if there is no seasons get from server 
					 if( seasons == null || seasons.size() <= 0)
					 {
					seasons = getSeasonsFromServer();
					 }
					
				} catch (Exception e) {
					
					e.printStackTrace();
				}
			}
		});
	}
	
	/**get Seasons from Server */
	public ArrayList<Seasons> getSeasonsFromServer()
	{
		ArrayList<Seasons> seasons = new  ArrayList<Seasons>();
		// set the language parameter 
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());

		 try {
			seasons = (ArrayList<Seasons>) getObjectList(
					URLs.Methods.GET_SEASONS, params,Seasons.class);
		} catch (AppException e) {
			e.printStackTrace();
		}
		 if(seasons != null)
				cacheSeasons(seasons);
		 return seasons;
	}
	
	/** Filter Photos "Overloaded method"*/

	public void getStudioPhotos(final int folderId ,final int pageNo,final int filterBy , final List<Seasons> seasons) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					
					List<StudioPhoto> studioPhotos = (ArrayList<StudioPhoto>) getObjectList(
							URLs.Methods.GET_STUDIO_FOLDER_PHOTOS, prepareFilterStudioPhotosParamaters(folderId, pageNo,filterBy,seasons),StudioPhoto.class);
					 
					notifyEntityListReceviedSuccess(studioPhotos,
							OnStudioPhotosReceivedListener.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnStudioPhotosReceivedListener.class);
					e.printStackTrace();
				}
				
			}
		});
	}
	/* set the parameters of pageNo and userId if logged in */
	private Map<String, Object> prepareFilterStudioPhotosParamaters(int folderId , int PageNo, int filterBy , List<Seasons>seasons ) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUserId());
		params.put(Params.Studio.FOLDER_ID, folderId);
		params.put(Params.Studio.PAGE_NUMBER, PageNo);
		params.put(Params.Studio.FILTERBY, filterBy);
		params.put(Params.Studio.SEASONLIST, seasons);
		
		return params;
	}
	
	
	/** Share post to wall  */
	public void addPost(final PostViewModel post )
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				try{
					BaseResponse response;
					String path = post.getPostPhotoUrl();
					//post.setPostPhotoUrl("");
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(Params.Share.POST, 	DataHelper.serialize(post, PostViewModel.class));
				
				 response = submit(params, new File(path), URLs.Methods.SHARE_TO_WALL);
				
				 if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
						PostViewModel post = DataHelper.deserialize(response.getData(), PostViewModel.class);
						notifyShare(post);
					} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

						if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
							throw new AppException(response.getValidationRule().errorMessage);
						else
							throw new AppException(BaseResponse.getExceptionType(response.getStatus()));

					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnShareListener.class);
					e.printStackTrace();
				}
			}
		});
		
	}
	// notify the share listener 
	private void notifyShare(final PostViewModel post)
	{
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for(UiListener listener : getListeners()){
					if(listener instanceof OnShareListener){
						((OnShareListener)listener).onSuccess(post);
					}
				}
			}
		});
	}
	
	/**Update User Credit */
	public void updateUserCredit(final int userCredit)
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() { 
				try {
					executeUserCredit(UserManager.getInstance().getCurrentUser().getCredit() + userCredit);
				}catch (AppException e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnSuccessVoidListener.class);
				}
			}
		});
	}
	private void executeUserCredit(int userCredit)throws AppException
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUser().getUserId());
		params.put(Params.User.USER_CREDIT, userCredit);
		BaseResponse response = submit( params, URLs.Methods.UPDATE_USER_CREDIT);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
		UserManager.getInstance().getCurrentUser().setCredit(userCredit);
		UserManager.getInstance().cacheUser(UserManager.getInstance().getCurrentUser());
		//notifyVoidSuccess(OnSuccessVoidListener.class);
	}
	
	
	/** Purchase Studio Photo method */
	public void purchasePhoto(final int StudioPhotoId)
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				try {
					performPurchasePhoto(StudioPhotoId);
					//notifyVoidSuccess(OnSuccessVoidListener.class);
					
					
					
				} catch (AppException e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnSuccessVoidListener.class);
				}
				
			}
		});
	}
	
	private void performPurchasePhoto(int StudioPhotoId)throws AppException
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUser().getUserId());
		params.put(Params.Studio.STUDIO_PHOTO_ID, StudioPhotoId);
		
		BaseResponse response = submit(params, URLs.Methods.PURCHASE);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}
	
	
	/**get Studio Photo Details 
	 * @param studioPhotoId
	 * */
	public void getStudioPhoto(final int studioPhotoId,final int userId)
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				try {
					executeGetStudioPhoto(studioPhotoId);
				} catch (AppException e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnStudioPhotoReceived.class);
				}
			}
		});
	}
	
	private void executeGetStudioPhoto(int studioPhotoId)throws AppException
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Studio.STUDIO_PHOTO_ID, studioPhotoId);
		params.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUser().getUserId());
		
		StudioPhoto photo = getObject(params, URLs.Methods.GET_STUDIO_PHOTO, StudioPhoto.class);
		notifyEntityReceviedSuccess(photo, OnStudioPhotoReceived.class);
	}
	
	
	/**Add Poster method */
	public void sendPosterRequest(final String UserPhotoPath, final String MaskedPhotoPath){
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				try {
					executesendPoster(UserPhotoPath, MaskedPhotoPath);
					notifyVoidSuccess(OnSuccessVoidListener.class);
				} catch (AppException e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnSuccessVoidListener.class);
				}
				
			}
		});
	}
	private Map<String, Object> prepareSendPosterParams()
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUserId());
		params.put(Params.Studio.USER_EMAIL, UserManager.getInstance().getCurrentUser().getEmail());
		//params.put(Params.Studio.QUANTITY, 1);
		/*params.put(Params.Studio.USER_PHOTO, new File(UserPhotoPath));
		params.put(Params.Studio.MASKED_PHOTO, new File(MaskedPhotoPath));*/
		return params;
	}
	
	private void executesendPoster(String UserPhotoPath, String MaskedPhotoPath)throws AppException
	{
			BaseResponse response = submitPosterFiles(prepareSendPosterParams(), new File(UserPhotoPath), new File(MaskedPhotoPath),  URLs.Methods.SEND_POSTER);
			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {

				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
					throw new AppException(
							response.getValidationRule().errorMessage);
				else
					throw new AppException(BaseResponse.getExceptionType(response
							.getStatus()));
			}
		
	}
	
	/** Share Social media updated points method */
	public void shareToSocilaMedia()
	{
		App.getInstance().runInBackground(new Runnable() {
			
			@Override
			public void run() {
				try {
					performshareToSocialMedia();
					
					//notifyVoidSuccess(OnSuccessVoidListener.class);
					
				} catch (AppException e) {
					e.printStackTrace();
//					notifyRetrievalException(AppException.getAppException(e),
//							OnSuccessVoidListener.class);
				}
			
			}
		});
	}
	
	private void performshareToSocialMedia()throws AppException
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance().getDeviceLanguage());
		params.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUser().getUserId());
		
		BaseResponse response = submit(params, URLs.Methods.SHARE_SOCIAL_MEDIA);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}
	/** ----------- Caching Studio Folders methods --------------*/
	// get cached folders from DB
	private List<StudioFolder> getCahcedFolders() {
		return StudioFoldersTable.getInstance().getAll();
	}
	// cache folders in DB
	private void cacheFolders(List<StudioFolder> folders) {
		StudioFoldersTable.getInstance().insert(folders);
	}
	// cache Images in ImageLoader "Not used Now !" 
	private void cachefolderImages(ArrayList<StudioFolder> folders)
	{
		for (StudioFolder studioFolder : folders) {
			App.getInstance().getImageLoader().loadImageSync(studioFolder.getFolderPicURL());
		}
	}
	
	/** ----------- Caching Seasons methods --------------*/
	// get cached folders from DB
		private List<Seasons> getCahcedSeasons() {
			return SeasonsTable.getInstance().getAll();
		}
		// cache folders in DB
		private void cacheSeasons(List<Seasons> seasons) {
			SeasonsTable.getInstance().insert(seasons);
		}
		
		
		
		/** ----------- Caching Photos methods --------------*/
	// get cached studio photos from DB
		public List<StudioPhoto> getCahcedPhotos(int folderId)
		{
			List<StudioPhoto> photos =   StudioPhotosTable.getInstance().getBySelection(StudioPhotosTable.getInstance().getProjection()[1] + " = " + folderId, null);
			for (StudioPhoto studioPhoto : photos) {
				studioPhoto.setTags(getCachedTags(studioPhoto.getStudioPhotoId()));
			}
			return photos;
		}
	// cache photos in DB 
		private void cachePhotos(List<StudioPhoto> photos)
		{
			StudioPhotosTable.getInstance().insert(photos);
			
			for (StudioPhoto studioPhoto : photos) {
				List<KeyValuePairs> tags = studioPhoto.getTags(); 
				if(tags != null){
				List<KeyValuePairs>convertedTags = new ArrayList<KeyValuePairs>();
				for (KeyValuePairs tag : tags) {
					convertedTags.add(new KeyValuePairs(tag.get_Id(), tag.getText(), tag.getType(), studioPhoto.getStudioPhotoId()));
				}
				TagsTable.getInstance().insert(convertedTags);
				}
			}
		}
		
		
		private List<KeyValuePairs> getCachedTags(int postId)
		{
			return TagsTable.getInstance().getBySelection(TagsTable.getInstance().getProjection()[1] + " = " + postId, null);
		}
	private List<StudioPhoto> getCachedPurchasedPhotos()
		{
			return StudioPhotosTable.getInstance().getPurchasedPhotos();
		}
		
	/*private ArrayList<StudioFolder> fillFolders()
	{
		ArrayList<StudioFolder> folders = new ArrayList<StudioFolder>();
		folders.add(new StudioFolder(1, "Players1", "http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg", 500, 2, 1));
		folders.add(new StudioFolder(2, "Ahmed", "https://igcdn-photos-g-a.akamaihd.net//hphotos-ak-xpf1//10570155_806254719395518_1563946762_a.jpg", 500, 98, 2));
		folders.add(new StudioFolder(3, "Mohga", "http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg", 500, 50, 1));
		folders.add(new StudioFolder(4, "Mohamed", "https://igcdn-photos-g-a.akamaihd.net//hphotos-ak-xpf1//10570155_806254719395518_1563946762_a.jpg", 500, 70, 2));
		return folders;
	}

	private ArrayList<StudioPhoto> fillPhotos()
	{
		ArrayList<StudioPhoto> photos = new ArrayList<StudioPhoto>();
		photos.add(new StudioPhoto(1, 1, "https://igcdn-photos-g-a.akamaihd.net//hphotos-ak-xpf1//10570155_806254719395518_1563946762_a.jpg", StudioPhoto.STUDIO_PHOTO_ORIENTATION_LANDSCAPE, 2, StudioPhoto.PHOTO_CAPTURE_TYPE_MASK_SHOT, "Test", 0, 200, 0, 0, null));
		photos.add(new StudioPhoto(2, 2, "http://pbs.twimg.com/profile_images/438661062322163712/wYoSA3il_normal.jpeg", StudioPhoto.STUDIO_PHOTO_ORIENTATION_PORTRAIT, 10, StudioPhoto.PHOTO_CAPTURE_TYPE_SCREEN_SHOT, "Test33", 0, 100, 1, 1, null));
		return photos;
	}*/

}
