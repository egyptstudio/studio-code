package com.tawasol.fcbBarcelona.exception;

public class NullIntentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2187442454884424444L;

	static String msg = "you have to give an intent for LOGIN_FOR_RESULT or LOGIN_WITH_INTENT";
	
	public NullIntentException(){
		super(msg);
	}
}
