package com.tawasol.fcbBarcelona.data.cache;

import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.UserContacts;

public class UserContactsTable extends AbstractTable<UserContacts> {

	private static UserContactsTable instance;
	// Table name
	private static final String TABLE_CONTACTS = "userContactsTable";
	
	public static final class Fields implements BaseColumns {
		// Column names
		private static final String ID = BaseColumns._ID;
		private static final String USER_ID = "userId";
		private static final String PHONE = "phone";
		private static final String EMAIL = "mail";
	}

	
	// Create Table Message
		private static final String CREATE_TABLE_USER_CONTACTS = "CREATE TABLE "
				+ TABLE_CONTACTS + " (" 
//				AUTOINCREMENT
				+ Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ Fields.USER_ID + " INTEGER,"
				+ Fields.PHONE + " TEXT," + Fields.EMAIL + " TEXT"
				+ ")";

	
		public static UserContactsTable getInstance() {
			if (instance == null)
				instance = new UserContactsTable();
			return instance;
		}
		
	public UserContactsTable() {
		super(DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()), App.getInstance()
				.getApplicationContext());
	}

	public int getCount (){
		String query = "select * from "+TABLE_CONTACTS;
		Cursor cursor = DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()).getReadableDatabase().rawQuery(query, null);
		return cursor.getCount();
	}
	
	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_USER_CONTACTS);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_CONTACTS;
	}

	@Override
	protected String[] getProjection() {
		return new String[] { Fields.USER_ID, Fields.PHONE, Fields.EMAIL };
	}

	@Override
	protected ContentValues getContentValues(UserContacts entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.USER_ID, entity.getUserId());
		values.put(Fields.PHONE, entity.getPhone());
		values.put(Fields.EMAIL, entity.getEmail());
		return values;
	}

	@Override
	protected UserContacts getObjFromCursor(Cursor cursor) {
		UserContacts contact = new UserContacts();
		contact.setUserId(cursor.getInt(cursor
				.getColumnIndex(Fields.USER_ID)));
		contact.setPhone(cursor.getString(cursor
				.getColumnIndex(Fields.PHONE)));
		contact.setEmail(cursor.getString(cursor
				.getColumnIndex(Fields.EMAIL)));
		return contact;
	}

	public void changeUserId(List<UserContacts> contacts){
		for(UserContacts contact : contacts){
			SQLiteDatabase db = this.getDb();
	        ContentValues values = new ContentValues();
	        values.put(Fields.USER_ID, contact.getUserId());
	        // updating row
	         db.update(TABLE_CONTACTS, values, Fields.PHONE + " = ?",
	                new String[] { String.valueOf(contact.getPhone()) });
		}
	}
	
}
