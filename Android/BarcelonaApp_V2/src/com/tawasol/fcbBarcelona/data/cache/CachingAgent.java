package com.tawasol.fcbBarcelona.data.cache;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import android.content.Context;
import android.os.Parcelable;

/**
 * @author Morabea
 */
public class CachingAgent {

	/**
	 * @param context
	 * @param fileName The name which the file will be saved on disk.
	 * @param obj The object to be saved, which must be {@linkplain Serializable} or {@linkplain Parcelable}
	 */
	public static void cacheObject(Context context, String fileName,
			Object obj) {
		try {
			InternalFileSaveDataLayer.saveObject(context, fileName, obj);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		}
	}
	
	/**
	 * @param context
	 * @param fileName
	 * @return The specified object or null if it doesn't exist.
	 */
	public static Object restoreObject(Context context, String fileName) {
		Object restoredObj = null;
		try {
			restoredObj = InternalFileSaveDataLayer.getObject(context, fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(); // TODO
		}
		return restoredObj;
	}
	
}
