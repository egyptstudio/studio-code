package com.tawasol.fcbBarcelona.data.cache;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.StudioFolder;

public class StudioFoldersTable extends AbstractTable<StudioFolder> {

	private static StudioFoldersTable sInstance;
	
	// Table name
	private static final String TABLE_STUDIO_FOLDER = "StudioFolders";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String FLD_FOLDER_ID      = "folderId";
		private static final String FLD_FOLDER_NAME         = "folderName";
		private static final String FLD_FOLDER_PIC_URL              = "folderPicUrl";
		private static final String FLD_NO_OF_PICS      = "noOfPics";
		private static final String FLD_RANK = "rank";
		private static final String FLD_FOLDER_TYPE          = "folderType";
	}
	
	// Create Table Folders
	private static final String CREATE_TABLE_STUDIO_FOLDERS  = "CREATE TABLE " + TABLE_STUDIO_FOLDER 
			+ "(" 
				+ Fields.FLD_FOLDER_ID + " INTEGER PRIMARY KEY," 
				+ Fields.FLD_FOLDER_NAME      + " TEXT," 
				+ Fields.FLD_FOLDER_PIC_URL         + " TEXT," 
				+ Fields.FLD_NO_OF_PICS              + " INTEGER," 
				+ Fields.FLD_RANK      + " INTEGER,"  
				+ Fields.FLD_FOLDER_TYPE          + " INTEGER" 
			+ ")";
		
	public static StudioFoldersTable getInstance() {
		if (sInstance == null)
			sInstance = new StudioFoldersTable();
		return sInstance;
	}
	
	private StudioFoldersTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_STUDIO_FOLDERS);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		return TABLE_STUDIO_FOLDER;
	}

	@Override
	protected String[] getProjection() {
		return new String[] {Fields.FLD_FOLDER_ID, Fields.FLD_FOLDER_NAME, Fields.FLD_FOLDER_PIC_URL,
				Fields.FLD_NO_OF_PICS, Fields.FLD_RANK, Fields.FLD_FOLDER_TYPE};
	}

	@Override
	protected ContentValues getContentValues(StudioFolder entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.FLD_FOLDER_ID      , entity.getFolderId());
	    values.put(Fields.FLD_FOLDER_NAME         , entity.getFolderName());
	    values.put(Fields.FLD_FOLDER_PIC_URL              , entity.getFolderPicURL());
	    values.put(Fields.FLD_NO_OF_PICS      , entity.getNoOfPics());
	    values.put(Fields.FLD_RANK , entity.getRank());
	    values.put(Fields.FLD_FOLDER_TYPE          , entity.getFolderType());
		return values;
	}

	@Override
	protected StudioFolder getObjFromCursor(Cursor cursor) {
		StudioFolder folder = new StudioFolder();
		folder.setFolderId(cursor.getInt(cursor.getColumnIndex(Fields.FLD_FOLDER_ID)));
		folder.setFolderName(cursor.getString(cursor.getColumnIndex(Fields.FLD_FOLDER_NAME)));
		folder.setFolderPicURL(cursor.getString(cursor.getColumnIndex(Fields.FLD_FOLDER_PIC_URL)));
		folder.setNoOfPics(cursor.getInt(cursor.getColumnIndex(Fields.FLD_NO_OF_PICS)));
		folder.setRank(cursor.getInt(cursor.getColumnIndex(Fields.FLD_RANK)));
		folder.setFolderType(cursor.getInt(cursor.getColumnIndex(Fields.FLD_FOLDER_TYPE)));
		
		return folder;
	}
	
	public int getCount(){
		String query = "select * from "+TABLE_STUDIO_FOLDER;
		Cursor cursor = DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()).getReadableDatabase().rawQuery(query, null);
		return cursor.getCount();
	}

	
	
}










