package com.tawasol.fcbBarcelona.data.cache;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.CommentTable.Fields;
import com.tawasol.fcbBarcelona.entities.PostLike;
import com.tawasol.fcbBarcelona.entities.UserComment;

public class LikeTable extends AbstractTable<PostLike> {

	private static LikeTable instance;
	// Table name
	private static final String TABLE_LIKE = "likeTable";

	public static final class Fields implements BaseColumns {
		// Column names
		private static final String LIKE_ID = BaseColumns._ID;
		private static final String PostID = "postId";
		private static final String USER_ID = "userId";
		private static final String FULL_NAME = "fullName";
		private static final String PROFILE_PIC = "profilePic";
		private static final String IS_FOLLOWING = "isFollowing";
	}

	// Create Table Message
	private static final String CREATE_TABLE_COMMENT = "CREATE TABLE "
			+ TABLE_LIKE + "(" + Fields.LIKE_ID + " INTEGER PRIMARY KEY,"
			+ Fields.PostID + " INTEGER," + Fields.USER_ID + " INTEGER,"
			+ Fields.FULL_NAME + " TEXT," + Fields.PROFILE_PIC + " TEXT,"
			+ Fields.IS_FOLLOWING + " INTEGER" // DATETIME
			+ ")";

	public static LikeTable getInstance() {
		if (instance == null)
			instance = new LikeTable();
		return instance;
	}

	public LikeTable() {
		super(DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()), App.getInstance()
				.getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_COMMENT);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		return TABLE_LIKE;
	}

	@Override
	protected String[] getProjection() {
		return new String[] { Fields.LIKE_ID, Fields.PostID, Fields.USER_ID,
				Fields.FULL_NAME, Fields.PROFILE_PIC, Fields.IS_FOLLOWING };
	}

	@Override
	protected ContentValues getContentValues(PostLike entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.LIKE_ID, entity.getLikeId());
		values.put(Fields.USER_ID, entity.getUserId());
		values.put(Fields.FULL_NAME, entity.getFullName());
		values.put(Fields.PROFILE_PIC, entity.getProfilePicURL());
		values.put(Fields.IS_FOLLOWING, entity.isFollowing() ? 1 : 0);
		return values;
	}

	@Override
	protected PostLike getObjFromCursor(Cursor cursor) {
		PostLike like = new PostLike();
		like.setLikeId(cursor.getInt(cursor
				.getColumnIndex(Fields.LIKE_ID)));
		like.setUserId(cursor.getInt(cursor.getColumnIndex(Fields.USER_ID)));
		like.setFullName(cursor.getString(cursor
				.getColumnIndex(Fields.FULL_NAME)));
		like.setProfilePicURL(cursor.getString(cursor
				.getColumnIndex(Fields.PROFILE_PIC)));
		like.setFollowing(cursor.getInt(cursor
				.getColumnIndex(Fields.IS_FOLLOWING)) == 1);
		return like;
	}
}