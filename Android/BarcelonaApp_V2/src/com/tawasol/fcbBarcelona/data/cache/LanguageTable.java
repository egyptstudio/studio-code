package com.tawasol.fcbBarcelona.data.cache;
 
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase; 
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.LanguageEntity;
import com.tawasol.fcbBarcelona.entities.MessagesUsersEntity;

/**
 * @author Turki
 * 
 */
public class LanguageTable extends AbstractTable<LanguageEntity> {

	private static LanguageTable sInstance;
	
	// Table name
	private static final String TABLE_LANGUAGE = "Language";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String LANGUAGE_ID   = BaseColumns._ID;
		private static final String LANGUAGE_NAME = "languageName";
		private static final String LANGUAGE_CODE = "code"; 
	}
	
	// Create Table Message
	private static final String CREATE_TABLE_LANGUAGE  = "CREATE TABLE " + TABLE_LANGUAGE 
			+ "(" 
				+ Fields.LANGUAGE_ID    + " INTEGER PRIMARY KEY," 
				+ Fields.LANGUAGE_NAME  + " TEXT," 
				+ Fields.LANGUAGE_CODE  + " INTEGER" 
			+ ")";
		
	public static LanguageTable getInstance() {
		if (sInstance == null)
			sInstance = new LanguageTable();
		return sInstance;
	}
	
	private LanguageTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void create(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_LANGUAGE);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_LANGUAGE;
	}

	@Override
	protected String[] getProjection() {
		// TODO Auto-generated method stub
		return new String[] {Fields.LANGUAGE_ID, Fields.LANGUAGE_NAME, Fields.LANGUAGE_CODE};
	}

	@Override
	protected ContentValues getContentValues(LanguageEntity entity) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put(Fields.LANGUAGE_ID   , entity.getLanguageId());
	    values.put(Fields.LANGUAGE_NAME , entity.getLanguageName());
	    values.put(Fields.LANGUAGE_CODE , entity.getLanguageCode());
		return values;
	}

	@Override
	protected LanguageEntity getObjFromCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		LanguageEntity languageEntity = new LanguageEntity();
		languageEntity.setLanguageId(cursor.getInt(cursor.getColumnIndex(Fields.LANGUAGE_ID)));
		languageEntity.setLanguageName(cursor.getString(cursor.getColumnIndex(Fields.LANGUAGE_NAME)));
		languageEntity.setLanguageCode(cursor.getString(cursor.getColumnIndex(Fields.LANGUAGE_CODE)));
		return languageEntity;
	}
}










