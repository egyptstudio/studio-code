package com.tawasol.fcbBarcelona.data.cache;



import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.Seasons;

public class SeasonsTable extends AbstractTable<Seasons> {

	private static SeasonsTable sInstance;
	
	// Table name
	private static final String TABLE_SEASON = "Seasons";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String SEASON_ID      = "seasonId";
		private static final String SEASON_NAME         = "seasonnName";
		private static final String SEASON_RANK = "rank";
	}
	
	// Create Table Folders
	private static final String CREATE_TABLE_SEASONS  = "CREATE TABLE " + TABLE_SEASON 
			+ "(" 
				+ Fields.SEASON_ID + " INTEGER PRIMARY KEY," 
				+ Fields.SEASON_NAME      + " TEXT," 
				+ Fields.SEASON_RANK         + " INTEGER" 
			+ ")";
		
	public static SeasonsTable getInstance() {
		if (sInstance == null)
			sInstance = new SeasonsTable();
		return sInstance;
	}
	
	private SeasonsTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_SEASONS);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		return TABLE_SEASON;
	}

	@Override
	protected String[] getProjection() {
		return new String[] {Fields.SEASON_ID, Fields.SEASON_NAME, Fields.SEASON_RANK};
	}

	@Override
	protected ContentValues getContentValues(Seasons entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.SEASON_ID      , entity.getSeasonId());
	    values.put(Fields.SEASON_NAME         , entity.getSeasonName());
	    values.put(Fields.SEASON_RANK              , entity.getRank());
		return values;
	}

	@Override
	protected Seasons getObjFromCursor(Cursor cursor) {
		Seasons season = new Seasons();
		season.setSeasonId(cursor.getInt(cursor.getColumnIndex(Fields.SEASON_ID)));
		season.setSeasonName(cursor.getString(cursor.getColumnIndex(Fields.SEASON_NAME)));
		season.setRank(cursor.getInt(cursor.getColumnIndex(Fields.SEASON_RANK)));		
		return season;
	}
	
	public int getCount(){
		String query = "select * from "+TABLE_SEASON;
		Cursor cursor = DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()).getReadableDatabase().rawQuery(query, null);
		return cursor.getCount();
	}
	
	
}
