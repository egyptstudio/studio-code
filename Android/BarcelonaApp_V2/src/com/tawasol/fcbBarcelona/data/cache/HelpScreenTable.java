package com.tawasol.fcbBarcelona.data.cache;


import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.HelpScreen;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;

public class HelpScreenTable extends AbstractTable<HelpScreen> {

	private static HelpScreenTable sInstance;
	
	// Table name
	private static final String TABLE_HELP_SCREEN = "helpScreen";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String HLP_SCREEN_ID      = "screenId";
		private static final String HLP_LANGUAGE_CODE         = "languageCode";
		private static final String HLP_SCREEN_TITLE              = "screenTitle";
		private static final String HLP_SCREEN_IMG_URL      = "screenImageUrl";
		private static final String HLP_SCREEN_ICON_URL = "screenIconUrl";
		private static final String HLP_DESCRIPTION          = "description";
	}
	
	// Create Table Folders
	private static final String CREATE_TABLE_HELP_SCREEN  = "CREATE TABLE " + TABLE_HELP_SCREEN 
			+ "(" 
				+ Fields.HLP_SCREEN_ID + " INTEGER ," //PRIMARY KEY
				+ Fields.HLP_LANGUAGE_CODE      + " TEXT," 
				+ Fields.HLP_SCREEN_TITLE         + " TEXT," 
				+ Fields.HLP_SCREEN_IMG_URL              + " TEXT," 
				+ Fields.HLP_SCREEN_ICON_URL      + " TEXT,"  
				+ Fields.HLP_DESCRIPTION          + " TEXT" 
			+ ")";
		
	public static HelpScreenTable getInstance() {
		if (sInstance == null)
			sInstance = new HelpScreenTable();
		return sInstance;
	}
	
	private HelpScreenTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_HELP_SCREEN);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		return TABLE_HELP_SCREEN;
	}

	@Override
	protected String[] getProjection() {
		return new String[] {Fields.HLP_SCREEN_ID, Fields.HLP_LANGUAGE_CODE, Fields.HLP_SCREEN_TITLE,
				Fields.HLP_SCREEN_IMG_URL, Fields.HLP_SCREEN_ICON_URL, Fields.HLP_DESCRIPTION};
	}

	@Override
	protected ContentValues getContentValues(HelpScreen entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.HLP_SCREEN_ID      , entity.getScreenId());
	    values.put(Fields.HLP_LANGUAGE_CODE         , LanguageUtils.getInstance().getDeviceLanguage());
	    values.put(Fields.HLP_SCREEN_TITLE              , entity.getScreenTitle());
	    values.put(Fields.HLP_SCREEN_IMG_URL      , entity.getScreenImageUrl());
	    values.put(Fields.HLP_SCREEN_ICON_URL , entity.getScreenIconUrl());
	    values.put(Fields.HLP_DESCRIPTION          , entity.getDescription());
		return values;
	}

	@Override
	protected HelpScreen getObjFromCursor(Cursor cursor) {
		HelpScreen helpScreen = new HelpScreen();
		helpScreen.setScreenId(cursor.getInt(cursor.getColumnIndex(Fields.HLP_SCREEN_ID)));
		helpScreen.setLanguageCode(cursor.getString(cursor.getColumnIndex(Fields.HLP_LANGUAGE_CODE)));
		helpScreen.setScreenTitle(cursor.getString(cursor.getColumnIndex(Fields.HLP_SCREEN_TITLE)));
		helpScreen.setScreenImageUrl(cursor.getString(cursor.getColumnIndex(Fields.HLP_SCREEN_IMG_URL)));
		helpScreen.setScreenIconUrl(cursor.getString(cursor.getColumnIndex(Fields.HLP_SCREEN_ICON_URL)));
		helpScreen.setDescription(cursor.getString(cursor.getColumnIndex(Fields.HLP_DESCRIPTION)));
		
		return helpScreen;
	}

		public List<HelpScreen> getLocalizedHelpScreens(String languageCode /*int languageCode*/) {
			String queryString = "SELECT * "
					+ " FROM " + getTableName() + " "
					+ "WHERE " + Fields.HLP_LANGUAGE_CODE + " LIKE '" + languageCode + "' "
					+ " ORDER BY " + Fields.HLP_SCREEN_ID + " ASC";
			
			Cursor cursor = getDb().rawQuery(queryString, null);
			
			List<HelpScreen> helpScreens = null;
			
			if (cursor.moveToFirst()) {
				helpScreens = new ArrayList<HelpScreen>(cursor.getCount());
				do {
					helpScreens.add(getObjFromCursor(cursor));
				} while (cursor.moveToNext());
			}
			
			return helpScreens;
		}
	
}










