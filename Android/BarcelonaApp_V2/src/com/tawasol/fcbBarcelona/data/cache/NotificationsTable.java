package com.tawasol.fcbBarcelona.data.cache;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.Notification;

public class NotificationsTable extends AbstractTable<Notification> {

	private static NotificationsTable sInstance;

	// Table name
	private static final String TABLE_NOTIFICATIONS = "Notifications";

	private static final class Fields implements BaseColumns {
		// Column names
		private static final String NTF_NOTIFICATION_ID = BaseColumns._ID;//"notificationId";
		private static final String NTF_SENDER_ID = "senderId";
		private static final String NTF_SENDER_FULLNAME = "senderFullName";
		private static final String NTF_RECEIVER_ID = "receiverId";
		private static final String NTF_RECEIVER_FULLNAME = "receiverFullName";
		private static final String NTF_TIME = "time";
		private static final String NTF_POST_ID = "postId";
		private static final String NTF_NOTIFICATION_TYPE = "notificationType";
		private static final String NTF_READ_STATE = "readState";
		private static final String NTF_SENDER_PROFILE_PIC_URL = "senderProfilePicUrl";
	}

	// Create Table Folders
	private static final String CREATE_TABLE_NOTIFICATIONS = "CREATE TABLE "
			+ TABLE_NOTIFICATIONS + "(" + Fields.NTF_NOTIFICATION_ID
			+ " INTEGER PRIMARY KEY," + Fields.NTF_SENDER_ID + " INTEGER,"
			+ Fields.NTF_SENDER_FULLNAME + " TEXT," + Fields.NTF_RECEIVER_ID
			+ " INTEGER," + Fields.NTF_RECEIVER_FULLNAME + " TEXT,"
			+ Fields.NTF_TIME + " INTEGER," + Fields.NTF_POST_ID + " INTEGER,"
			+ Fields.NTF_READ_STATE + " INTEGER,"
			+ Fields.NTF_NOTIFICATION_TYPE + " INTEGER," +
			Fields.NTF_SENDER_PROFILE_PIC_URL + " TEXT" +")";

	public static NotificationsTable getInstance() {
		if (sInstance == null)
			sInstance = new NotificationsTable();
		return sInstance;
	}

	private NotificationsTable() {
		super(DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()), App.getInstance()
				.getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_NOTIFICATIONS);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	@Override
	protected String getTableName() {
		return TABLE_NOTIFICATIONS;
	}

	@Override
	public String[] getProjection() {
		return new String[] { Fields.NTF_NOTIFICATION_ID, Fields.NTF_SENDER_ID,
				Fields.NTF_SENDER_FULLNAME, Fields.NTF_RECEIVER_ID,
				Fields.NTF_RECEIVER_FULLNAME, Fields.NTF_TIME,
				Fields.NTF_POST_ID, Fields.NTF_NOTIFICATION_TYPE,Fields.NTF_READ_STATE,Fields.NTF_SENDER_PROFILE_PIC_URL };
	}

	@Override
	protected ContentValues getContentValues(Notification entity) {
		ContentValues values = new ContentValues();
		values.put(Fields.NTF_NOTIFICATION_ID, entity.getNotificationId());
		values.put(Fields.NTF_SENDER_ID, entity.getSenderId());
		values.put(Fields.NTF_SENDER_FULLNAME, entity.getSenderFullName());
		values.put(Fields.NTF_RECEIVER_ID, entity.getReceiverId());
		values.put(Fields.NTF_RECEIVER_FULLNAME, entity.getReceiverFullName());
		values.put(Fields.NTF_TIME, entity.getTime());
		values.put(Fields.NTF_POST_ID, entity.getPostId());
		values.put(Fields.NTF_NOTIFICATION_TYPE, entity.getNotificationType());
		values.put(Fields.NTF_READ_STATE, entity.isRead() ? 1 : 0);
		values.put(Fields.NTF_SENDER_PROFILE_PIC_URL, entity.getSenderImgUrl());

		return values;
	}

	@Override
	protected Notification getObjFromCursor(Cursor cursor) {
		Notification notification = new Notification();
		notification.setNotificationId(cursor.getInt(cursor
				.getColumnIndex(Fields.NTF_NOTIFICATION_ID)));
		notification.setSenderId(cursor.getInt(cursor
				.getColumnIndex(Fields.NTF_SENDER_ID)));
		notification.setSenderFullName(cursor.getString(cursor
				.getColumnIndex(Fields.NTF_SENDER_FULLNAME)));
		notification.setReceiverId(cursor.getInt(cursor
				.getColumnIndex(Fields.NTF_RECEIVER_ID)));
		notification.setReceiverFullName(cursor.getString(cursor
				.getColumnIndex(Fields.NTF_RECEIVER_FULLNAME)));
		notification.setTime(cursor.getInt(cursor
				.getColumnIndex(Fields.NTF_TIME)));
		notification.setPostId(cursor.getInt(cursor
				.getColumnIndex(Fields.NTF_POST_ID)));
		notification.setNotificationType(cursor.getInt(cursor
				.getColumnIndex(Fields.NTF_NOTIFICATION_TYPE)));
		notification.setRead(cursor.getInt(cursor.getColumnIndex(Fields.NTF_READ_STATE)) == 1);
		notification.setSenderImgUrl(cursor.getString(cursor
				.getColumnIndex(Fields.NTF_SENDER_PROFILE_PIC_URL)));

		return notification;
	}

	/**
	 * to filter notifications of types (LIKES , COMMENTS , FOLLOWIG , FRIEND
	 * REQUESTS) Only
	 * 
	 * @param Notification
	 *            Type ID {1, 2, 3 or 4}
	 * */
	public List<Notification> getFilteredNotifications(int Notificationtype) {
		Cursor cursor = getDb().query(
				getTableName(),
				getProjection(),
				/*Fields.NTF_RECEIVER_ID + " = "
						+ UserManager.getInstance().getCurrentUserId() + " and "
						+*/Fields.NTF_NOTIFICATION_TYPE + " = " + Notificationtype
						/*+ " GROUP BY " + Fields.NTF_SENDER_ID*/ + " ORDER BY "
						+ Fields.NTF_TIME + " DESC", null, null, null, null);

		List<Notification> notifications = new ArrayList<Notification>(
				cursor.getCount() >= 0 ? cursor.getCount() : 10/*
																 * Normal
																 * initial
																 * capacity for
																 * ArrayList
																 */);
		if (cursor.moveToFirst()) {
			do {
				notifications.add(getObjFromCursor(cursor));
			} while (cursor.moveToNext());
		}

		return notifications;
	}

	/**
	 * to filter Favorite Notifications Only of these types {Change profile ,
	 * Post New Photo , Add Friend} actions
	 * */
	public List<Notification> getFavoriteNotifications() {
		String whereQuery =
				/*Fields.NTF_RECEIVER_ID + " = "
				+ UserManager.getInstance().getCurrentUserId() + " and "
				+*/ Fields.NTF_NOTIFICATION_TYPE + " = "
				+ Notification.CHANGE_RROFILE_NOTIFICATIONS + " or "
				+ Fields.NTF_NOTIFICATION_TYPE + " = "
				+ Notification.POST_NEW_PHOTO_NOTIFICATIONS + " or "
				+ Fields.NTF_NOTIFICATION_TYPE + " = "
				+ Notification.ADD_FRIEND_NOTIFICATIONS /*+ " GROUP BY "
				+ Fields.NTF_SENDER_ID*/ + " ORDER BY " + Fields.NTF_TIME
				+ " DESC";
		Cursor cursor = getDb().query(getTableName(), getProjection(),
				whereQuery, null, null, null, null);

		List<Notification> notifications = new ArrayList<Notification>(
				cursor.getCount() >= 0 ? cursor.getCount() : 10/*
																 * Normal
																 * initial
																 * capacity for
																 * ArrayList
																 */);
		if (cursor.moveToFirst()) {
			do {
				notifications.add(getObjFromCursor(cursor));
			} while (cursor.moveToNext());
		}

		return notifications;
	}
	
	public int getUnreadNotificationsCount(){
		//String query = "select * from "+TABLE_NOTIFICATIONS;
		
		Cursor cursor = getDb().query(getTableName(), getProjection(),
				Fields.NTF_READ_STATE + " = " + 0, null, null, null, null);
		
		return cursor.getCount();
	}
	public int getFilteredUnreadNotificationsCount(int Notificationtype){
		Cursor cursor = getDb().query(getTableName(), getProjection(),
				Fields.NTF_READ_STATE + " = " + 0 + " and " + Fields.NTF_NOTIFICATION_TYPE + " = " + Notificationtype, null, null, null, null);
		
		return cursor.getCount();
	}
	
	public int getFavouritesUnreadCount(){
		Cursor cursor = getDb().query(getTableName(), getProjection(),
				Fields.NTF_READ_STATE + " = " + 0 + " and " + "(" + Fields.NTF_NOTIFICATION_TYPE + " = "
						+ Notification.CHANGE_RROFILE_NOTIFICATIONS + " or "
						+ Fields.NTF_NOTIFICATION_TYPE + " = "
						+ Notification.POST_NEW_PHOTO_NOTIFICATIONS + " or "
						+ Fields.NTF_NOTIFICATION_TYPE + " = "
						+ Notification.ADD_FRIEND_NOTIFICATIONS + ")", null, null, null, null);
		
		List<Notification> notifications = new ArrayList<Notification>(
				cursor.getCount() >= 0 ? cursor.getCount() : 10/*
																 * Normal
																 * initial
																 * capacity for
																 * ArrayList
																 */);
		if (cursor.moveToFirst()) {
			do {
				notifications.add(getObjFromCursor(cursor));
			} while (cursor.moveToNext());
		}
		
		System.out.println(notifications);
		
		return cursor.getCount();
	}
	
	public void setRead(int notificationId )
 {
		ContentValues values = new ContentValues();
		values.put(Fields.NTF_READ_STATE, 1);
		DatabaseManager
				.getInstance(App.getInstance().getApplicationContext())
				.getDb()
				.update(TABLE_NOTIFICATIONS,
						values,
						/*Fields.NTF_NOTIFICATION_TYPE + " = " + notificationType + " and " +*/ Fields.NTF_NOTIFICATION_ID + " = "+ notificationId,
						null);

	}
	
	public void setNotificationRead(int notificationType){
		ContentValues values = new ContentValues();
		values.put(Fields.NTF_READ_STATE, 1);
		DatabaseManager
				.getInstance(App.getInstance().getApplicationContext())
				.getDb()
				.update(TABLE_NOTIFICATIONS,
						values,
						/*Fields.NTF_NOTIFICATION_TYPE + " = " + notificationType + " and " +*/ Fields.NTF_NOTIFICATION_TYPE + " = "+ notificationType,
						null);

	}
	
	public void setFavRead(int notificationId)
	{
		ContentValues values = new ContentValues();
		values.put(Fields.NTF_READ_STATE, 1);
		DatabaseManager
				.getInstance(App.getInstance().getApplicationContext())
				.getDb()
				.update(TABLE_NOTIFICATIONS,
						values,
						Fields.NTF_NOTIFICATION_TYPE + " = "
								+ Notification.CHANGE_RROFILE_NOTIFICATIONS + " or "
								+ Fields.NTF_NOTIFICATION_TYPE + " = "
								+ Notification.POST_NEW_PHOTO_NOTIFICATIONS + " or "
								+ Fields.NTF_NOTIFICATION_TYPE + " = "
								+ Notification.ADD_FRIEND_NOTIFICATIONS + " or "
								+ Fields.NTF_NOTIFICATION_ID + " = " + notificationId,
						null);
	}
}
