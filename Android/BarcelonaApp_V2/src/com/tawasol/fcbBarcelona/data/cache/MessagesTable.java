package com.tawasol.fcbBarcelona.data.cache;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.Message;

/**
 * @author Turki
 * 
 */
public class MessagesTable extends AbstractTable<Message> {

	private static MessagesTable sInstance;
	
	// Table name
	private static final String TABLE_MESSAGE = "Messages";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String MSG_REMOTE_MESSAGE_ID = BaseColumns._ID;
		private static final String MSG_MESSAGE_TEXT      = "messageText";
		private static final String MSG_FRIEND_ID         = "friendID";
		private static final String MSG_SENT              = "sent";
		private static final String MSG_MESSAGE_TIME      = "messageTime";
		private static final String MSG_RECEVIED          = "recevied";
		
		/** Add to solve fuck issue **/
		private static final String MSG_FRIEND_NAME         = "friendName";
		private static final String MSG_FRIEND_PIC          = "friendPic";
	}
	
	// Create Table Message
	private static final String CREATE_TABLE_MESSAGE  = "CREATE TABLE " + TABLE_MESSAGE 
			+ "(" 
				+ Fields.MSG_REMOTE_MESSAGE_ID + " INTEGER PRIMARY KEY," 
				+ Fields.MSG_MESSAGE_TEXT      + " TEXT," 
				+ Fields.MSG_FRIEND_ID         + " INTEGER," 
				+ Fields.MSG_SENT              + " INTEGER," 
				+ Fields.MSG_MESSAGE_TIME      + " INTEGER,"  // DATETIME
				+ Fields.MSG_RECEVIED          + " INTEGER," 
				+ Fields.MSG_FRIEND_NAME       + " TEXT," 
				+ Fields.MSG_FRIEND_PIC        + " TEXT" 
			+ ")";
		
	public static MessagesTable getInstance() {
		if (sInstance == null)
			sInstance = new MessagesTable();
		return sInstance;
	}
	
	private MessagesTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
	}

	@Override
	protected void create(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_MESSAGE);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_MESSAGE;
	}

	@Override
	protected String[] getProjection() {
		// TODO Auto-generated method stub
		return new String[] {Fields.MSG_REMOTE_MESSAGE_ID, Fields.MSG_MESSAGE_TEXT, Fields.MSG_FRIEND_ID,
				Fields.MSG_SENT, Fields.MSG_MESSAGE_TIME, Fields.MSG_RECEVIED, Fields.MSG_FRIEND_NAME, 
				Fields.MSG_FRIEND_PIC};
	}

	@Override
	protected ContentValues getContentValues(Message entity) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put(Fields.MSG_MESSAGE_TEXT      , entity.getText());
	    values.put(Fields.MSG_FRIEND_ID         , entity.getFriendId());
	    values.put(Fields.MSG_SENT              , entity.getSent());	
	    values.put(Fields.MSG_MESSAGE_TIME      , entity.getMessageTime());
	    values.put(Fields.MSG_REMOTE_MESSAGE_ID , entity.getMessageId());
	    values.put(Fields.MSG_RECEVIED          , entity.getReceived());
	    values.put(Fields.MSG_FRIEND_NAME       , entity.getSenderName());
	    values.put(Fields.MSG_FRIEND_PIC        , entity.getSenderPicture());
		return values;
	}

	@Override
	protected Message getObjFromCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		Message message = new Message();
		message.setText(cursor.getString(cursor.getColumnIndex(Fields.MSG_MESSAGE_TEXT)));
		message.setFriendId(cursor.getInt(cursor.getColumnIndex(Fields.MSG_FRIEND_ID)));
		message.setSent(cursor.getInt(cursor.getColumnIndex(Fields.MSG_SENT)));
		message.setMessageTime(cursor.getLong(cursor.getColumnIndex(Fields.MSG_MESSAGE_TIME)));
		message.setMessageId(cursor.getInt(cursor.getColumnIndex(Fields.MSG_REMOTE_MESSAGE_ID)));
		message.setReceived(cursor.getInt(cursor.getColumnIndex(Fields.MSG_RECEVIED)));

		message.setSenderName(cursor.getString(cursor.getColumnIndex(Fields.MSG_FRIEND_NAME)));
		message.setSenderPicture(cursor.getString(cursor.getColumnIndex(Fields.MSG_FRIEND_PIC)));
		return message;
	}

	private String getProjectionAsCommaSeparatedString(String[] projection) {
		if (projection == null) throw new RuntimeException("Projection cannot be null");
		
		String value = "";
		
		for (int i = 0; i < projection.length; i++) {
			if (!isFirstOrLastElement(i, projection.length))
				value += ", ";
			value += projection[i];
		}
		return value;
	}
	
	private boolean isFirstOrLastElement(int i, int length) {
		/** Add by turki **/
//		return (i == 0) || (i == (length - 1));  /** outPut: _id, messageText, friendID, sent, messageTimerecevied **/
		return (i == 0);						 /** outPut: _id, messageText, friendID, sent, messageTime, recevied **/
	}

	// TODO: not fully implemented
	public List<Message> getLatestMessages() {
		String queryString = "SELECT DISTINCT " + getProjectionAsCommaSeparatedString(getProjection())
				+ " FROM " + getTableName() + " "
				+ "GROUP BY " + Fields.MSG_FRIEND_ID + " ORDER BY " + BaseColumns._ID + " DESC";
		
		Cursor cursor = getDb().rawQuery(queryString, null);
		
		List<Message> msgs = null;
		
		if (cursor.moveToFirst()) {
			msgs = new ArrayList<Message>(cursor.getCount());
			do {
				msgs.add(getObjFromCursor(cursor));
			} while (cursor.moveToNext());
		}
		
		return msgs;
	}
	
	public int getCount(){
		String query = "select * from "+TABLE_MESSAGE;
		Cursor cursor = DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()).getReadableDatabase().rawQuery(query, null);
		return cursor.getCount();
	}
}










