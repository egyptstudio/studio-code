package com.tawasol.fcbBarcelona.data.cache;
 
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase; 
import android.provider.BaseColumns;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.MessagesUsersEntity;

/**
 * @author Turki
 * 
 */
public class MessagesUsersTable extends AbstractTable<MessagesUsersEntity> {

	private static MessagesUsersTable sInstance;
	
	// Table name
	private static final String TABLE_MESSAGES_USERS = "MessagesUsers";
	
	private static final class Fields implements BaseColumns {
		// Column names
		private static final String MSG_U_FRIEND_ID   = BaseColumns._ID;
		private static final String MSG_U_USER_NAME   = "userName";
		private static final String MSG_U_PROFILE_PIC = "profilePic";
		private static final String MSG_U_TYPE        = "type";
	}
	
	// Create Table Message
	private static final String CREATE_TABLE_MESSAGES_USERS  = "CREATE TABLE " + TABLE_MESSAGES_USERS 
			+ "(" 
				+ Fields.MSG_U_FRIEND_ID    + " INTEGER PRIMARY KEY," 
				+ Fields.MSG_U_USER_NAME    + " TEXT," 
				+ Fields.MSG_U_PROFILE_PIC  + " TEXT," 
				+ Fields.MSG_U_TYPE         + " INTEGER" 
			+ ")";
		
	public static MessagesUsersTable getInstance() {
		if (sInstance == null)
			sInstance = new MessagesUsersTable();
		return sInstance;
	}
	
	private MessagesUsersTable() {
		super(DatabaseManager.getInstance(App.getInstance().getApplicationContext()),
				App.getInstance().getApplicationContext());
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void create(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_MESSAGES_USERS);
	}

	@Override
	protected void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return TABLE_MESSAGES_USERS;
	}

	@Override
	protected String[] getProjection() {
		// TODO Auto-generated method stub
		return new String[] {Fields.MSG_U_FRIEND_ID, Fields.MSG_U_USER_NAME, Fields.MSG_U_PROFILE_PIC, Fields.MSG_U_TYPE};
	}

	@Override
	protected ContentValues getContentValues(MessagesUsersEntity entity) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put(Fields.MSG_U_FRIEND_ID   , entity.getFanID());
	    values.put(Fields.MSG_U_USER_NAME   , entity.getFanName());
	    values.put(Fields.MSG_U_PROFILE_PIC , entity.getFanPicture());
	    values.put(Fields.MSG_U_TYPE        , entity.getType());
		return values;
	}

	@Override
	protected MessagesUsersEntity getObjFromCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		MessagesUsersEntity msgUsersEntity = new MessagesUsersEntity();
		msgUsersEntity.setFanID(cursor.getInt(cursor.getColumnIndex(Fields.MSG_U_FRIEND_ID)));
		msgUsersEntity.setFanName(cursor.getString(cursor.getColumnIndex(Fields.MSG_U_USER_NAME)));
		msgUsersEntity.setFanPicture(cursor.getString(cursor.getColumnIndex(Fields.MSG_U_PROFILE_PIC)));
		msgUsersEntity.setType(cursor.getInt(cursor.getColumnIndex(Fields.MSG_U_TYPE)));
		return msgUsersEntity;
	}
	
	public int getCount(){
		String query = "select * from "+TABLE_MESSAGES_USERS;
		Cursor cursor = DatabaseManager.getInstance(App.getInstance()
				.getApplicationContext()).getReadableDatabase().rawQuery(query, null);
		return cursor.getCount();
	}
}










