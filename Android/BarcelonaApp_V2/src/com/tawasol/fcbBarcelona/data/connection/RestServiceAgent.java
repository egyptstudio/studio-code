package com.tawasol.fcbBarcelona.data.connection;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

/**
 * @author Morabea
 */
public class RestServiceAgent {
	
	/**
	 * @param baseUrl
	 * @param method
	 * @param getParams
	 * @return
	 * @throws ClientProtocolException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static String get(String baseUrl, String method, Map<String, String> getParams) 
			throws ClientProtocolException, UnsupportedEncodingException, IOException {
		return RESTRequestFactory.newInstance(baseUrl).performGetRequest(method, getParams);
	}
	
	/**
	 * @param baseUrl
	 * @param method
	 * @param postParams
	 * @return
	 * @throws ClientProtocolException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static String post(String baseUrl, String method, Map<String, Object> postParams) 
			throws ClientProtocolException, UnsupportedEncodingException, IOException {
		return RESTRequestFactory.newInstance(baseUrl).performPostRequest(method, postParams);
	}
	
}
