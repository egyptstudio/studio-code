package com.tawasol.fcbBarcelona.data.connection;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;

public abstract class BaseTask extends AsyncTask<Object, Integer, Object> {

	@SuppressLint("NewApi")
	public void executePerApi() {
		if (Build.VERSION.SDK_INT >= 11) {
			executeOnExecutor(THREAD_POOL_EXECUTOR);
		} else {
			execute();
		}
	}
	
}
