package com.tawasol.fcbBarcelona.data.connection;

/**
 * URLs contract.
 * 
 * @author Morabea
 * 
 */
public class URLs {
	/*public static final String BASE_URL = 
										 * "http://api.tawasoldev.com/barca/index.php/api/barca"
										 * ;
										 "http://barca.tawasoldev.com/barca/index.php/api/";*/
			/*"http://moh2013.com/Tawasol/"*/;

	public static final class Methods {
		public static final String LOGIN_METHOD = "user/login";
		public static final String SEND_VERIFICATION_METHOD = "user/sendVerificationCode";
		public static final String CHANGE_PASSWORD_METHOD = "user/changePassword";
		public static final String LOGIN_SOCIAL_MEDIA = "user/loginSocialMedia";
		public static final String GET_COUNTRIES = "user/getCountryList";
		public static final String GET_STUDIO_FOLDERS = "studio/getStudioFolders";
		public static final String GET_STUDIO_FOLDER_PHOTOS = "studio/getStudioFolderPhotos";
		public static final String GET_PURCHASED_PHOTOS = "studio/getUserPurchasedPhotos";
		public static final String GET_SEASONS = "studio/getSeasons";
		public static final String SHARE_TO_WALL = "studio/addPost";
		public static final String GET_FANS = "fans/loadFans";
		public static final String GET_FAN = "getFan";
		public static final String GET_STUDIO_PHOTO = "getStudioPhoto";
		public static final String GET_TOP_FANS = "getTopFans";
		public static final String UPDATE_PROFILE_PIC = "updateProfilePic";
		public static final String REGISTER_METHOD = "user/registerUser";
		public static final String GET_FCB_PHOTOS = "user/getFCBPhotos";
		public static final String SEND_SMS_INVITATION = "user/sendInviteSMS";
		public static final String UPLOAD_USER_PHOTOS = "user/uploadUserPhoto";
		public static final String FOLLOW_FAN = "wall/followFan";
		public static final String UNFOLLOW_FAN = "unfollowFan";
		public static final String ADD_FRIEND = "addFriendRequest";
		public static final String ACCEPT_FRIEND = "acceptFriendRequest";
		public static final String REJECT_FRIEND = "rejectFriendRequest";
		public static final String ADD_FAVORITE_FRIEND = "addFavoriteFriend";
		public static final String REMOVE_FAVORITE_FRIEND = "removeFavoriteFriend";
		public static final String REMOVE_FRIEND = "removeFriend";
		public static final String BLOCK_FAN = "blockFan";
		public static final String UNBLOCK_FAN = "unBlockFan";
		public static final String UPDATE_USER_STATUS = "updateStatus";
		public static final String GET_LATEST = "getLatest";
		public static final String GET_WALL = "getWall";
		public static final String GET_MY_GALLERY = "getMyGallery";
		public static final String GET_TOP_TEN = "getTopTen";
		public static final String ADD_COMMENT = "wall/addComments";
		public static final String LIKE_PHOTO = "likePhoto";
		public static final String DISLIKE_PHOTO = "dislikePhoto";
		public static final String GET_COMMENTS = "wall/getComments";
		public static final String LIKE_POST = "wall/likePost";
		public static final String DISLIKE_POST = "wall/disLikePost";
		public static final String REPOST = "wall/rePost";
		public static final String REPORT = "wall/rePortPost";
		public static final String DELETEPOST = "wall/deletePost";
		public static final String DELETECOMMENT = "wall/deleteComment";
		public static final String GET_TAG_POSTS = "wall/getTagPosts";
		public static final String GET_LIKE_DETAILS = "wall/getLikeDetails";
		public static final String GET_USER_CREDIT = "studio/getUserCredit";
		public static final String UPDATE_PROFILE = "profile/updateUserProfile";
		public static final String UPDATE_USER_CREDIT = "studio/updateUserCredit";
		public static final String GET_CHAT_FRIENDS = "chat/loadFriends";
		public static final String GET_CHAT_MESSAGES = "chat/sendMessage";
		public static final String GET_CITIES = "user/getCountryCities";
		public static final String GET_MY_POSTS = "wall/getMyPosts";
		public static final String GET_CHAT_DELETE_MESSAGES = "chat/deleteMessage";
		public static final String GET_CHAT_CHANGE_STATUS = "chat/changeStatus";
		public static final String GET_PREMIUM_BAR = "fans/loadPremuimBar";
		public static final String CHANGE_FOLLOW = "fans/changeFollow";
		public static final String CHANGE_FAVOURATE = "fans/changeFavorite";
		public static final String GET_SUGGESTED_FANS = "fans/getSuggestionsFans";
		public static final String GET_PUBLIC_PROFILE = "fans/getPublicProfileFan";
		public static final String GET_FRIENDS_REQUEST = "fans/loadFriendRequests";
		public static final String CHANGE_FRIEND = "fans/changeFriend";
		public static final String LOAD_FOLLOWING_FOR_FAN = "fans/loadFollowingForFan";
		public static final String LOAD_FOLLOWERS_FOR_FAN = "fans/loadFollowersForFan";
		public static final String CONTACT_US = "settings/sendContactUs";
		public static final String CHANGE_PASSWORD = "settings/changePassword";

		public static final String DECLINE_FRIENDS = "fans/declineFriendRequests";
		public static final String LOAD_CONTACTS = "fans/ loadContacts";
		public static final String WHO_INSTALLED_APP = "fans/whoInstalledApp";
		public static final String VERIFY_EMAIL = "Profile/verifyUserEmail";
		public static final String VERIFY_PHONE = "Profile/verifyUserPhone";

		public static final String GET_SUPPORTED_LANGUAGE = "settings/getSupportedLanguages";
		public static final String SMS_Verification = "Profile/sendSMSverificationCode";

		public static final String LOAD_CHAT_CONTACT = "chat/loadChatContacts";

		public static final String GET_HELP_SCREENS = "help/getHelpScreen";

		public static final String GET_CONTEST = "contest/getContest";

		public static final String GET_INFO = "settings/info_links";
		public static final String CHANGE_BLOCK = "fans/changeBlock";
		
		
		public static final String GET_MY_POINTS = "store/getMyPoints";
		public static final String LOAD_FOLLOWING_SUGGESTIONS = "fans/loadFollowingSuggestion";
		public static final String SUBSCRIPE_IN_PREMIUM = "store/subscripeInPremium";
		public static final String MY_PREMIUM_STATE = "store/MyPremiumStatus";
		public static final String LOAD_IN_APP_PRODUCTS = "store/loadInAppProductIds";
		
		public static final String TEST_SHOP = "LoadInAppProducts.php";
		
		public static final String GET_NOTIFICATIONS="notification/getNotifications";
		public static final String GET_Studio_photo_Details="studio/getStudioPhoto";
		public static final String EDIT_POST="wall/EditPost";
		public static final String GET_TAGS = "studio/getTags";
		public static final String PURCHASE="studio/purchaseStudioPhoto";
		public static final String SEND_POSTER="studio/sendPosterRequest";
		public static final String SHARE_SOCIAL_MEDIA="studio/shareSocailMedia";
		public static final String GET_POST="wall/getpost";
		public static final String GET_USER="user/getUserByID";
		public static final String SYNC_CHAT_BASIC_DATA = "chat/syncBasicData";
		public static final String GET_BASE_URL = "version/getBaseURL";
		public static final String INVITE_FRIENDS="settings/sendInviteInvitationText";		
		public static final String GET_BLOCKED_USERS = "settings/getBlockedUsers";
		public static final String UNBLOCK_USER = "settings/unblockUser";
		public static final String FILTER_USER_CONTACTS = "settings/filterUserContacts";
		public static final String CHECK_STUDIO_VERSION = "studio/getLibVer";
		public static final String FANS_INVITE_TEXT = "user/generateInviteToken";
		public static final String FAQ_LINK = "settings/getLinks";
		public static final String GET_SHOP_URL = "store/getStoreLink";
		public static final String ACTIVATE_USER = "user/userIsActive";
		public static final String DEACTIVATE_USER = "user/userIsNotActive";
		public static final String CLEAR_NOTIFICATIONS = "notification/clearNotification";

	}

}
