package com.tawasol.fcbBarcelona.data.connection;

public abstract class Params {
	
	public static final String STATUS = "status";
	public static final String DATA = "data";
	public static final String COMMENT_ID = "commentId";
	public static final int APP_LOGIN    = 1;
	public static final int SOCIAL_LOGIN = 2;
	public static final class Error {
		public static final String MSG = "message";
	}

	public static final class BusinessObject {
		public static final String ID = "id";
	}
	
	public static final class Common {
		public static final String BASE_64_KEY = "MIIBIhcbdcbhbcxkljnCAQ8AMIIBCgKCAQEAiCh92usJsiBOz6q5lCHvvzM184j1VEMBRlnu7T2BZyzSJI53mq/BqlYdjFpef0YikfxxoNT3oty/vCtaqCZ4t9+w7xCZE0EdUeHsB+ERX5nOX1u9D/u+6+5pZP8B5UiQQjW/l9NWkaWgh4k9i+/cgIpCgqtG2pui80xUU1mtmwDzNZgBD+2nJtk2+Pf5nVBcHv2ZdA5yDFY8mGgPcWU9E98Ihu1HS4GhLjm+Vi+/5166ARRAdn58XUo4W9YoUz7wvh8TydF5j5U7lQc4JUTbpDF45On8EZBf2Bdf4i3BaaCuMsefL3+tg29GqhVFcKcd/m4jvY7J/qhfgLaJJf4T8wIDAQAB";
		public static final String LANG = "lang";
		public static final String DEVICE_TYPE = "deviceType";
		public static final String ANDROID = "Android";
		public static final String NAME = "name";
	}
	
	public static final class ValidationRule {
		public static final String RULE_NAME = "ruleName";
		public static final String ERROR_MESSAGE = "errorMessage";
	}
	
	public static final class Studio {
		public static final String USER_ID = "userId";
		public static final String FOLDER_ID = "folderId";
		public static final String CATEGORY_ID = "categoryId";
		public static final String PAGE_NUMBER = "pageNo";
		public static final String SORT_BY = "sortBy";
		public static final String SEASONS = "seasons";
		public static final String STUDIO_PHOTO_ID = "studioPhotoId";
		public static final String SEASONLIST = "seasonList";
		public static final String FILTERBY = "filterBy";
		public static final String USER_EMAIL="userEmail";
		public static final String USER_PHOTO="userPhoto";
		public static final String MASKED_PHOTO="maskedPhoto";
		public static final String QUANTITY="quantity";
	}
	
	public static final class User {
		public static final String MALE = "m";
		public static final String FEMALE = "f";
		
		public static final String USER_ID = "userId";
		public static final String PROFILE_PICTURE = "profilePic";
		public static final String USER_NAME = "fullName";
		public static final String REGISTRATION_USER_NAME = "fullname";
		public static final String PHOTO_URL = "photoURL";
		public static final String PASSWORD = "password";
		public static final String EMAIL = "email";
		public static final String DATE_OF_BIRTH = "dateOfBirth";
		public static final String GENDER = "gender";
		public static final String NOOFPICS = "noOfPics";
		public static final String USERTYPE = "userType";
		public static final String SOCIALMEDIAID = "socialMediaId";
		public static final String COUNTRY_ID = "countryId";
		public static final String IS_ONLINE = "isOnline";
		public static final String LAST_LOGIN_DATE = "lastLoginDate";
		public static final String IS_PREMIUM = "isPremium";
		public static final String REGIDTERATION_DATE = "registerationDate";
		public static final String BRIEF ="brief";
		public static final String lATITUDE ="latitude";
		public static final String LONGITUDE ="longitude";
		public static final String CREDIT ="credit";
		public static final String ALLOW_LOCATION ="allowLocation";
		public static final String COUNTRY_NAME ="countryName";
		public static final String FOLLOWING_COUNT ="followingCount";
		public static final String FOLLOWERS_COUNT ="followersCount";
		public static final String PHONE="phone";
		public static final String LOGIN_COUNTRY="country";
		public static final String LOGIN_CITY="city";
		public static final String DISTRICT="district";
		public static final String LOGIN_BIRTHDAY="birthday";
		public static final String LOGIN_PROFILE_PIC_URL="profilePicURL";
		public static final String VERIFICATION_CODE="verificationCode";
		public static final String NEW_PASSOWRD="newPassword";
		public static final String DEVICE_TOKEN="deviceToken";
		public static final String DEVICE_TYPE="deviceType";
		public static final String USER_CREDIT="creditValue";
		public static final String USER_MODEL="user";
	
	}
	
	public static final class Fans{
		public static final String USER_ID = "userID";
		public static final String STARTINGLIMIT = "startingLimit";
		public static final String MALE_FEMALE_FILTER = "maleFemaleFilter";
		public static final String ONLINE_OFFLINE_FILTER = "onlineOfflineFilter";
		public static final String FAVOURATE_FILTER = "favoritesFilter";
		public static final String COUNTRY_FILTER = "countryFilter";
		public static final String ONLY_PREMIUM = "onlyPremuim";
		public static final String SORT_BY = "sortedBy";
		public static final String KEYWORD = "keyword";
		public static final String FOLLOW = "follow";
		public static final String FAVOURATE = "favorite";
		public static final String FRIEND = "friend";
		public static final String BLOCK = "block";
		public static final String PAGE_NUM = "pageNo";
		public static final String FILTERS = "filters";
		public static final String COUNTRIES_ARRAY = "countries";
		public static final String FAN_ID = "fanID";
		public static final String SEARCH_TEXT = "searchText";
		public static final String CONTACTS = "contacts";
		public static final String FOLLOWING_FILTER = "followingFilter";
		public static final String INVITEMSG_TEXT = "messageText";
	}
	
	public static final class Wall{
		public static final String USER_ID = "userId";
		public static final String CURRENT_USER_ID = "currentUserId";
		public static final String PAGE_NUM = "pageNo";
		public static final String PHOTO_ID = "photoId";
		public static final String POST_ID = "postId";
		public static final String DATE = "date";
		public static final String NUM_OF_POSTS = "n";
		public static final String LAST_REQUEST_NUM = "lastRequestTime";
		public static final String TIME_FILTER = "timeFilter";
		public static final String TAG_ID = "tagId";
		public static final String TAG_Name = "tagName";
		public static final String FAN_ID = "fanId";
		public static final String COUNTRYLIST = "countryList";
		public static final String FAVO_POST_FLAG = "favoritePostsFlag";
		public static final String COMMENT_ID = "commentId";
		public static final String REFRESH = "refresh";
		public static final String CONTACT_LIST = "contacts";
		public static final String ADS_UPDATE_CREDIT = "creditValue";		
	}
	public static class Share{
		public static final String PHOTO = "photo";
		public static final String USER_ID = "userId";
		public static final String STUDIO_PHOTO_ID = "studioPhotoId";
		public static final String CAPTION = "caption";
		public static final String PRIVACY = "privacy";
		public static final String TAGS = "tags";
		public static final String POST="post";
	}
	public static final class SEASON{
		public static final String SEASON_ID = "seasonId";
		public static final String SEASON_NAME = "seasonName";
	}
	
	public static final class IMAGE_DETAILS{
		public static final String USER_PHOTO_ID = "userPhotoId";
		public static final String Mehtod = "deletePhoto";
		public static final String USER_ID = "userId";
		public static final String PAGE_NUM = "pageNo";
		public static final String PHOTO_ID = "photoId";
		public static final String POST_ID = "postId";
	}

	public static final class UPLOAD_USER_PHOTO{
		public static final String USER_PHOTO = "photo";
		public static final String USER_ID = "userId";
		public static final String FCB_PHOTO_OBJECT ="FCBPhotoURL";
		public static final String FCB_BUNDLE ="BUNDLE";
		public static final String FCB_LIST_NAME = "FCBList";
		public static final int FCB_RANDOM_IMAGE = 1;
		public static final int FCB_SELECT_IMAGE = 2;
		public static final int FCB_SET_RANDOM_IMAGE_BEFORE_REGISTER = 3;
	}
	
	public static final class SMS_INVITATION{
		public static final String SMS_NUMBER = "phoneNumbers";
		public static final String SMS_TEXT = "invitationText";
		public static final String INVITATION_FLAG = "Finish Invitation";
		public static final String USER_ID = "userId";

		public static final String PHONES_BUNDLE ="Phones BUNDLE";
		public static final String PHONES_LIST_NAME = "PhonesList";
	}
	
	public static final class Chat {
		public static final String USER_ID = "myID";
		public static final String LAST_TIME_STAMP_CHECKED = "lastTimeStampChecked";
		
		public static final String LOAD_FRIENDS_USER_ID        = "userID";
		public static final String LOAD_FRIENDS_FILTER         = "filter";
		public static final String LOAD_FRIENDS_STARTING_LIMIT = "startingLimit";
		
		public static final String SEND_MESSAGE_MY_ID          = "myID";
		public static final String SEND_MESSAGE_FAN_ID         = "fanID";
		public static final String SEND_MESSAGE_MSG_TEXT       = "messageText";
		
		public static final String STORED_MESSAGE_ID           = "storedMessageID";
		
		public static final String DELETED_MESSAGE_ID          = "messageID";
		
		public static final String CHANGE_STATUS_ID            = "myID";
		public static final String CHANGE_STATUS               = "online";
		
		public static final String CONTACT_MODEL               = "contacts";
		public static final String CONTACT_USER_ID             = "userId";
		
		public static final String CONTACT_FAN_IDS             = "fanIDS";
	}
	
	public static final class Settings {
		public static final String EMAIL         = "email";
		public static final String MESSAGE_TITLE = "messageTitle";
		public static final String MESSAGE_TEXT  = "messageText";
		
		public static final String USER_ID          = "userId";
		public static final String CURRENT_PASSWORD = "currentPassword";
		public static final String OLD_PASSWORD     = "oldPassword";
	}
	
	public static final class SMSVerification {
		public static final String SMS_VER_CODE         = "code";
	}
	public static final class Contest {
		public static final String USER_ID          = "userId";
	}
	public static final class Info{
		public static final String PAGE_NAME          = "page";
		public static final String NAME          = "name";
	}
	
	public static final class Shop{
		public static final String USER_ID          = "userID";
		public static final String POINTS_BOUGHT    = "pointsBought";
		public static final String SUBSCRIPTION_DURATION  = "subscriptionDuration";
		public static final String NAME = "name";
		public static final String SURVY = "survey";
		public static final String POINTS = "points";
	}
	
	public static final class Notifications{
		public static final String USER_ID="userId";
		public static  final String LAST_REQUEST_TIME="lastRequestTime";
		public static final String NOTIFICATION_ID="notificationId";
		public static final String PAGE_NO="pageNo";
		
	}
	
}
