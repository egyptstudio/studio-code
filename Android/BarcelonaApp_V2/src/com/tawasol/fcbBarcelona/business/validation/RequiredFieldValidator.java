package com.tawasol.fcbBarcelona.business.validation;

import com.tawasol.fcbBarcelona.exception.AppException;

public class RequiredFieldValidator extends ValidationRule{

	String errorMessage,Field;
	public RequiredFieldValidator ( String Field,String errorMessage)
	{
		this.Field = Field;
		this.errorMessage = errorMessage;
		passed = true;
		
	}
	@Override
	public void validateRule() throws AppException {
		if (Field.length() == 0 || Field.equals("") )
		{
			passed  =false;	
			throw new AppException(errorMessage);
		}
	}

}
