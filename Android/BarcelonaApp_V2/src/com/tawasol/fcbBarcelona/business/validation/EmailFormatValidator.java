package com.tawasol.fcbBarcelona.business.validation;

import com.tawasol.fcbBarcelona.exception.AppException;


public class EmailFormatValidator extends ValidationRule {
	private String email;

	public EmailFormatValidator(String email, String errorMessage) {
		this.email = email;
		this.errorMessage = errorMessage;
	}

	@Override
	public void validateRule() throws AppException {
		if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
			passed = false;
			throw new AppException(errorMessage);
		}
	}

}
