package com.tawasol.fcbBarcelona.business.validation;

import com.tawasol.fcbBarcelona.exception.AppException;

public class EmptyObjectValidator extends ValidationRule {
	private Object object;

	public EmptyObjectValidator(Object object, String errorMessage) {
		this.object = object;
		this.errorMessage = errorMessage;
	}

	@Override
	public void validateRule() throws AppException {
		if (object == null) {
			passed = false;
			throw new AppException(errorMessage);
		}
	}

}
