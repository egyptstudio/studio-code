package com.tawasol.fcbBarcelona.business.validation;

import com.tawasol.fcbBarcelona.exception.AppException;


public abstract class ValidationRule {

	public String ruleName;
	public String errorMessage;
	public boolean passed; 
	
	public abstract void validateRule() throws AppException; 
	
}