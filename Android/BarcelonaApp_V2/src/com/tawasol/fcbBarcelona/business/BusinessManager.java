package com.tawasol.fcbBarcelona.business;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.connection.ParameterGenerator;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.data.connection.RestServiceAgent;
import com.tawasol.fcbBarcelona.data.helper.DataHelper;
import com.tawasol.fcbBarcelona.data.helper.VersionController;
import com.tawasol.fcbBarcelona.data.parsing.ParsingManager;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnEntityListReceivedListener;
import com.tawasol.fcbBarcelona.listeners.OnEntityReceivedListener;
import com.tawasol.fcbBarcelona.listeners.OnSuccessVoidListener;
import com.tawasol.fcbBarcelona.listeners.UiListener;
import com.tawasol.fcbBarcelona.responses.BaseResponse;

/**

 * @author Morabea
 * 
 * @param <T>
 *            An entity type with an "is-a" relationship with
 *            {@linkplain BusinessObject}.
 */
public abstract class BusinessManager<T extends BusinessObject> {

	private Class<T> mClass;

	private List<Action> mActionsList;

	private List<UiListener> Uilisteners;

	/**
	 * @param clazz
	 */
	protected BusinessManager(Class<T> clazz) {
		this.mClass = clazz;
		mActionsList = new ArrayList<Action>();
		Uilisteners = new ArrayList<UiListener>();
	}

	/**
	 * @param listener
	 */
	public void addListener(UiListener listener) {
		if (!Uilisteners.contains(listener))
			Uilisteners.add(listener);
	}

	/**
	 * @param listener
	 */
	public void removeListener(UiListener listener) {
		if (!App.appEnterBackGround) {
			if (Uilisteners.contains(listener))
				Uilisteners.remove(listener);
		}
	}

	/**
	 * @return
	 */
	protected List<UiListener> getListeners() {
		return Uilisteners;
	}

	protected <D extends OnSuccessVoidListener> void notifyVoidSuccess(
			final Class<D> listenerClass) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener listener : getListeners())
					if (listenerClass.isInstance(listener))
						((OnSuccessVoidListener) listener).onSuccess();
			}
		});
	}

	@SuppressWarnings("hiding")
	protected <T, D extends OnEntityReceivedListener<?>> void notifyEntityReceviedSuccess(
			final T entity, final Class<D> listenerClass) {
		App.getInstance().runOnUiThread(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				for (UiListener listener : getListeners())
					if (listenerClass.isInstance(listener))
						((OnEntityReceivedListener<T>) listener)
								.onSuccess(entity);
			}
		});
	}

	/**
	 * @param entityList
	 *            The received entities list.
	 * @param listenerClass
	 *            {@link Class} object of the desired listeners to be notified.
	 */
	@SuppressWarnings("hiding")
	protected <T, D extends OnEntityListReceivedListener<?>> void notifyEntityListReceviedSuccess(
			final List<T> entityList, final Class<D> listenerClass) {
		App.getInstance().runOnUiThread(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				for (UiListener listener : getListeners())
					if (listenerClass.isInstance(listener))
						((OnEntityListReceivedListener<T>) listener)
								.onSuccess(entityList);
			}
		});
	}

	/**
	 * 
	 * @param e
	 * @param listenerClass
	 */
	@SuppressWarnings("hiding")
	protected <T extends UiListener> void notifyRetrievalException(
			final AppException e, final Class<T> listenerClass) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener listener : getListeners())
					if (listenerClass.isInstance(listener))
						((UiListener) listener).onException(e);
			}
		});
	}

	/**
	 * 
	 * @param action
	 * @return True if the action isn't being processed, false otherwise.
	 */
	protected boolean needsProcessing(Action action) {
		synchronized (mActionsList) {
			return !mActionsList.contains(action);
		}
	}

	/**
	 * @param action
	 *            Action that is being processed.
	 */
	protected void notifyActionStarted(Action action) {
		if (mActionsList.contains(action))
			throw new RuntimeException(
					"This BusinessManager#Action is already being processed");

		synchronized (mActionsList) {
			mActionsList.add(action);
		}
	}

	/**
	 * @param action
	 *            Action that is done being processed.
	 */
	protected void notifyActionDone(Action action) {
		if (!mActionsList.contains(action))
			throw new RuntimeException(
					"Ending a BusinessManager#Action that isn't being processed");

		synchronized (mActionsList) {
			mActionsList.remove(action);
		}
	}

	/**
	 * @param id
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public T getObject(int id) throws AppException, InstantiationException,
			IllegalAccessException {
		return getObjectFromServer(id);
	}

	public <D extends BusinessObject> D getObject(Map<String, Object> params,
			String method, Class<D> clazz) throws AppException {
		return getObjectFromServer(params, method, clazz);
	}

	private T getObjectFromServer(int id) throws AppException,
			InstantiationException, IllegalAccessException {
		// Constructing the map with id parameter to generate JSON
		Map<String, Object> paramMap = new HashMap<String, Object>(1);
		paramMap.put(Params.BusinessObject.ID, String.valueOf(id));
		return getObjectFromServer(paramMap, mClass.newInstance().GET_METHOD,
				mClass);
	}

	private <D extends BusinessObject> D getObjectFromServer(
			Map<String, Object> params, String method, Class<D> clazz)
			throws AppException {
		try {
			String result = RestServiceAgent.post(getBaseUrl(), method,
					ParameterGenerator.getParamMap(DataHelper
							.formatAsJSON(params)));
			BaseResponse response = ParsingManager.parseServerResponse(result);
			return handleServerResponse(response, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}

	private <D extends BusinessObject> D handleServerResponse(
			BaseResponse response, Class<D> clazz) throws AppException {
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA)
			return DataHelper.deserialize(response.getData(), clazz);
		else if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
			return null;
		else if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
			throw new AppException(response.getValidationRule().errorMessage);
		else
			throw new AppException(BaseResponse.getExceptionType(response
					.getStatus()));
	}

	/**
	 * @param methodName
	 *            Target resource at server.
	 * @return
	 * @throws AppException
	 */
	public List<T> getObjectList(String methodName) throws AppException {
		return getObjectList(methodName, null);
	}

	/**
	 * 
	 * @param methodName
	 * @param params
	 * @return
	 * @throws AppException
	 */
	public List<T> getObjectList(String methodName, Map<String, Object> params)
			throws AppException {
		return getObjectList(methodName, params, mClass);
	}

	/**
	 * 
	 * @param methodName
	 * @param params
	 * @param class type
	 * @return
	 * @throws AppException
	 */
	public <D extends BusinessObject> List<D> getObjectList(String methodName,
			Map<String, Object> params, Class<D> clazz) throws AppException {
		try {
			String result = RestServiceAgent.post(getBaseUrl(), methodName,
					ParameterGenerator.getParamMap(DataHelper
							.formatAsJSON(params)));
			BaseResponse response = ParsingManager.parseServerResponse(result);
			return handleObjectListServerResponse(response, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}

	private <D extends BusinessObject> List<D> handleObjectListServerResponse(
			BaseResponse response, Class<D> clazz) throws AppException {
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getData().equals("[]")) {// TODO remove
				throw new AppException(AppException.NO_DATA_EXCEPTION);
			}
			return (List<D>) DataHelper.deserializeList(response.getData(),
					clazz);
		} else if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
			throw new AppException(response.getValidationRule().errorMessage);
		else
			throw new AppException(BaseResponse.getExceptionType(response
					.getStatus()));
	}

	/**
	 * 
	 * @param obj
	 */
	public void saveObject(T obj) throws AppException {
		saveObjectToServer(obj, mClass);
	}

	/**
	 * 
	 * @param obj
	 * @param clazz
	 * @throws AppException
	 */
	public <D extends BusinessObject> void saveObject(D obj, Class<D> clazz)
			throws AppException {
		saveObjectToServer(obj, clazz);
	}

	private <D extends BusinessObject> void saveObjectToServer(D obj,
			Class<D> clazz) throws AppException {
		BaseResponse response = submit(obj, obj.SAVE_METHOD, clazz);
		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));
		}
	}

	/**
	 * 
	 * @param id
	 */
	public void deleteObject(int id) throws AppException {
		// TODO
	}

	/**
	 * 
	 * @param obj
	 * @param method
	 * @param clazz
	 * @return
	 * @throws AppException
	 */
	public <D extends BusinessObject> BaseResponse submit(D obj, String method,
			Class<D> clazz) throws AppException {
		obj.validateObject(App.getInstance().getApplicationContext());
		return submit(method, DataHelper.serialize(obj, clazz));
	}

	public <D extends BusinessObject> BaseResponse submitList(List<D> list,
			String method, Class<D> clazz) throws AppException {
		return submit(method, DataHelper.serializeList(list, clazz));
	}

	/**
	 * @param obj
	 * @param file
	 * @param method
	 * @param clazz
	 * @return
	 * @throws AppException
	 */
	public <D extends BusinessObject> BaseResponse submit(D obj, File file,
			String method, Class<D> clazz) throws AppException {
		obj.validateObject(App.getInstance().getApplicationContext());
		return submit(method, DataHelper.serialize(obj, clazz), file);
	}

	/**
	 * @param obj
	 * @param file
	 * @param params
	 * @param method
	 * @param clazz
	 * @return
	 * @throws AppException
	 */
	public <D extends BusinessObject> BaseResponse submit(D obj, File file,
			Map<String, Object> params, String method, Class<D> clazz)
			throws AppException {
		obj.validateObject(App.getInstance().getApplicationContext());

		try {
			return submit(method, DataHelper.serialize(obj, clazz), file,
					DataHelper.formatAsJSON(params));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new AppException(AppException.JSON_PARSING_EXCEPTION);
		}
	}

	/**
	 * @param obj
	 * @param method
	 * @return
	 * @throws AppException
	 */
	public BaseResponse submit(T obj, String method) throws AppException {
		return submit(obj, method, mClass);
	}

	/**
	 * @param obj
	 * @param method
	 * @return
	 * @throws AppException
	 */
	public BaseResponse submit(T obj, File file, String method)
			throws AppException {
		return submit(obj, file, method, mClass);
	}

	/**
	 * @param params
	 * @param method
	 * @return
	 * @throws AppException
	 */
	public BaseResponse submit(Map<String, Object> params, String method)
			throws AppException {
		try {
			return submit(method, DataHelper.formatAsJSON(params));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new AppException(AppException.JSON_PARSING_EXCEPTION);
		}
	}

	/**
	 * @param params
	 * @param file
	 * @param method
	 * @return
	 * @throws AppException
	 */
	public BaseResponse submit(Map<String, Object> params, File file,
			String method) throws AppException {
		try {
			return submit(method, DataHelper.formatAsJSON(params), file);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new AppException(AppException.JSON_PARSING_EXCEPTION);
		}
	}

	public BaseResponse submitPosterFiles(Map<String, Object> params,
			File file, File file2, String method) throws AppException {
		try {
			return submitFiles(method, DataHelper.formatAsJSON(params), file,
					file2);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new AppException(AppException.JSON_PARSING_EXCEPTION);
		}
	}

	private BaseResponse submitFiles(String method, Object... params)
			throws AppException {
		try {
			String result = RestServiceAgent.post(getBaseUrl(), method,
					ParameterGenerator.getPosterParamMap(params));
			return ParsingManager.parseServerResponse(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}

	private BaseResponse submit(String method, Object... params)
			throws AppException {
		try {
			String result = RestServiceAgent.post(getBaseUrl(), method,
					ParameterGenerator.getParamMap(params));
			return ParsingManager.parseServerResponse(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw AppException.getAppException(e);
		}
	}

	private static String getBaseUrl() throws AppException {
		return VersionController.getBaseUrl();
	}

	/**
	 * @author Morabea
	 */
	public static class Action {

		public static enum ActionType {
			LIKE, REPOST, FOLLOW, GET_POSTS, GET_CHAT_MESSAGES, GET_NOTIFICATIONS;
		}

		private ActionType type;
		private Object associatedObj;

		/**
		 * @param type
		 *            {@link ActionType#LIKE}, {@link ActionType#REPOST},
		 *            {@link ActionType#FOLLOW}
		 * @param associatedId
		 */
		public Action(ActionType type, Object associatedObj) {
			this.type = type;
			this.associatedObj = associatedObj;
		}

		public ActionType getType() {
			return type;
		}

		public Object getAssociatedObj() {
			return associatedObj;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((associatedObj == null) ? 0 : associatedObj.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Action other = (Action) obj;
			if (associatedObj == null) {
				if (other.associatedObj != null)
					return false;
			} else if (!associatedObj.equals(other.associatedObj))
				return false;
			if (type != other.type)
				return false;
			return true;
		}

	}

}
