/**
 * 
 */
package com.tawasol.fcbBarcelona.ui.adapters;

import java.util.ArrayList;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.PostViewModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * @author Mohga
 * 
 */
public class MyPicsAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<PostViewModel> list;

	static class FCBViewHolder {
		ImageView myPicsIV;	 
	}
	
	public MyPicsAdapter(Context context, ArrayList<PostViewModel> list) {
		this.context = context;
		this.list    = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public PostViewModel getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return list.get(0).getPostId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FCBViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new FCBViewHolder();
			convertView = inflater.inflate(R.layout.fcb_photo_grid, parent , false);
			holder.myPicsIV = (ImageView) convertView.findViewById(R.id.grid_image);
			convertView.setTag(holder);
		} else {
			holder = (FCBViewHolder) convertView.getTag();
		}
		App.getInstance()
		   .getImageLoader()
		   .displayImage(list.get(position).getPostPhotoUrl(), holder.myPicsIV,
			App.getInstance().getDisplayOption());
		return convertView;
	}
}
