package com.tawasol.fcbBarcelona.ui.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType; 
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.Fan;
import com.tawasol.fcbBarcelona.entities.FriendsEntity;

/**
 * 
 * @author basyouni
 *
 */
public class ContactFriendSuggestion  extends BaseAdapter{

		private Context context;
		private List<Fan> contacts;

		private ImageLoader mLoader;
		DisplayImageOptions options;

		public ContactFriendSuggestion(Context context, List<Fan> contacts) {
			this.context  = context;
			this.contacts = contacts;
			mLoader =  App.getInstance().getImageLoader();
			
			/** Initialize Options to display RoundedImage **/
			options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.contact_img)
				.showImageOnFail(R.drawable.contact_img)
				.showImageOnLoading(R.drawable.contact_img)
				.resetViewBeforeLoading(true)
				.cacheOnDisc(true)
				.cacheInMemory(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.considerExifParams(true)
				.build();
		}

		@Override
		public int getCount() {
			return contacts.size();
		}

		@Override
		public Object getItem(int position) {
			return contacts.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(R.layout.chat_contacts_grid_item, parent, false);
				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final FriendsEntity friend = (FriendsEntity) getItem(position);
			
			// Contact Name
			if (friend.getFanName() != null)
				holder.friendName.setText(friend.getFanName());
			else
				holder.friendName.setText("");

			// Number of posts
			if (friend.getFanPicNumber() > 0)
				holder.numberOfPic.setText(""+friend.getFanPicNumber());
			else
				holder.numberOfPic.setText("");
						
			// Favorite
			if (friend.getIsFavorite()==1)
				holder.favoriteImg.setImageResource(R.drawable.favourate);
			else
				holder.favoriteImg.setImageResource(R.drawable.unfavourate);
			
			// Followed
			if (friend.getIsFollowed() == 1)
				holder.followedImg.setImageResource(R.drawable.followed);
			else
				holder.followedImg.setImageResource(R.drawable.unfollowed);
				
//			// Online
//			if (friend.isFanOnline())
//				holder.statusFrame.setImageResource(R.drawable.followed);
//			else
//				holder.statusFrame.setImageResource(R.drawable.unfollowed);
						
			// Contact Profile Img
			if (friend.getFanPicture() != null)
				mLoader.displayImage(friend.getFanPicture(), holder.profilePic, options);
			else
				holder.profilePic.setImageResource(R.drawable.contact_img);

			return convertView;
		}

		private static class ViewHolder {
			TextView friendName, numberOfPic;
			ImageView profilePic, favoriteImg, followedImg, numberOfPicImg;
			LinearLayout statusFrame;

			public ViewHolder(View view) {
				friendName     = (TextView) view.findViewById(R.id.friend_name);
				numberOfPic    = (TextView) view.findViewById(R.id.number_of_pic_txt);
				profilePic     = (ImageView) view.findViewById(R.id.profile_pic);
				favoriteImg    = (ImageView) view.findViewById(R.id.favorite_img);
				followedImg    = (ImageView) view.findViewById(R.id.followed_img);
				numberOfPicImg = (ImageView) view.findViewById(R.id.number_of_pic_img);
				statusFrame    = (LinearLayout) view.findViewById(R.id.status_frame);
			}
		}
	}