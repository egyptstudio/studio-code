/**
 * 
 */
package com.tawasol.fcbBarcelona.ui.adapters;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.SectionIndexer;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.Country;
import com.tawasol.fcbBarcelona.ui.activities.FilterActivity;

/**
 * @author Basyouni
 *
 */
@SuppressLint("DefaultLocale")
public class FilterAdapter extends BaseAdapter implements SectionIndexer {
	private List<Country> stringArray;
	private Activity context;
	private boolean[] checkBoxState;

	public FilterAdapter(Activity _context, List<Country> objs) {
		stringArray = objs;
		context = _context;
		checkBoxState = new boolean[objs.size()];
	}

	public int getCount() {
		return stringArray.size();
	}

	public Object getItem(int arg0) {
		return stringArray.get(arg0);
	}

	public long getItemId(int arg0) {
		return 0;
	}

	public View getView(final int position, View v, ViewGroup parent) {
		LayoutInflater inflate = ((Activity) context).getLayoutInflater();
		View view = v;
		if (view == null)
			view = inflate.inflate(R.layout.filter_row, parent, false);
		String label = stringArray.get(position).getCountryName();
		final CheckBox checkBox = (CheckBox) view
				.findViewById(R.id.country_check_box);
		checkBox.setText(label);
		checkBox.setChecked(checkBoxState[position]);
		checkBox.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkBox.isChecked()) {
					((FilterActivity) context).addtoCountryList(stringArray
							.get(position).getCountryId());
					checkBoxState[position] = true;
				} else {
					((FilterActivity) context)
							.removeFromCountryList(stringArray.get(position).getCountryId());
					checkBoxState[position] = false;
				}
			}
		});
		return view;
	}

	public int getPositionForSection(int section) {
		if (section == 35) {
			return 0;
		}
		for (int i = 0; i < stringArray.size(); i++) {
			String l = stringArray.get(i).getCountryName();
			char firstChar = l.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}
		return -1;
	}

	public int getSectionForPosition(int arg0) {
		return 0;
	}

	public Object[] getSections() {
		return null;
	}
}
