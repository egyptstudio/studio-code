package com.tawasol.fcbBarcelona.ui.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.Notification;
import com.tawasol.fcbBarcelona.managers.NotificationsManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;

public class NotificationsRequestsAdapter extends BaseAdapter{

	private Activity activity;
	private ArrayList<Notification> notifications ;
	private DisplayImageOptions profileOption;
	
	public NotificationsRequestsAdapter (Activity activity , ArrayList<Notification> notifications)
	{
		this.activity = activity;
		this.notifications = notifications;
		
		profileOption = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.resetViewBeforeLoading(true)
		.showImageOnFail(R.drawable.ic_launcher)
		.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(1000)).build();
	}
	@Override
	public int getCount() {
		return notifications.size();
	}

	@Override
	public Notification getItem(int position) {
		return notifications.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final NotificationsRequestsViewHolder holder;
		
		if(convertView ==  null)
		{

			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new NotificationsRequestsViewHolder();
			convertView = inflater.inflate(
					R.layout.notifications_request_item, parent,
					false);
			
			holder.requesterName = (TextView)convertView.findViewById(R.id.fan_name);
			holder.requesterImg = (ImageView)convertView.findViewById(R.id.fan_profile_img_id);
		//	holder.favImg=(ImageButton)convertView.findViewById(R.id.favourate_button);
			//holder.followImg = (ImageButton)convertView.findViewById(R.id.followed_fan);
			//holder.noOfPicsTxt = (TextView)convertView.findViewById(R.id.num_of_pics);
			holder.acceptImg = (ImageView)convertView.findViewById(R.id.acceptRequest);
			holder.rejectImg = (ImageView)convertView.findViewById(R.id.reject_request);
			holder.requesterimgPB = (ProgressBar)convertView.findViewById(R.id.progress);
			
			// set the tag of buttons
			holder.acceptImg.setTag(position);
			holder.rejectImg.setTag(position);
			
			convertView.setTag(holder);
		}
		else
			holder = (NotificationsRequestsViewHolder) convertView.getTag();
		
		// set the requester Name 
		holder.requesterName.setText(notifications.get(position).getSenderFullName());
		
		// set the requester image 
		App.getInstance().getImageLoader().displayImage(notifications.get(position).getSenderImgUrl(), holder.requesterImg, profileOption, new ImageLoadingListener() {
			
			@Override
			public void onLoadingStarted(String arg0, View arg1) {				
			}
			
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				holder.requesterimgPB.setVisibility(View.GONE);
				holder.requesterImg.setImageResource(R.drawable.profile_icon);	
			}
			
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				holder.requesterimgPB.setVisibility(View.GONE);				
			}
			
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {				
			}
		});
			
		
		// set the fav image 
		// TODO ask hamed :) 
		
		// set the follow image 
		// TODO ask hamed :) 
		
		// set the no of pics
		// TODO ask hamed :) 
		
		holder.acceptImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				NotificationsManager.getInstance().handleFriend(UserManager.getInstance().getCurrentUserId(), notifications.get((Integer) v.getTag()).getSenderId(), true);
				((BaseActivity)activity).refreshMenu();
			}
		});
		
		holder.rejectImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				NotificationsManager.getInstance().handleFriend(UserManager.getInstance().getCurrentUserId(), notifications.get((Integer) v.getTag()).getSenderId(), false);
			((BaseActivity)activity).refreshMenu();
			}
		});
			return convertView;
	}
	
	private class NotificationsRequestsViewHolder
	{
		TextView requesterName;
		ImageView requesterImg;
//		ImageButton favImg;
//		ImageButton followImg;
//		TextView noOfPicsTxt;
		ImageView acceptImg;
		ImageView rejectImg;
		ProgressBar requesterimgPB;
	}

}
