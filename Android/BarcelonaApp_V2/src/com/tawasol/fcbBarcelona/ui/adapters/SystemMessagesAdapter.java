package com.tawasol.fcbBarcelona.ui.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context; 
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup; 
import android.widget.BaseAdapter;
import android.widget.CheckBox; 
import android.widget.CompoundButton;
import android.widget.TextView; 
import android.widget.CompoundButton.OnCheckedChangeListener;
  

import com.tawasol.fcbBarcelona.R; 
import com.tawasol.fcbBarcelona.entities.SystemMessagesEntity;

/**
 * 
 * @author Turki
 *
 */
public class SystemMessagesAdapter  extends BaseAdapter {

	private Context context; 
	private List<SystemMessagesEntity> mSystemMessages;
	 
	public SystemMessagesAdapter(Context context, List<SystemMessagesEntity> mSystemMessages) {
		this.context         = context;
		this.mSystemMessages = mSystemMessages;
		 
	}
	
	 
	@Override
	public int getCount() {
		return mSystemMessages.size();
	}

	@Override
	public Object getItem(int position) {
		return mSystemMessages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.system_msgs_item, parent, false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		final SystemMessagesEntity sysMsg = (SystemMessagesEntity) getItem(position); 
		
		if (sysMsg.getMessageText() != null) {
			holder.systemMsg.setText(sysMsg.getMessageText());
		}
		
		holder.checkBx.setOnCheckedChangeListener(myCheckChangList);
		holder.checkBx.setTag(position);
		holder.checkBx.setChecked(sysMsg.isChecked());
		
		return convertView;
	}
	
	static class ViewHolder {
		CheckBox checkBx;
		TextView systemMsg;

		public ViewHolder(View view) {
			checkBx   = (CheckBox) view.findViewById(R.id.system_msgs_check_box);
			systemMsg = (TextView) view.findViewById(R.id.system_msg_txt); 
		}	
	}
	
	SystemMessagesEntity getSystemMessage(int position) {
		return ((SystemMessagesEntity) getItem(position));
	}

	public ArrayList<SystemMessagesEntity> getCheckedSystemMessagesEntity() {
		ArrayList<SystemMessagesEntity> checkedList = new ArrayList<SystemMessagesEntity>();
		for (SystemMessagesEntity obj : mSystemMessages) {
			if (obj.isChecked())
				checkedList.add(obj);
		}
		return checkedList;
	}

	OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			getSystemMessage((Integer) buttonView.getTag()).setChecked(isChecked);
		}
	};
}

