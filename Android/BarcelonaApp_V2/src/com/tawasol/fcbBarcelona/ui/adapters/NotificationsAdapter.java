package com.tawasol.fcbBarcelona.ui.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.WallTable;
import com.tawasol.fcbBarcelona.entities.Notification;
import com.tawasol.fcbBarcelona.entities.PostViewModel;
import com.tawasol.fcbBarcelona.managers.NotificationsManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.FanProfileActivity;
import com.tawasol.fcbBarcelona.ui.activities.ImageDetailsActivity;

public class NotificationsAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<Notification> notifications;
	Notification currentItem,previousItem;
	DisplayImageOptions profileOption;

	public NotificationsAdapter(Activity activity,
			ArrayList<Notification> notifications) {
		this.activity = activity;
		this.notifications = notifications;
		profileOption = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.resetViewBeforeLoading(true)
		.showImageOnFail(R.drawable.ic_launcher)
		.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(1000)).build();
	}

	@Override
	public int getCount() {
		return notifications.size();
	}

	@Override
	public Object getItem(int position) {
		return notifications.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final NotificationsViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new NotificationsViewHolder();
			convertView = inflater.inflate(
					R.layout.notification_item, parent,
					false);

			holder.notificationTxt = (TextView) convertView
					.findViewById(R.id.notification_txt);
			holder.dateTxt = (TextView) convertView
					.findViewById(R.id.notification_date);
			holder.notifierImg = (ImageView) convertView
					.findViewById(R.id.notification_img);
			holder.imgLayout = (RelativeLayout) convertView
					.findViewById(R.id.notification_image_layout);
			holder.notificationPB = (ProgressBar) convertView
					.findViewById(R.id.notification_progress_bar);
			holder.dateGroupingTxt = (TextView) convertView
					.findViewById(R.id.date_grouping_txt);
			holder.dateLayout = (RelativeLayout) convertView
					.findViewById(R.id.notifications_date_layout);
			holder.notificationLayout = (RelativeLayout) convertView
					.findViewById(R.id.notification_layout_id);

			holder.notificationLayout.setTag(position);
			convertView.setTag(holder);
		} else
			holder = (NotificationsViewHolder) convertView.getTag();

		// set the data for Like Notifications
		if (notifications.get(position).getNotificationType() == Notification.LIKE_NOTIFICATIONS) {
			holder.notificationTxt.setText(notifications.get(position)
					.getSenderFullName()
					+ " "
					+ activity.getResources().getString(
							R.string.notifications_liked_photo));
		//	holder.imgLayout.setVisibility(View.GONE);
		}

		else if (notifications.get(position).getNotificationType() == Notification.COMMENT_NOTIFICATIONS) {
			holder.notificationTxt.setText(notifications.get(position)
					.getSenderFullName()
					+ " "
					+ activity.getResources().getString(
							R.string.notifications_commented_on_photo));
		//	holder.imgLayout.setVisibility(View.GONE);
		}  
			// favorite cases 
		else if (notifications.get(position).getNotificationType() == Notification.FOLLOWING_NOTIFICATIONS)
				holder.notificationTxt.setText(notifications.get(position)
						.getSenderFullName()
						+ " "
						+ activity.getResources().getString(
								R.string.notifications_following_photo));

			else if (notifications.get(position).getNotificationType() == Notification.ADD_FRIEND_NOTIFICATIONS)
				holder.notificationTxt.setText(notifications.get(position)
						.getSenderFullName()
						+ " "
						+ activity.getResources().getString(
								R.string.notifications_added_friend));

			else if (notifications.get(position).getNotificationType() == Notification.CHANGE_RROFILE_NOTIFICATIONS)
				holder.notificationTxt
						.setText(notifications.get(position)
								.getSenderFullName()
								+ " "
								+ activity
										.getResources()
										.getString(
												R.string.notifications_changed_profile_picture));

			else if (notifications.get(position).getNotificationType() == Notification.POST_NEW_PHOTO_NOTIFICATIONS)
				holder.notificationTxt.setText(notifications.get(position)
						.getSenderFullName()
						+ " "
						+ activity.getResources().getString(
								R.string.notifications_posted_new_photo));

			holder.imgLayout.setVisibility(View.VISIBLE);
			DisplayImage(notifications.get(position).getSenderImgUrl(),
					holder.notifierImg, holder.notificationPB);
		

		holder.dateTxt.setText(setTime(notifications.get(position).getTime()));

		// highlight the notification if not read :) 
		if(!notifications.get(position).isRead())
			holder.notificationLayout.setBackgroundResource(R.drawable.notification_new_list_bg);
		else
			holder.notificationLayout.setBackgroundResource(R.drawable.notification_list_bg);
		
		
		// set the on click of view 
		/*holder.notificationLayout.setTag(position);
		holder.notificationLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int index =  (Integer) v.getTag();
				if(index != -1 && index != 0  )
					if(notifications.get(index).getNotificationType() == Notification.LIKE_NOTIFICATIONS || notifications.get(index).getNotificationType() == Notification.COMMENT_NOTIFICATIONS || notifications.get(index).getNotificationType() == Notification.POST_NEW_PHOTO_NOTIFICATIONS){
						PostViewModel post = WallTable.getInstance().getPostById(notifications.get(index).getPostId());
						if(post != null)
						activity.startActivity(ImageDetailsActivity.getActivityIntent(activity, post, 0, 0, false, 0, 0));
						else
							NotificationsManager.getInstance().getPostDetails(notifications.get(index).getPostId());
					}
					else if(notifications.get(index).getNotificationType() == Notification.FOLLOWING_NOTIFICATIONS || notifications.get(index).getNotificationType() == Notification.CHANGE_RROFILE_NOTIFICATIONS || notifications.get(index).getNotificationType() == Notification.ADD_FRIEND_NOTIFICATIONS)
					{
						activity.startActivity(FanProfileActivity.getIntent(activity, notifications.get(index).getSenderId(), notifications.get(index).getSenderFullName()));
					}
			NotificationsManager.getInstance().setNotificationRead(notifications.get(index).getNotificationId());
			((BaseActivity)activity).refreshMenu();
			}
		});*/
		
		//Current Item
        try {
            currentItem = (Notification) getItem(position);
        } catch (Exception e) {
        }

    try {
        previousItem = (Notification) getItem(position-1);
    } catch (Exception e) {
        previousItem = null;
    }

    if (previousItem ==null) {
        previousItem = currentItem;
        holder.dateLayout.setVisibility(View.VISIBLE);
        holder.dateGroupingTxt.setText(setDate(currentItem.getTime()));
    } else {
        if (isDateMatched(currentItem, previousItem)) {
            holder.dateLayout.setVisibility(View.GONE);
        } else{
            holder.dateLayout.setVisibility(View.VISIBLE);
            holder.dateGroupingTxt.setText(setDate(currentItem.getTime()));
        }

        previousItem = currentItem;
    }
    
		return convertView;
	}

	private void DisplayImage(String imageUrl, final ImageView imgView,
			final ProgressBar progressBar) {
		App.getInstance()
				.getImageLoader()
				.displayImage(imageUrl, imgView,
						profileOption,
						new ImageLoadingListener() {

							@Override
							public void onLoadingStarted(String arg0, View arg1) {
							}

							@Override
							public void onLoadingFailed(String arg0, View arg1,
									FailReason arg2) {
								progressBar.setVisibility(View.GONE);
								imgView.setImageResource(R.drawable.profile_icon);
							}

							@Override
							public void onLoadingComplete(String arg0,
									View arg1, Bitmap arg2) {
								progressBar.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingCancelled(String arg0,
									View arg1) {
							}
						});
	}

	private String setTime(long date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a",
				Locale.getDefault());
		dateFormat.setTimeZone(TimeZone.getDefault());
		return dateFormat.format(new Date(date * 1000));
	}
	
	private String setDate(long date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",
				Locale.getDefault());
		dateFormat.setTimeZone(TimeZone.getDefault());
		return dateFormat.format(new Date(date * 1000));
	}
	
	private boolean isDateMatched(Notification currentNotification , Notification previousNotification)
	{
		Calendar currentCalendar = Calendar.getInstance();
		currentCalendar.setTime(new Date(currentNotification.getTime()*1000));
		int currentYear = currentCalendar.get(Calendar.YEAR); 
		int currentMonth = currentCalendar.get(Calendar.MONTH);
		int currentDay = currentCalendar.get(Calendar.DAY_OF_MONTH);
		
		Calendar previousCalendar = Calendar.getInstance();
		previousCalendar.setTime(new Date(previousNotification.getTime()*1000));
		int previousYear = previousCalendar.get(Calendar.YEAR); 
		int previousMonth = previousCalendar.get(Calendar.MONTH);
		int previousDay = previousCalendar.get(Calendar.DAY_OF_MONTH);
		
		if(currentYear == previousYear && currentMonth == previousMonth && currentDay == previousDay)
			return true;
		else
			return false;
		
	}
	private class NotificationsViewHolder {
		TextView notificationTxt;
		TextView dateTxt;
		ImageView notifierImg;
		ProgressBar notificationPB;
		RelativeLayout imgLayout;
		TextView dateGroupingTxt;
		RelativeLayout dateLayout;
		RelativeLayout notificationLayout;

	}

}
