package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.ui.fragments.ForgetPasswordVerificationFragment;

public class ForgetPassowrdVerificationActivity extends BaseActivity {
	public final static String USERID_KEY="User_Id";
	public final static String EMAIL_KEY="Email";
private FragmentTransaction transaction;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_forget_passwordverification);

		ForgetPasswordVerificationFragment forgetPassFrag = new ForgetPasswordVerificationFragment();
		 transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.add(R.id.forgetpass_container_id, forgetPassFrag);
		transaction.commit();
		setTitle(getResources().getString(R.string.Login_forgotPassword));
		showBackBtn();
		hideBottomBar();
		hideMenuBtn();
	}

	
	public static Intent getActivityIntent(Context context,String UserId,String Email,int LoginType , Intent navigationIntent) {
		
		Bundle data  = new Bundle();
		data.putInt(LogInActivity.LOGIN_TYPE_KEY, LoginType);
		data.putParcelable(LogInActivity.INTENT_NAVIGATION_KEY, navigationIntent);
		data.putString(USERID_KEY, UserId);
		data.putString(EMAIL_KEY, Email);
		
		return new Intent(context, ForgetPassowrdVerificationActivity.class)
		.putExtras(data);
		
	}


}
