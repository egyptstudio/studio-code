package com.tawasol.fcbBarcelona.ui.activities;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.data.helper.VersionController;
import com.tawasol.fcbBarcelona.data.helper.VersionController.VersionInfo;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.managers.NotificationsManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.Utils;

public class SplashActivity extends FragmentActivity {

	private static final int SPLASH_DELAY = 3000;
	public static final String FCB_FIRST_INSTALL="FcbFirstInstall";


	private AsyncTask<Void, Void, Void> mTask;
	private boolean callAgain;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);

		if (VersionController.isUpdateRequired()) {
			UIUtils.showUpdatePopup(this, new Runnable() {
				@Override
				public void run() {
					Utils.updateAppAction(SplashActivity.this);
				}
			});
		} else {
			mTask = new SplashTask();
			mTask.execute();
		}
	}

	private void doSplashMagic() {
		if(SharedPrefrencesDataLayer.getBooleanPreferences(SplashActivity.this, FCB_FIRST_INSTALL, true)) { 
		    String langCode[] = getResources().getStringArray(R.array.languages_code_list_items);
		    
		    for(int i=0; i<langCode.length; i++){
				if (Locale.getDefault().getLanguage().equalsIgnoreCase(langCode[i])){
		    		SharedPrefrencesDataLayer.saveStringPreferences(SplashActivity.this, 
		    				LanguageUtils.APP_LANGUAGE_CODE_KEY, langCode[i]);
		    		break;
		    	}
		    }
		}
		// Language init 
        String language = SharedPrefrencesDataLayer.getStringPreferences(SplashActivity.this, 
        		LanguageUtils.APP_LANGUAGE_CODE_KEY, LanguageUtils.LANGUAGE_ENGLISH);
        LanguageUtils.changeApplicationLanguage(language , SplashActivity.this , false); 
		proceedToNextScreen();
		if(UserManager.getInstance().getCurrentUserId() != 0 && !callAgain){
			callAgain = true;
			NotificationsManager.getInstance().getNotificationsFromServer();
		}
	}
	
	private void proceedToNextScreen() {
		if(NetworkingUtils.isNetworkConnected(this)){
			if(isFirstTime())
				startActivity(HelpScreenFullScreenActivity.getActivityIntent(this));
			else
//				startActivity(new Intent(this, New_Home_Activity.class));
				startActivity(HomeActivity.getActivityIntent(this));
			}else{
//				startActivity(new Intent(this, New_Home_Activity.class));
			//UIUtils.showToast(this,getResources().getString(R.string.reg_no_internet));
			startActivity(HomeActivity.getActivityIntent(this));
		}
		finish();
	}
	

	@Override
	public void onBackPressed() {
		if (mTask != null)
			mTask.cancel(true);
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}
	
	private boolean isFirstTime()	{
		if(SharedPrefrencesDataLayer.getBooleanPreferences(this, FCB_FIRST_INSTALL, true)) { 
		    SharedPrefrencesDataLayer.saveBooleanPreferences(this, FCB_FIRST_INSTALL, false);
		    return true;
		}
		else
			return false;
	}
	
	private final class SplashTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			
			long startTime = System.currentTimeMillis();
			
			if (!isCancelled()) {
				try {
					getBaseUrlAndUpdateStatus();
				} catch (Exception e) {
					// Failed to get the version info for some reason
					// No worries, it will be retrieved with the first coming request
				}
			}
			
			long timeTaken = System.currentTimeMillis() - startTime;
			
			if (!isCancelled()) {
				try {
					waitForRemainingOfTimeIfAny(timeTaken);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			doSplashMagic();
		}

		private void getBaseUrlAndUpdateStatus() throws AppException {
			VersionController.getVersionInfo(true);
		}

		private void waitForRemainingOfTimeIfAny(long timeTaken) throws InterruptedException {
			if (timeTaken < SPLASH_DELAY)
				Thread.sleep(SPLASH_DELAY - timeTaken);
		}
		
	}
	
}
