package com.tawasol.fcbBarcelona.ui.activities;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.customView.NestedListView;
import com.tawasol.fcbBarcelona.entities.WalkThroughEntity;

public class FreePointsWalkThrough extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		List<WalkThroughEntity> walkThroughList = new ArrayList<>();
		walkThroughList.add(new WalkThroughEntity(
				getString(R.string.FP_Complete_profile), "300 XP",
				getString(R.string.FP_One_time),
				WalkThroughEntity.GO));
		walkThroughList.add(new WalkThroughEntity(
				getString(R.string.FP_Invite), "10 XP",
				getString(R.string.FP_Friend),
				WalkThroughEntity.GO));
		walkThroughList.add(new WalkThroughEntity(
				getString(R.string.FP_Public_social), "40 XP",
				getString(R.string.FP_Post),
				WalkThroughEntity.SHOW_ME));
		walkThroughList.add(new WalkThroughEntity(
				getString(R.string.FP_Share_Images), "10 XP",
				getString(R.string.FP_Share),
				WalkThroughEntity.SHOW_ME));
		walkThroughList.add(new WalkThroughEntity(
				getString(R.string.FP_Daily), "10 XP",
				getString(R.string.FP_Day),
				WalkThroughEntity.empty));
		walkThroughList.add(new WalkThroughEntity(
				getString(R.string.FP_Purchased), null, null,
				WalkThroughEntity.CHECK));

		super.onCreate(arg0);
		setContentView(R.layout.free_points_walkthrough);
		setTitle(getString(R.string.Free_Points));
		hideBottomBar();
		hideMenuBtn();
		showBackBtn();
		final ScrollView walkThroughContainer = (ScrollView) findViewById(R.id.walkThroughContainer);
		(walkThroughContainer).post(new Runnable() {

			@Override
			public void run() {
				walkThroughContainer.fullScroll(ScrollView.FOCUS_UP);
			}
		});
		NestedListView walkThoroughList = (NestedListView) findViewById(R.id.walkTroughList);
		// walkThoroughList.setOnTouchListener(new OnTouchListener() {
		// // Setting on Touch Listener for handling the touch inside
		// // ScrollView
		// @Override
		// public boolean onTouch(View v, MotionEvent event) {
		// // Disallow the touch request for parent scroll on touch of
		// // child view
		// v.getParent().requestDisallowInterceptTouchEvent(true);
		// return false;
		// }
		// });
		// setListViewHeightBasedOnChildren(walkThoroughList);
		walkThoroughList.setAdapter(new WlakThroughAdapter(walkThroughList));
	}

	/****
	 * Method for Setting the Height of the ListView dynamically. Hack to fix
	 * the issue of not showing all the items of the ListView when placed inside
	 * a ScrollView
	 ****/
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	private class WlakThroughAdapter extends BaseAdapter {
		List<WalkThroughEntity> walkThroughList;

		public WlakThroughAdapter(List<WalkThroughEntity> walkThroughList) {
			this.walkThroughList = walkThroughList;
		}

		@Override
		public int getCount() {
			return walkThroughList.size();
		}

		@Override
		public Object getItem(int position) {
			return walkThroughList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			if (convertView == null)
				convertView = getLayoutInflater().inflate(
						R.layout.free_points_walkthrough_item, parent, false);

			TextView description = (TextView) convertView
					.findViewById(R.id.walk_through_description);
			TextView price = (TextView) convertView
					.findViewById(R.id.walk_through_price);
			TextView repetation = (TextView) convertView
					.findViewById(R.id.walk_through_repetation);
			Button action = (Button) convertView
					.findViewById(R.id.walk_through_action);

			if (walkThroughList.get(position).getDescription() != null) {
				description.setVisibility(View.VISIBLE);
				description.setText(walkThroughList.get(position)
						.getDescription());
			} else
				description.setVisibility(View.GONE);

			if (walkThroughList.get(position).getWonXp() != null) {
				price.setVisibility(View.VISIBLE);
				price.setText(walkThroughList.get(position).getWonXp());
			} else {
				price.setVisibility(View.GONE);
			}
			if (walkThroughList.get(position).getRepetation() != null) {
				repetation.setVisibility(View.VISIBLE);
				repetation.setText(walkThroughList.get(position)
						.getRepetation());
			} else {
				repetation.setVisibility(View.GONE);
			}

			switch (walkThroughList.get(position).getButtonType()) {
			case WalkThroughEntity.GO:
				action.setVisibility(View.VISIBLE);
				action.setText(getString(R.string.FP_GO));
				break;
			case WalkThroughEntity.SHOW_ME:
				action.setVisibility(View.VISIBLE);
				action.setText(getString(R.string.FP_Show));
				break;
			case WalkThroughEntity.CHECK:
				action.setVisibility(View.VISIBLE);
				action.setText(getString(R.string.FP_Check));
				break;
			case WalkThroughEntity.empty:
				action.setVisibility(View.GONE);
				break;

			default:
				break;
			}

			action.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					switch (position) {
					case 0:
						startActivity(Edit_UserProfile_Activity
								.getActivityIntent(FreePointsWalkThrough.this,
										false));
						break;
					case 1:
						startActivity(ChatInviteActivity
								.getActivityIntent(FreePointsWalkThrough.this));
						break;
					case 2:
						startActivity(ShowMeActivity.getActivityIntent(
								FreePointsWalkThrough.this,
								ShowMeActivity.POST_TYPE));
						break;
					case 3:
						startActivity(ShowMeActivity.getActivityIntent(
								FreePointsWalkThrough.this,
								ShowMeActivity.SHARE_TYPE));
						break;
					case 5:
						startActivity(StudioFoldersActivity.getActivityIntent(
								FreePointsWalkThrough.this,
								StudioFoldersActivity.PURCHASED_FOLDER));
						break;

					default:
						break;
					}
				}
			});

			return convertView;
		}

	}
}
