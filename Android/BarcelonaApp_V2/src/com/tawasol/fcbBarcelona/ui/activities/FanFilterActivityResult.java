package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.FansFilterParams;
import com.tawasol.fcbBarcelona.managers.FanManager;
import com.tawasol.fcbBarcelona.ui.fragments.FanFilterFragmentResult;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class FanFilterActivityResult extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.fan_filter_activity_result);
		showBackBtn();
		setTitle(getString(R.string.filterResult_Results));
		hideBottomBar();
	}
	
	@Override
	public void onBackPressed() {
		FanManager.getInstance().clearFiltered();
		super.onBackPressed();
	}
	
	public static Intent getIntent (Context context , FansFilterParams params){
		return new Intent(context, FanFilterActivityResult.class).putExtra("params", params);
	}
}
