package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.data.connection.Params;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * 
 * @author Turki
 *
 */
public class InviteMessageActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_invite_message);
		setTitle(getResources().getString(R.string.smsInvite_title));
		
		/**  Initialize InviteMessage Activity **/
		init();
	}
	
	/**  Get InviteMessageActivity Intent to start it from any activity **/
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, InviteMessageActivity.class);
	}
	
	private void init(){
		/** Hide BottomBar **/
		hideBottomBar();
		
		/** Show RightButton, BackButton**/
		showRightBtn();
		showBackBtn();
	}
}
