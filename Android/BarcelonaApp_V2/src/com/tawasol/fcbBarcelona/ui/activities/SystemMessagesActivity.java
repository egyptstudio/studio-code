package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle; 
import com.tawasol.fcbBarcelona.R; 
/**
 * @author Turki
 * 
 */
public class SystemMessagesActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_system_messages);
		setTitle(getResources().getString(R.string.Settings_SystemMessages));
		showBackBtn(); 
//		showDeleteButton();
//		hideMenuBtn();
	}
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, SystemMessagesActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	}
}
