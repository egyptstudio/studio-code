package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.dialogs.CustomAlertDialog;
import com.tawasol.fcbBarcelona.ui.fragments.Edit_More_Profile_Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class Edit_More_Profile_Activity extends BaseActivity implements
		OnClickListener {
	public static final String USER_OBJ_KEY = "User_Key";

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_edit_more_profile);
		setTitle(getString(R.string.MyProfile_MoreAboutMe));
		showBackBtn();
		showRightBtn();
		hideBottomBar();
		hideMenuBtn();
		getBackButton().setOnClickListener(this);
	}

	public static Intent getActivityIntent(Context context, User user,
			boolean fromShare) {
		return new Intent(context, Edit_More_Profile_Activity.class).putExtra(
				USER_OBJ_KEY, user).putExtra(
				Edit_UserProfile_Activity.fromShare, fromShare);
	}

	@Override
	public void onBackPressed() {
		backPressed();

	}

	private void backPressed() {
		Edit_More_Profile_Fragment profileFragment = (Edit_More_Profile_Fragment) getSupportFragmentManager()
				.findFragmentByTag("edit_more_profile_frag");
		// if(!
		// profileFragment.buildUser().equals(UserManager.getInstance().getCurrentUser())){
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					Edit_More_Profile_Activity.super.onBackPressed();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					dialog.dismiss();
					break;
				}
			}
		};
		Dialog dialog = new CustomAlertDialog.customBuilder(this)
				.setTitle(R.string.exit).setMessage(R.string.save_changes_msg)
				.setPositiveButton(android.R.string.ok, listener)
				.setNegativeButton(android.R.string.cancel, listener).create();
		dialog.show();
		// }
		// else
		// Edit_More_Profile_Activity.super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backBtn:
			backPressed();
			break;

		default:
			break;
		}
	}

}
