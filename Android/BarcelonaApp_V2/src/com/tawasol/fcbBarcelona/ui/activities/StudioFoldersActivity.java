package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;

public class StudioFoldersActivity extends BaseActivity implements
		OnClickListener {
	public static final int PLAYER_FOLDER = 0;
	public static final int TEAM_FOLDER = 1;
	public static final int PURCHASED_FOLDER = 2;
	public static final String TAB_OBJ = "whcihTab";
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_studio_folders);
		// Get a Tracker (should auto-report)
		((App) getApplication()).getTracker(App.TrackerName.APP_TRACKER);
		// hideBottomBar();
		setTitle(getResources().getString(R.string.Gallary));
		showMenuBtn();

		getRightBtn().setCompoundDrawablesWithIntrinsicBounds(
				R.drawable.fan_filter, 0, 0, 0);
		getRightBtn().setText(" ");
		getRightBtn().setOnClickListener(this);
		showRightBtn();
	}

	public static Intent getActivityIntent(Context context , int whichTab) {
		return new Intent(context, StudioFoldersActivity.class).putExtra(TAB_OBJ, whichTab);
	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshMenu();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rightBtn:
			startActivity(FoldersFilterActivity
					.getActivityIntent(StudioFoldersActivity.this));
			break;

		default:
			break;
		}

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// Stop the analytics tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}


}
