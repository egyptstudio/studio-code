package com.tawasol.fcbBarcelona.ui.activities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.almeros.android.multitouch.MoveGestureDetector;
import com.almeros.android.multitouch.RotateGestureDetector;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.customView.CustomSeekBar;
import com.tawasol.fcbBarcelona.entities.StudioPhoto;
import com.tawasol.fcbBarcelona.ui.activities.CaptureActivity.Flash.FlashType;

/**
 * 
 * @author Basyouni
 * 
 */

// dear future me try not to panic when you see that code again :D
@SuppressWarnings("deprecation")
public class CaptureActivity extends Activity implements
		SurfaceHolder.Callback, OnClickListener, View.OnTouchListener,
		OnSeekBarChangeListener {

	public static Matrix mMatrix;
	public static float mScaleFactor = 1f;
	public static float mRotationDegrees = 0.f;
	public static float mFocusX = 0.f;
	public static float mFocusY = 0.f;

	private int mImageHeight, mImageWidth;

	private ScaleGestureDetector mScaleDetector;
	private RotateGestureDetector mRotateDetector;
	private MoveGestureDetector mMoveDetector;
	private String oriantation;
	View titleBar;
	int cameraWidth;

	// ----------------------

	// These matrices will be used to move and zoom image
	Matrix matrix = new Matrix();
	boolean hasFrontCamera = false;
	// ----------------------
	private String imagePath;
	private SurfaceView surfaceView;
	private SurfaceHolder surfaceHolder;
	private boolean isRunning;
	Context context = this;
	public static Camera camera = null;
	private RelativeLayout preview;
	private Bitmap capturedBitmap, finalBitmap;
	private ImageView capturedImage;
	private Button captureButton;
	private Button front;
	private TextView flash;
	private LinearLayout containerLayout;
	private int cameraId;
	boolean backIsOn = false;
	boolean chooseFront = false;
	private boolean hasBackCamera;
	private StudioPhoto photo;
	private boolean flashOn = true;
	private ProgressDialog dialog;
	private ImageView maskImageView;
	int currentVersion;
	private boolean hasFlash;
	private CustomSeekBar seekBar;
	private Flash flashObject;
	private ImageButton back;
	Bitmap Maskbitmap;

	// private CameraPreview maPreview;

	public static class Flash {
		public static enum FlashType {
			AUTO, ON, OFF
		}

		FlashType type;

		public FlashType getType() {
			return type;
		}

		public void setType(FlashType type) {
			this.type = type;
		}

	}

	Size getSitubleSize(Parameters params) {
		List<Size> sizes = params.getSupportedPictureSizes();
		Size mSize = null;
		for (Size size : sizes) {
			Log.i("avilable", "Available resolution: " + size.width + " "
					+ size.height);
			if (size.width <= 2000) {
				mSize = size;
				break;
			}
		}
		return mSize;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		checkCameraAvialability();

		currentVersion = android.os.Build.VERSION.SDK_INT;
		PackageManager packageManager = context.getPackageManager();

		// if device support flash?
		hasFlash = packageManager
				.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

		photo = (StudioPhoto) getIntent().getExtras().get("selectedPhoto");
		// if (photo.getCaptureType() ==
		// StudioPhoto.PHOTO_CAPTURE_TYPE_MASK_SHOT)
		// if (photo.getPhotoOrientation() ==
		// StudioPhoto.STUDIO_PHOTO_ORIENTATION_LANDSCAPE)
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		// else if (photo.getPhotoOrientation() ==
		// StudioPhoto.STUDIO_PHOTO_ORIENTATION_PORTRAIT)
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		mScaleDetector = new ScaleGestureDetector(getApplicationContext(),
				new ScaleListener());
		mRotateDetector = new RotateGestureDetector(getApplicationContext(),
				new RotateListener());
		mMoveDetector = new MoveGestureDetector(getApplicationContext(),
				new MoveListener());
		setContentView(R.layout.cam_screen);

		// camera = Camera.open();
		//
		// maLayoutPreview = (FrameLayout) findViewById(R.id.camera_preview);
		//
		// surfaceView = new CameraPreview(this, camera);
		//
		// Point displayDim = getDisplayWH();
		// Point layoutPreviewDim = calcCamPrevDimensions(displayDim,
		// surfaceView.getOptimalPreviewSize(surfaceView.prSupportedPreviewSizes,
		// displayDim.x, displayDim.y));
		// if (layoutPreviewDim != null) {
		// RelativeLayout.LayoutParams layoutPreviewParams =
		// (RelativeLayout.LayoutParams) maLayoutPreview.getLayoutParams();
		// layoutPreviewParams.width = layoutPreviewDim.x;
		// layoutPreviewParams.height = layoutPreviewDim.y;
		// layoutPreviewParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		// maLayoutPreview.setLayoutParams(layoutPreviewParams);
		// }
		// maLayoutPreview.addView(surfaceView);

		dialog = new ProgressDialog(this, R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

		Maskbitmap = initMask();

		initViews();

		flashObject = new Flash();
		if (hasFlash)
			flashObject.setType(FlashType.ON);
		else {
			flashObject.setType(FlashType.OFF);
			flash.setText(getResources().getString(
					R.string.flash_mode_Off_capital));
		}

		maskImageView.setImageBitmap(Maskbitmap);

		// Maskbitmap = null;

		if (currentVersion < Build.VERSION_CODES.HONEYCOMB)
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		captureButton.setOnClickListener(this);
		flash.setOnClickListener(this);
		front.setOnClickListener(this);
		back.setOnClickListener(this);
		maskImageView.setOnTouchListener(this);
	}

	@SuppressLint("NewApi")
	private Point getDisplayWH() {

		Display display = this.getWindowManager().getDefaultDisplay();
		Point displayWH = new Point();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			display.getSize(displayWH);
			return displayWH;
		}
		displayWH.set(display.getWidth(), display.getHeight());
		return displayWH;
	}

	/**
	 * 
	 */
	private Bitmap initMask() {
		final String path = getIntent().getExtras().getString("image");
		File f = new File(path, "image.jpg");
		Bitmap bitmap = getBitmap(f);
		Drawable d = new BitmapDrawable(getResources(), bitmap);
		mImageHeight = d.getIntrinsicHeight();
		mImageWidth = d.getIntrinsicWidth();
		deleteFileNoThrow(path);
		return /* SetBrightness( */bitmap/* , -60) */;
	}

	/**
	 * 
	 */
	private void initViews() {
		preview = (RelativeLayout) findViewById(R.id.basePreview);
		containerLayout = (LinearLayout) findViewById(R.id.container);
		surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
		front = (Button) findViewById(R.id.button2);
		flash = (TextView) findViewById(R.id.flash);
		capturedImage = (ImageView) findViewById(R.id.camera_image);
		maskImageView = (ImageView) findViewById(R.id.maskImage);
		maskImageView.setBackgroundColor(getResources().getColor(
				android.R.color.transparent));
		captureButton = (Button) findViewById(R.id.button1);
		titleBar = findViewById(R.id.title_bar);
		back = (ImageButton) findViewById(R.id.backBtn);
		seekBar = (CustomSeekBar) findViewById(R.id.seekBar1);
		seekBar.setProgress(5);
		seekBar.setOnSeekBarChangeListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		seekBar.setProgress(5);
		capturedBitmap = null;
		capturedImage.setImageBitmap(capturedBitmap);
		Display display = getWindowManager().getDefaultDisplay();
		mFocusX = display.getWidth() / 2f;
		mFocusY = display.getHeight() / 2f;

		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);
		float scaledImageCenterX = (mImageWidth * mScaleFactor) / 2;
		float scaledImageCenterY = (mImageHeight * mScaleFactor) / 2;
		mMatrix = new Matrix();
		mMatrix.reset();
		mMatrix.postScale(mScaleFactor, mScaleFactor);
		mMatrix.postRotate(mRotationDegrees, scaledImageCenterX,
				scaledImageCenterY);
		mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY
				- scaledImageCenterY);
		maskImageView.setImageMatrix(mMatrix);
		if (captureButton != null)
			captureButton.setEnabled(true);
		if (maskImageView != null)
			maskImageView.setEnabled(true);
		if (preview != null)
			preview.setEnabled(true);
		if (containerLayout != null)
			containerLayout.setEnabled(true);
		if (surfaceView != null)
			surfaceView.setEnabled(true);
		if (front != null)
			front.setEnabled(true);
		if (flash != null)
			flash.setEnabled(true);
		if (capturedImage != null)
			capturedImage.setEnabled(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
		matrix.reset();
		mScaleFactor = 1f;
		mRotationDegrees = 0.f;
		mFocusX = 0.f;
		mFocusY = 0.f;

	}

	private Bitmap getBitmap(File f) {
		return BitmapFactory.decodeFile(f.getAbsolutePath());
	}

	private boolean deleteFileNoThrow(String path) {
		File file;
		try {
			file = new File(path);
		} catch (NullPointerException e) {
			return false;
		}

		if (file.exists()) {
			return file.delete();
		}
		return false;
	}

	@SuppressLint("NewApi")
	void startFront() {
		if ((chooseFront && hasBackCamera) || (backIsOn && hasFrontCamera)) {
			if (isRunning && hasBackCamera) {
				camera.stopPreview();
				camera.release();
			}
			if (backIsOn) {
				chooseFront = true;
			} else {
				chooseFront = false;
			}
			if (hasFrontCamera && chooseFront) {
				camera = Camera.open(cameraId);
				backIsOn = false;
				flash.setText(getResources().getString(
						R.string.flash_mode_Off_capital));
				flashObject.setType(FlashType.OFF);
			} else if (hasBackCamera && !chooseFront) {
				camera = Camera.open();
				backIsOn = true;
				flash.setText(getResources().getString(
						R.string.flash_mode_On_capital));
				flashObject.setType(FlashType.ON);
			}
			try {
				if (hasBackCamera)
					camera.setPreviewDisplay(surfaceHolder);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				return;
			}
			Camera.Parameters params = camera.getParameters();
			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
				if (currentVersion != 7) {
					camera.setDisplayOrientation(90);
					if (!chooseFront) {
						params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
						flash.setText(getResources().getString(
								R.string.flash_mode_On_capital));
						flashObject.setType(FlashType.ON);
					}
					// Size mSize = getSitubleSize(params);
					// params.setPictureSize(mSize.width, mSize.height);
					camera.setParameters(params);
				} else {
					params.setRotation(90);
					if (!chooseFront) {
						params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
						flash.setText(getResources().getString(
								R.string.flash_mode_On_capital));
						flashObject.setType(FlashType.ON);
					}
					// Size mSize = getSitubleSize(params);
					// params.setPictureSize(mSize.width, mSize.height);
					camera.setParameters(params);

				}
			} else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
				if (!chooseFront) {
					params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
					flash.setText(getResources().getString(
							R.string.flash_mode_On_capital));
					flashObject.setType(FlashType.ON);
				}
				// Size mSize = getSitubleSize(params);
				// params.setPictureSize(mSize.width, mSize.height);
				camera.setParameters(params);

			}

			camera.startPreview();
		} else {
			if (!hasFrontCamera) {
				Toast.makeText(getApplicationContext(), "u dont have front",
						Toast.LENGTH_SHORT).show();
				return;
			} else if (!hasBackCamera) {
				Toast.makeText(getApplicationContext(), "u dont have back",
						Toast.LENGTH_SHORT).show();
				return;
			}
		}
		captureButton.setEnabled(true);
		maskImageView.setEnabled(true);
		preview.setEnabled(true);
		containerLayout.setEnabled(true);
		surfaceView.setEnabled(true);
		front.setEnabled(true);
		flash.setEnabled(true);
		capturedImage.setEnabled(true);
	}

	/**
	 * 
	 */
	@SuppressLint("NewApi")
	private void checkCameraAvialability() {
		int cameraCount = 0;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		cameraCount = Camera.getNumberOfCameras();
		for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
			Camera.getCameraInfo(camIdx, cameraInfo);
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				hasFrontCamera = true;
			} else if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
				hasBackCamera = true;
			}
		}
		if (context.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA_FRONT)) {
			cameraId = getFrontCameraId();

			if (cameraId != -1) {
				hasFrontCamera = true;
			}
		}
	}

	@SuppressLint("NewApi")
	private int getFrontCameraId() {
		int camId = -1;
		int numberOfCameras = Camera.getNumberOfCameras();
		CameraInfo ci = new CameraInfo();

		for (int i = 0; i < numberOfCameras; i++) {
			Camera.getCameraInfo(i, ci);
			if (ci.facing == CameraInfo.CAMERA_FACING_FRONT) {
				camId = i;
			}
		}
		return camId;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		if (isRunning) {
			camera.stopPreview();
		}
		try {
			if (camera == null)
				camera = Camera.open();// open camera
			camera.setPreviewDisplay(holder);
			camera.startPreview();
			isRunning = true;
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressLint("NewApi")
	public void surfaceCreated(SurfaceHolder holder) {
		try {
			if (camera != null) {
				camera.stopPreview();
				camera.release();
				camera = null;
			}
			if (hasBackCamera) {
				camera = Camera.open();// open camera
				android.hardware.Camera.Parameters parameters = camera
						.getParameters();
				android.hardware.Camera.Size size = parameters.getPictureSize();
				cameraWidth = size.width;

				chooseFront = false;
				backIsOn = true;
			} else if (hasFrontCamera) {
				camera = Camera.open(cameraId);
				backIsOn = false;
				chooseFront = true;
			}
			Camera.Parameters params = camera.getParameters();
			int currentInt = android.os.Build.VERSION.SDK_INT;
			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
				if (currentInt != 7) {
					camera.setDisplayOrientation(90);
					if (flashOn && backIsOn)
						params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
					camera.setParameters(params);
				} else {
					params.setRotation(90);
					if (flashOn && backIsOn)
						params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
					// Size size = getSitubleSize(params);
					// params.setPictureSize(size.width, size.height);
					camera.setParameters(params);

				}
			} else {
				params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
				camera.setParameters(params);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), getString(R.string.error),
					Toast.LENGTH_SHORT).show();
			CaptureActivity.this.finish();
		}
	}

	void changeFlash() {
		if (hasFlash) {
			if (backIsOn) {
				if (flashObject.getType() == FlashType.ON) {
					Camera.Parameters params = null;
					if (camera != null)
						params = camera.getParameters();
					else {
						camera = Camera.open();
						params = camera.getParameters();
					}
					params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
					flashObject.setType(FlashType.OFF);
					// Size mSize = getSitubleSize(params);
					// params.setPictureSize(mSize.width, mSize.height);
					camera.setParameters(params);
					camera.startPreview();
					flash.setText(getResources().getString(
							R.string.flash_mode_Off_capital));
					flashOn = false;
				} else if (flashObject.getType() == FlashType.OFF) {
					Camera.Parameters params = camera.getParameters();
					params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
					flashObject.setType(FlashType.AUTO);
					// Size mSize = getSitubleSize(params);
					// params.setPictureSize(mSize.width, mSize.height);
					camera.setParameters(params);
					camera.startPreview();
					// flash.setBackgroundResource(R.drawable.flash_sc);
					flash.setText(getResources().getString(
							R.string.flash_mode_Auto));
					flashOn = true;
				} else if (flashObject.getType() == FlashType.AUTO) {
					Camera.Parameters params = camera.getParameters();
					params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
					flashObject.setType(FlashType.ON);
					flash.setText(getResources().getString(
							R.string.flash_mode_On_capital));
					// Size mSize = getSitubleSize(params);
					// params.setPictureSize(mSize.width, mSize.height);
					camera.setParameters(params);
					camera.startPreview();
					// flash.setBackgroundResource(R.drawable.flash_sc);
					// flash.setText(getResources().getString(R.string.flash_mode_On));
					flashOn = true;
				}
			} else {
				Toast.makeText(getApplicationContext(),
						"Flash mode is enabled with back camera only",
						Toast.LENGTH_SHORT).show();
			}
			captureButton.setEnabled(true);
			maskImageView.setEnabled(true);
			preview.setEnabled(true);
			containerLayout.setEnabled(true);
			surfaceView.setEnabled(true);
			front.setEnabled(true);
			flash.setEnabled(true);
			capturedImage.setEnabled(true);
		} else {
			Toast.makeText(getApplicationContext(),
					"You don't seem to has flash", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		try {
			camera.stopPreview();
			camera.release();
			camera = null;
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Couldn't open camera",
					Toast.LENGTH_SHORT).show();
		}
	}

	Bitmap getBitmapFromView(View v) {
		Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(),
				Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		v.draw(c);
		return b;
	}

	String getLocalDirectoryPath() {
		ContextWrapper cw = new ContextWrapper(CaptureActivity.this);
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		return directory.getAbsolutePath();
	}

	// save to internal storage
	private String saveToInternalSorageBitmap(Bitmap bitmapImage,
			String fileName, boolean original) {
		// preview.setDrawingCacheEnabled(false);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		// if(!original)
		// if(cameraWidth < 3500)
		// bitmapImage.compress(CompressFormat.JPEG, 80, bos);
		// else if (cameraWidth > 3500 && cameraWidth < 4200)
		// bitmapImage.compress(CompressFormat.JPEG, 60, bos);
		// else
		// bitmapImage.compress(CompressFormat.JPEG, 40, bos);
		// else
		bitmapImage.compress(CompressFormat.JPEG, 100, bos);
		byte[] bitmapdata = bos.toByteArray();

		ContextWrapper cw = new ContextWrapper(CaptureActivity.this);
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		// Create imageDir
		File mypath = new File(directory, fileName);
		if (mypath.exists())
			mypath.delete();
		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(mypath);
			fos.write(bitmapdata);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return directory.getAbsolutePath();
	}

	// // save to External storage
	// private String saveToExternalSorageBitmap(Bitmap bitmapImage,
	// String fileName, boolean original) {
	// // preview.setDrawingCacheEnabled(false);
	// ByteArrayOutputStream bos = new ByteArrayOutputStream();
	// if (!original)
	// if (cameraWidth < 3500)
	// bitmapImage.compress(CompressFormat.JPEG, 80, bos);
	// else if (cameraWidth > 3500 && cameraWidth < 4200)
	// bitmapImage.compress(CompressFormat.JPEG, 60, bos);
	// else
	// bitmapImage.compress(CompressFormat.JPEG, 40, bos);
	// else
	// bitmapImage.compress(CompressFormat.JPEG, 100, bos);
	// byte[] bitmapdata = bos.toByteArray();
	//
	// // ContextWrapper cw = new ContextWrapper(CaptureActivity.this);
	// // path to /data/data/yourapp/app_data/imageDir
	// // File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
	// // Create imageDir
	//
	// File directory = new File(Environment.getExternalStorageDirectory()
	// .getAbsolutePath() + File.separator + "FCBStudio");
	// if (!directory.exists())
	// directory.mkdir();
	// File mypath = new File(directory, fileName);
	// if (mypath.exists())
	// mypath.delete();
	// FileOutputStream fos = null;
	// try {
	//
	// fos = new FileOutputStream(mypath);
	// fos.write(bitmapdata);
	// fos.close();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return directory.getAbsolutePath();
	// }

	@SuppressLint("NewApi")
	public void TakeScreenshot(String originalPicName) {
		captureButton.setVisibility(View.INVISIBLE);
		containerLayout.setVisibility(View.INVISIBLE);
		seekBar.setVisibility(View.INVISIBLE);
		titleBar.setVisibility(View.INVISIBLE);
		Bitmap bmp = getBitmapFromView(preview);
		// saveToExternalSorageBitmap(bmp, "captured" +
		// System.currentTimeMillis()
		// + ".jpg", false);
		titleBar.setVisibility(View.VISIBLE);
		seekBar.setVisibility(View.VISIBLE);
		// ByteArrayOutputStream bos = new ByteArrayOutputStream();
		// bmp.compress(CompressFormat.JPEG, 100, bos);
		// byte[] bitmapdata = bos.toByteArray();
		String path = /* saveToInternalSorageByte(bitmapdata); */saveToInternalSorageBitmap(
				bmp, "tempImage.jpg", false);
		File tmpFile = new File(path, "tempImage.jpg");
		imagePath = tmpFile.getAbsolutePath();
		captureButton.setVisibility(View.VISIBLE);
		containerLayout.setVisibility(View.VISIBLE);
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (!isFinishing())
					dialog.dismiss();
			}
		});
		startActivity(ShareActivity.getActivityIntent(CaptureActivity.this,
				photo, imagePath, oriantation, getLocalDirectoryPath()
						+ File.separator + originalPicName, true, true, 0,
				true, cameraWidth));
		// startActivity(Share.getActivityIntentScreen(CaptureActivity.this,
		// photo, imagePath, oriantation ,
		// getLocalDirectoryPath()+File.separator+originalPicName));
		resetValues();
	}

	private void resetValues() {
		mMatrix = null;
		mScaleFactor = 2f;
		mRotationDegrees = 0.f;
		mFocusX = 0.f;
		mFocusY = 0.f;
	}

	private PictureCallback mPicture = new PictureCallback() { // THIS METHOD
																// AND THE
																// METHOD BELOW
		// CONVERT THE CAPTURED IMAGE IN A JPG FILE AND SAVE IT

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			capture pic = new capture(data);
			pic.execute();
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			takePic();
			break;
		case R.id.button2:
			startFront();
			break;
		case R.id.flash:
			changeFlash();
			break;
		case R.id.backBtn:
			this.onBackPressed();
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 */
	private void takePic() {
		captureButton.setEnabled(false);
		captureButton.setEnabled(false);
		maskImageView.setEnabled(false);
		preview.setEnabled(false);
		containerLayout.setEnabled(false);
		surfaceView.setEnabled(false);
		front.setEnabled(false);
		flash.setEnabled(false);
		capturedImage.setEnabled(false);
		if (camera == null)
			camera = Camera.open();
		camera.takePicture(null, null, mPicture);// TAKING THE
		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_PORTRAIT)
			oriantation = "portrait";
		else if (orientation == Configuration.ORIENTATION_LANDSCAPE)
			oriantation = "LANDSCAPE";
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// ImageView view = (ImageView) v;
		//
		// // Dump touch event to log
		// //dumpEvent(event);
		//
		// // Handle touch events here...
		// switch (event.getAction() & MotionEvent.ACTION_MASK) {
		if (event != null) {
			switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				captureButton.setEnabled(false);
				// imageView.setEnabled(true);
				preview.setEnabled(false);
				containerLayout.setEnabled(false);
				surfaceView.setEnabled(false);
				front.setEnabled(false);
				flash.setEnabled(false);
				capturedImage.setEnabled(false);
				break;
			case MotionEvent.ACTION_UP:
				captureButton.setEnabled(true);
				// imageView.setEnabled(false);
				preview.setEnabled(true);
				containerLayout.setEnabled(true);
				surfaceView.setEnabled(true);
				front.setEnabled(true);
				flash.setEnabled(true);
				capturedImage.setEnabled(true);
				break;
			}
			if (mScaleDetector != null)
				mScaleDetector.onTouchEvent(event);
			if (getIntent().getBooleanExtra("singlePlayer", false)
					&& mRotateDetector != null)
				mRotateDetector.onTouchEvent(event);
			if (mMoveDetector != null)
				mMoveDetector.onTouchEvent(event);
			float scaledImageCenterX = (mImageWidth * mScaleFactor) / 2;
			float scaledImageCenterY = (mImageHeight * mScaleFactor) / 2;
			mMatrix = new Matrix();
			mMatrix.reset();
			mMatrix.postScale(mScaleFactor, mScaleFactor);
			mMatrix.postRotate(mRotationDegrees, scaledImageCenterX,
					scaledImageCenterY);
			mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY
					- scaledImageCenterY);

			ImageView view = (ImageView) v;
			view.setImageMatrix(mMatrix);
		}
		return true; // indicate event was handled
	}

	private class ScaleListener extends
			ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			mScaleFactor *= detector.getScaleFactor(); // scale change since
														// previous event

			// Don't let the object get too small or too large.
			mScaleFactor = Math.max(0.6f, Math.min(mScaleFactor, 3f));

			return true;
		}
	}

	private class RotateListener extends
			RotateGestureDetector.SimpleOnRotateGestureListener {
		@Override
		public boolean onRotate(RotateGestureDetector detector) {
			mRotationDegrees -= detector.getRotationDegreesDelta();
			return true;
		}
	}

	private class MoveListener extends
			MoveGestureDetector.SimpleOnMoveGestureListener {
		@Override
		public boolean onMove(MoveGestureDetector detector) {
			PointF d = detector.getFocusDelta();
			mFocusX += d.x;
			mFocusY += d.y;
			Display display = getWindowManager().getDefaultDisplay();
			mFocusX = Math.max(80, Math.min(mFocusX, display.getWidth() - 80));
			mFocusY = Math.max(80, Math.min(mFocusY, display.getHeight() - 80));
			// mFocusX = detector.getFocusX();
			// mFocusY = detector.getFocusY();
			return true;
		}
	}

	@Override
	public void onBackPressed() {
		mMatrix.reset();
		mScaleFactor = 1f;
		mRotationDegrees = 0.f;
		mFocusX = 0.f;
		mFocusY = 0.f;
		CaptureActivity.super.onBackPressed();

		// DialogInterface.OnClickListener yesListener = new
		// DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// CaptureActivity.super.onBackPressed();
		// }
		// };
		//
		// DialogInterface.OnClickListener noListener = new
		// DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// return;
		// }
		// };
		// AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// builder.setTitle("Warning");
		// builder.setMessage(getResources().getString(
		// R.string.capture_back_pressed));
		// builder.setPositiveButton(
		// getResources().getString(R.string.capture_back_pressed_confirm),
		// yesListener).setNegativeButton(android.R.string.no, noListener)
		// .setCancelable(false);
		// AlertDialog dialog = builder.show();
		//
		// // Must call show() prior to fetching text view
		// TextView messageView = (TextView) dialog
		// .findViewById(android.R.id.message);
		// messageView.setGravity(Gravity.CENTER);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public static Intent getActivityIntent(Context context, String image,
			StudioPhoto selectedPhoto, boolean singlePlayer) {
		return new Intent(context, CaptureActivity.class)
				.putExtra("image", image)
				.putExtra("selectedPhoto", selectedPhoto)
				.putExtra("singlePlayer", singlePlayer)
				.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	}

	String path;

	private class capture extends AsyncTask<Void, Void, Bitmap> {

		byte[] data;
		String path;
		String originalPicName = "image" + System.currentTimeMillis() + ".jpg";

		public capture(byte[] data) {
			this.data = data;
		}

		@Override
		protected void onPreExecute() {
			// captureButton.setVisibility(View.INVISIBLE);
			// containerLayout.setVisibility(View.INVISIBLE);
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected Bitmap doInBackground(Void... params) {
			// path = saveToInternalSorageByte(data);
			// File f = new File(path, "tempImage.jpg");
			try {
				capturedBitmap = /* getBitmap(f); */BitmapFactory
						.decodeByteArray(data, 0, data.length);
				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
					matrix.postRotate(90);
				else
					matrix.postRotate(0);
				finalBitmap = Bitmap.createBitmap(capturedBitmap, 0, 0,
						capturedBitmap.getWidth(),
						capturedBitmap.getHeight() - 40, matrix, true);
				createOriginalRawImage(finalBitmap, originalPicName);
				// saveToExternalSorageBitmap(finalBitmap, originalPicName,
				// true);
				return /* SetBrightness( */finalBitmap/* , 40) */;
			} catch (Exception e) {
				CaptureActivity.this.finish();
				Toast.makeText(CaptureActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			super.onPostExecute(bitmap);
			if (finalBitmap != null) {
				if (hasFrontCamera && chooseFront) {
					new FlipMatrix().execute();
				} else {

					capturedImage.setImageBitmap(finalBitmap); // SETTING
					// File f = new File(path, "tempImage.jpg");
					// f.delete();
					TakeScreenshot(originalPicName);// CALLING THIS METHOD TO
													// TAKE A
													// SCREENSHOT
				}
			}
		}

	}

	/**
	 * @param finalBitmap
	 */
	public void createOriginalRawImage(final Bitmap finalBitmap,
			final String fileName) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// File dir_image2 = new File(Environment
				// .getExternalStorageDirectory().getAbsolutePath()
				// + File.separator + "MYImage");
				// dir_image2.mkdirs();
				//
				// File tmpFile = new File(dir_image2,
				// "LargeOriginalSingleImage.jpg");
				//
				// try {
				// FileOutputStream fos = new FileOutputStream(tmpFile);
				// finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
				// fos.close();
				// } catch (FileNotFoundException e) {
				// Toast.makeText(getApplicationContext(),
				// getString(R.string.error), Toast.LENGTH_SHORT)
				// .show();
				// } catch (IOException e) {
				// Toast.makeText(getApplicationContext(),
				// getString(R.string.error), Toast.LENGTH_SHORT)
				// .show();
				// }

				saveToInternalSorageBitmap(finalBitmap, fileName, true);
			}
		}).start();
	}

	// public Bitmap SetBrightness(Bitmap src, int value) {
	// // original image size
	// int width = src.getWidth();
	// int height = src.getHeight();
	// // create output bitmap
	// Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
	// // color information
	// int A, R, G, B;
	// int pixel;
	//
	// // scan through all pixels
	// for (int x = 0; x < width; ++x) {
	// for (int y = 0; y < height; ++y) {
	// // get pixel color
	// pixel = src.getPixel(x, y);
	// A = Color.alpha(pixel);
	// R = Color.red(pixel);
	// G = Color.green(pixel);
	// B = Color.blue(pixel);
	//
	// // increase/decrease each channel
	// R += value;
	// if (R > 255) {
	// R = 255;
	// } else if (R < 0) {
	// R = 0;
	// }
	//
	// G += value;
	// if (G > 255) {
	// G = 255;
	// } else if (G < 0) {
	// G = 0;
	// }
	//
	// B += value;
	// if (B > 255) {
	// B = 255;
	// } else if (B < 0) {
	// B = 0;
	// }
	//
	// // apply new pixel color to output bitmap
	// bmOut.setPixel(x, y, Color.argb(A, R, G, B));
	// }
	// }
	//
	// // return final image
	// return bmOut;
	// }

	/**
	 * 
	 * @param bmp
	 *            input bitmap
	 * @param contrast
	 *            0..10 1 is default
	 * @param brightness
	 *            -255..255 0 is default
	 * @return new bitmap
	 */
	public static Bitmap SetBrightness(Bitmap bmp, float brightness,
			float contrast) {
		ColorMatrix cm = new ColorMatrix(new float[] { contrast, 0, 0, 0,
				brightness, 0, contrast, 0, 0, brightness, 0, 0, contrast, 0,
				brightness, 0, 0, 0, 1, 0 });

		Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(),
				bmp.getConfig());

		Canvas canvas = new Canvas(ret);

		Paint paint = new Paint();
		paint.setColorFilter(new ColorMatrixColorFilter(cm));
		canvas.drawBitmap(bmp, 0, 0, paint);

		return ret;
	}

	// save to internal storage
	private String saveToInternalSorageByte(byte[] bitmapImage) {
		ContextWrapper cw = new ContextWrapper(CaptureActivity.this);
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		// Create imageDir
		File mypath = new File(directory, "tempImage.jpg");

		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(mypath);

			// Use the compress method on the BitMap object to write image to
			// the OutputStream
			fos.write(bitmapImage);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return directory.getAbsolutePath();
	}

	private class FlipMatrix extends AsyncTask<Void, Void, Bitmap> {
		String originalPicName = "image" + System.currentTimeMillis() + ".jpg";
		Bitmap bitmap;

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.show();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		protected Bitmap doInBackground(Void... params) {
			// Matrix rotateRight = null;
			if (android.os.Build.VERSION.SDK_INT > 13) {
				Matrix rotateRight = null;
				float[] mirrorY = { -1, 0, 0, 0, 1, 0, 0, 0, 1 };
				rotateRight = new Matrix();
				Matrix matrixMirrorY = new Matrix();
				matrixMirrorY.setValues(mirrorY);

				rotateRight.postConcat(matrixMirrorY);
				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
					rotateRight.preRotate(270);
				else
					rotateRight.preRotate(0);

				bitmap = Bitmap.createBitmap(capturedBitmap, 0, 0,
						capturedBitmap.getWidth(), capturedBitmap.getHeight(),
						rotateRight, true);

			} else {
				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
					matrix.postRotate(-90);// make a new matrix set it's
				// orientation
				else
					matrix.postRotate(0);
				bitmap = Bitmap.createBitmap(capturedBitmap, 0, 0,
						capturedBitmap.getWidth(), capturedBitmap.getHeight(),
						matrix, true);
			}
			createOriginalRawImage(bitmap, originalPicName);
			return /* SetBrightness( */bitmap /* , 40) */;

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			super.onPostExecute(bitmap);

			capturedImage.setImageBitmap(bitmap);
			dialog.dismiss();
			// IN AN IMAGEVIEW(SOMETHING
			// LIKE A BACKGROUNG FOR THE LAYOUT)
			File f = new File(path, "tempImage.jpg");
			f.delete();
			// tmpFile.delete(); // delete tmp file after it done it's job
			// ScreenShot shot = new ScreenShot();
			// shot.execute();
			TakeScreenshot(originalPicName);// CALLING THIS METHOD TO TAKE A
											// SCREENSHOT

		}

	}

	int progress = 0;
	int originalProgress;

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		this.progress = progress;
		switch (progress) {
		case 1:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, -40, 1));
			break;
		case 2:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, -30, 1));
			break;
		case 3:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, -20, 1));
			break;
		case 0:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, -10, 1));
			break;
		case 5:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 0, 1));
			break;
		case 6:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 10, 1));
			break;
		case 7:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 20, 1));
			break;
		case 8:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 30, 1));
			break;
		case 9:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 40, 1));
			break;
		case 10:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 50, 1));
			break;
		default:
			break;
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		switch ((int) Math.floor(progress)) {
		case 1:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, -40, 1));
			break;
		case 2:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, -30, 1));
			break;
		case 3:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, -20, 1));
			break;
		case 0:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, -10, 1));
			break;
		case 5:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 0, 1));
			break;
		case 6:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 10, 1));
			break;
		case 7:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 20, 1));
			break;
		case 8:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 30, 1));
			break;
		case 9:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 40, 1));
			break;
		case 10:
			maskImageView.setImageBitmap(SetBrightness(Maskbitmap, 50, 1));
			break;
		default:
			break;
		}
	}
}