package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle; 
import com.tawasol.fcbBarcelona.R; 
/**
 * @author Turki
 * 
 */
public class ContactUsActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_contact_us);
		setTitle(getResources().getString(R.string.settings_contact_us));
		showBackBtn(); 
		hideMenuBtn();
		hideBottomBar(); // added by mohga
	}
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, ContactUsActivity.class);
	}
}
