package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.StudioFolder;
import com.tawasol.fcbBarcelona.ui.fragments.ImageDitailsFragment;
import com.tawasol.fcbBarcelona.ui.fragments.StudioPhotosFragment;

public class StudioPhotosActivity extends BaseActivity {

	static StudioFolder selectedfolder;
	public static final String STUDIO_FOLDER_KEY="SelectedFolder";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_studio_photos);
//		hideBottomBar();
		if(selectedfolder != null)
		setTitle(selectedfolder.getFolderName());
		showMenuBtn();
		showBackBtn();
		
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		refreshMenu();
	}
	public static Intent getActivityIntent(Context context,StudioFolder folder , boolean showAds) {
		selectedfolder = folder;
		return new Intent(context, StudioPhotosActivity.class)
		.putExtra(STUDIO_FOLDER_KEY, folder).putExtra(StudioPhotosFragment.ShowAds, showAds);
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		StudioPhotosFragment photosFrag = (StudioPhotosFragment) getSupportFragmentManager()
				.findFragmentById(R.id.photos_fragment_id);
		//photosFrag.onActivityResult(arg0, arg1, arg2);
	}
	}


