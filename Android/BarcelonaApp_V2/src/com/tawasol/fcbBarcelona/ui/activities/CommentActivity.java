/**
 * 
 */
package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.customView.PostViewFactory;
import com.tawasol.fcbBarcelona.data.cache.WallTable;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.entities.PostViewModel;
import com.tawasol.fcbBarcelona.managers.WallManager;
import com.tawasol.fcbBarcelona.ui.fragments.CommentFragment;

/**
 * @author Basyouni
 *
 */
public class CommentActivity extends BaseActivity {

	public static final String CommentsCount = "count";
	public static String POST_ID = "postId";
	public static String COMMENTCOUNT = "commentCount";
	public static String WHICH_TAB = "whichTab";
	public static String OWN_POST = "ownPost";
	String commentsCountountString = "";

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.comment_activity);
		Log.i("Inside comment Activity Count",
				String.valueOf(WallTable.getInstance().getCount()));
		hideBottomBar();
		hideMenuBtn();
		showBackBtn();

		/* Added by Mohga */
		int commentsCount = getIntent().getIntExtra(COMMENTCOUNT, 0);
//		if (commentsCount > 0)
		commentsCountountString = String.valueOf(commentsCount);
		setTitle(commentsCountountString + " "+ getString(R.string.notifications_comments));
	}

	public static Intent getIntent(Context context, int postId,
			int commentCount, int whichTab, boolean ownPost) {
		Bundle bundle = new Bundle();
		bundle.putInt(POST_ID, postId);
		bundle.putInt(COMMENTCOUNT, commentCount);
		bundle.putInt(WHICH_TAB, whichTab);
		bundle.putBoolean(OWN_POST, ownPost);
		return new Intent(context, CommentActivity.class).putExtras(bundle);
	}

	public void updateTitle(int count) {
//		if (count > 0)
			setTitle(count + " " + getString(R.string.notifications_comments));
//		else
//			setTitle(getString(R.string.notifications_comments));
	}

	@Override
	public void onBackPressed() {
		int postId = getIntent().getExtras().getInt(POST_ID);
		int whichTab = getIntent().getIntExtra(WHICH_TAB, 0);
		int commentCount = CommentFragment.getAdapter() != null ? CommentFragment
				.getAdapter().getCount() : 0;
		WallManager.getInstance().updateCount(postId, whichTab, commentCount);
		if (CommentFragment.getAdapter() != null
				&& CommentFragment.getAdapter().getCount() > 0)
			WallManager.getInstance().updateCommentCount(
					CommentFragment.getAdapter().getCount());
		if (CommentFragment.getAdapter() != null) {
			PostViewModel post = null;
			switch (getIntent().getIntExtra(WHICH_TAB, 0)) {
			case FragmentInfo.LATEST_TAB:
				post = WallManager.getInstance().getLatestPostForId(postId);
				break;
			case FragmentInfo.MY_PIC:
				post = WallManager.getInstance().getMyPicsPostForId(postId);
				break;
			case FragmentInfo.WALL:
				post = WallManager.getInstance().getWallPostForId(postId);
				break;
			case FragmentInfo.TOP_TEN:
				post = WallManager.getInstance().getTopTenPostForId(postId);
				break;
			default:
				break;
			}
			new PostViewFactory(this)
					.handelaction(new Intent()
							.putExtra(PostViewFactory.INTENT_EXTRA_POST, post)
							.putExtra(PostViewFactory.INTENT_EXTRA_ACTION,
									PostViewFactory.ACTION_COMMENT_COUNT)
							.putExtra(
									CommentActivity.CommentsCount,
									getIntent().getIntExtra(COMMENTCOUNT, 0) != 0 ? getIntent()
											.getIntExtra(COMMENTCOUNT, 0) : 0));
		}

		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				collapseMenu();
			}
		}, 4000);

		super.onBackPressed();
	}
}
