package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.customView.MaskedImageView;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.ui.fragments.RegistrationLevelOneFragment;
/**
 * @author Turki
 * 
 */
public class RegistrationActivity extends BaseActivity {
	public static final String SOCIAL_MEDIA_KEY = "Social_Media_User";
	public static Intent returnedIntent;
	public static int typeId;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_registration);
		init();
		showBackBtn();
		hideBottomBar();
		hideMenuBtn();
	}
	
	private void init(){
		RegistrationLevelOneFragment registrationFragment = new RegistrationLevelOneFragment();
		FragmentTransaction transaction                   = getSupportFragmentManager().beginTransaction();
		
		transaction.add(R.id.registeration_cotainer_id, registrationFragment , "Register");
		transaction.commit();
		
		setTitle(getResources().getString(R.string.Registration_title));
	}
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, RegistrationActivity.class);
	}
	
	/**
	 * you have to provide type_id which indicate login type (i.e
	 * NORMAL_LOGIN , LOGIN_WITH_INTENT , or LOGIN_FOR_RESULT) in case of
	 * NORMAL_LOGIN only you can pass a null intent other wise program will
	 * throw Null_Intent_Exception and will crash
	 * @param context
	 * @param type_id
	 * @param intent
	 * @return
	 */
	public static Intent getIntent(Context context , final int type_id , final Intent intent ){
		final RegistrationLevelOneFragment fragment = new RegistrationLevelOneFragment();
		App.getInstance().runOnUiThread(new Runnable() {
			
			
			@Override
			public void run() {
				fragment.populateLoginArgument(type_id, intent,null);
			}
		});
		return new Intent(context, RegistrationActivity.class);
	}

	
	/** The Same as the previous one adding new User object 
	 * that will be used to fill the data when this navigated for completing Social Media Login process
	 * @param context
	 * @param type_id
	 * @param intent
	 * @param user
	 * @return Intent
	 */
	public static Intent getIntent(Context context , final int type_id , final Intent intent, final User user ){
		final RegistrationLevelOneFragment fragment = new RegistrationLevelOneFragment();
		App.getInstance().runOnUiThread(new Runnable() {			
			@Override
			public void run() {
				fragment.populateLoginArgument(type_id, intent,user);
			}
		});
		return new Intent(context, RegistrationActivity.class);
	}

	/* (non-Javadoc)
	 * @see com.tawasol.barcelona.ui.activities.BaseActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}
	
	/* (non-Javadoc)
	 * @see com.tawasol.barcelona.ui.activities.BaseActivity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		RegistrationLevelOneFragment fragment = (RegistrationLevelOneFragment) getSupportFragmentManager().findFragmentByTag("Register");
		fragment.onActivityResult(requestCode, resultCode, data);
		System.out.println();
	}
	
	
	
}
