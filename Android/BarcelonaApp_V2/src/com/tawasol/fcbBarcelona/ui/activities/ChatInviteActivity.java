package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.data.connection.Params;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

/**
 * 
 * @author Turki
 *
 */
public class ChatInviteActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.activity_chat_invite);
		setTitle(getResources().getString(R.string.smsInvite_title));
		
		/**  Initialize InviteMessage Activity **/
		init();
	}
	
	/**  Get InviteMessageActivity Intent to start it from any activity **/
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, ChatInviteActivity.class);
	}
	
	private void init(){
		/** Hide BottomBar **/
		hideBottomBar();
		
		hideMenuBtn();
		
		/** Show RightButton, BackButton**/
//		showRightBtn();
		showBackBtn();
		
		getRightBtn().setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.share_ico,0);
	}
}
