package com.tawasol.fcbBarcelona.ui.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction; 

import com.tawasol.fcbBarcelona.R; 
import com.tawasol.fcbBarcelona.ui.fragments.FCBPhotoFragment;

public class MyPicsActivity extends BaseActivity {
	public static final int REQUEST_MY_PICS= 1234;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_my_pics);
		showBackBtn();
		hideBottomBar();
		setTitle(getResources().getString(R.string.Home_tab4));
	}
	
	
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, MyPicsActivity.class);
	}

	/* (non-Javadoc)
	 * @see com.tawasol.barcelona.ui.activities.BaseActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		setResult(Activity.RESULT_CANCELED);
		super.onBackPressed();
	}
}
