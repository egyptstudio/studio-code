package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ShopActivity extends BaseActivity {

	public static final String isShop = "isShop";

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.shop_activity);
//		showBackBtn();
		if (!getIntent().getBooleanExtra(isShop, false)) {
			hideBottomBar();
			hideMenuBtn();
		}
		setTitle(getResources().getString(R.string.Home_Shop));
	}

	public static Intent getIntent(Context context, boolean isShop) {
		return new Intent(context, ShopActivity.class).putExtra(
				ShopActivity.isShop, isShop);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
