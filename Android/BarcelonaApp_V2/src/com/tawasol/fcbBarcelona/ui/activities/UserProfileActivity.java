package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.fcbBarcelona.R;

public class UserProfileActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_user_profile);
		setTitle(getString(R.string.User_Profile));
		//ShowProfileButtonsLayout();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, UserProfileActivity.class)
		.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		refreshMenu();
	}
	
}
