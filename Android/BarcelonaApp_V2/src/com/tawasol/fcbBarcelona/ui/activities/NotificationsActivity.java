package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.ui.fragments.NotificationsFragment;

public class NotificationsActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notifications);
		setTitle(getResources().getString(R.string.notifications));

	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, NotificationsActivity.class);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		NotificationsFragment fragment = (NotificationsFragment) getSupportFragmentManager()
				.findFragmentById(R.id.notifications_fragment_id);
		if (intent.getExtras() != null) {
			int tab = intent.getIntExtra(NotificationsFragment.WHICH_TAB, 0);
			fragment.switchTabs(tab);
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(HomeActivity.HOME_WAS_PRESENTED)
			super.onBackPressed();
		else{
			startActivity(HomeActivity.getActivityIntent(this));
			this.finish();
		}
	}

}
