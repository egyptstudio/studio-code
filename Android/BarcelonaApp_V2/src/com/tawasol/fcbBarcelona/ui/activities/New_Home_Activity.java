package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;

import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TabHost.TabSpec;


public class New_Home_Activity extends BaseActivity{

	TabHost mTabHost;

	Intent intent;
	TabWidget widget;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_home_activity);

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		LocalActivityManager mLocalActivityManager = new LocalActivityManager(this, false);
	    mLocalActivityManager.dispatchCreate(savedInstanceState); // state will be bundle your activity state which you get in onCreate
	    mTabHost.setup(mLocalActivityManager);
		widget = (TabWidget) findViewById(android.R.id.tabs);
	    
//		mTabHost.getTabWidget().setDividerDrawable(null);
		Intent intent; // Reusable Intent for each tab

		intent = new Intent().setClass(New_Home_Activity.this, PlaylistActivity.class);
		setupTab(new TextView(this), "Artists Tab", intent);

		intent = new Intent().setClass(New_Home_Activity.this, PlaylistActivity.class);
		setupTab(new TextView(this), "Albums Tab", intent);

		intent = new Intent().setClass(New_Home_Activity.this, PlaylistActivity.class);
		setupTab(new TextView(this), "Songs Tab", intent);
		
		intent = new Intent().setClass(New_Home_Activity.this, PlaylistActivity.class);
		setupTab(new TextView(this), "Playlist Tab", intent);
		
		ViewTreeObserver observer = widget.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			
			@Override
			public void onGlobalLayout() {
				Display display = getWindowManager().getDefaultDisplay(); 
				int width = display.getWidth();
				if(widget.getWidth() < width){
					FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
					params.gravity = Gravity.CENTER_HORIZONTAL;

					widget.setLayoutParams(params);
				}
			}
		});
	} // end onCreate

	private void setupTab(final View view, final String tag, final Intent myIntent) {

		View tabview = createTabView(mTabHost.getContext(), tag);
		TabSpec setContent =  mTabHost.newTabSpec(tag).setIndicator(tabview).setContent(myIntent);
		mTabHost.addTab(setContent);
	}

	private static View createTabView(final Context context, final String text) {

		View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
		TextView tv = (TextView) view.findViewById(R.id.textView);
		tv.setText(text);
		return view;
	}

}

