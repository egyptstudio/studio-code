package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.managers.ContestManager;

public class ContestActivity extends BaseActivity {
	public static final int WINNER_INTENT_RESULT_CODE = 27965;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contest);
		// Get a Tracker (should auto-report)
		((App) getApplication()).getTracker(App.TrackerName.APP_TRACKER);
		setTitle(getResources().getString(R.string.menu_Contest));
		// showBackBtn();
		// hideMenuBtn();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, ContestActivity.class);
	}

	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// Stop the analytics tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		ContestManager.getInstance().resetContest();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		ContestManager.getInstance().resetContest();
	}
	
	/*
	 * @Override protected void onActivityResult(int arg0, int arg1, Intent
	 * arg2) { super.onActivityResult(arg0, arg1, arg2); if (arg1 ==
	 * Activity.RESULT_OK && arg0 == WINNER_INTENT_RESULT_CODE) {
	 * ContestWinnersFragment winnerFrag = new ContestWinnersFragment();
	 * winnerFrag.onActivityResult(arg0, arg1, arg2); } }
	 */
}
