package com.tawasol.fcbBarcelona.ui.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction; 

import com.tawasol.fcbBarcelona.R; 
import com.tawasol.fcbBarcelona.ui.fragments.FCBPhotoFragment;
/**
 * @author Turki
 * 
 */
public class FCBPhotoActivity extends BaseActivity {
	public static final int REQUEST_FCB_PHOTO = 10;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_fcb_photo);
		init();
		showBackBtn();
		hideBottomBar();
	}
	
	private void init(){
		FCBPhotoFragment fcbPhotoFragment = new FCBPhotoFragment();
		FragmentTransaction transaction   = getSupportFragmentManager().beginTransaction();
		
		transaction.add(R.id.fcb_cotainer_id, fcbPhotoFragment);
		transaction.commit();
		
		setTitle("FCB Photo");
	}
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, FCBPhotoActivity.class);
	}

	/* (non-Javadoc)
	 * @see com.tawasol.barcelona.ui.activities.BaseActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		setResult(Activity.RESULT_CANCELED);
		super.onBackPressed();
	}
}
