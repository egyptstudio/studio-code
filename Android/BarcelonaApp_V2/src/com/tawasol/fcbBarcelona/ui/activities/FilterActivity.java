/**
 * 
 */
package com.tawasol.fcbBarcelona.ui.activities;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.managers.WallManager;
import com.tawasol.fcbBarcelona.ui.fragments.FilterFragment;
import com.tawasol.fcbBarcelona.ui.fragments.WallFragment;

/**
 * @author Basyouni
 *
 */
public class FilterActivity extends BaseActivity implements OnClickListener {

	public static final String FAV_CHOOSED = "fav_choosed";

	static List<Integer> countries = new ArrayList<Integer>();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.filter_activity);
		countries.clear();
		showBackBtn();
		showRightBtn();
		hideBottomBar();
		hideMenuBtn();
		setTitle(getString(R.string.Filter1_title2));
		getRightBtn().setCompoundDrawablesWithIntrinsicBounds(0, // left
				0, // top
				R.drawable.done, // right
				0/* bottom */);

		getRightBtn().setOnClickListener(this);
	}

	public static Intent getIntent(Context context) {
		return new Intent(context, FilterActivity.class);
	}

	public void addtoCountryList(int countryId) {
		countries.add(Integer.valueOf(countryId));
	}

	public void removeFromCountryList(int countryId) {
		countries.remove(Integer.valueOf(countryId));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rightBtn:
			FilterFragment fragment = (FilterFragment) getSupportFragmentManager().findFragmentById(R.id.filter_fragment_id);
			setResult(RESULT_OK, new Intent().putIntegerArrayListExtra(WallFragment.COUNTRY_LIST, fragment.getList()));
			this.finish();
			break;

		default:
			break;
		}
	}
}
