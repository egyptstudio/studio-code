package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.managers.FanManager;

import android.os.Bundle;
import android.view.View;

public class PremiumUsersActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.premium_users_activity);
		showBackBtn();
		setTitle(getString(R.string.PremiumList_title));
		showFilterhBtn();
		getFilterBtn().setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(FanFilterActivity.getIntent(PremiumUsersActivity.this, true));	
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		FanManager.getInstance().clearPremium();
		super.onBackPressed();
	}
}
