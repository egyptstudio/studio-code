package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.ui.fragments.AddPosterFragment;
import com.tawasol.fcbBarcelona.ui.fragments.Edit_More_Profile_Fragment;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

public class AddPosterActivity extends BaseActivity {
	public static final String ORIGINAL_IMG_PATH = "OriginalImgPath";
	public static final String PRIVACY_PROGRESS_KEY = "PrivacyProgressKey";
	public static final String MASKED_IMG_PATH = "MaskedImgPath";
	private String originalImgPath;
	private int privacyProgress;
	private String maskedImage;

	@Override
	protected void onCreate(Bundle bundle) {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		super.onCreate(bundle);
		setContentView(R.layout.activity_add_poster);

		originalImgPath = getIntent().getStringExtra(
				AddPosterActivity.ORIGINAL_IMG_PATH);
		maskedImage = getIntent().getStringExtra(
				AddPosterActivity.MASKED_IMG_PATH);
		privacyProgress = getIntent().getIntExtra(
				AddPosterActivity.PRIVACY_PROGRESS_KEY, 0);

		if (originalImgPath != null)
			Utils.deleteFileNoThrow(originalImgPath);
		if(maskedImage != null)
			Utils.deleteFileNoThrow(maskedImage);
		navigateToHome();

		setTitle(getString(R.string.extra_items));
		showRightBtn();
	}

	public static Intent getActivityIntent(Context context,
			String originalImgPth, String maskedImgPath, int privacyProgress) {
		return new Intent(context, AddPosterActivity.class)
				.putExtra(ORIGINAL_IMG_PATH, originalImgPth)
				.putExtra(MASKED_IMG_PATH, maskedImgPath)
				.putExtra(PRIVACY_PROGRESS_KEY, privacyProgress);
	}

	@Override
	public void onBackPressed() {
		AddPosterFragment addPosterFrag = (AddPosterFragment) getSupportFragmentManager()
				.findFragmentByTag("add_poster_fragment");
		addPosterFrag.navigateToHome();

	}

	public void navigateToHome() {
		switch (privacyProgress) {
		case 0: // ONLY Me ==> My Pics
			UIUtils.showToast(AddPosterActivity.this,
					getResources().getString(R.string.share2_affirm1));
			startActivity(HomeActivity.getActivityIntent(
					AddPosterActivity.this, FragmentInfo.MY_PIC, true, true));
			break;
		case 1: // Friends ==> My Pics
			UIUtils.showToast(AddPosterActivity.this,
					getResources().getString(R.string.share2_affirm2));
			startActivity(HomeActivity.getActivityIntent(
					AddPosterActivity.this, FragmentInfo.MY_PIC, true, true));
			break;
		case 2: // Public ==> Latest
			UIUtils.showToast(AddPosterActivity.this,
					getResources().getString(R.string.share2_affirm3));
			startActivity(HomeActivity.getActivityIntent(
					AddPosterActivity.this, FragmentInfo.LATEST_TAB, false,
					true));
			break;

		default:
			break;
		}
	}
}
