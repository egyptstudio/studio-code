package com.tawasol.fcbBarcelona.ui.activities;

import com.tawasol.fcbBarcelona.R;

import android.os.Bundle;

public class BlockedUserListActivity extends BaseActivity{

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.blocked_list_activity);
		setTitle(getResources().getString(R.string.settings_blocked_user));
		showBackBtn();
	}
	
}
