package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.tawasol.fcbBarcelona.R;

public class HelpScreenActivity extends BaseActivity {

	
	@Override
	protected void onCreate(Bundle bundle) {
		
		super.onCreate(bundle);
		setContentView(R.layout.activity_help_screen);
		setTitle(getString(R.string.i_Help));
		showBackBtn();
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, HelpScreenActivity.class);
	}
}
