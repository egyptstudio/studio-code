package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.ui.fragments.InfoFragment;

public class WebViewActivity extends BaseActivity {
	public static final String SELECTED_ID_BUNDLE ="SelectedId";
	static String selectedID;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_webview);
		SetTitle();
		showBackBtn();
	}

	public static Intent getActivityIntent(Context context,String SelectedID) {
		selectedID = SelectedID;
		Bundle data  = new Bundle();
		data.putString(SELECTED_ID_BUNDLE, SelectedID);
		
		return new Intent(context, WebViewActivity.class)
		.putExtras(data);
	}
	
	private void SetTitle()
	{
		String[] infoElements = getResources().getStringArray(R.array.infoElements);
//		if(selectedID == InfoFragment.SELECTED_HELP)
//			setTitle(infoElements[0]);
		 if(selectedID == InfoFragment.SELECTED_AGREEMENT)
			 setTitle(infoElements[3]);
		 else if(selectedID == InfoFragment.SELECTED_STUDIO)
			 setTitle(infoElements[1]);
		 else if(selectedID == InfoFragment.SELECTED_BARCELONA)
			 setTitle(infoElements[0]);
		 else if(selectedID == InfoFragment.SELECTED_TAWASOL)
			 setTitle(infoElements[2]);
		 else if(selectedID == InfoFragment.SELECTED_FAQ)
			 setTitle(infoElements[5]);
		 else if(selectedID == InfoFragment.SELECTED_FREE_POINT)
			 setTitle(infoElements[4]);

	}


}
