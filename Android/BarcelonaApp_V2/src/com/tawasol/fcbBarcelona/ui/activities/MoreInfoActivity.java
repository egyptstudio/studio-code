package com.tawasol.fcbBarcelona.ui.activities;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.customView.MaskedImageView;
import com.tawasol.fcbBarcelona.entities.Fan;
import com.tawasol.fcbBarcelona.entities.User;

public class MoreInfoActivity extends BaseActivity {

	public static final String INFO = "info";
	private String[] educationList;
	private String[] jobList;
	private String[] jobRoleList;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_more_inf);
		showBackBtn();
		educationList = getResources().getStringArray(R.array.education_array);
		jobList = getResources().getStringArray(R.array.job_array);
		jobRoleList = getResources().getStringArray(R.array.jobRoles_array);
		setTitle(getString(R.string.user_profile_more));
		EditText info = (EditText) findViewById(R.id.info);
		info.setText("\n");
		Fan fan = (Fan) getIntent().getExtras().getSerializable(INFO);
		final ArrayList<String> income = new ArrayList<String>(
				Arrays.asList(getResources()
						.getStringArray(R.array.income_list)));

		final ArrayList<String> Relations = new ArrayList<String>(
				Arrays.asList(getResources().getStringArray(
						R.array.relationship_list)));

		for (int x = 0; x < fan.getMoreInfo().size(); x++) {
			String key = fan.getMoreInfo().get(x).getTitle();
			String value = fan.getMoreInfo().get(x).getValue();

			if (key.equalsIgnoreCase("relationship")) {
				if (value != null && !value.equalsIgnoreCase(""))
					if (key.equalsIgnoreCase("relationship")) {
						switch (Integer.parseInt(value)) {
						case User.RELATIONSHIP_MARRIED:
							value = getResources().getString(
									R.string.relationship_list_item2);
							break;
						case User.RELATIONSHIP_ENGAGED:
							value = getResources().getString(
									R.string.relationship_list_item3);
							break;
						case User.RELATIONSHIP_IN_RELATIONSHIP:
							value = getResources().getString(
									R.string.relationship_list_item4);
							break;
						case User.RELATIONSHIP_SINGLE:
							value = getResources().getString(
									R.string.relationship_list_item1);
							break;
						default:
							break;
						}
					}
			}
			if (key.equalsIgnoreCase("income")) {
				if (value != null && !value.equalsIgnoreCase(""))
					if (key.equalsIgnoreCase("income")) {
						switch (Integer.parseInt(value)) {
						case User.INCOME_LOW:
							value = getResources().getString(
									R.string.income_list_item1);
							break;
						case User.INCOME_HIGH:
							value = getResources().getString(
									R.string.income_list_item3);
							break;
						case User.INCOME_AVERAGE:
							value = getResources().getString(
									R.string.income_list_item2);
							break;
						default:
							break;
						}
					}
			}if(key.equalsIgnoreCase("education")){
				if (value != null && !value.equalsIgnoreCase(""))
					if(Integer.parseInt(value) != 0){
						value = educationList[Integer.parseInt(value)];
					}
			}
			
			if(key.equalsIgnoreCase("Profession")){
				if (value != null && !value.equalsIgnoreCase(""))
					if(Integer.parseInt(value) != 0){
						value = jobList[Integer.parseInt(value)];
					}
			}
			
			if(key.equalsIgnoreCase("job role")){
				if (value != null && !value.equalsIgnoreCase(""))
					if(Integer.parseInt(value) != 0){
						value = jobRoleList[Integer.parseInt(value)];
					}
			}
			
				if(value != null && !value.trim().equalsIgnoreCase("") && !value.equalsIgnoreCase(String.valueOf(0)))
				info.setText(info.getText().toString() + key + "\n" + value
						+ "\n\n");
		}

	}

	public static Intent getIntent(Context context, Fan fan) {
		return new Intent(context, MoreInfoActivity.class).putExtra(INFO, fan);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
