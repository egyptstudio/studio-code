package com.tawasol.fcbBarcelona.ui.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.managers.NotificationsManager;
import com.tawasol.fcbBarcelona.managers.SuperSonicManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.managers.WallManager;
import com.tawasol.fcbBarcelona.ui.dialogs.CustomAlertDialog;
import com.tawasol.fcbBarcelona.ui.fragments.WallFragment;

public class HomeActivity extends BaseActivity {
	public static String TAB_OBJ = "Tab";
	public static String COMING_FROM_SHARE = "comingFromShare";
	private static boolean callAgain;
	public static boolean HOME_WAS_PRESENTED = false;
	private static InterstitialAd mInterstitialAd;
	private String adMobID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		HOME_WAS_PRESENTED = true;
		setContentView(R.layout.activity_homel);

		adMobID = getString(R.string.banner_ad_unit_id);

		mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId(adMobID);
		
		requestNewInterstitial();

		// Get a Tracker (should auto-report)
		((App) getApplication()).getTracker(App.TrackerName.APP_TRACKER);
		// getWindow().setBackgroundDrawable(null);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		hideBottomBar();
		setTitle(Html.fromHtml(getResources().getString(R.string.home)));
		showMenuBtn();
		UserManager.getInstance().activateUser();
		if (UserManager.getInstance().getCurrentUserId() != 0)
			SuperSonicManager.getInstance().initSuperSonicAgent(this);
	}

	public static Intent getActivityIntent(Context context) {
		return new Intent(context, HomeActivity.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
	}

	public static Intent getActivityIntent(Context context, int TabIndex,
			boolean refresh, boolean openFromShare) {
		return new Intent(context, HomeActivity.class)
				.putExtra(TAB_OBJ, TabIndex)
				.putExtra(WallFragment.refresh, refresh)
				.putExtra(COMING_FROM_SHARE, openFromShare)
				.setFlags(
						Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
	}

	@Override
	public void onBackPressed() {
		if (getSlideLayout().isPanelExpanded()) {
			getSlideLayout().collapsePanel();
			return;
		}
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					HomeActivity.super.onBackPressed();
					WallManager.getInstance().clearAllData();
					UserManager.getInstance().deActivateUser();
					HOME_WAS_PRESENTED = false;
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					dialog.dismiss();
					break;
				}
			}
		};
		Dialog dialog = new CustomAlertDialog.customBuilder(HomeActivity.this)
				.setTitle(R.string.exit).setMessage(R.string.exit_msg)
				.setPositiveButton(android.R.string.yes, listener)
				.setNegativeButton(android.R.string.no, listener).create();
		dialog.show();

	}

	@Override
	protected void onResume() {
		refreshMenu();
		super.onResume();
	}

	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// Stop the analytics tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// super.onActivityResult(requestCode, resultCode, data);
		// if(requestCode == 3){
		// WallFragment_new fragment1 = (WallFragment_new)
		// getSupportFragmentManager().findFragmentByTag("Latest");
		if (resultCode == Activity.RESULT_OK) {
			WallFragment fragment = new WallFragment();
			fragment.onActivityResult(requestCode, resultCode, data);
			// refreshMenu();
		}
		// }

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		WallManager.getInstance().clearAllData();
	}

	public static InterstitialAd getInterstitialAd(){
		return mInterstitialAd;
	}
	
	public static void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                  .build();
        mInterstitialAd.loadAd(adRequest);
    }
}
