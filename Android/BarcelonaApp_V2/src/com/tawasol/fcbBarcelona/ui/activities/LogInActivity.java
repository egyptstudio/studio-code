package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.WallTable;
import com.tawasol.fcbBarcelona.ui.fragments.LogInFragment;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class LogInActivity extends BaseActivity {

	public static final String EMAIL_KEY ="Email";
	public static final String LOGIN_TYPE_KEY ="Login_Type";
	public static final String INTENT_NAVIGATION_KEY ="Intent_Navigation";
	private static LogInFragment loginfrag;

	@Override
	protected void onCreate(Bundle bundle) {

		super.onCreate(bundle);
		setContentView(R.layout.activity_login);

		// check the coming parameters
		Bundle SentData = getIntent().getExtras();
		loginfrag = new LogInFragment();
		Bundle args = new Bundle();

		if (SentData != null) {
			args.putString("Email", SentData.getString("Email"));
			loginfrag.setArguments(args);
		}
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.login_container_id, loginfrag);
		transaction.commit();

		setTitle(getResources().getString(R.string.HomeInitialState_WallLogin));
		 showBackBtn();
		 hideBottomBar();
		 hideMenuBtn();
	}

	

	public static Intent getActivityIntent(Context context,int LoginType , Intent navigationIntent) {
		Log.i("Inside Login activity Count",String.valueOf(WallTable.getInstance().getCount()));
		Bundle data  = new Bundle();
		data.putInt(LOGIN_TYPE_KEY, LoginType);
		data.putParcelable(INTENT_NAVIGATION_KEY, navigationIntent);
		
		return new Intent(context, LogInActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
		.putExtras(data);
				
	}
	public static Intent getActivityIntent(Context context, String Email,int LoginType , Intent navigationIntent) {
		Log.i("Inside Login activity Count",String.valueOf(WallTable.getInstance().getCount()));
		Bundle data  = new Bundle();
		data.putString(EMAIL_KEY, Email);
		data.putInt(LOGIN_TYPE_KEY, LoginType);
		data.putParcelable(INTENT_NAVIGATION_KEY, navigationIntent);
		
		return new Intent(context, LogInActivity.class)
				.putExtras(data);
		
		
	}

	@Override
	public void onActivityResult(int arg0, int arg1, Intent arg2) {
		super.onActivityResult(arg0, arg1, arg2);
		loginfrag.onActivityResult(arg0, arg1, arg2);
	}

	@Override
	public void onBackPressed() {
		// set the result to be Canceled
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}
	
}
