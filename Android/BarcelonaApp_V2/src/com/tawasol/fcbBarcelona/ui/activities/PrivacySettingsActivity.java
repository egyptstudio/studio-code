package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle; 
import com.tawasol.fcbBarcelona.R; 
/**
 * @author Turki
 * 
 */
public class PrivacySettingsActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_privacy_settings);
		setTitle(getResources().getString(R.string.Settings_Privacy));
		showBackBtn(); 
		hideMenuBtn();
	}
	
	public static Intent getActivityIntent(Context context) {
		return new Intent(context, PrivacySettingsActivity.class);
	}
}
