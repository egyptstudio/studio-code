package com.tawasol.fcbBarcelona.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.managers.ShopManager;
import com.tawasol.fcbBarcelona.ui.fragments.ShopFragmentTabs;

public class ShopActivityTabs extends BaseActivity {

	public static final int POINTS_REQUEST_CODE = 485625;
	public static final int PREMIUM_REQUEST_CODE = 485626;

	public static final String WHICH_TAB = "which_tab";
	public static final int NIKE_STORE_TAB = 1;
	public static final int PREMIUM_TAB = 2;
	public static final int POINTS_PACKAGES = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shop_activity_tabs);
		ShopManager.getInstance().LoadInAppProductIDs();
		// hideBottomBar();
		// Get a Tracker (should auto-report)
		((App) getApplication()).getTracker(App.TrackerName.APP_TRACKER);
		setTitle(getResources().getString(R.string.Home_Shop));
		showMenuBtn();
	}

	public static Intent getIntent(Context context, int shopTab) {
		return new Intent(context, ShopActivityTabs.class).putExtra(WHICH_TAB,
				shopTab);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {

			ShopFragmentTabs fragment = (ShopFragmentTabs) getSupportFragmentManager()
					.findFragmentById(R.id.shop_fragment_id);
			fragment.onActivityResult(requestCode, resultCode, data);
			// refreshMenu();
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// Stop the analytics tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}


}
