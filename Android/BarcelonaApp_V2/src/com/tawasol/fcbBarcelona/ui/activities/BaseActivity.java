package com.tawasol.fcbBarcelona.ui.activities;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.etiennelawlor.quickreturn.library.listeners.QuickReturnListViewOnScrollListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.application.ForegroundStatus;
import com.tawasol.fcbBarcelona.customView.ViewServer;
import com.tawasol.fcbBarcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.fcbBarcelona.data.cache.NotificationsTable;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.data.cache.WallTable;
import com.tawasol.fcbBarcelona.data.helper.VersionController;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.fragments.MenuAnonymousFragment;
import com.tawasol.fcbBarcelona.ui.fragments.MenuFragment;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.Utils;

public class BaseActivity extends FragmentActivity {

	public static final String PopUPInitialState = "popUpInitialState";

	private LinearLayout studioPopUp;
	private LinearLayout hideMenu;
	private RelativeLayout bottomBar, titleBarView;

	private TextView title, anonymousTitle;
	private ImageButton backBtn;
	private ImageButton shareBtn;
	private ImageButton refreshBtn;
	private ImageButton filterBtn;
	private ImageView menuBtnRight, menuBtnLeft;
	private LinearLayout titleBarLayout;
	private static boolean isWall = false;
	private TextView rightBtn;
	// private LinearLayout profilBtnsLayout;
	// private ImageButton helpBtn, settingBtn, notificationBtnId;
	private FrameLayout contentLayout;
	private RelativeLayout chatRightBtnLayout, deleteRightBtnLayout;
	private ImageButton chatInviteFriends;
	public ImageButton chatSearchBtn;
	public SearchView chatSearchFriends;
	LinearLayout actionBarInMenu;
	TextView skip;
	private View profileMenuView, anonymousMenuView;
	float startY = 0;
	public static int bottomBarHeight;
	private static boolean slideMenuIsShowing;;
	private RelativeLayout searchLayout;
	public ImageButton seacrhClearBtn, searcListBackBtn;
	public EditText searchET;

	private LinearLayout menuLayout;
	private SlidingUpPanelLayout slideLayout;

	public SlidingUpPanelLayout getSlideLayout() {
		return slideLayout;
	}

	public boolean slideMenuIsShowing() {
		return slideMenuIsShowing;
	}

	public int getBottomBarHeight() {
		return bottomBarHeight;
	}

	public void setIsWall() {
		isWall = true;
	}

	public void setBottomBarHeight(int bottomBarHeight) {
		this.bottomBarHeight = bottomBarHeight;
	}

	String language;

	private String adMobID;

//	private InterstitialAd mInterstitialAd;

	public static int notificationsCount;

	
	private void requestNewInterstitial() {
//		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
//				"1689F622DD32B37E07CDC03FCCA102F7")
//				.addTestDevice("C3BA92C1BABFA6D9A3103F3AEF19BD01").build();
//		
////		C3BA92C1BABFA6D9A3103F3AEF19BD01
//
//		mInterstitialAd.loadAd(adRequest);
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		
//		adMobID = getString(R.string.banner_ad_unit_id);
//
//		mInterstitialAd = new InterstitialAd(this);
//		mInterstitialAd.setAdUnitId(adMobID);
//
//		requestNewInterstitial();
//
//		mInterstitialAd.setAdListener(new AdListener() {
//			@Override
//			public void onAdClosed() {
//				requestNewInterstitial();
//				navigateToStudio();
//			}
//
//			@Override
//			public void onAdFailedToLoad(int errorCode) {
//				super.onAdFailedToLoad(errorCode);
//				System.out.println("Error " + errorCode);
//			}
//
//			@Override
//			public void onAdLoaded() {
//				// TODO Auto-generated method stub
//				super.onAdLoaded();
//			}
//		});
		
		language = SharedPrefrencesDataLayer.getStringPreferences(this,
				LanguageUtils.APP_LANGUAGE_CODE_KEY, "en");

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		ViewServer.get(this).addWindow(this);

		super.setContentView(R.layout.base_activity);
		isWall = false;
		searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
		seacrhClearBtn = (ImageButton) findViewById(R.id.search_clear_Btn);
		searchET = (EditText) findViewById(R.id.search_text);
		searcListBackBtn = (ImageButton) findViewById(R.id.search_back_Btn);

		chatRightBtnLayout = (RelativeLayout) findViewById(R.id.chatRightBtnLayout);
		deleteRightBtnLayout = (RelativeLayout) findViewById(R.id.deleteRightBtnLayout);
		chatInviteFriends = (ImageButton) findViewById(R.id.chat_invite_friends);
		chatSearchFriends = (SearchView) findViewById(R.id.chat_search_friends);
		chatSearchBtn = (ImageButton) findViewById(R.id.chat_search);
		int searchIconId = chatSearchFriends.getContext().getResources()
				.getIdentifier("android:id/search_button", null, null);
		ImageView searchIcon = (ImageView) chatSearchFriends
				.findViewById(searchIconId);
		searchIcon.setImageDrawable(null);
		int searchTextId = chatSearchFriends.getContext().getResources()
				.getIdentifier("android:id/search_src_text", null, null);
		TextView textView = (TextView) chatSearchFriends
				.findViewById(searchTextId);
		textView.setTextColor(Color.WHITE);

		// get title bat layout
		titleBarLayout = (LinearLayout) findViewById(R.id.centered_Layout);

		profileMenuView = (View) findViewById(R.id.menu_layoutView);
		anonymousMenuView = (View) findViewById(R.id.anonymous_menu_layoutView);
		setMenuLayout(); // to show the layout based on user accessiblity

		slideLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
		studioPopUp = (LinearLayout) findViewById(R.id.studioPopUp);
		studioPopUp.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				hideStudioPopUp();
			}
		});
		setupBottomBar();
		title = (TextView) findViewById(R.id.title);
		menuLayout = (LinearLayout) findViewById(R.id.dragView);

		hideMenu = (LinearLayout) findViewById(R.id.menuHide);

		backBtn = (ImageButton) findViewById(R.id.backBtn);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				doBack();
			}
		});
		skip = (TextView) findViewById(R.id.skip);
		shareBtn = (ImageButton) findViewById(R.id.shareBtn);
		refreshBtn = (ImageButton) findViewById(R.id.refreshBtn);
		filterBtn = (ImageButton) findViewById(R.id.filterBtn);
		rightBtn = (TextView) findViewById(R.id.rightBtn);
		menuBtnRight = (ImageView) findViewById(R.id.menu_btn_id);
		menuBtnLeft = (ImageView) findViewById(R.id.menu2_btn_id);
		contentLayout = (FrameLayout) findViewById(R.id.base_activity_content);
		bottomBar = (RelativeLayout) findViewById(R.id.bottom_bar);
		titleBarView = (RelativeLayout) findViewById(R.id.titleBar_relativeLayout);

		/** set the panel listener of the dragging layout */

		slideLayout.setPanelSlideListener(new PanelSlideListener() {

			@SuppressLint("NewApi")
			@Override
			public void onPanelSlide(View panel, float slideOffset) {
				// fade the sliding view out
				final QuickReturnListViewOnScrollListener listenr = new QuickReturnListViewOnScrollListener();
				if (slideOffset <= 0.99f) {
//					menuLayout.setAlpha(slideOffset * 2);
					if (listenr.getFooterState()) {
						listenr.hideFooter();
						slideMenuIsShowing = false;
					}
				} else {
					if(UserManager.getInstance().getCurrentUserId() != 0)
						refreshNotifications();
//					menuLayout.setAlpha(1);
					if (listenr.getFooterState()) {
						listenr.showFooter();
						slideMenuIsShowing = true;
					}
					// setMenuTitle();
				}

			}

			@Override
			public void onPanelHidden(View panel) {
			}

			@Override
			public void onPanelExpanded(View panel) {
				// UIUtils.showToast(BaseActivity.this, "Expanded");
				// showBar();
				// change the drawable
				// getMenuBtn().setImageResource(R.drawable.menu_up_new);
				showBottomBar();
				// msh moqtne3a beha tmaman :S
				// titleBarView.setBackgroundResource(R.color.titleBar);
				// (findViewById(R.id.title_bar)).setBackgroundResource(R.color.titleBar);
			}

			@Override
			public void onPanelCollapsed(View panel) {
				// UIUtils.showToast(BaseActivity.this, "Collapsed");
				DisableSliding();
				if (isWall)
					hideBottomBar();
				// change image drawable and hide the dragging layout
				// getMenuBtn().setImageResource(R.drawable.menu_down_new);
				// menuLayout.setVisibility(View.GONE);
				// titleBarView.setBackgroundResource(R.drawable.navigationbar);
				// (findViewById(R.id.title_bar)).setBackgroundColor(Color.TRANSPARENT);

			}

			@Override
			public void onPanelAnchored(View panel) {
			}
		});

		/**
		 * get the height of the title bar to set the "Menu Button" in the
		 * middle of it
		 */
		final View titleView = findViewById(R.id.title_bar);
		ViewTreeObserver observer = titleView.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressLint("NewApi")
			@Override
			public void onGlobalLayout() {
				// menuBtnRight.setPadding(0, titleView.getHeight() / 2, 0, 0);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					titleView.getViewTreeObserver()
							.removeOnGlobalLayoutListener(this);
				} else {
					titleView.getViewTreeObserver()
							.removeGlobalOnLayoutListener(this);
				}
				titleView.setClickable(true);
				titleView.setEnabled(true);
			}
		});

		// initialize the 3 buttons of Profile screen
		// " Help , Setting , Notification "
		// profilBtnsLayout = (LinearLayout)
		// findViewById(R.id.profilBtnsLayout);
		// helpBtn = (ImageButton) findViewById(R.id.helpBtn);
		// settingBtn = (ImageButton) findViewById(R.id.settingBtn);
		// notificationBtnId = (ImageButton)
		// findViewById(R.id.notificationBtnId);

		DisableSliding();
		/*
		 * getMenuBtn().setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { menuBtnClick();
		 * 
		 * } });
		 */

		/* getMenuBtn() */titleBarLayout
				.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:
							startY = event.getY();
							break;
						case MotionEvent.ACTION_UP:
							if (event.getY() >= startY)
								menuBtnClick();
							break;
						}
						return true;
					}
				});

		// Check for forced update
		checkForcedUpdate();

	}

	protected void navigateToStudio() {
		// TODO Auto-generated method stub
		if (studioPopUp.getVisibility() == View.VISIBLE) {
			hideStudioPopUp();
		}
		startActivity(StudioFoldersActivity
				.getActivityIntent(BaseActivity.this , StudioFoldersActivity.PLAYER_FOLDER));
	}

	protected void refreshNotifications() {
		TextView notifcationNumberTxt = (TextView) 
				findViewById(R.id.notification_num_txt_id);
		if (notificationsCount != 0 && notificationsCount != -1) {
			notifcationNumberTxt.setText(String.valueOf(notificationsCount));
			notifcationNumberTxt.setVisibility(View.VISIBLE);
		} else
			notifcationNumberTxt.setVisibility(View.GONE);
	}

	private void checkForcedUpdate() {
		if (VersionController.isUpdateRequired()) {
			UIUtils.showUpdatePopup(this, new Runnable() {
				@Override
				public void run() {
					Utils.updateAppAction(BaseActivity.this);
				}
			});
		}
	}

	@Override
	public void setContentView(int layoutResID) {
		View content = LayoutInflater.from(this).inflate(layoutResID,
				contentLayout, false);
		contentLayout.addView(content);
	}

	/**
	 * method that checks whether the user is logged in or not to view the
	 * suitable layout
	 */
	public void setMenuLayout() {
		if (UserManager.getInstance().getCurrentUserId() != 0)
			profileMenuView.setVisibility(View.VISIBLE);
		else
			anonymousMenuView.setVisibility(View.VISIBLE);
	}

	@Override
	public void setContentView(View view) {
		contentLayout.addView(view);
	}

	public void menuBtnClick() {
		/*
		 * if (slideLayout.isPanelExpanded()) { slideLayout.collapsePanel();
		 * slideLayout.setPanelHeight(0);
		 * //getMenuBtn().setImageResource(R.drawable.menu_down_new);
		 * DisableSliding(); } else
		 */{
			getSlideLayout().setVisibility(View.VISIBLE);
			menuLayout.setVisibility(View.VISIBLE);
			slideLayout.expandPanel();
			// getMenuBtn().setImageResource(R.drawable.menu_up_new);
		}
		startY = 0;
	}

	public void homeInitialMenuState() {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				slideLayout.expandPanel();
			}
		});
		Timer mTimer = new Timer();
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						slideLayout.collapsePanel();

					}
				});
			}
		}, 2000);
	}

	public void showBackBtn() {
		backBtn.setVisibility(View.VISIBLE);
	}

	public void hideBackBtn() {
		backBtn.setVisibility(View.INVISIBLE);
	}

	public void showChatInviteFriends() {
		chatInviteFriends.setVisibility(View.VISIBLE);
	}

	public void hideChatInviteFriends() {
		chatInviteFriends.setVisibility(View.INVISIBLE);
	}

	public void showShareBtn() {
		shareBtn.setVisibility(View.VISIBLE);
	}

	public void showRefreshBtn() {
		refreshBtn.setVisibility(View.VISIBLE);
	}

	public void showFilterhBtn() {
		filterBtn.setVisibility(View.VISIBLE);
	}

	public void showRightBtn() {
		rightBtn.setVisibility(View.VISIBLE);
	}

	public void hideRightBtn() {
		rightBtn.setVisibility(View.GONE);
	}

	public void showChatSearchBtn() {
		chatSearchBtn.setVisibility(View.VISIBLE);
	}

	public void hideChatSearchBtn() {
		chatSearchBtn.setVisibility(View.GONE);
	}

	public void showMenuBtn() {
		menuBtnRight.setVisibility(View.VISIBLE);
		menuBtnLeft.setVisibility(View.VISIBLE);
	}

	public void hideMenuBtn() {
		menuBtnRight.setVisibility(View.GONE);
		menuBtnLeft.setVisibility(View.GONE);
		DisableSliding();
		titleBarLayout.setClickable(false);
		titleBarLayout.setEnabled(false);
	}

	public void hideBottomBar() {
		bottomBar.setVisibility(View.GONE);
	}

	public void showBottomBar() {
		bottomBar.setVisibility(View.VISIBLE);
	}

	public void setTitle(String strTitle) {
		title.setText(strTitle);
	}

	public void setTitle(Spanned strTitle) {
		title.setText(strTitle);
	}

	public RelativeLayout getDeleteRightBtnLayoutt() {
		return deleteRightBtnLayout;
	}

	/**
	 * @return the chatRightBtnLayout
	 */
	public RelativeLayout getChatRightBtnLayout() {
		return chatRightBtnLayout;
	}

	/**
	 * @return the chatInviteFriends
	 */
	public ImageButton getChatInviteFriends() {
		return chatInviteFriends;
	}

	/**
	 * @return the chatSearchFriends
	 */
	public SearchView getChatSearchFriends() {
		return chatSearchFriends;
	}

	private void doBack() {
		this.onBackPressed();
	}

	public ImageButton getShareBtn() {
		return shareBtn;
	}

	public ImageButton getRefreshBtn() {
		return refreshBtn;
	}

	public ImageButton getSearchBtn() {
		return chatSearchBtn;
	}

	public ImageButton getFilterBtn() {
		return filterBtn;
	}

	public EditText getSearchET() {
		return searchET;
	}

	public TextView getRightBtn() {
		return rightBtn;
	}

	public ImageView getMenuBtn() {
		return menuBtnRight;
	}

	public ImageView getLeftMenuBtn() {
		return menuBtnLeft;
	}

	public ImageButton getSeacrhClearBtn() {
		return seacrhClearBtn;
	}

	public ImageView getBackButton() {
		return backBtn;
	}

	public ImageButton getSearcListBackBtn() {
		return searcListBackBtn;
	}

	public TextView getSkipButton() {
		return skip;
	}

	public void showSkipButton() {
		skip.setVisibility(View.VISIBLE);
	}

	public void hideSkipButton() {
		skip.setVisibility(View.INVISIBLE);
	}

	public void showDeleteButton() {
		deleteRightBtnLayout.setVisibility(View.VISIBLE);
	}

	public void hideDeleteButton() {
		deleteRightBtnLayout.setVisibility(View.INVISIBLE);
	}

	public void showTitleBar() {
		titleBarLayout.setVisibility(View.VISIBLE);
	}

	public void hideTitleBar() {
		((LinearLayout) findViewById(R.id.centered_Layout))
				.setVisibility(View.INVISIBLE);
	}

	private void setupBottomBar() {
		Button captureBtn = (Button) findViewById(R.id.capture_btn);
		captureBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
//				if (mInterstitialAd.isLoaded()) {
//					mInterstitialAd.show();
//				} else {
					navigateToStudio();
//				}
				// TODO navigate to studio
			}
		});

		TextView shopBtn = (TextView) findViewById(R.id.shop_btn);
		shopBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(ShopActivityTabs.getIntent(BaseActivity.this , ShopActivityTabs.NIKE_STORE_TAB).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			}
		});

		TextView chatBtn = (TextView) findViewById(R.id.chat_btn);
		chatBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// UIUtils.showToast(BaseActivity.this,
				// getResources().getString(R.string.demo_version));

				/** Added by turki **/
				if (UserManager.getInstance().getCurrentUser() != null
						&& UserManager.getInstance().getCurrentUserId() > 0) {
					startActivity(ChatActivity
							.getActivityIntent(BaseActivity.this));
				} else {
					startActivity(LogInActivity.getActivityIntent(
							BaseActivity.this, UserManager.LOGIN_WITH_INTENT,
							ChatActivity.getActivityIntent(BaseActivity.this)));
				}
			}
		});

	}

	public void DisableSliding() {
		// to disable menu sliding
		menuLayout.setVisibility(View.GONE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshMenu();
		ViewServer.get(this).setFocusedWindow(this);

		String oldLanguage = language;
		language = SharedPrefrencesDataLayer.getStringPreferences(this,
				LanguageUtils.APP_LANGUAGE_CODE_KEY, "en");
		if (!oldLanguage.equals(language)) {
			finish();
			startActivity(getIntent());
		}
		
		// Language init 
        String language = SharedPrefrencesDataLayer.getStringPreferences(BaseActivity.this, 
        		LanguageUtils.APP_LANGUAGE_CODE_KEY, LanguageUtils.LANGUAGE_ENGLISH);
        LanguageUtils.changeApplicationLanguage(language , BaseActivity.this , false); 
        if (App.appEnterBackGround) {
			App.appEnterBackGround = false;
			UserManager.getInstance().activateUser();
		}
        
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ViewServer.get(this).removeWindow(this);
		Log.i("Inside On Destroy Count",
				String.valueOf(WallTable.getInstance().getCount()));
	}

	public void refreshMenu() {
		System.out.println("refreshed");
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			MenuFragment menu = new MenuFragment();
			FragmentTransaction tr = getSupportFragmentManager()
					.beginTransaction();
			tr.replace(R.id.menuHide, menu);
			tr.commitAllowingStateLoss();
			setMenuLayout();
		} else {
			MenuAnonymousFragment menu = new MenuAnonymousFragment();
			FragmentTransaction tr = getSupportFragmentManager()
					.beginTransaction();
			tr.replace(R.id.menuHide, menu);
			tr.commitAllowingStateLoss();
			setMenuLayout();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		ForegroundStatus.get().onActivityPaused(this);
		if (slideLayout.isPanelExpanded())
			slideLayout.collapsePanel();
	}

	// handle back if menu is shown
	@Override
	public void onBackPressed() {
		if (slideLayout.isPanelExpanded())
			slideLayout.collapsePanel();
		else
			super.onBackPressed();
	}

	public RelativeLayout getBottomBar() {
		return bottomBar;
	}

	public void showBar() {
		bottomBar.setVisibility(View.VISIBLE);
	}

	public void showChatSearchAndInviteLayout() {
		chatRightBtnLayout.setVisibility(View.VISIBLE);
	}

	public void hideChatSearchAndInviteLayout() {
		chatRightBtnLayout.setVisibility(View.GONE);
	}

	public void showSearchLayout() {
		searchLayout.setVisibility(View.VISIBLE);
	}

	public void hideSearchLayout() {
		searchLayout.setVisibility(View.GONE);
	}

	public void showStudioPopUp() {
		if (!checkIfInitialState())
			studioPopUp.setVisibility(View.VISIBLE);
	}

	public void hideStudioPopUp() {
		studioPopUp.setVisibility(View.GONE);
		saveInitialPopUpStateEnded();
	}

	// public void ShowProfileButtonsLayout() {
	// profilBtnsLayout.setVisibility(View.VISIBLE);
	// }
	//
	// public void HideProfileButtonsLayout() {
	// profilBtnsLayout.setVisibility(View.GONE);
	// }

	private void setMenuTitle() {
		if (UserManager.getInstance().getCurrentUserId() != 0)
			((TextView) findViewById(R.id.menu_title_txt)).setText(title
					.getText().toString());

		else
			((TextView) findViewById(R.id.anonymous_menu_title_txt))
					.setText(title.getText().toString());

	}

	public boolean checkIfInitialState() {
		return new File(BaseActivity.this.getFilesDir() + "/"
				+ PopUPInitialState).exists();
	}

	public void saveInitialPopUpStateEnded() {
		App.getInstance().runInBackground(new Runnable() {
			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(BaseActivity.this,
							PopUPInitialState, "");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void collapseMenu() {
		slideLayout.collapsePanel();
	}
}
