package com.tawasol.fcbBarcelona.ui.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.dialogs.CustomAlertDialog;
import com.tawasol.fcbBarcelona.ui.fragments.Edit_UserProfile_Fragment;

public class Edit_UserProfile_Activity extends BaseActivity implements
		OnClickListener {
	public static final String MY_PICS_BUNLE_OBJ = "MyPicture";
	public static final String fromShare = "formShare";
	public static boolean finish = false;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		finish = false;
		setContentView(R.layout.activity_edit_user_profile);
		setTitle(getString(R.string.EditProfile_title));
		showBackBtn();
		showRightBtn();
		hideBottomBar();
		hideMenuBtn();
		getBackButton().setOnClickListener(this);
	}

	public static Intent getActivityIntent(Context context , boolean fromShare) {
		return new Intent(context, Edit_UserProfile_Activity.class).putExtra(Edit_UserProfile_Activity.fromShare, fromShare);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Edit_UserProfile_Fragment profileFragment = (Edit_UserProfile_Fragment) getSupportFragmentManager()
				.findFragmentByTag("edit_profile_frag");
		profileFragment.onActivityResult(requestCode, resultCode, data);
	}

	private void backPressed() {
		Edit_UserProfile_Fragment profileFragment = (Edit_UserProfile_Fragment) getSupportFragmentManager()
				.findFragmentByTag("edit_profile_frag");
		User profileUser = profileFragment.buildUser();
		User user = UserManager.getInstance().getCurrentUser();
		// if(! new User(profileUser.getProfilePic()
		// ,profileUser.getFullName(),profileUser.getEmail(),profileUser.getDateOfBirth(),profileUser.getCountryId(),profileUser.getCityId(),profileUser.getDistrict(),profileUser.getPhone(),profileUser.getGender()).equals(new
		// User(user.getProfilePic()
		// ,user.getFullName(),user.getEmail(),user.getDateOfBirth(),user.getCountryId(),user.getCityId(),user.getDistrict(),user.getPhone(),user.getGender())))
		// if(profileUser.getProfilePic().equals(user.getProfilePic()) &&
		// profileUser.getFullName().equals(user.getFullName()) &&
		// profileUser.getEmail().equals(user.getEmail()) &&
		// profileUser.getDateOfBirth()== user.getDateOfBirth() &&
		// profileUser.getCountryId() == user.getCountryId() &&
		// profileUser.getCityId() == user.getCityId() &&
		// profileUser.getDistrict().equals(user.getDistrict()) &&
		// profileUser.getPhone().equals(user.getPhone()) &&
		// profileUser.getGender()==user.getGender())
		// {
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					Edit_UserProfile_Activity.super.onBackPressed();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					dialog.dismiss();
					break;
				}
			}
		};
		Dialog dialog = new CustomAlertDialog.customBuilder(this)
				.setTitle(R.string.exit).setMessage(R.string.save_changes_msg)
				.setPositiveButton(android.R.string.ok, listener)
				.setNegativeButton(android.R.string.cancel, listener).create();
		dialog.show();
		// }
		// else
		// Edit_UserProfile_Activity.super.onBackPressed();
	}

	@Override
	public void onBackPressed() {
		backPressed();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backBtn:
			backPressed();
			break;

		default:
			break;
		}
	}

}
