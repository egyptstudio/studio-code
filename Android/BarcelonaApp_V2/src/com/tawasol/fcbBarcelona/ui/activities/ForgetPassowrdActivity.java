package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.ui.fragments.ForgetPasswordFragment;

public class ForgetPassowrdActivity extends BaseActivity {
	private FragmentTransaction transaction;
	

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_forgetpassword);

		ForgetPasswordFragment forgetPassFrag = new ForgetPasswordFragment();
		transaction = getSupportFragmentManager().beginTransaction();
		transaction.add(R.id.forgetpass_container_id, forgetPassFrag);
		transaction.commit();
		setTitle(getResources().getString(R.string.Login_forgotPassword));
		showBackBtn();
		hideMenuBtn();
		hideBottomBar();
	}

	public static Intent getActivityIntent(Context context,int LoginType , Intent navigationIntent,String Email) {
		Bundle data  = new Bundle();
		data.putInt(LogInActivity.LOGIN_TYPE_KEY, LoginType);
		data.putParcelable(LogInActivity.INTENT_NAVIGATION_KEY, navigationIntent);
		data.putString(LogInActivity.EMAIL_KEY, Email);
		return new Intent(context, ForgetPassowrdActivity.class)
		.putExtras(data);
	}
	
	@Override
	public void onBackPressed() {
		// set the result to be Canceled
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}

}
