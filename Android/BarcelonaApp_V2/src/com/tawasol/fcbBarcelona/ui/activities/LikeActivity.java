/**
 * 
 */
package com.tawasol.fcbBarcelona.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.tawasol.fcbBarcelona.R;

/**
 * @author Basyouni
 *
 */
public class LikeActivity extends BaseActivity {

	
	public static String POST_ID = "postId";
	public static String LIKECOUNT = "likeCount";

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.like_activity);
		hideBottomBar();
		showBackBtn();
		hideMenuBtn();
		setTitle(String.valueOf(getIntent().getIntExtra(LIKECOUNT, 0)) + " Likes");
	}
	
	public static Intent getIntent(Context context, int postId , int likeCount){
		Bundle bundle = new Bundle();
		bundle.putInt(POST_ID, postId);
		bundle.putInt(LIKECOUNT, likeCount);
		return new Intent(context, LikeActivity.class).putExtras(bundle);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
		  @Override
		  public void run() {
		    collapseMenu();
		  }
		}, 4000);
	}
}
