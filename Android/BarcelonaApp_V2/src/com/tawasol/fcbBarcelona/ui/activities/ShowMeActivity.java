package com.tawasol.fcbBarcelona.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.tawasol.fcbBarcelona.R;

public class ShowMeActivity extends Activity {

	public static final int POST_TYPE = 0;
	public static final int SHARE_TYPE = 1;
	public static final String SHOW_ME_TYPE = "typeshowme";

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.show_me_layout);
		ImageView showMeImage = (ImageView) findViewById(R.id.show_me_image);
		ImageButton close = (ImageButton) findViewById(R.id.close);

		close.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ShowMeActivity.this.finish();
			}
		});

		switch (getIntent().getIntExtra(SHOW_ME_TYPE, POST_TYPE)) {
		case POST_TYPE:
			showMeImage.setImageDrawable(getResources().getDrawable(R.drawable.post_walk_through));
			break;
		case SHARE_TYPE:
			showMeImage.setImageDrawable(getResources().getDrawable(R.drawable.share_walk_through));
			break;
		default:
			break;
		}
	}

	public static Intent getActivityIntent(Context context, int type) {
		return new Intent(context, ShowMeActivity.class).putExtra(SHOW_ME_TYPE,
				type);
	}

}
