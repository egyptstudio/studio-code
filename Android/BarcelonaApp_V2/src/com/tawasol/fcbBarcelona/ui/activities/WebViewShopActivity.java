package com.tawasol.fcbBarcelona.ui.activities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnNikeStoreLinkRecieved;
import com.tawasol.fcbBarcelona.managers.ShopManager;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;

@SuppressLint("SetJavaScriptEnabled")
public class WebViewShopActivity extends BaseActivity implements
		OnNikeStoreLinkRecieved {

	public static final String IS_SURVY = "isSurvy";
	private ProgressDialog dialog;
	WebView nikeStore;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.nike_store);

		nikeStore = (WebView) findViewById(R.id.webView1);
		nikeStore.setBackgroundColor(getResources().getColor(android.R.color.transparent));
		dialog = new ProgressDialog(this, R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		dialog.setCancelable(false);
		showBackBtn();
		/** Added by turki **/
		nikeStore.getSettings().setJavaScriptEnabled(true);
		nikeStore.getSettings().setBuiltInZoomControls(false);
		RelativeLayout.LayoutParams params = (LayoutParams) nikeStore.getLayoutParams();
		params.setMargins(0, 0, 0, BaseActivity.bottomBarHeight);
		nikeStore.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				return false;
			}
		});

		TextView offline = (TextView) findViewById(R.id.textView1);

		if (NetworkingUtils.isNetworkConnected(this)) {
			dialog.show();
			if (getIntent().getBooleanExtra(IS_SURVY, false)){
				ShopManager.getInstance().GetSurvey();
				setTitle(getString(R.string.Take_Survey));
			}
			else{
				ShopManager.getInstance().rewardingPoints();
				setTitle(getString(R.string.Free_Points));
			}
		} else {
			offline.setVisibility(View.VISIBLE);
			nikeStore.setVisibility(View.GONE);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		ShopManager.getInstance().addListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		ShopManager.getInstance().removeListener(this);
	}

	@Override
	public void onException(AppException ex) {
		dialog.dismiss();
		if (ex.getErrorCode() != AppException.NO_DATA_EXCEPTION)
			UIUtils.showToast(this, ex.getMessage());
	}

	@Override
	public void onLinkRecieved(String link) {
		dialog.dismiss();
		JSONArray array = null;
		try {
			array = new JSONArray(link);
			nikeStore.loadUrl(array.getJSONObject(0).getString("link"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
