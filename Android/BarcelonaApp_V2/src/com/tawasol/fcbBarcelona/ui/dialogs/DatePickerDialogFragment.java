package com.tawasol.fcbBarcelona.ui.dialogs;

import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;


public class DatePickerDialogFragment extends android.support.v4.app.DialogFragment {

	private Date date;
	private OnDateSetListener onDateSetListener;

	public DatePickerDialogFragment(Date date, OnDateSetListener onDateSetListener) {
		this.date = date;
		this.onDateSetListener = onDateSetListener;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int style = DialogFragment.STYLE_NORMAL, theme = 0;
	    theme = android.R.style.Theme_Light;
	    setStyle(style, theme); 
	}

	@SuppressWarnings("deprecation")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		int year;
		int month;
		int day;

		if (date == null) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DAY_OF_MONTH);
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			year = calendar.get(Calendar.YEAR); /*date.getYear();*/
			month = calendar.get(Calendar.MONTH); /*date.getMonth();*/
			day = calendar.get(Calendar.DAY_OF_MONTH);/* date.getDay();*/
		}

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), onDateSetListener, year,
				month, day);
	}
}

