package com.tawasol.fcbBarcelona.ui.custom.pager;

import com.tawasol.fcbBarcelona.R;

import android.content.Context;
import android.support.v4.view.PagerAdapter; 
import android.util.AttributeSet;

public class VerticalOverscrollViewPager extends OverscrollContainer<VerticalViewPager> {

    public VerticalOverscrollViewPager(Context context) {
	this(context, null);
    }

    public VerticalOverscrollViewPager(Context context, AttributeSet attrs) {
	this(context, attrs, 0);
    }

    public VerticalOverscrollViewPager(Context context, AttributeSet attrs, int defStyle) {
	super(context, attrs, defStyle);
    }

    @Override
    protected boolean canOverscrollAtStart() {
	VerticalViewPager viewPager = getOverscrollView();
	PagerAdapter adapter = viewPager.getAdapter();
	if (null != adapter) {
	    if (viewPager.getCurrentItem() == 0) {
		return true;
	    }
	    return false;
	}

	return false;
    }

    @Override
    protected boolean canOverscrollAtEnd() {
	VerticalViewPager viewPager = getOverscrollView();
	PagerAdapter adapter = viewPager.getAdapter();
	if (null != adapter && adapter.getCount() > 0) {
	    if (viewPager.getCurrentItem() == adapter.getCount() - 1) {
		return true;
	    }
	    return false;
	}

	return false;
    }

    @Override
    protected OverscrollContainer.OverscrollDirection getOverscrollDirection() {
	return OverscrollContainer.OverscrollDirection.Vertical;
    }

    @Override
    protected VerticalViewPager createOverscrollView() {
	VerticalViewPager viewPager = new VerticalViewPager(getContext());
	viewPager.setId(R.id.post_full_scr_pager);
	return viewPager;
    }

}
