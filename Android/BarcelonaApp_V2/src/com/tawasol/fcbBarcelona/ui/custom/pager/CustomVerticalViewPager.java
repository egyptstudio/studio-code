package com.tawasol.fcbBarcelona.ui.custom.pager;

import java.lang.reflect.Field;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class CustomVerticalViewPager extends VerticalViewPager {


	private Scroller mScroller;

	public CustomVerticalViewPager(Context context) {
		super(context);
		initCustomScroller();
	}
	
	public CustomVerticalViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		initCustomScroller();
	}
	
	private void initCustomScroller(){
		try {
            Field scrollerField = VerticalViewPager.class.getDeclaredField("mScroller");
            scrollerField.setAccessible(true);
            Field interpolatorField = VerticalViewPager.class.getDeclaredField("sInterpolator");
            interpolatorField.setAccessible(true);

            mScroller = new CustomDurationScroller(getContext(), (Interpolator)interpolatorField.get(null));
            scrollerField.set(this, mScroller);
        } catch (Exception e) {
        	Log.e("CustomVerticalViewPager#initCustomScroller", "Setting custom view pager scroller failed");
            e.printStackTrace();
            mScroller = null;
        }
	}

	 /**
     * Set the factor by which the duration of sliding animation will change.
     * The factor will be multiplied by the default duration, so the bigger the factor is
     * the slower the scroll will be.
     */
    public void setScrollDurationFactor(double scrollFactor) {
    	if (mScroller != null)
    		((CustomDurationScroller) mScroller).setScrollDurationFactor(scrollFactor);
    }
}
