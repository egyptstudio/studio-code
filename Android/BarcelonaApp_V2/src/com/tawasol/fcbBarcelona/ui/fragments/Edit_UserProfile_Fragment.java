package com.tawasol.fcbBarcelona.ui.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.soundcloud.android.crop.Crop;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.entities.City;
import com.tawasol.fcbBarcelona.entities.Country;
import com.tawasol.fcbBarcelona.entities.FCBPhoto;
import com.tawasol.fcbBarcelona.entities.KeyValuePairs;
import com.tawasol.fcbBarcelona.entities.PostViewModel;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnCountriesCodesRecieved;
import com.tawasol.fcbBarcelona.listeners.OnEntityListReceivedListener;
import com.tawasol.fcbBarcelona.listeners.OnUpdateProfileListener;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.Edit_More_Profile_Activity;
import com.tawasol.fcbBarcelona.ui.activities.Edit_UserProfile_Activity;
import com.tawasol.fcbBarcelona.ui.activities.FCBPhotoActivity;
import com.tawasol.fcbBarcelona.ui.activities.MyPicsActivity;
import com.tawasol.fcbBarcelona.ui.adapters.SpinnerAdapter;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.Utils;
import com.tawasol.fcbBarcelona.utils.ValidatorUtils;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class Edit_UserProfile_Fragment<T> extends BaseFragment implements
		OnClickListener, OnEntityListReceivedListener<T>,
		OnUpdateProfileListener, OnCountriesCodesRecieved {
	ImageView userImg;
	EditText userNameTxt;
	EditText districtTxt;
	EditText mobileTxt;
	EditText emailTxt;
	TextView countryTxt;
	TextView cityTxt;
	TextView birthDateTxt;
	RadioGroup genderRG;
	RadioButton maleRBtn;
	RadioButton femaleRBtn;
	Button moreBtn;
	TextView doneTxt;
	User user;
	EditText countryCode;
	ProgressBar profilePB;

	private Button cameraBtn, myPicsBtn;

	private List<KeyValuePairs> countriesList;
	private SpinnerAdapter countriesAdapter;
	RelativeLayout countryLayout;
	Country selectedCountry;

	private List<KeyValuePairs> citiesList;
	private SpinnerAdapter citiesAdapter;
	RelativeLayout cityLayout;
	City selectedCity;

	ArrayList<PostViewModel> myPics;

	String[] countriesCodes;
	// copied from registration Fragment
	private ArrayList<FCBPhoto> fcbPhotoList;
	private static Uri cropPhotoURI;
	private static int FCB_IMAGE = 0;
	private boolean GOTO_FCB_PHOTO_FRAGMENT = false;
	List<Country> originalCountryList = new ArrayList<Country>();

	// date picker
	DialogFragment dateDialogFragment;

	private boolean isImageFile = false;
	private ArrayAdapter<String> adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_edit_profile,
				container, false);

		this.user = UserManager.getInstance().getCurrentUser();
		ViewByID(rootView);
		InitViewControls();
		// call get countries WB
		UserManager.getInstance().getCountries();
		// call get my pics to hide or view "My Pics" button
		// UserManager.getInstance().getMyPics(100);

		return rootView;
	}

	private void ViewByID(View rootView) {
		userImg = (ImageView) rootView
				.findViewById(R.id.edit_profile_user_image_id);
		userNameTxt = (EditText) rootView
				.findViewById(R.id.edit_profile_user_name_txt);
		districtTxt = (EditText) rootView
				.findViewById(R.id.edit_user_profile_district_txt);
		mobileTxt = (EditText) rootView
				.findViewById(R.id.edit_user_profile_mobile_txt);
		emailTxt = (EditText) rootView
				.findViewById(R.id.edit_user_profile_email_txt);
		countryTxt = (TextView) rootView
				.findViewById(R.id.edit_user_profile_country_txt);
		countryLayout = (RelativeLayout) rootView
				.findViewById(R.id.country_layout);
		cityTxt = (TextView) rootView
				.findViewById(R.id.edit_user_profile_city_txt);
		cityLayout = (RelativeLayout) rootView.findViewById(R.id.city_layout);
		birthDateTxt = (TextView) rootView
				.findViewById(R.id.edit_user_profile_birth_date_txt);
		genderRG = (RadioGroup) rootView
				.findViewById(R.id.edit_user_profile_genderRadioGroup);
		maleRBtn = (RadioButton) rootView
				.findViewById(R.id.edit_profile_male_radio);
		femaleRBtn = (RadioButton) rootView
				.findViewById(R.id.edit_profile_female_radio);
		moreBtn = (Button) rootView.findViewById(R.id.edit_more_btn);
		doneTxt = (TextView) getActivity().findViewById(R.id.rightBtn);
		profilePB = (ProgressBar) rootView
				.findViewById(R.id.edit_profile_progressBar);
		countryCode = (EditText) rootView.findViewById(R.id.countryCodeText);
		countryCode.setOnClickListener(this);

	}

	private void InitViewControls() {
		// set the default drawable of user image
		// userImg.setImageDrawable(getResources().getDrawable(R.drawable.image_sample));
		// set the drawable of country
		countryTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				R.drawable.right_arrows, 0);
		// set the drawable of city
		cityTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				R.drawable.right_arrows, 0);
		// set the drawable of birthdate
		birthDateTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				R.drawable.right_arrows, 0);
		// set the right button
		doneTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.done,
				0);

		// set the user image text
		DisplayImage();
		/*
		 * if(NetworkingUtils.isNetworkConnected(getActivity())){
		 * DownloadImagesTask task = new DownloadImagesTask();
		 * task.execute(false); }else
		 * UIUtils.showToast(getActivity(),getResources
		 * ().getString(R.string.connection_noConnection));
		 */

		// set the user name ,email,phone ,mobile, birthdate
		userNameTxt.setText(user.getFullName());
		emailTxt.setText(user.getEmail());
		districtTxt.setText(user.getDistrict() != null
				&& !user.getDistrict().equals("") ? user.getDistrict() : null);
		mobileTxt.setText(user.getPhone() != null
				&& !user.getPhone().equals("") ? user.getPhone() : null);
		birthDateTxt.setText(user.getDateOfBirth() != 0 ? getDate(user
				.getDateOfBirth()) : "");

		// set the country
		if (user.getCountryName() != null && !user.getCountryName().equals("")) {
			countryTxt.setText(user.getCountryName());
			UserManager.getInstance().getCities(user.getCountryId());
		} else {
			countryTxt.setText("");
			cityTxt.setText("");
		}

		if (user.getPhoneCode() != null && !user.getPhoneCode().equals(""))
			countryCode.setText(user.getPhoneCode());

		// set the city
		if (user.getCityName() != null && !user.getCityName().equals(""))
			cityTxt.setText(user.getCityName());
		else {
			cityTxt.setText("");
		}

		// set the gender
		if (user.getGender() == 1) // male
			maleRBtn.setChecked(true);
		else if (user.getGender() == 2) // female
			femaleRBtn.setChecked(true);

		// countries list and adapter settings
		countriesList = new ArrayList<KeyValuePairs>();
		countriesAdapter = new SpinnerAdapter(getActivity(), countriesList);

		// cities list and adapter settings
		citiesList = new ArrayList<KeyValuePairs>();
		citiesAdapter = new SpinnerAdapter(getActivity(), citiesList);

		// set on click listeners
		birthDateTxt.setOnClickListener(this);
		userImg.setOnClickListener(this);
		countryLayout.setOnClickListener(this);
		cityLayout.setOnClickListener(this);
		moreBtn.setOnClickListener(this);
		doneTxt.setOnClickListener(this);

		textFilters();
	}

	private String getDate(long DOB) {
		Date date = new Date(DOB * 1000);
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy",
				Locale.getDefault());
		format.setTimeZone(TimeZone.getDefault());
		return format.format(date.getTime());
	}

	private String setDate(int year, int month, int day) {
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy",
				Locale.getDefault());
		format.setTimeZone(TimeZone.getDefault());

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.MONTH, month);

		return format.format(cal.getTime());

	}

	/** Download the bitmap of the image URL */
	public class DownloadImagesTask extends AsyncTask<Boolean, Void, Void> {
		Bitmap bmp = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			profilePB.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(Void result) {
			profilePB.setVisibility(View.GONE);
			isImageFile = false;
			if (bmp != null) {
				userImg.setImageBitmap(bmp);
			} else
				DisplayImage();
		}

		@Override
		protected Void doInBackground(Boolean... flag) {
			boolean flag_ = flag[0];

			try {
				URL ulrn;
				if (!flag_)
					ulrn = new URL(user.getProfilePic());
				else
					ulrn = new URL((String) userImg.getTag());
				HttpURLConnection con = (HttpURLConnection) ulrn
						.openConnection();
				InputStream is = con.getInputStream();
				bmp = BitmapFactory.decodeStream(is);
			} catch (Exception e) {
			}

			return null;
		}

	}

	private void DisplayImage() {
		DisplayImageOptions displayOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.ic_launcher)
				.cacheInMemory(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.considerExifParams(true)
				.displayer(
						new RoundedBitmapDisplayer((int) getResources()
								.getDimension(R.dimen.button_padding))).build();

		App.getInstance()
				.getImageLoader()
				.displayImage(user.getProfilePic(), userImg, displayOption,
						new ImageLoadingListener() {

							@Override
							public void onLoadingStarted(String arg0, View arg1) {
								profilePB.setVisibility(View.VISIBLE);
							}

							@Override
							public void onLoadingFailed(String arg0, View arg1,
									FailReason arg2) {
								profilePB.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingComplete(String arg0,
									View arg1, Bitmap arg2) {
								profilePB.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingCancelled(String arg0,
									View arg1) {

							}
						});

	}

	// copied from registration Fragment
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Crop.REQUEST_PICK
				&& resultCode == Activity.RESULT_OK) {
			if (data.getData() != null)
				beginCrop(data.getData());
			else {
				Bitmap returnedBMP = (Bitmap) data.getExtras().get("data");
				beginCrop(getImageUri(returnedBMP));
			}

		} else if (requestCode == Crop.REQUEST_CROP) {
			handleCrop(resultCode, data);

		} else if (requestCode == FCBPhotoActivity.REQUEST_FCB_PHOTO
				&& data != null) {

			Bundle bundle = data.getExtras();
			FCBPhoto fcbPhotoModel = (FCBPhoto) bundle
					.getSerializable(Params.UPLOAD_USER_PHOTO.FCB_PHOTO_OBJECT);

			String url = fcbPhotoModel.getUrl();
			user.setProfilePic(fcbPhotoModel.getPhotoName());
			userImg.setTag(url);

			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				DownloadImagesTask task = new DownloadImagesTask();
				task.execute(true);
			} else {
				UIUtils.showToast(
						getActivity(),
						getResources().getString(
								R.string.connection_noConnection));
			}
		} else if (requestCode == MyPicsActivity.REQUEST_MY_PICS
				&& data != null) {

			Bundle bundle = data.getExtras();
			PostViewModel selectedPic = (PostViewModel) bundle
					.getSerializable(Edit_UserProfile_Activity.MY_PICS_BUNLE_OBJ);

			String url = selectedPic.getPostPhotoUrl();
			user.setProfilePic(url);
			userImg.setTag(url);

			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				DownloadImagesTask task = new DownloadImagesTask();
				task.execute(true);
			} else {
				UIUtils.showToast(
						getActivity(),
						getResources().getString(
								R.string.connection_noConnection));
			}
		}
	}

	/*
	 * private int getImageOrientation(){ final String[] imageColumns = {
	 * MediaStore.Images.Media._ID, MediaStore.Images.ImageColumns.ORIENTATION
	 * }; final String imageOrderBy = MediaStore.Images.Media._ID+" DESC";
	 * Cursor cursor =
	 * getActivity().getContentResolver().query(MediaStore.Images
	 * .Media.EXTERNAL_CONTENT_URI, imageColumns, null, null, imageOrderBy);
	 * 
	 * if(cursor.moveToFirst()){ int orientation =
	 * cursor.getInt(cursor.getColumnIndex
	 * (MediaStore.Images.ImageColumns.ORIENTATION)); // rotate=orientation;
	 * System.out.println("orientation==="+orientation); cursor.close(); return
	 * orientation; } else { return 0; } }
	 */

	// copied from registration Fragment
	private void beginCrop(Uri source) {
		if (cropPhotoURI != null) {
			Utils.deleteFileNoThrow(cropPhotoURI.getPath());
		}

		ContextWrapper contextWrapper = new ContextWrapper(getActivity());

		// path to /data/data/yourapp/app_data/imageDir
		File directory = contextWrapper
				.getDir("imageDir", Context.MODE_PRIVATE);

		// Create imageDir
		File mypath = new File(directory, "tempImage");

		Uri outputUri = Uri.fromFile(mypath);
		cropPhotoURI = outputUri;

		new Crop(source).output(outputUri).asSquare().start(getActivity());
	}

	// copied from registration Fragment
	private void handleCrop(int resultCode, Intent result) {
		if (resultCode == Activity.RESULT_OK) {
			// Bitmap bmp = BitmapFactory.decodeFile(cropPhotoURI.getPath());
			// userImg.setBitmap(bmp);
			setOrientation();
			isImageFile = true;
		} else if (resultCode == Crop.RESULT_ERROR) {
			UIUtils.showToast(getActivity(), Crop.getError(result).getMessage());
		}
	}

	public Uri getImageUri(Bitmap inImage) {
		// ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		// inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(getActivity()
				.getContentResolver(), inImage, "tempImage", null);
		return Uri.parse(path);
	}

	private void setOrientation() {
		try {
			File f = new File(cropPhotoURI.getPath());
			ExifInterface exif = new ExifInterface(f.getPath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			int angle = 0;

			if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
				angle = 90;
			} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
				angle = 180;
			} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
				angle = 270;
			}
			Matrix mat = new Matrix();
			mat.postRotate(angle);
			Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f),
					null, null);
			Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
					bmp.getHeight(), mat, true);
			saveToInternalSorageBitmap(correctBmp, "tempImage");
			userImg.setImageBitmap(correctBmp);
		} catch (IOException e) {
			Log.w("TAG", "-- Error in setting image");
		} catch (OutOfMemoryError oom) {
			Log.w("TAG", "-- OOM Error in setting image");
		}
	}

	// save to internal storage
	private String saveToInternalSorageBitmap(Bitmap bitmapImage,
			String fileName) {
		// preview.setDrawingCacheEnabled(false);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmapImage.compress(CompressFormat.JPEG, 100, bos);
		byte[] bitmapdata = bos.toByteArray();

		ContextWrapper cw = new ContextWrapper(getActivity());
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		// Create imageDir
		File mypath = new File(directory, fileName);
		if (mypath.exists())
			mypath.delete();
		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(mypath);
			fos.write(bitmapdata);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		cropPhotoURI = Uri.fromFile(mypath);
		return directory.getAbsolutePath();
	}

	/**
	 * Initialize of List Dialog for country , city
	 * 
	 * @param <D>
	 **/
	private <D> void showListDialog(final D Object) {
		// Initialization of dialog
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.popup_list);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.GRAY));
		dialog.getWindow().setTitleColor(android.graphics.Color.WHITE);

		// set the title
		if (Object instanceof Country)
			dialog.setTitle(getResources().getString(R.string.Filter1_title2));
		else if (Object instanceof City)
			dialog.setTitle(getResources().getString(
					R.string.select_city_header));

		// initialize controls
		ListView listView = (ListView) dialog.findViewById(R.id.popup_list);
		// countries list and adapter settings

		if (Object instanceof Country)
			listView.setAdapter(countriesAdapter);
		else if (Object instanceof City)
			listView.setAdapter(citiesAdapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (Object instanceof Country) {
					selectedCountry = new Country();
					selectedCountry.setCountryId(countriesList.get(position)
							.get_Id());
					selectedCountry.setCountryName((String) countriesList.get(
							position).getText());
					cityTxt.setText("");
					UserManager.getInstance().getCities(
							selectedCountry.getCountryId());
					countryCode.setText(originalCountryList.get(position).getPhoneCode());
					showLoadingDialog();
				} else if (Object instanceof City) {
					selectedCity = new City();
					selectedCity.setCityId(citiesList.get(position).get_Id());
					selectedCity.setCityName((String) citiesList.get(position)
							.getText());
					districtTxt.requestFocus();
				}

				dialog.dismiss();
			}
		});
		dialog.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				if (Object instanceof Country) {
					if (selectedCountry != null)
						countryTxt.setText(selectedCountry.getCountryName());
				} else if (Object instanceof City) {
					if (selectedCity != null)
						cityTxt.setText(selectedCity.getCityName());
				}

			}
		});

		// show the dialog
		dialog.show();

	}

	/**
	 * Initialize of List Dialog for country , city
	 * 
	 * @param <D>
	 **/
	private <D> void showPhoneCodesDialog() {

		// Initialization of dialog
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_list);
//		dialog.getWindow().setBackgroundDrawable(
//				new ColorDrawable(android.graphics.Color.GRAY));
//		dialog.getWindow().setTitleColor(android.graphics.Color.WHITE);

		// set the title and list
		String[] stringsList = null;
		stringsList = countriesCodes;
//		dialog.setTitle(getResources()
//				.getString(R.string.select_jobRole_header));

		// initailize controls
		ListView listView = (ListView) dialog.findViewById(R.id.popup_list);
		// countries list and adapter settings
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.popup_list_item, stringsList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				countryCode.setText(countriesCodes[position]);
				dialog.dismiss();
			}
		});

		// show the dialog
		dialog.show();

	}

	/** Initialize of PhotoList Dialog **/
	private void showEditProfilePicDialog() {

		// Initialization of dialog
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.edit_profile_photo_dialog);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		// My pics
		myPicsBtn = (Button) dialog.findViewById(R.id.MyPicsBtn);
		myPicsBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (myPics != null && myPics.size() > 0) {
					Intent intent = MyPicsActivity
							.getActivityIntent(getActivity());
					// Bundle bundle = new Bundle();
					// bundle.putSerializable(Edit_UserProfile_Activity.MY_PICS_BUNLE_OBJ,
					// (Serializable) myPics);
					intent.putExtra(
							Edit_UserProfile_Activity.MY_PICS_BUNLE_OBJ,
							(Serializable) myPics);
					getActivity().startActivityForResult(intent,
							MyPicsActivity.REQUEST_MY_PICS);

				}

				/*
				 * if(! myPics.isEmpty()) { Intent intent =
				 * MyPicsActivity.getActivityIntent(getActivity()); // Bundle
				 * bundle = new Bundle(); //
				 * bundle.putSerializable(Edit_UserProfile_Activity
				 * .MY_PICS_BUNLE_OBJ, (Serializable) myPics);
				 * intent.putExtra(Edit_UserProfile_Activity.MY_PICS_BUNLE_OBJ,
				 * (Serializable) myPics);
				 * getActivity().startActivityForResult(intent
				 * ,MyPicsActivity.REQUEST_MY_PICS);
				 * 
				 * dialog.dismiss(); }
				 */
				else {
					showLoadingDialog();
					UserManager.getInstance().getMyPics(100);
				}
				dialog.dismiss();
			}
		});

		// camera
		cameraBtn = (Button) dialog.findViewById(R.id.CameraBtn);
		cameraBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Crop.pickImageFromCamera(getActivity());
				dialog.dismiss();
			}
		});

		// photo album
		Button photoAlbumBtn = (Button) dialog.findViewById(R.id.PhotoAlbumBtn);
		photoAlbumBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Crop.pickImage(getActivity());
				dialog.dismiss();
			}
		});

		// Call FCBPhoto web service
		Button FCBPhotosBtn = (Button) dialog.findViewById(R.id.FcbPhotosBtn);
		FCBPhotosBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (fcbPhotoList != null && fcbPhotoList.size() > 0
						&& GOTO_FCB_PHOTO_FRAGMENT) {
					goToFCBPhotoFragment(fcbPhotoList);
				} else {
					FCB_IMAGE = Params.UPLOAD_USER_PHOTO.FCB_SELECT_IMAGE;
					UserManager.getInstance().callFCBPhoto(
							LanguageUtils.getInstance().getDeviceLanguage(),
							true);
					showLoadingDialog();
				}

				dialog.dismiss();
			}
		});

		// dismiss dialog for cancel
		Button cancelBtn = (Button) dialog.findViewById(R.id.CancelBtn);
		cancelBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});

		// hide the button if there is no data and vice versa
		// if(myPics != null && !myPics.isEmpty())
		// {
		myPicsBtn.setVisibility(View.VISIBLE);
		myPicsBtn.setBackgroundResource(R.drawable.ic_dialog_top);
		cameraBtn.setBackgroundResource(R.drawable.ic_dialog_center);
		// }
		// else
		// {
		// myPicsBtn.setVisibility(View.GONE);
		// cameraBtn.setBackgroundResource(R.drawable.ic_dialog_top);
		// }

		/** Show dialog **/
		dialog.show();
	}

	/** GoTo FCBPhotoFragment with FCB photos list **/
	private void goToFCBPhotoFragment(ArrayList<FCBPhoto> list) {
		/**
		 * Put FCB photos in list, then start FCBPhotoFragment to chose profile
		 * photo
		 **/
		Intent intent = FCBPhotoActivity.getActivityIntent(getActivity());
		Bundle bundle = new Bundle();
		bundle.putSerializable(Params.UPLOAD_USER_PHOTO.FCB_LIST_NAME,
				(Serializable) list);
		intent.putExtra(Params.UPLOAD_USER_PHOTO.FCB_BUNDLE, bundle);
		getActivity().startActivityForResult(intent,
				FCBPhotoActivity.REQUEST_FCB_PHOTO);
	}

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	/** Handle FCBPhoto response */
	// @Override
	// public void onSuccess(List<FCBPhoto> list) {
	// hideLoadingDialog();
	// if(list.size() > 0 && FCB_IMAGE ==
	// Params.UPLOAD_USER_PHOTO.FCB_RANDOM_IMAGE){
	// /** Check if list > 0, if true set flag"GOTO_FCB_PHOTO_FRAGMENT" equal
	// true then proceed to FCBPhotosFragment **/
	// GOTO_FCB_PHOTO_FRAGMENT = true;
	// fcbPhotoList = (ArrayList<FCBPhoto>) list;
	//
	// }else if(list.size() > 0 && FCB_IMAGE ==
	// Params.UPLOAD_USER_PHOTO.FCB_SELECT_IMAGE){
	// GOTO_FCB_PHOTO_FRAGMENT = true;
	// fcbPhotoList = (ArrayList<FCBPhoto>) list;
	//
	// goToFCBPhotoFragment(fcbPhotoList);
	// }
	// }
	public User buildUser() {

		if (isImageFile)
			user.setProfilePic(cropPhotoURI.getPath());

		// user.setProfilePic((String)userImg.getTag());
		user.setFullName(userNameTxt.getText().toString());
		user.setEmail(emailTxt.getText().toString());
		if (!ValidatorUtils.isRequired(birthDateTxt.getText().toString())) {
			SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy",
					Locale.getDefault());
			Date d = null;
			try {
				d = f.parse(birthDateTxt.getText().toString());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if (d != null)
				user.setDateOfBirth(d.getTime() / 1000);
		}
		//
		if (maleRBtn.isChecked())
			user.setGender(1);
		else if (femaleRBtn.isChecked())
			user.setGender(2);

		if (selectedCountry != null)
			user.setCountryId(selectedCountry.getCountryId());
		if (selectedCity != null)
			user.setCityId(selectedCity.getCityId());

		user.setCountryName(countryTxt.getText().toString());
		user.setCityName(cityTxt.getText().toString());

		user.setDistrict(districtTxt.getText().toString());
		user.setPhone(mobileTxt.getText().toString());
		if (countryCode.getText().toString() != null
				&& !countryCode.getText().toString().equals(""))
			user.setPhoneCode(countryCode.getText().toString());
		return user;
	}

	@Override
	public void onResume() {
		if (Edit_UserProfile_Activity.finish
				&& getActivity().getIntent().getBooleanExtra(
						Edit_UserProfile_Activity.fromShare, false))
			getActivity().finish();
		super.onResume();
		if (UserManager.getInstance().getCurrentUserId() != 0)
			UserManager.getInstance().addListener(this);
		else
			getActivity().finish();
	}

	@Override
	public void onPause() {
		super.onPause();
		UserManager.getInstance().removeListener(this);
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.edit_user_profile_birth_date_txt:
			Date date = null;
			if (!birthDateTxt.getText().toString().isEmpty()) {
				SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy");
				try {
					date = f.parse(birthDateTxt.getText().toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else if (user.getDateOfBirth() != 0)
				date = new Date(getDate(user.getDateOfBirth()));
			else
				date = Calendar.getInstance().getTime();

			// if (dateDialogFragment ==
			// null)
			{
				dateDialogFragment = UIUtils.getDatePicker(date,
						new OnDateSetListener() {

							@Override
							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {
								/*
								 * birthDateTxt.setText(year + "-" + monthOfYear
								 * + "-" + dayOfMonth);
								 */
								Calendar cal = Calendar.getInstance();
								cal.set(Calendar.YEAR, year);
								cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
								cal.set(Calendar.MONTH, monthOfYear);
								if (!cal.getTime().after(
										Calendar.getInstance().getTime()))
									birthDateTxt.setText(setDate(year,
											monthOfYear, dayOfMonth));
								else
									UIUtils.showToast(
											getActivity(),
											getResources().getString(
													R.string.future_date));
							}
						});
			}

			dateDialogFragment.show(getFragmentManager(), getResources()
					.getString(R.string.MyProfile_Birthdate));
			break;

		case R.id.edit_profile_user_image_id:
			showEditProfilePicDialog();
			// EditProfilePictureDialog editPhotoDialog =
			// EditProfilePictureDialog.new_Instance();
			// editPhotoDialog.show(getActivity().getSupportFragmentManager(),
			// null);
			break;
		case R.id.edit_more_btn:
			startActivity(Edit_More_Profile_Activity.getActivityIntent(
					getActivity(),
					buildUser(),
					getActivity().getIntent().getBooleanExtra(
							Edit_UserProfile_Activity.fromShare, false)));
			break;
		case R.id.country_layout:
			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				if (!countriesList.isEmpty()) {
					showListDialog(new Country());
				}
			} else
				UIUtils.showToast(
						getActivity(),
						getResources().getString(
								R.string.connection_noConnection));
			break;
		case R.id.city_layout:
			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				if (!citiesList.isEmpty()) {
					if (!ValidatorUtils.isRequired(countryTxt.getText()
							.toString())) {
						// selectedCity = null;
						showListDialog(new City());
					} else
						UIUtils.showToast(getActivity(), getResources()
								.getString(R.string.enter_country_first));
				}
			} else
				UIUtils.showToast(
						getActivity(),
						getResources().getString(
								R.string.connection_noConnection));
			break;
		case R.id.rightBtn:
			if (!ValidatorUtils.isRequired(emailTxt.getText())) {
				if (ValidatorUtils.isValidEmail(emailTxt.getText())) {
					showLoadingDialog();
					UserManager.getInstance().udateProfile(buildUser(),
							isImageFile, true);
				} else
					UIUtils.showToast(
							getActivity(),
							getResources()
									.getString(
											R.string.Registration_IncorrectEmailFormatAlert));
			} else
				UIUtils.showToast(
						getActivity(),
						getResources().getString(
								R.string.Login_emailMissingAlert));

			break;
		case R.id.countryCodeText:
			if (countriesCodes != null)
				showPhoneCodesDialog();
			break;
		default:
			break;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public void onSuccess(List<T> objs) {
		if (!objs.isEmpty()) {
			if (objs.get(0) instanceof FCBPhoto) {
				hideLoadingDialog();
				if (objs.size() > 0
						&& FCB_IMAGE == Params.UPLOAD_USER_PHOTO.FCB_RANDOM_IMAGE) {
					/**
					 * Check if list > 0, if true set flag
					 * "GOTO_FCB_PHOTO_FRAGMENT" equal true then proceed to
					 * FCBPhotosFragment
					 **/
					GOTO_FCB_PHOTO_FRAGMENT = true;
					fcbPhotoList = (ArrayList<FCBPhoto>) objs;

				} else if (objs.size() > 0
						&& FCB_IMAGE == Params.UPLOAD_USER_PHOTO.FCB_SELECT_IMAGE) {
					GOTO_FCB_PHOTO_FRAGMENT = true;
					fcbPhotoList = (ArrayList<FCBPhoto>) objs;

					goToFCBPhotoFragment(fcbPhotoList);
				}
			} else if (objs.get(0) instanceof PostViewModel) {
				hideLoadingDialog();
				if (objs.size() > 0) {
					myPics = (ArrayList<PostViewModel>) objs;
					Intent intent = MyPicsActivity
							.getActivityIntent(getActivity());
					intent.putExtra(
							Edit_UserProfile_Activity.MY_PICS_BUNLE_OBJ,
							(Serializable) myPics);
					getActivity().startActivityForResult(intent,
							MyPicsActivity.REQUEST_MY_PICS);
				}

				/*
				 * if(cameraBtn!= null && myPicsBtn != null) {
				 * myPicsBtn.setVisibility(View.VISIBLE);
				 * myPicsBtn.setBackgroundResource(R.drawable.ic_dialog_top);
				 * cameraBtn.setBackgroundResource(R.drawable.ic_dialog_center);
				 * }
				 */

			} else if (objs.get(0) instanceof Country) {
				originalCountryList.clear();
				originalCountryList.addAll((List<Country>) objs);
				countriesList.clear();
				countriesList.addAll(convertToCountries((List<Country>) objs));
				countriesAdapter.notifyDataSetChanged();
				if (selectedCountry != null)
					UserManager.getInstance().getCities(
							((Country) objs.get(0)).getCountryId());
			} else if (objs.get(0) instanceof City) {
				hideLoadingDialog();
				citiesList.clear();
				citiesList.addAll(convertToCities((List<City>) objs));
				citiesAdapter.notifyDataSetChanged();
				if (selectedCountry != null) {
					cityTxt.setText(((City) objs.get(0)).getCityName());
					selectedCity = (City) objs.get(0);
				}
			}

		} else {
			hideLoadingDialog();
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.no_data));
		}

	}

	private List<KeyValuePairs> convertToCountries(List<Country> countries) {
		List<KeyValuePairs> countriesList_ = new ArrayList<KeyValuePairs>();
		for (Country country : countries) {
			countriesList_.add(new KeyValuePairs(country.getCountryId(),
					country.getCountryName()));

		}
		return countriesList_;
	}

	private List<KeyValuePairs> convertToCities(List<City> cities) {
		List<KeyValuePairs> citiesList_ = new ArrayList<KeyValuePairs>();
		for (City city : cities) {
			citiesList_.add(new KeyValuePairs(city.getCityId(), city
					.getCityName()));

		}
		return citiesList_;
	}

	@Override
	public void onSuccess(User obj) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), getString(R.string.profile_updated));
		getActivity().finish();
	}

	private void textFilters() {
		userNameTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					userNameTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		emailTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					emailTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		mobileTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					mobileTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		districtTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					districtTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	@Override
	public void onPhonesCodesRecieved(Set<String> phonesCodes) {
		countriesCodes = new String[phonesCodes.size()];
		countriesCodes = phonesCodes.toArray(countriesCodes);
	}
}
