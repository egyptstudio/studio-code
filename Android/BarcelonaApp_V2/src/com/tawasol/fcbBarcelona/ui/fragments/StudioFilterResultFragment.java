package com.tawasol.fcbBarcelona.ui.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.StudioPhotosTable;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.entities.Seasons;
import com.tawasol.fcbBarcelona.entities.StudioPhoto;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnStudioPhotosReceivedListener;
import com.tawasol.fcbBarcelona.listeners.OnUserCreditReceived;
import com.tawasol.fcbBarcelona.managers.StudioManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.CaptureActivity;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivityTabs;
import com.tawasol.fcbBarcelona.ui.activities.StudioFilterResultActivity;
import com.tawasol.fcbBarcelona.ui.adapters.StudioPhotosAdapter;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class StudioFilterResultFragment extends BaseFragment implements
		OnStudioPhotosReceivedListener, OnScrollListener, OnItemClickListener,
		OnUserCreditReceived {

	private final static int USE = 100;
	private final static int UPGRADE_TO_PREMIUM = 101;
	private final static int BUY_POINTS = 102;
	TextView noOfPhotos;
	GridView photosResultGV;
	TextView noResultsTxt;
	ImageView noResultsImg;
	private static int pageNo = 1;
	private boolean gettinMoreData = true;
	int filterBy;
	ArrayList<Seasons> seasons;

	List<StudioPhoto> photos;
	StudioPhotosAdapter adapter;

	String UserCredit = "-1";
	TextView userCreditTxt;
	private ProgressBar creditPB;
	Dialog dialog;
	ViewPager photosPager;
	PhotoPagerAdapter photospagerAdapter;
	ImageView imageView;

	private boolean userScrolled;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_studio_filter_result, container, false);
		pageNo = 1;

		/* get view controls */
		viewByID(rootView);

		getFilterData();

		initViewControls();

		/* call Filter WB */
		StudioManager.getInstance().getStudioPhotos(0, pageNo, filterBy,
				seasons);

		return rootView;
	}

	private void viewByID(View rootView) {
		noOfPhotos = (TextView) rootView
				.findViewById(R.id.no_of_photos_result_txt);
		photosResultGV = (GridView) rootView
				.findViewById(R.id.result_photos_gridview);
		noResultsTxt = (TextView) rootView
				.findViewById(R.id.no_filter_result_txt);
		noResultsImg = (ImageView) rootView.findViewById(R.id.no_results_img);
	}

	private void initViewControls() {
		showLoadingDialog();
		// initialize the list
		photos = new ArrayList<StudioPhoto>();

		// initialize the adapter
		adapter = new StudioPhotosAdapter(getActivity(),
				(ArrayList<StudioPhoto>) photos);
		photosResultGV.setAdapter(adapter);

		photosResultGV.setOnItemClickListener(this);
		photosResultGV.setOnScrollListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ShowPhotosPopUp(position);
	}

	@SuppressWarnings("unchecked")
	private void getFilterData() {
		this.filterBy = getActivity().getIntent().getExtras()
				.getInt(StudioFilterResultActivity.FILTER_BY_KEY);
		this.seasons = (ArrayList<Seasons>) getActivity().getIntent()
				.getExtras()
				.getSerializable(StudioFilterResultActivity.SEASONS_KEY);
	}

	// private void ShowPhotosPopUp(final int Selected_Position) {
	// // if (dialog != null && dialog.isShowing())
	// // return;
	// // // custom dialog
	// // dialog = new Dialog(getActivity(), R.style.FullHeightDialog);
	// // dialog.setContentView(R.layout.photos_dialog);
	// // dialog.getWindow().setBackgroundDrawable(
	// // new ColorDrawable(android.graphics.Color.TRANSPARENT));
	// //
	// // // get and set user the credit text
	// // TextView userCreditTxt = (TextView) dialog
	// // .findViewById(R.id.usercredit_txt);
	// //
	// //
	// userCreditTxt.setText(UserManager.getInstance().getCurrentUser().getCredit()
	// // + "\n "
	// // + getString(R.string.studio_xp));
	// //
	// // // get and set the photo points
	// // TextView photoPoints = (TextView) dialog
	// // .findViewById(R.id.photo_points_txt);
	// // photoPoints.setText(getString(R.string.photoUse_unock_credit) + " "
	// // + photos.get(Selected_Position).getRequiredPoints() + " "
	// // + getString(R.string.studio_xp));
	// // // define the view pager
	// // photosPager = (ViewPager) dialog.findViewById(R.id.photo_pager_id);
	// // // define the pervious btn
	// // ImageButton prevBtn = (ImageButton) dialog
	// // .findViewById(R.id.photos_previous_img_id);
	// // // define the next btn
	// // ImageButton nextBtn = (ImageButton) dialog
	// // .findViewById(R.id.photos_next_img_id);
	// // // define the user button
	// // Button useBtn = (Button) dialog.findViewById(R.id.photos_use_btn_id);
	// //
	// // // setting adapter of the pager
	// // photospagerAdapter = new PhotoPagerAdapter();
	// // photosPager.setAdapter(photospagerAdapter);
	// // set the on pagescroll listener to detect the last swiped image
	//
	// if (dialog != null && dialog.isShowing())
	// return;
	// // custom dialog
	// dialog = new Dialog(getActivity(), R.style.FullHeightDialog);
	// dialog.setContentView(R.layout.photos_dialog);
	// dialog.getWindow().setBackgroundDrawable(
	// new ColorDrawable(android.graphics.Color.TRANSPARENT));
	//
	// // get and set user the credit text
	// userCreditTxt = (TextView) dialog.findViewById(R.id.usercredit_txt);
	// creditPB = (ProgressBar) dialog
	// .findViewById(R.id.user_credit_progress_bar);
	//
	// if (UserManager.getInstance().getCurrentUserId() != 0) {
	// if (this.UserCredit != null && !this.UserCredit.equals("-1"))
	// userCreditTxt.setText(getString(R.string.photoUse_unock_credit)
	// + " " + this.UserCredit + " "
	// + getString(R.string.studio_xp));
	// creditPB.setVisibility(View.GONE);
	// /*
	// * else userCreditTxt.setText(UserManager.getInstance()
	// * .getCurrentUser().getCredit() + "\n " +
	// * getString(R.string.studio_xp));
	// */
	// } else {
	// userCreditTxt.setVisibility(View.GONE);
	// creditPB.setVisibility(View.GONE);
	// }
	//
	// // get and set the photo points
	// final TextView photoPoints = (TextView) dialog
	// .findViewById(R.id.photo_points_txt);
	//
	// // define the view pager
	// photosPager = (ViewPager) dialog.findViewById(R.id.photo_pager_id);
	// // define the pervious btn
	// ImageButton prevBtn = (ImageButton) dialog
	// .findViewById(R.id.photos_previous_img_id);
	// // define the next btn
	// ImageButton nextBtn = (ImageButton) dialog
	// .findViewById(R.id.photos_next_img_id);
	// // define the user button
	// final Button useBtn = (Button) dialog
	// .findViewById(R.id.photos_use_btn_id);
	//
	// // setting adapter of the pager
	// photospagerAdapter = new PhotoPagerAdapter();
	// photosPager.setAdapter(photospagerAdapter);
	//
	// OnPageChangeListener mListener = new OnPageChangeListener() {
	//
	// @Override
	// public void onPageSelected(int arg0) {
	// }
	//
	// @Override
	// public void onPageScrolled(int arg0, float arg1, int arg2) {
	// // check if standing at the last item , then call web service to
	// // get more pictures
	// photoPoints.setText(photos.get(photosPager.getCurrentItem())
	// .getRequiredPoints()
	// + " "
	// + getString(R.string.studio_xp));
	// if (photosPager.getCurrentItem() == photos.size() - 1
	// && !gettinMoreData && pageNo != -1) {
	// gettinMoreData = true;
	// StudioManager.getInstance().getStudioPhotos(0, pageNo,
	// filterBy, seasons);
	// }
	// }
	//
	// @Override
	// public void onPageScrollStateChanged(int arg0) {
	// }
	// };
	// photosPager.setOnPageChangeListener(mListener);
	//
	// // set the position of pager with the selected item
	// photosPager.setCurrentItem(Selected_Position);
	//
	// // setting the click listener of previous button
	// prevBtn.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// photoPoints.setText(photos.get(photosPager.getCurrentItem())
	// .getRequiredPoints()
	// + " "
	// + getString(R.string.studio_xp));
	// photosPager.setCurrentItem(photosPager.getCurrentItem() - 1,
	// true);
	// // Toast.makeText(getActivity(),
	// // photosPager.getCurrentItem()-1+"",
	// // Toast.LENGTH_SHORT).show();
	//
	// }
	// });
	// // setting the click listener of next button
	// nextBtn.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// photoPoints.setText(photos.get(photosPager.getCurrentItem())
	// .getRequiredPoints()
	// + " "
	// + getString(R.string.studio_xp));
	// photosPager.setCurrentItem(photosPager.getCurrentItem() + 1,
	// true);
	// // check if standing at the last item , then call web service to
	// // get more pictures
	// if (photosPager.getCurrentItem() == photos.size() - 1
	// && !gettinMoreData && pageNo != -1) {
	// gettinMoreData = true;
	// StudioManager.getInstance().getStudioPhotos(0, pageNo,
	// filterBy, seasons);
	// }
	// }
	// });
	//
	// // setting the listener of Use button
	// useBtn.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // TODO first check user , unlock ! :)
	//
	// // get the selected photo
	// final StudioPhoto selectedPhoto = photos.get(photosPager
	// .getCurrentItem());
	// // // get the bitmap of the image
	// //
	// // App.getInstance()
	// // .getImageLoader()
	// // .loadImage(selectedPhoto.getPhotoURL(),
	// // new SimpleImageLoadingListener() {
	// //
	// // @Override
	// // public void onLoadingComplete(
	// // String imageUri, View view,
	// // Bitmap loadedImage) {
	// // // TODO navigate to Capture :)
	// // // save the Stream to device
	// // super.onLoadingComplete(imageUri, view,
	// // loadedImage);
	// // ByteArrayOutputStream stream = new ByteArrayOutputStream();
	// // loadedImage.compress(
	// // Bitmap.CompressFormat.PNG, 60,
	// // stream);
	// // String path = saveToInternalSorage(loadedImage);
	// // startActivity(CaptureActivity
	// // .getActivityIntent(
	// // getActivity(),
	// // path,
	// // selectedPhoto,
	// // selectedPhoto
	// // .getFolderId() == StudioFolder.FOLDER_TYPE_PLAYER ? true
	// // : false));
	// //
	// // }
	// //
	// // });
	//
	// if (UserManager.getInstance().getCurrentUserId() != 0) {
	//
	// if (Integer.parseInt(UserCredit) >= selectedPhoto
	// .getRequiredPoints() || selectedPhoto.getIsPurchased() == 1) {
	//
	// // TODO first check user , unlock ! :)
	//
	// if ((Integer) useBtn.getTag() == USE) {
	// // get the selected photo
	//
	// // get the bitmap of the image
	//
	// App.getInstance()
	// .getImageLoader()
	// .loadImage(selectedPhoto.getPhotoURL(),
	// new SimpleImageLoadingListener() {
	//
	// @Override
	// public void onLoadingComplete(
	// String imageUri,
	// View view,
	// Bitmap loadedImage) {
	// // save the Stream to device
	// super.onLoadingComplete(
	// imageUri, view,
	// loadedImage);
	// ByteArrayOutputStream stream = new ByteArrayOutputStream();
	// loadedImage
	// .compress(
	// Bitmap.CompressFormat.PNG,
	// 60, stream);
	//
	// try {
	// boolean single = false;
	// if (selectedPhoto
	// .getCaptureType() == StudioPhoto.PHOTO_CAPTURE_TYPE_MASK_SHOT) // 1
	// // -->
	// // rotatable
	// // a5er
	// // klam
	// // isA
	// // :D
	// single = true;
	// else if (selectedPhoto
	// .getCaptureType() == StudioPhoto.PHOTO_CAPTURE_TYPE_SCREEN_SHOT) // 2
	// // --
	// // not
	// // rotatable
	// single = false;
	//
	// String path = saveToInternalSorage(loadedImage);
	// startActivity(CaptureActivity
	// .getActivityIntent(
	// getActivity(),
	// path,
	// selectedPhoto,
	// single));
	// // selectedPhoto.getFolderId()
	// // ==
	// // StudioFolder.FOLDER_TYPE_PLAYER
	// // ? true: false
	//
	// // TODO call update user
	// // credit
	// // .. to be discussed
	// // with Hamed
	// // !
	// // TODO call purchase
	// // photo WB
	// if (selectedPhoto
	// .getIsPurchased() != 1) {
	// StudioManager
	// .getInstance()
	// .purchasePhoto(
	// selectedPhoto
	// .getStudioPhotoId());
	// StudioPhotosTable
	// .getInstance()
	// .setPurchased(
	// selectedPhoto
	// .getStudioPhotoId());
	// }
	//
	// }
	//
	// catch (Exception e) {
	// }
	// }
	// });
	//
	// // if(selectedPhoto.getIsPurchased() == 0)
	// // /** --- Added by Turki --- **/
	// // StudioManager.getInstance().updateUserCredit(
	// // -1 * selectedPhoto.getRequiredPoints());
	// /** --- --- **/
	// } else if ((Integer) useBtn.getTag() == UPGRADE_TO_PREMIUM) // UNLOCK
	// // (Upgrade
	// // To
	// // Premium)
	// {
	// /*
	// * UIUtils.showToast(getActivity(),
	// * "navigate to shop to upgrade to premium");
	// */
	// startActivity(ShopActivityTabs
	// .getIntent(getActivity(),
	// ShopActivityTabs.PREMIUM_TAB));
	//
	// // TODO Redirect to Shop Screen
	// /*
	// * startActivityForResult( new Intent(getActivity(),
	// * PremiumPackagesActivity.class).putExtra(
	// * Params.Studio.FOLDER_ID,
	// * folder.getFolderId()).putExtra(
	// * Params.Studio.STUDIO_PHOTO_ID,
	// * selectedPhoto.getStudioPhotoId()),
	// * STUDIO_PHOTOS_INTENT_REQUEST_CODE);
	// */
	// } else if ((Integer) useBtn.getTag() == BUY_POINTS) // UNLOCK
	// // (Buy
	// // Points)
	// {
	// /*
	// * UIUtils.showToast(getActivity(),
	// * "navigate to shop to buy points");
	// */
	// startActivity(ShopActivityTabs.getIntent(
	// getActivity(),
	// ShopActivityTabs.POINTS_PACKAGES));
	//
	// // TODO Redirect to Shop Screen
	// /*
	// * startActivityForResult( ShopActivity
	// * .getIntent(getActivity(), false)
	// * .putExtra(Params.Studio.FOLDER_ID,
	// * folder.getFolderId())
	// * .putExtra(Params.Studio.STUDIO_PHOTO_ID,
	// * selectedPhoto.getStudioPhotoId()),
	// * STUDIO_PHOTOS_INTENT_REQUEST_CODE);
	// */
	//
	// }
	// } else {
	// startActivity(ShopActivityTabs.getIntent(getActivity(),
	// ShopActivityTabs.POINTS_PACKAGES));
	// }
	// } else {
	// if (folder != null)
	// getActivity().startActivityForResult(
	// LogInActivity.getActivityIntent(getActivity(),
	// UserManager.LOGIN_FOR_RESULT,
	// new Intent().putExtra(
	// Params.Studio.FOLDER_ID,
	// folder.getFolderId())
	// // .putExtra(Params.Studio.STUDIO_PHOTO_ID,
	// // selectedPhoto.getStudioPhotoId())
	// ), ACTION_REQUEST_LOGIN);
	// }
	// }
	// });
	//
	// // dismiss the dialog button
	// ImageButton CloseBtn = (ImageButton) dialog
	// .findViewById(R.id.photos_close_img_id);
	// CloseBtn.setOnClickListener(new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// dialog.dismiss();
	// }
	// });
	//
	// dialog.show();
	//
	// }

	/** Check the Photo usability Status */
	private int canUsePhoto(StudioPhoto selectedPhoto) {

		// if (UserManager.getInstance().getCurrentUserId() != 0) // logged in
		// {
		if (selectedPhoto.getIsPurchased() != 1) {
			User currentUser = UserManager.getInstance().getCurrentUser();

			if (selectedPhoto.getIsPermium() == 1) // if Photo Premium
			{
				if (currentUser.isPremium()) // if User is Premium
				{
					return doesUserHasEnoughPoints(currentUser, selectedPhoto);
				} else // Not Premium User
				{
					// TODO Navigate to Shop to upgrade to premium
					return UPGRADE_TO_PREMIUM;
				}
			} else // Photo is not premium
			{
				return doesUserHasEnoughPoints(currentUser, selectedPhoto);
			}
		} else
			return USE;
		// } else
		// // not logged in
		// return USE;
		// return USE;

	}

	private int doesUserHasEnoughPoints(User currentUser,
			StudioPhoto selectedPhoto) {

		if (currentUser.getCredit() >= selectedPhoto.getRequiredPoints()
				&& currentUser.getCredit() > 0 /*
												 * &&
												 * selectedPhoto.getRequiredPoints
												 * () >0
												 */) // User
		// has
		// enough
		// points
		{
			// TODO set "Use" Button and navigate to Capture :)
			return USE;
		} else // No Enough Credit
		{
			// TODO Navigate to Shop to buy points
			return BUY_POINTS;
		}

	}

	private void ShowPhotosPopUp(final int Selected_Position) {
		if (dialog != null && dialog.isShowing())
			return;
		// custom dialog
		dialog = new Dialog(getActivity(), R.style.FullHeightDialog);
		dialog.setContentView(R.layout.photos_dialog);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		// get and set user the credit text
		userCreditTxt = (TextView) dialog.findViewById(R.id.usercredit_txt);
		creditPB = (ProgressBar) dialog
				.findViewById(R.id.user_credit_progress_bar);

		if (UserManager.getInstance().getCurrentUserId() != 0) {
			if (this.UserCredit != null && !this.UserCredit.equals("-1"))
				userCreditTxt.setText(getString(R.string.photoUse_unock_credit)
						+ " " + this.UserCredit + " "
						+ getString(R.string.studio_xp));
			creditPB.setVisibility(View.GONE);
			/*
			 * else userCreditTxt.setText(UserManager.getInstance()
			 * .getCurrentUser().getCredit() + "\n " +
			 * getString(R.string.studio_xp));
			 */
		} else {
			userCreditTxt.setVisibility(View.GONE);
			creditPB.setVisibility(View.GONE);
		}

		// get and set the photo points
		final TextView photoPoints = (TextView) dialog
				.findViewById(R.id.photo_points_txt);

		// define the view pager
		photosPager = (ViewPager) dialog.findViewById(R.id.photo_pager_id);
		// define the pervious btn
		ImageButton prevBtn = (ImageButton) dialog
				.findViewById(R.id.photos_previous_img_id);
		// define the next btn
		ImageButton nextBtn = (ImageButton) dialog
				.findViewById(R.id.photos_next_img_id);
		// define the user button
		final Button useBtn = (Button) dialog
				.findViewById(R.id.photos_use_btn_id);

		// setting adapter of the pager
		photospagerAdapter = new PhotoPagerAdapter();
		photosPager.setAdapter(photospagerAdapter);
		// set the on pagescroll listener to detect the last swiped image
		OnPageChangeListener mListener = new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// check if standing at the last item , then call web service to
				// get more pictures
				photoPoints.setText(photos.get(photosPager.getCurrentItem())
						.getRequiredPoints()
						+ " "
						+ getString(R.string.studio_xp));
				// get the selected photo
				StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				if (canUsePhoto(selectedPhoto) == USE) // USE
				{
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Use));
					useBtn.setTag(USE);
				} else {
					if (canUsePhoto(selectedPhoto) == UPGRADE_TO_PREMIUM)
						useBtn.setTag(UPGRADE_TO_PREMIUM);
					else if (canUsePhoto(selectedPhoto) == BUY_POINTS)
						useBtn.setTag(BUY_POINTS);

					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
				}

				// if (photosPager.getCurrentItem() == photos.size() - 1
				// && !gettinMoreData && pageNo != -1) {
				// gettinMoreData = true;
				// if (folder != null)
				// StudioManager.getInstance().getStudioPhotos(
				// photos.get(photosPager.getCurrentItem())
				// .getFolderId(), pageNo);
				// else
				// StudioManager.getInstance().getPurchasedPhotos(pageNo);
				// }
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		};
		photosPager.setOnPageChangeListener(mListener);

		// set the position of pager with the selected item
		photosPager.setCurrentItem(Selected_Position);

		// setting the click listener of previous button
		prevBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				photoPoints.setText(photos.get(photosPager.getCurrentItem())
						.getRequiredPoints()
						+ " "
						+ getString(R.string.studio_xp));

				// get the selected photo
				StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				if (canUsePhoto(selectedPhoto) == USE) // USE
				{
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Use));
					useBtn.setTag(USE);
				} else if (canUsePhoto(selectedPhoto) == UPGRADE_TO_PREMIUM) {
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
					useBtn.setTag(UPGRADE_TO_PREMIUM);
				} else if (canUsePhoto(selectedPhoto) == BUY_POINTS) {
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
					useBtn.setTag(BUY_POINTS);
				}
				photosPager.setCurrentItem(photosPager.getCurrentItem() - 1,
						true);
				// Toast.makeText(getActivity(),
				// photosPager.getCurrentItem()-1+"",
				// Toast.LENGTH_SHORT).show();

			}
		});
		// setting the click listener of next button
		nextBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				photoPoints.setText(photos.get(photosPager.getCurrentItem())
						.getRequiredPoints()
						+ " "
						+ getString(R.string.studio_xp));

				// get the selected photo
				StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				if (canUsePhoto(selectedPhoto) == USE) // USE
				{
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Use));
					useBtn.setTag(USE);
				} else if (canUsePhoto(selectedPhoto) == UPGRADE_TO_PREMIUM) {
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
					useBtn.setTag(UPGRADE_TO_PREMIUM);
				} else if (canUsePhoto(selectedPhoto) == BUY_POINTS) {
					useBtn.setText(getResources().getString(
							R.string.PhotoDetails_Unlock));
					useBtn.setTag(BUY_POINTS);
				}

				photosPager.setCurrentItem(photosPager.getCurrentItem() + 1,
						true);
				// check if standing at the last item , then call web service to
				// get more pictures
				if (photosPager.getCurrentItem() == photos.size() - 1
						&& !gettinMoreData && pageNo != -1) {
					gettinMoreData = true;
					StudioManager.getInstance().getStudioPhotos(0, pageNo,
							filterBy, seasons);
				}

			}
		});

		// setting the listener of Use button
		useBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final StudioPhoto selectedPhoto = photos.get(photosPager
						.getCurrentItem());
				if (UserManager.getInstance().getCurrentUserId() != 0) {

					if (Integer.parseInt(UserCredit) >= selectedPhoto
							.getRequiredPoints()
							|| selectedPhoto.getIsPurchased() == 1) {

						// TODO first check user , unlock ! :)

						if ((Integer) useBtn.getTag() == USE) {
							// get the selected photo

							// get the bitmap of the image

							App.getInstance()
									.getImageLoader()
									.loadImage(selectedPhoto.getPhotoURL(),
											new SimpleImageLoadingListener() {

												@Override
												public void onLoadingComplete(
														String imageUri,
														View view,
														Bitmap loadedImage) {
													// save the Stream to device
													super.onLoadingComplete(
															imageUri, view,
															loadedImage);
													ByteArrayOutputStream stream = new ByteArrayOutputStream();
													loadedImage
															.compress(
																	Bitmap.CompressFormat.PNG,
																	60, stream);

													try {
														boolean single = false;
														if (selectedPhoto
																.getCaptureType() == StudioPhoto.PHOTO_CAPTURE_TYPE_MASK_SHOT) // 1
																																// -->
																																// rotatable
																																// a5er
																																// klam
																																// isA
																																// :D
															single = true;
														else if (selectedPhoto
																.getCaptureType() == StudioPhoto.PHOTO_CAPTURE_TYPE_SCREEN_SHOT) // 2
																																	// --
																																	// not
																																	// rotatable
															single = false;

														String path = saveToInternalSorage(loadedImage);
														startActivity(CaptureActivity
																.getActivityIntent(
																		getActivity(),
																		path,
																		selectedPhoto,
																		single));
														// selectedPhoto.getFolderId()
														// ==
														// StudioFolder.FOLDER_TYPE_PLAYER
														// ? true: false

														// TODO call update user
														// credit
														// .. to be discussed
														// with Hamed
														// !
														// TODO call purchase
														// photo WB
														if (selectedPhoto
																.getIsPurchased() != 1
																&& doesUserHasEnoughPoints(
																		UserManager
																				.getInstance()
																				.getCurrentUser(),
																		selectedPhoto) == USE
																&& selectedPhoto
																		.getRequiredPoints() > 0) {
															StudioManager
																	.getInstance()
																	.purchasePhoto(
																			selectedPhoto
																					.getStudioPhotoId());
															StudioPhotosTable
																	.getInstance()
																	.setPurchased(
																			selectedPhoto
																					.getStudioPhotoId());
														}

													}

													catch (Exception e) {
													}
												}
											});

							// if(selectedPhoto.getIsPurchased() == 0)
							// /** --- Added by Turki --- **/
							// StudioManager.getInstance().updateUserCredit(
							// -1 * selectedPhoto.getRequiredPoints());
							/** --- --- **/
						} else if ((Integer) useBtn.getTag() == UPGRADE_TO_PREMIUM) // UNLOCK
																					// (Upgrade
																					// To
																					// Premium)
						{
							/*
							 * UIUtils.showToast(getActivity(),
							 * "navigate to shop to upgrade to premium");
							 */
							startActivity(ShopActivityTabs
									.getIntent(getActivity(),
											ShopActivityTabs.PREMIUM_TAB));

							// TODO Redirect to Shop Screen
							/*
							 * startActivityForResult( new Intent(getActivity(),
							 * PremiumPackagesActivity.class).putExtra(
							 * Params.Studio.FOLDER_ID,
							 * folder.getFolderId()).putExtra(
							 * Params.Studio.STUDIO_PHOTO_ID,
							 * selectedPhoto.getStudioPhotoId()),
							 * STUDIO_PHOTOS_INTENT_REQUEST_CODE);
							 */
						} else if ((Integer) useBtn.getTag() == BUY_POINTS) // UNLOCK
																			// (Buy
																			// Points)
						{
							/*
							 * UIUtils.showToast(getActivity(),
							 * "navigate to shop to buy points");
							 */
							startActivity(ShopActivityTabs.getIntent(
									getActivity(),
									ShopActivityTabs.POINTS_PACKAGES));

							// TODO Redirect to Shop Screen
							/*
							 * startActivityForResult( ShopActivity
							 * .getIntent(getActivity(), false)
							 * .putExtra(Params.Studio.FOLDER_ID,
							 * folder.getFolderId())
							 * .putExtra(Params.Studio.STUDIO_PHOTO_ID,
							 * selectedPhoto.getStudioPhotoId()),
							 * STUDIO_PHOTOS_INTENT_REQUEST_CODE);
							 */

						}
					} else {
						startActivity(ShopActivityTabs.getIntent(getActivity(),
								ShopActivityTabs.POINTS_PACKAGES));
					}
				} /*
				 * else { if (folder != null)
				 * getActivity().startActivityForResult(
				 * LogInActivity.getActivityIntent(getActivity(),
				 * UserManager.LOGIN_FOR_RESULT, new Intent().putExtra(
				 * Params.Studio.FOLDER_ID, folder.getFolderId()) //
				 * .putExtra(Params.Studio.STUDIO_PHOTO_ID, //
				 * selectedPhoto.getStudioPhotoId()) ), ACTION_REQUEST_LOGIN); }
				 */
			}
		});

		// dismiss the dialog button
		ImageButton CloseBtn = (ImageButton) dialog
				.findViewById(R.id.photos_close_img_id);
		CloseBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

	}

	// save to internal storage
	private String saveToInternalSorage(Bitmap bitmapImage) {
		ContextWrapper cw = new ContextWrapper(getActivity());
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		// Create imageDir
		File mypath = new File(directory, "image.jpg");

		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(mypath);

			// Use the compress method on the BitMap object to write image to
			// the OutputStream
			bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return directory.getAbsolutePath();
	}

	private class PhotoPagerAdapter extends PagerAdapter {

		// ArrayList<StudioPhoto> images;
		private LayoutInflater inflater;

		PhotoPagerAdapter() {
			// this.images = images;
			inflater = getActivity().getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return photos.size();
		}

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			View imageLayout = null;

			if (imageLayout == null) {
				imageLayout = inflater.inflate(R.layout.photo_popup_item, view,
						false);
			}
			final ProgressBar bar = (ProgressBar) imageLayout
					.findViewById(R.id.progressBar1);
			imageView = (ImageView) imageLayout.findViewById(R.id.imageView1);
			App.getInstance()
					.getImageLoader()
					.displayImage(photos.get(position).getPhotoURL(),
							imageView, App.getInstance().getDisplayOption(),
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingStarted(String imageUri,
										View view) {
									bar.setVisibility(View.VISIBLE);
									System.out.println("Loading");
								}

								@Override
								public void onLoadingFailed(String imageUri,
										View view, FailReason failReason) {
									String message = null;
									switch (failReason.getType()) {
									case IO_ERROR:
										message = "Input/Output error";
										break;
									case DECODING_ERROR:
										message = "Image can't be decoded";
										break;
									case NETWORK_DENIED:
										message = "Downloads are denied";
										break;
									case OUT_OF_MEMORY:
										message = "Out Of Memory error";
										break;
									case UNKNOWN:
										message = "Unknown error";
										break;
									}
									Toast.makeText(
											getActivity()
													.getApplicationContext(),
											message, Toast.LENGTH_SHORT).show();

									bar.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingComplete(String imageUri,
										View view, Bitmap loadedImage) {
									bar.setVisibility(View.GONE);
								}
							});

			view.addView(imageLayout);

			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		StudioManager.getInstance().addListener(this);
		UserManager.getInstance().addListener(this);
		if (UserManager.getInstance().getCurrentUserId() != 0)
			UserManager.getInstance().getUserCredit();
	}

	@Override
	public void onPause() {
		super.onPause();
		StudioManager.getInstance().removeListener(this);
		UserManager.getInstance().removeListener(this);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if (scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
			userScrolled = true;
		}

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		int lastInScreen = firstVisibleItem + visibleItemCount;
		if ((lastInScreen == photos.size()) && userScrolled && pageNo != -1) {
			if (!gettinMoreData) {
				gettinMoreData = true;
				showLoadingDialog();
				StudioManager.getInstance().getStudioPhotos(0, pageNo,
						filterBy, seasons);
			}
		}

	}

	@Override
	public void onSuccess(List<StudioPhoto> objs) {

		hideLoadingDialog();
		gettinMoreData = false;
		if (objs != null && !objs.isEmpty()) {
			this.photos.addAll(objs);
			adapter.notifyDataSetChanged();
			if (photospagerAdapter != null)
				photospagerAdapter.notifyDataSetChanged();

			// check if there is no more data set page no to -1 , else if there
			// was more data in server increment the page no
			if (objs.size() < 24)
				pageNo = -1;
			else
				pageNo++;
		} else {
			noResultsTxt.setVisibility(View.VISIBLE);
			noResultsImg.setVisibility(View.VISIBLE);
			photosResultGV.setVisibility(View.GONE);
		}

	}

	@Override
	public void onException(AppException ex) {
		gettinMoreData = false;
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
		pageNo = -1;
	}

	@Override
	public void onSuccess(String obj) {
		this.UserCredit = obj;
		if (creditPB != null) {
			creditPB.setVisibility(View.GONE);

			if (userCreditTxt != null) {
				if (this.UserCredit != null)
					userCreditTxt
							.setText(getString(R.string.photoUse_unock_credit)
									+ " " + this.UserCredit + " "
									+ getString(R.string.studio_xp));
				else
					userCreditTxt
							.setText(getString(R.string.photoUse_unock_credit)
									+ " "
									+ UserManager.getInstance()
											.getCurrentUser().getCredit() + " "
									+ getString(R.string.studio_xp));
			}
		}
	}
}
