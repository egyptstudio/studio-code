package com.tawasol.fcbBarcelona.ui.fragments;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost.TabSpec;

public class FriendListFragment extends Fragment {

	private FragmentTabHost mTabHost;
	private TabWidget widget;
	private HorizontalScrollView hs;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_friend_list,
				container, false);

		initView(rootView);

		return rootView;
	}

	private void initView(View rootView) {
		FragmentInfo info;

		mTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);

		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// TabWidget tw = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// LinearLayout ll = (LinearLayout) tw.getParent();
		hs = (HorizontalScrollView) rootView
				.findViewById(R.id.horizontalScrollView);
		hs.setFillViewport(false);

		mTabHost.setup(getActivity(), getChildFragmentManager(),
				android.R.id.tabcontent);
		mTabHost.getTabWidget().setStripEnabled(false);

		Bundle bundle = new Bundle();

		mTabHost.addTab(

				mTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Messages_Friends)).setIndicator(
						getTabIndicator(
								mTabHost.getContext(),
								getActivity().getResources().getString(
										R.string.Messages_Friends),
								android.R.drawable.star_on)),
				FriendsFragment.class, bundle);

		// mTabHost.addTab(
		// setIndicator(mTabHost.newTabSpec(getActivity().getResources()
		// .getString(R.string.Messages_Friends)), R.drawable.chat_left_tab),
		// FriendsFragment.class, bundle);

		mTabHost.addTab(

				mTabHost.newTabSpec(
						getActivity().getResources()
								.getString(R.string.request)).setIndicator(
						getTabIndicator(mTabHost.getContext(), getActivity()
								.getResources().getString(R.string.request),
								android.R.drawable.star_on)),
				FriendRequestFragment.class, bundle);

		// mTabHost.addTab(
		// setIndicator(mTabHost.newTabSpec(getActivity().getResources()
		// .getString(R.string.request)),
		// R.drawable.chat_center_tab), FriendRequestFragment.class,
		// bundle);

		mTabHost.addTab(

				mTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.suggestion)).setIndicator(
						getTabIndicator(mTabHost.getContext(), getActivity()
								.getResources().getString(R.string.suggestion),
								android.R.drawable.star_on)),
				SuggestionsFragment.class, bundle);

		// mTabHost.addTab(
		// setIndicator(mTabHost.newTabSpec(getActivity().getResources()
		// .getString(R.string.suggestion)),
		// R.drawable.chat_right_tab), SuggestionsFragment.class,
		// bundle);

		// mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
		//
		// @Override
		// public void onTabChanged(String tabId) {
		// for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
		// TextView tv = (TextView) mTabHost.getTabWidget()
		// .getChildAt(i).findViewById(R.id.tab_text);
		// tv.setTextColor(getActivity().getResources().getColor(
		// R.color.new_yellow));
		// }
		// TextView tv = (TextView) mTabHost.getTabWidget()
		// .getChildAt(mTabHost.getCurrentTab())
		// .findViewById(R.id.tab_text);
		// tv.setTextColor(Color.WHITE);
		// }
		// });
		ViewTreeObserver observer = widget.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@SuppressLint("NewApi")
			@Override
			public void onGlobalLayout() {

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					widget.getViewTreeObserver().removeOnGlobalLayoutListener(
							this);
				} else {
					widget.getViewTreeObserver().removeGlobalOnLayoutListener(
							this);
				}

				Display display = getActivity().getWindowManager()
						.getDefaultDisplay();
				int width = display.getWidth();
				if (widget.getWidth() < width) {
					FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.MATCH_PARENT);
					params.gravity = Gravity.CENTER_HORIZONTAL;

					widget.setLayoutParams(params);
					hs.setFillViewport(true);
				}
			}
		});

		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
					TextView tv = (TextView) mTabHost.getTabWidget()
							.getChildAt(i).findViewById(R.id.textView);
					// tv.setTextColor(getActivity().getResources().getColor(
					// R.color.new_yellow));
					tv.setTypeface(Typeface.DEFAULT);
				}
				TextView tv = (TextView) mTabHost.getTabWidget()
						.getChildAt(mTabHost.getCurrentTab())
						.findViewById(R.id.textView);
				// tv.setTextColor(Color.WHITE);
				tv.setTypeface(Typeface.DEFAULT_BOLD);
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		// for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
		// TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i)
		// .findViewById(R.id.tab_text);
		// tv.setTextColor(getActivity().getResources().getColor(
		// R.color.new_yellow));
		// }
		// TextView tv = (TextView) mTabHost.getTabWidget()
		// .getChildAt(mTabHost.getCurrentTab())
		// .findViewById(R.id.tab_text);
		// tv.setTextColor(Color.WHITE);

		for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.textView);
			// tv.setTextColor(getActivity().getResources().getColor(
			// R.color.new_yellow));
			tv.setTypeface(Typeface.DEFAULT);
		}
		TextView tv = (TextView) mTabHost.getTabWidget()
				.getChildAt(mTabHost.getCurrentTab())
				.findViewById(R.id.textView);
		// tv.setTextColor(Color.WHITE);
		tv.setTypeface(Typeface.DEFAULT_BOLD);
	}

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid) {
		// TODO Auto-generated method stub
		View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_host_text, null);
		v.setBackgroundResource(resid);

		final TextView text = (TextView) v.findViewById(R.id.tab_text);
		text.setText(spec.getTag());
		text.setTextColor(getActivity().getResources().getColor(
				android.R.color.white));
		text.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mTabHost.getCurrentTabTag().equalsIgnoreCase(
						text.getText().toString())) {
					Fragment chat = getChildFragmentManager()
							.findFragmentByTag(text.getText().toString());
					if (chat instanceof RecentFragment) {

					} else if (chat instanceof FriendsFragment) {

					} else if (chat instanceof ContactsFragment) {

					}
				} else {
					mTabHost.setCurrentTabByTag(text.getText().toString());
				}
			}
		});
		return spec.setIndicator(v);
	}

	private View getTabIndicator(Context context, String title, int icon) {
		final View view = LayoutInflater.from(context).inflate(
				R.layout.tab_layout, null);

		// ImageView iv = (ImageView) view.findViewById(R.id.imageView);
		// iv.setImageResource(icon);
		final TextView tv = (TextView) view.findViewById(R.id.textView);
		tv.setText(title);
		return view;
	}
}
