package com.tawasol.fcbBarcelona.ui.fragments;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.NotificationsTable;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.managers.FanManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.ContestActivity;
import com.tawasol.fcbBarcelona.ui.activities.Edit_UserProfile_Activity;
import com.tawasol.fcbBarcelona.ui.activities.FansGridActivity;
import com.tawasol.fcbBarcelona.ui.activities.FriendsList;
import com.tawasol.fcbBarcelona.ui.activities.HomeActivity;
import com.tawasol.fcbBarcelona.ui.activities.InfoActivity;
import com.tawasol.fcbBarcelona.ui.activities.NotificationsActivity;
import com.tawasol.fcbBarcelona.ui.activities.SettingsActivity;
import com.tawasol.fcbBarcelona.ui.activities.SponsersActivity;
import com.tawasol.fcbBarcelona.ui.activities.UserProfileActivity;
import com.tawasol.fcbBarcelona.utils.ValidatorUtils;

/**
 * 
 * @author Basyouni
 * 
 */
public class MenuFragment extends BaseFragment implements OnClickListener {
	private static final int Following = 1;
	private static final int Followers = 2;

	User user;
	ProgressBar percentagePB, profilePicPB;
	ImageButton settingBtn/* , rightArrowBtn */, infoBtn;
	RelativeLayout notificationBtn, studioBtn, fansBtn, contestBtn, friendsBtn,
			sponsersBtn;
	ImageView userImg;
	TextView userNameTxt, percentageTxt, pointsBalanceTxt, statusTxt;
	View followingView, followersView;
	TextView notifcationNumberTxt;
	// TextView
	// followingFirstDigit,followingSecondDigit,followingThirdDigit,followingFourthDigit;
	// TextView
	// followersFirstDigit,followersSecondDigit,followersThirdDigit,followersFourthDigit;
	RelativeLayout userRelativeLayout;
	RelativeLayout followingRelativeLayout, followersRelativeLayout;
	LinearLayout percentageLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.menu_profile_fragment,
				container, false);

		/* find the controls of the XML */
		findViewByID(rootView);

		/* get the user object info */
		initUser();

		return rootView;
	}

	/** @author Mohga */
	/*******************/
	/** get the Controls from XML file */
	private void findViewByID(View rootView) {
		percentagePB = (ProgressBar) rootView
				.findViewById(R.id.menu_progressbar);
		profilePicPB = (ProgressBar) rootView
				.findViewById(R.id.menu_progressBar);
		pointsBalanceTxt = (TextView) rootView
				.findViewById(R.id.menu_points_balance_txt);
		percentageLayout = (LinearLayout) rootView
				.findViewById(R.id.profilePercentage_LinearLayout);
		settingBtn = (ImageButton) rootView.findViewById(R.id.menu_setting_img);
		infoBtn = (ImageButton) rootView.findViewById(R.id.menu_info_img);
		notificationBtn = (RelativeLayout) rootView
				.findViewById(R.id.menu_notification_img);
		/*
		 * rightArrowBtn = (ImageButton) rootView
		 * .findViewById(R.id.right_arrow_Img);
		 */
		studioBtn = (RelativeLayout) rootView.findViewById(R.id.menu_fcb_btn);
		fansBtn = (RelativeLayout) rootView.findViewById(R.id.menu_fans_btn);
		contestBtn = (RelativeLayout) rootView
				.findViewById(R.id.menu_contest_btn);
		friendsBtn = (RelativeLayout) rootView
				.findViewById(R.id.menu_friends_btn);
		sponsersBtn = (RelativeLayout) rootView
				.findViewById(R.id.menu_sponsers_btn);
		userImg = (ImageView) rootView.findViewById(R.id.menu_profile_img_id);

		/* set the initial drawable of masked image view */
		// userImg.setImageDrawable((getResources()
		// .getDrawable(R.drawable.image_sample)));

		userNameTxt = (TextView) rootView.findViewById(R.id.menu_user_name_txt);
		percentageTxt = (TextView) rootView
				.findViewById(R.id.menu_progress_percentage_txt);
		// followingView = (View)rootView.findViewById(R.id.following_btn_view);
		// followersView = (View)rootView.findViewById(R.id.followers_btn_view);
		userRelativeLayout = (RelativeLayout) rootView
				.findViewById(R.id.secondRow_LinearLayout);
		followingRelativeLayout = (RelativeLayout) rootView
				.findViewById(R.id.profile_following_rel_layout);
		followersRelativeLayout = (RelativeLayout) rootView
				.findViewById(R.id.profile_followers_rel_layout);

		notifcationNumberTxt = (TextView) rootView
				.findViewById(R.id.notification_num_txt_id);
		statusTxt = (TextView) rootView.findViewById(R.id.menu_status_txt);
		// getFollowingViewByID(followingView);
		// getFollowerViewByID(followersView);
	}

	@Override
	public void onResume() {
		super.onResume();
		/* set the info in controls */
		initViewControls();

	}

	/** initialize the controls of following button layout */
	/*
	 * private void getFollowingViewByID(View view) { followingFirstDigit =
	 * (TextView)view.findViewById(R.id.first_digit_following_txt);
	 * followingSecondDigit =
	 * (TextView)view.findViewById(R.id.second_digit_following_txt);
	 * followingThirdDigit =
	 * (TextView)view.findViewById(R.id.third_digit_following_txt);
	 * followingFourthDigit =
	 * (TextView)view.findViewById(R.id.fourth_digit_following_txt); }
	 *//** initialize the controls of followers button layout */
	/*
	 * private void getFollowerViewByID (View view){ followersFirstDigit =
	 * (TextView)view.findViewById(R.id.first_digit_followers_txt);
	 * followersSecondDigit =
	 * (TextView)view.findViewById(R.id.second_digit_followers_txt);
	 * followersThirdDigit =
	 * (TextView)view.findViewById(R.id.third_digit_followers_txt);
	 * followersFourthDigit =
	 * (TextView)view.findViewById(R.id.fourth_digit_followers_txt); }
	 */

	/**
	 * set the numbers individually in the views according to the type
	 * "Following or Followers"
	 * 
	 * @param Id
	 *            that indicates the type
	 * @param Number
	 *            that will be set according to the type in the right controls !
	 * */
	void SetNumbers(int Id, int Number) {

		if (Id == Following) {

			for (char num : (Number + "").toCharArray()) {

				TextView followingTxt = new TextView(getActivity());
				followingTxt.setBackgroundResource(R.drawable.follow_counter);
				followingTxt.setText(num + "");
				followingTxt.setGravity(Gravity.CENTER);
				// following.setTextSize(TypedValue.COMPLEX_UNIT_SP,
				// getResources().getDimension(R.dimen.smallTextSize));

				((LinearLayout) followingRelativeLayout
						.findViewById(R.id.profile_following_Layout))
						.addView(followingTxt);

			}

		} else if (Id == Followers) {

			for (char num : (Number + "").toCharArray()) {

				TextView followersTxt = new TextView(getActivity());
				followersTxt.setText(num + "");
				followersTxt.setBackgroundResource(R.drawable.follow_counter);
				followersTxt.setGravity(Gravity.CENTER);

				((LinearLayout) followersRelativeLayout
						.findViewById(R.id.profile_followers_Layout))
						.addView(followersTxt);

			}

		}

		/*
		 * if(Id == Following) {
		 * followingFirstDigit.setText(Number.charAt(0)+"");
		 * followingSecondDigit.setText(Number.charAt(1)+"");
		 * followingThirdDigit.setText(Number.charAt(2)+"");
		 * followingFourthDigit.setText(Number.charAt(3)+""); } else if(Id ==
		 * Followers) { followersFirstDigit.setText(Number.charAt(0)+"");
		 * followersSecondDigit.setText(Number.charAt(1)+"");
		 * followersThirdDigit.setText(Number.charAt(2)+"");
		 * followersFourthDigit.setText(Number.charAt(3)+"");
		 * 
		 * }
		 */

	}

	/**
	 * Initialize the user variable using the UserManger to get the current
	 * logged in user
	 */
	private void initUser() {
		this.user = UserManager.getInstance().getCurrentUser();
	}

	/** Initialize the controls of the view */
	private void initViewControls() {
		// Loading Font Face
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/points_font.ttf");
		// Applying font
		pointsBalanceTxt.setTypeface(tf);

		// set the points balance text
		pointsBalanceTxt.setText(user.getCredit() + " XP ");

		// check if view complete and incomplete
		handleComplete_InComplete_Profile();

		// set the user name
		userNameTxt.setText(user.getFullName());

		// set the status text
		if (!ValidatorUtils.isRequired(user.getStatus()))
			statusTxt.setText(user.getStatus());
		else
			statusTxt.setText("");

		// set the following and followers number
		SetNumbers(Following, user.getFollowingCount()/*
													 * String.format("%04d",
													 * user.getFollowingCount())
													 */);
		SetNumbers(Followers, user.getFollowersCount()/*
													 * String.format("%04d",
													 * user.getFollowersCount())
													 */);

		// set the notifications number
		int notificationsCount = NotificationsTable.getInstance()
				.getUnreadNotificationsCount();
		System.out.println("Notif Count " + notificationsCount);
		if (notificationsCount != 0 && notificationsCount != -1) {
			notifcationNumberTxt.setText(String.valueOf(notificationsCount));
			notifcationNumberTxt.setVisibility(View.VISIBLE);
		} else
			notifcationNumberTxt.setVisibility(View.GONE);

		// set the user image
		// App.getInstance().getImageLoader().displayImage(user.getProfilePic(),
		// userImg);
		DisplayImage();
		/*
		 * if (NetworkingUtils.isNetworkConnected(getActivity())) {
		 * DownloadImagesTask task = new DownloadImagesTask(); task.execute(); }
		 * else UIUtils.showToast(getActivity(),
		 * getResources().getString(R.string.connection_noConnection));
		 */

		// set the on click listener of the buttons
		// followingView.setOnClickListener(this);
		// followersView.setOnClickListener(this);
		followersRelativeLayout.setOnClickListener(this);
		followingRelativeLayout.setOnClickListener(this);
		studioBtn.setOnClickListener(this);
		fansBtn.setOnClickListener(this);
		contestBtn.setOnClickListener(this);
		friendsBtn.setOnClickListener(this);
		sponsersBtn.setOnClickListener(this);
		userRelativeLayout.setOnClickListener(this);
		percentageLayout.setOnClickListener(this);
		infoBtn.setOnClickListener(this);
		settingBtn.setOnClickListener(this);
		notificationBtn.setOnClickListener(this);
	}

	/** Handle Complete and InComplete Profile View */
	private void handleComplete_InComplete_Profile() {
		double accuratePercentage = (UserManager.getInstance()
				.calculateProfilePercentage() / 17.0) * 100;

		if (accuratePercentage == 100.0)
			percentageLayout.setVisibility(View.INVISIBLE);
		else {
			int profilePercentage = (int) (Math.round(accuratePercentage));
			percentagePB.setProgress(profilePercentage);
			percentageTxt.setText(profilePercentage + " %");
		}
	}

	/** Download the bitmap of the image URL */
	public class DownloadImagesTask extends AsyncTask<Void, Void, Void> {
		Bitmap bmp = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			profilePicPB.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(Void result) {
			profilePicPB.setVisibility(View.GONE);
			if (bmp != null) {
				userImg.setImageBitmap(bmp);
			} else
				DisplayImage();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				URL ulrn = new URL(user.getProfilePic());
				HttpURLConnection con = (HttpURLConnection) ulrn
						.openConnection();
				InputStream is = con.getInputStream();
				try {
					bmp = BitmapFactory.decodeStream(is);
				} catch (OutOfMemoryError e) {
				}
			} catch (Exception e) {
			}

			return null;
		}

	}

	private void DisplayImage() {
		DisplayImageOptions displayOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.ic_launcher)
				.cacheInMemory(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.considerExifParams(true)
				.displayer(
						new RoundedBitmapDisplayer((int) getResources()
								.getDimension(R.dimen.button_padding))).build();
		App.getInstance()
				.getImageLoader()
				.displayImage(user.getProfilePic(), userImg, displayOption,
						new ImageLoadingListener() {

							@Override
							public void onLoadingStarted(String arg0, View arg1) {
								profilePicPB.setVisibility(View.VISIBLE);
							}

							@Override
							public void onLoadingFailed(String arg0, View arg1,
									FailReason arg2) {
								profilePicPB.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingComplete(String arg0,
									View arg1, Bitmap arg2) {
								profilePicPB.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingCancelled(String arg0,
									View arg1) {

							}
						});

	}

	/** handle the on click listener of the fragment */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.profile_following_rel_layout:
			if (UserManager.getInstance().getCurrentUser().getFollowingCount() != 0)
				startActivity(FansGridActivity.getIntent(getActivity(),
						FansGridActivity.LOADFOLLOWING, UserManager
								.getInstance().getCurrentUser().getUserId()));
			else
				startActivity(FansGridActivity.getIntent(getActivity(),
						FansGridActivity.LOADFOLLOWINGSuggestions, UserManager
								.getInstance().getCurrentUser().getUserId()));
			// UIUtils.showToast(getActivity(),
			// getResources().getString(R.string.no_following));
			break;
		case R.id.profile_followers_rel_layout:
			// if(UserManager.getInstance().getCurrentUser().getFollowersCount()
			// != 0)
			startActivity(FansGridActivity.getIntent(getActivity(),
					FansGridActivity.LOADFOLLOWERS, UserManager.getInstance()
							.getCurrentUser().getUserId()));
			// else
			// UIUtils.showToast(getActivity(),
			// getResources().getString(R.string.FollowersInitalState_no_followers));
			break;
		case R.id.menu_fcb_btn:
			startActivity(HomeActivity.getActivityIntent(getActivity()));
			break;
		case R.id.menu_fans_btn:
			FanManager.getInstance().clearFans();
			startActivity(FansGridActivity.getIntent(getActivity(),
					FansGridActivity.LOADFANS).setFlags(
					Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
		case R.id.menu_contest_btn:
			startActivity(ContestActivity.getActivityIntent(getActivity())
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
		case R.id.menu_friends_btn:
			startActivity(FriendsList.getActivityIntent(getActivity())
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
		case R.id.menu_sponsers_btn:
			startActivity(new Intent(getActivity(), SponsersActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
		/*
		 * case R.id.right_arrow_Img:
		 * startActivity(UserProfileActivity.getActivityIntent(getActivity()));
		 * break;
		 */
		case R.id.secondRow_LinearLayout:
			startActivity(UserProfileActivity.getActivityIntent(getActivity()));
			break;
		case R.id.profilePercentage_LinearLayout:
			startActivity(Edit_UserProfile_Activity.getActivityIntent(
					getActivity(), false));
			break;
		/** Added by Turki **/
		case R.id.menu_setting_img:
			startActivity(SettingsActivity.getActivityIntent(getActivity())
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
		case R.id.menu_info_img:
			startActivity(InfoActivity.getActivityIntent(getActivity())
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
		case R.id.menu_notification_img:
			startActivity(NotificationsActivity
					.getActivityIntent(getActivity()));
			break;
		default:
			break;
		}

	}

}
