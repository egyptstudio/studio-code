package com.tawasol.fcbBarcelona.ui.fragments;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.vending.billing.util.BarcaIabHelper;
import com.android.vending.billing.util.BarcaIabHelper.OnIabPurchaseFinishedListener;
import com.android.vending.billing.util.BarcaIabHelper.OnIabSetupFinishedListener;
import com.android.vending.billing.util.BarcaIabResult;
import com.android.vending.billing.util.BarcaPurchase;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefsHelper;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.entities.InAppPointsEntity;
import com.tawasol.fcbBarcelona.entities.PremiumStateObject;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnPremiumStateRecieved;
import com.tawasol.fcbBarcelona.managers.ShopManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivityTabs;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class PremiumPackageFragment extends Fragment implements
		OnIabSetupFinishedListener, OnIabPurchaseFinishedListener,
		OnPremiumStateRecieved {

	private BarcaIabHelper billingHelper;
	private static final String PURCHASE_PAYLOAD_CACHE_KEY = "com.tawasol.Barchelona.PAYLOAD2";
	public static final String RESPONSE_PRODUCT_ID = "productId";
	public static final String RESPONSE_PAYLOAD = "developerPayload";
	public static boolean shopAvailable;
	public static final int loginRequestCode = 1549;

	GridView premiumGrid;
	private ProgressBar pointsProgress;
	private TextView offline;
	private Handler customHandler;
	private PremiumAdapter adapter;
	ProgressDialog dialog;
	private TextView subscribeTxt;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.premium_package_grid,
				container, false);
		subscribeTxt = (TextView) rootView.findViewById(R.id.subscribe);
		premiumGrid = (GridView) rootView.findViewById(R.id.premium_grid);
		pointsProgress = (ProgressBar) rootView
				.findViewById(R.id.pointsPackagesProgress);
		offline = (TextView) rootView.findViewById(R.id.offline);
		dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			billingHelper = new BarcaIabHelper(getActivity(),
					Params.Common.BASE_64_KEY);
			billingHelper.startSetup(this);
			customHandler = new Handler();
			customHandler.postDelayed(checkData, 0);
		} else {
			offline.setVisibility(View.VISIBLE);
		}
		return rootView;
	}

	private Runnable checkData = new Runnable() {

		@Override
		public void run() {
			if (ShopManager.getInstance().isDataAvailableException()) {
				pointsProgress.setVisibility(View.GONE);
				UIUtils.showToast(getActivity(), ShopManager.getInstance()
						.dataRetrievalException());
			} else {
				if (!ShopManager.getInstance().isDataAvailable()) {
					pointsProgress.setVisibility(View.VISIBLE);
					customHandler.postDelayed(this, 1000);
				} else {
					pointsProgress.setVisibility(View.GONE);
					setAdapter();			
				}
			}
		}
	};

	protected void setAdapter() {
		List<InAppPointsEntity> premium = ShopManager.getInstance()
				.getPremium();
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			ShopManager.getInstance().MyPremiumStatus(UserManager.getInstance().getCurrentUserId(),
					premium.get(0).getInAppId());
		}
		if (premium != null) {
			adapter = new PremiumAdapter(premium);
			premiumGrid.setAdapter(adapter);
		}
	}

	private class PremiumAdapter extends BaseAdapter {
		List<InAppPointsEntity> premium;

		public PremiumAdapter(List<InAppPointsEntity> premium) {
			this.premium = premium;
		}

		List<InAppPointsEntity> getList() {
			return this.premium;
		}

		@Override
		public int getCount() {
			return premium.size();
		}

		@Override
		public Object getItem(int position) {
			return premium.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View rootView = convertView;
			if (rootView == null) {
				rootView = LayoutInflater.from(
						App.getInstance().getApplicationContext()).inflate(
						R.layout.premium_package_item, parent, false);
			}
			final InAppPointsEntity premium = this.premium.get(position);
			
			RelativeLayout PackageContainer = (RelativeLayout) rootView
					.findViewById(R.id.shop_item_container);
			TextView title = (TextView) rootView.findViewById(R.id.title);
			TextView price = (TextView) rootView.findViewById(R.id.price);
			TextView packageDuration = (TextView) rootView
					.findViewById(R.id.packageDuration);
			TextView packageDescription = (TextView) rootView
					.findViewById(R.id.packageDescription);

			if (position == 0) {
				PackageContainer
						.setBackgroundResource(R.drawable.full_basic_pk);
				title.setText(getActivity().getResources().getString(
						R.string.basic));
			} else if (position == 1) {
				PackageContainer
						.setBackgroundResource(R.drawable.full_bronze_pk);
				title.setText(getActivity().getResources().getString(
						R.string.bronze));
			} else if (position == 2) {
				PackageContainer
						.setBackgroundResource(R.drawable.full_silver_pk);
				title.setText(getActivity().getResources().getString(
						R.string.silver));
			} else if (position == 3) {
				PackageContainer
						.setBackgroundResource(R.drawable.full_golden_pk);
				title.setText(getActivity().getResources().getString(
						R.string.gold));
			}
			if (premium.getTitle() != null)
				packageDuration.setText(premium.getTitle());
			if (premium.getDescription() != null)
				packageDescription.setText(premium.getDescription());
			if (premium.getPrice() != 0)
				price.setText(premium.getPrice() + " $");

			rootView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						if (shopAvailable && !ShopManager.BILING_NOT_AVAILABLE) {
							dialog.show();
							ShopManager.getInstance().MyPremiumStatus(UserManager.getInstance().getCurrentUserId(),
									premium.getInAppId());
						} else {
							UIUtils.showToast(getActivity(),ShopManager.BILING_IS_NOT_AVAILABLE);
						}
					} else {
						getActivity().startActivityForResult(LogInActivity.getActivityIntent(getActivity(),
								UserManager.LOGIN_FOR_RESULT, new Intent()), loginRequestCode);
					}
				}
			});

			return rootView;
		}

	}

	@Override
	public void onIabSetupFinished(BarcaIabResult result) {
		if (result.isSuccess()) {
			shopAvailable = true;
			String purchaseToken = "inapp:" + getActivity().getPackageName()
					+ ":android.test.purchased";
			BarcaPurchase info = new BarcaPurchase(purchaseToken,
					"android.test.purchased", 0);
			BarcaIabHelper.OnConsumeFinishedListener listener = new BarcaIabHelper.OnConsumeFinishedListener() {

				@Override
				public void onConsumeFinished(BarcaPurchase purchase,
						BarcaIabResult result) {
					if (result.isSuccess())
						return;
					if (result.isFailure())
						return;
				}
			};
			billingHelper.consumeAsync(info, listener);
		} else {
			ShopManager.BILING_NOT_AVAILABLE = true;
			UIUtils.showToast(getActivity(),
					ShopManager.BILING_IS_NOT_AVAILABLE);
		}
	}

	@Override
	public void onIabPurchaseFinished(BarcaIabResult result, BarcaPurchase info) {
		if (result.isFailure()) {
			// dealWithPurchaseFailed(result);
			System.out.println("Failure " + result.getMessage());
		} else if (result.isSuccess()) {
			System.out.println("result success");
			dealWithPurchaseSuccess(info);
		}
	}

	private void dealWithPurchaseSuccess(BarcaPurchase info) {
		if (adapter != null) {
			if (adapter.getList() != null) {
				InAppPointsEntity point = adapter.getList().get(
						adapter.getList().indexOf(
								new InAppPointsEntity(info.getSku(), 0)));
				if (point != null) {
					System.out.println("Point not equal null");
					int subscriptionDuration = 0;
					if (point.getTitle()
							.equalsIgnoreCase(ShopManager.ONE_MONTH))
						subscriptionDuration = ShopManager.ONE_MONTH_SUBSCRIPTION;
					else if (point.getTitle().equalsIgnoreCase(
							ShopManager.THREE_MONTHS))
						subscriptionDuration = ShopManager.THREE_MONTHS_SUBSCRIPTION;
					else if (point.getTitle().equalsIgnoreCase(
							ShopManager.SIX_MONTHS))
						subscriptionDuration = ShopManager.SIX_MONTHS_SUBSCRIPTION;
					else if (point.getTitle().equalsIgnoreCase(
							ShopManager.ONE_YEAR))
						subscriptionDuration = ShopManager.ONE_YEAR_SUBSCRIPTION;
					System.out.println("Sub duration " + subscriptionDuration);
					ShopManager.getInstance().subscripeInPremium(
							UserManager.getInstance().getCurrentUserId(),
							subscriptionDuration);
				}
				// ShopManager.getInstance().addPointsForUser(
				// UserManager.getInstance().getCurrentUserId(),
				// Integer.parseInt(point.getTitle()), info,
				// billingHelper, null, true);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		ShopManager.getInstance().addListener(this);
		
	}

	@Override
	public void onPause() {
		super.onPause();
		ShopManager.getInstance().removeListener(this);
	}

	@Override
	public void onException(AppException ex) {
		dialog.dismiss();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onPremiumStateRecieved(PremiumStateObject premium,
			String productID) {
		dialog.dismiss();
		if (premium.isPremium()) {
			@SuppressWarnings("deprecation")
			String dateString = formatedate(Long
					.parseLong(premium.getEndDate()));
			// String dateString = new SimpleDateFormat("MM/dd/yyyy").format(new
			// Date(premium.getEndDate()));
			UIUtils.showToast(getActivity(), getActivity().getResources()
					.getString(R.string.valid_sub) + " " + dateString);
			subscribeTxt.setVisibility(View.VISIBLE);
			subscribeTxt.setText( getActivity().getResources()
					.getString(R.string.valid_sub) + " " + dateString);
		} else {
			String purchasePayload = BarcaIabHelper.ITEM_TYPE_SUBS + ":"
					+ UUID.randomUUID().toString();
			savePurchasePayload(purchasePayload);
			if (billingHelper != null)
				billingHelper.flagEndAsync();
			System.out.println("Product ID " + productID);
			billingHelper.launchSubscriptionPurchaseFlow(getActivity(),
					productID, ShopActivityTabs.PREMIUM_REQUEST_CODE,
					PremiumPackageFragment.this, purchasePayload);
		}
	}

	private String formatedate(long date1) {

		long time = date1 * (long) 1000;
		// Date date = new Date(time);
		// SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm",
		// Locale.getDefault());
		// format.setTimeZone(TimeZone.getTimeZone("GMT"));
		// String dt = format.format(date);
		// ---------------ago------------------------
		// Period dateDiff = calcDiff(new Date(time),
		// new Date(System.currentTimeMillis()));
		// String d = PeriodFormat.wordBased().print(dateDiff);
		// String[] da = d.split(",");
		// return da[0] + " ago";
		// ------------------------------------------
		Date date = new Date(time);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm",
				Locale.getDefault());
		format.setTimeZone(TimeZone.getDefault());
		String beforeSplit = format.format(date);
		String[] afterSplit = beforeSplit.split(" ");
		return afterSplit[0];
	}

	protected void savePurchasePayload(String purchasePayload) {
		SharedPrefsHelper.setRegiterKey(PURCHASE_PAYLOAD_CACHE_KEY,
				purchasePayload);
	}

	private String getPurchasePayload() {
		return SharedPrefsHelper.getRegisterKey(PURCHASE_PAYLOAD_CACHE_KEY);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		int responseCode = data.getIntExtra(BarcaIabHelper.RESPONSE_CODE,
				BarcaIabHelper.BILLING_RESPONSE_RESULT_OK);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK
				&& responseCode == BarcaIabHelper.BILLING_RESPONSE_RESULT_OK) {
			if (requestCode == ShopActivityTabs.PREMIUM_REQUEST_CODE) {
				try {
					System.out.println("on activity result child");
//					String purchaseData = data
//							.getStringExtra(BarcaIabHelper.RESPONSE_INAPP_PURCHASE_DATA);
//					JSONObject purchase = new JSONObject(purchaseData);
//					String developerPayload = purchase
//							.getString(RESPONSE_PAYLOAD);
//					if (developerPayload == null)
//						developerPayload = "";
//					if (getPurchasePayload().equals(developerPayload)) {
						System.out.println("Verification success");
						billingHelper.handleActivityResult(requestCode,
								resultCode, data);
//					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if (resultCode == Activity.RESULT_OK
				&& requestCode == FansListFragment.INTENT_RESULT_CODE) {
			((BaseActivity) getActivity()).refreshMenu();
		}
	}

}
