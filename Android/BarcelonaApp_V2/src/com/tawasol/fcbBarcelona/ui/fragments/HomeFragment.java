package com.tawasol.fcbBarcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.ui.activities.HomeActivity;

public class HomeFragment extends Fragment {

	private FragmentTabHost mTabHost;

	TabWidget widget;
	HorizontalScrollView hs;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home, container,
				false);

		initView(rootView);
		switchTabs();

		return rootView;
	}

	
	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);

		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// TabWidget tw = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// LinearLayout ll = (LinearLayout) tw.getParent();
		hs = (HorizontalScrollView) rootView
				.findViewById(R.id.horizontalScrollView);
//		hs.setFillViewport(false);
		// hs.setLayoutParams(new FrameLayout.LayoutParams(
		// FrameLayout.LayoutParams.MATCH_PARENT,
		// FrameLayout.LayoutParams.WRAP_CONTENT));
		// LinearLayout tabsContainer = new LinearLayout(getActivity());
		// tabsContainer.setLayoutParams(new FrameLayout.LayoutParams(
		// FrameLayout.LayoutParams.MATCH_PARENT,
		// FrameLayout.LayoutParams.WRAP_CONTENT));
		// tabsContainer.setGravity(Gravity.CENTER);
		// ll.setGravity(Gravity.CENTER_HORIZONTAL);
		// ll.addView(hs, 0);
		// ll.removeView(tw);
		// tabsContainer.addView(tw);
		// hs.addView(tabsContainer);
		// hs.setFillViewport(true);
		// hs.setHorizontalScrollBarEnabled(false);

		mTabHost.setup(getActivity(), getChildFragmentManager(),
				android.R.id.tabcontent);
		mTabHost.getTabWidget().setDividerDrawable(null);
		mTabHost.getTabWidget().setStripEnabled(false);
		FragmentInfo info;
		Bundle bundle = new Bundle();
		info = new FragmentInfo(FragmentInfo.LATEST_TAB,
				FragmentInfo.ACTION_FIRST_TIME);
		bundle.putSerializable(FragmentInfo.FRAGMENT_INFO_KEY, info);

		mTabHost.addTab(
				/*
				 * setIndicator(mTabHost.newTabSpec(getActivity().getResources()
				 * .getString(R.string.Home_tab1)), R.drawable.left_tab),
				 */

				mTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Home_tab1)).setIndicator(
						getTabIndicator(mTabHost.getContext(), getActivity()
								.getResources().getString(R.string.Home_tab1),
								android.R.drawable.star_on)),
				WallFragment.class, bundle);
		bundle = new Bundle();
		info = new FragmentInfo(FragmentInfo.TOP_TEN,
				FragmentInfo.ACTION_FIRST_TIME);
		bundle.putSerializable(FragmentInfo.FRAGMENT_INFO_KEY, info);

		mTabHost.addTab(
				/*
				 * setIndicator(mTabHost.newTabSpec(getActivity().getResources()
				 * .getString(R.string.Home_tab2)), R.drawable.center_tab),
				 */
				mTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Home_tab2)).setIndicator(
						getTabIndicator(mTabHost.getContext(), getActivity()
								.getResources().getString(R.string.Home_tab2),
								android.R.drawable.star_on)),
				TopTenFragment.class, bundle);
		bundle = new Bundle();
		info = new FragmentInfo(FragmentInfo.WALL,
				FragmentInfo.ACTION_FIRST_TIME);
		bundle.putSerializable(FragmentInfo.FRAGMENT_INFO_KEY, info);
		mTabHost.addTab(
				/*
				 * setIndicator(mTabHost.newTabSpec(getActivity().getResources()
				 * .getString(R.string.Home_tab3)), R.drawable.center_tab),
				 */
				mTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Home_tab3)).setIndicator(
						getTabIndicator(mTabHost.getContext(), getActivity()
								.getResources().getString(R.string.Home_tab3),
								android.R.drawable.star_on)),
				WallFragment.class, bundle);
		bundle = new Bundle();
		info = new FragmentInfo(FragmentInfo.MY_PIC,
				FragmentInfo.ACTION_FIRST_TIME);
		bundle.putSerializable(FragmentInfo.FRAGMENT_INFO_KEY, info);
		mTabHost.addTab(
				/*
				 * setIndicator(mTabHost.newTabSpec(getActivity().getResources()
				 * .getString(R.string.Home_tab4)), R.drawable.right_tab),
				 */
				mTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Home_tab4)).setIndicator(
						getTabIndicator(mTabHost.getContext(), getActivity()
								.getResources().getString(R.string.Home_tab4),
								android.R.drawable.star_on)),
				WallFragment.class, bundle);

		ViewTreeObserver observer = widget.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@SuppressLint("NewApi")
			@Override
			public void onGlobalLayout() {

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					widget.getViewTreeObserver().removeOnGlobalLayoutListener(
							this);
				} else {
					widget.getViewTreeObserver().removeGlobalOnLayoutListener(
							this);
				}

				Display display = getActivity().getWindowManager()
						.getDefaultDisplay();
				int width = display.getWidth();
				if (widget.getWidth() < width) {
					FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.MATCH_PARENT);
					params.gravity = Gravity.CENTER_HORIZONTAL;

					widget.setLayoutParams(params);
//					 hs.setFillViewport(true);
				}
			}
		});
//		android.view.ViewGroup.LayoutParams param = mTabHost.getTabWidget().getChildAt(0).getLayoutParams();
//		param.width = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
//		param.height = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
//		
//		mTabHost.getTabWidget().getChildAt(0).setLayoutParams(param);
//		mTabHost.getTabWidget().getChildAt(1).setLayoutParams(param);
//		mTabHost.getTabWidget().getChildAt(2).setLayoutParams(param);
//		mTabHost.getTabWidget().getChildAt(3).setLayoutParams(param);
		// ViewTreeObserver observer = mTabHost.getViewTreeObserver();
		// observer.addOnGlobalLayoutListener(new
		// ViewTreeObserver.OnGlobalLayoutListener() {
		//
		// @SuppressWarnings("deprecation")
		// @SuppressLint("NewApi")
		// @Override
		// public void onGlobalLayout() {
		// // menuBtnRight.setPadding(0, titleView.getHeight() / 2, 0, 0);
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
		// mTabHost.getViewTreeObserver()
		// .removeOnGlobalLayoutListener(this);
		// } else {
		// mTabHost.getViewTreeObserver()
		// .removeGlobalOnLayoutListener(this);
		// }
		// for (int x = 0; x < mTabHost.getTabWidget().getChildCount(); x++) {
		// TextView tv = (TextView) mTabHost.getTabWidget()
		// .getChildAt(x).findViewById(R.id.textView);
		// LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
		// mTabHost.getTabWidget().getChildAt(x).getLayoutParams();
		// params.width = tv.getWidth();
		// mTabHost.getTabWidget().getChildAt(x).setLayoutParams(params );
		// // mTabHost.getTabWidget().getChildAt(x).getLayoutParams().width = tv
		// // .getWidth();
		// }
		// }
		// });
		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// for (int i = 0; i < mTabHost.getTabWidget().getChildCount();
				// i++) {
				// TextView tv = (TextView) mTabHost.getCurrentTabView()
				// .findViewById(R.id.tab_text);
				// tv.setTextColor(Color.WHITE);
				//
				// }
				// //
				// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				//
				// TextView tv = (TextView)
				// mTabHost.getChildAt(mTabHost.getCurrentTab())
				// .findViewById(R.id.tab_text);
				// tv.setTextColor(Color.BLACK);

				for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
					TextView tv = (TextView) mTabHost.getTabWidget()
							.getChildAt(i).findViewById(R.id.textView);
					// .setBackgroundResource(R.drawable.tab); // unselected

					// TextView tv = (TextView) mTabHost.getCurrentTabView()
					// .findViewById(R.id.tab_text); // for Selected
					// Tab
					tv.setTypeface(Typeface.DEFAULT);
				}
				TextView tv = (TextView) mTabHost.getTabWidget()
						.getChildAt(mTabHost.getCurrentTab())
						.findViewById(R.id.textView);
				// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				// .setBackgroundResource(R.drawable.tab_pressed); // selected
				// TextView tv = (TextView) mTabHost.getCurrentTabView()
				// .findViewById(android.R.id.title); // for Selected Tab
				tv.setTypeface(Typeface.DEFAULT_BOLD);

				// WallFragment wall = (WallFragment) getChildFragmentManager()
				// .findFragmentByTag("Latest");
				// if (getActivity().getIntent().getExtras() == null)
				// wall.getToFirst();
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		// for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
		// TextView tv = (TextView) mTabHost.getCurrentTabView()
		// .findViewById(R.id.tab_text);
		// tv.setTextColor(Color.WHITE);
		//
		// }
		// // mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
		//
		// TextView tv = (TextView)
		// mTabHost.getChildAt(mTabHost.getCurrentTab())
		// .findViewById(R.id.tab_text);
		// tv.setTextColor(Color.BLACK);
		// .setBackgroundResource(R.drawable.tab_pressed);

		for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.textView);
			// .setBackgroundResource(R.drawable.tab); // unselected

			// TextView tv = (TextView) mTabHost.getCurrentTabView()
			// .findViewById(R.id.tab_text); // for Selected
			// Tab
			tv.setTypeface(Typeface.DEFAULT);
			// tv.setTextSize(getActivity().getResources().getDimension(
			// R.dimen.smallTextSize));
		}
		// TextView tv = (TextView) mTabHost.getTabWidget()
		// .getChildAt(mTabHost.getCurrentTab())
		// .findViewById(R.id.tab_text);
		// // tv.setTextSize(getActivity().getResources().getDimension(
		// // R.dimen.smallTextSize));
		// // mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
		// // .setBackgroundResource(R.drawable.tab_pressed); // selected
		TextView tv = (TextView) mTabHost.getCurrentTabView().findViewById(
				R.id.textView); // for Selected Tab
		tv.setTypeface(Typeface.DEFAULT_BOLD);
	}

	// @SuppressLint("InflateParams")
	// public TabSpec setIndicator(final TabSpec spec, int resid) {
	// // TODO Auto-generated method stub
	// View v = LayoutInflater.from(getActivity()).inflate(
	// R.layout.tab_host_text, null);
	// v.setBackgroundResource(resid);
	// // LinearLayout container =
	// // (LinearLayout)v.findViewById(R.id.CUSTOMCONTAINER);
	// // container.setBackgroundResource(resid);
	// final TextView text = (TextView) v.findViewById(R.id.tab_text);
	// text.setText(spec.getTag());
	// // text.setTextSize(getActivity().getResources().getDimension(
	// // R.dimen.smallTextSize));
	// text.setTextColor(getActivity().getResources().getColor(
	// android.R.color.white));
	// text.setOnClickListener(new View.OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// if (mTabHost.getCurrentTabTag().equalsIgnoreCase(
	// text.getText().toString())) {
	// Fragment wall = getChildFragmentManager()
	// .findFragmentByTag(text.getText().toString());
	// if (wall instanceof WallFragment) {
	// ((WallFragment) wall).getToFirst();
	// } else if (wall instanceof TopTenFragment) {
	// ((TopTenFragment) wall).getToFirst();
	// }
	// } else {
	// mTabHost.setCurrentTabByTag(text.getText().toString());
	// }
	// }
	// });
	// return spec.setIndicator(v);
	// }

	private void switchTabs() {
		if (getActivity().getIntent().getExtras() != null) {
			int tab = getActivity().getIntent().getExtras()
					.getInt(HomeActivity.TAB_OBJ);
			if (tab != 0) {
				switch (tab) {
				case FragmentInfo.LATEST_TAB:
					mTabHost.setCurrentTab(0);
					break;
				case FragmentInfo.TOP_TEN:
					mTabHost.setCurrentTab(1);
					break;
				case FragmentInfo.WALL:
					mTabHost.setCurrentTab(2);
					break;
				case FragmentInfo.MY_PIC:
					mTabHost.setCurrentTab(3);
					break;

				default:
					break;
				}
			}
		}
	}

	private View getTabIndicator(Context context, String title, int icon) {
		final View view = LayoutInflater.from(context).inflate(
				R.layout.tab_layout, null);

		// ImageView iv = (ImageView) view.findViewById(R.id.imageView);
		// iv.setImageResource(icon);
		final TextView tv = (TextView) view.findViewById(R.id.textView);
		tv.setText(title);

		// ViewTreeObserver observer = tv.getViewTreeObserver();
		// observer.addOnGlobalLayoutListener(new
		// ViewTreeObserver.OnGlobalLayoutListener() {
		//
		// @SuppressLint("NewApi")
		// @Override
		// public void onGlobalLayout() {
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
		// tv.getViewTreeObserver()
		// .removeOnGlobalLayoutListener(this);
		// } else {
		// tv.getViewTreeObserver()
		// .removeGlobalOnLayoutListener(this);
		// }
		//
		// LinearLayout.LayoutParams param = new
		// LinearLayout.LayoutParams(tv.getWidth(),
		// LinearLayout.LayoutParams.WRAP_CONTENT);
		// view.setLayoutParams(param);
		// }
		// });
		tv.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mTabHost.getCurrentTabTag().equalsIgnoreCase(
						tv.getText().toString())) {
					Fragment wall = getChildFragmentManager()
							.findFragmentByTag(tv.getText().toString());
					if (wall instanceof WallFragment) {
						((WallFragment) wall).getToFirst();
					} else if (wall instanceof TopTenFragment) {
						((TopTenFragment) wall).getToFirst();
					}
				} else {
					mTabHost.setCurrentTabByTag(tv.getText().toString());
				}
			}
		});
		return view;
	}
}
