package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.commonsware.cwac.endless.EndlessAdapter;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.customView.PostViewFactory;
import com.tawasol.fcbBarcelona.data.connection.URLs;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.entities.PostViewModel;
import com.tawasol.fcbBarcelona.entities.WebServiceRequestInfo;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnCommentScreenFinish;
import com.tawasol.fcbBarcelona.listeners.OnPostsRecieved;
import com.tawasol.fcbBarcelona.listeners.OnUpdateFinished;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.managers.WallManager;
import com.tawasol.fcbBarcelona.ui.activities.TagListActivity;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class TagListFragment extends Fragment implements OnPostsRecieved,
		OnRefreshListener, OnUpdateFinished, OnCommentScreenFinish {

	ListView wallList;
	PostViewModel post;
	SwipeRefreshLayout swipeView;
	TextView initialStateText;
	Button LoginButton;
	LinearLayout initial_state_layout;
	ListAdapter adapter;
	private ProgressBar bar;
	public static PostViewFactory viewFactory;
	boolean isTag;
	RelativeLayout bottomBar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_wall_new, container,
				false);

		ImageButton filterBtn = (ImageButton) rootView
				.findViewById(R.id.filterButton);
		filterBtn.setVisibility(View.GONE);
		wallList = (ListView) rootView.findViewById(R.id.listView1);
		// wallList.setPadding(
		// 0,
		// 0,
		// 0,
		// getActivity().getIntent().getIntExtra(
		// TagListActivity.BOTTOM_BAR_HEIGHT, 200));
		bottomBar = (RelativeLayout) rootView.findViewById(R.id.bottom_bar);
		bottomBar.setVisibility(View.GONE);
		PauseOnScrollListener listener = new PauseOnScrollListener(App
				.getInstance().getImageLoader(), true, true);
		wallList.setOnScrollListener(listener);
		swipeView = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
		swipeView.setColorSchemeResources(R.color.imageFooter);
		swipeView.setOnRefreshListener(this);
		initial_state_layout = (LinearLayout) rootView
				.findViewById(R.id.initial_state_layout);
		LoginButton = (Button) rootView.findViewById(R.id.loginButton);
		initialStateText = (TextView) rootView
				.findViewById(R.id.initial_state_text);
		bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
		bar.setVisibility(View.VISIBLE);

		isTag = getActivity().getIntent().getExtras()
				.getBoolean(TagListActivity.IS_TAG_LIST, false);
		WebServiceRequestInfo requestInfo = null;
		if (isTag) {
			requestInfo = new WebServiceRequestInfo(
					FragmentInfo.ACTION_FIRST_TIME, UserManager.getInstance()
							.getCurrentUserId(), WallManager.NUM_OF_POSTS, 0,
					WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
					WebServiceRequestInfo.GET_POSTS, URLs.Methods.GET_TAG_POSTS);
		} else {
			requestInfo = new WebServiceRequestInfo(
					FragmentInfo.ACTION_FIRST_TIME, UserManager.getInstance()
							.getCurrentUserId(), WallManager.NUM_OF_POSTS, 0,
					WebServiceRequestInfo.TIME_FILTER_OLD, 0, getActivity()
							.getIntent()
							.getIntExtra(TagListActivity.USER_ID, 0),
					WebServiceRequestInfo.GET_POSTS, "wall/getUserPosts");
		}
		WallManager.getInstance().getTagPosts(
				getActivity().getIntent().getStringExtra(
						TagListActivity.TAG_NAME), requestInfo, isTag);

		return rootView;
	}

	@Override
	public void onResume() {
		WallManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
		WallManager.getInstance().removeListener(this);
		super.onPause();
	}

	@Override
	public void onSuccess(List<PostViewModel> objs) {
		bar.setVisibility(View.GONE);
		swipeView.setRefreshing(false);
		// if (objs.isEmpty())
		// if (adapter != null && adapter.getCount() == 0)
		// fireNoPostsFoundByFilter();
		// else if (adapter == null)
		// fireNoPostsFoundByFilter();

		wallList.setVisibility(View.VISIBLE);
		adapter = new ListAdapter(objs);
		wallList.setAdapter(new updatePosts(getActivity(),
				new ListAdapter(objs), R.layout.progress_bar));
	}

	@Override
	public void onException(AppException ex) {
		if (ex.getErrorCode() != AppException.NO_DATA_EXCEPTION) {
			bar.setVisibility(View.GONE);
			UIUtils.showToast(getActivity(), ex.getMessage());
		} else if (adapter == null) {
			fireNoPostsFoundByFilter();
		}
	}

	@Override
	public void onRefresh() {
		WebServiceRequestInfo requestInfo = null;
		long currentTime = WallManager.getInstance().getTagListCurrentTime();
		if (isTag)
			requestInfo = new WebServiceRequestInfo(FragmentInfo.ACTION_UPDATE,
					UserManager.getInstance().getCurrentUserId(),
					WallManager.NUM_OF_POSTS, currentTime,
					WebServiceRequestInfo.TIME_FILTER_NEW, 0, 0,
					WebServiceRequestInfo.GET_POSTS, URLs.Methods.GET_TAG_POSTS);
		else
			requestInfo = new WebServiceRequestInfo(FragmentInfo.ACTION_UPDATE,
					UserManager.getInstance().getCurrentUserId(),
					WallManager.NUM_OF_POSTS, currentTime,
					WebServiceRequestInfo.TIME_FILTER_NEW, 0, getActivity()
							.getIntent()
							.getIntExtra(TagListActivity.USER_ID, 0),
					WebServiceRequestInfo.GET_POSTS, "wall/getUserPosts");
		WallManager.getInstance().getTagPosts(
				getActivity().getIntent().getStringExtra(
						TagListActivity.TAG_NAME), requestInfo, isTag);

	}

	private class ListAdapter extends BaseAdapter {
		List<PostViewModel> posts;

		public ListAdapter(List<PostViewModel> posts) {
			this.posts = posts;
			viewFactory = new PostViewFactory(getActivity(), 10);
		}

		List<PostViewModel> getPosts() {
			return this.posts;
		}

		void addToList(List<PostViewModel> posts) {
			this.posts.addAll(posts);
		}

		@Override
		public int getCount() {
			return this.posts.size();
		}

		@Override
		public Object getItem(int position) {
			return this.posts.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View rootView = null;
			if (!this.posts.isEmpty())
				rootView = viewFactory.getView(convertView, parent,
						this.posts.get(position), wallList, position, 0);
			return rootView;
		}

	}

	public void getToFirst() {
		wallList.smoothScrollToPosition(0);
	}

	private void fireNoPostsFoundByFilter() {
		initial_state_layout.setVisibility(View.VISIBLE);
		wallList.setVisibility(View.GONE);
		initialStateText.setText(getActivity().getResources().getString(
				R.string.HomeInitialState_MypicsNoPhotosMessage));
		LoginButton.setVisibility(View.GONE);
	}

	private class updatePosts extends EndlessAdapter {

		List<PostViewModel> tempList;

		public updatePosts(Context context, ListAdapter wrapped,
				int pendingResource) {
			super(context, wrapped, pendingResource);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.commonsware.cwac.endless.EndlessAdapter#appendCachedData()
		 */
		@Override
		protected void appendCachedData() {
			if (getWrappedAdapter() != null)
				((ListAdapter) getWrappedAdapter()).addToList(tempList);
			// tempList = null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.commonsware.cwac.endless.EndlessAdapter#cacheInBackground()
		 */
		@Override
		protected boolean cacheInBackground() throws Exception {
			WebServiceRequestInfo requestInfo = null;
			if (isTag)
				requestInfo = new WebServiceRequestInfo(
						WebServiceRequestInfo.ACTION_GETMORE, UserManager
								.getInstance().getCurrentUserId(),
						WallManager.NUM_OF_POSTS, getLastUpdateTime(),
						WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
						WebServiceRequestInfo.GET_POSTS,
						URLs.Methods.GET_TAG_POSTS);
			else
				requestInfo = new WebServiceRequestInfo(
						WebServiceRequestInfo.ACTION_GETMORE, UserManager
								.getInstance().getCurrentUserId(),
						WallManager.NUM_OF_POSTS, getLastUpdateTime(),
						WebServiceRequestInfo.TIME_FILTER_OLD, 0, getActivity()
								.getIntent().getIntExtra(
										TagListActivity.USER_ID, 0),
						WebServiceRequestInfo.GET_POSTS, "wall/getUserPosts");
			tempList = WallManager.getInstance().getTagPostsFromServer(
					getActivity().getIntent().getStringExtra(
							TagListActivity.TAG_NAME), requestInfo, isTag);
			return !tempList.isEmpty();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tawasol.barcelona.listeners.OnUpdateFinished#onUpdateFinished()
	 */
	@Override
	public void onUpdateFinished() {
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		swipeView.setRefreshing(false);
		wallList.invalidateViews();
	}

	public long getLastUpdateTime() {
		return adapter.getPosts().get(adapter.getPosts().size() - 1)
				.getLastUpdateTime();
	}

	@Override
	public void getCount(int count) {
		// TODO Auto-generated method stub

	}
}
