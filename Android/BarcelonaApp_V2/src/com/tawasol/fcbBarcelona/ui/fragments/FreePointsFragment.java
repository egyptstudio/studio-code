package com.tawasol.fcbBarcelona.ui.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.supersonic.adapters.supersonicads.SupersonicConfig;
import com.supersonic.mediationsdk.logger.SupersonicError;
import com.supersonic.mediationsdk.sdk.OfferwallListener;
import com.supersonic.mediationsdk.sdk.RewardedVideoListener;
import com.supersonic.mediationsdk.sdk.Supersonic;
import com.supersonic.mediationsdk.sdk.SupersonicFactory;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.managers.ShopManager;
import com.tawasol.fcbBarcelona.managers.SuperSonicManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.WebViewShopActivity;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class FreePointsFragment extends Fragment implements
		RewardedVideoListener, OnClickListener ,OfferwallListener {

	// Declare the Supersonic Mediation Agent
	private Supersonic mMediationAgent;
	private ProgressDialog superSonicLoading;
	RelativeLayout watchVedio;
	private RelativeLayout surveyBtn;
	private RelativeLayout rewardingBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		superSonicLoading = new ProgressDialog(getActivity(), R.style.MyTheme);
		superSonicLoading
				.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		superSonicLoading.setCancelable(false);
		mMediationAgent = SuperSonicManager.getInstance().getSuperSonicAgent();
//		// Get the mediation publisher instance
//		mMediationAgent = SupersonicFactory.getInstance();
//		// Set the Rewarded Video Listener
//		mMediationAgent.setRewardedVideoListener(this);
//		superSonicAppKey = getActivity().getString(R.string.super_sonic_app_id);
//		String userId = String.valueOf(UserManager.getInstance()
//				.getCurrentUserId());
//
//		//Set the Offerwall Listener
//		mMediationAgent.setOfferwallListener(this);
//		
//		// Init Rewarded Video
//		mMediationAgent.initRewardedVideo(getActivity(), superSonicAppKey,
//				userId);
//		//You can set optional parameters as well via the .getConfigObj
//		SupersonicConfig.getConfigObj().setClientSideCallbacks(true);
//
//		//Init Offerwall
//		mMediationAgent.initOfferwall(getActivity(), superSonicAppKey, userId);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.free_points_fragment,
				container, false);
		watchVedio = (RelativeLayout) rootView.findViewById(R.id.superSonicBtn);
		watchVedio.setOnClickListener(this);
		surveyBtn = (RelativeLayout) rootView.findViewById(R.id.survyBtn);
		surveyBtn.setOnClickListener(this);
		rewardingBtn = (RelativeLayout) rootView
				.findViewById(R.id.rewardingBtn);
		rewardingBtn.setOnClickListener(this);
		return rootView;
	}

	public void onResume() {
		super.onResume();
		if (mMediationAgent != null) {
			mMediationAgent.onResume(getActivity());
		}
	}

	public void onPause() {
		super.onPause();
		if (mMediationAgent != null) {
			mMediationAgent.onPause(getActivity());
		}
	}

	@Override
	public void onRewardedVideoAdClosed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdOpened() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdRewarded(int arg0) {
		UserManager.getInstance().updateUserCriedt(arg0);
	}

	@Override
	public void onRewardedVideoInitFail(SupersonicError arg0) {
//		superSonicLoading.dismiss();
		UIUtils.showToast(getActivity(),
				getString(R.string.super_sonic_ads_fail));
	}

	@Override
	public void onRewardedVideoInitSuccess() {
	}

	@Override
	public void onVideoAvailabilityChanged(boolean arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onVideoEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onVideoStart() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.superSonicBtn:
			startSuperSonic();
			break;
		case R.id.survyBtn:
//			startActivity(new Intent(getActivity(), WebViewShopActivity.class)
//					.putExtra(WebViewShopActivity.IS_SURVY, true));
			startWallOffer();
			break;
		case R.id.rewardingBtn:
			startActivity(new Intent(getActivity(), WebViewShopActivity.class)
					.putExtra(WebViewShopActivity.IS_SURVY, false));
			break;

		default:
			break;
		}
	}

	private void startWallOffer() {
		if(mMediationAgent.isOfferwallAvailable()){
			System.out.println("Wall Available");
			mMediationAgent.showOfferwall();
		}else{
			UIUtils.showToast(getActivity(),
					getString(R.string.super_sonic_ads_fail));
		}
	}

	private void startSuperSonic() {
//		superSonicLoading.show();
		boolean available = mMediationAgent.isRewardedVideoAvailable();
		if (available){
			System.out.println("vedio Available");
			mMediationAgent.showRewardedVideo();
		}else{
			UIUtils.showToast(getActivity(),
					getString(R.string.super_sonic_ads_fail));
		}

	}

	@Override
	public void onGetOfferwallCreditsFail(SupersonicError arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onOfferwallAdCredited(int arg0, int arg1, boolean arg2) {
		UserManager.getInstance().updateUserCriedt(arg0);
		return false;
	}

	@Override
	public void onOfferwallClosed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onOfferwallInitFail(SupersonicError arg0) {
		UIUtils.showToast(getActivity(),
				getString(R.string.super_sonic_ads_fail));
	}

	@Override
	public void onOfferwallInitSuccess() {
	}

	@Override
	public void onOfferwallOpened() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onOfferwallShowFail(SupersonicError arg0) {
		// TODO Auto-generated method stub
		
	}
}
