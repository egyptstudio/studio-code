/**
 * 
 */
package com.tawasol.fcbBarcelona.ui.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.etiennelawlor.quickreturn.library.enums.QuickReturnType;
import com.etiennelawlor.quickreturn.library.listeners.OnViewStateChanged;
import com.etiennelawlor.quickreturn.library.listeners.QuickReturnListViewOnScrollListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.entities.PostViewModel;
import com.tawasol.fcbBarcelona.entities.TopTenModel;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnSeasonReceived;
import com.tawasol.fcbBarcelona.listeners.OnUpdateFinished;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.managers.WallManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.ChatActivity;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivityTabs;
import com.tawasol.fcbBarcelona.ui.activities.StudioFoldersActivity;
import com.tawasol.fcbBarcelona.ui.adapters.TopTenAdapter;
import com.tawasol.fcbBarcelona.utils.UIUtils;

/**
 * @author Basyouni
 *
 */
public class TopTenFragment extends Fragment implements OnSeasonReceived,
		OnViewStateChanged, OnRefreshListener, OnUpdateFinished/*
																 * ,
																 * OnTopTenRecieved
																 */{

	SwipeRefreshLayout swipeView;
	private Button captureBtn;
	RelativeLayout bottomBar;
	public static final String PopUPInitialState = "popUpInitialState";
	LinearLayout studioPopUp;
	ExpandableListView topTenExpandableList;
	private FragmentInfo info;
	private ProgressBar bar;
	
	private String adMobID;

//	private InterstitialAd mInterstitialAd;
	
	private void requestNewInterstitial() {
//		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
//				"1689F622DD32B37E07CDC03FCCA102F7").build();
//
//		mInterstitialAd.loadAd(adRequest);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
//		adMobID = getString(R.string.banner_ad_unit_id);
//
//		mInterstitialAd = new InterstitialAd(getActivity());
//		mInterstitialAd.setAdUnitId(adMobID);
//
//		requestNewInterstitial();
//
//		mInterstitialAd.setAdListener(new AdListener() {
//			@Override
//			public void onAdClosed() {
//				requestNewInterstitial();
//				navigateToStudio();
//			}
//
//			@Override
//			public void onAdFailedToLoad(int errorCode) {
//				super.onAdFailedToLoad(errorCode);
//				System.out.println("Error " + errorCode);
//			}
//
//			@Override
//			public void onAdLoaded() {
//				// TODO Auto-generated method stub
//				super.onAdLoaded();
//			}
//		});
	}

	protected void navigateToStudio() {
		// TODO Auto-generated method stub
		if (studioPopUp.getVisibility() == View.VISIBLE) {
			hideStudioPopUp();
		}
		startActivity(StudioFoldersActivity
				.getActivityIntent(getActivity() , StudioFoldersActivity.PLAYER_FOLDER));
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.top_ten_fragment, container,
				false);
		((BaseActivity) getActivity()).setIsWall();
		WallManager.setWHICH_TAB(FragmentInfo.TOP_TEN);
		bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
		topTenExpandableList = (ExpandableListView) rootView
				.findViewById(R.id.topTenExpandableList);
		swipeView = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
		swipeView.setOnRefreshListener(this);
		swipeView.setColorSchemeResources(R.color.imageFooter);
		PauseOnScrollListener listener = new PauseOnScrollListener(App
				.getInstance().getImageLoader(), true, true);
		topTenExpandableList.setOnScrollListener(listener);
		info = (FragmentInfo) getArguments().getSerializable(
				FragmentInfo.FRAGMENT_INFO_KEY);
		studioPopUp = (LinearLayout) rootView.findViewById(R.id.studioPopUp);
		studioPopUp.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				hideStudioPopUp();
			}
		});
		checkInitialState(info);
		bottomBar = (RelativeLayout) rootView.findViewById(R.id.bottom_bar);
		setupBottomBar(rootView);
		activityStarted();
		return rootView;
	}

	private void setupBottomBar(View rootView) {
		captureBtn = (Button) rootView.findViewById(R.id.capture_btn);
		captureBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				if (mInterstitialAd.isLoaded()) {
//					mInterstitialAd.show();
//				} else {
					navigateToStudio();
//				}
			}
		});

		TextView storeBtn = (TextView) rootView.findViewById(R.id.home_btn);
		storeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(ShopActivityTabs.getIntent(getActivity() , ShopActivityTabs.NIKE_STORE_TAB).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			}
		});

		TextView chatBtn = (TextView) rootView.findViewById(R.id.chat_btn);
		chatBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// UIUtils.showToast(getActivity(),
				// getResources().getString(R.string.demo_version));
				/** Add by Turki **/
				if (UserManager.getInstance().getCurrentUser() != null
						&& UserManager.getInstance().getCurrentUserId() > 0) {
					getActivity().startActivity(
							ChatActivity.getActivityIntent(getActivity()));
				} else {
					getActivity().startActivity(
							LogInActivity.getActivityIntent(getActivity(),
									UserManager.LOGIN_WITH_INTENT, ChatActivity
											.getActivityIntent(getActivity())));
				}
			}
		});

	}

	public void showStudioPopUp() {
		if (!checkIfInitialState())
			studioPopUp.setVisibility(View.VISIBLE);
	}

	public void hideStudioPopUp() {
		studioPopUp.setVisibility(View.GONE);
		saveInitialPopUpStateEnded();
	}

	public boolean checkIfInitialState() {
		return new File(getActivity().getFilesDir() + "/" + PopUPInitialState)
				.exists();
	}

	public void saveInitialPopUpStateEnded() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(getActivity(),
							PopUPInitialState, "");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void checkInitialState(FragmentInfo info2) {
		showStudioPopUp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		QuickReturnListViewOnScrollListener.registerToUiListeners(this);
		WallManager.getInstance().addListener(this);
		topTenExpandableList.invalidateViews();
	}

	@Override
	public void onPause() {
		super.onPause();
		QuickReturnListViewOnScrollListener.unRegisterToUiListeners(this);
		WallManager.getInstance().removeListener(this);
	}

	public void activityStarted() {
		List<String> listHeaders = new ArrayList<String>();
		HashMap<String, List<PostViewModel>> childList = new HashMap<String, List<PostViewModel>>();

		List<TopTenModel> seasons = WallManager.getInstance()
				.getTopTenSessons();

		if (seasons != null && !seasons.isEmpty()) {
			for (TopTenModel model : seasons) {
				listHeaders.add(model.getSeasonName());
				childList.put(model.getSeasonName(), model.getPosts());
			}

			if (bar.getVisibility() == View.VISIBLE)
				bar.setVisibility(View.GONE);
			ViewTreeObserver observer = bottomBar.getViewTreeObserver();
			observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

				@SuppressLint("NewApi")
				@SuppressWarnings("deprecation")
				@Override
				public void onGlobalLayout() {

					if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
						bottomBar.getViewTreeObserver()
								.removeOnGlobalLayoutListener(this);
					} else {
						bottomBar.getViewTreeObserver()
								.removeGlobalOnLayoutListener(this);
					}
					// QuickReturnListViewOnScrollListener scrollListener = new
					// QuickReturnListViewOnScrollListener(
					// QuickReturnType.BOTH, HomeFragment.mTabHost
					// .getTabWidget(), HomeFragment.mTabHost
					// .getTabWidget().getHeight(), bottomBar,
					// bottomBar.getHeight());
					QuickReturnListViewOnScrollListener scrollListener = new QuickReturnListViewOnScrollListener(
							QuickReturnType.FOOTER, null, 0, bottomBar,
							bottomBar.getHeight());
					// Setting to true will slide the header and/or footer into
					// view or slide out of view based
					// on what is visible in the idle scroll state
					scrollListener.setCanSlideInIdleScrollState(true);
					topTenExpandableList.setOnScrollListener(scrollListener);
				}
			});

			TopTenAdapter adapter = new TopTenAdapter(getActivity(),
					listHeaders, childList, topTenExpandableList);
			topTenExpandableList.setAdapter(adapter);
			if (adapter != null) {
				for (int i = 0; i < adapter.getGroupCount(); i++) {
					if (i == 0)
						topTenExpandableList.expandGroup(i);
				}
			}
		} else {
			WallManager.getInstance().getTopTen(
					UserManager.getInstance().getCurrentUserId(),
					FragmentInfo.getMethodName((FragmentInfo) getArguments()
							.getSerializable(FragmentInfo.FRAGMENT_INFO_KEY)),
					0);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tawasol.barcelona.listeners.OnSeasonReceived_new#onSuccess(java.util
	 * .List)
	 */
	@Override
	public void onSuccess(List<TopTenModel> seasons) {
		swipeView.setRefreshing(false);
		List<String> listHeaders = new ArrayList<String>();
		HashMap<String, List<PostViewModel>> childList = new HashMap<String, List<PostViewModel>>();

		for (TopTenModel model : seasons) {
			listHeaders.add(model.getSeasonName());
			childList.put(model.getSeasonName(), model.getPosts());
		}

		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		// ViewTreeObserver observer = bottomBar.getViewTreeObserver();
		// observer.addOnGlobalLayoutListener(new
		// ViewTreeObserver.OnGlobalLayoutListener() {
		//
		// @SuppressLint("NewApi")
		// @SuppressWarnings("deprecation")
		// @Override
		// public void onGlobalLayout() {
		//
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
		// bottomBar.getViewTreeObserver()
		// .removeOnGlobalLayoutListener(this);
		// } else {
		// bottomBar.getViewTreeObserver()
		// .removeGlobalOnLayoutListener(this);
		// }
		// // QuickReturnListViewOnScrollListener scrollListener = new
		// // QuickReturnListViewOnScrollListener(
		// // QuickReturnType.BOTH, HomeFragment.mTabHost
		// // .getTabWidget(), HomeFragment.mTabHost
		// // .getTabWidget().getHeight(), bottomBar,
		// // bottomBar.getHeight());
		// QuickReturnListViewOnScrollListener scrollListener = new
		// QuickReturnListViewOnScrollListener(
		// QuickReturnType.FOOTER, null, 0, bottomBar, bottomBar
		// .getHeight());
		// // Setting to true will slide the header and/or footer into
		// // view or slide out of view based
		// // on what is visible in the idle scroll state
		// scrollListener.setCanSlideInIdleScrollState(true);
		// topTenExpandableList.setOnScrollListener(scrollListener);
		// }
		// });

		QuickReturnListViewOnScrollListener scrollListener = new QuickReturnListViewOnScrollListener(
				QuickReturnType.FOOTER, null, 0, bottomBar,
				((BaseActivity) getActivity()).getBottomBarHeight());
		// Setting to true will slide the header and/or footer into
		// view or slide out of view based
		// on what is visible in the idle scroll state
		scrollListener.setCanSlideInIdleScrollState(true);
		topTenExpandableList.setOnScrollListener(scrollListener);

		TopTenAdapter adapter = new TopTenAdapter(getActivity(), listHeaders,
				childList, topTenExpandableList);
		topTenExpandableList.setAdapter(adapter);
		if (adapter != null) {
			for (int i = 0; i < adapter.getGroupCount(); i++) {
				if (i == 0)
					topTenExpandableList.expandGroup(i);
			}
		}
	}

	public void getToFirst() {
		topTenExpandableList.smoothScrollToPosition(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tawasol.barcelona.listeners.OnSeasonReceived_new#onException(com.
	 * tawasol.barcelona.exception.AppException)
	 */
	@Override
	public void onException(AppException exception) {
		swipeView.setRefreshing(false);
		if (bar.getVisibility() == View.VISIBLE)
			bar.setVisibility(View.GONE);
		if (exception.getErrorCode() != AppException.NO_DATA_EXCEPTION)
			UIUtils.showToast(getActivity(), exception.getMessage());
	}

	@Override
	public void viewDisappeared() {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (!checkIfInitialState()) {
					studioPopUp.setVisibility(View.GONE);
					saveInitialPopUpStateEnded();
				}
			}
		});
	}

	@Override
	public void viewAppeared() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRefresh() {
		WallManager.getInstance().getTopTen(
				UserManager.getInstance().getCurrentUserId(),
				FragmentInfo.getMethodName((FragmentInfo) getArguments()
						.getSerializable(FragmentInfo.FRAGMENT_INFO_KEY)), 0);
	}

	@Override
	public void onUpdateFinished() {
		swipeView.setRefreshing(false);
	}

	// @Override
	// public void onTopTenRecieved(List<String> seasonsNames,
	// List<PostViewModel> posts) {
	// List<String> listHeaders = new ArrayList<String>();
	// HashMap<String, List<PostViewModel>> childList = new HashMap<String,
	// List<PostViewModel>>();
	// listHeaders.addAll(seasonsNames);
	//
	// // for (TopTenModel model : seasons) {
	// // listHeaders.add(model.getSeasonName());
	// // childList.put(model.getSeasonName(), model.getPosts());
	// // }
	//
	// int factor = posts.size() / listHeaders.size();// 10
	// for (int x = 0; x < listHeaders.size(); x++) {
	// int start = x * factor;
	// int finalLimit = ((x + 1) * 10) - 1;
	// for (int j = 0; j < posts.size(); j++) {
	// if (j > start && j < finalLimit) {
	// postToSave.add(posts.get(j));
	// }
	// }
	// childList.put(listHeaders.get(x), postToSave);
	// }
	//
	// if (bar.getVisibility() == View.VISIBLE)
	// bar.setVisibility(View.GONE);
	// // ViewTreeObserver observer = bottomBar.getViewTreeObserver();
	// // observer.addOnGlobalLayoutListener(new
	// // ViewTreeObserver.OnGlobalLayoutListener() {
	// //
	// // @SuppressLint("NewApi")
	// // @SuppressWarnings("deprecation")
	// // @Override
	// // public void onGlobalLayout() {
	// //
	// // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
	// // bottomBar.getViewTreeObserver()
	// // .removeOnGlobalLayoutListener(this);
	// // } else {
	// // bottomBar.getViewTreeObserver()
	// // .removeGlobalOnLayoutListener(this);
	// // }
	// // // QuickReturnListViewOnScrollListener scrollListener = new
	// // // QuickReturnListViewOnScrollListener(
	// // // QuickReturnType.BOTH, HomeFragment.mTabHost
	// // // .getTabWidget(), HomeFragment.mTabHost
	// // // .getTabWidget().getHeight(), bottomBar,
	// // // bottomBar.getHeight());
	// // QuickReturnListViewOnScrollListener scrollListener = new
	// // QuickReturnListViewOnScrollListener(
	// // QuickReturnType.FOOTER, null, 0, bottomBar, bottomBar
	// // .getHeight());
	// // // Setting to true will slide the header and/or footer into
	// // // view or slide out of view based
	// // // on what is visible in the idle scroll state
	// // scrollListener.setCanSlideInIdleScrollState(true);
	// // topTenExpandableList.setOnScrollListener(scrollListener);
	// // }
	// // });
	//
	// QuickReturnListViewOnScrollListener scrollListener = new
	// QuickReturnListViewOnScrollListener(
	// QuickReturnType.FOOTER, null, 0, bottomBar,
	// ((BaseActivity) getActivity()).getBottomBarHeight());
	// // Setting to true will slide the header and/or footer into
	// // view or slide out of view based
	// // on what is visible in the idle scroll state
	// scrollListener.setCanSlideInIdleScrollState(true);
	// topTenExpandableList.setOnScrollListener(scrollListener);
	//
	// TopTenAdapter adapter = new TopTenAdapter(getActivity(), listHeaders,
	// childList, topTenExpandableList);
	// topTenExpandableList.setAdapter(adapter);
	// if (adapter != null) {
	// for (int i = 0; i < adapter.getGroupCount(); i++) {
	// if (i == 0)
	// topTenExpandableList.expandGroup(i);
	// }
	// }
	// }
}
