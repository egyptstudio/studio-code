package com.tawasol.fcbBarcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivityTabs;

public class ShopFragmentTabs extends Fragment {

	private FragmentTabHost mTabHost;
	private TabWidget widget;
	private HorizontalScrollView hs;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.shop_fragment_tabs,
				container, false);

		initView(rootView);
		switchTabs();
		return rootView;
	}

	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);

		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// TabWidget tw = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// LinearLayout ll = (LinearLayout) tw.getParent();
		hs = (HorizontalScrollView) rootView
				.findViewById(R.id.horizontalScrollView);
		hs.setFillViewport(false);

		mTabHost.setup(getActivity(), getChildFragmentManager(),
				android.R.id.tabcontent);

		mTabHost.addTab(

				mTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.nike_store)).setIndicator(
						getTabIndicator(mTabHost.getContext(), getActivity()
								.getResources().getString(R.string.nike_store),
								android.R.drawable.star_on)), NikeStore.class,
				null);

		// setIndicator(mTabHost.newTabSpec(getActivity().getResources()
		// .getString(R.string.nike_store)), R.drawable.left_tab),
		// NikeStore.class, null);

		mTabHost.addTab(
				mTabHost.newTabSpec(getActivity().getResources().getString(
						R.string.premium_packages)).setIndicator(
				getTabIndicator(mTabHost.getContext(), getActivity()
						.getResources().getString(R.string.premium_packages),
						android.R.drawable.star_on)), PremiumPackageFragment.class,
		null);
				
				
//				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
//						.getString(R.string.premium_packages)),
//						R.drawable.center_tab), PremiumPackageFragment.class,
//				null);
		mTabHost.addTab(
				
				mTabHost.newTabSpec(getActivity().getResources().getString(
						R.string.points_packages)).setIndicator(
				getTabIndicator(mTabHost.getContext(), getActivity()
						.getResources().getString(R.string.Shop_package4),
						android.R.drawable.star_on)), PointPAckages.class,
		null);
				
//				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
//						.getString(R.string.points_packages)),
//						R.drawable.center_tab), PointPAckages.class, null);

		mTabHost.addTab(
				mTabHost.newTabSpec(getActivity().getResources().getString(
						R.string.Redemption)).setIndicator(
				getTabIndicator(mTabHost.getContext(), getActivity()
						.getResources().getString(R.string.Redemption),
						android.R.drawable.star_on)), RedemptionFragment.class,
		null);
				
//				
//				setIndicator(mTabHost.newTabSpec(getActivity().getResources()
//						.getString(R.string.Redemption)), R.drawable.right_tab),
//				RedemptionFragment.class, null);

		ViewTreeObserver observer = widget.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@SuppressLint("NewApi")
			@Override
			public void onGlobalLayout() {

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					widget.getViewTreeObserver().removeOnGlobalLayoutListener(
							this);
				} else {
					widget.getViewTreeObserver().removeGlobalOnLayoutListener(
							this);
				}

				Display display = getActivity().getWindowManager()
						.getDefaultDisplay();
				int width = display.getWidth();
				if (widget.getWidth() < width) {
					FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.MATCH_PARENT);
					params.gravity = Gravity.CENTER_HORIZONTAL;

					widget.setLayoutParams(params);
					 hs.setFillViewport(true);
				}
			}
		});

		// mTabHost.getTabWidget().getChildTabViewAt(1).setEnabled(false);
		// mTabHost.getTabWidget().getChildTabViewAt(3).setEnabled(false);
		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// for (int i = 0; i < mTabHost.getTabWidget().getChildCount();
				// i++) {
				// TextView tv = (TextView) mTabHost.getCurrentTabView()
				// .findViewById(R.id.tabs_text);
				// tv.setTextColor(Color.WHITE);
				//
				// }
				// //
				// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				//
				// TextView tv = (TextView)
				// mTabHost.getChildAt(mTabHost.getCurrentTab())
				// .findViewById(R.id.tabs_text);
				// tv.setTextColor(Color.BLACK);

				for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
					TextView tv = (TextView) mTabHost.getTabWidget()
							.getChildAt(i).findViewById(R.id.textView);
					// .setBackgroundResource(R.drawable.tab); // unselected

					// TextView tv = (TextView) mTabHost.getCurrentTabView()
					// .findViewById(R.id.tabs_text); // for Selected
					// Tab
					tv.setTypeface(Typeface.DEFAULT);
				}
				TextView tv = (TextView) mTabHost.getTabWidget()
						.getChildAt(mTabHost.getCurrentTab())
						.findViewById(R.id.textView);
				// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				// .setBackgroundResource(R.drawable.tab_pressed); // selected
				// TextView tv = (TextView) mTabHost.getCurrentTabView()
				// .findViewById(android.R.id.title); // for Selected Tab
				tv.setTypeface(Typeface.DEFAULT_BOLD);

				// WallFragment wall = (WallFragment) getChildFragmentManager()
				// .findFragmentByTag("Latest");
				// if (getActivity().getIntent().getExtras() == null)
				// wall.getToFirst();
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		// for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
		// TextView tv = (TextView) mTabHost.getCurrentTabView()
		// .findViewById(R.id.tabs_text);
		// tv.setTextColor(Color.WHITE);
		//
		// }
		// // mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
		//
		// TextView tv = (TextView)
		// mTabHost.getChildAt(mTabHost.getCurrentTab())
		// .findViewById(R.id.tabs_text);
		// tv.setTextColor(Color.BLACK);
		// .setBackgroundResource(R.drawable.tab_pressed);

		for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.textView);
			// .setBackgroundResource(R.drawable.tab); // unselected

			// TextView tv = (TextView) mTabHost.getCurrentTabView()
			// .findViewById(R.id.tabs_text); // for Selected
			// Tab
			tv.setTypeface(Typeface.DEFAULT);
			// tv.setTextSize(getActivity().getResources().getDimension(
			// R.dimen.smallTextSize));
		}
		TextView tv = (TextView) mTabHost.getTabWidget()
				.getChildAt(mTabHost.getCurrentTab())
				.findViewById(R.id.textView);
		// tv.setTextSize(getActivity().getResources().getDimension(
		// R.dimen.smallTextSize));
		// mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
		// .setBackgroundResource(R.drawable.tab_pressed); // selected
		// TextView tv = (TextView) mTabHost.getCurrentTabView()
		// .findViewById(android.R.id.title); // for Selected Tab
		tv.setTypeface(Typeface.DEFAULT_BOLD);
	}

	// @SuppressLint("InflateParams")
	// public TabSpec setIndicator(final TabSpec spec, int resid) {
	// // TODO Auto-generated method stub
	// // View v = LayoutInflater.from(getActivity()).inflate(
	// // R.layout.shop_tabs, null);
	// View v = getActivity().getLayoutInflater().inflate(R.layout.shop_tabs,
	// null);
	// v.setBackgroundResource(resid);
	// // LinearLayout container =
	// // (LinearLayout)v.findViewById(R.id.CUSTOMCONTAINER);
	// // container.setBackgroundResource(resid);
	// final TextView text = (TextView) v.findViewById(R.id.tabs_text);
	// text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getActivity()
	// .getResources().getDimensionPixelSize(R.dimen.tabsText));
	// text.setText(spec.getTag());
	// // text.setTextSize(getActivity().getResources().getDimension(
	// // R.dimen.smallTextSize));
	// text.setTextColor(getActivity().getResources().getColor(
	// android.R.color.white));
	// // text.setOnClickListener(new View.OnClickListener() {
	// //
	// // @Override
	// // public void onClick(View v) {
	// // if (mTabHost.getCurrentTabTag().equalsIgnoreCase(
	// // text.getText().toString())) {
	// // Fragment wall = getChildFragmentManager()
	// // .findFragmentByTag(text.getText().toString());
	// // if (wall instanceof WallFragment) {
	// // ((WallFragment) wall).getToFirst();
	// // } else if (wall instanceof TopTenFragment) {
	// // ((TopTenFragment) wall).getToFirst();
	// // }
	// // } else {
	// // mTabHost.setCurrentTabByTag(text.getText().toString());
	// // }
	// // }
	// // });
	// return spec.setIndicator(v);
	// }

	@SuppressLint("InflateParams")
	private View getTabIndicator(Context context, String title, int icon) {
		final View view = LayoutInflater.from(context).inflate(
				R.layout.tab_layout, null);

		// ImageView iv = (ImageView) view.findViewById(R.id.imageView);
		// iv.setImageResource(icon);
		final TextView tv = (TextView) view.findViewById(R.id.textView);
		tv.setText(title);
		return view;
	}

	private void switchTabs() {
		if (getActivity().getIntent().getExtras() != null) {
			int tab = getActivity().getIntent().getExtras()
					.getInt(ShopActivityTabs.WHICH_TAB);
			if (tab != 0) {
				switch (tab) {
				case ShopActivityTabs.NIKE_STORE_TAB:
					mTabHost.setCurrentTab(0);
					break;
				case ShopActivityTabs.PREMIUM_TAB:
					mTabHost.setCurrentTab(1);
					break;
				case ShopActivityTabs.POINTS_PACKAGES:
					mTabHost.setCurrentTab(2);
					break;

				default:
					break;
				}
			}
		}
	}

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		System.out.println("On Activity result parent");
		System.out.println(requestCode);
		if (resultCode == Activity.RESULT_OK) {

			if (requestCode == ShopActivityTabs.POINTS_REQUEST_CODE) {
				PointPAckages fragment = (PointPAckages) getChildFragmentManager()
						.findFragmentByTag(
								getResources().getString(
										R.string.points_packages));
				fragment.onActivityResult(requestCode, resultCode, data);
			} else if (requestCode == PointPAckages.loginRequestCode) {
				PointPAckages fragment = (PointPAckages) getChildFragmentManager()
						.findFragmentByTag(
								getResources().getString(
										R.string.points_packages));
				fragment.onActivityResult(requestCode, resultCode, data);
			} else if (requestCode == ShopActivityTabs.PREMIUM_REQUEST_CODE) {
				PremiumPackageFragment fragment = (PremiumPackageFragment) getChildFragmentManager()
						.findFragmentByTag(
								getResources().getString(
										R.string.premium_packages));
				fragment.onActivityResult(requestCode, resultCode, data);
			}

			else {
				PremiumPackageFragment fragment = (PremiumPackageFragment) getChildFragmentManager()
						.findFragmentByTag(
								getResources().getString(
										R.string.premium_packages));
				fragment.onActivityResult(requestCode, resultCode, data);
			}
			// refreshMenu();
		}
	}

}
