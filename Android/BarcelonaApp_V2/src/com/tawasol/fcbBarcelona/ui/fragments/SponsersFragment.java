package com.tawasol.fcbBarcelona.ui.fragments;

import com.tawasol.fcbBarcelona.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SponsersFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.sponsers_fragment	, container	, false);
		return rootView;
	}
}
