package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.Cursor; 
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener; 
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup; 
import android.widget.ProgressBar; 
import android.widget.GridView;
 






import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.ChatContactsEntity;
import com.tawasol.fcbBarcelona.entities.DeviceContactModel;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnChatContactsResponseListener;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.ui.adapters.ContactsChatAdapter;
import com.tawasol.fcbBarcelona.utils.UIUtils;

/**
 * 
 * @author Turki
 *
 */
public class ContactsFragment extends BaseFragment implements OnChatContactsResponseListener , OnRefreshListener{
	SwipeRefreshLayout swipeRefreshLayout;
	GridView suggestList;
	ContactsChatAdapter adapter;
	ProgressBar progress;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contact,	container, false);
//		initView(rootView);
		return rootView;
	}

//	private void initView(View rootView) {
//		progress           = (ProgressBar) rootView.findViewById(R.id.progress);
//		suggestList        = (GridView) rootView.findViewById(R.id.chat_contacts_grid_view);
//		swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
//		swipeRefreshLayout.setOnRefreshListener(this);
//		List<DeviceContactModel> contactsList = loadContacts();
//		ChatManager.getInstance().callChatContacts(contactsList);
//		if (adapter == null)
//		adapter = new ContactsChatAdapter(getActivity(), ChatManager.getInstance().getChatContactsList());
//		suggestList.setAdapter(adapter);
//	}

	@Override
	public void onResume() {
//		ChatManager.getInstance().addListener(this);
		super.onResume();
	}
	
	@Override
	public void onPause() {
//		ChatManager.getInstance().removeListener(this);
		super.onPause();
	}
	
	@Override
	public void onException(AppException ex) {
		swipeRefreshLayout.setRefreshing(false);
		progress.setVisibility(View.GONE);
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@SuppressLint("DefaultLocale")
	private List<DeviceContactModel> loadContacts() {
		List<DeviceContactModel> phoneList = new ArrayList<DeviceContactModel>();
		List<String> phones                = new ArrayList<String>();
		List<String> emails                = new ArrayList<String>();
		ContentResolver cr                 = getActivity().getContentResolver();
		Cursor cur                         = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		Cursor cursor = null;
		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));

				/** Get Contact Name **/
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

				if (Integer.parseInt(cur.getString(
						cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?", new String[] { id }, null);
					
					cursor = getActivity().getContentResolver().query(
							Email.CONTENT_URI, null, Email.CONTACT_ID + "=?",
							new String[] { id }, null);
					
					int emailIdx = cursor.getColumnIndex(Email.DATA);

					// let's just get the first email
					if (cursor.moveToFirst()) {
						emails.add(cursor.getString(emailIdx));
					}
					DeviceContactModel con = null;
					while (pCur.moveToNext()) {
						String phoneNo = pCur
								.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						phones.add(phoneNo);
						con = new DeviceContactModel(name, phones, emails);
					}
					phoneList.add(con);
					pCur.close();
				}
			}
		}
		return phoneList;
	}

	@Override
	public void onRefresh() {
		List<DeviceContactModel> contactsList = loadContacts();
		ChatManager.getInstance().callChatContacts(contactsList);
	}

	@Override
	public void onSuccess(List<ChatContactsEntity> objs) {
		progress.setVisibility(View.GONE);
		swipeRefreshLayout.setRefreshing(false);
		progress.setVisibility(View.GONE);
		adapter = new ContactsChatAdapter(getActivity(), objs);
		suggestList.setAdapter(adapter);
	}
}