package com.tawasol.fcbBarcelona.ui.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnMsgTextRecieved;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.managers.FanManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.utils.UIUtils;

/**
 * 
 * @author Turki
 *
 */
public class ChatInviteFragment extends BaseFragment implements
		OnMsgTextRecieved {

	private EditText invitationEt;
	private TextView inviteBtn;
	private static String appURL;
	private String invtationMsg;
	private ProgressDialog dialog;
	private LinearLayout apply_btn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.invite_fragment_new,
				container, false);

		dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		dialog.setCancelable(false);

		/** Initialize registration view **/
		init(rootView);

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		FanManager.getInstance().addListener(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		FanManager.getInstance().removeListener(this);
	}
	
	private void init(View rootView) {
		invitationEt = (EditText) rootView.findViewById(R.id.user_message_txt);
		apply_btn = (LinearLayout) rootView.findViewById(R.id.apply_btn);
		apply_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				invtationMsg = invitationEt.getText().toString();
				dialog.show();
				FanManager.getInstance().getInviteText(invtationMsg);
			}
		});
	}

	@Override
	public void onException(AppException ex) {
		dialog.dismiss();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onMsgTextReiceved(String msg) {
		dialog.dismiss();
		String shareBody = msg;
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				getActivity().getResources().getString(R.string.smsInvite_Subject));
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
		startActivity(Intent.createChooser(sharingIntent, "Share Using"));

	}
}
