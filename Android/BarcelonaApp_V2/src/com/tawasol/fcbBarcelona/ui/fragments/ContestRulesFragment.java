package com.tawasol.fcbBarcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.ContestData;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnContestReceived;
import com.tawasol.fcbBarcelona.managers.ContestManager;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.ValidatorUtils;

public class ContestRulesFragment extends BaseFragment implements OnContestReceived {
	WebView webView;
	ContestData contest;
	ProgressBar rulesPB;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contest_rules, container,
				false);

		// get view controls (web view and progress bar)
		webView = (WebView) rootView.findViewById(R.id.contest_rules_webview);
		rulesPB = (ProgressBar)rootView.findViewById(R.id.contest_rules_frag_pb);

		InitViewControls();
		
		//showLoadingDialog();
		rulesPB.setVisibility(View.VISIBLE);
		
		// get data from manager
				contest = ContestManager.getInstance().getContest();
				
				//showLoadingDialog();
				rulesPB.setVisibility(View.VISIBLE);
				
				// get the contest data from manager 
				if(contest == null)
					ContestManager.getInstance().getContestData();
				else
					if(!ValidatorUtils.isRequired(contest.getContestRules())){
						// load returned URL and hide the progress bar
						webView.loadUrl(contest.getContestRules());
						rulesPB.setVisibility(View.GONE);
					}
				
				

		return rootView;
	}

	
	/**Initialize the web view control and adjust settings**/
	@SuppressLint("NewApi")
	private void InitViewControls() {
		
		

		//if(NetworkingUtils.isNetworkConnected(getActivity())){
		//webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
//		}
//		else
//		{
		/** to get the data from cache if the URL was loaded before */
			webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//			  //webView.loadUrl(url);
//		}
		//webView.loadUrl("http://api.tawasoldev.com/hajji/Privacy_Policy/");//("file:///android_asset/" + filePath);
			
			/**show the Zoom controls (for large tablets)*/
		webView.getSettings().setBuiltInZoomControls(true);
		/**enable javaScript to handle actions of webView (redirect links for example)*/
		webView.getSettings().setJavaScriptEnabled(true);
		
		/** To set Transparent Background */
		webView.setBackgroundColor(0x00000000);
		if (Build.VERSION.SDK_INT >= 11)
			webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

		this.webView.setWebViewClient(new WebViewClient() {
			@SuppressLint("NewApi")
			@Override
			public void onPageFinished(WebView view, String url) {
				view.setBackgroundColor(0x00000000);
				if (Build.VERSION.SDK_INT >= 11)
					view.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
			}
		});
		
	}

	@Override
	public void onResume() {
		super.onResume();
		ContestManager.getInstance().addListener(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		ContestManager.getInstance().removeListener(this);
	}

	


	@Override
	public void onException(AppException ex) {
		//hideLoadingDialog();
		rulesPB.setVisibility(View.GONE);
		UIUtils.showToast(getActivity(), ex.getMessage());
	}


	@Override
	public void onSuccess(ContestData obj) {
		//hideLoadingDialog();
		rulesPB.setVisibility(View.GONE);
		// load the returned URL
		webView.loadUrl(obj.getContestRules());
		
	}
}
