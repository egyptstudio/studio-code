package com.tawasol.fcbBarcelona.ui.fragments;
  
import com.tawasol.fcbBarcelona.R;  
 
import com.tawasol.fcbBarcelona.entities.ContactUs;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnContactUsListener;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.ValidatorUtils;

import android.os.Bundle; 
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup; 
import android.widget.Button;
import android.widget.EditText;

/**
 * @author TURKI
 *  
 */
public class ContactUsFragment extends BaseFragment implements OnClickListener, OnContactUsListener{
 
	private EditText emailET, msgTitleET, msgBodyET;
	private String email    , msgTitle  , msgBody  ;
	private Button sendBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contact_us, container, false);
		
		/** Initialize contact us view **/
		init(rootView);

		return rootView;
	}

	/** Handle OnClickListeners **/
	@Override
	public void onClick(View v) {
		/** Get view values **/
		getValues();
		
		switch (v.getId()) {
		case R.id.send_btn:
			if(checkValidationField()){
				ContactUs contactEntity = new ContactUs(email, msgTitle, msgBody);
				UserManager.getInstance().callContactUs(contactEntity);
			}
			break; 
		}
	}

	/** Initialize contact us view **/
	private void init(View rootView) {
		emailET    = (EditText) rootView.findViewById(R.id.email_txt);
		msgTitleET = (EditText) rootView.findViewById(R.id.msg_title_Txt);
		msgBodyET  = (EditText) rootView.findViewById(R.id.msg_body_Txt);
		sendBtn    = (Button) rootView.findViewById(R.id.send_btn);
		sendBtn.setOnClickListener(this);
		
		if(UserManager.getInstance().getCurrentUser() != null
				&& UserManager.getInstance().getCurrentUserId() > 0){
			email = UserManager.getInstance().getCurrentUser().getEmail();
		}else
			emailET.setVisibility(View.VISIBLE);
	}

	/**  Get change password fields value **/
	private void getValues() {
		/** Get all values from user **/
		if(emailET.getVisibility() == View.VISIBLE)
			email      = emailET.getText().toString();
		msgTitle   = msgTitleET.getText().toString();
		msgBody    = msgBodyET.getText().toString();
	}


	/**  Validate validation fields **/
	private boolean checkValidationField() {

		if ((emailET.getVisibility() == View.VISIBLE && ValidatorUtils.isRequired(email)) ||
				ValidatorUtils.isRequired(msgTitle) || 	ValidatorUtils.isRequired(msgBody)) {
			UIUtils.showToast(getActivity(), getResources().getString(R.string.reg_fill_data_validation));
			return false;
		}else {
			return true;
		}
	}

	@Override
	public void onSuccess() {
		UIUtils.showToast(getActivity(), getActivity().getResources().getString(R.string.ContactUs_OnSend));
		getActivity().finish();
	}

	@Override
	public void onException(AppException ex) {
		UIUtils.showToast(getActivity(), ex.getMessage());
	}	
	
	/** Add ContactUS listener onResume **/
	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	/** Remove ContactUS listener onPause **/
	@Override
	public void onPause() {
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}
}
