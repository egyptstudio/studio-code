package com.tawasol.fcbBarcelona.ui.fragments;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnNikeStoreLinkRecieved;
import com.tawasol.fcbBarcelona.managers.ShopManager;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class NikeStore extends Fragment implements OnNikeStoreLinkRecieved{

	private ProgressDialog dialog;
	WebView nikeStore;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.nike_store, container, false);

		
		nikeStore = (WebView) rootView.findViewById(R.id.webView1);
		dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		dialog.setCancelable(false);
		
		/** Added by turki **/
		nikeStore.getSettings().setJavaScriptEnabled(true);   
		nikeStore.getSettings().setBuiltInZoomControls(true);
		nikeStore.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				return false;
			}
		});

		TextView offline = (TextView)rootView.findViewById(R.id.textView1);
		
		if(NetworkingUtils.isNetworkConnected(getActivity())){
			dialog.show();
			ShopManager.getInstance().getShopUrl();
			
		}else{
			offline.setVisibility(View.VISIBLE);
			nikeStore.setVisibility(View.GONE);
		}
		
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		ShopManager.getInstance().addListener(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		ShopManager.getInstance().removeListener(this);
	}
	
	@Override
	public void onException(AppException ex) {
		dialog.dismiss();
		if(ex.getErrorCode() != AppException.NO_DATA_EXCEPTION)
			UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onLinkRecieved(String link) {
		dialog.dismiss();
		nikeStore.loadUrl(link);
	}

}
