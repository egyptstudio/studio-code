package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.customView.PostViewFactory;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.data.cache.WallTable;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.exception.NullIntentException;
import com.tawasol.fcbBarcelona.listeners.OnLoginResponseListener;
import com.tawasol.fcbBarcelona.managers.SuperSonicManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.managers.WallManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.ForgetPassowrdActivity;
import com.tawasol.fcbBarcelona.ui.activities.HomeActivity;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.RegistrationActivity;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.Twitter_Util;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.Utils;
import com.tawasol.fcbBarcelona.utils.ValidatorUtils;

/**
 * @author Mohga
 * */
public class LogInFragment extends BaseFragment implements
		OnLoginResponseListener, ConnectionCallbacks,
		OnConnectionFailedListener, OnClickListener, OnDismissListener,
		OnCancelListener {

	/* Declarations */
	private final static int GOOGLE_PLUS_SIGN_IN = 101;
	int LogIntype;
	Button ForgetPassword_btn, NewAccount_btn;
	Button login_btn;
	EditText Email_txt, Password_txt;
	User LoggedInUser;
	String Email;
	ImageButton twitter_img;
	ImageButton google_img;
	LoginButton facebook_img;
	// FaceBook_Util faceUtil;
	static LogInFragment logfrag;
	Twitter_Util twitterUtil;
	int navigationTypeId;
	Intent intentAction;

	// declare Google Client
	GoogleApiClient mGoogleApiClient;
	private ConnectionResult mConnectionResult;
	// Track whether the sign-in button has been clicked
	// so that we know to resolve
	// all issues preventing sign-in without waiting.
	private boolean mSignInClicked = false;
	private CallbackManager callbackManager;

	/* End Of Declarations */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

		callbackManager = CallbackManager.Factory.create();

		LoginManager.getInstance().registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						// handlePendingAction();
						getUserInfo();
					}

					@Override
					public void onCancel() {
						showAlert();
					}

					@Override
					public void onError(FacebookException exception) {
						showAlert();
					}

					private void showAlert() {
						new AlertDialog.Builder(getActivity())
								.setTitle(R.string.cancelled)
								.setMessage(R.string.permission_not_granted)
								.setPositiveButton(R.string.PhotoDetails_OK,
										null).show();
					}
				});

	}

	protected void getUserInfo() {
		boolean enableButtons = AccessToken.getCurrentAccessToken() != null;

		if (enableButtons) {
			GraphRequest request = GraphRequest.newMeRequest(
					AccessToken.getCurrentAccessToken(),
					new GraphRequest.GraphJSONObjectCallback() {
						@Override
						public void onCompleted(JSONObject user,
								GraphResponse response) {
							getInfoFromFaceBookJson(user);
							user.optString("name");
						}
					});
			Bundle parameters = new Bundle();
			parameters.putString("fields",
					"id,name");
			request.setParameters(parameters);
			request.executeAsync();
		}
	}

	protected void getInfoFromFaceBookJson(JSONObject user) {
		User faceBookUser = new User();

		faceBookUser.setPhone("");
		faceBookUser.setDistrict("");
		faceBookUser.setCountryName("");

		// // set the user full name
		faceBookUser.setFullName(user.optString("name"));
		// // set the user email
		faceBookUser.setEmail("");

		// // set the user profile pic url
		faceBookUser.setProfilePic("https://graph.facebook.com/"
				+ user.optString("id") + "/picture?type=small");

		// // set social media id
		faceBookUser.setSocialMediaID(user.optString("id"));

		// // set user type
		faceBookUser.setUserType(User.USER_TYPE_SOCIAL_MEDIA);

		// // ------- Setting Gender -----
	     faceBookUser.setGender(0);
		// ------- end of setting gender ------

		// // set user BD
	    faceBookUser.setDateOfBirth(0);
		// ------ setting user location -------
		try {

			if (user.optString("location") != null) // first through home town
			{
				JSONObject obj = new JSONObject(user.optString("location"));

				String homeTown = obj.getString("name");

				String[] places = homeTown.trim().split(",");
				if (places != null && places.length >= 2) {
					faceBookUser.setCountryName(places[places.length - 1]);
					faceBookUser.setCityName(places[places.length - 2]);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		onLoginSuccess(faceBookUser, User.USER_TYPE_SOCIAL_MEDIA);
//		System.out.println(faceBookUser);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login, container,
				false);
		logfrag = LogInFragment.this;
		FindViewID(rootView);
		InitializeViewControls();
		setEmailTextIfExisted();
		HandleLoginType_And_Intent();

		// set the underline text of forget password button
		ForgetPassword_btn.setText(Html
				.fromHtml(getString(R.string.log_ForgetPassword_Underline)));

		// set the underline text of New Account button
		NewAccount_btn.setText(Html
				.fromHtml(getString(R.string.Login_register)));

		// Initiate Facebook Util class , set the fragment of auth button and UI
		// as well :)
		// faceUtil = new FaceBook_Util(getActivity(), savedInstanceState);
		// faceUtil.faceBookLogin((LogInActivity)getActivity() ,facebook_img,
		// LogInFragment.this);
		facebook_img.setBackgroundResource(R.drawable.facebook_login);
		facebook_img
				.setReadPermissions(Arrays
						.asList("public_profile, email, user_birthday, user_friends , user_location "));
		facebook_img.setFragment(this);
		facebook_img.setBackgroundResource(R.drawable.facebook_login);
		facebook_img.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		facebook_img.setPadding(0, 0, 0, 0);
		facebook_img.setFragment(this);

		// prepare Google builder , callback
		mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Plus.API)
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();
		mGoogleApiClient.connect();

		// Initiate Twitter Util class
		// twitterUtil = new Twitter_Util(getActivity());

		return rootView;
	}

	/** set the actions of the defined Controls */
	private void InitializeViewControls() {
		// set the click listener of Login
		login_btn.setOnClickListener(this);
		// set the click listener of Twitter
		twitter_img.setOnClickListener(this);
		// set the click listener of Google +
		google_img.setOnClickListener(this);
		// set the click listener of ForgetPassword
		ForgetPassword_btn.setOnClickListener(this);
		// set the click listener of New Account
		NewAccount_btn.setOnClickListener(this);
	}

	/**
	 * this method used when the user returned from register to login screen to
	 * fill the Email field !
	 **/
	private void setEmailTextIfExisted() {
		// get from arguments
		Bundle Data = getArguments();
		if (Data != null) {
			Email = (String) getArguments().getString("Email");
			if (Email != null && !Email.equals(""))
				Email_txt.setText(Email);
		}
	}

	/**
	 * this method should be called when starting this fragment to specify the
	 * loginType and Intent action if exists ! so it will handled on
	 * loginSuccess method :)
	 **/
	public void HandleLoginType_And_Intent() {
		this.navigationTypeId = getActivity().getIntent().getExtras()
				.getInt(LogInActivity.LOGIN_TYPE_KEY);
		this.intentAction = getActivity().getIntent().getExtras()
				.getParcelable(LogInActivity.INTENT_NAVIGATION_KEY);
	}

	public void hideSoftKeyboard() {
		if (getActivity().getCurrentFocus() != null) {
			InputMethodManager inputMethodManager = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(getActivity()
					.getCurrentFocus().getWindowToken(), 0);
		}
	}

	/** Initializing Controls from XML file ! */
	private void FindViewID(View rootView) {
		Email_txt = (EditText) rootView.findViewById(R.id.Email_txt_id);
		Password_txt = (EditText) rootView.findViewById(R.id.Password_txt_id);
		login_btn = (Button) rootView.findViewById(R.id.login_btn_id);
		NewAccount_btn = (Button) rootView
				.findViewById(R.id.LogIn_NewAccount_btn_id);
		ForgetPassword_btn = (Button) rootView
				.findViewById(R.id.LogIn_ForgetPassword_btn_id);
		facebook_img = (LoginButton) rootView
				.findViewById(R.id.log_facebook_auth_btn_id);
		twitter_img = (ImageButton) rootView
				.findViewById(R.id.log_twitter_img_id);
		google_img = (ImageButton) rootView
				.findViewById(R.id.log_google_img_id);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// faceUtil.loginresult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK
				&& requestCode == PostViewFactory.INTENT_REQUEST_CODE) {
			getActivity().setResult(Activity.RESULT_OK, data);
			getActivity().finish();
		}
		// if null then it's Google plus on activity result !
		if (requestCode == GOOGLE_PLUS_SIGN_IN) {
			if (resultCode != Activity.RESULT_OK) // Not RESULT_OK !
				mSignInClicked = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}

		}

		// else then it is Facebook result
		else {
			callbackManager.onActivityResult(requestCode, resultCode, data);
			// faceUtil.result(requestCode, resultCode, data);
			// if (Session.getActiveSession() != null)
			// Session.getActiveSession().onActivityResult(getActivity(),
			// requestCode, resultCode, data);
		}
	}

	public static Fragment newFragment() {
		return new LogInFragment();
	}

	/** To Client-Validate Data of Log in */
	private boolean isValidData() {
		String Email = Email_txt.getText().toString();
		String password = Password_txt.getText().toString();
		if (ValidatorUtils.isRequired(Email)
				&& ValidatorUtils.isRequired(password)) {
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.reg_fill_data_validation));
			return false;
		}
		// check required Email field
		if (ValidatorUtils.isRequired(Email)) {
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.Login_emailMissingAlert));
			return false;
		}
		// check valid Email format field
		else if (!ValidatorUtils.isValidEmail(Email)) {
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.email_formate_error));
			return false;
		}
		// check required password field
		else if (ValidatorUtils.isRequired(password)) {
			UIUtils.showToast(
					getActivity(),
					getResources().getString(
							R.string.Login_PasswordMissingMlert));
			return false;
		}
		// success
		else
			return true;
	}

	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}

	/*** Handle Responses ****/

	/* Google Plus Response Listener ! */
	/* A helper method to resolve the current ConnectionResult error. */
	private void resolveGoogleSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				((LogInActivity) getActivity()).startIntentSenderForResult(
						mConnectionResult.getResolution().getIntentSender(),
						GOOGLE_PLUS_SIGN_IN, null, 0, 0, 0);
			} catch (SendIntentException e) {
				// The intent was canceled before it was sent. Return to the
				// default
				// state and attempt to connect to get an updated
				// ConnectionResult.
				mGoogleApiClient.connect();
			}
		}
	}

	/**
	 * On Connection Failed , handle the connection result to try another
	 * connection
	 */
	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// Store the ConnectionResult so that we can use it later when the user
		// clicks 'sign-in'.
		mConnectionResult = arg0;

		if (mSignInClicked) {
			// The user has already clicked 'sign-in' so we attempt to resolve
			// all
			// errors until the user is signed in, or they cancel.
			resolveGoogleSignInError();
		}

	}

	/**
	 * On Connection Success Response --> get the user's Info and call
	 * loginWithSocialMedia WB
	 */
	@Override
	public void onConnected(Bundle arg0) {
		mSignInClicked = false;
		// Retrieve some profile information to personalize our app for the
		Person currentUser = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
		if (currentUser != null) {
			// UIUtils.showToast(getActivity(), currentUser.getDisplayName());

			String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
			User googlePlusUser = new User();
			googlePlusUser.setFullName(currentUser.getDisplayName());
			googlePlusUser.setSocialMediaID(currentUser.getId());
			googlePlusUser.setProfilePic(currentUser.getImage().getUrl());
			googlePlusUser.setDateOfBirth(0);
			googlePlusUser.setDistrict("");
			googlePlusUser.setCountryName("");
			googlePlusUser.setEmail(email);
			googlePlusUser.setUserType(User.USER_TYPE_SOCIAL_MEDIA);
			googlePlusUser.setPhone("");
			googlePlusUser.setGender(currentUser.getGender());

			GoogleSignOut();

			showLoadingDialog();
			UserManager.getInstance().loginWithSocialMedia(
					(BaseActivity) getActivity(), googlePlusUser,
					User.USER_TYPE_APP);
		}
	}

	void GoogleSignOut() {

		// Prior to disconnecting, run clearDefaultAccount().
		Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
		/*
		 * Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
		 * .setResultCallback(new ResultCallback<Status>() {
		 * 
		 * @Override public void onResult(Status arg0) { // TODO Auto-generated
		 * method stub } });
		 */
		mGoogleApiClient.disconnect();
		// mGoogleApiClient.connect();

	}

	/** On Connection Suspend , Connect again ! */
	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();

	}

	/* End Of Google Plus */

	/** On Log in Success Response handle */
	@Override
	public void onLoginSuccess(User user, int LoginState) {
		hideLoadingDialog();
		// if the response of " FB , Twitter " login , then call
		// loginWithSocialMedia WB
		if (LoginState == User.USER_TYPE_SOCIAL_MEDIA) {
			showLoadingDialog();
			UserManager.getInstance().loginWithSocialMedia(
					(BaseActivity) getActivity(), user, User.USER_TYPE_APP);
		}
		else if (LoginState == User.USER_TYPE_APP) {
			int anotherLoginID = SharedPrefrencesDataLayer.getIntPreferences(
					getActivity(), UserManager.LOGOUT_USER_ID_KEY, 0);
			if (user.getUserId() != anotherLoginID) {
				App.getInstance().clearApplicationData();
				// new clearHomeDataTask().execute();
				clearHomeData();
			}
						saveUser(user);
			UIUtils.showToast(getActivity(), getActivity().getString(R.string.hello) + user.getFullName());

			// fire notification service
//			App.getInstance().startNotificationService();
			procedNextScreen(navigationTypeId, intentAction);
			// getActivity().finish();
			SuperSonicManager.getInstance().initSuperSonicAgent(getActivity());
		}

	}

	/** Cache the user in external file storage */
	private void saveUser(User user) {
		UserManager.getInstance().cacheUser(user);
	}

	/** Handle the on Exception of Any request Failed */
	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		mSignInClicked = false;
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	/** Basyouni's Method to handle pending actions and navigations */
	void procedNextScreen(int login_Type, Intent intent) {
		switch (login_Type) {
		case UserManager.NORMAL_LOGIN:
			startActivity(HomeActivity.getActivityIntent(getActivity())/*
																		 * new
																		 * Intent
																		 * (
																		 * getActivity
																		 * (),
																		 * HomeActivity
																		 * .
																		 * class
																		 * )
																		 */);
			break;
		case UserManager.LOGIN_WITH_INTENT:
			if (intent != null) {
				startActivity(intent);
				getActivity().finish();
			} else
				throw new NullIntentException();
			break;
		case UserManager.LOGIN_FOR_RESULT:
			if (intent != null) {
				getActivity().setResult(Activity.RESULT_OK, intent);
				getActivity().finish();
			} else
				throw new NullIntentException();
			break;
		default:
			getActivity().finish();
			break;
		}
	}

	/**
	 * Handle ON Click of LogIn , Twitter , FB , G+ , Forget password and New
	 * Account buttons
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_btn_id:
			hideSoftKeyboard();
			if (isValidData()) {
				showLoadingDialog();
				LoggedInUser = new User();
				UserManager.getInstance().login((BaseActivity) getActivity(),
						Email_txt.getText().toString(),
						Password_txt.getText().toString(), User.USER_TYPE_APP);
				SharedPrefrencesDataLayer.saveIntPreferences(App.getInstance().getApplicationContext(),
						UserManager.LOGIN_TYPE, Params.APP_LOGIN);
			}
			break;

		case R.id.log_google_img_id:
			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				if (!mGoogleApiClient.isConnecting()
						&& mConnectionResult != null) {
					mSignInClicked = true;
					resolveGoogleSignInError();
				} else if (!mGoogleApiClient.isConnecting())
					mGoogleApiClient.connect();
			} else
				UIUtils.showToast(
						getActivity(),
						getResources().getString(
								R.string.connection_noConnection));
			break;

		case R.id.log_twitter_img_id:
			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				twitterUtil = new Twitter_Util(getActivity());
				showLoadingDialog();
				twitterUtil.loginToTwitter(LogInFragment.this, false);
				twitterUtil.getAuth_dialog().setOnCancelListener(this);
				twitterUtil.getAuth_dialog().setOnDismissListener(this);
			} else
				UIUtils.showToast(
						getActivity(),
						getResources().getString(
								R.string.connection_noConnection));
			break;

		case R.id.LogIn_ForgetPassword_btn_id:
			startActivity(ForgetPassowrdActivity.getActivityIntent(
					getActivity(), navigationTypeId, intentAction, Email_txt
							.getText().toString()));
			getActivity().finish();
			break;

		case R.id.LogIn_NewAccount_btn_id:
			if (navigationTypeId == UserManager.LOGIN_FOR_RESULT)
				getActivity().startActivityForResult(
						RegistrationActivity.getIntent(getActivity(),
								this.navigationTypeId, this.intentAction),
						PostViewFactory.INTENT_REQUEST_CODE);
			else
				startActivity(RegistrationActivity.getIntent(getActivity(),
						this.navigationTypeId, this.intentAction));
			break;

		default:
			break;
		}

	}

	/** Handle the Response when there is No E-mail in BE to be handled in FE */

	@Override
	public void onLoginWithSocialMediaNavigation(User user) {
		hideLoadingDialog();
		SharedPrefrencesDataLayer.saveIntPreferences(App.getInstance().getApplicationContext(),
				UserManager.LOGIN_TYPE, Params.SOCIAL_LOGIN);
		startActivity(RegistrationActivity.getIntent(getActivity(), LogIntype, intentAction, user));		getActivity().finish();
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		hideLoadingDialog();

	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		hideLoadingDialog();

	}

	/** Clear Home Data in manager and DB */
	private void clearHomeData() {
		WallManager.getInstance().clearAllData();
		WallTable.getInstance().deleteAll();
//		App.getInstance().runInBackground(new Runnable() {
//
//			@Override
//			public void run() {
//				WallTable.getInstance().deleteAll();
//			}
//		});

	}

}
