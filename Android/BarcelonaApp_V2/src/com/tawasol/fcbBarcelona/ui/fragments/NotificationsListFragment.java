package com.tawasol.fcbBarcelona.ui.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.WallTable;
import com.tawasol.fcbBarcelona.entities.Notification;
import com.tawasol.fcbBarcelona.entities.PostViewModel;
import com.tawasol.fcbBarcelona.entities.WALLPosts;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnNotificationsReceivedListener;
import com.tawasol.fcbBarcelona.listeners.OnPostReceived;
import com.tawasol.fcbBarcelona.managers.NotificationsManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.FanProfileActivity;
import com.tawasol.fcbBarcelona.ui.activities.ImageDetailsActivity;
import com.tawasol.fcbBarcelona.utils.UIUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * handle notifications view of type {Like , Comment , Following , Change
 * Profile , Post New Photos , Add Friend }
 * 
 * @author Mohga
 * */

public class NotificationsListFragment extends BaseFragment implements
		OnItemClickListener, OnPostReceived, OnNotificationsReceivedListener {

	private static NotificationsListFragment notificationListFrag;

	private ListView notificationsLV;
	NotificationsAdapter adapter;
	// NotificationsRequestsAdapter requestsAdapter;
	ArrayList<Notification> notifications;
	int NotificationViewType;
	LinearLayout listLayout, initialLayout;
	DisplayImageOptions profileOption;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_notifications_list,
				container, false);

		// get the list
		notificationsLV = (ListView) rootView
				.findViewById(R.id.notifications_list_view);

		// initialize the list
		notifications = new ArrayList<Notification>();

		// get notification form server
		// notifications = (ArrayList<Notification>)
		// NotificationsManager.getInstance().getCachedFilteredNotifications(Notification.LIKE_NOTIFICATIONS);

		// initialize the adapters
		adapter = new NotificationsAdapter();
		// requestsAdapter = new NotificationsRequestsAdapter(getActivity(),
		// notifications);

		// set the adapter
		notificationsLV.setAdapter(adapter);

		// get bundle data
		getBundleData();

		viewById(rootView);
		// initView(rootView);

		return rootView;
	}

	private void viewById(View rootView) {
		listLayout = (LinearLayout) rootView
				.findViewById(R.id.notifications_list_layout);
		initialLayout = (LinearLayout) rootView
				.findViewById(R.id.notifications_initial_state_layout);
	}

	private void initView() {

		profileOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1000)).build();
		/*
		 * notificationsLV.setPadding(0, 0, 0, ((BaseActivity)
		 * getActivity()).getBottomBarHeight());
		 */
		notificationsLV.setOnItemClickListener(this);

		notifications.clear();
		loadNotifications();
		if (notifications != null && !notifications.isEmpty())
			handleLayouts();
		else {
			showLoadingDialog();
			NotificationsManager.getInstance().getNotificationsFromServer();
		}
	}

	private void handleLayouts() {
		if (notifications != null && !notifications.isEmpty()) {
			adapter.notifyDataSetChanged();
			listLayout.setVisibility(View.VISIBLE);
			// setTabRead();
		} else {
			listLayout.setVisibility(View.GONE);
			initialLayout.setVisibility(View.VISIBLE);
		}
	}

	private void loadNotifications() {
		if (NotificationViewType == NotificationsFragment.LIKES)
			notifications.addAll((ArrayList<Notification>) NotificationsManager
					.getInstance().getCachedFilteredNotifications(
							Notification.LIKE_NOTIFICATIONS));
		else if (NotificationViewType == NotificationsFragment.COMMENTS)
			notifications.addAll((ArrayList<Notification>) NotificationsManager
					.getInstance().getCachedFilteredNotifications(
							Notification.COMMENT_NOTIFICATIONS));
		// else if (NotificationViewType == NotificationsFragment.MESSAGES)
		// notifications = (ArrayList<Notification>)
		// NotificationsManager.getInstance().getCachedFilteredNotifications(Notification.);
		/*
		 * else if (NotificationViewType == NotificationsFragment.REQUESTS) {
		 * notifications.addAll((ArrayList<Notification>)
		 * NotificationsManager.getInstance
		 * ().getrequested()(ArrayList<Notification>)
		 * NotificationsManager.getInstance
		 * ().getCachedFilteredNotifications(Notification
		 * .FRIEND_REQUEST_NOTIFICATIONS)); handleLayouts(); return; }
		 */
		else if (NotificationViewType == NotificationsFragment.FOLLOWING)
			notifications.addAll((ArrayList<Notification>) NotificationsManager
					.getInstance().getCachedFilteredNotifications(
							Notification.FOLLOWING_NOTIFICATIONS));
		else if (NotificationViewType == NotificationsFragment.FAVORITES) {
			notifications.addAll((ArrayList<Notification>) NotificationsManager
					.getInstance().getCachedFavoriteNotifications());
			makeFavNotificationRead();
		}
		if (notifications != null && !notifications.isEmpty())
			makeNotificationRead();

	}

	private void makeFavNotificationRead() {
		for (Notification notificatin : notifications) {
			NotificationsManager.getInstance().setFavTabReab(notificatin.getNotificationId());
		}
	}

	private void makeNotificationRead() {
		for (Notification notificatin : notifications) {
			NotificationsManager.getInstance().setNotificationRead(
					notificatin.getNotificationId());
		}
	}

	/*
	 * private void setTabRead() { if(NotificationViewType ==
	 * NotificationsFragment.LIKES)
	 * NotificationsManager.getInstance().setTabReab
	 * (Notification.LIKE_NOTIFICATIONS); else if(NotificationViewType ==
	 * NotificationsFragment.COMMENTS)
	 * NotificationsManager.getInstance().setTabReab
	 * (Notification.COMMENT_NOTIFICATIONS); else if(NotificationViewType ==
	 * NotificationsFragment.FOLLOWING)
	 * NotificationsManager.getInstance().setTabReab
	 * (Notification.FOLLOWING_NOTIFICATIONS); else if(NotificationViewType ==
	 * NotificationsFragment.FAVORITES)
	 * NotificationsManager.getInstance().setFavTabReab(); }
	 */
	private void getBundleData() {
		try {
			NotificationViewType = getArguments().getInt(
					NotificationsFragment.NOTIFCATION_TYPE);
		} catch (Exception e) {
			NotificationViewType = NotificationsFragment.LIKES; // default
		}

	}

	public static NotificationsListFragment newInstance() {
		if (notificationListFrag == null)
			notificationListFrag = new NotificationsListFragment();
		return notificationListFrag;
	}

	@Override
	public void onResume() {
		super.onResume();
		NotificationsManager.getInstance().addListener(this);
		initView();
	}

	@Override
	public void onPause() {
		super.onPause();
		NotificationsManager.getInstance().removeListener(this);
	}

	/*
	 * @Override public void onSuccess(List<Notification> objs) { if(objs !=
	 * null && !objs.isEmpty()) { if(notifications != null &&
	 * !notifications.isEmpty()) { listLayout.setVisibility(View.VISIBLE);
	 * notifications.clear(); notifications.addAll(objs);
	 * adapter.notifyDataSetChanged(); } else {
	 * listLayout.setVisibility(View.GONE);
	 * initialLayout.setVisibility(View.VISIBLE); }
	 * 
	 * }
	 * 
	 * }
	 */

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		// UIUtils.showToast(getActivity(), ex.getMessage());
		handleLayouts();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		// // int index = (Integer) v.getTag();
		// // if(index != -1 && index != 0 )
		// if (notifications.get(position).getNotificationType() ==
		// Notification.POST_NEW_PHOTO_NOTIFICATIONS
		// || notifications.get(position).getNotificationType() ==
		// Notification.LIKE_NOTIFICATIONS
		// || notifications.get(position).getNotificationType() ==
		// Notification.COMMENT_NOTIFICATIONS){
		// PostViewModel post = WallTable.getInstance().getPostById(
		// notifications.get(position).getPostId());
		// if (post != null)
		// startActivity(ImageDetailsActivity.getActivityIntent(
		// getActivity(), post, 0, 0, false, 0, 0));
		// else {
		// showLoadingDialog();
		// NotificationsManager.getInstance().getPostDetails(
		// notifications.get(position).getPostId());
		// }
		// } else if (notifications.get(position).getNotificationType() ==
		// Notification.FOLLOWING_NOTIFICATIONS
		// || notifications.get(position).getNotificationType() ==
		// Notification.CHANGE_RROFILE_NOTIFICATIONS
		// || notifications.get(position).getNotificationType() ==
		// Notification.ADD_FRIEND_NOTIFICATIONS
		// ) {
		// startActivity(FanProfileActivity.getIntent(getActivity(),
		// notifications.get(position).getSenderId(), notifications
		// .get(position).getSenderFullName()));
		// }
		// NotificationsManager.getInstance().setNotificationRead(
		// notifications.get(position).getNotificationId());
		// ((BaseActivity) getActivity()).refreshMenu();
		// // NotificationsFragment fragment = (NotificationsFragment)
		// getActivity().getSupportFragmentManager().findFragmentByTag("notifications_fragment");
		// // if(fragment != null){
		// // fragment.updateTab();
		// // }
		// NotificationsFragment.updateTab();

	}

	private class NotificationsAdapter extends BaseAdapter {

		Notification currentItem, previousItem;

		@Override
		public int getCount() {
			return notifications.size();
		}

		@Override
		public Object getItem(int position) {
			return notifications.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			final NotificationsViewHolder holder;
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				holder = new NotificationsViewHolder();
				convertView = inflater.inflate(R.layout.notification_item,
						parent, false);

				holder.notificationTxt = (TextView) convertView
						.findViewById(R.id.notification_txt);
				holder.dateTxt = (TextView) convertView
						.findViewById(R.id.notification_date);
				holder.notifierImg = (ImageView) convertView
						.findViewById(R.id.notification_img);
				holder.imgLayout = (RelativeLayout) convertView
						.findViewById(R.id.notification_image_layout);
				holder.notificationPB = (ProgressBar) convertView
						.findViewById(R.id.notification_progress_bar);
				holder.dateGroupingTxt = (TextView) convertView
						.findViewById(R.id.date_grouping_txt);
				holder.dateLayout = (RelativeLayout) convertView
						.findViewById(R.id.notifications_date_layout);
				holder.notificationLayout = (RelativeLayout) convertView
						.findViewById(R.id.notification_layout_id);

				holder.notificationLayout.setTag(position);
				convertView.setTag(holder);
			} else
				holder = (NotificationsViewHolder) convertView.getTag();

			// set the data for Like Notifications
			if (notifications.get(position).getNotificationType() == Notification.LIKE_NOTIFICATIONS) {
				holder.notificationTxt.setText(notifications.get(position)
						.getSenderFullName()
						+ " "
						+ getResources().getString(
								R.string.notifications_liked_photo));
				// holder.imgLayout.setVisibility(View.GONE);
			}

			else if (notifications.get(position).getNotificationType() == Notification.COMMENT_NOTIFICATIONS) {
				holder.notificationTxt.setText(notifications.get(position)
						.getSenderFullName()
						+ " "
						+ getResources().getString(
								R.string.notifications_commented_on_photo));
				// holder.imgLayout.setVisibility(View.GONE);
			}
			// favorite cases
			else if (notifications.get(position).getNotificationType() == Notification.FOLLOWING_NOTIFICATIONS)
				holder.notificationTxt.setText(notifications.get(position)
						.getSenderFullName()
						+ " "
						+ getResources().getString(
								R.string.notifications_following_photo));

			else if (notifications.get(position).getNotificationType() == Notification.ADD_FRIEND_NOTIFICATIONS)
				holder.notificationTxt.setText(notifications.get(position)
						.getSenderFullName()
						+ " "
						+ getResources().getString(
								R.string.notifications_added_friend));

			else if (notifications.get(position).getNotificationType() == Notification.CHANGE_RROFILE_NOTIFICATIONS)
				holder.notificationTxt
						.setText(notifications.get(position)
								.getSenderFullName()
								+ " "
								+ getResources()
										.getString(
												R.string.notifications_changed_profile_picture));

			else if (notifications.get(position).getNotificationType() == Notification.POST_NEW_PHOTO_NOTIFICATIONS)
				holder.notificationTxt.setText(notifications.get(position)
						.getSenderFullName()
						+ " "
						+ getResources().getString(
								R.string.notifications_posted_new_photo));

			holder.imgLayout.setVisibility(View.VISIBLE);
			DisplayImage(notifications.get(position).getSenderImgUrl(),
					holder.notifierImg, holder.notificationPB);

			holder.dateTxt.setText(setTime(notifications.get(position)
					.getTime()));

			// highlight the notification if not read :)
			if (!notifications.get(position).isRead())
				holder.notificationLayout
						.setBackgroundResource(R.drawable.notification_new_list_bg);
			else
				holder.notificationLayout
						.setBackgroundResource(R.drawable.notification_list_bg);

			convertView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// int index = (Integer) v.getTag();
					// if(index != -1 && index != 0 )
					if (notifications.get(position).getNotificationType() == Notification.POST_NEW_PHOTO_NOTIFICATIONS
							|| notifications.get(position)
									.getNotificationType() == Notification.LIKE_NOTIFICATIONS
							|| notifications.get(position)
									.getNotificationType() == Notification.COMMENT_NOTIFICATIONS) {
						PostViewModel post = WallTable
								.getInstance()
								.getPostById(
										notifications.get(position).getPostId());
						if (post != null)
							startActivity(ImageDetailsActivity
									.getActivityIntent(getActivity(), post, 0,
											0, false, 0, 0));
						else {
							showLoadingDialog();
							NotificationsManager.getInstance().getPostDetails(
									notifications.get(position).getPostId());
						}
					} else if (notifications.get(position)
							.getNotificationType() == Notification.FOLLOWING_NOTIFICATIONS
							|| notifications.get(position)
									.getNotificationType() == Notification.CHANGE_RROFILE_NOTIFICATIONS
							|| notifications.get(position)
									.getNotificationType() == Notification.ADD_FRIEND_NOTIFICATIONS) {
						startActivity(FanProfileActivity
								.getIntent(getActivity(),
										notifications.get(position)
												.getSenderId(), notifications
												.get(position)
												.getSenderFullName()));
					}
					NotificationsManager.getInstance().setNotificationRead(
							notifications.get(position).getNotificationId());
					((BaseActivity) getActivity()).refreshMenu();
					// NotificationsFragment fragment = (NotificationsFragment)
					// getActivity().getSupportFragmentManager().findFragmentByTag("notifications_fragment");
					// if(fragment != null){
					// fragment.updateTab();
					// }
					NotificationsFragment.updateTab();
					holder.notificationLayout
							.setBackgroundResource(R.drawable.notification_list_bg);
				}
			});

			// set the on click of view
			/*
			 * holder.notificationLayout.setTag(position);
			 * holder.notificationLayout.setOnClickListener(new
			 * OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { int index = (Integer)
			 * v.getTag(); if(index != -1 && index != 0 )
			 * if(notifications.get(index).getNotificationType() ==
			 * Notification.LIKE_NOTIFICATIONS ||
			 * notifications.get(index).getNotificationType() ==
			 * Notification.COMMENT_NOTIFICATIONS ||
			 * notifications.get(index).getNotificationType() ==
			 * Notification.POST_NEW_PHOTO_NOTIFICATIONS){ PostViewModel post =
			 * WallTable
			 * .getInstance().getPostById(notifications.get(index).getPostId());
			 * if(post != null)
			 * activity.startActivity(ImageDetailsActivity.getActivityIntent
			 * (activity, post, 0, 0, false, 0, 0)); else
			 * NotificationsManager.getInstance
			 * ().getPostDetails(notifications.get(index).getPostId()); } else
			 * if(notifications.get(index).getNotificationType() ==
			 * Notification.FOLLOWING_NOTIFICATIONS ||
			 * notifications.get(index).getNotificationType() ==
			 * Notification.CHANGE_RROFILE_NOTIFICATIONS ||
			 * notifications.get(index).getNotificationType() ==
			 * Notification.ADD_FRIEND_NOTIFICATIONS) {
			 * activity.startActivity(FanProfileActivity.getIntent(activity,
			 * notifications.get(index).getSenderId(),
			 * notifications.get(index).getSenderFullName())); }
			 * NotificationsManager
			 * .getInstance().setNotificationRead(notifications
			 * .get(index).getNotificationId());
			 * ((BaseActivity)activity).refreshMenu(); } });
			 */

			// Current Item
			try {
				currentItem = (Notification) getItem(position);
			} catch (Exception e) {
			}

			try {
				previousItem = (Notification) getItem(position - 1);
			} catch (Exception e) {
				previousItem = null;
			}

			if (previousItem == null) {
				previousItem = currentItem;
				holder.dateLayout.setVisibility(View.VISIBLE);
				holder.dateGroupingTxt.setText(setDate(currentItem.getTime()));
			} else {
				if (isDateMatched(currentItem, previousItem)) {
					holder.dateLayout.setVisibility(View.GONE);
				} else {
					holder.dateLayout.setVisibility(View.VISIBLE);
					holder.dateGroupingTxt.setText(setDate(currentItem
							.getTime()));
				}

				previousItem = currentItem;
			}

			return convertView;
		}

		private void DisplayImage(String imageUrl, final ImageView imgView,
				final ProgressBar progressBar) {
			App.getInstance()
					.getImageLoader()
					.displayImage(imageUrl, imgView, profileOption,
							new ImageLoadingListener() {

								@Override
								public void onLoadingStarted(String arg0,
										View arg1) {
								}

								@Override
								public void onLoadingFailed(String arg0,
										View arg1, FailReason arg2) {
									progressBar.setVisibility(View.GONE);
									imgView.setImageResource(R.drawable.profile_icon);
								}

								@Override
								public void onLoadingComplete(String arg0,
										View arg1, Bitmap arg2) {
									progressBar.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingCancelled(String arg0,
										View arg1) {
								}
							});
		}

		private String setTime(long date) {

			SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a",
					Locale.getDefault());
			dateFormat.setTimeZone(TimeZone.getDefault());
			return dateFormat.format(new Date(date * 1000));
		}

		private String setDate(long date) {

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",
					Locale.getDefault());
			dateFormat.setTimeZone(TimeZone.getDefault());
			return dateFormat.format(new Date(date * 1000));
		}

		private boolean isDateMatched(Notification currentNotification,
				Notification previousNotification) {
			Calendar currentCalendar = Calendar.getInstance();
			currentCalendar.setTime(new Date(
					currentNotification.getTime() * 1000));
			int currentYear = currentCalendar.get(Calendar.YEAR);
			int currentMonth = currentCalendar.get(Calendar.MONTH);
			int currentDay = currentCalendar.get(Calendar.DAY_OF_MONTH);

			Calendar previousCalendar = Calendar.getInstance();
			previousCalendar.setTime(new Date(
					previousNotification.getTime() * 1000));
			int previousYear = previousCalendar.get(Calendar.YEAR);
			int previousMonth = previousCalendar.get(Calendar.MONTH);
			int previousDay = previousCalendar.get(Calendar.DAY_OF_MONTH);

			if (currentYear == previousYear && currentMonth == previousMonth
					&& currentDay == previousDay)
				return true;
			else
				return false;

		}

		private class NotificationsViewHolder {
			TextView notificationTxt;
			TextView dateTxt;
			ImageView notifierImg;
			ProgressBar notificationPB;
			RelativeLayout imgLayout;
			TextView dateGroupingTxt;
			RelativeLayout dateLayout;
			RelativeLayout notificationLayout;

		}

	}

	@Override
	public void onSuccess(WALLPosts obj) {
		hideLoadingDialog();
		if (obj != null && obj.getPosts() != null && !obj.getPosts().isEmpty()
				&& obj.getPosts().get(0) != null)
			startActivity(ImageDetailsActivity.getActivityIntent(getActivity(),
					obj.getPosts().get(0), -1, 0, false, 0, 0));
		else
			UIUtils.showToast(getActivity(),
					getActivity().getString(R.string.no_post));

	}

	@Override
	public void onSuccess(List<Notification> objs) {
		if (objs != null) {
			notifications.clear();
			loadNotifications();
			if (notifications != null && !notifications.isEmpty())
				handleLayouts();
			else {
				showLoadingDialog();
				NotificationsManager.getInstance().getNotificationsFromServer();
				handleLayouts();
			}

			/*
			 * if(notifications!= null ) list.addAll(notifications);
			 * notifications.clear(); notifications.addAll(list);
			 * notifications.addAll(objs); adapter.notifyDataSetChanged();
			 * handleLayouts();
			 */
		}
	}

}
