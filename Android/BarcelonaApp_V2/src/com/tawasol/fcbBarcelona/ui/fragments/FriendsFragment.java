package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.data.cache.FriendTable;
import com.tawasol.fcbBarcelona.entities.FriendsEntity;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnBackgroundTask;
import com.tawasol.fcbBarcelona.listeners.OnChatContactUpdatedListener;
import com.tawasol.fcbBarcelona.listeners.OnChatFriendsResponseListener;
import com.tawasol.fcbBarcelona.listeners.OnVerifyPhoneListener;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.pushnotifications.BackgroundTask;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.ChatInviteActivity;
import com.tawasol.fcbBarcelona.ui.activities.VerifyPhoneActivity;
import com.tawasol.fcbBarcelona.ui.adapters.FriendsChatAdapter;
import com.tawasol.fcbBarcelona.utils.UIUtils;

/**
 * 
 * @author Turki
 *
 */
@SuppressLint("NewApi")
public class FriendsFragment extends BaseFragment implements
		OnChatFriendsResponseListener, OnChatContactUpdatedListener,
		SearchView.OnQueryTextListener, SearchView.OnCloseListener,
		OnVerifyPhoneListener {

	private GridView friendsGrid;
	private List<FriendsEntity> friendsList = new ArrayList<FriendsEntity>();
	private FriendsChatAdapter adapter;
	private boolean NoMoreFriendToLoading = true;
	private RelativeLayout loadingPanel;
	private boolean isLoading;
	private View verificationView;
	private Button verifyButton;
	private Button help;
	private Button inviteBtn;
	private SearchView mSearchView;
	private FrameLayout noFriendsMsgFrame;
	public static int mLastStartingLimit = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_friends, container,
				false);
		friendsList.clear();
		mLastStartingLimit = 0;
		/** Initialize registration view **/
		init(rootView);

		return rootView;
	}

	@SuppressLint("NewApi")
	private void init(View rootView) {
		loadingPanel = (RelativeLayout) rootView
				.findViewById(R.id.loadingPanel);
		friendsGrid = (GridView) rootView
				.findViewById(R.id.chat_friends_grid_view);

		noFriendsMsgFrame = (FrameLayout) rootView
				.findViewById(R.id.initial_no_friends_msg);
		inviteBtn = (Button) rootView.findViewById(R.id.invite_btn);
		if (!ChatManager.getInstance().isInviteFriendBefor()) {
			noFriendsMsgFrame.setVisibility(View.VISIBLE);
			friendsGrid.setVisibility(View.GONE);
		}
		inviteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getActivity().startActivity(
						ChatInviteActivity.getActivityIntent(getActivity()));
			}
		});

		adapter = new FriendsChatAdapter(getActivity(), friendsList,
				getActivity().getClass().getSimpleName()
						.equalsIgnoreCase("FriendsList") ? false : true);
		friendsGrid.setAdapter(adapter);
		friendsGrid.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (adapter == null) {
					return;
				}

				// if(adapter.getCount() == 0)
				// return;

				int l = visibleItemCount + firstVisibleItem;

				if (l >= totalItemCount && !isLoading && NoMoreFriendToLoading) {
					// set progress boolean
					isLoading = true;
					loadingPanel.setVisibility(View.VISIBLE);
					ChatManager.getInstance().callLoadingFriends();
				}
			}
		});

		/** For phone verification **/
		verificationView = rootView.findViewById(R.id.pop_up_phone_verify);
		verificationView.setVisibility(View.GONE);
		verifyButton = (Button) rootView.findViewById(R.id.verifytypebutton);
		help = (Button) rootView.findViewById(R.id.helpButton);
		handleVerficationView(verificationView);

		mSearchView = (SearchView) getActivity().findViewById(
				R.id.chat_search_friends);
		mSearchView.setOnSearchClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((BaseActivity) getActivity()).hideTitleBar();
				((BaseActivity) getActivity()).hideChatInviteFriends();
			}
		});
		((BaseActivity) getActivity()).getSearchBtn().setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						mSearchView.setIconified(false);
						((BaseActivity) getActivity()).hideChatSearchBtn();
					}
				});

		LinearLayout listLayout = (LinearLayout) rootView
				.findViewById(R.id.postLayout);
		
		listLayout.setPadding(0, 0, 0, (int) (BaseActivity.bottomBarHeight - (70 + convertPixelsToDp(20, getActivity()))));
		AdView mAdView = (AdView) rootView.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
		final LinearLayout adContainer = (LinearLayout) rootView
				.findViewById(R.id.adsLayout);
		mAdView.setAdListener(new AdListener() {
			@Override
			public void onAdFailedToLoad(int errorCode) {
				super.onAdFailedToLoad(errorCode);
				adContainer.setVisibility(View.GONE);
			}
		});
		setupSearchView();
	}

	public static float convertPixelsToDp(float px, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;
	}
	
	private void setupSearchView() {
		mSearchView.setOnQueryTextListener(this);
		mSearchView.setOnCloseListener(this);
		mSearchView.setQueryHint(" ");
	}

	/** Add ChatFriends listeners onResume **/
	@Override
	public void onResume() {
//		if (friendsList != null)
//			friendsList.clear();
		if (adapter != null)
			adapter.notifyDataSetChanged();

		ChatManager.getInstance().addListener(this);
		if (FriendTable.getInstance().getCount() != 0)
			ChatManager.getInstance().loadCachedContactsAsync();
		else {
			if (!isLoading) {
				isLoading = true;
				loadingPanel.setVisibility(View.VISIBLE);
				ChatManager.getInstance().callLoadingFriends();
			}
		}
		super.onResume();
	}

	/** Remove ChatFriends listeners onPause **/
	@Override
	public void onPause() {
		ChatManager.getInstance().removeListener(this);
		super.onPause();
	}

	
	@Override
	public void onSuccess(List<FriendsEntity> friendsFilterEntity) {
		loadingPanel.setVisibility(View.INVISIBLE);
		isLoading = false;
		if (friendsFilterEntity != null && friendsFilterEntity.size() > 0) {
			noFriendsMsgFrame.setVisibility(View.GONE); /*edit*/
//			noFriendsMsgFrame.setVisibility(View.VISIBLE);
			if (mLastStartingLimit == 0) {
				friendsList.clear();
//				FriendsEntity friendsEntity = new FriendsEntity();
//				friendsEntity.setFanName("no name");
//				friendsFilterEntity.add(0, friendsEntity);
				saveFriendsToDatabase(friendsFilterEntity, true);
			} else
				saveFriendsToDatabase(friendsFilterEntity, false);

			mLastStartingLimit += friendsFilterEntity.size();
			friendsList.addAll(friendsFilterEntity);
			Collections.sort(friendsList);
			FriendsEntity fEntity = new FriendsEntity();
			fEntity.setFanName("a");
			friendsList.add(0, fEntity);
			adapter.notifyDataSetChanged();
			friendsGrid.setVisibility(View.VISIBLE);
		} else {
			NoMoreFriendToLoading = false;
//			if (friendsList != null && friendsList.isEmpty()) {
//				friendsList.add(0, new FriendsEntity());
//				noFriendsMsgFrame.setVisibility(View.VISIBLE);
//				friendsGrid.setVisibility(View.GONE);
//			}
		}

	}

	@Override
	public void onException(AppException ex) {
		isLoading = false;
		NoMoreFriendToLoading = false;
		loadingPanel.setVisibility(View.INVISIBLE);
		// if(ex.getErrorCode() == AppException.NO_DATA_EXCEPTION && friendsList
		// != null && friendsList.isEmpty()){
		// noFriendsMsgFrame.setVisibility(View.VISIBLE);
		// friendsGrid.setVisibility(View.GONE);
		// }else{
		UIUtils.showToast(getActivity(), ex.getMessage());
		// }
	}

	@Override
	public void onSuccess() {
		if (!isLoading)
			loadingPanel.setVisibility(View.INVISIBLE);
		List<FriendsEntity> list = ChatManager.getInstance().getContacts();
		if (list.size() > 0) {
			noFriendsMsgFrame.setVisibility(View.GONE); /*edit*/
//			noFriendsMsgFrame.setVisibility(View.VISIBLE);
			friendsList.clear();
			friendsList.addAll(list);
			Collections.sort(friendsList);
			FriendsEntity fEntity = new FriendsEntity();
			fEntity.setFanName("a");
			friendsList.add(0, fEntity);
//			friendsList.add(0, new FriendsEntity());
			adapter.notifyDataSetChanged();
		}
		// ChatManager.getInstance().callLoadingFriends();
	}

	private void handleVerficationView(View view) {
		if (!UserManager.getInstance().getCurrentUser().isPhoneVerified())
			view.setVisibility(View.GONE);
		else
			view.setVisibility(View.GONE);
		verifyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UserManager.getInstance().callVerifyPhone();
			}
		});
		help.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showHelpDialog(v);
			}
		});
	}

	/** Initialize of help PopUp **/
	private void showHelpDialog(View view) {
		final View popupView = LayoutInflater.from(getActivity()).inflate(
				R.layout.chat_help_dialog, null);
		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.setOutsideTouchable(true);
		popupWindow.setFocusable(true);
		popupWindow.showAsDropDown(view, 0, 0/* , Gravity.LEFT */);
	}

	@Override
	public boolean onClose() {
		((BaseActivity) getActivity()).showTitleBar();
		((BaseActivity) getActivity()).showChatInviteFriends();
		((BaseActivity) getActivity()).showChatSearchBtn();
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		adapter.getFilter().filter(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	private void saveFriendsToDatabase(final List<FriendsEntity> friends,
			final boolean clearFriendDB) {
		OnBackgroundTask backgroundTaskListener = new OnBackgroundTask() {

			@Override
			public void onTaskCompleted() {
				// TODO Auto-generated method stub
				FriendTable.getInstance().insert(friends);
			}

			@Override
			public void doTaskInBackground() {
				// TODO Auto-generated method stub
				if (clearFriendDB) {
					FriendTable.getInstance().deleteAll();
				}
			}
		};
		new BackgroundTask(backgroundTaskListener).execute();
	}

	@Override
	public void onSuccess(User userPhoneVerified) {
		getActivity().startActivity(
				VerifyPhoneActivity.getActivityIntent(getActivity()));
	}
}