package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.List;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.BlockedEntity;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnBlockedUsersRecieved;
import com.tawasol.fcbBarcelona.listeners.OnSuccessVoidListener;
import com.tawasol.fcbBarcelona.listeners.OnUnBlockSuccess;
import com.tawasol.fcbBarcelona.managers.FanManager;
import com.tawasol.fcbBarcelona.utils.UIUtils;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BlockedListFragment extends Fragment implements
		OnBlockedUsersRecieved, OnUnBlockSuccess {

	ListView blockedList;
	RelativeLayout noUsers;
	ProgressBar blockedListProgress;
	ProgressDialog unBlockDialog;
	BlockedListAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.blocked_list_fragment,
				container, false);
		FanManager.getInstance().getBlockedList();
		unBlockDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		unBlockDialog
				.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		unBlockDialog.setCancelable(false);
		blockedList = (ListView) rootView.findViewById(R.id.listView1);
		noUsers = (RelativeLayout) rootView.findViewById(R.id.no_blocked_users);
		blockedListProgress = (ProgressBar) rootView
				.findViewById(R.id.blocked_list_progress);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		FanManager.getInstance().addListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		FanManager.getInstance().removeListener(this);
	}

	private class BlockedListAdapter extends BaseAdapter {

		List<BlockedEntity> users;

		public BlockedListAdapter(List<BlockedEntity> users) {
			this.users = users;
		}

		@Override
		public int getCount() {
			return this.users.size();
		}

		@Override
		public Object getItem(int position) {
			return this.users.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rootView = convertView;
			if (rootView == null)
				rootView = LayoutInflater.from(
						App.getInstance().getApplicationContext()).inflate(
						R.layout.blocked_list_item, parent, false);

			final BlockedEntity user = users.get(position);

			TextView userName = (TextView) rootView.findViewById(R.id.userName);

			Button unBlockUser = (Button) rootView
					.findViewById(R.id.unblock_button);

			userName.setText(user.getUsername());
			unBlockUser.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					unBlockDialog.show();
					FanManager.getInstance().unBlockUser(user.getFanId());
				}
			});

			return rootView;
		}

		public void deleteRow(int fanId) {
			unBlockDialog.dismiss();
			this.users.remove(this.users.indexOf(new BlockedEntity(fanId)));
			notifyDataSetChanged();
			if (this.users.isEmpty()) {
				noUsers.setVisibility(View.VISIBLE);
				blockedList.setVisibility(View.GONE);
			}
		}

	}

	@Override
	public void onSuccess(List<BlockedEntity> objs) {
		blockedListProgress.setVisibility(View.GONE);
		if (objs.isEmpty()) {
			noUsers.setVisibility(View.VISIBLE);
			blockedList.setVisibility(View.GONE);
		} else {
			adapter = new BlockedListAdapter(objs);
			blockedList.setAdapter(adapter);
		}
	}

	@Override
	public void onException(AppException ex) {
		unBlockDialog.dismiss();
		blockedListProgress.setVisibility(View.GONE);
		if (ex.getErrorCode() != AppException.NO_DATA_EXCEPTION)
			UIUtils.showToast(getActivity(), ex.getMessage());
		else {
			noUsers.setVisibility(View.VISIBLE);
			blockedList.setVisibility(View.GONE);
		}

	}

	@Override
	public void onSuccess(int fanId) {
		adapter.deleteRow(fanId);
	}

}
