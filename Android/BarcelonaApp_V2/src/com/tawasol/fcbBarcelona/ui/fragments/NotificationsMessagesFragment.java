package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.Message;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnChatUpdatedListener;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.adapters.RecentChatAdapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

/**
 * handle notification recent arrived messages view 
 * @author Mohga 
 * */

public class NotificationsMessagesFragment extends BaseFragment implements OnChatUpdatedListener {

	private static NotificationsMessagesFragment notificationMessagesFrag;

	private ListView notificationsLV;
	RecentChatAdapter adapter;
	ArrayList<Message> messages;
	LinearLayout messagesLayout,initialStateLayout;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_notifications_messages_list,
				container, false);
		
		// get the list
		notificationsLV = (ListView)rootView.findViewById(R.id.messages_list_view);
		messagesLayout = (LinearLayout)rootView.findViewById(R.id.notifications_messages_layout);
		initialStateLayout = (LinearLayout)rootView.findViewById(R.id.notifications_initial_state_layout);
		
		//initialize the list
		messages = new ArrayList<Message>();
		
		// get notification form server
		//notifications = (ArrayList<Notification>) NotificationsManager.getInstance().getCachedFilteredNotifications(Notification.LIKE_NOTIFICATIONS);

		
		// initialize the adapters
		adapter = new RecentChatAdapter(getActivity(), messages);
		
		// set the adapter
		notificationsLV.setAdapter(adapter);
		
		notificationsLV.setPadding(0, 0, 0,
				((BaseActivity) getActivity()).getBottomBarHeight());

		

		
		return rootView;
	}

	
	
	
	

	public static NotificationsMessagesFragment newInstance() {
		if (notificationMessagesFrag == null)
			notificationMessagesFrag = new NotificationsMessagesFragment();
		return notificationMessagesFrag;
	}

@Override
	public void onResume() {
		super.onResume();
		ChatManager.getInstance().addListener(this);
		
		// get the recent messages 
				showLoadingDialog();
				ChatManager.getInstance().loadRecentMessagesAsync();
	}

	@Override
	public void onPause() {
		super.onPause();
		ChatManager.getInstance().removeListener(this);
	}






	@Override
	public void onSuccess() {
		hideLoadingDialog();
		ArrayList<Message>recentMessages =  (ArrayList<Message>) ChatManager.getInstance().getLatestMessages();
		if(recentMessages != null && !recentMessages.isEmpty()){
		messages.addAll(recentMessages);
		adapter.notifyDataSetChanged();
		initialStateLayout.setVisibility(View.GONE);
		messagesLayout.setVisibility(View.VISIBLE);
		}
		else {
			initialStateLayout.setVisibility(View.VISIBLE);
			messagesLayout.setVisibility(View.GONE);
		}
		
	}






	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
	//	UIUtils.showToast(getActivity(), ex.getMessage());

		initialStateLayout.setVisibility(View.VISIBLE);
		messagesLayout.setVisibility(View.GONE);
	
	}

}
