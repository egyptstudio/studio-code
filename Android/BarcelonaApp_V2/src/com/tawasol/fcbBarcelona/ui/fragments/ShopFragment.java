package com.tawasol.fcbBarcelona.ui.fragments;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.ui.activities.PremiumPackagesActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class ShopFragment extends Fragment {
	
	GridView shopGrid;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.shop_fragment, container, false);
		shopGrid = (GridView)rootView.findViewById(R.id.shopGrid);
		ShopAdapter adapter = new ShopAdapter();
		shopGrid.setAdapter(adapter);
		return rootView;
	}

	
	private class ShopAdapter extends BaseAdapter{

		String [] adapterList;
		
		public ShopAdapter(){
			if(getActivity().getIntent().getBooleanExtra(ShopActivity.isShop, false)){
				adapterList = getActivity().getResources().getStringArray(R.array.Shop_packages);
			}else{
				adapterList = getActivity().getResources().getStringArray(R.array.points_packages);
			}
		}
		
		@Override
		public int getCount() {
			return adapterList.length;
		}

		@Override
		public Object getItem(int position) {
			return adapterList[position];
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View rootView = convertView;
			final ViewHolder holder;
			if(rootView == null){
				holder = new ViewHolder();
				rootView = getActivity().getLayoutInflater().inflate(
						R.layout.shop_fragment_item, parent, false);
				holder.packageName = (TextView)rootView.findViewById(R.id.packageName);
				rootView.setTag(holder);
			}else{
				holder = (ViewHolder) rootView.getTag();
			}
			holder.packageName.setText(adapterList[position]);
			if(getActivity().getIntent().getBooleanExtra(ShopActivity.isShop, false)){
				rootView.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(position == 0){
							startActivity(new Intent(getActivity(), PremiumPackagesActivity.class));
						}else if(position == 1){
							startActivity(ShopActivity.getIntent(getActivity(), false));
						}else if(position == 2){
							//navigate to physical items
						}
					}
				});
			}
			return rootView;
		}
		
		private class ViewHolder{
			TextView packageName;
		}
		
	}
}
