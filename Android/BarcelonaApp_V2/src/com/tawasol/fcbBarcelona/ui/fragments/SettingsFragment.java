package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.NotificationsTable;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnLogOutListener;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.managers.NotificationsManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.BlockedUserListActivity;
import com.tawasol.fcbBarcelona.ui.activities.ChangePasswordActivity;
import com.tawasol.fcbBarcelona.ui.activities.ContactUsActivity;
import com.tawasol.fcbBarcelona.ui.activities.HomeActivity;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.PrivacySettingsActivity;
import com.tawasol.fcbBarcelona.ui.activities.SystemMessagesActivity;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;

/**
 * @author TURKI
 * 
 */
public class SettingsFragment extends BaseFragment implements OnClickListener, OnLogOutListener {

	private TextView languageTxt, privacySettingsTxt, systemMsgsTxt,
			contactUsTxt, logoutTxt, changePasswordTxt;
	private ImageView languageArrow;
	private String listValue;
	private TextView blockedSettingsTxt;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings, container,
				false);

		/** Initialize registration view **/
		init(rootView);

		return rootView;
	}

	/** Handle OnClickListeners **/
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.language_arrow:
			showList(languageTxt);
			break;
		case R.id.privacy_setting_txt:
			startActivity(PrivacySettingsActivity
					.getActivityIntent(getActivity()));
			break;
		case R.id.blockedList_setting_txt:
			if (UserManager.getInstance().getCurrentUserId() != 0)
				startActivity(new Intent(getActivity(),
						BlockedUserListActivity.class));
			else {
				LogInActivity.getActivityIntent(getActivity(),
						UserManager.LOGIN_WITH_INTENT, new Intent(
								getActivity(), BlockedUserListActivity.class));
			}
			break;
		case R.id.system_msg_txt:
			startActivity(SystemMessagesActivity
					.getActivityIntent(getActivity()));
			// UIUtils.showToast(getActivity(), "Not implememted yet");
			break;
		case R.id.contact_us_txt:
			startActivity(ContactUsActivity.getActivityIntent(getActivity()));
			break;
		case R.id.change_password_txt:
			startActivity(ChangePasswordActivity
					.getActivityIntent(getActivity()));
			break;
		case R.id.logout_txt:
			showLoadingDialog();
			UserManager.getInstance().callLogOut();
			break;
		}
	}

	private void clearNotificationsData() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				NotificationsTable.getInstance().deleteAll();
			}
		});

	}

	/** Initialize registration view **/
	private void init(View rootView) {

		languageTxt = (TextView) rootView.findViewById(R.id.language_list);
		privacySettingsTxt = (TextView) rootView
				.findViewById(R.id.privacy_setting_txt);
		blockedSettingsTxt = (TextView) rootView
				.findViewById(R.id.blockedList_setting_txt);
		contactUsTxt = (TextView) rootView.findViewById(R.id.contact_us_txt);
		changePasswordTxt = (TextView) rootView
				.findViewById(R.id.change_password_txt);
		logoutTxt = (TextView) rootView.findViewById(R.id.logout_txt);
		systemMsgsTxt = (TextView) rootView.findViewById(R.id.system_msg_txt);
		languageArrow = (ImageView) rootView.findViewById(R.id.language_arrow);
		languageTxt.setText(SharedPrefrencesDataLayer.getStringPreferences(getActivity(), 
				LanguageUtils.APP_LANGUAGE_KEY, getDefaultLanguage()));
		if(SharedPrefrencesDataLayer.getIntPreferences(App.getInstance().getApplicationContext(),
				UserManager.LOGIN_TYPE, Params.APP_LOGIN)== Params.SOCIAL_LOGIN)
			changePasswordTxt.setVisibility(View.GONE);
		else
			changePasswordTxt.setVisibility(View.VISIBLE);

			String language[] = getResources().getStringArray(
				R.array.languages_list_items);
		final ArrayList<String> langCode = new ArrayList<String>(
				Arrays.asList(getResources().getStringArray(
						R.array.languages_code_list_items)));

		String langAbb = SharedPrefrencesDataLayer.getStringPreferences(
				getActivity(), LanguageUtils.APP_LANGUAGE_CODE_KEY,
				LanguageUtils.LANGUAGE_ENGLISH);

		languageTxt.setText(language[langCode.indexOf(langAbb)]);
		languageArrow.setOnClickListener(this);
		privacySettingsTxt.setOnClickListener(this);
		blockedSettingsTxt.setOnClickListener(this);
		contactUsTxt.setOnClickListener(this);
		changePasswordTxt.setOnClickListener(this);
		logoutTxt.setOnClickListener(this);
		systemMsgsTxt.setOnClickListener(this);
	}

	/** Initialize of list Dialog **/
	private String showList(final TextView textView) {

		String language[] = getResources().getStringArray(
				R.array.languages_list_items);
		final String langCode[] = getResources().getStringArray(
				R.array.languages_code_list_items);

		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.settings_list_dialog);

		final ListView listView = (ListView) dialog
				.findViewById(R.id.settings_list);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, language) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				View view = super.getView(position, convertView, parent);
				TextView textView = (TextView) view
						.findViewById(android.R.id.text1);
				textView.setGravity(Gravity.CENTER);
				textView.setTextSize(getActivity().getResources().getDimension(
						R.dimen.smallestTextSize));
				textView.setTextColor(getActivity().getResources().getColor(
						R.color.black));
				return view;
			}
		};

		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				listValue = listView.getItemAtPosition(position).toString();
				textView.setText(listValue);
				switchLanguage(langCode[position], listValue);
				dialog.dismiss();
			}
		});
		dialog.setCancelable(true);
		dialog.show();

		return listValue;
	}

	
	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	private void switchLanguage(String languageCode, String language) {
		// Change application language
		LanguageUtils.changeApplicationLanguage(languageCode, getActivity(),
				true);

		// save app language in preferences
		SharedPrefrencesDataLayer.saveStringPreferences(getActivity(),
				LanguageUtils.APP_LANGUAGE_CODE_KEY, languageCode);
		SharedPrefrencesDataLayer.saveStringPreferences(getActivity(),
				LanguageUtils.APP_LANGUAGE_KEY, language);
		LanguageUtils.getInstance().setLanguage(languageCode);
		getActivity().finish();
		startActivity(HomeActivity.getActivityIntent(getActivity()));
		SharedPrefrencesDataLayer.saveBooleanPreferences(getActivity(),
				App.LANG_CHANGED, true);
	}

	String getDefaultLanguage() {
		String defaultValue = "English";
		String langCode[] = getResources().getStringArray(
				R.array.languages_code_list_items);
		for (int i = 0; i < langCode.length; i++) {
			if (Locale.getDefault().getISO3Language().substring(0, 2)
					.equalsIgnoreCase(langCode[i])) {
				defaultValue = Locale.getDefault().getDisplayLanguage();
				break;
			}
		}
		return defaultValue;
	}

	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}
	
	@Override
	public void onSuccess() {
		hideLoadingDialog();
		ChatManager.getInstance().saveLastTimeStampChecked(0);
		LoginManager.getInstance().logOut();
		SharedPrefrencesDataLayer.saveIntPreferences(App.getInstance()
				.getApplicationContext(), UserManager.LOGOUT_USER_ID_KEY,
				UserManager.getInstance().getCurrentUserId());
		User user = new User();
		UserManager.getInstance().cacheUser(user);
		((BaseActivity) getActivity()).refreshMenu();

		App.getInstance().clearApplicationData();

		// cancel the periodic task of notifications , Added by Mohga
//		App.getInstance().stopNotificationService();
		// reset the cached notification table
		clearNotificationsData();
		// reset the last request time
		NotificationsManager.getInstance().saveLastRequestTime(0);

		// navigate to home
		startActivity(HomeActivity.getActivityIntent(getActivity()));
		// startActivity(LogInActivity.getActivityIntent(getActivity(),UserManager.NORMAL_LOGIN,
		// null));
	}
}
