package com.tawasol.fcbBarcelona.ui.fragments;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.HelpScreenActivity;
import com.tawasol.fcbBarcelona.ui.activities.WebViewActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class InfoFragment extends BaseFragment {

	public static final String SELECTED_HELP = "SelectedHelp";
	public static final String SELECTED_AGREEMENT = "privacy";
	public static final String SELECTED_STUDIO = "about_tawasol";
	public static final String SELECTED_BARCELONA = "about_fcb";
	public static final String SELECTED_TAWASOL = "terms";
	public static final String SELECTED_FAQ = "faq";
	public static final String SELECTED_FREE_POINT = "free point";
	ListView infoList;
	String[] infoElements;
	// Create an ArrayAdapter that will contain all list items
	ArrayAdapter<String> infoAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_info, container,	false);

		infoList = (ListView) rootView.findViewById(android.R.id.list);
		infoElements = getResources().getStringArray(R.array.infoElements);

		/*
		 * Assign the name array to that adapter and also choose a simple layout
		 * for the list items
		 */
		infoAdapter = new ArrayAdapter<String>(getActivity(),R.layout.simple_txt, infoElements);
		// Assign the adapter to this ListActivity
		infoList.setAdapter(infoAdapter);
		// set the on item seletcted listener !
		infoList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,	int position, long id) {

				String SelectedItem = "";
				if (position == 0) // Privacy Agreement
					SelectedItem = SELECTED_BARCELONA; // about fcb
				else if (position == 1) // About FCB Studio
					SelectedItem = SELECTED_STUDIO; // about tawasol
				else if (position == 2) // About FCB Barcelona
					SelectedItem = SELECTED_TAWASOL; // terms
				else if (position == 3) // About TawasolIT
					SelectedItem = SELECTED_AGREEMENT; // privacy
				else if (position == 4){  
					SelectedItem = SELECTED_FREE_POINT; // FAQ
				}
				else if (position == 5){  
					SelectedItem = SELECTED_FAQ; // FAQ
				}
				if (position == 6){ // Help
					startActivity(HelpScreenActivity.getActivityIntent(getActivity()));
				}
				if (position != 6)
					startActivity(WebViewActivity.getActivityIntent(getActivity(), SelectedItem));
			}
		});
		return rootView;
	}
}
