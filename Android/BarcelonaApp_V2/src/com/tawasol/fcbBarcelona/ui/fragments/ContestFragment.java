package com.tawasol.fcbBarcelona.ui.fragments;

import com.tawasol.fcbBarcelona.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost.TabSpec;

public class ContestFragment extends BaseFragment {

	private FragmentTabHost contestTabHost;
	private TabWidget widget;
	private HorizontalScrollView hs;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contest, container,
				false);
		contestTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);
		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// TabWidget tw = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// LinearLayout ll = (LinearLayout) tw.getParent();
		hs = (HorizontalScrollView) rootView
				.findViewById(R.id.horizontalScrollView);
		hs.setFillViewport(false);

		setUpTabHost();

		return rootView;
	}

	private void setUpTabHost() {
		contestTabHost.setup(getActivity(), getChildFragmentManager(),
				android.R.id.tabcontent);

		Bundle data;
		// adding Prizes tab
		data = new Bundle();
		// contestTabHost.addTab(
		// setIndicator(contestTabHost.newTabSpec(getActivity().getResources().getString(R.string.Contest_Prizes)),
		// R.drawable.chat_left_tab),
		// ContestPrizesFragment.class, data);

		contestTabHost.addTab(

				contestTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Contest_Prizes)).setIndicator(
						getTabIndicator(
								contestTabHost.getContext(),
								getActivity().getResources().getString(
										R.string.Contest_Prizes),
								android.R.drawable.star_on)),
				ContestPrizesFragment.class, data);

		// adding Rules tab
		// contestTabHost.addTab(
		// setIndicator(contestTabHost.newTabSpec(getActivity()
		// .getResources().getString(R.string.Contest_Rules)),
		// R.drawable.chat_center_tab),
		// ContestRulesFragment.class, data);

		contestTabHost.addTab(

				contestTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Contest_Rules)).setIndicator(
						getTabIndicator(
								contestTabHost.getContext(),
								getActivity().getResources().getString(
										R.string.Contest_Rules),
								android.R.drawable.star_on)),
				ContestRulesFragment.class, data);

		// adding winners tab
		data = new Bundle();
		// contestTabHost.addTab(
		// setIndicator(contestTabHost.newTabSpec(getActivity()
		// .getResources().getString(R.string.contest_winners)),
		// R.drawable.chat_right_tab),
		// ContestWinnersFragment.class, data);

		contestTabHost.addTab(

				contestTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.contest_winners)).setIndicator(
						getTabIndicator(
								contestTabHost.getContext(),
								getActivity().getResources().getString(
										R.string.contest_winners),
								android.R.drawable.star_on)),
				ContestWinnersFragment.class, data);

		// contestTabHost
		// .setOnTabChangedListener(new TabHost.OnTabChangeListener() {
		//
		// @Override
		// public void onTabChanged(String tabId) {
		// setTabColors();
		// }
		// });
		ViewTreeObserver observer = widget.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@SuppressLint("NewApi")
			@Override
			public void onGlobalLayout() {

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					widget.getViewTreeObserver().removeOnGlobalLayoutListener(
							this);
				} else {
					widget.getViewTreeObserver().removeGlobalOnLayoutListener(
							this);
				}

				Display display = getActivity().getWindowManager()
						.getDefaultDisplay();
				int width = display.getWidth();
				if (widget.getWidth() < width) {
					FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.MATCH_PARENT);
					params.gravity = Gravity.CENTER_HORIZONTAL;

					widget.setLayoutParams(params);
					hs.setFillViewport(true);
				}
			}
		});

		contestTabHost
				.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

					@Override
					public void onTabChanged(String tabId) {
						setTabColors();
					}
				});
	}

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid) {
		View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_host_text, null);
		v.setBackgroundResource(resid);

		final TextView text = (TextView) v.findViewById(R.id.tab_text);
		text.setText(spec.getTag());

		text.setTextColor(getActivity().getResources().getColor(
				android.R.color.white));

		return spec.setIndicator(v);
	}

	@Override
	public void onResume() {
		super.onResume();
		setTabColors();
	}

	// private void setTabColors() {
	// for (int i = 0; i < contestTabHost.getTabWidget().getChildCount(); i++) {
	// TextView tv = (TextView) contestTabHost.getTabWidget()
	// .getChildAt(i).findViewById(R.id.tab_text);
	// tv.setTextColor(getActivity().getResources().getColor(
	// R.color.new_yellow));
	// }
	// TextView tv = (TextView) contestTabHost.getTabWidget()
	// .getChildAt(contestTabHost.getCurrentTab())
	// .findViewById(R.id.tab_text);
	// tv.setTextColor(Color.WHITE);
	// }

	private void setTabColors() {
		for (int i = 0; i < contestTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) contestTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.textView);
			// tv.setTextColor(getActivity().getResources().getColor(
			// R.color.new_yellow));
			tv.setTypeface(Typeface.DEFAULT);
		}
		TextView tv = (TextView) contestTabHost.getTabWidget()
				.getChildAt(contestTabHost.getCurrentTab())
				.findViewById(R.id.textView);
		// tv.setTextColor(Color.WHITE);
		tv.setTypeface(Typeface.DEFAULT_BOLD);
	}

	private View getTabIndicator(Context context, String title, int icon) {
		final View view = LayoutInflater.from(context).inflate(
				R.layout.tab_layout, null);

		// ImageView iv = (ImageView) view.findViewById(R.id.imageView);
		// iv.setImageResource(icon);
		final TextView tv = (TextView) view.findViewById(R.id.textView);
		tv.setText(title);
		return view;
	}

}
