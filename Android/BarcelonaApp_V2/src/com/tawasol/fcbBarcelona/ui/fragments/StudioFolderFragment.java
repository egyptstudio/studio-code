package com.tawasol.fcbBarcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.managers.StudioManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.StudioFoldersActivity;

public class StudioFolderFragment extends BaseFragment {

	/*
	 * public static final String SELECTED_TAB_KEY ="Studio_Selected_Tab";
	 * public static final int PLAYERS_TAB = 1; public static final int TEAM_TAB
	 * = 2; public static final int PURCHASED_TAB = 3;
	 */

	private FragmentTabHost folderTabHost;
	private TabWidget widget;
	private HorizontalScrollView hs;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_studio_folder,
				container, false);
		folderTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);
		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// TabWidget tw = (TabWidget) rootView.findViewById(android.R.id.tabs);
		// LinearLayout ll = (LinearLayout) tw.getParent();
		hs = (HorizontalScrollView) rootView
				.findViewById(R.id.horizontalScrollView);
		hs.setFillViewport(false);

		setUpTabHost();

		LinearLayout listLayout = (LinearLayout) rootView
				.findViewById(R.id.postLayout);
		
		listLayout.setPadding(0, 0, 0, (int) (BaseActivity.bottomBarHeight - (70 + convertPixelsToDp(20, getActivity()))));
		AdView mAdView = (AdView) rootView.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
		final LinearLayout adContainer = (LinearLayout) rootView
				.findViewById(R.id.adsLayout);
		mAdView.setAdListener(new AdListener() {
			@Override
			public void onAdFailedToLoad(int errorCode) {
				super.onAdFailedToLoad(errorCode);
				adContainer.setVisibility(View.GONE);
			}
		});
		
		if (StudioManager.getInstance().getSeasonList() == null)
			StudioManager.getInstance().getSeasons();
		switchTabs();
		return rootView;
	}

	public static float convertPixelsToDp(float px, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;
	}
	
	private void setUpTabHost() {
		folderTabHost.setup(getActivity(), getChildFragmentManager(),
				android.R.id.tabcontent);

		Bundle data;

		// adding player tab
		data = new Bundle();
		// data.putInt(SELECTED_TAB_KEY, PLAYERS_TAB);
		folderTabHost.addTab(

				folderTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Studio_Players)).setIndicator(
						getTabIndicator(
								folderTabHost.getContext(),
								getActivity().getResources().getString(
										R.string.Studio_Players),
								android.R.drawable.star_on)),
				PlayerFragment.class, null);

		// setIndicator(folderTabHost.newTabSpec(getActivity().getResources().getString(R.string.Studio_Players)),
		// R.drawable.chat_left_tab),
		// PlayerFragment.class, data);
		// adding Team tab
		folderTabHost.addTab(

				folderTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Studio_Team)).setIndicator(
						getTabIndicator(
								folderTabHost.getContext(),
								getActivity().getResources().getString(
										R.string.Studio_Team),
								android.R.drawable.star_on)),
				TeamFragment.class, null);

		// setIndicator(folderTabHost.newTabSpec(getActivity().getResources().getString(R.string.Studio_Team)),
		// R.drawable.chat_center_tab),
		// TeamFragment.class, data);

		// adding Purchased photos tab
		// boolean enabled
		// =UserManager.getInstance().getCurrentUser().getUserId() == 0 ?
		// false:true;
		folderTabHost.addTab(

				folderTabHost.newTabSpec(
						getActivity().getResources().getString(
								R.string.Studio_Purchased)).setIndicator(
						getTabIndicator(
								folderTabHost.getContext(),
								getActivity().getResources().getString(
										R.string.Studio_Purchased),
								android.R.drawable.star_on)),
				StudioPhotosFragment.class, null);

		// setIndicator(folderTabHost.newTabSpec(getActivity().getResources().getString(R.string.Studio_Purchased)),
		// R.drawable.chat_right_tab),
		// StudioPhotosFragment.class, data);
		// if(!enabled){
		// folderTabHost.getTabWidget().getChildTabViewAt(2).setEnabled(false);
		// }

		ViewTreeObserver observer = widget.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@SuppressLint("NewApi")
			@Override
			public void onGlobalLayout() {

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					widget.getViewTreeObserver().removeOnGlobalLayoutListener(
							this);
				} else {
					widget.getViewTreeObserver().removeGlobalOnLayoutListener(
							this);
				}

				Display display = getActivity().getWindowManager()
						.getDefaultDisplay();
				int width = display.getWidth();
				if (widget.getWidth() < width) {
					FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.MATCH_PARENT);
					params.gravity = Gravity.CENTER_HORIZONTAL;

					widget.setLayoutParams(params);
					hs.setFillViewport(true);
				}
			}
		});

		folderTabHost
				.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

					@Override
					public void onTabChanged(String tabId) {
						setTabColors();
					}
				});
	}

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid) {
		View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_host_text, null);
		v.setBackgroundResource(resid);

		final TextView text = (TextView) v.findViewById(R.id.tab_text);
		text.setText(spec.getTag());

		text.setTextColor(getActivity().getResources().getColor(
				android.R.color.white));

		return spec.setIndicator(v);
	}

	@Override
	public void onResume() {
		super.onResume();
		setTabColors();
	}

	private View getTabIndicator(Context context, String title, int icon) {
		final View view = LayoutInflater.from(context).inflate(
				R.layout.tab_layout, null);

		// ImageView iv = (ImageView) view.findViewById(R.id.imageView);
		// iv.setImageResource(icon);
		final TextView tv = (TextView) view.findViewById(R.id.textView);
		tv.setText(title);
		return view;
	}

	private void setTabColors() {
		for (int i = 0; i < folderTabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) folderTabHost.getTabWidget().getChildAt(i)
					.findViewById(R.id.textView);
			// tv.setTextColor(getActivity().getResources().getColor(
			// R.color.new_yellow));
			tv.setTypeface(Typeface.DEFAULT);
		}
		TextView tv = (TextView) folderTabHost.getTabWidget()
				.getChildAt(folderTabHost.getCurrentTab())
				.findViewById(R.id.textView);
		// tv.setTextColor(Color.WHITE);
		tv.setTypeface(Typeface.DEFAULT_BOLD);
	}

	private void switchTabs() {
		if (getActivity().getIntent().getExtras() != null) {
			int tab = getActivity().getIntent().getExtras()
					.getInt(StudioFoldersActivity.TAB_OBJ);
			if (tab != 0) {
				switch (tab) {
				case 0:
					folderTabHost.setCurrentTab(0);
					break;
				case 1:
					folderTabHost.setCurrentTab(1);
					break;
				case 2:
					folderTabHost.setCurrentTab(2);
					break;
				default:
					break;
				}
			}
		}
	}

}
