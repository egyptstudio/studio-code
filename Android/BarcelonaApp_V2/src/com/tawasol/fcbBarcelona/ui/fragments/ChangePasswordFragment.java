package com.tawasol.fcbBarcelona.ui.fragments;
  
import com.tawasol.fcbBarcelona.R;  
 
import com.tawasol.fcbBarcelona.entities.ChangePassword;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnChangePasswordListener;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.SettingsActivity;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.ValidatorUtils;

import android.os.Bundle; 
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup; 
import android.widget.Button;
import android.widget.EditText;

/**
 * @author TURKI
 * 
 */
public class ChangePasswordFragment extends BaseFragment implements OnClickListener, OnChangePasswordListener{
	private EditText oldPasswordET, newPasswordET, confirmPasswordET;
	private String oldPassword    , newPassword  , confirmPassword  ;
	private Button sendBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
		
		/** Initialize change password view **/
		init(rootView);

		return rootView;
	}

	/** Handle OnClickListeners **/
	@Override
	public void onClick(View v) {
		/** Get view values **/
		getValues();
		switch (v.getId()) {
		case R.id.send_btn:
			if(checkValidationField()){
				ChangePassword changePassEntity = new ChangePassword(newPassword, oldPassword);
				UserManager.getInstance().callChangePassword(changePassEntity);
			}
			break;
			
		}
	}

	/** Initialize change password view **/
	private void init(View rootView) {
		oldPasswordET     = (EditText) rootView.findViewById(R.id.old_password_txt);
		newPasswordET     = (EditText) rootView.findViewById(R.id.new_password_txt);
		confirmPasswordET = (EditText) rootView.findViewById(R.id.confirm_password_txt);
		sendBtn           = (Button) rootView.findViewById(R.id.send_btn);
		sendBtn.setOnClickListener(this);
	}
	
	/**  Get change password fields value **/
	private void getValues() {
		/** Get all values from user **/
		oldPassword        = oldPasswordET.getText().toString();
		newPassword        = newPasswordET.getText().toString();
		confirmPassword    = confirmPasswordET.getText().toString();
	}


	/**  Validate validation fields **/
	private boolean checkValidationField() {

		if (ValidatorUtils.isRequired(oldPassword) || ValidatorUtils.isRequired(newPassword) || 
				ValidatorUtils.isRequired(confirmPassword)) {
			UIUtils.showToast(getActivity(), getResources().getString(R.string.reg_fill_data_validation));
			return false;
		}else if (ValidatorUtils.isRequired(oldPassword)) {
			UIUtils.showToast(getActivity(), getResources().getString(R.string.ChangePassword_oldPasswordAlert));
			return false;
		}else if (ValidatorUtils.isRequired(newPassword)) {
			UIUtils.showToast(getActivity(), getResources().getString(R.string.ChangePassword_newPasswordAlert));
			return false;
		}else if (newPassword.length() < 5) {
			UIUtils.showToast(getActivity(), getResources().getString(R.string.ChangePassword_passwordLengthAlert));
			return false;
		}else if (ValidatorUtils.isRequired(confirmPassword)) {
			UIUtils.showToast(getActivity(), getResources().getString(R.string.ChangePassword_ConfirmPasswordAlert));
			return false;
		}else if (!confirmPassword.equalsIgnoreCase(newPassword)) {
			UIUtils.showToast(getActivity(), getResources().getString(R.string.ChangePassword_Password_confirmDontMatchAlert));
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void onSuccess() {
		UIUtils.showToast(getActivity(), getActivity().getResources().getString(R.string.ChangePassword_successChange));
		startActivity(SettingsActivity.getActivityIntent(getActivity()));
		getActivity().finish();
	}

	@Override
	public void onException(AppException ex) {
		UIUtils.showToast(getActivity(), ex.getMessage());
	}	
	
	/** Add Change Password listener onResume **/
	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	/** Remove Change Password listener onPause **/
	@Override
	public void onPause() {
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}
}
