package com.tawasol.fcbBarcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.NotificationsTable;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.entities.Notification;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.managers.NotificationsManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;

public class NotificationsFragment extends BaseFragment {

	public static final String NOTIFCATION_TYPE = "NotificationViewType";
	public static final String WHICH_TAB = "whichTab";
	public static final int LIKES = 100;
	
	public static final int COMMENTS = 101;
	public static final int MESSAGES = 102;
	public static final int REQUESTS = 103;
	public static final int FOLLOWING = 104;
	public static final int FAVORITES = 105;
	public static final int SYSTEM_MSG = 108;
	static int LikesCount, CommentsCount, RequestsCount, FollowingsCount,
			FavouritesCount;

	private static FragmentTabHost notificationsTabHost;
	private IntentFilter gcmFilter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_notifications,
				container, false);
		notificationsTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);
		setUpTabHost();
		gcmFilter = new IntentFilter();
		gcmFilter.addAction("GCM_RECEIVED_ACTION");
		if (getActivity().getIntent().getExtras() != null) {
			int tab = getActivity().getIntent().getIntExtra(WHICH_TAB, 0);
			switchTabs(tab);
		}
		return rootView;
	}

	public static void getNotificationsCount() {
		LikesCount = NotificationsTable.getInstance()
				.getFilteredUnreadNotificationsCount(
						Notification.LIKE_NOTIFICATIONS);
		CommentsCount = NotificationsTable.getInstance()
				.getFilteredUnreadNotificationsCount(
						Notification.COMMENT_NOTIFICATIONS);
		RequestsCount = NotificationsTable.getInstance()
				.getFilteredUnreadNotificationsCount(
						Notification.FRIEND_REQUEST_NOTIFICATIONS);
		FollowingsCount = NotificationsTable.getInstance()
				.getFilteredUnreadNotificationsCount(
						Notification.FOLLOWING_NOTIFICATIONS);
		FavouritesCount = NotificationsTable.getInstance()
				.getFavouritesUnreadCount();
		BaseActivity.notificationsCount = LikesCount + CommentsCount
				+ RequestsCount + FollowingsCount + FavouritesCount;
	}

	private void setUpTabHost() {
		getNotificationsCount();

		notificationsTabHost.setup(getActivity(), getChildFragmentManager(),
				android.R.id.tabcontent);

		Bundle data;

		// adding likes tab

		data = new Bundle();
		data.putInt(NOTIFCATION_TYPE, LIKES);
		notificationsTabHost.addTab(
				setIndicator(notificationsTabHost
						.newTabSpec(getActivity().getResources().getString(
								R.string.notifications_likes)),
						R.drawable.notifications_likes_tab, LikesCount),
				NotificationsListFragment.class, data);

		// adding comments tab

		data = new Bundle();
		data.putInt(NOTIFCATION_TYPE, COMMENTS);
		notificationsTabHost.addTab(
				setIndicator(notificationsTabHost.newTabSpec(getActivity()
						.getResources().getString(
								R.string.notifications_comments)),
						R.drawable.notifications_comments_tab, CommentsCount),
				NotificationsListFragment.class, data);

		// adding messages tab

		/*
		 * data = new Bundle(); data.putInt(NOTIFCATION_TYPE, MESSAGES);
		 * notificationsTabHost.addTab(
		 * setIndicator(notificationsTabHost.newTabSpec
		 * (getActivity().getResources
		 * ().getString(R.string.notifications_messages)),
		 * R.drawable.notifications_messages_tab),
		 * NotificationsMessagesFragment.class, data);
		 */

		// adding requests tab

		/*
		 * data = new Bundle(); data.putInt(NOTIFCATION_TYPE, REQUESTS);
		 */
		notificationsTabHost.addTab(
				setIndicator(notificationsTabHost.newTabSpec(getActivity()
						.getResources().getString(
								R.string.notifications_requests)),
						R.drawable.notifications_requests_tab, RequestsCount),
				NotificationsRequestsFragment.class, null);

		// adding following tab

		data = new Bundle();
		data.putInt(NOTIFCATION_TYPE, FOLLOWING);
		notificationsTabHost
				.addTab(setIndicator(notificationsTabHost
						.newTabSpec(getActivity().getResources().getString(
								R.string.notifications_likes_Following)),
						R.drawable.notifications_following_tab, FollowingsCount),
						NotificationsListFragment.class, data);

		// adding Favorite tab

//		data = new Bundle();
//		data.putInt(NOTIFCATION_TYPE, FAVORITES);
//		notificationsTabHost
//				.addTab(setIndicator(notificationsTabHost
//						.newTabSpec(getActivity().getResources().getString(
//								R.string.FanProfileLevel2_Favorate)),
//						R.drawable.notifications_favorite_tab, FavouritesCount),
//						NotificationsListFragment.class, data);

		data = new Bundle();
		data.putInt(NOTIFCATION_TYPE, SYSTEM_MSG);
		notificationsTabHost.addTab(
				setIndicator(notificationsTabHost
						.newTabSpec(getActivity().getResources().getString(
								R.string.Settings_System)),
						R.drawable.notifications_syste_msg_tab, 
						
						SharedPrefrencesDataLayer.getIntPreferences(App.getInstance()
								.getApplicationContext(),ChatManager.SYSTEM_MSG_CHECKED_KEY, 0)),
				SystemMessagesFragment.class, data);
		
		notificationsTabHost
				.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

					@Override
					public void onTabChanged(String tabId) {
						updateTab();
					}
				});

	}

	@SuppressLint("InflateParams")
	public TabSpec setIndicator(final TabSpec spec, int resid,
			int notificationCount) {
		View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_host_notifications, null);
		TextView tabText = (TextView) v.findViewById(R.id.tab_text);
		TextView tabNumberText = (TextView) v
				.findViewById(R.id.tab_notification_number);

		tabText.setCompoundDrawablesWithIntrinsicBounds(0,
				resid, 0, 0);
		tabText.setText(spec.getTag());

		if (notificationCount > 0) {
			tabNumberText.setVisibility(View.VISIBLE);
			tabNumberText.setText(String.valueOf(notificationCount));
		} else
			tabNumberText.setVisibility(View.GONE);

		tabText.setTextColor(getActivity().getResources().getColor(
				R.color.yellow));

		return spec.setIndicator(v);
	}

	public static void updateTab() {
		getNotificationsCount();
		if (notificationsTabHost != null) {
			for (int i = 0; i < notificationsTabHost.getTabWidget()
					.getChildCount(); i++) {
				TextView tv = (TextView) notificationsTabHost.getTabWidget()
						.getChildAt(i).findViewById(R.id.tab_text);
				tv.setTextColor(App.getInstance().getApplicationContext()
						.getResources().getColor(R.color.white));

			}
			TextView tv = (TextView) notificationsTabHost.getTabWidget()
					.getChildAt(notificationsTabHost.getCurrentTab())
					.findViewById(R.id.tab_text);
			tv.setTextColor(App.getInstance().getApplicationContext()
					.getResources().getColor(R.color.new_yellow));
			for (int i = 0; i < notificationsTabHost.getTabWidget()
					.getChildCount(); i++) {
				TextView numberTV = (TextView) notificationsTabHost
						.getTabWidget().getChildAt(i)
						.findViewById(R.id.tab_notification_number);
				switch (i) {
				case 0: // likes
					if (LikesCount > 0) {
						numberTV.setVisibility(View.VISIBLE);
						numberTV.setText(String.valueOf(LikesCount));
					} else
						numberTV.setVisibility(View.GONE);
					break;

				case 1: // comments
					if (CommentsCount > 0) {
						numberTV.setVisibility(View.VISIBLE);
						numberTV.setText(String.valueOf(CommentsCount));
					} else
						numberTV.setVisibility(View.GONE);
					break;

				case 2: // requests
					if (RequestsCount > 0) {
						numberTV.setVisibility(View.VISIBLE);
						numberTV.setText(String.valueOf(RequestsCount));
					} else
						numberTV.setVisibility(View.GONE);
					break;

				case 3: // following
					if (FollowingsCount > 0) {
						numberTV.setVisibility(View.VISIBLE);
						numberTV.setText(String.valueOf(FollowingsCount));
					} else
						numberTV.setVisibility(View.GONE);
					break;

				case 4: // favorites
					if (FavouritesCount > 0) {
						numberTV.setVisibility(View.VISIBLE);
						numberTV.setText(String.valueOf(FavouritesCount));
					} else
						numberTV.setVisibility(View.GONE);
					break;

				default:
					break;
				}

			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		updateTab();
		getActivity().registerReceiver(gcmReceiver, gcmFilter);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		getActivity().unregisterReceiver(gcmReceiver);
	}

	public void switchTabs(int tab) {

		if (tab != 0) {
			switch (tab) {
			case LIKES:
				NotificationsManager.getInstance().setNotificationWithTypeRead(
						Notification.LIKE_NOTIFICATIONS);
				notificationsTabHost.setCurrentTab(0);
				break;

			case COMMENTS:
				NotificationsManager.getInstance().setNotificationWithTypeRead(
						Notification.COMMENT_NOTIFICATIONS);
				notificationsTabHost.setCurrentTab(1);
				break;
			case REQUESTS:
				NotificationsManager.getInstance().setNotificationWithTypeRead(
						Notification.FRIEND_REQUEST_NOTIFICATIONS);
				notificationsTabHost.setCurrentTab(2);
				break;
			case FOLLOWING:
				NotificationsManager.getInstance().setNotificationWithTypeRead(
						Notification.FOLLOWING_NOTIFICATIONS);
				notificationsTabHost.setCurrentTab(3);
//				break;
//			case FAVORITES:
//				NotificationsManager.getInstance().setNotificationWithTypeRead(
//						Notification.ADD_FRIEND_NOTIFICATIONS);
//				NotificationsManager.getInstance().setNotificationWithTypeRead(
//						Notification.CHANGE_RROFILE_NOTIFICATIONS);
//				NotificationsManager.getInstance().setNotificationWithTypeRead(
//						Notification.POST_NEW_PHOTO_NOTIFICATIONS);
//				notificationsTabHost.setCurrentTab(4);
//				break;
			
			case SYSTEM_MSG:
//				NotificationsManager.getInstance().setNotificationWithTypeRead(
//						Notification.SYS_MSG_NOTIFICATIONS);
				notificationsTabHost.setCurrentTab(4);
				break;
			default:
				NotificationsManager.getInstance().setNotificationWithTypeRead(
						Notification.LIKE_NOTIFICATIONS);
				break;
			}
		}
	}

	private BroadcastReceiver gcmReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			int type = intent.getIntExtra(WHICH_TAB, 0);
			switchTabs(type);
		}
	};
}
