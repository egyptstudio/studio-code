package com.tawasol.fcbBarcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnUpdateProfileListener;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.Edit_More_Profile_Activity;
import com.tawasol.fcbBarcelona.ui.activities.Edit_UserProfile_Activity;
import com.tawasol.fcbBarcelona.ui.activities.UserProfileActivity;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class Edit_More_Profile_Fragment extends BaseFragment implements
		OnClickListener, OnUpdateProfileListener {
	private static final int CASE_RELATIOSHIP = 1;
	private static final int CASE_INCOME = 2;
	private static final int CASE_EDUCATION = 3;
	private static final int CASE_JOB = 4;
	private static final int CASE_JOB_ROLE = 5;
	private EditText statusTxt;
	private EditText aboutMeTxt;
	private TextView relationshipTxt;
	// private EditText religionTxt;
	// private EditText educationTxt;
	// private EditText jobTxt;
	private EditText companyTxt;
	private TextView incomeTxt;
	private TextView educationTxt;
	private TextView jobTxt;
	private TextView jobRoleTxt;
	private TextView aboutMeCounter;
	private TextView doneTxt;
	private User user;
	private String[] relationshipList;
	private String[] incomeList;
	private String[] educationList;
	private String[] jobList;
	private String[] jobRoleList;
	private ArrayAdapter<String> adapter;
	private boolean isRelationShipchanged = false, isIncomeChanged = false;
	private ImageView backImg;
	private boolean isEducationChanged;
	private boolean isJobChanged;
	private boolean isJobRoleChanged;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_edit_more_about_me,
				container, false);
		ViewByID(rootView);
		populateUser();
		InitViewControls();
		return rootView;
	}

	private void ViewByID(View rootView) {
		statusTxt = (EditText) rootView.findViewById(R.id.edit_more_status_txt);
		aboutMeTxt = (EditText) rootView
				.findViewById(R.id.edit_more_about_me_txt);
		relationshipTxt = (TextView) rootView
				.findViewById(R.id.edit_more_relationship_txt);
		// religionTxt=(EditText)rootView.findViewById(R.id.edit_more_religion_txt);
		educationTxt = (TextView) rootView
				.findViewById(R.id.edit_more_education_txt);
		jobTxt = (TextView) rootView.findViewById(R.id.edit_more_job_txt);
		jobRoleTxt = (TextView) rootView
				.findViewById(R.id.edit_more_jobRole_txt);
		companyTxt = (EditText) rootView
				.findViewById(R.id.edit_more_copany_txt);
		incomeTxt = (TextView) rootView.findViewById(R.id.edit_more_income_txt);
		aboutMeCounter = (TextView) rootView
				.findViewById(R.id.edit_more_about_me_counter_txt);
		doneTxt = (TextView) getActivity().findViewById(R.id.rightBtn);

	}

	private void populateUser() {
		this.user = (User) getActivity().getIntent().getExtras()
				.getSerializable(Edit_More_Profile_Activity.USER_OBJ_KEY);
	}

	@SuppressLint("NewApi")
	private void InitViewControls() {

		// get the income and relationship lists
		incomeList = getResources().getStringArray(R.array.income_list);
		relationshipList = getResources().getStringArray(
				R.array.relationship_list);

		educationList = getResources().getStringArray(R.array.education_array);
		jobList = getResources().getStringArray(R.array.job_array);
		jobRoleList = getResources().getStringArray(R.array.jobRoles_array);

		// set the drawable of relationship
		relationshipTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				R.drawable.right_arrows, 0);
		// set the drawable of income
		incomeTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				R.drawable.right_arrows, 0);

		educationTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				R.drawable.right_arrows, 0);

		jobTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				R.drawable.right_arrows, 0);
		jobRoleTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				R.drawable.right_arrows, 0);
		// set the right button
		doneTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.done,
				0);

		statusTxt.setText((user.getStatus() != null && !user.getStatus()
				.isEmpty()) ? user.getStatus() : null);
		aboutMeTxt.setText((user.getAboutMe() != null && !user.getAboutMe()
				.isEmpty()) ? user.getAboutMe() : null);
		// educationTxt.setText((user.getEducation() != null && !user
		// .getEducation().isEmpty()) ? user.getEducation() : null);
		// jobTxt.setText((user.getJob() != null && !user.getJob().isEmpty()) ?
		// user
		// .getJob() : null);
		companyTxt.setText((user.getCompany() != null && !user.getCompany()
				.isEmpty()) ? user.getCompany() : null);
		// religionTxt.setText((user.getReligion()!= null &&
		// !user.getReligion().isEmpty())? user.getReligion() : "");

		// set the count of about me text
		aboutMeCounter.setText((400 - aboutMeTxt.getText().toString().length())
				+ "");

		// set the state of pickers !
		setRelationship(user.getRelationship());
		setIncome(user.getIncome());
		setEducation(user.getEducation());
		setJob(user.getJob());
		setJobRole(user.getJobRole());
		// handle the counter of the about me text
		/*************** Chat Counter Text SetUp **************/
		aboutMeTxt.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable mEdit) {
				aboutMeCounter.setText((400 - mEdit.toString().length()) + "");
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				aboutMeCounter.setText((400 - aboutMeTxt.getText().toString()
						.length())
						+ "");
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
		/************************************************/

		// set on click listeners
		statusTxt.setOnClickListener(this);
		relationshipTxt.setOnClickListener(this);
		// religionTxt.setOnClickListener(this);
		incomeTxt.setOnClickListener(this);
		doneTxt.setOnClickListener(this);
		educationTxt.setOnClickListener(this);
		jobTxt.setOnClickListener(this);
		jobRoleTxt.setOnClickListener(this);

		textwatcherListeners();
	}

	/**
	 * Initialize of List Dialog for country , city
	 * 
	 * @param <D>
	 **/
	private <D> void showListDialog(final int state) {

		// Initialization of dialog
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.popup_list);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.GRAY));
		dialog.getWindow().setTitleColor(android.graphics.Color.WHITE);

		// set the title and list
		String[] stringsList = null;
		if (state == CASE_RELATIOSHIP) {
			stringsList = relationshipList;
			dialog.setTitle(getResources().getString(
					R.string.select_relationship_header));
		} else if (state == CASE_INCOME) {
			stringsList = incomeList;
			dialog.setTitle(getResources().getString(
					R.string.select_income_header));
		} else if (state == CASE_EDUCATION) {
			stringsList = educationList;
			dialog.setTitle(getResources().getString(
					R.string.select_education_header));
		} else if (state == CASE_JOB) {
			stringsList = jobList;
			dialog.setTitle(getResources()
					.getString(R.string.select_job_header));
		} else if (state == CASE_JOB_ROLE) {
			stringsList = jobRoleList;
			dialog.setTitle(getResources().getString(
					R.string.select_jobRole_header));
		}

		// initailize controls
		ListView listView = (ListView) dialog.findViewById(R.id.popup_list);
		// countries list and adapter settings
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.popup_list_item, stringsList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (state == CASE_RELATIOSHIP)
					relationshipTxt.setText(relationshipList[position]);
				else if (state == CASE_INCOME)
					incomeTxt.setText(incomeList[position]);
				else if (state == CASE_EDUCATION)
					educationTxt.setText(educationList[position]);
				else if (state == CASE_JOB)
					jobTxt.setText(jobList[position]);
				else if (state == CASE_JOB_ROLE)
					jobRoleTxt.setText(jobRoleList[position]);
				dialog.dismiss();
			}
		});

		// show the dialog
		dialog.show();

	}

	// set the relationShipStatus
	private void setRelationship(int key) {
		if (key == User.RELATIONSHIP_MARRIED)
			relationshipTxt
					.setText(getString(R.string.relationship_list_item2));
		else if (key == User.RELATIONSHIP_ENGAGED)
			relationshipTxt
					.setText(getString(R.string.relationship_list_item3));
		else if (key == User.RELATIONSHIP_IN_RELATIONSHIP)
			relationshipTxt
					.setText(getString(R.string.relationship_list_item4));
		else
			/* if(key == User.RELATIONSHIP_SINGLE) */
			relationshipTxt
					.setText(getString(R.string.relationship_list_item1));
		// else
		// relationshipTxt.setText(getResources().getString(R.string.MyProfile_Relationship));
	}

	// set the relationShipStatus
	private void setEducation(int key) {
		if (key == User.EDUCATION_PhD)
			educationTxt.setText(getString(R.string.Ph_D));
		else if (key == User.EDUCTION_Master)
			educationTxt.setText(getString(R.string.Master));
		else if (key == User.EDUCATION_Bachelor)
			educationTxt.setText(getString(R.string.Bachelor));
		else if (key == User.EDUCATION_College)
			educationTxt.setText(getString(R.string.College));
		else if (key == User.EDUCTION_High_School)
			educationTxt.setText(getString(R.string.High_School));
		else
			/* if(key == User.RELATIONSHIP_SINGLE) */
			educationTxt.setText(getString(R.string.None));
		// else
		// relationshipTxt.setText(getResources().getString(R.string.MyProfile_Relationship));
	}

	// set the relationShipStatus
	private void setJob(int key) {
		if (key == User.JOB_Self_Employed)
			jobTxt.setText(getString(R.string.Self_Employed));
		else if (key == User.JOB_Employee)
			jobTxt.setText(getString(R.string.Employee));
		else if (key == User.JOB_Housewife)
			jobTxt.setText(getString(R.string.Housewife));
		else if (key == User.JOB_unemployed)
			jobTxt.setText(getString(R.string.unemployed));
		else if (key == User.JOB_STUDENT)
			jobTxt.setText(getString(R.string.Student));
		else
			/* if(key == User.RELATIONSHIP_SINGLE) */
			jobTxt.setText(getString(R.string.None));
		// else
		// relationshipTxt.setText(getResources().getString(R.string.MyProfile_Relationship));
	}

	// set the relationShipStatus
	private void setJobRole(int key) {
		jobRoleTxt.setText(jobRoleList[key]);
	}

	// set the relationShipStatus
	private void setIncome(int key) {
		if (key == User.INCOME_LOW)
			incomeTxt.setText(getString(R.string.income_list_item1));
		else if (key == User.INCOME_HIGH)
			incomeTxt.setText(getString(R.string.income_list_item3));
		else
			/* if(key == User.INCOME_AVERAGE) */
			incomeTxt.setText(getString(R.string.income_list_item2));
		// else
		// incomeTxt.setText(getResources().getString(R.string.initial_state_income));
	}

	private int getRelationship() {
		if (relationshipTxt
				.getText()
				.toString()
				.equals(getResources().getString(
						R.string.relationship_list_item2)))
			return User.RELATIONSHIP_MARRIED;
		else if (relationshipTxt
				.getText()
				.toString()
				.equals(getResources().getString(
						R.string.relationship_list_item3)))
			return User.RELATIONSHIP_ENGAGED;
		else if (relationshipTxt
				.getText()
				.toString()
				.equals(getResources().getString(
						R.string.relationship_list_item4)))
			return User.RELATIONSHIP_IN_RELATIONSHIP;
		else if (relationshipTxt
				.getText()
				.toString()
				.equals(getResources().getString(
						R.string.relationship_list_item1)))
			return User.RELATIONSHIP_SINGLE;
		else
			return 0;
	}

	private int getEducation() {
		if (educationTxt.getText().toString()
				.equals(getResources().getString(R.string.Ph_D)))
			return User.EDUCATION_PhD;
		else if (educationTxt.getText().toString()
				.equals(getResources().getString(R.string.Master)))
			return User.EDUCTION_Master;
		else if (educationTxt.getText().toString()
				.equals(getResources().getString(R.string.Bachelor)))
			return User.EDUCATION_Bachelor;
		else if (educationTxt.getText().toString()
				.equals(getResources().getString(R.string.College)))
			return User.EDUCATION_College;
		else if (educationTxt.getText().toString()
				.equals(getResources().getString(R.string.High_School)))
			return User.EDUCTION_High_School;
		else
			return 0;
	}

	private int getJob() {
		if (jobTxt.getText().toString()
				.equals(getResources().getString(R.string.Self_Employed)))
			return User.JOB_Self_Employed;
		else if (jobTxt.getText().toString()
				.equals(getResources().getString(R.string.Employee)))
			return User.JOB_Employee;
		else if (jobTxt.getText().toString()
				.equals(getResources().getString(R.string.Student)))
			return User.JOB_STUDENT;
		else if (jobTxt.getText().toString()
				.equals(getResources().getString(R.string.Housewife)))
			return User.JOB_Housewife;
		else if (jobTxt.getText().toString()
				.equals(getResources().getString(R.string.unemployed)))
			return User.JOB_unemployed;
		else
			return 0;
	}

	private int getIncome() {
		if (incomeTxt.getText().toString()
				.equals(getResources().getString(R.string.income_list_item1)))
			return User.INCOME_LOW;
		else if (incomeTxt.getText().toString()
				.equals(getResources().getString(R.string.income_list_item3)))
			return User.INCOME_HIGH;
		else if (incomeTxt.getText().toString()
				.equals(getResources().getString(R.string.income_list_item2)))
			return User.INCOME_AVERAGE;
		else
			return 0;
	}

	private int getJobRole() {
		for (int x = 0; x < jobRoleList.length; x++) {
			if (jobRoleList[x]
					.equalsIgnoreCase(jobRoleTxt.getText().toString()))
				return x;
		}
		return 0;
	}

	public User buildUser() {
		this.user.setStatus(statusTxt.getText().toString());
		this.user.setAboutMe(aboutMeTxt.getText().toString());
		if (isEducationChanged)
			this.user.setEducation(getEducation());
		if (isJobChanged)
			this.user.setJob(getJob());
		this.user.setCompany(companyTxt.getText().toString());
		// this.user.setReligion(religionTxt.getText().toString());
		if (isRelationShipchanged)
			this.user.setRelationship(getRelationship());
		if (isIncomeChanged)
			this.user.setIncome(getIncome());
		if (isJobRoleChanged)
			this.user.setJobRole(getJobRole());
		return user;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (UserManager.getInstance().getCurrentUserId() != 0)
			UserManager.getInstance().addListener(this);
		else
			getActivity().finish();
	}

	@Override
	public void onPause() {
		super.onPause();
		UserManager.getInstance().removeListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rightBtn:
			showLoadingDialog();
			UserManager.getInstance().udateProfile(buildUser(), false, false);
			break;

		case R.id.edit_more_relationship_txt:
			isRelationShipchanged = true;
			showListDialog(CASE_RELATIOSHIP);
			break;
		case R.id.edit_more_income_txt:
			isIncomeChanged = true;
			showListDialog(CASE_INCOME);
			break;
		case R.id.edit_more_education_txt:
			isEducationChanged = true;
			showListDialog(CASE_EDUCATION);
			break;
		case R.id.edit_more_job_txt:
			isJobChanged = true;
			showListDialog(CASE_JOB);
			break;
		case R.id.edit_more_jobRole_txt:
			isJobRoleChanged = true;
			showListDialog(CASE_JOB_ROLE);
			break;
		default:
			break;
		}

	}

	@Override
	public void onSuccess(User obj) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), getActivity().getString(R.string.profile_updated));
		if (!getActivity().getIntent().getBooleanExtra(
				Edit_UserProfile_Activity.fromShare, false))
			startActivity(UserProfileActivity.getActivityIntent(getActivity())
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		// ((Edit_More_Profile_Activity)getActivity()).onBackPressed();
		Edit_UserProfile_Activity.finish = true;
		getActivity().finish();
	}

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());

	}

	private void textwatcherListeners() {
		statusTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					statusTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		aboutMeTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					aboutMeTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		educationTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					educationTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		companyTxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().startsWith(" "))
					companyTxt.setText("");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	InputFilter filter = new InputFilter() {
		boolean canEnterSpace = false;

		public CharSequence filter(CharSequence source, int start, int end,
				Spanned dest, int dstart, int dend) {

			StringBuilder builder = new StringBuilder();

			for (int i = start; i < end; i++) {
				char currentChar = source.charAt(i);

				if (i == 0)
					canEnterSpace = false;

				if (!Character.isWhitespace(currentChar)/*
														 * Character.isLetterOrDigit
														 * (currentChar) ||
														 * currentChar == '_'
														 */) {
					builder.append(currentChar);
					canEnterSpace = true;
				}

				if (Character.isWhitespace(currentChar) && canEnterSpace) {
					builder.append(currentChar);

				}

			}
			return builder.toString();
		}

	};

}
