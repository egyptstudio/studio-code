package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.util.BarcaIabHelper;
import com.android.vending.billing.util.BarcaIabHelper.OnIabPurchaseFinishedListener;
import com.android.vending.billing.util.BarcaIabHelper.OnIabSetupFinishedListener;
import com.android.vending.billing.util.BarcaIabResult;
import com.android.vending.billing.util.BarcaInventory;
import com.android.vending.billing.util.BarcaPurchase;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefsHelper;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.entities.InAppPointsEntity;
import com.tawasol.fcbBarcelona.managers.ShopManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.FreePointsActivity;
import com.tawasol.fcbBarcelona.ui.activities.FreePointsWalkThrough;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivityTabs;
import com.tawasol.fcbBarcelona.ui.activities.WebViewShopActivity;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class PointPAckages extends Fragment implements
		OnIabSetupFinishedListener, OnIabPurchaseFinishedListener {

	private BarcaIabHelper billingHelper;
	private static final String PURCHASE_PAYLOAD_CACHE_KEY = "com.tawasol.Barchelona.PAYLOAD";
	public static final String RESPONSE_PRODUCT_ID = "productId";
	public static final String RESPONSE_PAYLOAD = "developerPayload";
	public static boolean shopAvailable;
	public static final int loginRequestCode = 1547;
	PointsAdapter adapter;

	ListView pointsList;
	ProgressBar pointsProgress;
	private Handler customHandler;
	TextView offline;
	private ProgressDialog dialog;
	private Typeface tf;
	LinearLayout Free_Points;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.points_packages, container,
				false);
		dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		pointsList = (ListView) rootView.findViewById(R.id.pointsList);
		pointsProgress = (ProgressBar) rootView
				.findViewById(R.id.pointsPackagesProgress);
		Free_Points = (LinearLayout) rootView.findViewById(R.id.Free_Points);
		Free_Points.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// startActivity(new Intent(getActivity(),
				// FreePointsActivity.class));
				// startActivity(new Intent(getActivity(),
				// WebViewShopActivity.class).putExtra(
				// WebViewShopActivity.IS_SURVY, false));
				startActivity(new Intent(getActivity(),
						FreePointsWalkThrough.class));
			}
		});
		offline = (TextView) rootView.findViewById(R.id.offline);
		// Loading Font Face
		tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/FontBureau-Interstate-Light.ttf");
		if (NetworkingUtils.isNetworkConnected(getActivity())) {
			billingHelper = new BarcaIabHelper(getActivity(),
					Params.Common.BASE_64_KEY);
			billingHelper.startSetup(this);
			customHandler = new Handler();
			customHandler.postDelayed(checkData, 0);
		} else {
			offline.setVisibility(View.VISIBLE);
		}
		return rootView;
	}

	private Runnable checkData = new Runnable() {

		@Override
		public void run() {
			if (ShopManager.getInstance().isDataAvailableException()) {
				pointsProgress.setVisibility(View.GONE);
				UIUtils.showToast(getActivity(), ShopManager.getInstance()
						.dataRetrievalException());
			} else {
				if (!ShopManager.getInstance().isDataAvailable()) {
					pointsProgress.setVisibility(View.VISIBLE);
					customHandler.postDelayed(this, 1000);
				} else {
					pointsProgress.setVisibility(View.GONE);
					setAdapter();
				}
			}
		}
	};

	protected void setAdapter() {
		Free_Points.setVisibility(View.VISIBLE);
		List<InAppPointsEntity> points = ShopManager.getInstance().getPoints();
		if (points != null) {
			adapter = new PointsAdapter(points);
			pointsList.setAdapter(adapter);
		}
	}

	private class PointsAdapter extends BaseAdapter {

		List<InAppPointsEntity> points;

		public PointsAdapter(List<InAppPointsEntity> points) {
			this.points = points;
		}

		public List<InAppPointsEntity> getPoints() {
			return this.points;
		}

		@Override
		public int getCount() {
			return points.size();
		}

		@Override
		public Object getItem(int position) {
			return points.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rootView = convertView;
			final InAppPointsEntity point = points.get(position);
			if (rootView == null) {
				rootView = LayoutInflater.from(
						App.getInstance().getApplicationContext()).inflate(
						R.layout.points_item, parent, false);
			}
			TextView greenNum = (TextView) rootView.findViewById(R.id.greenNum);
			TextView smallNum = (TextView) rootView.findViewById(R.id.samllNum);

			TextView pointDescription = (TextView) rootView
					.findViewById(R.id.Pointdescription);
			pointDescription.setTypeface(tf);
			smallNum.setTypeface(tf);

			if (point.getDescription() != null) {
				pointDescription.setText(Html.fromHtml(Html.fromHtml(
						(String) point.getDescription()).toString()));
			}

			smallNum.setText("$" + String.valueOf(point.getPrice()));
			if (point.getTitle() != null) {
				greenNum.setText(point.getTitle());
				// smallNum.setText(point.getTitle()
				// + getActivity().getResources().getString(
				// R.string.points));
			}

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			int digitNum = Integer.parseInt(point.getTitle());
			if (digitNum(digitNum) == 4)
				params.setMargins((int) getActivity().getResources()
						.getDimension(R.dimen.marginLeft), (int) getActivity()
						.getResources().getDimension(R.dimen.marginTop), 0, 0);
			else
				params.setMargins(
						(int) getActivity().getResources().getDimension(
								R.dimen.points_margin_left),
						(int) getActivity().getResources().getDimension(
								R.dimen.points_margin_top), 0, 0);

			greenNum.setLayoutParams(params);
			rootView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						if (shopAvailable && !ShopManager.BILING_NOT_AVAILABLE) {
							String purchasePayload = BarcaIabHelper.ITEM_TYPE_INAPP
									+ ":" + UUID.randomUUID().toString();
							savePurchasePayload(purchasePayload);
							if (billingHelper != null)
								billingHelper.flagEndAsync();
							billingHelper.launchPurchaseFlow(getActivity(),
									point.getInAppId(),
									ShopActivityTabs.POINTS_REQUEST_CODE,
									PointPAckages.this, purchasePayload);
						} else {
							UIUtils.showToast(getActivity(),
									ShopManager.BILING_IS_NOT_AVAILABLE);
						}
					} else {
						getActivity().startActivityForResult(
								LogInActivity.getActivityIntent(getActivity(),
										UserManager.LOGIN_FOR_RESULT,
										new Intent()), loginRequestCode);
					}
				}
			});

			return rootView;
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		int responseCode = data.getIntExtra(BarcaIabHelper.RESPONSE_CODE,
				BarcaIabHelper.BILLING_RESPONSE_RESULT_OK);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK
				&& responseCode == BarcaIabHelper.BILLING_RESPONSE_RESULT_OK) {
			if (requestCode == ShopActivityTabs.POINTS_REQUEST_CODE) {
				try {
					String purchaseData = data
							.getStringExtra(BarcaIabHelper.RESPONSE_INAPP_PURCHASE_DATA);
					JSONObject purchase = new JSONObject(purchaseData);
					String developerPayload = purchase
							.getString(RESPONSE_PAYLOAD);
					if (developerPayload == null)
						developerPayload = "";
					if (getPurchasePayload().equals(developerPayload)) {
						billingHelper.handleActivityResult(requestCode,
								resultCode, data);
					}
				} catch (Exception e) {
					// Toast.makeText(getActivity(), "Inside On exception",
					// Toast.LENGTH_SHORT).show();
					System.out.println(e);
				}
			}
		} else if (resultCode == Activity.RESULT_OK
				&& requestCode == FansListFragment.INTENT_RESULT_CODE) {
			((BaseActivity) getActivity()).refreshMenu();
		}
	}

	@Override
	public void onIabPurchaseFinished(BarcaIabResult result, BarcaPurchase info) {
		if (result.isFailure()) {
			// dealWithPurchaseFailed(result);
		} else if (result.isSuccess()) {
			dealWithPurchaseSuccess(info);
		}
	}

	private void dealWithPurchaseSuccess(BarcaPurchase info) {
		if (adapter != null) {
			if (adapter.getPoints() != null) {
				InAppPointsEntity point = adapter.getPoints().get(
						adapter.getPoints().indexOf(
								new InAppPointsEntity(info.getSku(), 0)));
				if (point != null) {
					dialog.show();
					ShopManager.getInstance().addPointsForUser(
							UserManager.getInstance().getCurrentUserId(),
							Integer.parseInt(point.getTitle()), info,
							billingHelper, dialog, true, getActivity());
				}
			}
		}
	}

	@Override
	public void onIabSetupFinished(BarcaIabResult result) {
		if (result.isSuccess()) {
			// Toast.makeText(getActivity(), "Consuming",
			// Toast.LENGTH_SHORT).show();
			shopAvailable = true;
			List<InAppPointsEntity> points = ShopManager.getInstance()
					.getPoints();
			List<String> skuList = new ArrayList<>();
			if (points != null && !points.isEmpty()) {
				for (int x = 0; x < points.size(); x++) {
					skuList.add(points.get(x).getInAppId());
				}
				if (skuList != null && !skuList.isEmpty())
					query(skuList);
			}
			// String purchaseToken = "inapp:" + getActivity().getPackageName()
			// + ":com.tawasol.fcb.100point";
			// BarcaPurchase info = new BarcaPurchase(purchaseToken,
			// "com.tawasol.fcb.100point", 0);
			// BarcaIabHelper.OnConsumeFinishedListener listener = new
			// BarcaIabHelper.OnConsumeFinishedListener() {
			//
			// @Override
			// public void onConsumeFinished(BarcaPurchase purchase,
			// BarcaIabResult result) {
			// if (result.isSuccess()){
			// Toast.makeText(getActivity(), "success",
			// Toast.LENGTH_SHORT).show();
			// return;
			//
			// }if (result.isFailure()){
			// Toast.makeText(getActivity(), "failed "+ result.getMessage(),
			// Toast.LENGTH_SHORT).show();
			// return;
			// }
			// }
			// };
			// billingHelper.consumeAsync(info, listener);
		} else {
			ShopManager.BILING_NOT_AVAILABLE = true;
			UIUtils.showToast(getActivity(),
					ShopManager.BILING_IS_NOT_AVAILABLE);
		}
	}

	protected void savePurchasePayload(String purchasePayload) {
		SharedPrefsHelper.setRegiterKey(PURCHASE_PAYLOAD_CACHE_KEY,
				purchasePayload);
	}

	private String getPurchasePayload() {
		return SharedPrefsHelper.getRegisterKey(PURCHASE_PAYLOAD_CACHE_KEY);
	}

	private int digitNum(int n) {
		if (n < 100000) {
			// 5 or less
			if (n < 100) {
				// 1 or 2
				if (n < 10)
					return 1;
				else
					return 2;
			} else {
				// 3 or 4 or 5
				if (n < 1000)
					return 3;
				else {
					// 4 or 5
					if (n < 10000)
						return 4;
					else
						return 5;
				}
			}
		} else {
			// 6 or more
			if (n < 10000000) {
				// 6 or 7
				if (n < 1000000)
					return 6;
				else
					return 7;
			} else {
				// 8 to 10
				if (n < 100000000)
					return 8;
				else {
					// 9 or 10
					if (n < 1000000000)
						return 9;
					else
						return 10;
				}
			}
		}
	}

	public void query(final List<String> skuList) {
		// Listener that's called when we finish querying the items and
		// subscriptions we own
		BarcaIabHelper.QueryInventoryFinishedListener mGotInventoryListener = new BarcaIabHelper.QueryInventoryFinishedListener() {
			public void onQueryInventoryFinished(BarcaIabResult result,
					BarcaInventory inventory) {

				// Have we been disposed of in the meantime? If so, quit.
				if (billingHelper == null)
					return;

				// Is it a failure?
				if (result.isFailure()) {
					return;
				}

				if (inventory != null)
					for (int x = 0; x < skuList.size(); x++) {
						BarcaPurchase userItem = inventory.getPurchase(skuList
								.get(x));
						BarcaIabHelper.OnConsumeFinishedListener listener = new BarcaIabHelper.OnConsumeFinishedListener() {

							@Override
							public void onConsumeFinished(
									BarcaPurchase purchase,
									BarcaIabResult result) {
								if (result.isSuccess()) {
									if (billingHelper != null)
										billingHelper.flagEndAsync();
									return;

								}
								if (result.isFailure()) {
									return;
								}
							}
						};
						if (userItem != null) {
							try {
								if (billingHelper != null)
									billingHelper.flagEndAsync();
								billingHelper.consumeAsync(userItem, listener);
							} catch (Exception e) {

							}
						}
					}

			}
		};
		billingHelper.queryInventoryAsync(true, skuList, mGotInventoryListener);
	}

}
