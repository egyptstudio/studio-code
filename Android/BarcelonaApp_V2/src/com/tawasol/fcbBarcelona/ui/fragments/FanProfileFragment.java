package com.tawasol.fcbBarcelona.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.customView.MaskedImageView;
import com.tawasol.fcbBarcelona.entities.Fan;
import com.tawasol.fcbBarcelona.entities.FriendsEntity;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnFanProfileRecieved;
import com.tawasol.fcbBarcelona.listeners.OnFriendResponseListener;
import com.tawasol.fcbBarcelona.managers.FanManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.responses.BaseResponse;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.ChatActivity;
import com.tawasol.fcbBarcelona.ui.activities.ChatViewActivity;
import com.tawasol.fcbBarcelona.ui.activities.FanProfileActivity;
import com.tawasol.fcbBarcelona.ui.activities.FansGridActivity;
import com.tawasol.fcbBarcelona.ui.activities.FriendsList;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.MoreInfoActivity;
import com.tawasol.fcbBarcelona.ui.activities.TagListActivity;
import com.tawasol.fcbBarcelona.ui.dialogs.CustomAlertDialog;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class FanProfileFragment extends Fragment implements
		OnFanProfileRecieved, OnClickListener, OnFriendResponseListener {

	TextView fanName;
	TextView fanStatus;
	ImageView fanProfilePic;
	TextView fanLocation;
	TextView followers;
	TextView following;
	TextView picsLayout;
	TextView chat;
	TextView addFriend;
	TextView follow;
	TextView fanFavo;
	ImageView onlineState;
	TextView favo;
	TextView block;
	TextView more;
	ProgressDialog dialog;
	ProgressBar bar;
	Fan fan;
	ScrollView scroll;
	int fanID;
	FrameLayout frame;
	String intentFanName;
	private DisplayImageOptions options;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fan_profile_fragment,
				container, false);
		fanID = getActivity().getIntent().getIntExtra(FanProfileActivity.FANID,
				0);
		intentFanName = getActivity().getIntent().getStringExtra(
				FanProfileActivity.FAN_NAME);
		dialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		dialog.setCancelable(false);
		dialog.show();
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.fan_no_image)
				.resetViewBeforeLoading(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.showImageForEmptyUri(R.drawable.fan_no_image)
				.showImageOnFail(R.drawable.fan_no_image).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		FanManager.getInstance().getPublicProfile(fanID,
				UserManager.getInstance().getCurrentUserId());
		initViews(rootView);
		return rootView;
	}

	private void initViews(final View rootView) {
		scroll = (ScrollView) rootView.findViewById(R.id.scrollView1);
		// final View view = ((BaseActivity) getActivity()).getBottomBar();
		ViewTreeObserver observer = scroll.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@SuppressLint("NewApi")
			@Override
			public void onGlobalLayout() {
				// menuBtnRight.setPadding(0, titleView.getHeight() / 2, 0, 0);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					scroll.getViewTreeObserver().removeOnGlobalLayoutListener(
							this);
				} else {
					scroll.getViewTreeObserver().removeGlobalOnLayoutListener(
							this);
				}
				LinearLayout.LayoutParams param = (android.widget.LinearLayout.LayoutParams) scroll
						.getLayoutParams();
				param.setMargins(
						0,
						0,
						0,
						((BaseActivity) getActivity()).getBottomBarHeight() + 10);
				scroll.setLayoutParams(param);
			}
		});
		//
		// scroll.setPadding(0, 0, 0,
		// ((BaseActivity) getActivity()).getBottomBarHeight() + 10);
		following = (TextView) rootView.findViewById(R.id.following_layout);
		followers = (TextView) rootView.findViewById(R.id.followers_layout);
		picsLayout = (TextView) rootView.findViewById(R.id.pic_layout);
		fanName = (TextView) rootView.findViewById(R.id.fan_name);
		fanStatus = (TextView) rootView.findViewById(R.id.fan_status);
		fanProfilePic = (ImageView) rootView.findViewById(R.id.fan_profile_pic);
		fanLocation = (TextView) rootView.findViewById(R.id.fan_location);
		chat = (TextView) rootView.findViewById(R.id.profile_chat_button);
		addFriend = (TextView) rootView
				.findViewById(R.id.profile_addFriend_button);
		follow = (TextView) rootView.findViewById(R.id.profile_follow_button);
		fanFavo = (TextView) rootView.findViewById(R.id.profile_Favo_button);
		frame = (FrameLayout) rootView.findViewById(R.id.frame);
		block = (TextView) rootView.findViewById(R.id.profile_block_button);
		onlineState = (ImageView) rootView.findViewById(R.id.onlineState);
		more = (TextView) rootView.findViewById(R.id.profile_more_button);
		bar = (ProgressBar) rootView.findViewById(R.id.progress);
		chat.setOnClickListener(this);
		addFriend.setOnClickListener(this);
		following.setOnClickListener(this);
		followers.setOnClickListener(this);
		picsLayout.setOnClickListener(this);
		follow.setOnClickListener(this);
		fanFavo.setOnClickListener(this);
		block.setOnClickListener(this);
		more.setOnClickListener(this);
		picsLayout.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.following_layout:
			if (fan != null)
				if (UserManager.getInstance().getCurrentUserId() != 0) {
					// TODO navigate to following
					FanManager.getInstance().clearFollowList();
					startActivity(FansGridActivity.getIntent(getActivity(),
							FansGridActivity.LOADFOLLOWING, fanID));
				} else {
					getActivity()
							.startActivityForResult(
									LogInActivity.getActivityIntent(
											getActivity(),
											UserManager.LOGIN_FOR_RESULT,
											new Intent()),
									FansListFragment.INTENT_RESULT_CODE);
				}
			break;
		case R.id.followers_layout:
			if (fan != null)
				if (UserManager.getInstance().getCurrentUserId() != 0) {
					// TODO navigate to followers
					FanManager.getInstance().clearFollowList();
					startActivity(FansGridActivity.getIntent(getActivity(),
							FansGridActivity.LOADFOLLOWERS, fanID));
				} else {
					getActivity()
							.startActivityForResult(
									LogInActivity.getActivityIntent(
											getActivity(),
											UserManager.LOGIN_FOR_RESULT,
											new Intent()),
									FansListFragment.INTENT_RESULT_CODE);
				}
			break;
		case R.id.pic_layout:
			if (fan != null)
				if (UserManager.getInstance().getCurrentUserId() != 0) {
					startActivity(TagListActivity.getIntent(getActivity(), 0,
							null, fan.getFanID(), fan.getFanName(), false));
				} else {
					getActivity()
							.startActivityForResult(
									LogInActivity.getActivityIntent(
											getActivity(),
											UserManager.LOGIN_FOR_RESULT,
											new Intent()),
									FansListFragment.INTENT_RESULT_CODE);
				}
			break;
		case R.id.profile_chat_button:
			if (fan != null)
				if (UserManager.getInstance().getCurrentUserId() != 0) {
					FriendsEntity friend = new FriendsEntity(fanID,
							intentFanName);
					startActivity(ChatViewActivity.getActivityIntent(
							getActivity(), friend));
				} else {
					getActivity()
							.startActivityForResult(
									LogInActivity.getActivityIntent(
											getActivity(),
											UserManager.LOGIN_FOR_RESULT,
											new Intent()),
									FansListFragment.INTENT_RESULT_CODE);
				}
			break;
		case R.id.profile_addFriend_button:
			if (fan == null)
				return; // Nothing to do, fan didn't load
			if (UserManager.getInstance().getCurrentUserId() != 0) {
				if (fan.isFriend()) {
					dialog.show();
					addFriend.setCompoundDrawablesWithIntrinsicBounds(0,
							R.drawable.fan_addfriends_icon, 0, 0);
					addFriend.setText(getActivity().getResources().getString(
							R.string.InitialMessagesBetweenFans_AddFriend));
					FanManager.getInstance().handleFriend(
							UserManager.getInstance().getCurrentUserId(),
							fanID, false);
				} else {
					dialog.show();
					FanManager.getInstance().handleFriend(
							UserManager.getInstance().getCurrentUserId(),
							fanID, true);
				}
			} else {
				getActivity().startActivityForResult(
						LogInActivity.getActivityIntent(getActivity(),
								UserManager.LOGIN_FOR_RESULT, new Intent()),
						FansListFragment.INTENT_RESULT_CODE);
			}
			break;
		case R.id.profile_follow_button:
			if (fan == null)
				return; // Nothing to do, fan didn't load
			if (UserManager.getInstance().getCurrentUserId() != 0) {
				if (fan.isFollowed()) {
					follow.setCompoundDrawablesWithIntrinsicBounds(0,
							R.drawable.fan_follow_icon, 0, 0);
					follow.setText(getActivity().getResources().getString(
							R.string.FanProfileLevel2_Follow));
					fan.setFollowed(false);
					FanManager.getInstance().handleFollow(
							UserManager.getInstance().getCurrentUserId(),
							fanID, false, getActivity());
				} else {
					follow.setCompoundDrawablesWithIntrinsicBounds(0,
							R.drawable.fan_unfollow_icon, 0, 0);
					follow.setText(getActivity().getResources().getString(
							R.string.FanProfileLevel2_unFollow));
					fan.setFollowed(true);
					FanManager.getInstance().handleFollow(
							UserManager.getInstance().getCurrentUserId(),
							fanID, true, getActivity());
				}
			} else {
				getActivity().startActivityForResult(
						LogInActivity.getActivityIntent(getActivity(),
								UserManager.LOGIN_FOR_RESULT, new Intent()),
						FansListFragment.INTENT_RESULT_CODE);
			}

			break;
		case R.id.profile_Favo_button:
			if (fan == null)
				return; // Nothing to do, fan didn't load
			if (UserManager.getInstance().getCurrentUserId() != 0) {
				if (fan.isFavorite()) {
					fanFavo.setCompoundDrawablesWithIntrinsicBounds(0,
							R.drawable.fan_favourit_icon, 0, 0);
					fanFavo.setText(getActivity().getResources().getString(
							R.string.FanProfileLevel2_Favorate));
					fan.setFavorite(false);
					FanManager.getInstance().handleFavo(
							UserManager.getInstance().getCurrentUserId(),
							fanID, false);
				} else {
					fanFavo.setCompoundDrawablesWithIntrinsicBounds(0,
							R.drawable.fan_unfavourit_icon, 0, 0);
					fanFavo.setText(getActivity().getResources().getString(
							R.string.FriendsLevel2Profile_unFavorate));
					fan.setFavorite(true);
					FanManager.getInstance().handleFavo(
							UserManager.getInstance().getCurrentUserId(),
							fanID, true);
				}
			} else {
				getActivity().startActivityForResult(
						LogInActivity.getActivityIntent(getActivity(),
								UserManager.LOGIN_FOR_RESULT, new Intent()),
						FansListFragment.INTENT_RESULT_CODE);
			}
			break;
		case R.id.profile_block_button:
			if (fan == null)
				return; // Nothing to do, fan didn't load
			if (UserManager.getInstance().getCurrentUserId() != 0) {
				if (UserManager.getInstance().getCurrentUserId() != 0) {
					String Message = "";

					DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case DialogInterface.BUTTON_POSITIVE:
								// block.setCompoundDrawablesWithIntrinsicBounds(0,
								// R.drawable.fan_block_icon, 0, 0);
								// block.setText(getActivity().getResources().getString(
								// R.string.Fans_UnBlock));
								// fan.setBlocked(true);
								FanManager.getInstance().handleBlock(
										UserManager.getInstance()
												.getCurrentUserId(), fanID,
										true, getActivity());
								dialog.dismiss();
								break;

							case DialogInterface.BUTTON_NEGATIVE:
								dialog.dismiss();
								break;
							}
						}
					};
					CustomAlertDialog.customBuilder builder = new CustomAlertDialog.customBuilder(
							getActivity());

					// set the message and Buttons
					Message = getString(R.string.fan_Block_msg);
					builder.setMessage(Message)
							.setPositiveButton(getActivity().getString(R.string.yes_block), dialogClickListener)
							.setNegativeButton(getActivity().getString(R.string.no_block), dialogClickListener);
					builder.create().show();
				}
			} else {
				getActivity().startActivityForResult(
						LogInActivity.getActivityIntent(getActivity(),
								UserManager.LOGIN_FOR_RESULT, new Intent()),
						FansListFragment.INTENT_RESULT_CODE);
			}
			break;
		case R.id.profile_more_button:
			if (fan == null || fan.getMoreInfo().isEmpty()) {
				// UIUtils.showToast(getActivity(), "No more Data");
			} else
				startActivity(MoreInfoActivity.getIntent(getActivity(), fan));
			break;

		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK
				&& requestCode == FansListFragment.INTENT_RESULT_CODE) {
			((BaseActivity) getActivity()).refreshMenu();
		}
	}

	@Override
	public void onResume() {
		FanManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
		FanManager.getInstance().removeListener(this);
		super.onPause();
	}

	@Override
	public void onSuccess(Fan obj) {
		fan = obj;
		dialog.dismiss();
		fillFields(obj);
	}

	private void fillFields(Fan obj) {
		LayoutParams layout = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		layout.gravity = Gravity.CENTER;

		for (int x = 0; x < obj.getMoreInfo().size(); x++) {
			if (obj.getMoreInfo().get(x).getTitle().equalsIgnoreCase("status")) {
				fanStatus.setVisibility(View.VISIBLE);
				fanStatus.setText(obj.getMoreInfo().get(x).getValue());
				break;
			}
		}

		if (obj.getFanName() != null)
			fanName.setText(obj.getFanName());
		// TODO add status
		if (obj.getFanPicture() != null)
			display(fanProfilePic, obj.getFanPicture(), bar);
		if (obj.isPremium())
			frame.setBackgroundResource(R.drawable.premium_frame);
		else
			frame.setBackgroundResource(R.drawable.basic_frame);
		if (fan.isFanOnline() == 1) {
			onlineState.setVisibility(View.VISIBLE);
			onlineState.setBackgroundResource(R.drawable.online_shape);
		} else if (fan.isFanOnline() == 2) {
			onlineState.setVisibility(View.VISIBLE);
			onlineState.setBackgroundResource(R.drawable.away_shape);
		} else
			onlineState.setVisibility(View.GONE);
		if (obj.getFanCountry() != null
				&& !obj.getFanCountry().equalsIgnoreCase("")) {
			fanLocation.setText(obj.getFanCountry());
		}
		if (obj.getFanCity() != null && !obj.getFanCity().equalsIgnoreCase("")) {
			String tempString = fanLocation.getText().toString();
			if (!tempString.equalsIgnoreCase("")) {
				fanLocation.setText(fanLocation.getText().toString() + " , "
						+ obj.getFanCity());
			} else
				fanLocation.setText(obj.getFanCity());
			// }else
			// fanLocation.setText(obj.getFanCity());

		}
		if (obj.getFanDistrict() != null
				&& !obj.getFanDistrict().equalsIgnoreCase("")) {
			String tempString = fanLocation.getText().toString();
			if (!tempString.equalsIgnoreCase("")) {
				fanLocation.setText(fanLocation.getText().toString() + " , "
						+ obj.getFanDistrict());
			} else
				fanLocation.setText(obj.getFanDistrict());
			// else
			// fanLocation.setText(obj.getFanDistrict());

		}
		fanLocation.getText().toString();

		followers.setText(followers.getText() + "\n"
				+ String.valueOf(obj.getFanFollowers()));
		following.setText(following.getText() + "\n"
				+ String.valueOf(obj.getFanFollows()));
		picsLayout.setText(picsLayout.getText() + "\n"
				+ String.valueOf(obj.getFanPicNumber()));
		if (obj.isFriend()) {
			addFriend.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.removefriends_icon, 0, 0);
			addFriend.setText(getActivity().getResources().getString(
					R.string.FriendsLevel2Profile_Unfriend));
		} else {
			addFriend.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.fan_addfriends_icon, 0, 0);
			addFriend.setText(getActivity().getResources().getString(
					R.string.InitialMessagesBetweenFans_AddFriend));
		}
		if (obj.isFollowed()) {
			follow.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.fan_unfollow_icon, 0, 0);
			follow.setText(getActivity().getResources().getString(
					R.string.FanProfileLevel2_unFollow));
		} else {
			follow.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.fan_follow_icon, 0, 0);
			follow.setText(getActivity().getResources().getString(
					R.string.FanProfileLevel2_Follow));
		}
		if (obj.isFavorite()) {
			fanFavo.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.fan_unfavourit_icon, 0, 0);
			fanFavo.setText(getActivity().getResources().getString(
					R.string.FriendsLevel2Profile_unFavorate));
		} else {
			fanFavo.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.fan_favourit_icon, 0, 0);
			fanFavo.setText(getActivity().getResources().getString(
					R.string.FanProfileLevel2_Favorate));
		}
		if (obj.isBlocked()) {
			block.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.fan_block_icon, 0, 0);
			block.setText(getActivity().getResources().getString(
					R.string.Fans_UnBlock));
		} else {
			block.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.fan_block_icon, 0, 0);
			block.setText(getActivity().getResources().getString(
					R.string.FanProfileLevel2_Block));
		}
		// if (obj.getMoreInfo().isEmpty())
		// more.setEnabled(false);

	}

	public void display(ImageView img, String url, final ProgressBar spinner) {
		App.getInstance().getImageLoader()
				.displayImage(url, img, options, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						spinner.setVisibility(View.VISIBLE);
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						spinner.setVisibility(View.GONE);

					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						spinner.setVisibility(View.GONE);
					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {

					}

				});
	}

	@Override
	public void onException(AppException ex) {
		dialog.dismiss();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onSuccess(int status, boolean friend) {
		dialog.dismiss();
		if (friend) {
			if (status == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
				UIUtils.showToast(getActivity(), getActivity().getString(R.string.request_sent));
			else if (status == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
				addFriend.setCompoundDrawablesWithIntrinsicBounds(0,
						R.drawable.removefriends_icon, 0, 0);
				addFriend.setText(getActivity().getResources().getString(
						R.string.FriendsLevel2Profile_Unfriend));
			}
		} else {
			startActivity(FriendsList.getActivityIntent(getActivity())
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		}
	}
}
