package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnFAQListener;
import com.tawasol.fcbBarcelona.listeners.OnInfoReceived;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.WebViewActivity;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class WebViewFragment extends BaseFragment implements OnInfoReceived,
		OnFAQListener {
	WebView webView;
	String SelectedFile;
	String filePath = "";
	TextView versionNumber;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_webview, container,
				false);

		webView = (WebView) rootView.findViewById(R.id.webView);
		versionNumber = (TextView)rootView.findViewById(R.id.versionNumber);
		versionNumber.setText("V " + getVersionName());
		RelativeLayout.LayoutParams params = (LayoutParams) versionNumber.getLayoutParams();
		return rootView;
	}

	private void getSelectedFile() {
		SelectedFile = getActivity().getIntent().getExtras()
				.getString(WebViewActivity.SELECTED_ID_BUNDLE);
		if(!SelectedFile.equals(InfoFragment.SELECTED_STUDIO))
			versionNumber.setVisibility(View.GONE);
	}

	private static String getVersionName() {
		try {
			Context cxt = App.getInstance().getApplicationContext();
			PackageManager manager = cxt.getPackageManager();
			PackageInfo info = manager.getPackageInfo(cxt.getPackageName(), 0);
			return info.versionName;
		} catch (NameNotFoundException e) {
			throw new IllegalStateException("No such package installed on the device: ", e);
		}
	}
	
	@SuppressLint("NewApi")
	private void InitViewControls() {

		if (SelectedFile.equals(InfoFragment.SELECTED_AGREEMENT))
			// filePath = "TermsConditions.html";
			UserManager.getInstance().getInfo(InfoFragment.SELECTED_AGREEMENT);

		if (SelectedFile.equals(InfoFragment.SELECTED_STUDIO))
			// filePath = "PrivacyPolicy.html";
			UserManager.getInstance().getInfo(InfoFragment.SELECTED_STUDIO);

		if (SelectedFile.equals(InfoFragment.SELECTED_BARCELONA))
			// filePath = "AboutFCB.html";
			UserManager.getInstance().getInfo(InfoFragment.SELECTED_BARCELONA);

		if (SelectedFile.equals(InfoFragment.SELECTED_TAWASOL))
			// filePath = "AboutTawasol.html";
			UserManager.getInstance().getInfo(InfoFragment.SELECTED_TAWASOL);

		if (SelectedFile.equals(InfoFragment.SELECTED_FAQ))
			// filePath = "AboutTawasol.html";
			UserManager.getInstance().callFAQ("faq");

		if (SelectedFile.equals(InfoFragment.SELECTED_FREE_POINT))
			// filePath = "AboutTawasol.html";
			UserManager.getInstance().callFAQ("completepoints");

		webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

		// webView.loadUrl("file:///android_asset/" + filePath);
		// //("http://api.tawasoldev.com/hajji/Privacy_Policy/")

		webView.setBackgroundColor(0x00000000);
		if (Build.VERSION.SDK_INT >= 11)
			webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

		this.webView.setWebViewClient(new WebViewClient() {
			@SuppressLint("NewApi")
			@Override
			public void onPageFinished(WebView view, String url) {
				view.setBackgroundColor(0x00000000);
				if (Build.VERSION.SDK_INT >= 11)
					view.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				return handleLinks(url);
			}
		});

		/*
		 * Timer timer = new Timer(); timer.schedule(new TimerTask() {
		 * 
		 * @Override public void run() {
		 * webView.loadUrl("file:///android_asset/" + filePath); } }, 500);
		 */
	}

	private boolean handleLinks(String url) {
		try {
			System.out.println("url called:::" + url);
			if (url.startsWith("tel:")) {
				Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
				startActivity(intent);
			} /*
			 * else if (url.startsWith("http:") || url.startsWith("https:")) {
			 * 
			 * Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			 * startActivity(intent);
			 * 
			 * }
			 */else if (url.startsWith("mailto:")) {

				MailTo mt = MailTo.parse(url);

				send_email(mt.getTo());

			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public void send_email(String email_add) {

		final Intent emailIntent = new Intent(
				android.content.Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { email_add });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
		startActivity(Intent.createChooser(emailIntent, "Send mail..."));

	}

	@Override
	public void onResume() {
		super.onResume();
		UserManager.getInstance().addListener(this);
		showLoadingDialog();
		getSelectedFile();
		InitViewControls();
	}

	@Override
	public void onPause() {
		super.onPause();
		UserManager.getInstance().removeListener(this);
	}

	@Override
	public void onSuccess(String obj) {
		hideLoadingDialog();
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl(obj);
	}

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}
}
