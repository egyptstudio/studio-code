package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.android.vending.billing.util.BarcaIabHelper;
import com.android.vending.billing.util.BarcaIabHelper.OnIabPurchaseFinishedListener;
import com.android.vending.billing.util.BarcaIabHelper.OnIabSetupFinishedListener;
import com.android.vending.billing.util.BarcaIabResult;
import com.android.vending.billing.util.BarcaInventory;
import com.android.vending.billing.util.BarcaPurchase;
import com.android.vending.billing.util.BarcaSkuDetails;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefsHelper;
import com.tawasol.fcbBarcelona.entities.ShopEntity;

public class PremiumGridFragment extends Fragment implements
		OnIabSetupFinishedListener, OnIabPurchaseFinishedListener {

	GridView shopGrid;
	public static final String BASE_64_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArFKsWOCguKlHkKngnwp3JFU/VF+ChVUNSgNlWdOXPKoW+ZEHZsWRD9CReNhiJoLvnkhlGioWyl729J9uNZF0j3qOAzTIBJpkDfYcGz8nYmUJrWcPxzGuQ7OMcIArJYJvpnd3bxafomhdqc1OzEhK/fEX26lbzh4HHvOpcFmrhfUjgkLvwzO/+mlvHAv9wUWMNplnEMrAOJkMyewz3qZuzAHY04m6OQYu/Xqp6cGtBXfntzx3UtaSY9bpjE0c3dGZQsf+4J9JtLqsGEfMf7JvAiqpBLeYwfNhYMrL7eeBlqTEN/pTkqJRsRdP24jxePWgA0VMK3R8ueh55/Tl0cvN3QIDAQAB";
	private BarcaIabHelper billingHelper;
	ProgressDialog dialog;
	ShopAdapter adapter;
	private static final String PURCHASE_PAYLOAD_CACHE_KEY = "com.tawasol.Barchelona.PAYLOAD";
	public static final int PURCHASE_REQUEST_CODE = 485625;
	public static final String RESPONSE_PRODUCT_ID = "productId";
	public static final String RESPONSE_PAYLOAD = "developerPayload";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.premium_package_grid,
				container, false);
		shopGrid = (GridView) rootView.findViewById(R.id.premium_grid);
		adapter = new ShopAdapter();
		shopGrid.setAdapter(adapter);
		dialog = new ProgressDialog(getActivity());
		dialog.show();
		billingHelper = new BarcaIabHelper(getActivity(), BASE_64_KEY);
		billingHelper.startSetup(this);
		return rootView;
	}

	private class ShopAdapter extends BaseAdapter {

		// String[] adapterList = new String[4];
		List<ShopEntity> adapterList = new ArrayList<ShopEntity>();

		public ShopAdapter() {
			adapterList.add(new ShopEntity("android.test.purchased", null));
			adapterList.add(new ShopEntity("10", null));
			adapterList.add(new ShopEntity("10", null));
			adapterList.add(new ShopEntity("10", null));
		}

		List<ShopEntity> getList() {
			return adapterList;
		}

		@Override
		public int getCount() {
			return adapterList.size();
		}

		@Override
		public Object getItem(int position) {
			return adapterList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View rootView = convertView;
			if (position == 0)
				rootView = getActivity().getLayoutInflater().inflate(
						R.layout.premium_package_one_month, parent, false);
			if (position == 1)
				rootView = getActivity().getLayoutInflater().inflate(
						R.layout.premium_package_three_month, parent, false);
			if (position == 2)
				rootView = getActivity().getLayoutInflater().inflate(
						R.layout.premium_package_six_months, parent, false);
			if (position == 3)
				rootView = getActivity().getLayoutInflater().inflate(
						R.layout.premium_package_one_year, parent, false);
			TextView price = (TextView) rootView.findViewById(R.id.Price);
			if (adapterList.get(position).getPrice() != null) {
				price.setText("For \n" + adapterList.get(position).getPrice());
			} else {
				price.setText("Empty");
			}
			rootView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					String purchasePayload = BarcaIabHelper.ITEM_TYPE_INAPP + ":"
							+ UUID.randomUUID().toString();
					savePurchasePayload(purchasePayload);
					billingHelper.launchPurchaseFlow(getActivity(),
							"android.test.purchased", PURCHASE_REQUEST_CODE,
							PremiumGridFragment.this, purchasePayload);
				}
			});
			return rootView;
		}

		protected void savePurchasePayload(String purchasePayload) {
			SharedPrefsHelper.setRegiterKey(PURCHASE_PAYLOAD_CACHE_KEY,
					purchasePayload);
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		int responseCode = data.getIntExtra(BarcaIabHelper.RESPONSE_CODE,
				BarcaIabHelper.BILLING_RESPONSE_RESULT_OK);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK
				&& responseCode == BarcaIabHelper.BILLING_RESPONSE_RESULT_OK) {
			if (requestCode == PURCHASE_REQUEST_CODE) {
				try {
					String purchaseData = data
							.getStringExtra(BarcaIabHelper.RESPONSE_INAPP_PURCHASE_DATA);
					JSONObject purchase = new JSONObject(purchaseData);
					String developerPayload = purchase
							.getString(RESPONSE_PAYLOAD);
					if (developerPayload == null)
						developerPayload = "";
					if (getPurchasePayload().equals(developerPayload)) {
						billingHelper.handleActivityResult(requestCode,
								resultCode, data);
					}
				} catch (Exception e) {

				}
			}
		}
	}

	public void query() {
		// Listener that's called when we finish querying the items and
		// subscriptions we own
		
		BarcaIabHelper.QueryInventoryFinishedListener mGotInventoryListener = new BarcaIabHelper.QueryInventoryFinishedListener() {
			
			@Override
			public void onQueryInventoryFinished(BarcaIabResult result,
					BarcaInventory inventory) {
				// Have we been disposed of in the meantime? If so, quit.
				if (billingHelper == null)
					return;

				// Is it a failure?
				if (result.isFailure()) {
					return;
				}

				/*
				 * Check for items we own. Notice that for each purchase, we
				 * check the developer payload to see if it's correct! See
				 * verifyDeveloperPayload().
				 */

				// Do we have the premium upgrade?
				BarcaPurchase premiumPurchase = inventory
						.getPurchase("android.test.purchased");
				BarcaSkuDetails details = inventory
						.getSkuDetails("android.test.purchased");
				dialog.dismiss();
				List<ShopEntity> products = adapter.getList();
				ShopEntity marked = new ShopEntity(details.getSku());
				int index = products.indexOf(marked);
				ShopEntity product = products.get(index);
				product.setPrice(details.getPrice());
				products.remove(index);
				products.add(index, product);
				adapter.notifyDataSetChanged();

			}
		};
		
		ArrayList<String> skuList = new ArrayList<String>();
		skuList.add("android.test.purchased");
		billingHelper.queryInventoryAsync(true, skuList, mGotInventoryListener);
	}

	@Override
	public void onIabPurchaseFinished(BarcaIabResult arg0, BarcaPurchase arg1) {
		String sku = "android.test.purchased";
		if (arg0.isFailure()) {
//            dealWithPurchaseFailed(result);
        } else if (sku.equals(arg1.getSku())) {
            dealWithPurchaseSuccess(arg0, arg1);
        }
	}

	private void dealWithPurchaseSuccess(BarcaIabResult arg0, BarcaPurchase arg1) {
		billingHelper.consumeAsync(arg1, null);
	}

	private String getPurchasePayload() {
		return SharedPrefsHelper.getRegisterKey(PURCHASE_PAYLOAD_CACHE_KEY);
	}

	@Override
	public void onIabSetupFinished(BarcaIabResult arg0) {
		String purchaseToken = "inapp:"+getActivity().getPackageName()+":android.test.purchased";
//		int response = billingHelper.consume(3, getActivity().getPackageName(),purchaseToken);
//		 BarcaPurchase info = new BarcaPurchase(purchaseToken , "android.test.purchased" , 0);
		BarcaPurchase info = new BarcaPurchase(purchaseToken , "android.test.purchased" , 0);
         BarcaIabHelper.OnConsumeFinishedListener listener = new BarcaIabHelper.OnConsumeFinishedListener() {
				
				@Override
				public void onConsumeFinished(BarcaPurchase purchase, BarcaIabResult result) {
					if(result.isFailure())
						return;
					else
						query();
				}
			};
			billingHelper.consumeAsync(info, listener);
		
	}
}
