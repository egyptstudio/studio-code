package com.tawasol.fcbBarcelona.ui.fragments;
  
import java.util.ArrayList;
import java.util.List;
 



import com.tawasol.fcbBarcelona.R; 
 
import com.tawasol.fcbBarcelona.entities.Message;
import com.tawasol.fcbBarcelona.entities.SystemMessagesEntity;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnSystemMsgsListener;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.ui.adapters.RecentChatAdapter;
import com.tawasol.fcbBarcelona.ui.adapters.SystemMessagesAdapter;
import com.tawasol.fcbBarcelona.utils.UIUtils;

import android.os.Bundle; 
import android.view.LayoutInflater;
import android.view.View; 
import android.view.View.OnClickListener;
import android.view.ViewGroup; 
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout; 

/**
 * @author TURKI
 *  
 */
public class SystemMessagesFragment extends BaseFragment implements OnSystemMsgsListener{

	private ListView listView;
	private SystemMessagesAdapter adapter;
	private ArrayList<SystemMessagesEntity> list = new ArrayList<SystemMessagesEntity>();
	private RelativeLayout deleteBtn;
	private LinearLayout noRecentMsgFrame;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_system_messages, container, false);
		
		/** Initialize registration view **/
		init(rootView);

		return rootView;
	}

	/** Initialize registration view **/
	private void init(View rootView) {
//		deleteBtn = (RelativeLayout) getActivity().findViewById(R.id.deleteRightBtnLayout);
//		deleteBtn.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				getCheckedItem();
//			}
//		});

		noRecentMsgFrame = (LinearLayout) rootView.findViewById(R.id.initial_no_sys_msg);
		
		listView  = (ListView) rootView.findViewById(R.id.system_msgs_list);
		adapter   = new SystemMessagesAdapter(getActivity(), list);
		listView.setAdapter(adapter);
				
	} 
	
	 public void getCheckedItem() {
		for (SystemMessagesEntity p : adapter.getCheckedSystemMessagesEntity()) {
		   if (p.isChecked()){
			 UIUtils.showToast(getActivity(), p.getMessageText());
		   }
		}
	}

	@Override
	public void onSuccess() {
		hideLoadingDialog();
		List<SystemMessagesEntity> sysList = ChatManager.getInstance().getSysMessages();
		if(sysList.size()>0)
			noRecentMsgFrame.setVisibility(View.GONE);
		
		if (listView.getAdapter() == null){
			adapter = new SystemMessagesAdapter(getActivity(), sysList);
			listView.setAdapter(adapter);
		}else {
			list.clear();
			list.addAll(sysList);
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}
	
	/** Add LatestMessages listeners onResume **/
	@Override
	public void onResume() {
		showLoadingDialog();
		ChatManager.getInstance().addListener(this);
		ChatManager.getInstance().loadSysMessagesAsync();
		super.onResume();
	}

	/** Remove LatestMessages listeners onPause **/
	@Override
	public void onPause() {
		ChatManager.getInstance().removeListener(this);
		super.onPause();
	}
	
}
