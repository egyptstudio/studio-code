package com.tawasol.fcbBarcelona.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.entities.Contest;
import com.tawasol.fcbBarcelona.entities.ContestData;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnContestReceived;
import com.tawasol.fcbBarcelona.managers.ContestManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.ContestActivity;
import com.tawasol.fcbBarcelona.ui.activities.HomeActivity;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.TagListActivity;

public class ContestWinnersFragment extends BaseFragment implements
		OnContestReceived, OnClickListener {
	ListView winnersLV;
	ContestWinnerAdapter winnersAdapter;
	// List<Contest_Prize_Winners> winners;
	LinearLayout finishedContestLayout, runningContestLayout,
			initialStateLayout;
	TextView contestNameTxt;
	private static ContestWinnersFragment winnersFragment;
	private Button runningTopTenBtn, topTenBtn;;
	private ContestData contest;
	ProgressBar winnersPB;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contest_winners,
				container, false);

		viewById(rootView);

		// initView();

		// showLoadingDialog();
		winnersPB.setVisibility(View.VISIBLE);

		// get data from manager
		contest = ContestManager.getInstance().getContest();

		// if null call manager to get data from WB , else reset layouts
		// according to contest object value
		if (contest == null)
			ContestManager.getInstance().getContestData();
		else if (contest.getContestWinner() != null) {
			handleLayouts();
		}

		return rootView;
	}

	/**
	 * handle the layouts of Initial state , Running Contest state , Finished
	 * Contest state , Prizes announced state
	 **/
	private void handleLayouts() {
		// if contest is running or finished , then show
		// "Running contest state layout" and hide others
		if (contest.getContest().getStatus() == Contest.CONTEST_STATUS_RUNNING
				|| contest.getContest().getStatus() == Contest.CONTEST_STATUS_FINISHED) {
			runningContestLayout.setVisibility(View.VISIBLE);
			initialStateLayout.setVisibility(View.GONE);
			finishedContestLayout.setVisibility(View.GONE);
		}

		// if contest is in prizes announces state , then show
		// "Finished contest state layout" and hide others
		else if (contest.getContest().getStatus() == Contest.CONTEST_STATUS_PRIZES_ANNOUNCED) {
			if (contest.getContestWinner() != null && contest.getContestPrize() != null
					&& contest.getContest() != null
					&& !contest.getContestPrize().isEmpty()
					&& !contest.getContestWinner().isEmpty()) {
			runningContestLayout.setVisibility(View.GONE);
			initialStateLayout.setVisibility(View.GONE);
			finishedContestLayout.setVisibility(View.VISIBLE);

			initView();
			contestNameTxt.setText(contest.getContest().getContestName());
		}
			// these case not going to happen , but just in case as it happened when data was static server side :) 
			else 
			{
				runningContestLayout.setVisibility(View.VISIBLE);
				initialStateLayout.setVisibility(View.GONE);
				finishedContestLayout.setVisibility(View.GONE);
			}
		 	
		}
		// otherwise show "Running state layout" and hide others
		else {
			runningContestLayout.setVisibility(View.GONE);
			initialStateLayout.setVisibility(View.VISIBLE);
			finishedContestLayout.setVisibility(View.GONE);
		}
		// hideLoadingDialog();
		winnersPB.setVisibility(View.GONE);
	}

	private void viewById(View rootView) {
		winnersLV = (ListView) rootView
				.findViewById(R.id.contest_winner_listview);
		finishedContestLayout = (LinearLayout) rootView
				.findViewById(R.id.finished_contest_layout);
		runningContestLayout = (LinearLayout) rootView
				.findViewById(R.id.running_contest_layout);
		initialStateLayout = (LinearLayout) rootView
				.findViewById(R.id.contest_initial_state_layout);
		contestNameTxt = (TextView) rootView
				.findViewById(R.id.winners_contest_name_txt);
		runningTopTenBtn = (Button) rootView
				.findViewById(R.id.running_top_ten_photos_btn);
		topTenBtn = (Button) rootView.findViewById(R.id.top_ten_photos_btn);
		winnersPB = (ProgressBar) rootView
				.findViewById(R.id.contest_winners_frag_pb);

		// set on click of buttons
		runningTopTenBtn.setOnClickListener(this);
		topTenBtn.setOnClickListener(this);
	}

	private void initView() {
		// initialize the adapter
		winnersAdapter = new ContestWinnerAdapter();
		winnersLV.setAdapter(winnersAdapter);
	}

	public static ContestWinnersFragment newInstance() {
		if (winnersFragment == null)
			winnersFragment = new ContestWinnersFragment();
		return winnersFragment;
	}

	@Override
	public void onResume() {
		super.onResume();
		ContestManager.getInstance().addListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		ContestManager.getInstance().removeListener(this);
	}

	public class ContestWinnerAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return contest.getContestWinner().size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ContestWinnerViewHolder holder;

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				holder = new ContestWinnerViewHolder();
				convertView = inflater.inflate(
						R.layout.contest_winner_list_item, parent, false);
				holder.winnerName = (TextView) convertView
						.findViewById(R.id.contest_winner_name_txt);
				holder.winnerImage = (ImageView) convertView
						.findViewById(R.id.contest_winner_img);
				holder.winnerState = (FrameLayout) convertView
						.findViewById(R.id.contest_winner_frame_img);
				holder.onlineBtn = (ImageButton) convertView
						.findViewById(R.id.contest_online_btn);
				holder.followBtn = (ImageButton) convertView
						.findViewById(R.id.follow_winner_btn);
				holder.noOfPics = (TextView) convertView
						.findViewById(R.id.winner_num_of_pics_txt);
				holder.winnerNumber = (TextView) convertView
						.findViewById(R.id.contest_winner_num_txt);
				// holder.prizeText=(TextView)convertView.findViewById(R.id.contest_winner_prize_txt);
				holder.prizeImg = (ImageView) convertView
						.findViewById(R.id.contest_winner_prize_img);
				holder.winnerPB = (ProgressBar) convertView
						.findViewById(R.id.contest_winner_pb);
				holder.prizePB = (ProgressBar) convertView
						.findViewById(R.id.contest_prize_pb);

				// set the tag of buttons
				//holder.onlineBtn.setTag(position);
				holder.followBtn.setTag(position);
				holder.noOfPics.setTag(position);

				convertView.setTag(holder);
			} else {
				holder = (ContestWinnerViewHolder) convertView.getTag();
			}
			// set the winner name
			holder.winnerName.setText(contest.getContestWinner().get(position)
					.getWinnerName());

			// set the winner image
			App.getInstance()
					.getImageLoader()
					.displayImage(
							contest.getContestWinner().get(position)
									.getProfilePicUrl(), holder.winnerImage,
							App.getInstance().getDisplayOption(),
							new ImageLoadingListener() {

								@Override
								public void onLoadingStarted(String arg0,
										View arg1) {
									holder.winnerPB.setVisibility(View.VISIBLE);
								}

								@Override
								public void onLoadingFailed(String arg0,
										View arg1, FailReason arg2) {
									holder.winnerPB.setVisibility(View.GONE);
									holder.winnerImage
											.setImageResource(R.drawable.ic_launcher);
								}

								@Override
								public void onLoadingComplete(String arg0,
										View arg1, Bitmap arg2) {
									holder.winnerPB.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingCancelled(String arg0,
										View arg1) {

								}
							});

			// set the online  image
			if (contest.getContestWinner().get(position).isOnline())
				holder.onlineBtn.setVisibility(View.VISIBLE);
			else
				holder.onlineBtn.setVisibility(View.GONE);
			
			

			// set follow btn
			if (contest.getContestWinner().get(position).isFollowed())
				holder.followBtn.setImageResource(R.drawable.followed);
			else
				holder.followBtn.setImageResource(R.drawable.unfollowed);

			// set the state of the winner
			if (contest.getContestWinner().get(position).isPremium())
				holder.winnerState
						.setBackgroundResource(R.drawable.premium_frame);
			else
				holder.winnerState
						.setBackgroundResource(R.drawable.basic_frame);
			
						/*if (contest.getContestWinner().get(position).isOnline())
				holder.winnerState
						.setBackgroundResource(R.drawable.image_frameonline);
			else
				holder.winnerState
						.setBackgroundResource(R.drawable.image_frameoffline);*/

			// set number of pictures
			holder.noOfPics.setText(contest.getContestWinner().get(position)
					.getNoOfPics()
					+ "");

			// set the prize text
			// holder.prizeText.setText(contest.getContestPrize().get(position).getPrizeText());

			// set the prize image
			App.getInstance()
					.getImageLoader()
					.displayImage(
							contest.getContestPrize().get(position)
									.getPrizeImageUrl(), holder.prizeImg,
							App.getInstance().getDisplayOption(),
							new ImageLoadingListener() {

								@Override
								public void onLoadingStarted(String arg0,
										View arg1) {
									holder.prizePB.setVisibility(View.VISIBLE);
								}

								@Override
								public void onLoadingFailed(String arg0,
										View arg1, FailReason arg2) {
									holder.prizePB.setVisibility(View.GONE);
									holder.prizeImg
											.setImageResource(R.drawable.ic_launcher);
								}

								@Override
								public void onLoadingComplete(String arg0,
										View arg1, Bitmap arg2) {
									holder.prizePB.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingCancelled(String arg0,
										View arg1) {

								}
							});
			// set the winner number
			holder.winnerNumber.setText(contest.getContestPrize().get(position)
					.getPrizeRank()
					+ "");

			// set the click listener of fav
			/*holder.onlineBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (UserManager.getInstance().getCurrentUserId() != 0) {
						if (contest.getContestWinner()
								.get((Integer) v.getTag()).isFavorite()) {
							((ImageButton) v)
									.setImageResource(R.drawable.unfavourate);
							// ((ImageButton) getRowFromList(listView, position)
							// .findViewById(R.id.favourate_button))
							// .setImageResource(R.drawable.unfavourate);
							ContestManager.getInstance().handleFavo(
									UserManager.getInstance()
											.getCurrentUserId(),
									contest.getContestWinner()
											.get((Integer) v.getTag())
											.getWinnerId(), false);
						} else {
							((ImageButton) v)
									.setImageResource(R.drawable.favourate);
							ContestManager.getInstance().handleFavo(
									UserManager.getInstance()
											.getCurrentUserId(),
									contest.getContestWinner()
											.get((Integer) v.getTag())
											.getWinnerId(), true);
						}
					} else {

						getActivity().startActivityForResult(
								LogInActivity.getActivityIntent(getActivity(),
										UserManager.LOGIN_FOR_RESULT,
										new Intent()
													 * .putExtra(ContestManager.
													 * WINNER_INDEX_KEY,
													 * (Integer)v.getTag())
													 * .putExtra(ContestManager.
													 * WINNER_OBJ_KEY,
													 * contest.getContestWinner
													 * ()
													 * .get((Integer)v.getTag()
													 * ))
													 * .putExtra(ContestManager
													 * .WINNER_OPERATION_ID,
													 * ContestManager
													 * .ADD_FAV_WINNER)
													 * .putExtra(ContestManager.
													 * WINNER_ROW_VIEW,
													 * convertView)
													 ),
								ContestActivity.WINNER_INTENT_RESULT_CODE);
					}
				}
			});*/

			// set on click listener of follow
			holder.followBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (UserManager.getInstance().getCurrentUserId() != 0) {
						if (contest.getContestWinner()
								.get((Integer) v.getTag()).isFollowed()) {
							((ImageButton) v)
									.setImageResource(R.drawable.unfollowed);
							ContestManager.getInstance().handleFollow(
									UserManager.getInstance()
											.getCurrentUserId(),
									contest.getContestWinner()
											.get((Integer) v.getTag())
											.getWinnerId(), false);
						} else {
							((ImageButton) v)
									.setImageResource(R.drawable.followed);
							ContestManager.getInstance().handleFollow(
									UserManager.getInstance()
											.getCurrentUserId(),
									contest.getContestWinner()
											.get((Integer) v.getTag())
											.getWinnerId(), true);
						}
					} else {

						getActivity().startActivityForResult(
								LogInActivity.getActivityIntent(getActivity(),
										UserManager.LOGIN_FOR_RESULT,
										new Intent()/*
													 * .putExtra(ContestManager.
													 * WINNER_INDEX_KEY,
													 * (Integer)v.getTag())
													 * .putExtra(ContestManager.
													 * WINNER_OBJ_KEY,
													 * contest.getContestWinner
													 * ()
													 * .get((Integer)v.getTag()
													 * ))
													 * .putExtra(ContestManager
													 * .WINNER_OPERATION_ID,
													 * ContestManager
													 * .FOLLOW_WINNER)
													 */),
								ContestActivity.WINNER_INTENT_RESULT_CODE);
					}
				}
			});

			// set on click listener of no of pics
			holder.noOfPics.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					startActivity(TagListActivity.getIntent(getActivity(), 0,
							null,
							contest.getContestWinner()
									.get((Integer) v.getTag()).getWinnerId(),
							contest.getContestWinner()
									.get((Integer) v.getTag()).getWinnerName(),
							false));
				}
			});
			return convertView;
		}

		private class ContestWinnerViewHolder {
			TextView winnerName;
			ImageView winnerImage;
			FrameLayout winnerState;
			ImageButton onlineBtn;
			ImageButton followBtn;
			TextView noOfPics;
			TextView winnerNumber;
			// TextView prizeText;
			ImageView prizeImg;
			ProgressBar winnerPB;
			ProgressBar prizePB;
		}

	}

	@Override
	public void onSuccess(ContestData obj) {
		winnersPB.setVisibility(View.GONE);
		// hideLoadingDialog();

		// validate the returned contest object and lists of it
		if (obj != null) {
			

				contest = obj;
				// update list and adapter
				handleLayouts();
				/*initView();
				// set the contest name
				contestNameTxt.setText(contest.getContest().getContestName());*/
			
		} else // if there the returned data wasn't valid , then hide the
				// current layout and show no running contest layout
		{
			initialStateLayout.setVisibility(View.VISIBLE);
			runningContestLayout.setVisibility(View.GONE);
			finishedContestLayout.setVisibility(View.GONE);
		}

	}

	@Override
	public void onException(AppException ex) {
		winnersPB.setVisibility(View.GONE);
		// hideLoadingDialog();

		// hide the displayed data layout of prizes , and show no running
		// contest layout
		initialStateLayout.setVisibility(View.VISIBLE);
		runningContestLayout.setVisibility(View.GONE);
		finishedContestLayout.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.running_top_ten_photos_btn:
		case R.id.top_ten_photos_btn:
			startActivity(HomeActivity.getActivityIntent(getActivity(),
					FragmentInfo.TOP_TEN , false , false));
			break;

		default:
			break;
		}
	}
	/*
	 * @Override public void onActivityResult(int requestCode, int resultCode,
	 * Intent data) { super.onActivityResult(requestCode, resultCode, data);
	 * if(resultCode == Activity.RESULT_OK && requestCode ==
	 * ContestActivity.WINNER_INTENT_RESULT_CODE){
	 * ContestManager.getInstance().handleWinnerOperations(data);
	 * 
	 * } }
	 */

}
