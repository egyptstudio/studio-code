package com.tawasol.fcbBarcelona.ui.fragments;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.exception.NullIntentException;
import com.tawasol.fcbBarcelona.listeners.OnSendVerificationListener;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.ForgetPassowrdVerificationActivity;
import com.tawasol.fcbBarcelona.ui.activities.HomeActivity;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;
import com.tawasol.fcbBarcelona.utils.ValidatorUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author Mohga
 * */
public class ForgetPasswordFragment extends BaseFragment implements
		OnClickListener, OnSendVerificationListener {

	/* Declarations */
	Button sendBtn;
	EditText emailTxt;
	int navigationTypeId;
	Intent intentAction;
	String email;

	/* End Of Declarations */

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_forgetpassword,
				container, false);
		HandleLoginType_And_Intent();
		// get controls from XML file
		sendBtn = (Button) rootView.findViewById(R.id.forgetPassword_btn_id);
		emailTxt = (EditText) rootView
				.findViewById(R.id.forget_password_email_txt_id);
		// set on click listener of send verification code button
		sendBtn.setOnClickListener(this);

		// set email
		if (email != null && !email.isEmpty())
			emailTxt.setText(email);
		return rootView;
	}

	/**
	 * this method should be called when starting this fragment to specify the
	 * loginType and Intent action if exists ! so it will handled on
	 * loginSuccess method :)
	 **/
	public void HandleLoginType_And_Intent() {
		this.navigationTypeId = getActivity().getIntent().getExtras()
				.getInt(LogInActivity.LOGIN_TYPE_KEY);
		this.intentAction = getActivity().getIntent().getExtras()
				.getParcelable(LogInActivity.INTENT_NAVIGATION_KEY);
		this.email = getActivity().getIntent().getExtras()
				.getString(LogInActivity.EMAIL_KEY);
	}

	/** To Client-Validate Data of Send Verification Code */
	private boolean isValidData() {
		String email = emailTxt.getText().toString();
		// check if email required
		if (ValidatorUtils.isRequired(email)) {
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.Login_emailMissingAlert));
			return false;
		}
		// check if email format is valid
		else if (!ValidatorUtils.isValidEmail(email)) {
			UIUtils.showToast(getActivity(),
					getResources().getString(R.string.email_formate_error));
			return false;
		}
		// success
		else
			return true;
	}

	public static Fragment newFragment() {
		return new ForgetPasswordFragment();
	}

	@Override
	public void onResume() {
		UserManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
		UserManager.getInstance().removeListener(this);
		super.onPause();
	}

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());

	}

	@Override
	public void onSuccess(String userId) {

		hideLoadingDialog();
		UIUtils.showToast(getActivity(),getString(R.string.verification_sent_to_email));
		//procedNextScreen(navigationTypeId,intentAction);
		//userId = "645";		
		startActivity(ForgetPassowrdVerificationActivity.getActivityIntent(
				getActivity(), userId, emailTxt.getText().toString(),
				navigationTypeId, intentAction));

		getActivity().finish();
	}

	/** Basyouni's Method to handle pending actions and navigations */
	void procedNextScreen(int login_Type, Intent intent) {
		switch (login_Type) {
		case UserManager.NORMAL_LOGIN:
			startActivity(HomeActivity.getActivityIntent(getActivity())/*
																		 * new
																		 * Intent
																		 * (
																		 * getActivity
																		 * (),
																		 * HomeActivity
																		 * .
																		 * class
																		 * )
																		 */);
			break;
		case UserManager.LOGIN_WITH_INTENT:
			if (intent != null) {
				startActivity(intent);
			} else
				throw new NullIntentException();
		case UserManager.LOGIN_FOR_RESULT:
			if (intent != null) {
				getActivity().setResult(Activity.RESULT_OK,
						new Intent().putExtras(intent.getExtras()));
			} else
				throw new NullIntentException();
		default:
			getActivity().finish();
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.forgetPassword_btn_id:
			if (NetworkingUtils.isNetworkConnected(getActivity())) {
				if (isValidData()) {
					showLoadingDialog();
					UserManager.getInstance().sendVerifcationCode(
							emailTxt.getText().toString());
				}
			} else {
				UIUtils.showToast(getActivity(), getActivity().getResources().getString(R.string.connection_noConnection));
			}
			break;

		default:
			break;
		}

	}
}
