package com.tawasol.fcbBarcelona.ui.fragments;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.google.android.gms.internal.fm;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.entities.PostViewModel;
import com.tawasol.fcbBarcelona.entities.WALLPosts;
import com.tawasol.fcbBarcelona.managers.WallManager;
import com.tawasol.fcbBarcelona.ui.activities.PostFullScreenDetailsActivity;
import com.tawasol.fcbBarcelona.ui.custom.pager.VerticalViewPager;
import com.tawasol.fcbBarcelona.utils.Utils;

public class PostFullScreenDetailsFragment extends BaseFragment {

	private PagerAdapter mAdapter;
	private long currentUpdateTime;
	private long lastRequestTime;
	private VerticalViewPager pager;
	private static final int REQUEST_TYPE_OLDER = 1;
	private static final int REQUEST_TYPE_NEWER = 2;
	private List<PostViewModel> posts = new ArrayList<PostViewModel>();

	interface UpdatableAdapter {
		void update(int position);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.post_full_screen_details_fragment, container, false);

		populateScreen(rootView);

		return rootView;
	}

	private void populateScreen(View rootView) {
		pager = (VerticalViewPager) rootView.findViewById(R.id.post_full_scr_pager);

		boolean firstTime = false;

		/**
		 * Get all intent value:
		 * "WhichTab, IndexOfCurrentItem, IsFilteredList Or Not and if filtered check IsFavorite Or Not"
		 **/
		int groupPosition = 0;
		int whichTab = getActivity().getIntent().getIntExtra(PostFullScreenDetailsActivity.WHICH_TAB, 0);
		int currentPosition = getActivity().getIntent().getIntExtra(PostFullScreenDetailsActivity.CURRENT_INDEX_BUNDLE_KEY, 0);
		boolean isFiltered = getActivity().getIntent().getBooleanExtra(PostFullScreenDetailsActivity.IS_FILTERED, false);
		boolean isFavorite = getActivity().getIntent().getBooleanExtra(PostFullScreenDetailsActivity.IS_FAVORITE, false);

		/** If tab is TopTen so we need group position of the current item **/
		if (whichTab == FragmentInfo.TOP_TEN) {
			groupPosition = getActivity().getIntent().getIntExtra(PostFullScreenDetailsActivity.GROUP_POSITION, 0);
		}

		if (mAdapter == null) {
			/** Set current update time to request GetUpdate **/
			currentUpdateTime = setCurrentUpdateTime(whichTab);

			/** Get posts from selected tab **/
			List<PostViewModel> comingPosts = loadOfflineData(whichTab, groupPosition, currentPosition, isFiltered);

			/** Set pagerAdapter with coming posts from selected tab **/
			mAdapter = new PagerAdapter(getChildFragmentManager(), comingPosts, whichTab, isFiltered, isFavorite);
			firstTime = true;
		}

		pager.setAdapter(mAdapter);
		pager.setPagingEnabled(Utils.isPortrait());

		if (firstTime) {
			/**
			 * If tab is not TopTen Or MyPic we will add EmptyPost so we add +1
			 * on currentPosition
			 **/
			if (whichTab == FragmentInfo.TOP_TEN || whichTab == FragmentInfo.MY_PIC)
				pager.setCurrentItem(currentPosition);
			else
				pager.setCurrentItem(currentPosition + 1);
		}

		/** Set OverLayout to help user how to navigate **/
		initOverLay(rootView);
	}

	private class PagerAdapter extends FragmentStatePagerAdapter implements	UpdatableAdapter {

		private int tabType = 0;
		private boolean isFiltered;
		private boolean isFavorite;
		private FragmentManager fragmentManager;

		public PagerAdapter(FragmentManager fm, List<PostViewModel> posts, int tabType, boolean isFiltered, boolean isFavorite) {
			super(fm);
			fragmentManager = fm;
			this.tabType = tabType;
			this.isFiltered = isFiltered;
			this.isFavorite = isFavorite;

			// Collections.sort(posts, Collections.reverseOrder());

			if (tabType == FragmentInfo.MY_PIC || tabType == FragmentInfo.TOP_TEN) {
				PostFullScreenDetailsFragment.this.posts = posts;
			} else {
//				PostFullScreenDetailsFragment.this.posts.add(0, WallManager.getInstance().getEmptyPost());
				PostFullScreenDetailsFragment.this.posts.addAll(posts);
//				PostFullScreenDetailsFragment.this.posts.add(WallManager.getInstance().getEmptyPost());
			}
		}

		@Override
		public Fragment getItem(int position) {

			/**
			 * Set OnPageChangeListener on pager to check if the user request
			 * GetUpdate Or GetMore
			 **/
			pager.setOnPageChangeListener(new OnPageChangeListener() {
				@Override
				public void onPageSelected(int arg0) {
					// TODO Auto-generated method stub

					/** Check if the post is Empty to execute Newer Or Older **/
//					if (WallManager.isEmptyPost(posts.get(pager
//							.getCurrentItem()))) {
//						new LoadMoreTask(pager.getCurrentItem(),
//								PagerAdapter.this, isFiltered, isFavorite).execute(
//								pager.getCurrentItem() == 0 ? REQUEST_TYPE_NEWER
//										: REQUEST_TYPE_OLDER, tabType);
//					}
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub
				}
			});
			return VerticalPageFragment.newInstance(posts.get(position),
					currentUpdateTime, tabType , getActivity().getIntent().getIntExtra(
							PostFullScreenDetailsActivity.CURRENT_INDEX_BUNDLE_KEY, 0));
		}

		@Override
		public int getCount() {
			return posts.size();
		}

		@Override
		public int getItemPosition(Object object) {
			// TODO Auto-generated method stub
			return POSITION_NONE;
		}

		public void addPosts(List<PostViewModel> newPosts) {
			if (newPosts.size() > 0) {

				PostViewModel firstNewPost = newPosts.get(0);
				PostViewModel lastPost = posts.get(posts.size() - 1);

				if (firstNewPost.compareTo(lastPost) == -1) {
					/**
					 * first.lastUpdateTime - last.lastUpdateTime (last > first)
					 **/
					/** 1419242442 - 0 **/
					posts.remove(0);
					posts.addAll(0, newPosts);
//					posts.add(0, WallManager.getInstance().getEmptyPost());
				} else {
					posts.remove(posts.size() - 1);
					posts.addAll(newPosts);
//					posts.add(WallManager.getInstance().getEmptyPost());
				}
				if (mAdapter != null
						&& !fragmentManager.getFragments().isEmpty()
						&& getActivity() != null
						&& !getActivity().isFinishing())
					notifyDataSetChanged();
			} else
				System.out.println("No More Posts");
		}

		@Override
		public void update(int position) {
			// TODO Auto-generated method stub
			getItem(position);
		}
	}

	public Fragment findFragmentByPosition(int position) {
		// FragmentPagerAdapter fragmentPagerAdapter =
		// getActivity().getFragmentPagerAdapter();
		return getActivity().getSupportFragmentManager().findFragmentByTag(
				"android:switcher:" + R.id.post_full_scr_pager + ":"
						+ pager.getCurrentItem());
	}

	private void initOverLay(final View rootView) {
		if (WallManager.getInstance().isInitialPhotoDetails())
			// Details page has already been initialized, no need to show the
			// overlay
			return;

		WallManager.getInstance().markPhotoDetailsInitialized();

		View overlayView = getOverlayView(rootView.getContext());
		((ViewGroup) rootView).addView(overlayView);
		overlayView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// The overlay removes itself when clicked
				((ViewGroup) v.getParent()).removeView(v);
			}
		});
	}

	private static View getOverlayView(Context context) {
		RelativeLayout container = new RelativeLayout(context);
		container.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		container.setBackgroundColor(context.getResources().getColor(R.color.semi_transparent_black));
		container.addView(getArrowImageView(R.drawable.top_arrows, context));
		container.addView(getArrowImageView(R.drawable.buttom_arrows, context));
		container.addView(getArrowImageView(R.drawable.right_arrows, context));
		container.addView(getArrowImageView(R.drawable.left_arrows, context));

		return container;
	}

	private static View getArrowImageView(int arrowResId, Context context) {
		ImageView img = new ImageView(context);
		img.setImageResource(arrowResId);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		if (arrowResId == R.drawable.top_arrows) {
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			params.addRule(RelativeLayout.CENTER_HORIZONTAL);
		} else if (arrowResId == R.drawable.buttom_arrows) {
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params.addRule(RelativeLayout.CENTER_HORIZONTAL);
		} else if (arrowResId == R.drawable.right_arrows) {
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.addRule(RelativeLayout.CENTER_VERTICAL);
		} else if (arrowResId == R.drawable.left_arrows) {
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			params.addRule(RelativeLayout.CENTER_VERTICAL);
		}
		img.setLayoutParams(params);
		return img;
	}

	private List<PostViewModel> loadOfflineData(int whichTab, int groupPosition, int currentPosition, boolean isFiltered) {
		List<PostViewModel> comingPosts = new ArrayList<PostViewModel>();

		switch (whichTab) {

		/** If LatestTab check if filtered or not, then get posts List **/
		case FragmentInfo.LATEST_TAB:
			if (isFiltered)
				comingPosts.addAll(WallManager.getInstance().getfilteredList());
			else
				comingPosts.addAll(WallManager.getInstance().getLatest());
			break;

		/** If TopTenTab get TopTen posts List of selected groupPosition **/
		case FragmentInfo.TOP_TEN:
			comingPosts.addAll(WallManager.getInstance().getTopTenSessons().get(groupPosition).getPosts());
			break;

		/** If WallTab check if filtered or not, then get posts List **/
		case FragmentInfo.WALL:
			if (isFiltered)
				comingPosts.addAll(WallManager.getInstance().getfilteredList());
			else
				comingPosts.addAll(WallManager.getInstance().getWall());
			break;

		/** If MyPicTab get MyPic selected post only "No vertical swipe" **/
		case FragmentInfo.MY_PIC:
			comingPosts.add(WallManager.getInstance().getMyPics().get(currentPosition));
			break;

		default:
			break;
		}
		return comingPosts;
	}

	private long setCurrentUpdateTime(int whichTab) {
		long currentUpdateTimeOfTab = 0;
		if (whichTab == FragmentInfo.LATEST_TAB) {
			;
			currentUpdateTimeOfTab = WallManager.getInstance()
					.getLatestCurrentTime();
		} else if (whichTab == FragmentInfo.WALL) {
			currentUpdateTimeOfTab = WallManager.getInstance()
					.getWallCurrentTime();
		} else if (whichTab == FragmentInfo.MY_PIC) {
			currentUpdateTimeOfTab = WallManager.getInstance()
					.getMyPicsCurrentTime();
		}
		return currentUpdateTimeOfTab;
	}

	private class LoadMoreTask extends AsyncTask<Integer, Void, List<PostViewModel>> {
		private WeakReference<UpdatableAdapter> adapterRef;
		private int position;
		private int type;
		private boolean isFiltered;
		private boolean isFavorite;

		public LoadMoreTask(int position, UpdatableAdapter adapter, boolean isFiltered, boolean isFavorite) {
			this.position = position;
			this.isFiltered = isFiltered;
			this.isFavorite = isFavorite;
			adapterRef = new WeakReference<UpdatableAdapter>(adapter);
		}

		@Override
		protected List<PostViewModel> doInBackground(Integer... params) {
			type = params[0];
			int methodName = params[1];
			List<PostViewModel> posts = null;

			if (type == REQUEST_TYPE_NEWER) {
				WALLPosts wallpost = WallManager.getInstance().getNewerPosts(currentUpdateTime, getMethodName(methodName),
						isFiltered, isFavorite);
				if (wallpost != null && wallpost.getPosts() != null
						&& wallpost.getPosts().size() > 0) {
					posts = wallpost.getPosts();
					currentUpdateTime = wallpost.getCurrentRequestTime();
				}
			} else if (type == REQUEST_TYPE_OLDER) {
				lastRequestTime = PostFullScreenDetailsFragment.this.posts.get(
						PostFullScreenDetailsFragment.this.posts.size() - 2).getLastUpdateTime();
				WALLPosts wallpost = WallManager.getInstance().getOlderPosts(lastRequestTime, getMethodName(methodName), isFiltered,
						isFavorite);
				if (wallpost != null && wallpost.getPosts() != null
						&& wallpost.getPosts().size() > 0) {
					posts = wallpost.getPosts();
				}
			}
			return posts;
		}

		@Override
		protected void onPostExecute(List<PostViewModel> result) {
			UpdatableAdapter adapter = adapterRef.get();
			adapter.update(position);
			if (result == null)
				return;
			if (type == REQUEST_TYPE_NEWER) {
				result = checkRepetaedItemInList(posts, result);
				mAdapter.addPosts(result);
			} else {
				// View v = pager.findViewWithTag(pager.getCurrentItem());
				// v.invalidate();
				mAdapter.addPosts(result);
			}
		}

		public String getMethodName(int tabName) {
			String methodName = null;
			if (tabName == FragmentInfo.LATEST_TAB)
				methodName = "wall/getLatest";
			else if (tabName == FragmentInfo.WALL)
				methodName = "wall/getWall";
			else if (tabName == FragmentInfo.MY_PIC)
				methodName = "wall/getMyPosts";
			else if (tabName == FragmentInfo.TOP_TEN)
				methodName = "wall/getTopTen";
			return methodName;
		}
	}

	public List<PostViewModel> checkRepetaedItemInList(List<PostViewModel> oldPosts, List<PostViewModel> newPosts) {

		/**
		 * If new posts is > WallManager.NUM_OF_POSTS="10", then clear all
		 * oldList and add new posts
		 **/
		if (newPosts.size() > WallManager.NUM_OF_POSTS) {
			oldPosts.clear();
			oldPosts.addAll(newPosts);

			/**
			 * If new posts is < WallManager.NUM_OF_POSTS="10", then clear the
			 * repeated posts only in OldList and add new posts
			 **/
		} else if (newPosts.size() < WallManager.NUM_OF_POSTS && newPosts.size() > 0) {
			for (int x = 0; x < newPosts.size(); x++) {
				for (int y = 0; y < oldPosts.size(); y++) {
					if (newPosts.get(x).getPostId() == oldPosts.get(y).getPostId()) {
						oldPosts.remove(y);
					}
				}
				oldPosts.addAll(newPosts);
			}
		}
		return oldPosts;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// if(mAdapter != null)
		// mAdapter.notifyDataSetChanged();
	}

}
