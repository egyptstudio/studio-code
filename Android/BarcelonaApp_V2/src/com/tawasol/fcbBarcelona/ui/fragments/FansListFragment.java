package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.commonsware.cwac.endless.EndlessAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.customView.ViewServer;
import com.tawasol.fcbBarcelona.entities.Fan;
import com.tawasol.fcbBarcelona.entities.FansFilterParams;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnFanListRecieved;
import com.tawasol.fcbBarcelona.listeners.OnPremiumBarRecieved;
import com.tawasol.fcbBarcelona.managers.FanManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.FanProfileActivity;
import com.tawasol.fcbBarcelona.ui.activities.FansGridActivity;
import com.tawasol.fcbBarcelona.ui.activities.LogInActivity;
import com.tawasol.fcbBarcelona.ui.activities.PremiumUsersActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivityTabs;
import com.tawasol.fcbBarcelona.ui.activities.TagListActivity;
import com.tawasol.fcbBarcelona.ui.activities.UserProfileActivity;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class FansListFragment extends Fragment implements OnFanListRecieved,
		OnPremiumBarRecieved {

	public static final int INTENT_RESULT_CODE = 58484;
	DisplayImageOptions options;
	protected GridView listView;
	LinearLayout premiumContainer;
	List<Fan> fans;
	private ProgressBar loadingBar;
	HorizontalScrollView horizontalScrollView;
	ImageAdapter adapter;
	private boolean recieved;
	private boolean listReceieved;
	List<Fan> PremiumFans;
	int bottomBarHeight;
	int viewType;
	ProgressDialog fansLoading;
	TextView no_followers;
	FrameLayout noFollowersLay;
	LinearLayout listLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ViewServer.get(getActivity()).addWindow(getActivity());
		fansLoading = new ProgressDialog(getActivity(), R.style.MyTheme);
		fansLoading.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		fansLoading.setCancelable(false);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		ViewServer.get(getActivity()).removeWindow(getActivity());
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fan_grid_list_fragment,
				container, false);

		viewType = (Integer) getActivity().getIntent().getExtras()
				.get(FansGridActivity.VIEWTYPE);
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.fan_no_image)
				.resetViewBeforeLoading(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.showImageForEmptyUri(R.drawable.fan_no_image)
				.showImageOnFail(R.drawable.fan_no_image).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		initViews(rootView);
		// TODO fans = from manager
		return rootView;
	}

	private void initViews(View rootView) {
		noFollowersLay = (FrameLayout) rootView.findViewById(R.id.no_followers_layout);
		loadingBar = (ProgressBar) rootView.findViewById(R.id.loadingBar);
		listView = (GridView) rootView.findViewById(R.id.fan_grid);
		no_followers = (TextView) rootView.findViewById(R.id.no_followers);
		listLayout = (LinearLayout) rootView
				.findViewById(R.id.postLayout);
		PauseOnScrollListener listener = new PauseOnScrollListener(App
				.getInstance().getImageLoader(), true, true);
		listView.setOnScrollListener(listener);
		premiumContainer = (LinearLayout) rootView
				.findViewById(R.id.premiumContainer);
		horizontalScrollView = (HorizontalScrollView) rootView
				.findViewById(R.id.horizontalScrollView1);
		AdView mAdView = (AdView) rootView.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
		final LinearLayout adContainer = (LinearLayout) rootView
				.findViewById(R.id.adsLayout);
		mAdView.setAdListener(new AdListener() {
			@Override
			public void onAdFailedToLoad(int errorCode) {
				super.onAdFailedToLoad(errorCode);
				adContainer.setVisibility(View.GONE);
			}
		});

		listLayout.setPadding(0, 0, 0,
				((BaseActivity) getActivity()).getBottomBarHeight() - 70);
	}

	void showToast(String msg) {
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onResume() {
		FanManager.getInstance().addListener(this);
		if (adapter != null)
			adapter.notifyDataSetChanged();
		List<Integer> countries = new ArrayList<Integer>();
		if (viewType == FansGridActivity.LOADFANS) {
			// fansLoading.show();
			loadingBar.setVisibility(View.VISIBLE);
			FanManager.getInstance().clearFans();
			FanManager.getInstance().getFans(
					new FansFilterParams(UserManager.getInstance()
							.getCurrentUserId(), 0,
							FansFilterParams.FILTER_ANY_GENDER,
							FansFilterParams.FILTER_ONLINE_AND_OFFLINE,
							FansFilterParams.FILTER_FAVOURATE_OR_NOT,
							FansFilterParams.ANY_USER, countries,
							FansFilterParams.NOT_PREMIUM_USER,
							FansFilterParams.SORT_BY_LOCATION_AND_ONLINE, ""));
			FanManager.getInstance().getPremiumBarFromServer();
		} else if (viewType == FansGridActivity.LOADFOLLOWERS) {
			// fansLoading.show();
			loadingBar.setVisibility(View.VISIBLE);
			FanManager.getInstance().clearFollowList();
			FanManager.getInstance().getFollowList(
					UserManager.getInstance().getCurrentUserId(),
					getActivity().getIntent().getIntExtra(
							FansGridActivity.FAN_ID, 0), FanManager.FOLLOWERS,
					0);
		} else if (viewType == FansGridActivity.LOADFOLLOWING) {
			// fansLoading.show();
			loadingBar.setVisibility(View.VISIBLE);
			FanManager.getInstance().clearFollowList();
			FanManager.getInstance().getFollowList(
					UserManager.getInstance().getCurrentUserId(),
					getActivity().getIntent().getIntExtra(
							FansGridActivity.FAN_ID, 0), FanManager.FOLLOWING,
					0);
		} else if (viewType == FansGridActivity.LOADFOLLOWINGSuggestions) {
			// fansLoading.show();
			loadingBar.setVisibility(View.VISIBLE);
			FanManager.getInstance().clearFollowList();
			FanManager.getInstance().getFansSuggestions(0);
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		FanManager.getInstance().removeListener(this);
		super.onPause();
	}

	private class ViewHolder {
		ImageView fanProfileImage;
		RelativeLayout fanState;
		// ProgressBar progressBar;
		ImageButton onlineState;
		ImageButton follow;
		TextView fanName;
		TextView numOfPics;
	}

	public class ImageAdapter extends BaseAdapter {

		List<Fan> fans;

		public ImageAdapter(List<Fan> fans) {
			this.fans = fans;
		}

		@Override
		public int getCount() {
			return this.fans.size();
		}

		@Override
		public Object getItem(int position) {
			return this.fans.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		public View getRowFromList(AbsListView listView, int id) {
			return listView.getChildAt(id - listView.getFirstVisiblePosition());
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			final Fan fan = this.fans.get(position);
			if (view == null) {
				view = getActivity().getLayoutInflater().inflate(
						R.layout.fan_list_item_modified, parent, false);
				holder = new ViewHolder();
				holder.numOfPics = (TextView) view
						.findViewById(R.id.num_of_pics);
				holder.fanProfileImage = (ImageView) view
						.findViewById(R.id.fan_profile_img_id);
				// holder.progressBar = (ProgressBar) view
				// .findViewById(R.id.progress);
				holder.onlineState = (ImageButton) view
						.findViewById(R.id.onlineState);
				holder.follow = (ImageButton) view
						.findViewById(R.id.followed_fan);
				holder.fanName = (TextView) view.findViewById(R.id.fanName);
				holder.fanState = (RelativeLayout) view
						.findViewById(R.id.fan_profile_frame);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}
			if (holder != null) {
				// view.setTag(fan.getFanID());
				if (holder.onlineState != null)
					if (fan.isFanOnline() == 1) {
						holder.onlineState.setVisibility(View.VISIBLE);
						holder.onlineState
								.setBackgroundResource(R.drawable.online_shape);
					} else if (fan.isFanOnline() == 2) {
						holder.onlineState.setVisibility(View.VISIBLE);
						holder.onlineState
								.setBackgroundResource(R.drawable.away_shape);
					} else
						holder.onlineState.setVisibility(View.GONE);

				if (holder.fanState != null)
					if (fan.isPremium())
						holder.fanState
								.setBackgroundResource(R.drawable.premium_frame);
					else
						holder.fanState
								.setBackgroundResource(R.drawable.basic_frame);
				if (holder.follow != null) {
					if (UserManager.getInstance().getCurrentUserId() != fan
							.getFanID()) {
						holder.follow.setVisibility(View.VISIBLE);
						if (fan.isFollowed()) {
							holder.follow.setImageResource(R.drawable.followed);

						} else {
							holder.follow
									.setImageResource(R.drawable.unfollowed);
						}
					} else {
						holder.follow.setVisibility(View.GONE);
					}
					holder.follow
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									if (UserManager.getInstance()
											.getCurrentUserId() != 0
											&& UserManager.getInstance()
													.getCurrentUserId() != fan
													.getFanID()) {
										if (fan.isFollowed()) {
											((ImageButton) getRowFromList(
													listView, position)
													.findViewById(
															R.id.followed_fan))
													.setImageResource(R.drawable.unfollowed);
											FanManager
													.getInstance()
													.handleFollow(
															UserManager
																	.getInstance()
																	.getCurrentUserId(),
															fan.getFanID(),
															false,
															getActivity());
										} else {
											((ImageButton) getRowFromList(
													listView, position)
													.findViewById(
															R.id.followed_fan))
													.setImageResource(R.drawable.followed);
											FanManager
													.getInstance()
													.handleFollow(
															UserManager
																	.getInstance()
																	.getCurrentUserId(),
															fan.getFanID(),
															true, getActivity());
										}
									} else {
										getActivity()
												.startActivityForResult(
														LogInActivity
																.getActivityIntent(
																		getActivity(),
																		UserManager.LOGIN_FOR_RESULT,
																		new Intent()),
														INTENT_RESULT_CODE);
									}
								}
							});
				}
				if (holder.fanName != null)
					holder.fanName.setText(fan.getFanName());
				if (holder.numOfPics != null) {
					holder.numOfPics.setText(String.valueOf(fan
							.getFanPicNumber()));
					holder.numOfPics
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									startActivity(TagListActivity.getIntent(
											getActivity(), 0, null,
											fan.getFanID(), fan.getFanName(),
											false));
								}
							});
				}
				// holder.favorute.setOnClickListener(new View.OnClickListener()
				// {
				//
				// @Override
				// public void onClick(View v) {
				// if (UserManager.getInstance().getCurrentUserId() != 0) {
				// if (fan.isFavorite()) {
				// ((ImageButton) getRowFromList(listView, position)
				// .findViewById(R.id.favourate_button))
				// .setImageResource(R.drawable.unfavourate);
				// FanManager.getInstance().handleFavo(
				// UserManager.getInstance()
				// .getCurrentUserId(),
				// fan.getFanID(), false);
				// } else {
				// ((ImageButton) getRowFromList(listView, position)
				// .findViewById(R.id.favourate_button))
				// .setImageResource(R.drawable.favourate);
				// FanManager.getInstance().handleFavo(
				// UserManager.getInstance()
				// .getCurrentUserId(),
				// fan.getFanID(), true);
				// }
				// } else {
				// getActivity().startActivityForResult(
				// LogInActivity.getActivityIntent(getActivity(),
				// UserManager.LOGIN_FOR_RESULT,
				// new Intent()), INTENT_RESULT_CODE);
				// }
				//
				// }
				// });

				display(holder.fanProfileImage, fan.getFanPicture(), null);
				holder.fanProfileImage
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								if (fan.getFanID() == UserManager.getInstance()
										.getCurrentUserId())
									startActivity(UserProfileActivity
											.getActivityIntent(getActivity()));
								else
									startActivity(FanProfileActivity.getIntent(
											getActivity(), fan.getFanID(),
											fan.getFanName()));
							}
						});
			}
			return view;
		}

		public void addToList(List<Fan> tempList) {
			// this.fans.addAll(tempList);'
			notifyDataSetChanged();
		}
	}

	public void display(ImageView img, String url, final ProgressBar spinner) {
		App.getInstance().getImageLoader().displayImage(url, img, options);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK
				&& requestCode == INTENT_RESULT_CODE) {
			((BaseActivity) getActivity()).refreshMenu();
		}
	}

	private class updatePosts extends EndlessAdapter {
		List<Fan> tempList;

		public updatePosts(Context context, ImageAdapter wrapped,
				int pendingResource) {
			super(context, wrapped, pendingResource);
		}

		@Override
		protected void appendCachedData() {
			if (getWrappedAdapter() != null){
				adapter.notifyDataSetChanged();
				((ImageAdapter) getWrappedAdapter()).addToList(tempList);
			}
			tempList = null;
		}

		@Override
		protected boolean cacheInBackground() throws Exception {
			List<Integer> countries = new ArrayList<Integer>();
			if (adapter.getCount() > 0) {
				if (viewType == FansGridActivity.LOADFANS) {
					tempList = FanManager
							.getInstance()
							.getFansFromServer(
									new FansFilterParams(
											UserManager.getInstance()
													.getCurrentUserId(),
											adapter.getCount(),
											FansFilterParams.FILTER_ANY_GENDER,
											FansFilterParams.FILTER_ONLINE_AND_OFFLINE,
											FansFilterParams.FILTER_FAVOURATE_OR_NOT,
											FansFilterParams.ANY_USER,
											countries,
											FansFilterParams.NOT_PREMIUM_USER,
											FansFilterParams.SORT_BY_LOCATION_AND_ONLINE,
											""));
				} else if (viewType == FansGridActivity.LOADFOLLOWERS) {
					tempList = FanManager.getInstance()
							.getFollowListFromServer(
									UserManager.getInstance()
											.getCurrentUserId(),
									getActivity().getIntent().getIntExtra(
											FansGridActivity.FAN_ID, 0),
									FanManager.FOLLOWERS, adapter.getCount());
				} else if (viewType == FansGridActivity.LOADFOLLOWING) {
					tempList = FanManager.getInstance()
							.getFollowListFromServer(
									UserManager.getInstance()
											.getCurrentUserId(),
									getActivity().getIntent().getIntExtra(
											FansGridActivity.FAN_ID, 0),
									FanManager.FOLLOWING, adapter.getCount());
				} else if (viewType == FansGridActivity.LOADFOLLOWINGSuggestions) {
					tempList = FanManager.getInstance()
							.getFansSuggestionsFromServer(adapter.getCount());
				}
			}
			return !tempList.isEmpty();
		}
	}

	@Override
	public void onSuccess(List<Fan> objs) {
		if (fansLoading.isShowing())
			fansLoading.dismiss();
		loadingBar.setVisibility(View.INVISIBLE);
		if (objs.isEmpty()
				&& getActivity().getIntent().getIntExtra(
						FansGridActivity.VIEWTYPE, 0) == FansGridActivity.LOADFOLLOWERS){
			listLayout.setVisibility(View.GONE);
			no_followers.setText(getActivity().getString(R.string.FilterResults_NoFans));
			noFollowersLay.setVisibility(View.VISIBLE);
		}else {
			listReceieved = true;
			if (recieved)
				((FansGridActivity) getActivity()).enableFilterButton();
			adapter = new ImageAdapter(objs);
			((GridView) listView).setAdapter(new updatePosts(getActivity(),
					adapter, R.layout.progress_bar));
		}
	}

	@Override
	public void onException(AppException ex) {
		if (fansLoading.isShowing())
			fansLoading.dismiss();
		// TODO Auto-generated method stub
		loadingBar.setVisibility(View.GONE);
		if (ex != null && ex.getMessage() != null
				&& ex.getErrorCode() != AppException.NO_DATA_EXCEPTION)
			UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@Override
	public void onPremiumListRecieved(final List<Fan> fans) {
		premiumContainer.removeAllViews();
		PremiumFans = fans;
		recieved = true;
		if (listReceieved)
			((FansGridActivity) getActivity()).enableFilterButton();
		horizontalScrollView.setVisibility(View.VISIBLE);
		LayoutInflater infalInflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View upgradeToPremium = infalInflater.inflate(
				R.layout.upgrade_to_premium, premiumContainer, false);
		upgradeToPremium.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(ShopActivityTabs.getIntent(getActivity(),
						ShopActivityTabs.PREMIUM_TAB));
			}
		});
		premiumContainer.addView(upgradeToPremium);
		for (int x = 0; x < PremiumFans.size(); x++) {
			final Fan fan = PremiumFans.get(x);
			final View premiumUsers = infalInflater.inflate(
					R.layout.premium_user_horizonta_view, premiumContainer,
					false);
			ImageView premiumFanProfilePic = (ImageView) premiumUsers
					.findViewById(R.id.premiumUserProfileImage);
			final ProgressBar premiumUserProfileImageLoader = (ProgressBar) premiumUsers
					.findViewById(R.id.premiumUserProfileImageLoader);
			if (x == PremiumFans.size() - 1) {
				ImageView leftseprator = (ImageView) premiumUsers
						.findViewById(R.id.leftSeparator);
				leftseprator.setVisibility(View.VISIBLE);
			}
			App.getInstance()
					.getImageLoader()
					.displayImage(PremiumFans.get(x).getFanPicture(),
							premiumFanProfilePic,
							App.getInstance().getDisplayOption(),
							new ImageLoadingListener() {
								@Override
								public void onLoadingStarted(String imageUri,
										View view) {
									premiumUserProfileImageLoader
											.setVisibility(View.VISIBLE);
								}

								@Override
								public void onLoadingFailed(String imageUri,
										View view, FailReason failReason) {
									premiumUserProfileImageLoader
											.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingComplete(String imageUri,
										View view, Bitmap loadedImage) {
									premiumUserProfileImageLoader
											.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingCancelled(String imageUri,
										View view) {

								}

							});
			premiumContainer.addView(premiumUsers);
			premiumUsers.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Fan fan = PremiumFans.get(((ViewGroup) premiumUsers
							.getParent()).indexOfChild(premiumUsers) - 1);
					if (fan.getFanID() == UserManager.getInstance()
							.getCurrentUserId())
						startActivity(UserProfileActivity
								.getActivityIntent(getActivity()));
					else
						startActivity(FanProfileActivity.getIntent(
								getActivity(), fan.getFanID(), fan.getFanName()));
					// showToast(Integer.toString(((ViewGroup) premiumUsers
					// .getParent()).indexOfChild(premiumUsers)));
				}
			});
		}
		View allpremiumUsers = infalInflater.inflate(R.layout.all_premium_user,
				premiumContainer, false);
		premiumContainer.addView(allpremiumUsers);
		allpremiumUsers.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				FanManager.getInstance().clearPremium();
				startActivity(new Intent(getActivity(),
						PremiumUsersActivity.class));
			}
		});
	}

}
