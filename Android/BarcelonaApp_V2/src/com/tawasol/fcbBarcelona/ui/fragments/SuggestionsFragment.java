package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.ContactEntity;
import com.tawasol.fcbBarcelona.entities.Fan;
import com.tawasol.fcbBarcelona.entities.FriendsEntity;
import com.tawasol.fcbBarcelona.entities.SuggestObject;
import com.tawasol.fcbBarcelona.entities.UserContacts;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnFanListRecieved;
import com.tawasol.fcbBarcelona.managers.FanManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.adapters.ContactFriendSuggestion;
import com.tawasol.fcbBarcelona.ui.adapters.ContactsChatAdapter;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class SuggestionsFragment extends Fragment implements OnFanListRecieved,
		OnRefreshListener {

	SwipeRefreshLayout swipeRefreshLayout;
	GridView suggestList;
	private ContactFriendSuggestion adapter;
	ProgressBar progress;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.suggestions_fragment,
				container, false);
//		initView(rootView);
		return rootView;
	}

//	private void initView(View rootView) {
//		progress = (ProgressBar) rootView.findViewById(R.id.progress);
//		suggestList = (GridView) rootView
//				.findViewById(R.id.friends_suggestions_grid_view);
//		swipeRefreshLayout = (SwipeRefreshLayout) rootView
//				.findViewById(R.id.swipe);
//		swipeRefreshLayout.setOnRefreshListener(this);
//		if (FanManager.getInstance().checkSuggestions()) {
//			if (!FanManager.getInstance().getContactsList().isEmpty()) {
//				progress.setVisibility(View.GONE);
//				if (adapter == null)
//					adapter = new ContactFriendSuggestion(getActivity(),
//							FanManager.getInstance().getContactsList());
//				suggestList.setAdapter(adapter);
//			} else
//				FanManager.getInstance().loadContacts();
//		} else {
//			List<UserContacts> fans = loadContacts();
//			FanManager.getInstance().sendContacts(
//					new SuggestObject(fans, UserManager.getInstance()
//							.getCurrentUserId()));
//		}
//
//	}

	@Override
	public void onResume() {
//		FanManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
//		UserManager.getInstance().removeListener(this);
		super.onPause();
	}

	@Override
	public void onSuccess(List<Fan> objs) {
		swipeRefreshLayout.setRefreshing(false);
		progress.setVisibility(View.GONE);
		adapter = new ContactFriendSuggestion(getActivity(), objs);
		suggestList.setAdapter(adapter);
	}

	@Override
	public void onException(AppException ex) {
		swipeRefreshLayout.setRefreshing(false);
		progress.setVisibility(View.GONE);
		if (ex.getType() != AppException.NO_DATA_EXCEPTION
				&& ex.getMessage() != null)
			UIUtils.showToast(getActivity(), ex.getMessage());
	}

	@SuppressLint("DefaultLocale")
	private List<UserContacts> loadContacts() {
		List<UserContacts> phoneList = new ArrayList<UserContacts>();
		// List<String> phones = new ArrayList<String>();
		// List<String> emails = new ArrayList<String>();
		String email = null;
		String phone = null;
		ContentResolver cr = getActivity().getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, null);
		Cursor cursor = null;
		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String id = cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts._ID));

				/** Get Contact Name **/
				String name = cur
						.getString(cur
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

				String hasPhone = cur
						.getString(cur
								.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
				if (hasPhone != null) {
					if (Integer.parseInt(hasPhone) > 0) {
						Cursor pCur = cr
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = ?", new String[] { id },
										null);
						cursor = getActivity().getContentResolver().query(
								Email.CONTENT_URI, null,
								Email.CONTACT_ID + "=?", new String[] { id },
								null);
						if (Email.DATA != null) {
							int emailIdx = cursor.getColumnIndex(Email.DATA);

							// let's just get the first email
							if (cursor.moveToFirst()) {
								email = cursor.getString(emailIdx);
							}
						}
						UserContacts con = null;
						if (pCur.moveToFirst()) {
							String phoneNo = pCur
									.getString(
											pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
									.replace(" ", "").trim();
							// phones.add(phoneNo);
							phone = phoneNo;
						}
						if (phone != null) {
							con = new UserContacts(-1, phone, email);
							phoneList.add(con);
							phone = null;
							email = null;
						} else {
							phone = null;
							email = null;
						}
						pCur.close();
					}
				}
			}
		}
		return phoneList;
	}

	@Override
	public void onRefresh() {
		List<UserContacts> fans = loadContacts();
		FanManager.getInstance().sendContacts(
				new SuggestObject(fans, UserManager.getInstance()
						.getCurrentUserId()));
	}

}
