package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.List;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.FriendTable;
import com.tawasol.fcbBarcelona.data.cache.NotificationsTable;
import com.tawasol.fcbBarcelona.entities.Fan;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnFanListRecieved;
import com.tawasol.fcbBarcelona.listeners.OnFriendResponseListener;
import com.tawasol.fcbBarcelona.listeners.OnSuccessVoidListener;
import com.tawasol.fcbBarcelona.managers.FanManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.FanProfileActivity;
import com.tawasol.fcbBarcelona.ui.activities.UserProfileActivity;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class FriendRequestFragment extends Fragment implements
		OnFanListRecieved, OnSuccessVoidListener, OnFriendResponseListener {

	ListView requestList;
	ProgressBar Requestprogress;
	private DisplayImageOptions options;
	private RequestAdapter adapter;
	ProgressDialog dialog;
	TextView noFriend;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.friend_request_fragment,
				container, false);
		dialog = new ProgressDialog(getActivity() , R.style.MyTheme);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		
		requestList = (ListView) rootView.findViewById(R.id.requestList);
		Requestprogress = (ProgressBar) rootView.findViewById(R.id.progress);
		noFriend = (TextView)rootView.findViewById(R.id.no_friend_requests);
		if (!FanManager.getInstance().getRequestList().isEmpty()) {
			Requestprogress.setVisibility(View.GONE);
			adapter = new RequestAdapter(FanManager.getInstance()
					.getRequestList());
			requestList.setAdapter(adapter);
		} else
			FanManager.getInstance().loadRequestList();
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.fan_no_image)
				.resetViewBeforeLoading(true)
				.showImageForEmptyUri(R.drawable.fan_no_image)
				.showImageOnFail(R.drawable.fan_no_image).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		return rootView;
	}

	@Override
	public void onResume() {
		FanManager.getInstance().addListener(this);
		super.onResume();
	}

	@Override
	public void onPause() {
		FanManager.getInstance().removeListener(this);
		super.onPause();
	}

	private class RequestAdapter extends BaseAdapter {
		List<Fan> fans;

		public RequestAdapter(List<Fan> fans) {
			this.fans = fans;
		}

		@Override
		public int getCount() {
			return this.fans.size();
		}

		@Override
		public Object getItem(int position) {
			return this.fans.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		public void deleteRow() {
			requestList.invalidateViews();
			if (adapter != null) adapter.notifyDataSetChanged();
			if (this.fans.isEmpty()) {
//				Requestprogress.setVisibility(View.VISIBLE);
				requestList.setVisibility(View.GONE);
			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final Holder holder;
			View rootView = convertView;
			final Fan fan = fans.get(position);
			if (rootView == null) {
				LayoutInflater inflator = LayoutInflater.from(App.getInstance()
						.getApplicationContext());
				rootView = inflator.inflate(R.layout.friend_request_item,
						parent, false);
				holder = new Holder();
				holder.fanState = (FrameLayout) rootView
						.findViewById(R.id.fan_profile_frame);
				holder.profileImageProgress = (ProgressBar) rootView
						.findViewById(R.id.progress);
				holder.fan_profile_img_id = (ImageView) rootView
						.findViewById(R.id.fan_profile_img_id);
				holder.favourate_button = (ImageButton) rootView
						.findViewById(R.id.favourate_button);
				holder.followed_fan = (ImageButton) rootView
						.findViewById(R.id.followed_fan);
				holder.fanName = (TextView) rootView
						.findViewById(R.id.fan_name);
				holder.accept = (ImageView) rootView
						.findViewById(R.id.acceptRequest);
				holder.reject = (ImageView) rootView
						.findViewById(R.id.reject_request);
				holder.num_of_pics = (TextView) rootView
						.findViewById(R.id.num_of_pics);
				rootView.setTag(holder);
			} else {
				holder = (Holder) rootView.getTag();
			}

			populateData(fan, holder, position);

			return rootView;
		}

		private void populateData(final Fan fan, Holder holder,
				final int position) {
			if (fan.isFavorite())
				holder.favourate_button.setImageResource(R.drawable.favourate);
			else
				holder.favourate_button
						.setImageResource(R.drawable.unfavourate);

			if (fan.isFanOnline() == 1)
				holder.fanState
						.setBackgroundResource(R.drawable.image_frameonline);
			else
				holder.fanState
						.setBackgroundResource(R.drawable.image_frameoffline);
			if (fan.isFollowed())
				holder.followed_fan.setImageResource(R.drawable.followed);
			else
				holder.followed_fan.setImageResource(R.drawable.unfollowed);// TODO
			// unfollow
			// image
			holder.followed_fan.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (fan.isFollowed()) {
						((ImageButton) getRowFromList(requestList, position)
								.findViewById(R.id.followed_fan))
								.setImageResource(R.drawable.unfollowed);
						FanManager.getInstance().handleFollow(
								UserManager.getInstance().getCurrentUserId(),
								fan.getFanID(), false, getActivity());
					} else {
						((ImageButton) getRowFromList(requestList, position)
								.findViewById(R.id.followed_fan))
								.setImageResource(R.drawable.followed);
						FanManager.getInstance().handleFollow(
								UserManager.getInstance().getCurrentUserId(),
								fan.getFanID(), true, getActivity());
					}
				}

			});
			holder.fanName.setText(fan.getFanName());
			holder.num_of_pics.setText(String.valueOf(fan.getFanPicNumber()));

			holder.accept.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.show();
					FanManager.getInstance().handleFriend(
							UserManager.getInstance().getCurrentUserId(),
							fan.getFanID(), true);
				}
			});

			

			holder.reject.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.show();
					FanManager.getInstance().handleFriend(
							UserManager.getInstance().getCurrentUserId(),
							fan.getFanID(), false);
				}
			});			/** Commented by Morabea */
//			dialog.show();
//			FanManager.getInstance().handleFriend(
//					UserManager.getInstance().getCurrentUserId(),
//					fan.getFanID(), true);
		

			holder.favourate_button
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (fan.isFavorite()) {
								((ImageButton) getRowFromList(requestList,
										position).findViewById(
										R.id.favourate_button))
										.setImageResource(R.drawable.unfavourate);
								FanManager.getInstance().handleFavo(
										UserManager.getInstance()
												.getCurrentUserId(),
										fan.getFanID(), false);
							} else {
								((ImageButton) getRowFromList(requestList,
										position).findViewById(
										R.id.favourate_button))
										.setImageResource(R.drawable.favourate);
								FanManager.getInstance().handleFavo(
										UserManager.getInstance()
												.getCurrentUserId(),
										fan.getFanID(), true);
							}
						}
					});

			display(holder.fan_profile_img_id, fan.getFanPicture(),
					holder.profileImageProgress);
			holder.fan_profile_img_id
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (fan.getFanID() == UserManager.getInstance()
									.getCurrentUserId())
								startActivity(UserProfileActivity
										.getActivityIntent(getActivity()));
							else
								startActivity(FanProfileActivity.getIntent(
										getActivity(), fan.getFanID(),
										fan.getFanName()));
						}
					});
		}

		private View getRowFromList(ListView requestList, int position) {
			return requestList.getChildAt(position
					- requestList.getFirstVisiblePosition());
		}

		public void display(ImageView img, String url, final ProgressBar spinner) {
			App.getInstance()
					.getImageLoader()
					.displayImage(url, img, options,
							new ImageLoadingListener() {
								@Override
								public void onLoadingStarted(String imageUri,
										View view) {
									spinner.setVisibility(View.VISIBLE);
								}

								@Override
								public void onLoadingFailed(String imageUri,
										View view, FailReason failReason) {
									spinner.setVisibility(View.GONE);

								}

								@Override
								public void onLoadingComplete(String imageUri,
										View view, Bitmap loadedImage) {
									spinner.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingCancelled(String imageUri,
										View view) {

								}

							});
		}

		private class Holder {
			FrameLayout fanState;
			ProgressBar profileImageProgress;
			ImageView fan_profile_img_id;
			ImageButton favourate_button;
			ImageButton followed_fan;
			TextView num_of_pics;
			TextView fanName;
			ImageView accept;
			ImageView reject;
		}

	}

	@Override
	public void onSuccess(List<Fan> objs) {
		Requestprogress.setVisibility(View.GONE);
		adapter = new RequestAdapter(objs);
		requestList.setAdapter(adapter);
	}

	@Override
	public void onException(AppException ex) {
		Requestprogress.setVisibility(View.GONE);
		if (ex.getType() != AppException.NO_DATA_EXCEPTION && ex.getMessage() != null)
			UIUtils.showToast(getActivity(), ex.getMessage());
		else{
			noFriend.setVisibility(View.VISIBLE);
			requestList.setVisibility(View.GONE);
		}
			
	}

	@Override
	public void onSuccess() {
		Requestprogress.setVisibility(View.GONE);
		if (dialog.isShowing())
			dialog.dismiss();
		adapter.deleteRow();
		FriendTable.getInstance().deleteAll();
		NotificationsTable.getInstance().deleteAll();
	}

	@Override
	public void onSuccess(int status , boolean friend) {
		Requestprogress.setVisibility(View.GONE);
		if (dialog.isShowing())
			dialog.dismiss();
		adapter.deleteRow();
		FriendTable.getInstance().deleteAll();
	}

}
