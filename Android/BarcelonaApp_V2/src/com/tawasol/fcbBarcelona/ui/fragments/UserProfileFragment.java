package com.tawasol.fcbBarcelona.ui.fragments;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.AboutMeActivity;
import com.tawasol.fcbBarcelona.ui.activities.Edit_UserProfile_Activity;
import com.tawasol.fcbBarcelona.ui.activities.FansGridActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivityTabs;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.LocationUtil;
import com.tawasol.fcbBarcelona.utils.ValidatorUtils;

/**
 * @author Mohga
 * */
public class UserProfileFragment extends BaseFragment implements
		OnClickListener/* , OnUpdateProfileListener */{
	private static final int Following = 1;
	private static final int Followers = 2;
	private static final String PROFILE_INITIAL_STATE = "ProfileInitialState";
	User user;
	Button moreBtn;
	ImageButton editImg/* ,helpBtn */;
	ImageView userImage;
	TextView userNameTxt, progressPercentageTxt, countryTxt, cityTxt,
			districtTxt, mobileTxt, emailTxt, birthDateTxt, genderTxt;
	// TextView followingFirstDigit, followingSecondDigit, followingThirdDigit,
	// followingFourthDigit;
	// TextView followersFirstDigit, followersSecondDigit, followersThirdDigit,
	// followersFourthDigit;
	TextView upgradeTxt, userProfilePointsTxt;
	RelativeLayout followingRelativeLayout, followersRelativeLayout;
	LinearLayout percentageLinearLayout;
	ImageView upperArrowImg;
	ProgressBar profilePicPB, percentagePB;
	TextView completeProfileTxt;
	String userCredit = "-1";

	LocationUtil locUtil;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_user_profile,
				container, false);
		// initialize location util
		locUtil = new LocationUtil(getActivity());

		/* find the controls of the XML */
		getViewById(rootView);

		/* set the initial drawable of masked image view */
		// userImage.setImageDrawable(getResources().getDrawable(
		// R.drawable.image_sample));

		/* get the user object info */
		initUser();

		return rootView;
	}

	/** get the Controls from XML file */
	private void getViewById(View view) {
		profilePicPB = (ProgressBar) view
				.findViewById(R.id.profile_progressBar);
		userImage = (ImageView) view.findViewById(R.id.user_image_id);
		editImg = (ImageButton) view
				.findViewById(R.id.user_profile_edit_imgbtn);
		userNameTxt = (TextView) view.findViewById(R.id.user_profile_name_txt);
		progressPercentageTxt = (TextView) view
				.findViewById(R.id.user_profile_progress_percentage_txt);
		countryTxt = (TextView) view
				.findViewById(R.id.user_profile_country_txt);
		cityTxt = (TextView) view.findViewById(R.id.user_profile_city_txt);
		districtTxt = (TextView) view
				.findViewById(R.id.user_profile_district_txt);
		mobileTxt = (TextView) view.findViewById(R.id.user_profile_mobile_txt);
		birthDateTxt = (TextView) view
				.findViewById(R.id.user_profile_birth_date_txt);
		emailTxt = (TextView) view.findViewById(R.id.user_profile_email_txt);
		genderTxt = (TextView) view.findViewById(R.id.user_profile_gender_txt);
		moreBtn = (Button) view.findViewById(R.id.more_btn);

		upgradeTxt = (TextView) view
				.findViewById(R.id.user_profile_upgrate_txt);
		userProfilePointsTxt = (TextView) view
				.findViewById(R.id.user_profile_points_balance_txt);

		followingRelativeLayout = (RelativeLayout) view
				.findViewById(R.id.profile_following_rel_layout);
		followersRelativeLayout = (RelativeLayout) view
				.findViewById(R.id.profile_followers_rel_layout);
		percentageLinearLayout = (LinearLayout) view
				.findViewById(R.id.percentageBar_LinearLayout);
		upperArrowImg = (ImageView) view
				.findViewById(R.id.profile_upper_arrow_img);
		percentagePB = (ProgressBar) view
				.findViewById(R.id.user_profile_progressbar);
		completeProfileTxt = (TextView) view
				.findViewById(R.id.complete_profile_txt);
		// helpBtn = (ImageButton) getActivity()
		// .findViewById(R.id.helpBtn);

	}

	/**
	 * set the numbers individually in the views according to the type
	 * "Following or Followers"
	 * 
	 * @param Id
	 *            that indicates the type
	 * @param Number
	 *            that will be set according to the type in the right controls !
	 * */
	private void SetNumbers(int Id, int Number) {

		if (Id == Following) {
			((LinearLayout)followingRelativeLayout.findViewById(R.id.profile_following_Layout)).removeAllViews();
			for (char num : (Number + "").toCharArray()) {

				TextView followingTxt = new TextView(getActivity());
				followingTxt.setText(num + "");
				followingTxt.setBackgroundResource(R.drawable.follow_counter);
				followingTxt.setGravity(Gravity.CENTER);
				// following.setTextSize(TypedValue.COMPLEX_UNIT_SP,
				// getResources().getDimension(R.dimen.smallTextSize));

				((LinearLayout) followingRelativeLayout
						.findViewById(R.id.profile_following_Layout))
						.addView(followingTxt);

			}

		} else if (Id == Followers) {
			((LinearLayout) followersRelativeLayout
					.findViewById(R.id.profile_followers_Layout)).removeAllViews();
			for (char num : (Number + "").toCharArray()) {

				TextView followersTxt = new TextView(getActivity());
				followersTxt.setText(num + "");
				followersTxt.setBackgroundResource(R.drawable.follow_counter);
				followersTxt.setGravity(Gravity.CENTER);

				((LinearLayout) followersRelativeLayout
						.findViewById(R.id.profile_followers_Layout))
						.addView(followersTxt);

			}

		}

	}

	/** Initialize the controls of the view */
	private void initViewControls() {

		if(user.isPremium())
			upgradeTxt.setVisibility(View.GONE);
		// Loading Font Face
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/points_font.ttf");
		// Applying font
		userProfilePointsTxt.setTypeface(tf);
		userProfilePointsTxt.setText(user.getCredit() + " XP ");

		// set the progress bar
		setProgress();

		// set the user name
		userNameTxt.setText(user.getFullName());

		// set the country text
		countryTxt
				.setText(user.getCountryName().equals("") ? getString(R.string.profile_country_initial)
						: user.getCountryName());

		// set the city text
		if (user.getCityName() != null)
			cityTxt.setText(user.getCityName().equals("") ? getString(R.string.profile_city_initial)
					: user.getCityName());
		else
			cityTxt.setText(getString(R.string.profile_city_initial));

		// set the district text
		districtTxt
				.setText(user.getDistrict().equals("") ? getString(R.string.profile_district_initial)
						: user.getDistrict());

		// set the phone text
		mobileTxt
				.setText(user.getPhone().equals("") ? getString(R.string.profile_phone_initial)
						: user.getPhone());
		// set the email text
		emailTxt.setText(user.getEmail());

		// set the birthdate text
		birthDateTxt
				.setText(user.getDateOfBirth() == 0 ? getString(R.string.profile_birthdate_initial)
						: getBirthDate(user.getDateOfBirth() * (long) 1000));

		// set the following and followers number
		SetNumbers(Following, user.getFollowingCount() /*
														 * String.format("%04d",
														 * user
														 * .getFollowingCount())
														 */);
		SetNumbers(Followers, user.getFollowersCount() /*
														 * String.format("%04d",user
														 * .getFollowersCount())
														 */);

		// set the gender text
		if (user.getGender() != 0) {
			if (user.getGender() == User.GENDER_TYPE_MALE)
				genderTxt.setText(getString(R.string.FansFilter_Male));
			else if (user.getGender() == User.GENDER_TYPE_FEMALE)
				genderTxt.setText(getString(R.string.FansFilter_Female));
		} else
			genderTxt.setText(getString(R.string.MyProfile_Gender));

		// set the user image text
		// App.getInstance().getImageLoader().displayImage(user.getProfilePic(),
		// userImage);
		DisplayImage();
		/*
		 * if (NetworkingUtils.isNetworkConnected(getActivity())) {
		 * DownloadImagesTask task = new DownloadImagesTask(); task.execute(); }
		 * else UIUtils.showToast(getActivity(),
		 * getResources().getString(R.string.connection_noConnection));
		 */

		// set upgrade drawable
		upgradeTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.premium,
				0, 0, 0);

		// set the click listeners
		followingRelativeLayout.setOnClickListener(this);
		followersRelativeLayout.setOnClickListener(this);
		editImg.setOnClickListener(this);
		moreBtn.setOnClickListener(this);
		percentageLinearLayout.setOnClickListener(this);
		upgradeTxt.setOnClickListener(this);

		// set the complete profile points text
		completeProfileTxt.setText(getResources().getString(
				R.string.complete_profile)
		/*
		 * + " " + String.valueOf(user.getRegestrationLeveOnePoints() +
		 * user.getRegestrationLeveTwoPoints() +
		 * user.getRegestrationLeveThreePoints()) +" " +
		 * getResources().getString(R.string.studio_xp)
		 */);

		if (IsInitialState())
			countryTxt.setOnClickListener(this);
		// helpBtn.setOnClickListener(this);

		if (calculateUserPoints() == 100)
			completeProfileTxt.setVisibility(View.INVISIBLE);
		else
			completeProfileTxt.setVisibility(View.VISIBLE);

	}

	@SuppressLint("NewApi")
	private void SetLocation() {
		// int creditValue = 0;
		// String CityName = "";
		if (locUtil.canGetLocation()) {
			Location currentLocation = locUtil.getLocation();
			if (currentLocation != null) {
				Geocoder geoCoder = new Geocoder(getActivity(), new Locale(
						LanguageUtils.getInstance().getDeviceLanguage()));
				try {
					List<Address> addresses = geoCoder.getFromLocation(
							currentLocation.getLatitude(),
							currentLocation.getLongitude(), 1);

					if (addresses.size() > 0) {
						// set the country text
						if (addresses.get(0).getCountryName() != null
								&& !addresses.get(0).getCountryName().isEmpty()) {
							countryTxt.setText(addresses.get(0)
									.getCountryName());
							// creditValue += 100;
							user.setCountryName(addresses.get(0)
									.getCountryName());
							user.setCountryCode(addresses.get(0)
									.getCountryCode());
						}
						// System.out.println(addresses.get(0).getCountryName());
						// if(addresses.size() <= 1){
						// CityName= addresses.get(0).getAddressLine(0) ;
						// if(CityName != null && !CityName.isEmpty())
						// {
						// cityTxt.setText(CityName);
						// creditValue+=100;
						// user.setCityName(CityName);
						// }
						// System.out.println(CityName);
						// }

						if ((countryTxt.getText() != null && !countryTxt
								.getText().toString().isEmpty())
								|| (cityTxt.getText().toString() != null && !cityTxt
										.getText().toString().isEmpty())) {
							// UserManager.getInstance().udateUserCredit(creditValue);
							// userProfilePointsTxt.setText(user.getCredit()+creditValue
							// + " XP ");
							UserManager.getInstance().udateProfile(user, false,
									false);
						}

					}

				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * Initialize the user variable using the UserManger to get the current
	 * logged in user
	 */
	private void initUser() {
		this.user = UserManager.getInstance().getCurrentUser();
	}

	/**
	 * get the Birthdate string according to specific format to be like
	 * "01 Jul 1991"
	 */
	private String getBirthDate(long time) {
		Date date = new Date(time);
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy",
				Locale.getDefault());
		format.setTimeZone(TimeZone.getDefault());
		return format.format(date);
	}

	/** a method that checks whether it is initial state of that screen or not */
	public boolean IsInitialState() {
		return !(new File(getActivity().getFilesDir() + "/"
				+ PROFILE_INITIAL_STATE).exists());
	}

	private void setProgress() {

		int profilePercentage = calculateUserPoints();
		progressPercentageTxt.setText(profilePercentage + " %");
		percentagePB.setProgress(profilePercentage);

	}

	private int calculateUserPoints() {
		double accuratePercentage = (UserManager.getInstance()
				.calculateProfilePercentage() / 17.0) * 100;

		return (int) (Math.round(accuratePercentage));

	}

	/**
	 * if initial state then need to handle the view hide the percentage bar
	 * indicator hide the credit points text show the edit arrow
	 * */
	private void handleProfileInitialState() {
		if (calculateUserPoints() < 100) {
			userProfilePointsTxt.setVisibility(View.GONE);
			percentageLinearLayout.setVisibility(View.GONE);
			upperArrowImg.setVisibility(View.VISIBLE);
		}

	}

	/** method that flags that this screen is not in intial state any more */
	private void removeInitialStateIndicator() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(getActivity(),
							PROFILE_INITIAL_STATE, "");
				} catch (Exception e) {
				}
			}
		});
	}

	/** Download the bitmap of the image URL */
	public class DownloadImagesTask extends AsyncTask<Void, Void, Void> {
		Bitmap bmp = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			profilePicPB.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(Void result) {
			profilePicPB.setVisibility(View.GONE);
			if (bmp != null) {
				userImage.setImageBitmap(bmp);
			} else
				DisplayImage();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				URL ulrn = new URL(user.getProfilePic());
				HttpURLConnection con = (HttpURLConnection) ulrn
						.openConnection();
				InputStream is = con.getInputStream();
				bmp = BitmapFactory.decodeStream(is);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

	}

	private void DisplayImage() {
		DisplayImageOptions displayOption = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true)
				.showImageOnFail(R.drawable.ic_launcher)
				.cacheInMemory(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.considerExifParams(true)
				.displayer(
						new RoundedBitmapDisplayer((int) getResources()
								.getDimension(R.dimen.button_padding))).build();

		App.getInstance()
				.getImageLoader()
				.displayImage(user.getProfilePic(), userImage, displayOption,
						new ImageLoadingListener() {

							@Override
							public void onLoadingStarted(String arg0, View arg1) {
								profilePicPB.setVisibility(View.VISIBLE);
							}

							@Override
							public void onLoadingFailed(String arg0, View arg1,
									FailReason arg2) {
								profilePicPB.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingComplete(String arg0,
									View arg1, Bitmap arg2) {
								profilePicPB.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingCancelled(String arg0,
									View arg1) {

							}
						});

	}

	@Override
	public void onResume() {
		super.onResume();
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			// UserManager.getInstance().addListener(this);

			/* get the user object info */
			initUser();

			/* set the info in controls */
			initViewControls();

			// check if initial state then adjust the view
			if (IsInitialState()) {
				handleProfileInitialState();
				if (this.user != null
						&& ValidatorUtils
								.isRequired(this.user.getCountryName()))
					SetLocation();
			}
		} else
			getActivity().finish();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (IsInitialState())
			removeInitialStateIndicator();
		// UserManager.getInstance().removeListener(this);
	}

	/** handle the on click listener */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.profile_following_rel_layout:
			if (UserManager.getInstance().getCurrentUser().getFollowingCount() != 0)
				startActivity(FansGridActivity.getIntent(getActivity(),
						FansGridActivity.LOADFOLLOWING, UserManager
								.getInstance().getCurrentUser().getUserId()));
			else
				startActivity(FansGridActivity.getIntent(getActivity(),
						FansGridActivity.LOADFOLLOWINGSuggestions, UserManager
								.getInstance().getCurrentUser().getUserId()));
			break;
		case R.id.profile_followers_rel_layout:
			// if
			// (UserManager.getInstance().getCurrentUser().getFollowersCount()
			// != 0)
			startActivity(FansGridActivity.getIntent(getActivity(),
					FansGridActivity.LOADFOLLOWERS, UserManager.getInstance()
							.getCurrentUser().getUserId()));
			// else
			// UIUtils.showToast(getActivity(),
			// getResources().getString(R.string.FollowersInitalState_no_followers));
			break;
		case R.id.user_profile_edit_imgbtn:
			startActivity(Edit_UserProfile_Activity
					.getActivityIntent(getActivity() , false));
			break;

		case R.id.user_profile_country_txt:
			if (IsInitialState()) {
				startActivity(Edit_UserProfile_Activity
						.getActivityIntent(getActivity() , false));
				removeInitialStateIndicator();
			}
			break;
		case R.id.more_btn:
			startActivity(AboutMeActivity.getActivityIntent(getActivity()));
			break;
		case R.id.percentageBar_LinearLayout:
			startActivity(Edit_UserProfile_Activity
					.getActivityIntent(getActivity() , false));
			break;
		case R.id.user_profile_upgrate_txt:
			startActivity(ShopActivityTabs.getIntent(getActivity(),
					ShopActivityTabs.PREMIUM_TAB));
			break;
		/*
		 * case R.id.helpBtn:
		 * startActivity(HelpScreenActivity.getActivityIntent(
		 * getActivity(),false)); break;
		 */
		default:
			break;
		}
	}

	/*
	 * @Override public void onSuccess(String obj) { this.userCredit = obj;
	 * //set the points balance text if(this.userCredit != null && !
	 * this.userCredit.equals("-1"))
	 * userProfilePointsTxt.setText(this.userCredit + " XP "); else
	 * userProfilePointsTxt.setText(user.getCredit() + " XP "); }
	 */
	/*
	 * @Override public void onException(AppException ex) { // do nothing for
	 * now :) }
	 */

	/*
	 * @Override public void onSuccess() { UIUtils.showToast(getActivity(),
	 * "Credit updated"); // call update user
	 * user.setCredit(Integer.valueOf(userCredit));
	 * UserManager.getInstance().udateProfile(user,false,false);
	 * 
	 * }
	 */
	/*
	 * @Override public void onSuccess(User obj) { //
	 * UIUtils.showToast(getActivity(),"Profile Updated" );
	 * userProfilePointsTxt.setText(obj.getCredit() + " XP "); }
	 */
}
