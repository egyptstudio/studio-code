package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle; 
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SearchView; 
import android.widget.FrameLayout.LayoutParams;

import com.tawasol.fcbBarcelona.R; 
import com.tawasol.fcbBarcelona.entities.Message;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnChatUpdatedListener;
import com.tawasol.fcbBarcelona.listeners.OnVerifyPhoneListener;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.VerifyPhoneActivity;
import com.tawasol.fcbBarcelona.ui.adapters.RecentChatAdapter;
import com.tawasol.fcbBarcelona.utils.UIUtils;

/**
 * 
 * @author Turki
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class RecentFragment extends BaseFragment implements OnChatUpdatedListener, OnVerifyPhoneListener,
						SearchView.OnQueryTextListener, SearchView.OnCloseListener{
	
	private ListView mList;
	private RecentChatAdapter adapter;
	private List<Message> latestMsgs = new ArrayList<Message>();
	private View verificationView;
	private Button verifyButton;
	private Button help;
	private FrameLayout noRecentMsgFrame;
	private SearchView mSearchView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView      = inflater.inflate(R.layout.fragment_recent, container, false);
		
		/** Initialize registration view **/
		init(rootView);
		
		return rootView;
	}

	private void init(View rootView){
		/** For phone verification **/
		verificationView = rootView.findViewById(R.id.pop_up_phone_verify);
		verifyButton     = (Button) rootView.findViewById(R.id.verifytypebutton);
		verificationView.setVisibility(View.GONE);
		help             = (Button) rootView.findViewById(R.id.helpButton);
		noRecentMsgFrame = (FrameLayout) rootView.findViewById(R.id.initial_no_recent_msg);
		if(!ChatManager.getInstance().isRecentMsgLoadedBefor())
			noRecentMsgFrame.setVisibility(View.VISIBLE);
		handleVerficationView(verificationView);
		
		mSearchView      = (SearchView) getActivity().findViewById(R.id.chat_search_friends);
		mSearchView.setOnSearchClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((BaseActivity) getActivity()).hideTitleBar();
				((BaseActivity) getActivity()).hideChatInviteFriends();
			}
		});
		((BaseActivity) getActivity()).getSearchBtn().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				mSearchView.setIconified(false);
				((BaseActivity) getActivity()).hideChatSearchBtn();
			}
		});
		
		mList = (ListView) rootView.findViewById(R.id.chat_recent_list);
		mList.setPadding(0, 0, 0, BaseActivity.bottomBarHeight);
		adapter = new RecentChatAdapter(getActivity(), latestMsgs);
		mList.setAdapter(adapter);
		mList.setTextFilterEnabled(true);
	    setupSearchView();
	}
	
	private void setupSearchView() {
	   mSearchView.setOnQueryTextListener(this);
	   mSearchView.setOnCloseListener(this);
	   mSearchView.setSaveEnabled(true);
	   mSearchView.setQueryHint(" ");
	 }
	 
	@Override
	public void onSuccess() {
		hideLoadingDialog();
		List<Message> list = ChatManager.getInstance().getLatestMessages();
		if(list.size()>0)
			noRecentMsgFrame.setVisibility(View.GONE);
		if (mList.getAdapter() == null){
			adapter = new RecentChatAdapter(getActivity(), list);
			mList.setAdapter(adapter);
		}else {
			latestMsgs.clear();
			latestMsgs.addAll(list);
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
	}
	
	/** Add LatestMessages listeners onResume **/
	@Override
	public void onResume() {
		showLoadingDialog();
		ChatManager.getInstance().addListener(this);
		ChatManager.getInstance().loadRecentMessagesAsync();
		super.onResume();
	}

	/** Remove LatestMessages listeners onPause **/
	@Override
	public void onPause() {
		ChatManager.getInstance().removeListener(this);
		super.onPause();
	}
	
	private void handleVerficationView(View view){
		if(!UserManager.getInstance().getCurrentUser().isPhoneVerified())
			view.setVisibility(View.GONE);
		else
			view.setVisibility(View.GONE);
		verifyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UserManager.getInstance().callVerifyPhone();
			}
		});
		help.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showHelpDialog(v);
			}
		});
	}
	
	/**  Initialize of help PopUp **/
	private void showHelpDialog(View view) {
		final View popupView = LayoutInflater.from(getActivity()).inflate(R.layout.chat_help_dialog, null);
		final PopupWindow popupWindow = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.setOutsideTouchable(true);
		popupWindow.setFocusable(true);		 
		popupWindow.showAsDropDown(view, 0, 0/*, Gravity.CENTER_HORIZONTAL*/);
	}

	@Override
	public boolean onClose() {
		((BaseActivity) getActivity()).showTitleBar();
		((BaseActivity) getActivity()).showChatInviteFriends();
		((BaseActivity) getActivity()).showChatSearchBtn();
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		adapter.getFilter().filter(newText);
        return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onSuccess(User userPhoneVerified) {
		getActivity().startActivity(VerifyPhoneActivity.getActivityIntent(getActivity()));
	}
}
