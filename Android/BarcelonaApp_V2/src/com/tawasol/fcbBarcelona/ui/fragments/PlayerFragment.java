package com.tawasol.fcbBarcelona.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.entities.StudioFolder;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnFoldersReceivedListener;
import com.tawasol.fcbBarcelona.managers.StudioManager;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.StudioPhotosActivity;
import com.tawasol.fcbBarcelona.ui.adapters.PlayerAdapter;
import com.tawasol.fcbBarcelona.utils.UIUtils;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class PlayerFragment extends BaseFragment implements OnFoldersReceivedListener,OnItemClickListener {
	
	GridView playerGV ;
	PlayerAdapter playerAdapter;
	List<StudioFolder> folders;
	
	private static PlayerFragment playerFragment;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_player, container,
				false);
		initView(rootView);
		
		return rootView;
	}
	private void initView(View rootView)
	{
		showLoadingDialog();
		playerGV = (GridView)rootView.findViewById(R.id.player_gridview);
		//playerGV.setPadding(0, 0, 0,((BaseActivity)getActivity()).getBottomBarHeight());
		
		// initialize the list
		folders = new ArrayList<StudioFolder>();
		
		// initialize the adapter  
		playerAdapter = new PlayerAdapter(getActivity(), (ArrayList<StudioFolder>)folders);
	
		playerGV.setAdapter(playerAdapter);
		
		// set the listener
		playerGV.setOnItemClickListener(this);
		
		
		
	}
	
	public static PlayerFragment newInstance() {
		if (playerFragment == null)
			playerFragment = new PlayerFragment();
		return playerFragment;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		StudioManager.getInstance().addListener(this);
		StudioManager.getInstance().getStudioFolders(StudioFolder.FOLDER_TYPE_PLAYER);
	}
	
	@Override
	public void onPause() {
		
		super.onPause();
		StudioManager.getInstance().removeListener(this);
	}
	@Override
	public void onSuccess(List<StudioFolder> objs) {
		hideLoadingDialog();
		
		folders.clear();
		folders.addAll(objs);
		playerAdapter.notifyDataSetChanged();
		
	}
	@Override
	public void onException(AppException ex) {
		hideLoadingDialog();
		UIUtils.showToast(getActivity(), ex.getMessage());
		
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		startActivity(StudioPhotosActivity.getActivityIntent(getActivity(),folders.get(position) , true));
		
	}

}
