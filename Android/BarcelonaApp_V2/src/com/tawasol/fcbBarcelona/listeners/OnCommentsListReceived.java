/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

import java.util.List;

import com.tawasol.fcbBarcelona.entities.UserComment;

/**
 * @author Areeg
 *
 */
public interface OnCommentsListReceived extends OnEntityListReceivedListener<UserComment>{

	<T> void onEntitiesListReceived(List<T> list, int pageNo);
	
}
