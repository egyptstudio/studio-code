/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.PostLike;

/**
 * @author Basyouni
 *
 */
public interface OnLikeRecieved extends OnEntityListReceivedListener<PostLike>{

}
