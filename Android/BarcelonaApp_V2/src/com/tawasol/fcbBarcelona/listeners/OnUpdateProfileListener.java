/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.User;


/**
 * @author Mohga
 *
 */
public interface OnUpdateProfileListener extends
		OnEntityReceivedListener<User> {

}
