/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.UserComment;

/**
 * @author Basyouni
 *
 */
public interface OnCommentAdded extends UiListener{
//	void getCount(int count);
	void commentAdded(UserComment comment);
}
