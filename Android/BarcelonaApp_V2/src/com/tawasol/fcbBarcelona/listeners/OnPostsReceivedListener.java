package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.PostViewModel;

public interface OnPostsReceivedListener extends OnEntityListReceivedListener<PostViewModel> {

}
