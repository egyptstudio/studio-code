package com.tawasol.fcbBarcelona.listeners;

public interface OnFriendResponseListener extends UiListener{
	void onSuccess(int status , boolean friend);
}
