package com.tawasol.fcbBarcelona.listeners;

import java.util.List;

import com.tawasol.fcbBarcelona.entities.Fan;

public interface OnPremiumBarRecieved extends UiListener {
	void onPremiumListRecieved(List<Fan> fans);
}
