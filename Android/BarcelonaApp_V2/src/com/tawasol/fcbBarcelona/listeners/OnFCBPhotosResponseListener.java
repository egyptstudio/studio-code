package com.tawasol.fcbBarcelona.listeners;


import com.tawasol.fcbBarcelona.entities.FCBPhoto;


/**
 * @author Turki
 * 
 */
public interface OnFCBPhotosResponseListener extends OnEntityListReceivedListener<FCBPhoto>{

}
