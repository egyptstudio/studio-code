package com.tawasol.fcbBarcelona.listeners;

import java.util.List;

public interface UpdateDialogListener extends UiListener {
	void countryList(List<Integer> countries);
}
