package com.tawasol.fcbBarcelona.listeners;

public interface OnFollowSuccess extends UiListener{

	void onFollowSuccess(int userId);
}
