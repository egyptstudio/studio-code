package com.tawasol.fcbBarcelona.listeners;

/**
 * 
 * @author Turki
 *
 */
public interface OnBackgroundTask {
	
	void onTaskCompleted();
	
	void doTaskInBackground();

}
