package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.User;

/**
 * @author Turki
 */
public interface OnVerifyPhoneListener extends OnEntityReceivedListener<User> {	
}
