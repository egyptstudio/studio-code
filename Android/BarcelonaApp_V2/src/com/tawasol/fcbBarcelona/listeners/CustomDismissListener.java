/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

/**
 * @author Basyouni
 *
 */
public interface CustomDismissListener {

	void onDismiss(int commentsCount);
	
}
