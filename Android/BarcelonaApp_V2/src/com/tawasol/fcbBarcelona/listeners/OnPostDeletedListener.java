package com.tawasol.fcbBarcelona.listeners;

public interface OnPostDeletedListener extends UiListener {

	void onPostDeleted(int postId);
}
