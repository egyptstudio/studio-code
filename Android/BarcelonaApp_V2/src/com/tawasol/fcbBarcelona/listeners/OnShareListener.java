/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.PostViewModel;


/**
 * @author Mohga
 *
 */
public interface OnShareListener extends
		OnEntityReceivedListener<PostViewModel> {

}
