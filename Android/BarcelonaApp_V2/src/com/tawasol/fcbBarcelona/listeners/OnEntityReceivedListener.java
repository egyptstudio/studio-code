package com.tawasol.fcbBarcelona.listeners;


public interface OnEntityReceivedListener<T> extends UiListener {

	void onSuccess(T obj);
	
}
