package com.tawasol.fcbBarcelona.listeners;

/**
 * Listener to provide callbacks with the result of any server method that return void.
 * @author Basyouni
 *
 */
public interface OnSuccessVoidListener extends UiListener{

	/**
	 * Process is successful 
	 */
	void onSuccess();
}
