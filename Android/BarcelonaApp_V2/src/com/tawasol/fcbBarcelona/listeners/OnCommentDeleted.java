package com.tawasol.fcbBarcelona.listeners;

public interface OnCommentDeleted extends UiListener{

	void onCommentDeleted(int postId , int commentId);
}
