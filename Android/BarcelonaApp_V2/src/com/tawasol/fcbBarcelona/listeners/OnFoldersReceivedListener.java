package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.StudioFolder;

public interface OnFoldersReceivedListener
extends OnEntityListReceivedListener<StudioFolder> {

}
