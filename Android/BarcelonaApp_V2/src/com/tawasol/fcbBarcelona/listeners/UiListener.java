package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.exception.AppException;

public interface UiListener {

	void onException(AppException ex);
}
