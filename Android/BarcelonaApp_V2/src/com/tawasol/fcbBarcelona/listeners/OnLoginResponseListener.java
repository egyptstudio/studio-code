package com.tawasol.fcbBarcelona.listeners;

import twitter4j.TwitterException;
import android.content.Intent;

import com.facebook.FacebookException;
import com.tawasol.fcbBarcelona.entities.User;

/**
 * Listener to provide callbacks with the result of the login process.
 * @author Mohga
 *
 */
public interface OnLoginResponseListener extends UiListener {

	/**
	 * Login process is successful and the logged in user object is passed.
	 * @param user
	 */
	void onLoginSuccess(User user,int LoginState);
	
	/**
	 * Login with Social Media Request returned {status : 1} , so should navigate to Register
	 * @param user
	 */
	void onLoginWithSocialMediaNavigation(User user);
	
	
}
