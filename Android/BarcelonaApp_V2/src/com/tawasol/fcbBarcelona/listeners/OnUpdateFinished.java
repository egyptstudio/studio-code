/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

/**
 * @author Basyouni
 *
 */
public interface OnUpdateFinished extends UiListener{
	void onUpdateFinished();
}
