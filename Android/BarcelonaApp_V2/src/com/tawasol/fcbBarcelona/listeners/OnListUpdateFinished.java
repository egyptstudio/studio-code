package com.tawasol.fcbBarcelona.listeners;

import java.util.List;

import com.tawasol.fcbBarcelona.entities.PostViewModel;

public interface OnListUpdateFinished extends UiListener{

	void onUpdatedListRecieved(List<PostViewModel> posts);
}
