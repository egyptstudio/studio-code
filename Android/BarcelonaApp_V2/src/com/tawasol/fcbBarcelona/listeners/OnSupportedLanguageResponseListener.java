package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.LanguageEntity;

/**
 * @author Turki
 * 
 */
public interface OnSupportedLanguageResponseListener extends OnEntityListReceivedListener<LanguageEntity>{

}
