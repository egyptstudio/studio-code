package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.StudioPhoto;

public interface OnStudioPhotosReceivedListener
extends OnEntityListReceivedListener<StudioPhoto> {

}
