package com.tawasol.fcbBarcelona.listeners;

import java.util.List;

import com.tawasol.fcbBarcelona.entities.Fan;

public interface OnSuggestionListRecieved extends UiListener{

	void onSuccess(List<Fan> fans);
}
