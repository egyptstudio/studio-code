package com.tawasol.fcbBarcelona.listeners;

public interface OnMyPointsReceived extends UiListener{

	public void onPointsRecieved(int points);
}
