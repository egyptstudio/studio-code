package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.SystemMessagesEntity;

/**
 * @author Turki
 * 
 */
public interface OnSystemMessagesResponseListener extends OnEntityListReceivedListener<SystemMessagesEntity>{

}
