/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.ContestData;

/**
 * @author Mohga
 *
 */
public interface OnContestReceived extends OnEntityReceivedListener<ContestData>{
}
