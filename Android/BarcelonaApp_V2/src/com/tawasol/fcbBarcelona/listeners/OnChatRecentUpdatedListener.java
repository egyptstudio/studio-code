package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.Message;


/**
 * @author Turki
 * 
 */
public interface OnChatRecentUpdatedListener extends OnEntityListReceivedListener<Message> {

}
