package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.TagsData;


public interface OnTagsDataReceived
extends OnEntityReceivedListener<TagsData> {

}
