package com.tawasol.fcbBarcelona.listeners;

import java.util.List;

import com.tawasol.fcbBarcelona.entities.PostViewModel;

public interface PostFullScreenListener extends UiListener {

	void handle(List<Integer> actions , PostViewModel post);
}
