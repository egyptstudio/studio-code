/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

/**
 * @author Basyouni
 *
 */
public interface OnCommentScreenFinish extends UiListener{
	void getCount(int count);
}
