package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.StudioPhoto;


public interface OnStudioPhotoReceived
extends OnEntityReceivedListener<StudioPhoto> {

}
