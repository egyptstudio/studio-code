package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.PremiumStateObject;

public interface OnPremiumStateRecieved extends UiListener{

	void onPremiumStateRecieved(PremiumStateObject premium , String productID);
}
