package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.StudioPhoto;

public interface OnStudioPhotoRecieved extends UiListener{
	
	void onStudioPhotoRecieved(StudioPhoto photo);
	void onStudioPhotoRecievedFailed(String msg);
}
