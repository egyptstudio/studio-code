/**
 * 
 */
package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.TopTenModel;

/**
 * @author Basyouni
 *
 */
public interface OnSeasonReceived extends
		OnEntityListReceivedListener<TopTenModel> {

}
