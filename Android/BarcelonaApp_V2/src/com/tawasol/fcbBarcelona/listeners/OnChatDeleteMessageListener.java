package com.tawasol.fcbBarcelona.listeners;

/**
 * @author Turki
 */
public interface OnChatDeleteMessageListener extends UiListener {	
	void onDeleteSuccess();
}
