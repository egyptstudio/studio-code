package com.tawasol.fcbBarcelona.listeners;

import java.util.List;

public interface OnEntityListReceivedListener<T> extends UiListener {

	void onSuccess(List<T> objs);
	
}
