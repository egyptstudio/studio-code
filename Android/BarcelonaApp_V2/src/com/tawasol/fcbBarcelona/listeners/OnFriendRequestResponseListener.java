package com.tawasol.fcbBarcelona.listeners;

public interface OnFriendRequestResponseListener extends UiListener{
	void onSuccess(int senderId);
}
