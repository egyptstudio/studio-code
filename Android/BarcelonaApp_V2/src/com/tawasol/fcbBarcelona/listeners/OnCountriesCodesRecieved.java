package com.tawasol.fcbBarcelona.listeners;

import java.util.Set;

public interface OnCountriesCodesRecieved extends UiListener{

	void onPhonesCodesRecieved(Set<String> phonesCodes);
}
