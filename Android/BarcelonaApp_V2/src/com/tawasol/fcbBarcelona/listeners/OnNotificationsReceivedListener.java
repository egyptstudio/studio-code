package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.Notification;


public interface OnNotificationsReceivedListener
extends OnEntityListReceivedListener<Notification> {

}
