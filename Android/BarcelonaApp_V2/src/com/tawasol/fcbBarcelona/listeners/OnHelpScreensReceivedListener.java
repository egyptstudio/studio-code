package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.HelpScreen;

public interface OnHelpScreensReceivedListener
extends OnEntityListReceivedListener<HelpScreen> {

}
