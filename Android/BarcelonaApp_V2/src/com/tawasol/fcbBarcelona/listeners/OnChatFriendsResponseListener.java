package com.tawasol.fcbBarcelona.listeners;

import com.tawasol.fcbBarcelona.entities.FriendsEntity;
import com.tawasol.fcbBarcelona.entities.FriendsFilterEntity;

/**
 * @author Turki
 */
public interface OnChatFriendsResponseListener extends OnEntityListReceivedListener<FriendsEntity>{

}
