package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class UserComment extends BusinessObject {

	private static final long serialVersionUID = 8352983063153237048L;

	public UserComment() {
		super(null, null, null);
	}

	@Expose
	private String commentText;
	@Expose
	private int commentId;
	@Expose
	private int userId;
	@Expose
	private int postId;
	@Expose
	private String fullName;
	@Expose
	@SerializedName("profilePicURL")
	private String profilePic;
	@Expose
	@SerializedName("commentDate")
	private long date;
	@Expose
	boolean following;

	public UserComment(int commentId, int postId) {
		super(null, null, null);
		this.commentId = commentId;
		this.postId = postId;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public boolean isFollowing() {
		return following;
	}

	public void setFollowing(boolean following) {
		this.following = following;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	// RAM
	public static final String USER_NAME_DB_COLUMN = "userName";
	public static final String COMMENTS_TEXT_DB_COLUMN = "commentText";
	public static final String COMMENT_ID_DB_COLUMN = "commentId";
	public static final String USER_ID_DB_COLUMN = "userId";
	public static final String PHOTO_ID_DB_COLUMN = "photoId";
	public static final String FULLNAME_DB_COLUMN = "fullName";
	public static final String PROFILE_PIC_DB_COLUMN = "profilePic";
	public static final String COMMENT_DATE_DB_COLUMN = "commentDate";

	// END

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPhotoId() {
		return postId;
	}

	public void setPhotoId(int photoId) {
		this.postId = photoId;
	}

	public long getCommentDate() {
		return date;
	}

	public void setCommentDate(long date) {
		this.date = date;
	}

	public String getComment() {
		return commentText;
	}

	public void setComment(String comment) {
		this.commentText = comment;
	}

	public String getUserName() {
		return fullName;
	}

	public void setUserName(String userName) {
		this.fullName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getProfilePicture() {
		return profilePic;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePic = profilePicture;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + commentId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserComment other = (UserComment) obj;
		if (commentId != other.commentId)
			return false;
		return true;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}
}