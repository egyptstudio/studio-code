package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class StudioPhotoTag extends BusinessObject{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5185935691899376249L;
	
	// define Studio Tag Type Variables 
	public static final int STUDIO_TAG_TYPE_PLAYER = 1;
	public static final int STUDIO_TAG_TYPE_TEAM = 2;
	public static final int STUDIO_TAG_TYPE_SEASON = 3;
	public static final int STUDIO_TAG_TYPE_TAG = 4;
	
	@Expose
	private int tagId;
	
	@Expose
	private String tagName;
	
	@Expose
	private int tagType;

	public StudioPhotoTag() {
		super(null,null,null);

	}
	
	

	public StudioPhotoTag(String saveMethod, String deleteMethod,
			String getMethod, int tagId, String tagName, int tagType) {
		this();
		this.tagId = tagId;
		this.tagName = tagName;
		this.tagType = tagType;
	}



	public int getTagId() {
		return tagId;
	}



	public void setTagId(int tagId) {
		this.tagId = tagId;
	}



	public String getTagName() {
		return tagName;
	}



	public void setTagName(String tagName) {
		this.tagName = tagName;
	}



	public int getTagType() {
		return tagType;
	}



	public void setTagType(int tagType) {
		this.tagType = tagType;
	}



	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
		
	}

}
