/**
 * 
 */
package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;
/**
 * @author Mohga
 *
 */
public class StudioFolder extends BusinessObject implements Serializable , Comparable<StudioFolder> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4117486188425061019L;
	
	
	// Specify folder Type variables 
	public static final int FOLDER_TYPE_PLAYER = 1;
	public static final int FOLDER_TYPE_TEAM = 2;
	
	

	@Expose
	private int folderId;
	
	@Expose
	private String folderName;
	
	@Expose 
	private String folderPicUrl;
	
	@Expose 
	private int noOfPics;
	
	@Expose 
	private Integer rank;
	
	@Expose 
	private int folderType;
	
	
	
	public StudioFolder() {
		super(null, null, null);
	}

	
	
	public StudioFolder(int folderId, String folderName,
			String folderPicUrl, int noOfPics, int rank, int folderType) {
		this();
		this.folderId = folderId;
		this.folderName = folderName;
		this.folderPicUrl = folderPicUrl;
		this.noOfPics = noOfPics;
		this.rank = rank;
		this.folderType = folderType;
	}



	public int getFolderId() {
		return folderId;
	}



	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}



	public String getFolderName() {
		return folderName;
	}



	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}



	public String getFolderPicURL() {
		return folderPicUrl;
	}



	public void setFolderPicURL(String folderPicURL) {
		this.folderPicUrl = folderPicURL;
	}



	public int getNoOfPics() {
		return noOfPics;
	}



	public void setNoOfPics(int noOfPics) {
		this.noOfPics = noOfPics;
	}



	public Integer getRank() {
		return rank;
	}



	public void setRank(Integer rank) {
		this.rank = rank;
	}



	public int getFolderType() {
		return folderType;
	}



	public void setFolderType(int folderType) {
		this.folderType = folderType;
	}



	/* (non-Javadoc)
	 * @see com.tawasol.barcelona.business.BusinessObject#addValidationRules(android.content.Context)
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}



	@Override
	public int compareTo(StudioFolder another) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
	

	
}
