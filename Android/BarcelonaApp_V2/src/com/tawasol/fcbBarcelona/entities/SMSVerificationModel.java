package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class SMSVerificationModel extends BusinessObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1683955879312569952L;

	@Expose
	private String code;
	
	public SMSVerificationModel(String code) {
		super(null, null, null);
		this.code = code;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	
}
