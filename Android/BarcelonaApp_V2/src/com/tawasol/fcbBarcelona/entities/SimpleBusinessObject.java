package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.tawasol.fcbBarcelona.business.BusinessObject;

public class SimpleBusinessObject extends BusinessObject {

	private static final long serialVersionUID = 7500259318683800683L;


	public SimpleBusinessObject() {
		super(null, null, null);
	}


	@Override
	public void addValidationRules(Context context) {
		// No validations for this simple object
	}

}
