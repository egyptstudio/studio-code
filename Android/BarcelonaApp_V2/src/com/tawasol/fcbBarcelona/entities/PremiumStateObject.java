package com.tawasol.fcbBarcelona.entities;

public class PremiumStateObject {

	boolean premium;
	String endDate;

	public PremiumStateObject(boolean premium, String endDate) {
		super();
		this.premium = premium;
		this.endDate = endDate;
	}

	public boolean isPremium() {
		return premium;
	}

	public void setPremium(boolean premium) {
		this.premium = premium;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
