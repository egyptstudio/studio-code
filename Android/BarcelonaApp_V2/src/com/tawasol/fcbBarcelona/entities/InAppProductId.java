package com.tawasol.fcbBarcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class InAppProductId extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6131187610487650714L;

	@Expose
	private List<InAppPointsEntity> Points;
	@Expose
	private List<InAppPointsEntity> Premium;

	public InAppProductId(List<InAppPointsEntity> points, List<InAppPointsEntity> premium) {
		this();
		Points = points;
		Premium = premium;
	}

	public InAppProductId() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	public List<InAppPointsEntity> getPoints() {
		return Points;
	}

	public void setPoints(List<InAppPointsEntity> points) {
		Points = points;
	}

	public List<InAppPointsEntity> getPremium() {
		return Premium;
	}

	public void setPremium(List<InAppPointsEntity> premium) {
		Premium = premium;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
