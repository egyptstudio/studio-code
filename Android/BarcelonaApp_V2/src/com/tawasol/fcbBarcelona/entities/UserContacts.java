package com.tawasol.fcbBarcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class UserContacts extends BusinessObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -200037038512723281L;

	@Expose
	int userId;
//	@Expose
//	private String name;
	@Expose
	private String phone;
	@Expose
	private String email;

	public UserContacts() {
		super(null, null, null);
	}

	public UserContacts(int userId, String phone, String email) {
		this();
		this.userId = userId;
		this.phone = phone;
		this.email = email;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
