/**
 * 
 */
package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;

import android.content.Context;
 



import com.tawasol.fcbBarcelona.business.BusinessObject;

/**
 * @author Turki
 * 
 */
public class ChangePassword extends BusinessObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5920893578014098879L;
	private String currentPassword;
	private String oldPassword;
	
	/**
	 * 
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}
	
	public ChangePassword() {
		super(null, null, null);
	}
	
	
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param email
	 * @param messageTitle
	 * @param messageText
	 */
	public ChangePassword(String currentPassword, String oldPassword) {
		super(null, null, null);
		this.currentPassword = currentPassword;
		this.oldPassword = oldPassword; 
	}

	/**
	 * @return the currentPassword
	 */
	public String getCurrentPassword() {
		return currentPassword;
	}

	/**
	 * @param currentPassword the currentPassword to set
	 */
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	/**
	 * @return the oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * @param oldPassword the oldPassword to set
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	
}
