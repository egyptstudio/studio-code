package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class GetFanModel extends BusinessObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3354710612218315381L;
	@Expose
	private int nextStartingLimit;
	@Expose
	@SerializedName("data")
	private List<Fan> fans;

	public GetFanModel(int nextStartingLimit, List<Fan> fans) {
		this();
		this.nextStartingLimit = nextStartingLimit;
		this.fans = fans;
	}

	public int getNextStartingLimit() {
		return nextStartingLimit;
	}

	public void setNextStartingLimit(int nextStartingLimit) {
		this.nextStartingLimit = nextStartingLimit;
	}

	public List<Fan> getFans() {
		return fans;
	}

	public void setFans(List<Fan> fans) {
		this.fans = fans;
	}

	public GetFanModel() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
