package com.tawasol.fcbBarcelona.entities;

public class WalkThroughEntity {

	public static final int GO = 1;
	public static final int SHOW_ME = 2;
	public static final int empty = 0;
	public static final int CHECK = 3;

	private String description;
	private String wonXp;
	private String repetation;
	private int buttonType;

	public WalkThroughEntity(String description, String wonXp,
			String repetation, int buttonType) {
		super();
		this.description = description;
		this.wonXp = wonXp;
		this.repetation = repetation;
		this.buttonType = buttonType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWonXp() {
		return wonXp;
	}

	public void setWonXp(String wonXp) {
		this.wonXp = wonXp;
	}

	public String getRepetation() {
		return repetation;
	}

	public void setRepetation(String repetation) {
		this.repetation = repetation;
	}

	public int getButtonType() {
		return buttonType;
	}

	public void setButtonType(int buttonType) {
		this.buttonType = buttonType;
	}

}
