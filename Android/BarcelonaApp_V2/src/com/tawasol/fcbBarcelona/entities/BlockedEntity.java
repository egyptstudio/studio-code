package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class BlockedEntity extends BusinessObject {

	@Expose
	private int fanId;
	@Expose
	private String username;

	/**
	 * 
	 */
	private static final long serialVersionUID = -700043341867729788L;

	public BlockedEntity() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	public BlockedEntity(int fanId) {
		this();
		this.fanId = fanId;
	}

	public BlockedEntity(int fanId, String username) {
		this();
		this.fanId = fanId;
		this.username = username;
	}

	public int getFanId() {
		return fanId;
	}

	public void setFanId(int fanId) {
		this.fanId = fanId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fanId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockedEntity other = (BlockedEntity) obj;
		if (fanId != other.fanId)
			return false;
		return true;
	}

}
