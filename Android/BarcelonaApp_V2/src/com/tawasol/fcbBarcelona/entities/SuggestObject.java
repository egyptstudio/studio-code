package com.tawasol.fcbBarcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class SuggestObject extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7920016069610528155L;
	@Expose
	List<UserContacts> contacts;
	@Expose
	int userID;

	public SuggestObject(List<UserContacts> contacts, int userID) {
		this();
		this.contacts = contacts;
		this.userID = userID;
	}

	public SuggestObject() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	public List<UserContacts> getContacts() {
		return contacts;
	}

	public void setContacts(List<UserContacts> contacts) {
		this.contacts = contacts;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
