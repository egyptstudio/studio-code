package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class Seasons  extends BusinessObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8283581623779501869L;

	@Expose
	private int seasonId;
	
	@Expose
	private String seasonName;
	
	@Expose
	private int rank;
	
	
	public Seasons() {
		super(null, null, null);
	}
	

	public Seasons(String saveMethod, String deleteMethod, String getMethod,
			int seasonId, String seasonName, int rank) {
		this();
		this.seasonId = seasonId;
		this.seasonName = seasonName;
		this.rank = rank;
	}



	public int getSeasonId() {
		return seasonId;
	}



	public void setSeasonId(int seasonId) {
		this.seasonId = seasonId;
	}



	public String getSeasonName() {
		return seasonName;
	}



	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}



	public int getRank() {
		return rank;
	}



	public void setRank(int rank) {
		this.rank = rank;
	}



	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
		
	}

}
