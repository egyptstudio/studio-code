package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class TagsData extends BusinessObject implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3970119672884281358L;

	@Expose 
	private List<KeyValuePairs> tags ;
	
	@Expose 
	private boolean reported;
	
	 
	public TagsData() {
		super(null, null, null);
	}
	

	public TagsData(
			List<KeyValuePairs> tags, boolean reported) {
		this();
		this.tags = tags;
		this.reported = reported;
	}


	public List<KeyValuePairs> getTags() {
		return tags;
	}


	public void setTags(List<KeyValuePairs> tags) {
		this.tags = tags;
	}


	public boolean isReported() {
		return reported;
	}


	public void setReported(boolean reported) {
		this.reported = reported;
	}


	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
		
	}

}
