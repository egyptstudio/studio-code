package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;

/**
 * @author Turki
 * 
 */
public class RecentChatEntity implements Serializable{
	
	String userName, date, lastMessage, profilePic;

	/**
	 * @param userName
	 * @param date
	 * @param lastMessage
	 * @param profilePic
	 */
	public RecentChatEntity(String userName, String date, String lastMessage, String profilePic) {
		super();
		this.userName = userName;
		this.date = date;
		this.lastMessage = lastMessage;
		this.profilePic = profilePic;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the lastMessage
	 */
	public String getLastMessage() {
		return lastMessage;
	}

	/**
	 * @param lastMessage the lastMessage to set
	 */
	public void setLastMessage(String lastMessage) {
		this.lastMessage = lastMessage;
	}

	/**
	 * @return the profilePic
	 */
	public String getProfilePic() {
		return profilePic;
	}

	/**
	 * @param profilePic the profilePic to set
	 */
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	
	
}
