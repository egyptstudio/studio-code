package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class FriendsEntity extends BusinessObject implements Serializable,
		Comparable<FriendsEntity> {

	/**
	 * @author Turki
	 * 
	 */
	private static final long serialVersionUID = 7484551043961738060L;

	public FriendsEntity(String saveMethod, String deleteMethod,
			String getMethod) {
		super(saveMethod, deleteMethod, getMethod);
		// TODO Auto-generated constructor stub
	}

	@Expose
	@SerializedName("friendID")
	int fanID;

	@Expose
	int isPremium;

	@Expose
	int isFollowed;

	@Expose
	@SerializedName("friendName")
	String fanName;

	@Expose
	@SerializedName("friendOnline")
	int fanOnline;

	@Expose
	int isFavorite;

	@Expose
	@SerializedName("friendPicture")
	String fanPicture;

	@Expose
	@SerializedName("friendPicNumber")
	int fanPicNumber;

	@Expose
	int followers;

	@Expose
	String bio;

	@Expose
	String country;

	@Expose
	String city;

	@Expose
	String district;

	@Expose
	String friendRequests;

	@Expose
	private boolean isFriend;
	@Expose
	private boolean isBlocked;
	@Expose
	List<MoreInfo> moreInfo;
	@Expose
	String fanCountry;
	@Expose
	String fanCity;
	@Expose
	String fanDistrict;
	@Expose
	String fanBrief;
	@Expose
	int fanFollowers;
	@Expose
	int fanFollows;

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public FriendsEntity() {
		super(null, null, null);
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param fanID
	 * @param fanName
	 * @param fanOnline
	 */
	public FriendsEntity(int fanID, String fanName) {
		super(null, null, null);
		this.fanID = fanID;
		this.fanName = fanName;
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param fanID
	 * @param isPremium
	 * @param isFollowed
	 * @param fanName
	 * @param fanOnline
	 * @param isFavorite
	 * @param fanPicture
	 * @param fanPicNumber
	 */
	public FriendsEntity(String saveMethod, String deleteMethod,
			String getMethod, int fanID, int isPremium, int isFollowed,
			String fanName, int fanOnline, int isFavorite, String fanPicture,
			int fanPicNumber) {
		this();
		this.fanID = fanID;
		this.isPremium = isPremium;
		this.isFollowed = isFollowed;
		this.fanName = fanName;
		this.fanOnline = fanOnline;
		this.isFavorite = isFavorite;
		this.fanPicture = fanPicture;
		this.fanPicNumber = fanPicNumber;
	}

	public FriendsEntity(String saveMethod, String deleteMethod,
			String getMethod, int fanID, int isPremium, int isFollowed,
			String fanName, int fanOnline, int isFavorite, String fanPicture,
			int fanPicNumber, int followers, String bio, String country,
			String city, String district, String friendRequests,
			boolean isFriend, boolean isBlocked, List<MoreInfo> moreInfo,
			String fanCountry, String fanCity, String fanDistrict,
			String fanBrief, int fanFollowers, int fanFollows) {
		super(saveMethod, deleteMethod, getMethod);
		this.fanID = fanID;
		this.isPremium = isPremium;
		this.isFollowed = isFollowed;
		this.fanName = fanName;
		this.fanOnline = fanOnline;
		this.isFavorite = isFavorite;
		this.fanPicture = fanPicture;
		this.fanPicNumber = fanPicNumber;
		this.followers = followers;
		this.bio = bio;
		this.country = country;
		this.city = city;
		this.district = district;
		this.friendRequests = friendRequests;
		this.isFriend = isFriend;
		this.isBlocked = isBlocked;
		this.moreInfo = moreInfo;
		this.fanCountry = fanCountry;
		this.fanCity = fanCity;
		this.fanDistrict = fanDistrict;
		this.fanBrief = fanBrief;
		this.fanFollowers = fanFollowers;
		this.fanFollows = fanFollows;
	}

	
	
	/**
	 * @return the fanID
	 */
	public int getFanID() {
		return fanID;
	}

	/**
	 * @param fanID
	 *            the fanID to set
	 */
	public void setFanID(int fanID) {
		this.fanID = fanID;
	}

	/**
	 * @return the fanName
	 */
	public String getFanName() {
		return fanName;
	}

	/**
	 * @param fanName
	 *            the fanName to set
	 */
	public void setFanName(String fanName) {
		this.fanName = fanName;
	}

	/**
	 * @return the fanPicture
	 */
	public String getFanPicture() {
		return fanPicture;
	}

	/**
	 * @param fanPicture
	 *            the fanPicture to set
	 */
	public void setFanPicture(String fanPicture) {
		this.fanPicture = fanPicture;
	}

	/**
	 * @return the fanPicNumber
	 */
	public int getFanPicNumber() {
		return fanPicNumber;
	}

	/**
	 * @param fanPicNumber
	 *            the fanPicNumber to set
	 */
	public void setFanPicNumber(int fanPicNumber) {
		this.fanPicNumber = fanPicNumber;
	}

	/**
	 * @return the isPremium
	 */
	public int getIsPremium() {
		return isPremium;
	}

	/**
	 * @param isPremium
	 *            the isPremium to set
	 */
	public void setIsPremium(int isPremium) {
		this.isPremium = isPremium;
	}

	/**
	 * @return the isFollowed
	 */
	public int getIsFollowed() {
		return isFollowed;
	}

	/**
	 * @param isFollowed
	 *            the isFollowed to set
	 */
	public void setIsFollowed(int isFollowed) {
		this.isFollowed = isFollowed;
	}

	/**
	 * @return the fanOnline
	 */
	public int getFanOnline() {
		return fanOnline;
	}

	/**
	 * @param fanOnline
	 *            the fanOnline to set
	 */
	public void setFanOnline(int fanOnline) {
		this.fanOnline = fanOnline;
	}

	/**
	 * @return the isFavorite
	 */
	public int getIsFavorite() {
		return isFavorite;
	}

	/**
	 * @param isFavorite
	 *            the isFavorite to set
	 */
	public void setIsFavorite(int isFavorite) {
		this.isFavorite = isFavorite;
	}

	/**
	 * @return the followers
	 */
	public int getFollowers() {
		return followers;
	}

	/**
	 * @param followers
	 *            the followers to set
	 */
	public void setFollowers(int followers) {
		this.followers = followers;
	}

	/**
	 * @return the bio
	 */
	public String getBio() {
		return bio;
	}

	/**
	 * @param bio
	 *            the bio to set
	 */
	public void setBio(String bio) {
		this.bio = bio;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}

	/**
	 * @param district
	 *            the district to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}

	/**
	 * @return the friendRequests
	 */
	public String getFriendRequests() {
		return friendRequests;
	}

	/**
	 * @param friendRequests
	 *            the friendRequests to set
	 */
	public void setFriendRequests(String friendRequests) {
		this.friendRequests = friendRequests;
	}

	@Override
	public int compareTo(FriendsEntity another) {
		// TODO Auto-generated method stub
		return this.fanName.compareToIgnoreCase(another.fanName);
	}
}
