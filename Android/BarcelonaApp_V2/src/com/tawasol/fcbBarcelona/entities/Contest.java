package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class Contest extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8174175407560937242L;
	
	public static final int CONTEST_STATUS_NOT_STARTED=1;
	public static final int CONTEST_STATUS_RUNNING=2;
	public static final int CONTEST_STATUS_FINISHED=3;
	public static final int CONTEST_STATUS_PRIZES_ANNOUNCED=4;

	@Expose
	private int contestId;
	
	@Expose
	private String name;
	
	//@Expose
	private long startDate;
	
	//@Expose
	private int endDate;
	
	@Expose
	private int status;
	
	
	public Contest() {
		super(null, null, null);
	}
	public Contest(int contestId, String name, long startDate, int endDate,
			int status) {
		this();
		this.contestId = contestId;
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
	}





	public int getContestId() {
		return contestId;
	}

	public void setContestId(int contestId) {
		this.contestId = contestId;
	}

	public String getContestName() {
		return name;
	}

	public void setContestName(String name) {
		this.name = name;
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public int getEndDate() {
		return endDate;
	}

	public void setEndDate(int endDate) {
		this.endDate = endDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	@Override
	public void addValidationRules(Context context) {
		
	}

}
