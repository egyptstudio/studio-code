package com.tawasol.fcbBarcelona.entities;

/**
 * @author Turki
 * 
 */
public class ContactEntity {

	String userName;
	String phone;
	String contactImageURI;
	boolean isSelected = false;

	public ContactEntity(String userName, String phone, String contactImageURI, boolean isSelected) {
		super();
		this.userName = userName;
		this.phone = phone;
		this.contactImageURI=contactImageURI;
		this.isSelected =  isSelected;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the contactImageURI
	 */
	public String getContactImageURI() {
		return contactImageURI;
	}

	/**
	 * @param contactImageURI the contactImageURI to set
	 */
	public void setContactImageURI(String contactImageURI) {
		this.contactImageURI = contactImageURI;
	}

	/**
	 * @return the isSelected
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * @param isSelected the isSelected to set
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	
}
