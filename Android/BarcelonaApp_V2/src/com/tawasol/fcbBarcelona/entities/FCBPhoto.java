/**
 * 
 */
package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

/**
 * @author Turki
 * 
 */
public class FCBPhoto extends BusinessObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1855157913370298847L;

	@Expose
	private String url;
	
	@Expose
	private String photoName;
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public FCBPhoto() {//(String saveMethod, String deleteMethod, String getMethod)
//		super(URLs.Methods.GET_FCB_PHOTOS, null, null);
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.tawasol.barcelona.business.BusinessObject#addValidationRules(android.content.Context)
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}

	
	
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param id
	 * @param url
	 */
	public FCBPhoto(String saveMethod, String deleteMethod, String getMethod, String url, String photoName) {
//		super(saveMethod, deleteMethod, getMethod);
		this();
		this.url = url;
		this.photoName = photoName;
	}


	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the photoName
	 */
	public String getPhotoName() {
		return photoName;
	}

	/**
	 * @param photoName the photoName to set
	 */
	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	
}
