package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tawasol.fcbBarcelona.business.BusinessObject;

/**
 * @author Turki
 * 
 */
public class Message extends BusinessObject {

	private static final long serialVersionUID = -7736959022734758637L;
	
	private int localMessageId;
	@Expose @SerializedName("messageRemoteID")
	private int messageId;
	@Expose @SerializedName("messageText")
	private String text;
	@Expose @SerializedName("senderUserID")
	private int friendId;
	private int sent;
	private int received;
	@Expose @SerializedName("sentAt")
	private long messageTime;
	@Expose 
	private String senderName;
	@Expose 
	private String senderPicture;
	
	@Expose
	int isPremium;
	
	@Expose
	int isFollowed;
	
	@Expose 
	int type;
	
	@Expose 
	int senderOnline;
	
	@Expose
	int isFavorite;
	
	private boolean isMine;
	public static final String senderPic   = "http://barca.s3.amazonaws.com/third_party/uploads/profile/1418908273.jpg";
	public static final String receiverPic = "https://lh6.googleusercontent.com/-55osAWw3x0Q/URquUtcFr5I/AAAAAAAAAbs/rWlj1RUKrYI/s1024/A%252520Photographer.jpg";
	
	
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param messageId
	 * @param text
	 * @param friendId
	 * @param sent
	 * @param received
	 * @param messageTime
	 * @param senderName
	 * @param senderPicture
	 */
	public Message(int messageId, String text, int friendId, int sent, int received,
			long messageTime, String senderName, String senderPicture) {
		super(null, null, null);
		this.messageId = messageId;
		this.text = text;
		this.friendId = friendId;
		this.sent = sent;
		this.received = received;
		this.messageTime = messageTime;
		this.senderName = senderName;
		this.senderPicture = senderPicture;
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param text
	 * @param isMine
	 */
	public Message(String text, int received) {
		super(null, null, null);
		this.text = text;
		this.received = received;
	}

	
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param text
	 * @param friendId
	 */
	public Message(String text, int friendId, int sent) {
		super(null, null, null);
		this.text = text;
		this.friendId = friendId;
		this.sent = sent;
	}

	public Message() {
		super(null, null, null);
	}

	@Override
	public void addValidationRules(Context context) {
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getFriendId() {
		return friendId;
	}

	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}

	public long getMessageTime() {
		return messageTime;
	}

	public void setMessageTime(long messageTime) {
		this.messageTime = messageTime;
	}
	
	@Override
	public int getId() {
		return messageId;
	}

	
	/**
	 * @return the senderName
	 */
	public String getSenderName() {
		return senderName;
	}

	/**
	 * @param senderName the senderName to set
	 */
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	/**
	 * @return the isMine
	 */
	public boolean isMine() {
		return isMine;
	}

	/**
	 * @param isMine the isMine to set
	 */
	public void setMine(boolean isMine) {
		this.isMine = isMine;
	}

	/**
	 * @return the senderPicture
	 */
	public String getSenderPicture() {
		return senderPicture;
	}

	/**
	 * @param senderPicture the senderPicture to set
	 */
	public void setSenderPicture(String senderPicture) {
		this.senderPicture = senderPicture;
	}

	/**
	 * @return the localMessageId
	 */
	public int getLocalMessageId() {
		return localMessageId;
	}

	/**
	 * @param localMessageId the localMessageId to set
	 */
	public void setLocalMessageId(int localMessageId) {
		this.localMessageId = localMessageId;
	}

	/**
	 * @return the sent
	 */
	public int getSent() {
		return sent;
	}

	/**
	 * @param sent the sent to set
	 */
	public void setSent(int sent) {
		this.sent = sent;
	}

	/**
	 * @return the received
	 */
	public int getReceived() {
		return received;
	}

	/**
	 * @param received the received to set
	 */
	public void setReceived(int received) {
		this.received = received;
	}

	/**
	 * @return the messageId
	 */
	public int getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return the isPremium
	 */
	public int getIsPremium() {
		return isPremium;
	}

	/**
	 * @param isPremium the isPremium to set
	 */
	public void setIsPremium(int isPremium) {
		this.isPremium = isPremium;
	}

	/**
	 * @return the isFollowed
	 */
	public int getIsFollowed() {
		return isFollowed;
	}

	/**
	 * @param isFollowed the isFollowed to set
	 */
	public void setIsFollowed(int isFollowed) {
		this.isFollowed = isFollowed;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the senderOnline
	 */
	public int getSenderOnline() {
		return senderOnline;
	}

	/**
	 * @param senderOnline the senderOnline to set
	 */
	public void setSenderOnline(int senderOnline) {
		this.senderOnline = senderOnline;
	}

	/**
	 * @return the isFavorite
	 */
	public int getIsFavorite() {
		return isFavorite;
	}

	/**
	 * @param isFavorite the isFavorite to set
	 */
	public void setIsFavorite(int isFavorite) {
		this.isFavorite = isFavorite;
	}
	
}
