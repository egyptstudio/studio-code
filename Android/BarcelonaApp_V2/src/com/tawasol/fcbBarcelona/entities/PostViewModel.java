package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;

public class PostViewModel extends Post implements Serializable,
		Comparable<PostViewModel> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6183932303590188840L;
	@Expose
	int postId;
	@Expose
	int originalPostId;
	@Expose
	long lastUpdateTime;
	@Expose
	int userId;
	@Expose
	int studioPhotoId;
	@Expose
	String postPhotoUrl;
	@Expose
	String userName;
	@Expose
	String userCountry;
	@Expose
	String userProfilePicUrl;
	@Expose
	long creationTime;
	@Expose
	boolean liked;
	@Expose
	int commentCount;
	@Expose
	int likeCount;
	@Expose
	int reportCount;
	@Expose
	int repostCount;
	@Expose
	int photoOrientation;
	@Expose
	int privacy;
	@Expose
	int contestRank;
	@Expose
	List<KeyValuePairs> tags;
	// @Expose
	// List<PostComment_new> postComments;
	// @Expose
	// List<PostLike_new> postLikes;
	@Expose
	boolean reported;
	@Expose
	boolean hidden;
	@Expose
	String postPhotoCaption;
	@Expose
	boolean rePosted;
	@Expose
	boolean following;
	@Expose
	boolean favorite;
	@Expose
	String rePostedBy;
	@Expose
	String rePosterProfilePic;
	@Expose
	int rePosterId;
	@Expose
	boolean rePosterIsFollowing;
	boolean isWall;
	boolean isLatest;
	boolean isTopTen;
	boolean isMyPics;

	public PostViewModel() {
	}

	public PostViewModel(int postId) {
		this.postId = postId;
	}

	public PostViewModel(int postId, int originalPostId, long lastUpdateTime,
			int userId, int studioPhotoId, String postPhotoUrl,
			String userName, String userCountry, String userProfilePicUrl,
			long creationTime, boolean liked, int commentCount, int likeCount,
			int reportCount, int photoOrientation, int privacy,
			int contestRank, List<KeyValuePairs> tags, boolean reported,
			boolean hidden, String postPhotoCaption, boolean rePosted,
			boolean following, boolean favorite, String repostedBy,
			String rePosterProfilePic, int rePosterId,
			boolean rePosterIsFollowing) {
		this.postId = postId;
		this.originalPostId = originalPostId;
		this.lastUpdateTime = lastUpdateTime;
		this.userId = userId;
		this.studioPhotoId = studioPhotoId;
		this.postPhotoUrl = postPhotoUrl;
		this.userName = userName;
		this.userCountry = userCountry;
		this.userProfilePicUrl = userProfilePicUrl;
		this.creationTime = creationTime;
		this.liked = liked;
		this.commentCount = commentCount;
		this.likeCount = likeCount;
		this.reportCount = reportCount;
		this.photoOrientation = photoOrientation;
		this.privacy = privacy;
		this.contestRank = contestRank;
		this.tags = tags;
		this.reported = reported;
		this.hidden = hidden;
		this.postPhotoCaption = postPhotoCaption;
		this.rePosted = rePosted;
		this.following = following;
		this.favorite = favorite;
		this.rePostedBy = repostedBy;
		this.rePosterProfilePic = rePosterProfilePic;
		this.rePosterId = rePosterId;
		this.rePosterIsFollowing = rePosterIsFollowing;
	}

	public PostViewModel(String saveMethod, String deleteMethod,
			String getMethod, int postId, int originalPostId,
			long lastUpdateTime, int userId, int studioPhotoId,
			String postPhotoUrl, String userName, String userCountry,
			String userProfilePicUrl, long creationTime, boolean liked,
			int commentCount, int likeCount, int reportCount,
			int photoOrientation, int privacy, int contestRank,
			List<KeyValuePairs> tags, boolean reported, boolean hidden,
			String postPhotoCaption, boolean rePosted, boolean following,
			boolean favorite, String repostedBy, String rePosterProfilePic,
			int rePosterId, boolean rePosterIsFollowing, boolean isWall,
			boolean isLatest, boolean isTopTen, boolean isMyPics) {
		this.postId = postId;
		this.originalPostId = originalPostId;
		this.lastUpdateTime = lastUpdateTime;
		this.userId = userId;
		this.studioPhotoId = studioPhotoId;
		this.postPhotoUrl = postPhotoUrl;
		this.userName = userName;
		this.userCountry = userCountry;
		this.userProfilePicUrl = userProfilePicUrl;
		this.creationTime = creationTime;
		this.liked = liked;
		this.commentCount = commentCount;
		this.likeCount = likeCount;
		this.reportCount = reportCount;
		this.photoOrientation = photoOrientation;
		this.privacy = privacy;
		this.contestRank = contestRank;
		this.tags = tags;
		this.reported = reported;
		this.hidden = hidden;
		this.postPhotoCaption = postPhotoCaption;
		this.rePosted = rePosted;
		this.following = following;
		this.favorite = favorite;
		this.rePostedBy = repostedBy;
		this.rePosterProfilePic = rePosterProfilePic;
		this.rePosterId = rePosterId;
		this.rePosterIsFollowing = rePosterIsFollowing;
		this.isWall = isWall;
		this.isLatest = isLatest;
		this.isTopTen = isTopTen;
		this.isMyPics = isMyPics;
	}

	public int getRepostCount() {
		return repostCount;
	}

	public void setRepostCount(int repostCount) {
		this.repostCount = repostCount;
	}

	public String getRePostedBy() {
		return rePostedBy;
	}

	public void setRePostedBy(String rePostedBy) {
		this.rePostedBy = rePostedBy;
	}

	public boolean isWall() {
		return isWall;
	}

	public void setWall(boolean isWall) {
		this.isWall = isWall;
	}

	public boolean isLatest() {
		return isLatest;
	}

	public void setLatest(boolean isLatest) {
		this.isLatest = isLatest;
	}

	public boolean isTopTen() {
		return isTopTen;
	}

	public void setTopTen(boolean isTopTen) {
		this.isTopTen = isTopTen;
	}

	public boolean isMyPics() {
		return isMyPics;
	}

	public void setMyPics(boolean isMyPics) {
		this.isMyPics = isMyPics;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public int getOriginalPostId() {
		return originalPostId;
	}

	public void setOriginalPostId(int originalPostId) {
		this.originalPostId = originalPostId;
	}

	public long getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(long lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getStudioPhotoId() {
		return studioPhotoId;
	}

	public void setStudioPhotoId(int studioPhotoId) {
		this.studioPhotoId = studioPhotoId;
	}

	public String getPostPhotoUrl() {
		return postPhotoUrl;
	}

	public void setPostPhotoUrl(String postPhotoUrl) {
		this.postPhotoUrl = postPhotoUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	public String getUserProfilePicUrl() {
		return userProfilePicUrl;
	}

	public void setUserProfilePicUrl(String userProfilePicUrl) {
		this.userProfilePicUrl = userProfilePicUrl;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public int getReportCount() {
		return reportCount;
	}

	public void setReportCount(int reportCount) {
		this.reportCount = reportCount;
	}

	public int getPhotoOrientation() {
		return photoOrientation;
	}

	public void setPhotoOrientation(int photoOrientation) {
		this.photoOrientation = photoOrientation;
	}

	public int getPrivacy() {
		return privacy;
	}

	public void setPrivacy(int privacy) {
		this.privacy = privacy;
	}

	public int getContestRank() {
		return contestRank;
	}

	public void setContestRank(int contestRank) {
		this.contestRank = contestRank;
	}

	public List<KeyValuePairs> getTags() {
		return tags;
	}

	public void setTags(List<KeyValuePairs> tags) {
		this.tags = tags;
	}

	public boolean isReported() {
		return reported;
	}

	public void setReported(boolean reported) {
		this.reported = reported;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public String getPostPhotoCaption() {
		return postPhotoCaption;
	}

	public void setPostPhotoCaption(String postPhotoCaption) {
		this.postPhotoCaption = postPhotoCaption;
	}

	public boolean isRePosted() {
		return rePosted;
	}

	public void setRePosted(boolean rePosted) {
		this.rePosted = rePosted;
	}

	public boolean isFollowing() {
		return following;
	}

	public void setFollowing(boolean following) {
		this.following = following;
	}

	public boolean isFavorite() {
		return favorite;
	}

	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}

	public String getRepostedBy() {
		return rePostedBy;
	}

	public void setRepostedBy(String repostedBy) {
		this.rePostedBy = repostedBy;
	}

	public String getRePosterProfilePic() {
		return rePosterProfilePic;
	}

	public void setRePosterProfilePic(String rePosterProfilePic) {
		this.rePosterProfilePic = rePosterProfilePic;
	}

	public int getRePosterId() {
		return rePosterId;
	}

	public void setRePosterId(int rePosterId) {
		this.rePosterId = rePosterId;
	}

	public boolean isRePosterIsFollowing() {
		return rePosterIsFollowing;
	}

	public void setRePosterIsFollowing(boolean rePosterIsFollowing) {
		this.rePosterIsFollowing = rePosterIsFollowing;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + postId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostViewModel other = (PostViewModel) obj;
		if (postId != other.postId)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tawasol.barcelona.business.BusinessObject#addValidationRules(android
	 * .content.Context)
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(PostViewModel another) {
		return (int) (this.lastUpdateTime - another.lastUpdateTime);
	}

}
