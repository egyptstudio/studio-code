/**
 * 
 */
package com.tawasol.fcbBarcelona.entities;

import java.util.List;

/**
 * @author Basyouni
 *
 */
public class WebServiceRequestInfo {

	public static final int GET_TAGS = 1;
	public static final int GET_POSTS = 2;
	public static final int GET_USER_POSTS = 3;

	// public static final String FRAGMENT_INFO_KEY = "fragmentInfoKey";
	// public static final int LATEST_TAB = 1;
	// public static final int WALL = 2;
	// public static final int MY_PIC = 3;
	// public static final int TOP_TEN = 4;

	public static final int TURN_OFF_FILTER = 0;
	public static final int TURN_ON_FILTER = 1;
	
	public static final int ACTION_GETMORE = 1;
	public static final int ACTION_UPDATE = 3;
	public static final int ACTION_FIRST_TIME = 2;

	public static final int TIME_FILTER_OLD = 0;
	public static final int TIME_FILTER_NEW = 1;

	// private int wallFragmentType;
	private int action;
	int userId;
	int n;
	long lasrRequestTime;
	int timeFilter;
	int tagId;
	int fanId;
	int type;
	String methodName;
	List<Integer> countryList;
	int favoritePostsFlag;
	
	public WebServiceRequestInfo(int action, int userId, int n,
			long lasrRequestTime, int timeFilter, int tagId, int fanId,
			int type, String methodName, List<Integer> countryList,
			int favoritePostsFlag) {
		super();
		this.action = action;
		this.userId = userId;
		this.n = n;
		this.lasrRequestTime = lasrRequestTime;
		this.timeFilter = timeFilter;
		this.tagId = tagId;
		this.fanId = fanId;
		this.type = type;
		this.methodName = methodName;
		this.countryList = countryList;
		this.favoritePostsFlag = favoritePostsFlag;
	}

	public WebServiceRequestInfo(/* int wallFragmentType, */int action,
			int userId, int n, long lasrRequestTime, int timeFilter, int tagId,
			int fanId, int type, String methodName) {
		super();
		// this.wallFragmentType = wallFragmentType;
		this.action = action;
		this.userId = userId;
		this.n = n;
		this.lasrRequestTime = lasrRequestTime;
		this.timeFilter = timeFilter;
		this.tagId = tagId;
		this.fanId = fanId;
		this.type = type;
		this.methodName = methodName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public long getLasrRequestTime() {
		return lasrRequestTime;
	}

	public void setLasrRequestTime(long lasrRequestTime) {
		this.lasrRequestTime = lasrRequestTime;
	}

	public int getTimeFilter() {
		return timeFilter;
	}

	public void setTimeFilter(int timeFilter) {
		this.timeFilter = timeFilter;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public int getFanId() {
		return fanId;
	}

	public void setFanId(int fanId) {
		this.fanId = fanId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	// public int getWallFragmentType() {
	// return wallFragmentType;
	// }
	//
	// public void setWallFragmentType(int wallFragmentType) {
	// this.wallFragmentType = wallFragmentType;
	// }

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + action;
		result = prime * result
				+ (int) (lasrRequestTime ^ (lasrRequestTime >>> 32));
		result = prime * result
				+ ((methodName == null) ? 0 : methodName.hashCode());
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebServiceRequestInfo other = (WebServiceRequestInfo) obj;
		if (action != other.action)
			return false;
		if (lasrRequestTime != other.lasrRequestTime)
			return false;
		if (methodName == null) {
			if (other.methodName != null)
				return false;
		} else if (!methodName.equals(other.methodName))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebServiceRequestInfo [action=" + action + ", lasrRequestTime="
				+ lasrRequestTime + ", type=" + type + ", methodName="
				+ methodName + "]";
	}

	public List<Integer> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<Integer> countryList) {
		this.countryList = countryList;
	}

	public int getFavoritePostsFlag() {
		return favoritePostsFlag;
	}

	public void setFavoritePostsFlag(int favoritePostsFlag) {
		this.favoritePostsFlag = favoritePostsFlag;
	}

}
