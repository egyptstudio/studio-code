package com.tawasol.fcbBarcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

/**
 * @author Turki
 * 
 */
public class DeviceContactModel extends BusinessObject{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = -3981539464179696291L;

	@Expose
	String userName;
	@Expose
	List<String> phones;
	@Expose
	List<String> emails;
	
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub	
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param userName
	 * @param phone
	 * @param emails
	 */
	public DeviceContactModel(String userName, List<String> phones, List<String> emails) {
		super(null, null, null);
		this.userName = userName;
		this.phones = phones;
		this.emails = emails;
	}
	
	
}
