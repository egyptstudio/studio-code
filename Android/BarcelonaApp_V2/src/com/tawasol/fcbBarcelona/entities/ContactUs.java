/**
 * 
 */
package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;

import android.content.Context;
 



import com.tawasol.fcbBarcelona.business.BusinessObject;

/**
 * @author Turki
 * 
 */
public class ContactUs extends BusinessObject implements Serializable{

	private static final long serialVersionUID = -8262577603949613758L;

	private String email;
	private String messageTitle;
	private String messageText;
	
	/**
	 * 
	 */
	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}
	
	public ContactUs() {
		super(null, null, null);
	}
	
	
	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 * @param email
	 * @param messageTitle
	 * @param messageText
	 */
	public ContactUs(String email, String messageTitle, String messageText) {
		super(null, null, null);
		this.email = email;
		this.messageTitle = messageTitle;
		this.messageText = messageText;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the messageTitle
	 */
	public String getMessageTitle() {
		return messageTitle;
	}
	/**
	 * @param messageTitle the messageTitle to set
	 */
	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}
	/**
	 * @return the messageText
	 */
	public String getMessageText() {
		return messageText;
	}
	/**
	 * @param messageText the messageText to set
	 */
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	
	
}
