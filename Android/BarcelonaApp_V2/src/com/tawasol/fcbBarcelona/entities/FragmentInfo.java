package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;

public class FragmentInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2414530183921698880L;
	
	public static final String FRAGMENT_INFO_KEY = "fragmentInfoKey";
	public static final int LATEST_TAB = 0;
	public static final int WALL = 1;
	public static final int MY_PIC = 2;
	public static final int TOP_TEN = 3;
	public static final int Filtered = 4;
	public static final int TAG_LIST = 5;
	public static final int FULL_SCREEN = 6;
	
	public static final int ACTION_GETMORE = 1;
	public static final int ACTION_UPDATE = 3;
	public static final int ACTION_FIRST_TIME = 2;

	private int type;
	private int action;

	public FragmentInfo(int type, int action) {
		super();
		this.type = type;
		this.action = action;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public static String getMethodName(FragmentInfo info){
		String methodName = null;
		if(info.getType() == LATEST_TAB)
			methodName = "wall/getLatest";
		else if(info.getType() == WALL)
			methodName = "wall/getWall";
		else if(info.getType() == MY_PIC)
			methodName = "wall/getMyPosts";
		else if(info.getType() == TOP_TEN)
			methodName = "wall/getTopTen";
		return methodName;
	}
	
}
