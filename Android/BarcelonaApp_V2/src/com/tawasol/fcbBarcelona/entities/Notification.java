package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class Notification extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -283649756250932646L;
	
	public static final int LIKE_NOTIFICATIONS =1;
	public static final int COMMENT_NOTIFICATIONS =2;
	public static final int FRIEND_REQUEST_NOTIFICATIONS =3;
	public static final int FOLLOWING_NOTIFICATIONS =4;
	public static final int CHANGE_RROFILE_NOTIFICATIONS =5;
	public static final int POST_NEW_PHOTO_NOTIFICATIONS =6;
	public static final int ADD_FRIEND_NOTIFICATIONS =7;
	public static final int STUDIO_NOTIFICATIONS =8;
	
	public static final int SYS_MSG_NOTIFICATIONS =9;

	@Expose
	private int notificationId;

	@Expose
	private int senderId;
	
	@Expose
	private String senderFullName;
	
	@Expose
	private int receiverId;
	
	@Expose
	private String receiverFullName;
	
	@Expose
	private long time;
	
	@Expose
	private int postId;
	
	@Expose
	private int notificationType;
	
	@Expose
	private String senderProfilePicUrl;
	
//	@Expose
//	private String receiverImgUrl;
	
	private boolean isRead;
	
	
	
	public Notification() {
		super(null, null, null);
	}
	
	public Notification(int senderId) {
		super(null, null, null);
		this.senderId = senderId;
	}

	@Override
	public void addValidationRules(Context context) {
		
	}
	
	

	

	public Notification(int notificationId, int senderId,
			String senderFullName, int receiverId, String receiverFullName,
			long time, int postId, int notificationType, String senderImgUrl,
		    boolean isRead) {
		this();
		this.notificationId = notificationId;
		this.senderId = senderId;
		this.senderFullName = senderFullName;
		this.receiverId = receiverId;
		this.receiverFullName = receiverFullName;
		this.time = time;
		this.postId = postId;
		this.notificationType = notificationType;
		this.senderProfilePicUrl = senderImgUrl;
	//	this.receiverImgUrl = receiverImgUrl;
		this.isRead = isRead;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public int getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public String getSenderFullName() {
		return senderFullName;
	}

	public void setSenderFullName(String senderFullName) {
		this.senderFullName = senderFullName;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public String getReceiverFullName() {
		return receiverFullName;
	}

	public void setReceiverFullName(String receiverFullName) {
		this.receiverFullName = receiverFullName;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public int getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(int notificationType) {
		this.notificationType = notificationType;
	}

	public String getSenderImgUrl() {
		return senderProfilePicUrl;
	}

	public void setSenderImgUrl(String senderImgUrl) {
		this.senderProfilePicUrl = senderImgUrl;
	}

	
	
	
	
	
	

}
