package com.tawasol.fcbBarcelona.entities;

import java.util.List;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class ContestData extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9024175004009971281L;
	
	
	@Expose
	private Contest Contest;
	
	@Expose 
	private List<ContestPrize> ContestPrize;
	
	@Expose
	private List<ContestWinner> ContestWinner;
	
	@Expose
	private String contestRules;

	public ContestData() {
		super(null, null, null);
	}
	
	

	public ContestData(Contest contest,
			List<ContestPrize> ContestPrize,
			List<ContestWinner> ContestWinner, String contestRules) {
		this();
		this.Contest = contest;
		this.ContestPrize = ContestPrize;
		this.ContestWinner = ContestWinner;
		this.contestRules = contestRules;
	}



	public Contest getContest() {
		return Contest;
	}



	public void setContest(Contest Contest) {
		this.Contest = Contest;
	}



	public List<ContestPrize> getContestPrize() {
		return ContestPrize;
	}



	public void setContestPrize(List<ContestPrize> contestPrizes) {
		this.ContestPrize = contestPrizes;
	}



	public List<ContestWinner> getContestWinner() {
		return ContestWinner;
	}



	public void setContestWinners(List<ContestWinner> contestWinner) {
		this.ContestWinner = contestWinner;
	}



	public String getContestRules() {
		return contestRules;
	}



	public void setContestRules(String contestRules) {
		this.contestRules = contestRules;
	}



	@Override
	public void addValidationRules(Context context) {
		
	}

}
