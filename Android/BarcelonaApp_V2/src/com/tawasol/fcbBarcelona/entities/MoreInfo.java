package com.tawasol.fcbBarcelona.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class MoreInfo extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6160774297482935651L;

	@Expose
	String title;
	@Expose
	String value;

	public MoreInfo(String saveMethod, String deleteMethod, String getMethod,
			String title, String value) {
		super(saveMethod, deleteMethod, getMethod);
		this.title = title;
		this.value = value;
	}

	public MoreInfo() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
