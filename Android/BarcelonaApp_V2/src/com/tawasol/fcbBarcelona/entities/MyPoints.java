package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.tawasol.fcbBarcelona.business.BusinessObject;

public class MyPoints extends BusinessObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5394807165842970887L;
	@Expose
	private int points;

	public MyPoints() {
		super(null, null, null);
		// TODO Auto-generated constructor stub
	}

	public MyPoints(int points) {
		this();
		this.points = points;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub

	}

}
