package com.tawasol.fcbBarcelona.entities;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.annotations.Expose; 
import com.tawasol.fcbBarcelona.business.BusinessObject;

/**
 * @author Turki
 * 
 */
public class SystemMessagesEntity extends BusinessObject implements Serializable{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 5931972492946958205L;

	public SystemMessagesEntity(String saveMethod, String deleteMethod, String getMethod) {
		super(saveMethod, deleteMethod, getMethod);
		// TODO Auto-generated constructor stub
	}
	
//	@Expose
//	String systemMsg;
	
	@Expose
	int messageId;
	
//	@Expose
//	int messageId;
//	
	@Expose
	long messageTime;
	
	@Expose
	String messageText;
	
	boolean checked;
	
	public SystemMessagesEntity(int messageId, String messageText) {
		super(null, null, null);
		this.messageId = messageId;
		this.messageText = messageText;
	}

	@Override
	public void addValidationRules(Context context) {
		// TODO Auto-generated method stub
	}

	/**
	 * @param saveMethod
	 * @param deleteMethod
	 * @param getMethod
	 */
	public SystemMessagesEntity() {
		super(null, null, null);
	}

	/**
	 * @return the systemMsg
	 */
//	public String getSystemMsg() {
//		return systemMsg;
//	}

	/**
	 * @param systemMsg the systemMsg to set
	 */
//	public void setSystemMsg(String systemMsg) {
//		this.systemMsg = systemMsg;
//	}

	/**
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * @return the messageId
	 */
	public int getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return the messageTime
	 */
	public long getMessageTime() {
		return messageTime;
	}

	/**
	 * @param messageTime the messageTime to set
	 */
	public void setMessageTime(long messageTime) {
		this.messageTime = messageTime;
	}

	/**
	 * @return the messageText
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * @param messageText the messageText to set
	 */
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}	
}
