package com.tawasol.fcbBarcelona.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.business.BusinessManager;
import com.tawasol.fcbBarcelona.business.BusinessManager.Action.ActionType;
import com.tawasol.fcbBarcelona.data.cache.NotificationsTable;
import com.tawasol.fcbBarcelona.data.cache.SeasonsTable;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.data.cache.StudioFoldersTable;
import com.tawasol.fcbBarcelona.data.cache.StudioPhotosTable;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.data.connection.URLs;
import com.tawasol.fcbBarcelona.entities.Notification;
import com.tawasol.fcbBarcelona.entities.WALLPosts;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.OnFriendRequestResponseListener;
import com.tawasol.fcbBarcelona.listeners.OnNotificationsReceivedListener;
import com.tawasol.fcbBarcelona.listeners.OnPostReceived;
import com.tawasol.fcbBarcelona.listeners.UiListener;
import com.tawasol.fcbBarcelona.responses.BaseResponse;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.NotificationsActivity;
import com.tawasol.fcbBarcelona.ui.fragments.NotificationsFragment;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;

/**
 * @author Mohga
 * */

public class NotificationsManager extends BusinessManager<Notification> {

	private static final String LAST_REQUEST_TIME_NOTIFICATIONS_KEY = "com.tawasol.barcelona.managers.lastRequestTime";
	private static NotificationsManager instance;
	private long lastRequestTime = 0;
	ArrayList<Notification> requestsList = new ArrayList<Notification>();
	private static int pageNo = 1;
	ArrayList<Notification> notifications;
	static boolean likeAlreadyFired = false;
	static boolean commentAlreadyFired = false;
	static boolean followAlreadyFired = false;
	static boolean friendRequestAlreadyFired = false;
	static boolean favAlreadyFired = false;

	// constructor
	protected NotificationsManager() {
		super(Notification.class);
	}

	// get instance of the manager
	public static NotificationsManager getInstance() {
		if (instance == null)
			instance = new NotificationsManager();
		return instance;
	}

	public List<Notification> getReqestsList() {
		return requestsList;
	}

	/** Get Notifications List */

	public void getNotificationsFromServer() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {

				Action notificationsAction = new Action(
						ActionType.GET_NOTIFICATIONS, getLastRequestTime());
				if (!needsProcessing(notificationsAction))
					return;

				try {
					// adding get notification action to actions list
					notifyActionStarted(notificationsAction);

					if(pageNo == 1){
						likeAlreadyFired = false;
						commentAlreadyFired = false;
						followAlreadyFired = false;
						friendRequestAlreadyFired = false;
						favAlreadyFired = false;
					}
						
					// get the list of notifications
					notifications = (ArrayList<Notification>) getObjectList(
							URLs.Methods.GET_NOTIFICATIONS,
							prepareNotificationsParams(pageNo));
					// notifications.add(new Notification(1111, 711, "", 0, "",
					// 0,
					// 0, 8, "", false));
					if (notifications != null && !notifications.isEmpty()) {
						// cache the returned notifications
						cachNotifications(notifications);
						ArrayList<Notification> sortedNotification = new ArrayList<Notification>();
						sortedNotification.addAll(notifications);
						Collections.sort(sortedNotification,
								new Comparator<Notification>() {

									public int compare(Notification o1,
											Notification o2) {
										return Integer
												.valueOf(o1.getNotificationId())
												.compareTo(
														Integer.valueOf(o2
																.getNotificationId()));
									}
								});
						clearNotification(sortedNotification.get(
								sortedNotification.size() - 1)
								.getNotificationId());
//						fireLocalNotification(notifications);
					}
					notifications = HandleStudioNotificationIFExist(notifications);

					// remove the notifications action from action list
					notifyActionDone(notificationsAction);

					// notify the UI if any listener registered
					notifyEntityListReceviedSuccess(notifications,
							OnNotificationsReceivedListener.class);

					pageNo += 1;
					getNotificationsFromServer();
				} catch (Exception e) {
					pageNo = 1;

					// validate the returned list and save the last request time
					if (notifications != null && notifications.size() > 0)
						saveLastRequestTime(notifications.get(
								notifications.size() - 1).getTime());
					notifyActionDone(notificationsAction);
					e.printStackTrace();
					if (!(e instanceof AppException))
						e = AppException.getAppException(e);
					notifyRetrievalException((AppException) e,
							OnNotificationsReceivedListener.class);
				}

			}
		});
	}

	protected void clearNotification(final int notificationId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				Map<String, Object> parametrs = new HashMap<String, Object>();
				parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
						.getDeviceLanguage());
				parametrs.put(Params.Notifications.USER_ID, UserManager
						.getInstance().getCurrentUserId());
				parametrs.put(Params.Notifications.NOTIFICATION_ID,
						notificationId);

				try {
					submit(parametrs, URLs.Methods.CLEAR_NOTIFICATIONS);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	protected void fireLocalNotification(List<Notification> notifications) {
		App.getInstance().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				NotificationsFragment.updateTab();
			}
		});
		for (Notification notification : notifications) {
			switch (notification.getNotificationType()) {
			case Notification.LIKE_NOTIFICATIONS:
				if (!likeAlreadyFired) {
					likeAlreadyFired = true;
					showNotification(Notification.LIKE_NOTIFICATIONS);
				}
				break;
			case Notification.COMMENT_NOTIFICATIONS:
				if (!commentAlreadyFired) {
					commentAlreadyFired = true;
					showNotification(Notification.COMMENT_NOTIFICATIONS);
				}
				break;
			case Notification.FOLLOWING_NOTIFICATIONS:
				if (!followAlreadyFired) {
					followAlreadyFired = true;
					showNotification(Notification.FOLLOWING_NOTIFICATIONS);
				}
				break;
			case Notification.FRIEND_REQUEST_NOTIFICATIONS:
				if (!friendRequestAlreadyFired) {
					friendRequestAlreadyFired = true;
					showNotification(Notification.FRIEND_REQUEST_NOTIFICATIONS);
				}
				break;
			default:
				if (!favAlreadyFired) {
					favAlreadyFired = true;
					showNotification(Notification.ADD_FRIEND_NOTIFICATIONS);
				}
				break;
			}
		}
	}

	private void showNotification(int type) {
		Context context2 = App.getInstance().getApplicationContext();
		Resources res = context2.getResources();
		switch (type) {
		case Notification.LIKE_NOTIFICATIONS:
			Notif(res.getString(R.string.notification_like),
					NotificationsFragment.LIKES);
			break;
		case Notification.COMMENT_NOTIFICATIONS:
			Notif(res.getString(R.string.notification_comments),
					NotificationsFragment.COMMENTS);
			break;
		case Notification.FRIEND_REQUEST_NOTIFICATIONS:
			Notif(res.getString(R.string.notification_request),
					NotificationsFragment.REQUESTS);
			break;
		case Notification.FOLLOWING_NOTIFICATIONS:
			Notif(res.getString(R.string.notification_follow),
					NotificationsFragment.FOLLOWING);
			break;
		case Notification.ADD_FRIEND_NOTIFICATIONS:
			Notif(res.getString(R.string.notification_fav),
					NotificationsFragment.FAVORITES);
			break;
		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	public void Notif(String msg, int type) {
		NotificationManager nm = (NotificationManager) App.getInstance()
				.getApplicationContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Context context2 = App.getInstance().getApplicationContext();
		Resources res = context2.getResources();
		NotificationCompat.Builder builder = new NotificationCompat.Builder(App
				.getInstance().getApplicationContext());
		builder.setSmallIcon(R.drawable.ic_launcher)
				.setLargeIcon(
						BitmapFactory.decodeResource(res,
								R.drawable.ic_launcher))
				.setTicker(res.getString(R.string.app_name))
				.setWhen(System.currentTimeMillis()).setAutoCancel(true)
				.setContentTitle(res.getString(R.string.app_name))
				.setContentText(msg);
		android.app.Notification notify = builder.build();
		notify.flags = android.app.Notification.FLAG_AUTO_CANCEL;
		notify.defaults = android.app.Notification.DEFAULT_ALL;
		notify.defaults = android.app.Notification.DEFAULT_VIBRATE;
		notify.defaults = android.app.Notification.DEFAULT_LIGHTS;
		notify.defaults = android.app.Notification.DEFAULT_SOUND;
		Context context1 = App.getInstance().getApplicationContext();
		CharSequence title = res.getString(R.string.app_name);
		CharSequence details = msg;
		Intent intent1 = new Intent(context1, NotificationsActivity.class);
		intent1.putExtra(NotificationsFragment.WHICH_TAB, type);
		intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		int iUniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
		PendingIntent pending = PendingIntent.getActivity(context1, iUniqueId, intent1,
				0);
		notify.setLatestEventInfo(context1, title, details, pending);
		int x = 0;
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(1000);
		x = randomInt;
		nm.notify(x, notify);

	}

	private Map<String, Object> prepareNotificationsParams(int pageNo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Notifications.USER_ID,
				String.valueOf(UserManager.getInstance().getCurrentUserId()));
		params.put(Params.Notifications.LAST_REQUEST_TIME,
				String.valueOf(getLastRequestTime()));
		params.put(Params.Notifications.PAGE_NO, String.valueOf(pageNo));
		return params;
	}

	/** check if there is Notification of Type (Studio Library) "8" */
	public ArrayList<Notification> HandleStudioNotificationIFExist(
			ArrayList<Notification> notifications) {
		for (int i = 0; i < notifications.size(); i++) {
			if (notifications.get(i).getNotificationType() == Notification.STUDIO_NOTIFICATIONS) {
				/** reset tables */
				// 1- reset seasons table
				if (SeasonsTable.getInstance().getCount() > 0)
					SeasonsTable.getInstance().deleteAll();

				// 2- reset Photos table
				if (StudioPhotosTable.getInstance().getCount() > 0)
					StudioPhotosTable.getInstance().deleteAll();

				// 3- reset Folders table
				if (StudioFoldersTable.getInstance().getCount() > 0)
					StudioFoldersTable.getInstance().deleteAll();

				/** get Studio data from Server */
				if (NetworkingUtils.isNetworkConnected(App.getInstance()
						.getApplicationContext())) {
					// 4- get Folders from Server and cache it
					StudioManager.getInstance().getFoldersFromServer();

					// 5- get Seasons from Server and cache it
					StudioManager.getInstance().getSeasonsFromServer();

					// remove the notification from the list :)) no need for it
					// again
					notifications.remove(i);
					break;

				}

			}

		}
		return notifications;
	}

	private long getLastRequestTime() {
		long lastRequestTime_ = 0;

		lastRequestTime_ = (Long) SharedPrefrencesDataLayer.getLongPreferences(
				App.getInstance().getApplicationContext(),
				LAST_REQUEST_TIME_NOTIFICATIONS_KEY, 0);
		return lastRequestTime_;
	}

	public void saveLastRequestTime(long timeStamp) {
		lastRequestTime = timeStamp;
		SharedPrefrencesDataLayer.saveLongPreferences(App.getInstance()
				.getApplicationContext(), LAST_REQUEST_TIME_NOTIFICATIONS_KEY,
				lastRequestTime);
	}

	private void cachNotifications(List<Notification> notifications) {
		for (Notification notification : notifications) {
			if (NotificationsTable.getInstance().get(
					notification.getNotificationId()) == null) {
				NotificationsTable.getInstance().insert(notification);
			}
		}
		int notificationsCount = NotificationsTable.getInstance()
				.getUnreadNotificationsCount();
		BaseActivity.notificationsCount = notificationsCount;
		fireLocalNotification(notifications);
	}

	public List<Notification> getCachedFilteredNotifications(
			int notificationType) {
		return NotificationsTable.getInstance().getFilteredNotifications(
				notificationType);
	}

	public List<Notification> getCachedFavoriteNotifications() {
		return NotificationsTable.getInstance().getFavoriteNotifications();
	}

	public List<Notification> getFriendRequestsNotifications() {
		requestsList = /* (ArrayList<Notification>) getrequested(); */(ArrayList<Notification>) getCachedFilteredNotifications(Notification.FRIEND_REQUEST_NOTIFICATIONS);
		return requestsList;
	}

	public void setNotificationRead(int notificationId) {
		NotificationsTable.getInstance().setRead(notificationId);
	}
	
	public void setNotificationWithTypeRead(int notificationType) {
		NotificationsTable.getInstance().setNotificationRead(notificationType);
	}

	public void setFavTabReab(int id) {
		NotificationsTable.getInstance().setFavRead(id);
	}

	public List<Notification> getrequested() {
		List<Notification> nots = new ArrayList<Notification>();
		nots.add(new Notification(99, 1546, "Mohga", 711, "Osama", 1423481771,
				758, Notification.FRIEND_REQUEST_NOTIFICATIONS, "", false));
		nots.add(new Notification(100, 1547, "Mohga", 711, "Osama", 1423580553,
				759, Notification.FRIEND_REQUEST_NOTIFICATIONS, "", true));
		return nots;
	}

	// --------------------------friend Part------------------------------

	public void handleFriend(final int userId, final int fanId,
			final boolean friend) {
		// this.requestsList = requests;
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.FOLLOW, fanId);

			@Override
			public void run() {
				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performFriend(userId, fanId, friend);
						notifyActionDone(action);
					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnFriendRequestResponseListener.class);
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	protected void performFriend(int userId, final int senderId, boolean friend)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		parametrs.put(Params.Fans.USER_ID, userId);
		parametrs.put(Params.Fans.FAN_ID, senderId);
		if (friend)
			parametrs.put(Params.Fans.FRIEND, 1);
		else
			parametrs.put(Params.Fans.FRIEND, 0);

		final BaseResponse response = submit(parametrs,
				URLs.Methods.CHANGE_FRIEND);

		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					for (UiListener listener : getListeners()) {
						if (listener instanceof OnFriendRequestResponseListener) {
							((OnFriendRequestResponseListener) listener)
									.onSuccess(senderId);
						}
					}
				}
			});

		}

		if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
				&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else {
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));

			}
		}
	}

	/**
	 * get Post Details
	 * 
	 * @param postId
	 * */
	public void getPostDetails(final int postId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					executeGetPost(postId);
				} catch (AppException e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnPostReceived.class);
				}
			}
		});
	}

	private void executeGetPost(int postId) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Common.LANG, LanguageUtils.getInstance()
				.getDeviceLanguage());
		params.put(Params.Wall.POST_ID, postId);
		params.put(Params.Wall.USER_ID, UserManager.getInstance()
				.getCurrentUserId());

		WALLPosts post = getObject(params, URLs.Methods.GET_POST,
				WALLPosts.class);
		notifyEntityReceviedSuccess(post, OnPostReceived.class);
	}

}
