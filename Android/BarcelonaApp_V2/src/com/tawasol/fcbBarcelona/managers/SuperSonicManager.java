package com.tawasol.fcbBarcelona.managers;

import java.lang.reflect.Field;

import android.app.Activity;

import com.supersonic.adapters.supersonicads.SupersonicConfig;
import com.supersonic.mediationsdk.logger.SupersonicError;
import com.supersonic.mediationsdk.sdk.OfferwallListener;
import com.supersonic.mediationsdk.sdk.RewardedVideoListener;
import com.supersonic.mediationsdk.sdk.Supersonic;
import com.supersonic.mediationsdk.sdk.SupersonicFactory;
import com.tawasol.fcbBarcelona.R;

public class SuperSonicManager implements RewardedVideoListener,
		OfferwallListener {

	private static SuperSonicManager instance;
	public static boolean vedioInitFail;
	public static boolean wallOfferInitFail;

	public static SuperSonicManager getInstance() {
		if (instance == null)
			instance = new SuperSonicManager();
		return instance;
	}

	private Supersonic mMediationAgent;

	public Supersonic getSuperSonicAgent() {
		return mMediationAgent;
	}

	public void initSuperSonicAgent(Activity activity) {
		// Get the mediation publisher instance
		// mMediationAgent = new SupersonicObject();

		mMediationAgent = SupersonicFactory.getInstance();

		try {
			System.out.println(mMediationAgent.getClass().getDeclaredFields());
			Field f = mMediationAgent.getClass().getDeclaredField("mUserId");
			f.setAccessible(true);
			String value = (String) f.get(mMediationAgent);
			f.set(mMediationAgent , null);
		} catch (IllegalAccessException | IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Set the Rewarded Video Listener
		mMediationAgent.setRewardedVideoListener(this);
		String superSonicAppKey = activity
				.getString(R.string.super_sonic_app_id);
		String userId = String.valueOf(UserManager.getInstance()
				.getCurrentUserId());

		// Set the Offerwall Listener
		mMediationAgent.setOfferwallListener(this);

		// Init Rewarded Video
//		mMediationAgent.initRewardedVideo(activity, superSonicAppKey, userId);
		// You can set optional parameters as well via the .getConfigObj
		SupersonicConfig.getConfigObj().setClientSideCallbacks(true);

		// Init Offerwall
		mMediationAgent.initOfferwall(activity, superSonicAppKey, userId);
	}

	public void userLoggedOut(Activity activity) {
		mMediationAgent.release(activity);
		mMediationAgent = null;
	}

	@Override
	public void onGetOfferwallCreditsFail(SupersonicError arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOfferwallAdCredited(int arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onOfferwallClosed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOfferwallInitFail(SupersonicError arg0) {
		System.out.println(arg0.getErrorMessage());
		wallOfferInitFail = true;
	}

	@Override
	public void onOfferwallInitSuccess() {
		// TODO Auto-generated method stub
		System.out.println("S");
	}

	@Override
	public void onOfferwallOpened() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOfferwallShowFail(SupersonicError arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdClosed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdOpened() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdRewarded(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoInitFail(SupersonicError arg0) {
		System.out.println(arg0.getErrorMessage());
		vedioInitFail = true;
	}

	@Override
	public void onRewardedVideoInitSuccess() {
		// TODO Auto-generated method stub
		System.out.println("S");
	}

	@Override
	public void onVideoAvailabilityChanged(boolean arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onVideoEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onVideoStart() {
		// TODO Auto-generated method stub

	}

}
