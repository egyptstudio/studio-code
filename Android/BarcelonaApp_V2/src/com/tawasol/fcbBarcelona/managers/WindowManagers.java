package com.tawasol.fcbBarcelona.managers;

import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.business.BusinessManager;
import com.tawasol.fcbBarcelona.entities.WALLPosts;
import com.tawasol.fcbBarcelona.listeners.RequestHideTRansperntView;
import com.tawasol.fcbBarcelona.listeners.UiListener;

public class WindowManagers extends BusinessManager<WALLPosts> {

	protected WindowManagers() {
		super(WALLPosts.class);
		// TODO Auto-generated constructor stub
	}
	
	public static WindowManagers instance;
	
	public static WindowManagers getInstance(){
		if(instance == null)
			instance = new WindowManagers();
		return instance;
	}

	public void notifyRemoveTransperntWall(){
		App.getInstance().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				for(UiListener listener : getListeners()){
					((RequestHideTRansperntView)listener).removeTransperntView();
				}
			}
		});
	}
}
