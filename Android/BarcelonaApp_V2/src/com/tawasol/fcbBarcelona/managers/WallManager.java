package com.tawasol.fcbBarcelona.managers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.business.BusinessManager;
import com.tawasol.fcbBarcelona.business.BusinessManager.Action.ActionType;
import com.tawasol.fcbBarcelona.data.cache.CommentTable;
import com.tawasol.fcbBarcelona.data.cache.InternalFileSaveDataLayer;
import com.tawasol.fcbBarcelona.data.cache.LikeTable;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.data.cache.WallTable;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.data.connection.URLs;
import com.tawasol.fcbBarcelona.data.helper.DataHelper;
import com.tawasol.fcbBarcelona.entities.Country;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.entities.KeyValuePairs;
import com.tawasol.fcbBarcelona.entities.PostLike;
import com.tawasol.fcbBarcelona.entities.PostViewModel;
import com.tawasol.fcbBarcelona.entities.StudioPhoto;
import com.tawasol.fcbBarcelona.entities.SuggestObject;
import com.tawasol.fcbBarcelona.entities.TagsData;
import com.tawasol.fcbBarcelona.entities.TopTenModel;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.entities.UserComment;
import com.tawasol.fcbBarcelona.entities.UserContacts;
import com.tawasol.fcbBarcelona.entities.WALLPosts;
import com.tawasol.fcbBarcelona.entities.WebServiceRequestInfo;
import com.tawasol.fcbBarcelona.entities.WhoInstalledAppObject;
import com.tawasol.fcbBarcelona.exception.AppException;
import com.tawasol.fcbBarcelona.listeners.MyPicsEmptyList;
import com.tawasol.fcbBarcelona.listeners.OnCommentAdded;
import com.tawasol.fcbBarcelona.listeners.OnCommentDeleted;
import com.tawasol.fcbBarcelona.listeners.OnCommentScreenFinish;
import com.tawasol.fcbBarcelona.listeners.OnCommentsListReceived;
import com.tawasol.fcbBarcelona.listeners.OnCountriesRecieved;
import com.tawasol.fcbBarcelona.listeners.OnEmailVerify;
import com.tawasol.fcbBarcelona.listeners.OnLikeRecieved;
import com.tawasol.fcbBarcelona.listeners.OnPostsRecieved;
import com.tawasol.fcbBarcelona.listeners.OnSeasonReceived;
import com.tawasol.fcbBarcelona.listeners.OnShareListener;
import com.tawasol.fcbBarcelona.listeners.OnStudioPhotoRecievedForUse;
import com.tawasol.fcbBarcelona.listeners.OnSuccessVoidListener;
import com.tawasol.fcbBarcelona.listeners.OnTagsDataReceived;
import com.tawasol.fcbBarcelona.listeners.OnUpdateFinished;
import com.tawasol.fcbBarcelona.listeners.PostFullScreenListener;
import com.tawasol.fcbBarcelona.listeners.UiListener;
import com.tawasol.fcbBarcelona.responses.BaseResponse;
import com.tawasol.fcbBarcelona.ui.activities.BaseActivity;
import com.tawasol.fcbBarcelona.ui.activities.ShopActivityTabs;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.NetworkingUtils;
import com.tawasol.fcbBarcelona.utils.UIUtils;

public class WallManager extends BusinessManager<WALLPosts> {

	Context context;

	public static final int LIKE_POST = 0;
	public static final int DISLIKE_POST = 1;
	public static final int DELETE_COMMENT = 2;
	public static final int ADD_COMMENT = 3;

	public static final int TYPE_LATEST = -1;
	public static final int TYPE_TOPTEN = -2;
	public static final int TYPE_WALL = -3;
	public static final int TYPE_MYPICS = -4;

	public static final int GET_TAGS = 1;
	public static final int GET_POSTS = 2;
	public static final int GET_USER_POSTS = 3;
	public static final int NUM_OF_POSTS = 10;

	private static final String PHOTO_DETAILS_INITIAL_STATE_KEY = "com.tawasol.barcelona.managers.photo_details_init_state";
	public static final String LATEST_CURRENT_TIME = "latestcurrentrquestTime";
	public static final String WALL_CURRENT_TIME = "wallcurrentrquestTime";
	public static final String MYPICS_CURRENT_TIME = "mypicscurrentrquestTime";
	public static final String TAGLIST_CURRENT_TIME = "taglistcurrentrquestTime";
	public static final String INITIAL_STATE_HOME = "initialState";

	List<PostViewModel> Recievedposts = null;
	// public static PostViewModel EMPTY_POST = new PostViewModel(-1);
	List<PostViewModel> latestPosts = new ArrayList<PostViewModel>();
	List<PostViewModel> wallPosts = new ArrayList<PostViewModel>();
	List<PostViewModel> myPicsPosts = new ArrayList<PostViewModel>();
	List<TopTenModel> topTenSessons = new ArrayList<TopTenModel>();
	// List<PostViewModel> topTen = new ArrayList<PostViewModel>();
	List<PostViewModel> filterList = new ArrayList<PostViewModel>();
	// List<PostViewModel> postFullScreen = new ArrayList<PostViewModel>();
	List<PostViewModel> tagPosts = new ArrayList<PostViewModel>();
	LinkedHashMap<Integer, List<PostViewModel>> fanDataStorage = new LinkedHashMap<Integer, List<PostViewModel>>();
	private boolean filtered;
	private List<Integer> countries;
	private boolean filterUsingFavo;
	static LinearLayout list;
	private static int WHICH_TAB;
	private static boolean isLatestDataCached;
	private static boolean isTopTenDataCached;
	private static boolean isWallDataCached;
	private static boolean isMyPicsDataCached;
	public static PostViewModel adPost = new PostViewModel(-2);
	public static boolean isCachedData;

	public void clearAllData() {
		latestPosts.clear();
		wallPosts.clear();
		myPicsPosts.clear();
		topTenSessons.clear();
		filterList.clear();
		tagPosts.clear();
		fanDataStorage.clear();
	}

	public static boolean isLatestDataCached() {
		return isLatestDataCached;
	}

	public static void setLatestDataCached(boolean isLatestDataCached) {
		WallManager.isLatestDataCached = isLatestDataCached;
	}

	public static boolean isTopTenDataCached() {
		return isTopTenDataCached;
	}

	public static void setTopTenDataCached(boolean isTopTenDataCached) {
		WallManager.isTopTenDataCached = isTopTenDataCached;
	}

	public static boolean isWallDataCached() {
		return isWallDataCached;
	}

	public static void setWallDataCached(boolean isWallDataCached) {
		WallManager.isWallDataCached = isWallDataCached;
	}

	public static boolean isMyPicsDataCached() {
		return isMyPicsDataCached;
	}

	public static void setMyPicsDataCached(boolean isMyPicsDataCached) {
		WallManager.isMyPicsDataCached = isMyPicsDataCached;
	}

	public static int getWHICH_TAB() {
		return WHICH_TAB;
	}

	public static void setWHICH_TAB(int wHICH_TAB) {
		WHICH_TAB = wHICH_TAB;
	}

	public void clearFiltered() {
		filterList.clear();
	}

	/**
	 * @param clazz
	 */
	protected WallManager() {
		super(WALLPosts.class);
		context = App.getInstance().getApplicationContext();
	}

	public static WallManager instance;

	public LinkedHashMap<Integer, List<PostViewModel>> getStorage() {
		return fanDataStorage;
	}

	public List<Integer> getCountriesList() {
		return countries;
	}

	public boolean isFiltered() {
		return filtered;
	}

	public static WallManager getInstance() {
		if (instance == null)
			instance = new WallManager();
		return instance;
	}

	public void setFilterWithFavo(boolean isFavo) {
		filterUsingFavo = isFavo;
	}

	public boolean isFilteredUsingFav() {
		return filterUsingFavo;
	}

	public List<PostViewModel> getLatest() {
		return latestPosts;
	}

	public List<PostViewModel> getWall() {
		return wallPosts;
	}

	public List<PostViewModel> getMyPics() {
		return myPicsPosts;
	}

	// public List<PostViewModel> getTopTen() {
	// return topTen;
	// }

	public List<PostViewModel> getfilteredList() {
		return filterList;
	}

	public List<TopTenModel> getTopTenSessons() {
		return topTenSessons;
	}

	public void getTagPosts(final String tagName,
			final WebServiceRequestInfo info, final boolean isTag) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					List<PostViewModel> posts = getTagPostsFromServer(tagName,
							info, isTag);
					notifyEntityListReceviedSuccess(posts,
							OnPostsRecieved.class);
					saveTagList(posts, info.getAction());
					notifyUpdateFinished();
				} catch (Exception e) {
					notifyUpdateFinished();
					notifyRetrievalException(AppException.getAppException(e),
							OnPostsRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	protected void saveTagList(final List<PostViewModel> posts,
			final int requestType) {
		if (requestType == WebServiceRequestInfo.ACTION_FIRST_TIME) {
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					tagPosts.addAll(posts);
				}
			});
		}

		else if (requestType == WebServiceRequestInfo.ACTION_UPDATE) {

			if (posts.size() > NUM_OF_POSTS) {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						tagPosts.clear();
						tagPosts.addAll(posts);
					}
				});

			} else if (posts.size() < NUM_OF_POSTS && posts.size() > 0) {

				List<PostViewModel> copyOfHomePosts = new ArrayList<PostViewModel>();
				copyOfHomePosts.addAll(tagPosts);

				final List<PostViewModel> afterUpdate = handleLatestData(posts,
						copyOfHomePosts);

				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						tagPosts.clear();
						tagPosts.addAll(afterUpdate);
					}
				});
			}
		}
	}

	public List<PostViewModel> getTagPostsFromServer(String tagName,
			WebServiceRequestInfo info, boolean isTag) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		if (isTag) {
			params.put(Params.Wall.TAG_Name, tagName);
			params.put(Params.Wall.NUM_OF_POSTS, info.getN());
			params.put(Params.Wall.LAST_REQUEST_NUM, info.getLasrRequestTime());
			params.put(Params.Wall.TIME_FILTER, info.getTimeFilter());
			params.put(Params.Wall.USER_ID, info.getUserId());
		} else {
			params.put(Params.Wall.USER_ID, info.getUserId());
			params.put(Params.Wall.FAN_ID, info.getFanId());
			params.put(Params.Wall.NUM_OF_POSTS, info.getN());
			params.put(Params.Wall.LAST_REQUEST_NUM, info.getLasrRequestTime());
			params.put(Params.Wall.TIME_FILTER, info.getTimeFilter());
		}

		WALLPosts post = getObject(params, info.getMethodName(),
				WALLPosts.class);
		if (post.getPosts().isEmpty())
			throw new AppException(AppException.NO_DATA_EXCEPTION);
		saveTagListCurrentTime(post.getCurrentRequestTime());
		return post.getPosts();
	}

	// public List<PostViewModel> getPostFullScreen(int whichTab) {
	// postFullScreen.clear();
	// switch (whichTab) {
	// case FragmentInfo.LATEST_TAB:
	// postFullScreen.addAll(latestPosts);
	// break;
	// case FragmentInfo.MY_PIC:
	// postFullScreen.addAll(myPicsPosts);
	// break;
	// case FragmentInfo.TOP_TEN:
	// for (TopTenModel season : topTenSessons)
	// postFullScreen.addAll(season.getPosts());
	// break;
	// case FragmentInfo.WALL:
	// postFullScreen.addAll(wallPosts);
	// break;
	// case FragmentInfo.Filtered:
	// postFullScreen.addAll(filterList);
	// break;
	// default:
	// break;
	// }
	//
	// return postFullScreen;
	// }

	// public void addToPostFullScreen(final List<PostViewModel> posts,
	// int requestType) {
	// if (requestType == WebServiceRequestInfo.ACTION_FIRST_TIME) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// postFullScreen.addAll(posts);
	// }
	// });
	// }
	//
	// else if (requestType == WebServiceRequestInfo.ACTION_UPDATE) {
	//
	// if (posts.size() > NUM_OF_POSTS) {
	// postFullScreen.clear();
	// postFullScreen.addAll(posts);
	//
	// } else if (posts.size() < NUM_OF_POSTS && posts.size() > 0) {
	//
	// List<PostViewModel> copyOfHomePosts = new ArrayList<PostViewModel>();
	// copyOfHomePosts.addAll(postFullScreen);
	//
	// final List<PostViewModel> afterUpdate = handleLatestData(posts,
	// copyOfHomePosts);
	//
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// postFullScreen.clear();
	// postFullScreen.addAll(afterUpdate);
	// }
	// });
	// }
	// }
	// }

	public WALLPosts getPosts(WebServiceRequestInfo info) throws AppException {
		WALLPosts post = getPostsFromServer(info);
		addToStorage(post.getPosts());
		return post;
	}

	List<TopTenModel> seasons = new ArrayList<TopTenModel>();

	public void getTopTen(final int userId, final String methodName,
			final int refresh) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put(Params.User.USER_ID, userId);
				params.put(Params.Wall.REFRESH, refresh);
				seasons.clear();
				try {
					if (refresh == 0) {
						seasons = getObjectList(methodName, params,
								TopTenModel.class);
						topTenSessons.addAll(seasons);
						// final List<String> seassons = new
						// ArrayList<String>();
						for (TopTenModel sesson : seasons) {
							addToStorage(sesson.getPosts());
							// seassons.add(sesson.getSeasonName());
							// topTen.addAll(sesson.getPosts());
						}
						// seassons.add("1");
						// seassons.add("2");
						// seassons.add("3");
						// for(int x = 0 ; x < 30 ; x ++){
						// topTen.add(seasons.get(0).getPosts().get(0));
						// }
						// App.getInstance().runOnUiThread(new Runnable() {
						//
						// @Override
						// public void run() {
						// for(UiListener listener : getListeners()){
						// if(listener instanceof OnTopTenRecieved)
						// ((OnTopTenRecieved)listener).onTopTenRecieved(seassons,
						// topTen);
						// }
						// }
						// });
						notifyEntityListReceviedSuccess(topTenSessons,
								OnSeasonReceived.class);
					} else {
						App.getInstance().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								if (!seasons.isEmpty()) {
									topTenSessons.clear();
									topTenSessons.addAll(seasons);
								}
								notifyUpdateFinished();
							}
						});
					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnSeasonReceived.class);
					e.printStackTrace();
				}
			}
		});

	}

	public void handlePosts(final WebServiceRequestInfo requestInfo,
			final int postType, LinearLayout list) {
		if (list != null)
			WallManager.list = list;
		final Action action = new Action(ActionType.GET_POSTS, requestInfo);
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				if (requestInfo.getAction() == WebServiceRequestInfo.ACTION_FIRST_TIME) {
					switch (postType) {
					case FragmentInfo.LATEST_TAB:
						final List<PostViewModel> posts = WallTable
								.getInstance().getBySelection(
										WallTable.Fields.LATEST_POST + "=1",
										null);
						App.getInstance().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								latestPosts.addAll(posts);
								setLatestDataCached(true);
								if (!latestPosts.isEmpty()) {
									Collections.sort(latestPosts);
									Collections.reverse(latestPosts);
									notifyEntityListReceviedSuccess(
											latestPosts, OnPostsRecieved.class);
								}
							}
						});
						break;
					case FragmentInfo.WALL:
						final List<PostViewModel> wallPosts = WallTable
								.getInstance()
								.getBySelection(
										WallTable.Fields.WALL_POST + "=1", null);
						App.getInstance().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								wallPosts.addAll(wallPosts);
								setWallDataCached(true);
								if (!wallPosts.isEmpty()) {
									Collections.sort(wallPosts);
									Collections.reverse(wallPosts);
									notifyEntityListReceviedSuccess(wallPosts,
											OnPostsRecieved.class);
								}
							}
						});
						break;
					case FragmentInfo.MY_PIC:
						final List<PostViewModel> MyPicsPosts = WallTable
								.getInstance().getBySelection(
										WallTable.Fields.MyPicsPost + "=1",
										null);
						App.getInstance().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								myPicsPosts.addAll(MyPicsPosts);
								setMyPicsDataCached(true);
								if (!myPicsPosts.isEmpty()) {
									Collections.sort(myPicsPosts);
									Collections.reverse(myPicsPosts);
									notifyEntityListReceviedSuccess(
											myPicsPosts, OnPostsRecieved.class);
								}
							}
						});
						break;
					default:
						break;
					}
				}

				try {
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						Recievedposts = getPosts(requestInfo, postType);
						notifyActionDone(action);
					}
					// if (requestInfo.getAction() ==
					// WebServiceRequestInfo.ACTION_FIRST_TIME) {
					// switch (postType) {
					// case FragmentInfo.LATEST_TAB:
					// // notifyEntityListReceviedSuccess(latestPosts,
					// // OnPostsRecieved.class);
					// break;
					// case FragmentInfo.WALL:
					// // notifyEntityListReceviedSuccess(wallPosts,
					// // OnPostsRecieved.class);
					// break;
					// case FragmentInfo.MY_PIC:
					// // notifyEntityListReceviedSuccess(myPicsPosts,
					// // OnPostsRecieved.class);
					// break;
					// case FragmentInfo.Filtered:
					// notifyEntityListReceviedSuccess(filterList,
					// OnPostsRecieved.class);
					// break;
					// default:
					// break;
					// }
					// }
					// notifyUpdateFinished();
				} catch (Exception e) {
					notifyActionDone(action);
					if (AppException.getAppException(e).getErrorCode() != AppException.NETWORK_EXCEPTION
							&& AppException.getAppException(e).getErrorCode() != AppException.UNKNOWN_EXCEPTION)
						notifyUpdateFinished();
					notifyRetrievalException(AppException.getAppException(e),
							OnPostsRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	@SuppressLint("NewApi")
	public List<PostViewModel> getPosts(WebServiceRequestInfo requestInfo,
			int postType) throws AppException {
		WALLPosts post = getPostsFromServer(requestInfo);
		addToStorage(post.getPosts());

		new SafeGuard(postType, post.getPosts(), requestInfo.getAction())
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		if (requestInfo.getAction() == WebServiceRequestInfo.ACTION_FIRST_TIME
				|| requestInfo.getAction() == WebServiceRequestInfo.ACTION_UPDATE) {
			switch (postType) {
			case FragmentInfo.LATEST_TAB:
				saveLatestCurrentTime(post.getCurrentRequestTime());
				break;
			case FragmentInfo.WALL:
				saveWallCurrentTime(post.getCurrentRequestTime());
				break;
			case FragmentInfo.MY_PIC:
				saveMyPicsCurrentTime(post.getCurrentRequestTime());
				break;
			default:
				break;
			}
		}
		WindowManagers.getInstance().notifyRemoveTransperntWall();
		return post.getPosts();
	}

	/**
	 * @param object
	 * @param chooseFavo
	 */
	public void applyFilter(List<Integer> countries, boolean chooseFavo,
			FragmentInfo info) {
		filtered = true;
		this.countries = countries;
		long lastUpdateTime = 0;
		if (info.getType() == FragmentInfo.LATEST_TAB)
			lastUpdateTime = latestPosts.get(latestPosts.size() - 1)
					.getLastUpdateTime();
		else if (info.getType() == FragmentInfo.WALL)
			lastUpdateTime = wallPosts.get(wallPosts.size() - 1)
					.getLastUpdateTime();
		WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
				FragmentInfo.ACTION_GETMORE, UserManager.getInstance()
						.getCurrentUserId(), WallManager.NUM_OF_POSTS,
				lastUpdateTime, WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
				WebServiceRequestInfo.GET_POSTS,
				FragmentInfo.getMethodName(info), countries,
				chooseFavo ? WebServiceRequestInfo.TURN_ON_FILTER
						: WebServiceRequestInfo.TURN_OFF_FILTER);

		WallManager.getInstance().handlePosts(requestInfo,
				FragmentInfo.Filtered, list);
	}

	/**
	 * @param posts
	 */
	private void addToStorage(List<PostViewModel> posts) {
		for (int x = 0; x < posts.size(); x++) {
			if (!fanDataStorage.containsKey(posts.get(x).getUserId())) {
				List<PostViewModel> postsToSave = new ArrayList<PostViewModel>();
				// postsToSave.add(0, EMPTY_POST);
				postsToSave.add(posts.get(x));
				// postsToSave.add(EMPTY_POST);
				fanDataStorage.put(posts.get(x).getUserId(), postsToSave);
			} else {
//				List<PostViewModel> poststoRemove = new ArrayList<>();
				List<PostViewModel> postsForFan = getPostsForFan(posts.get(x)
						.getUserId());
				// postsForFan.remove(postsForFan.size() - 1);
				for (int j = 0; j < postsForFan.size(); j++) {
					if (posts.get(x).getPostId() == postsForFan.get(j)
							.getPostId()) {
//						poststoRemove.add(postsForFan.get(j));
						postsForFan.remove(j);
						break;
					}
				}
//				postsForFan.removeAll(poststoRemove);
				postsForFan.add(posts.get(x));
				Collections.sort(postsForFan);
				// postsForFan.add(EMPTY_POST);
//				Collections.reverse(postsForFan);
				fanDataStorage.put(posts.get(x).getUserId(), postsForFan);
			}
		}
	}

	/**
	 * @param fan
	 * @return
	 */
	public List<PostViewModel> getPostsForFan(int userId) {
		List<PostViewModel> posts = new ArrayList<PostViewModel>();
		for (Map.Entry<Integer, List<PostViewModel>> value : fanDataStorage
				.entrySet()) {
			if (userId == value.getKey())
//				for(int x = 0 ; x < value.getValue().size() ; x++){
//					if(value.getValue().get(x).getOriginalPostId() == 0)
//						posts.add(value.getValue().get(x));
//				}
				posts.addAll(value.getValue());
				
		}
		return posts;
	}

	private void saveTagListCurrentTime(final long currentRequestTime) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(context,
							TAGLIST_CURRENT_TIME, currentRequestTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public long getTagListCurrentTime() {
		long currentTime = 0;
		try {
			currentTime = (Long) InternalFileSaveDataLayer.getObject(context,
					TAGLIST_CURRENT_TIME);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return currentTime;
	}

	private void saveMyPicsCurrentTime(final long currentRequestTime) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(context,
							MYPICS_CURRENT_TIME, currentRequestTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public long getMyPicsCurrentTime() {
		long currentTime = 0;
		try {
			currentTime = (Long) InternalFileSaveDataLayer.getObject(context,
					MYPICS_CURRENT_TIME);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return currentTime;
	}

	private void saveWallCurrentTime(final long currentRequestTime) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(context,
							WALL_CURRENT_TIME, currentRequestTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public long getWallCurrentTime() {
		long currentTime = 0;
		try {
			currentTime = (Long) InternalFileSaveDataLayer.getObject(context,
					WALL_CURRENT_TIME);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return currentTime;
	}

	private void saveLatestCurrentTime(final long currentRequestTime) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(context,
							LATEST_CURRENT_TIME, currentRequestTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public long getLatestCurrentTime() {
		long currentTime = 0;
		try {
			currentTime = (Long) InternalFileSaveDataLayer.getObject(context,
					LATEST_CURRENT_TIME);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return currentTime;
	}

	public void clearTimes() {
		InternalFileSaveDataLayer.deleteObjectFile(context.getFilesDir() + "/"
				+ LATEST_CURRENT_TIME);
	}

	// -----------------------------filterList-----------------------------
	private void saveFiltered(final List<PostViewModel> posts, int requestType) {
		if (requestType == WebServiceRequestInfo.ACTION_GETMORE
				|| requestType == WebServiceRequestInfo.ACTION_GETMORE) {
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					filterList.addAll(posts);
					notifyEntityListReceviedSuccess(filterList,
							OnPostsRecieved.class);
				}
			});
		}

		else if (requestType == WebServiceRequestInfo.ACTION_UPDATE) {
			if (posts.size() > NUM_OF_POSTS) {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						filterList.clear();
						filterList.addAll(posts);
						notifyUpdateFinished();
					}
				});
			} else if (posts.size() < NUM_OF_POSTS && posts.size() > 0) {
				List<PostViewModel> copyOfHomePosts = new ArrayList<PostViewModel>();
				copyOfHomePosts.addAll(filterList);
				final List<PostViewModel> afterUpdate = handleFilterData(posts,
						copyOfHomePosts);
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						filterList.clear();
						filterList.addAll(afterUpdate);
						notifyUpdateFinished();
					}
				});
			} else {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						list.setVisibility(View.GONE);
						notifyUpdateFinished();
					}
				});
			}
		}
	}

	private List<PostViewModel> handleFilterData(List<PostViewModel> posts,
			List<PostViewModel> copyOfHomePosts) {
		List<PostViewModel> temp = new ArrayList<PostViewModel>();
		for (int x = 0; x < posts.size(); x++) {
			for (int j = 0; j < filterList.size(); j++) {
				if (posts.get(x).getPostId() == copyOfHomePosts.get(j)
						.getPostId()) {
					temp.add(copyOfHomePosts.get(j));
				}
			}
			copyOfHomePosts.removeAll(temp);
			copyOfHomePosts.addAll(0, posts);
		}
		return copyOfHomePosts;
	}

	// -------------------------------endoffilter------------------------

	// -----------------------------latestList-----------------------------

	public void enableDisableView(View view, boolean enabled) {
		view.setEnabled(enabled);

		if (view instanceof ViewGroup) {
			ViewGroup group = (ViewGroup) view;

			for (int idx = 0; idx < group.getChildCount(); idx++) {
				enableDisableView(group.getChildAt(idx), enabled);
			}
		}
	}

	private class SafeGuard extends AsyncTask<Void, Void, Void> {

		List<PostViewModel> posts;
		int requestType;
		int postType;

		public SafeGuard(int postType, List<PostViewModel> posts,
				int requestType) {
			this.postType = postType;
			this.posts = posts;
			this.requestType = requestType;
		}

		@Override
		protected void onPreExecute() {
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (list != null)
						list.setVisibility(View.VISIBLE);
				}
			});
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
//			for(int x = 0 ; x < posts.size() ; x++){
//				if((x + 1) % 6 == 0){
//					posts.add(x, adPost);
//				}
//			}
//			if((posts.size() + 1 ) % 6 == 0){
//				posts.add(adPost);
//			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			switch (postType) {
			case FragmentInfo.LATEST_TAB:
				saveLatest(posts, requestType);
				break;
			case FragmentInfo.WALL:
				saveWall(posts, requestType);
				break;
			case FragmentInfo.MY_PIC:
				saveMyPics(posts, requestType);
				break;
			case FragmentInfo.Filtered:
				saveFiltered(posts, requestType);
				break;
			default:
				break;
			}

			super.onPostExecute(result);
		}

	}

	private void saveLatest(final List<PostViewModel> posts, int requestType) {
		if (requestType == WebServiceRequestInfo.ACTION_FIRST_TIME) {
//			saveToDataBase(posts, FragmentInfo.LATEST_TAB);
			// new SafeGuard(posts).execute();
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					latestPosts.clear();
					latestPosts.addAll(posts);
					setLatestDataCached(false);
					notifyEntityListReceviedSuccess(latestPosts,
							OnPostsRecieved.class);
				}
			});
		}

		else if (requestType == WebServiceRequestInfo.ACTION_UPDATE) {

			if (posts.size() >= NUM_OF_POSTS) {

				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						latestPosts.clear();
						latestPosts.addAll(posts);
						list.setVisibility(View.GONE);
						notifyUpdateFinished();
					}
				});
				new DeleteFromDataBaseAsync(latestPosts,
						FragmentInfo.LATEST_TAB).execute();
				// clearPostsfromDataBase(posts, FragmentInfo.LATEST_TAB);
			} else if (posts.size() < NUM_OF_POSTS && posts.size() > 0) {

				List<PostViewModel> copyOfHomePosts = new ArrayList<PostViewModel>();
				copyOfHomePosts.addAll(latestPosts);

				final List<PostViewModel> afterUpdate = handleLatestData(posts,
						copyOfHomePosts);
				new DeleteFromDataBaseAsync(afterUpdate,
						FragmentInfo.LATEST_TAB).execute();
				// clearPostsfromDataBase(afterUpdate, FragmentInfo.LATEST_TAB);
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						latestPosts.clear();
						latestPosts.addAll(afterUpdate);
						list.setVisibility(View.GONE);
						notifyUpdateFinished();
					}
				});
			} else {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						list.setVisibility(View.GONE);
						notifyUpdateFinished();
					}
				});
			}
		}
	}

	// private void clearPostsfromDataBase(final List<PostViewModel> posts,
	// final int whichTab) {
	// new Thread(new Runnable() {
	//
	// @Override
	// public void run() {
	// switch (whichTab) {
	// case FragmentInfo.LATEST_TAB:
	// WallTable.getInstance().delete(
	// WallTable.Fields.LATEST_POST + "=1", null);
	// saveToDataBase(posts, FragmentInfo.LATEST_TAB);
	// break;
	// case FragmentInfo.WALL:
	// WallTable.getInstance().delete(
	// WallTable.Fields.WALL_POST + "=1", null);
	// saveToDataBase(posts, FragmentInfo.WALL);
	// break;
	// case FragmentInfo.MY_PIC:
	// WallTable.getInstance().delete(
	// WallTable.Fields.MyPicsPost + "=1", null);
	// saveToDataBase(posts, FragmentInfo.MY_PIC);
	// break;
	// default:
	// break;
	// }
	// }
	// }).start();
	// }

	public void saveToDataBase(final List<PostViewModel> posts,
			final int whichTab) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				switch (whichTab) {
				case FragmentInfo.LATEST_TAB:
					for (PostViewModel post : posts) {
						post.setLatest(true);
						WallTable.getInstance().insert(post);
					}
					break;
				case FragmentInfo.WALL:
					for (PostViewModel post : posts) {
						post.setWall(true);
						WallTable.getInstance().insert(post);
					}
					break;
				case FragmentInfo.MY_PIC:
					for (PostViewModel post : posts) {
						post.setMyPics(true);
						WallTable.getInstance().insert(post);
					}
					break;
				default:
					break;
				}
			}
		}).start();
	}

	private class DeleteFromDataBaseAsync extends AsyncTask<Void, Void, Void> {

		List<PostViewModel> posts;
		int whichTab;

		public DeleteFromDataBaseAsync(List<PostViewModel> posts, int whichTab) {
			this.posts = posts;
			this.whichTab = whichTab;
		}

		@Override
		protected Void doInBackground(Void... params) {
			switch (whichTab) {
			case FragmentInfo.LATEST_TAB:
				WallTable.getInstance().delete(
						WallTable.Fields.LATEST_POST + "=1", null);
				break;
			case FragmentInfo.WALL:
				WallTable.getInstance().delete(
						WallTable.Fields.WALL_POST + "=1", null);
				break;
			case FragmentInfo.MY_PIC:
				WallTable.getInstance().delete(
						WallTable.Fields.MyPicsPost + "=1", null);
				break;
			default:
				break;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					saveToDataBase(posts, whichTab);
				}
			}).start();
			super.onPostExecute(result);
		}
	}

	private List<PostViewModel> handleLatestData(List<PostViewModel> posts,
			List<PostViewModel> copyOfHomePosts) {
		List<PostViewModel> temp = new ArrayList<PostViewModel>();
		for (int x = 0; x < posts.size(); x++) {
			for (int j = 0; j < latestPosts.size(); j++) {
				if (posts.get(x).getPostId() == copyOfHomePosts.get(j)
						.getPostId()) {
					temp.add(copyOfHomePosts.get(j));
				}
			}
		}
		copyOfHomePosts.removeAll(temp);
		copyOfHomePosts.addAll(0, posts);
		return copyOfHomePosts;
	}

	// -------------------------------endOfLatest------------------------

	// -----------------------------WalltList-----------------------------
	private void saveWall(final List<PostViewModel> posts, int requestType) {
		if (requestType != WebServiceRequestInfo.ACTION_UPDATE)
//			saveToDataBase(posts, FragmentInfo.WALL);
		if (requestType == WebServiceRequestInfo.ACTION_FIRST_TIME) {
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					wallPosts.clear();
					wallPosts.addAll(posts);
					setWallDataCached(false);
					notifyEntityListReceviedSuccess(wallPosts,
							OnPostsRecieved.class);
				}
			});
		}

		else if (requestType == WebServiceRequestInfo.ACTION_UPDATE) {
			if (posts.size() > NUM_OF_POSTS) {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						wallPosts.clear();
						wallPosts.addAll(posts);
						notifyUpdateFinished();
					}
				});
				new DeleteFromDataBaseAsync(wallPosts, FragmentInfo.WALL)
						.execute();
				// clearPostsfromDataBase(posts, FragmentInfo.WALL);
			} else if (posts.size() < NUM_OF_POSTS && posts.size() > 0) {
				List<PostViewModel> copyOfHomePosts = new ArrayList<PostViewModel>();
				copyOfHomePosts.addAll(wallPosts);
				final List<PostViewModel> afterUpdate = handleWallData(posts,
						copyOfHomePosts);
				new DeleteFromDataBaseAsync(afterUpdate, FragmentInfo.WALL)
						.execute();
				// clearPostsfromDataBase(afterUpdate, FragmentInfo.WALL);
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						wallPosts.clear();
						wallPosts.addAll(afterUpdate);
						notifyUpdateFinished();
					}
				});
			} else {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						list.setVisibility(View.GONE);
						notifyUpdateFinished();
					}
				});
			}
		}
	}

	private List<PostViewModel> handleWallData(List<PostViewModel> posts,
			List<PostViewModel> copyOfHomePosts) {
		List<PostViewModel> temp = new ArrayList<PostViewModel>();
		for (int x = 0; x < posts.size(); x++) {
			for (int j = 0; j < wallPosts.size(); j++) {
				if (posts.get(x).getPostId() == copyOfHomePosts.get(j)
						.getPostId()) {
					temp.add(copyOfHomePosts.get(j));
				}
			}
			copyOfHomePosts.removeAll(temp);
			copyOfHomePosts.addAll(0, posts);
		}
		return copyOfHomePosts;
	}

	// -------------------------------endOfWall------------------------

	// -----------------------------MYPICSList-----------------------------
	private void saveMyPics(final List<PostViewModel> posts, int requestType) {
		if (requestType != WebServiceRequestInfo.ACTION_UPDATE)
//			saveToDataBase(posts, FragmentInfo.MY_PIC);
		if (requestType == WebServiceRequestInfo.ACTION_FIRST_TIME) {
			App.getInstance().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					myPicsPosts.clear();
					myPicsPosts.addAll(posts);
					setMyPicsDataCached(false);
					notifyEntityListReceviedSuccess(myPicsPosts,
							OnPostsRecieved.class);
				}
			});
		}

		else if (requestType == WebServiceRequestInfo.ACTION_UPDATE) {
			if (posts.size() > NUM_OF_POSTS) {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						myPicsPosts.clear();
						myPicsPosts.addAll(posts);
						notifyUpdateFinished();
					}
				});
				new DeleteFromDataBaseAsync(myPicsPosts, FragmentInfo.MY_PIC)
						.execute();
				// clearPostsfromDataBase(posts, FragmentInfo.MY_PIC);
			} else if (posts.size() < NUM_OF_POSTS && posts.size() > 0) {
				List<PostViewModel> copyOfHomePosts = new ArrayList<PostViewModel>();
				copyOfHomePosts.addAll(myPicsPosts);
				final List<PostViewModel> afterUpdate = handleMYPICSData(posts,
						copyOfHomePosts);
				new DeleteFromDataBaseAsync(afterUpdate, FragmentInfo.MY_PIC)
						.execute();
				// clearPostsfromDataBase(afterUpdate, FragmentInfo.MY_PIC);
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						myPicsPosts.clear();
						myPicsPosts.addAll(afterUpdate);
						notifyUpdateFinished();
					}
				});
			} else {
				App.getInstance().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						list.setVisibility(View.GONE);
						notifyUpdateFinished();
					}
				});
			}
		}
	}

	private List<PostViewModel> handleMYPICSData(List<PostViewModel> posts,
			List<PostViewModel> copyOfHomePosts) {
		List<PostViewModel> temp = new ArrayList<PostViewModel>();
		for (int x = 0; x < posts.size(); x++) {
			for (int j = 0; j < myPicsPosts.size(); j++) {
				if (posts.get(x).getPostId() == copyOfHomePosts.get(j)
						.getPostId()) {
					temp.add(copyOfHomePosts.get(j));
				}
			}
			copyOfHomePosts.removeAll(temp);
			copyOfHomePosts.addAll(0, posts);
		}
		return copyOfHomePosts;
	}

	// -------------------------------endOfMYPICS------------------------

	/**
	 * @param methodName
	 * @param userId
	 * @param n
	 * @param lasrRequestTime
	 * @param timeFilter
	 */
	private WALLPosts getPostsFromServer(WebServiceRequestInfo info)
			throws AppException {
		WALLPosts posts = getObject(preparePostsParameters(info),
				info.getMethodName(), WALLPosts.class);
		return posts;
	}

	/**
	 * @param info
	 * @return
	 */
	private Map<String, Object> preparePostsParameters(
			WebServiceRequestInfo info) {
		Map<String, Object> params = new HashMap<String, Object>();
		if (info.getType() == GET_POSTS) {
			params.put(Params.Wall.USER_ID, info.getUserId());
			params.put(Params.Wall.NUM_OF_POSTS, info.getN());
			params.put(Params.Wall.LAST_REQUEST_NUM, info.getLasrRequestTime());
			params.put(Params.Wall.TIME_FILTER, info.getTimeFilter());
			if (info.getCountryList() != null
					&& !info.getCountryList().isEmpty())
				params.put(Params.Wall.COUNTRYLIST, info.getCountryList());
			if (info.getMethodName().equalsIgnoreCase("wall/getWall"))
				params.put(Params.Wall.FAVO_POST_FLAG,
						info.getFavoritePostsFlag());
			return params;
		} else if (info.getType() == GET_TAGS) {
			params.put(Params.Wall.TAG_ID, info.getTagId());
			params.put(Params.Wall.NUM_OF_POSTS, info.getN());
			params.put(Params.Wall.LAST_REQUEST_NUM, info.getLasrRequestTime());
			params.put(Params.Wall.TIME_FILTER, info.getTimeFilter());
			return params;
		} else if (info.getType() == GET_USER_POSTS) {
			params.put(Params.Wall.USER_ID, info.getUserId());
			params.put(Params.Wall.FAN_ID, info.getFanId());
			params.put(Params.Wall.NUM_OF_POSTS, info.getN());
			params.put(Params.Wall.LAST_REQUEST_NUM, info.getLasrRequestTime());
			params.put(Params.Wall.TIME_FILTER, info.getTimeFilter());
			return params;
		} else
			return null;
	}

	void notifyUpdateFinished() {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnUpdateFinished)
						((OnUpdateFinished) listener).onUpdateFinished();
				}
			}
		});
	}

	int groupPosition;
	int index = 0;
	PostViewModel post = null;

	public void handleLike(final int actionType, final int currentUserId,
			final int postId) {
		App.getInstance().runInBackground(new Runnable() {
			final Action action = new Action(ActionType.LIKE, postId);

			@Override
			public void run() {
				try {
					onLikeProcessSuccess(postId, actionType, false, false);
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performLike(actionType, currentUserId, postId);
						onLikeProcessSuccess(postId, actionType, false, true);
					}
				} catch (Exception e) {
					onLikeProcessSuccess(postId, actionType, true, false);
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @param type
	 */
	private void performLike(int type, int userId, int postId)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Wall.USER_ID, userId);
		parametrs.put(Params.Wall.POST_ID, postId);

		if (type == LIKE_POST) {
			BaseResponse response = submit(parametrs, URLs.Methods.LIKE_POST);
			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
					throw new AppException(
							response.getValidationRule().errorMessage);
				else
					throw new AppException(
							BaseResponse.getExceptionType(response.getStatus()));
			}
		} else if (type == DISLIKE_POST) {
			BaseResponse response = submit(parametrs, URLs.Methods.DISLIKE_POST);
			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
					throw new AppException(
							response.getValidationRule().errorMessage);
				else
					throw new AppException(
							BaseResponse.getExceptionType(response.getStatus()));
			}
		}
	}

	/**
	 * @param e
	 */
	protected void onLikeProcessFailed(AppException e) {

	}

	/**
	 * 
	 */
	protected void onLikeProcessSuccess(int postId, int type,
			boolean isException, boolean finish) {
		if (!filterList.isEmpty())
			changeListsLikeProcess(type, postId, FragmentInfo.Filtered,
					isException, finish);
		if (!latestPosts.isEmpty())
			changeListsLikeProcess(type, postId, FragmentInfo.LATEST_TAB,
					isException, finish);
		if (!wallPosts.isEmpty())
			changeListsLikeProcess(type, postId, FragmentInfo.WALL,
					isException, finish);
		if (!myPicsPosts.isEmpty())
			changeListsLikeProcess(type, postId, FragmentInfo.MY_PIC,
					isException, finish);
		if (!topTenSessons.isEmpty()) {
			changeListsLikeProcess(type, postId, FragmentInfo.TOP_TEN,
					isException, finish);
		}
		// if (!postFullScreen.isEmpty()) {
		// changeListsLikeProcess(type, postId, FragmentInfo.FULL_SCREEN,
		// isException, finish);
		// }
		if (!tagPosts.isEmpty())
			changeListsLikeProcess(type, postId, FragmentInfo.TAG_LIST,
					isException, finish);
	}

	private void changeListsLikeProcess(final int type, int postId,
			final int whichTab, boolean isException, boolean finish) {
		post = null;
		PostViewModel markedPost = new PostViewModel(postId);
		switch (whichTab) {
		case FragmentInfo.LATEST_TAB:
			index = latestPosts.indexOf(markedPost);
			if (index >= 0)
				post = latestPosts.get(index);
			break;
		case FragmentInfo.TOP_TEN:
			mainloop: for (int x = 0; x < topTenSessons.size(); x++) {
				for (int j = 0; j < topTenSessons.get(x).getPosts().size(); j++) {
					if (topTenSessons.get(x).getPosts().get(j)
							.equals(markedPost)) {
						groupPosition = x;
						index = j;
						if (index >= 0)
							post = topTenSessons.get(x).getPosts().get(j);
						break mainloop;
					}
				}
			}

			// index = topTen.indexOf(markedPost);
			// post = topTen.get(index);
			break;
		case FragmentInfo.MY_PIC:
			index = myPicsPosts.indexOf(markedPost);
			if (index >= 0)
				post = myPicsPosts.get(index);
			break;
		case FragmentInfo.WALL:
			index = wallPosts.indexOf(markedPost);
			if (index >= 0)
				post = wallPosts.get(index);
			break;
		case FragmentInfo.Filtered:
			index = filterList.indexOf(markedPost);
			if (index >= 0)
				post = filterList.get(index);
			break;
		// case FragmentInfo.FULL_SCREEN:
		// index = postFullScreen.indexOf(markedPost);
		// post = postFullScreen.get(index);
		// break;
		case FragmentInfo.TAG_LIST:
			index = tagPosts.indexOf(markedPost);
			if (index >= 0)
				post = tagPosts.get(index);
			break;
		default:
			break;
		}
		if (post != null) {
			System.out.println(post);
			if (!finish) {
				if (!isException) {
					if (type == LIKE_POST) {
						post.setLiked(true);
						post.setLikeCount(post.getLikeCount() + 1);
						// if(whichTab == FragmentInfo.TOP_TEN){
						// topTenSessons.get(groupPosition).getPosts().remove(index);
						// topTenSessons.get(groupPosition).getPosts().add(index,
						// post);
						// }
					} else {
						post.setLiked(false);
						post.setLikeCount(post.getLikeCount() - 1);
					}
				} else {
					if (type == LIKE_POST) {
						post.setLiked(false);
						post.setLikeCount(post.getLikeCount() - 1);
					} else {
						post.setLiked(true);
						post.setLikeCount(post.getLikeCount() + 1);
					}
				}
			}
			if (finish && !isException) {
				System.out.println(topTenSessons);
				UpdateWallDataBase(post, whichTab);
			}
		}
	}

	public void handleRepost(final int userId, final int postId , final boolean ownPost) {
		final Action action = new Action(ActionType.REPOST, postId);
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					onRepostSuccess(postId, false, false);
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performRepost(userId, postId);
						notifyActionDone(action);
						onRepostSuccess(postId, false, true);
						if(ownPost){
							User user = UserManager.getInstance().getCurrentUser();
//							if(user.isPremium())
//								user.setCredit(user.getCredit() - 100);
//							else
							user.setCredit(user.getCredit() - 50);
							UserManager.getInstance().cacheUser(user);
						}
					}
				} catch (Exception e) {
					onRepostSuccess(postId, true, false);
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @param type
	 * @param userId
	 * @param postId
	 */
	protected void performRepost(int userId, int postId) throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Wall.USER_ID, userId);
		parametrs.put(Params.Wall.POST_ID, postId);

		BaseResponse response;
		try {
			response = submit(parametrs, URLs.Methods.REPOST);
			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
					throw new AppException(
							response.getValidationRule().errorMessage);
				else
					throw new AppException(
							BaseResponse.getExceptionType(response.getStatus()));
			}
		} catch (AppException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param appException
	 */
	protected void onRepostFailed(AppException appException) {

	}

	/**
	 * 
	 */
	protected void onRepostSuccess(int postId, boolean isException,
			boolean finish) {
		if (!filterList.isEmpty())
			changeListsRepostProcess(postId, FragmentInfo.Filtered,
					isException, finish);
		if (!myPicsPosts.isEmpty())
			changeListsRepostProcess(postId, FragmentInfo.MY_PIC, isException,
					finish);
		if (!latestPosts.isEmpty())
			changeListsRepostProcess(postId, FragmentInfo.LATEST_TAB,
					isException, finish);
		if (!wallPosts.isEmpty())
			changeListsRepostProcess(postId, FragmentInfo.WALL, isException,
					finish);
		if (!topTenSessons.isEmpty())
			changeListsRepostProcess(postId, FragmentInfo.TOP_TEN, isException,
					finish);
		// if (!postFullScreen.isEmpty())
		// changeListsRepostProcess(postId, FragmentInfo.FULL_SCREEN,
		// isException, finish);
		if (!tagPosts.isEmpty())
			changeListsRepostProcess(postId, FragmentInfo.TAG_LIST,
					isException, finish);
	}

	private void changeListsRepostProcess(int postId, final int whichTab,
			boolean isException, boolean finish) {
		PostViewModel markedPost = new PostViewModel(postId);
		switch (whichTab) {
		case FragmentInfo.LATEST_TAB:
			index = latestPosts.indexOf(markedPost);
			if (index >= 0)
				post = latestPosts.get(index);
			break;
		case FragmentInfo.TOP_TEN:
			for (int x = 0; x < topTenSessons.size(); x++) {
				for (int j = 0; j < topTenSessons.get(x).getPosts().size(); j++) {
					if (topTenSessons.get(x).getPosts().get(j)
							.equals(markedPost)) {
						groupPosition = x;
						index = j;
						if (index >= 0)
							post = topTenSessons.get(x).getPosts().get(j);
						break;
					}
				}
			}
			break;
		case FragmentInfo.MY_PIC:
			index = myPicsPosts.indexOf(markedPost);
			if (index >= 0)
				post = myPicsPosts.get(index);
			break;
		case FragmentInfo.WALL:
			index = wallPosts.indexOf(markedPost);
			if (index >= 0)
				post = wallPosts.get(index);
			break;
		case FragmentInfo.Filtered:
			index = filterList.indexOf(markedPost);
			if (index >= 0)
				post = filterList.get(index);
			break;
		// case FragmentInfo.FULL_SCREEN:
		// index = postFullScreen.indexOf(markedPost);
		// post = postFullScreen.get(index);
		// break;
		case FragmentInfo.TAG_LIST:
			index = tagPosts.indexOf(markedPost);
			if (index >= 0)
				post = tagPosts.get(index);
			break;
		default:
			break;
		}
		if (post != null) {
			if (!finish)
				if (!isException)
					post.setRePosted(true);
				else
					post.setRePosted(false);
			if (finish && !isException)
				UpdateWallDataBase(post, whichTab);

		}
	}

	public void handleFollow(final int currentUserId, final int otherUserId,
			final int postId, final Activity activity) {
		final Action action = new Action(ActionType.FOLLOW, postId);
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					onFollowSuccess(otherUserId, false, false);
					if (needsProcessing(action)) {
						notifyActionStarted(action);
						performFollow(currentUserId, otherUserId);
						User user = UserManager.getInstance().getCurrentUser();
						user.setFollowingCount(user.getFollowingCount() + 1);
						UserManager.getInstance().cacheUser(user);
						if (activity instanceof BaseActivity)
							((BaseActivity) activity).refreshMenu();
						notifyActionDone(action);
						onFollowSuccess(otherUserId, false, true);
					}
				} catch (Exception e) {
					// onFollowSuccess(otherUserId, true, false);
					notifyActionDone(action);
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @param currentUserId
	 * @param otherUserId
	 */
	protected void performFollow(int currentUserId, int otherUserId)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Wall.USER_ID, currentUserId);
		parametrs.put(Params.Wall.FAN_ID, otherUserId);

		BaseResponse response;
		try {
			response = submit(parametrs, URLs.Methods.FOLLOW_FAN);
			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
					throw new AppException(
							response.getValidationRule().errorMessage);
				else
					throw new AppException(
							BaseResponse.getExceptionType(response.getStatus()));
			}
		} catch (AppException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param appException
	 */
	protected void onFollowFailed(AppException appException) {

	}

	/**
	 * @param postId
	 * 
	 */
	protected void onFollowSuccess(int userId, boolean isException,
			boolean finish) {
		if (!filterList.isEmpty())
			changeListsFollowProcess(userId, FragmentInfo.Filtered,
					isException, finish);
		if (!latestPosts.isEmpty())
			changeListsFollowProcess(userId, FragmentInfo.LATEST_TAB,
					isException, finish);
		if (!myPicsPosts.isEmpty())
			changeListsFollowProcess(userId, FragmentInfo.MY_PIC, isException,
					finish);
		if (!wallPosts.isEmpty())
			changeListsFollowProcess(userId, FragmentInfo.WALL, isException,
					finish);
		if (!topTenSessons.isEmpty())
			changeListsFollowProcess(userId, FragmentInfo.TOP_TEN, isException,
					finish);
		// if (!postFullScreen.isEmpty())
		// changeListsFollowProcess(userId, FragmentInfo.FULL_SCREEN,
		// isException, finish);
		if (!tagPosts.isEmpty())
			changeListsFollowProcess(userId, FragmentInfo.TAG_LIST,
					isException, finish);
	}

	void changeListsFollowProcess(final int userId, final int whichTab,
			boolean isException, boolean finish) {
		final List<PostViewModel> tempList = getUserPosts(userId, whichTab,
				isException, finish);
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				switch (whichTab) {
				case FragmentInfo.LATEST_TAB:
					latestPosts.clear();
					latestPosts.addAll(tempList);
					break;
				case FragmentInfo.WALL:
					wallPosts.clear();
					wallPosts.addAll(tempList);
					break;
				case FragmentInfo.MY_PIC:
					myPicsPosts.clear();
					myPicsPosts.addAll(tempList);
					break;
				case FragmentInfo.TAG_LIST:
					tagPosts.clear();
					tagPosts.addAll(tempList);
					break;
				case FragmentInfo.Filtered:
					filterList.clear();
					filterList.addAll(tempList);
					break;
				case FragmentInfo.TOP_TEN:
					// topTen.clear();
					// topTen.addAll(tempList);
					break;
				default:
					break;
				}
			}
		});
	}

	private List<PostViewModel> getUserPosts(final int userId,
			final int whichTab, boolean isException, boolean finish) {
		final List<PostViewModel> tempList = new ArrayList<PostViewModel>();
		switch (whichTab) {
		case FragmentInfo.LATEST_TAB:
			tempList.addAll(latestPosts);
			break;
		case FragmentInfo.TOP_TEN:
			for (TopTenModel season : topTenSessons) {
				tempList.addAll(season.getPosts());
			}
			break;
		case FragmentInfo.MY_PIC:
			tempList.addAll(myPicsPosts);
			break;
		case FragmentInfo.WALL:
			tempList.addAll(wallPosts);
			break;
		case FragmentInfo.Filtered:
			tempList.addAll(filterList);
			break;
		// case FragmentInfo.FULL_SCREEN:
		// tempList.addAll(postFullScreen);
		// break;
		case FragmentInfo.TAG_LIST:
			tempList.addAll(tagPosts);
			break;
		default:
			break;
		}
		List<PostViewModel> posts = getPostsOfUser(userId, whichTab,
				isException, finish);
		for (int x = 0; x < posts.size(); x++) {
			final PostViewModel post = posts.get(x);
			final int index = posts.indexOf(post);
			final PostViewModel modifiedPost = posts.get(index);
			if (modifiedPost != null) {
				if (!finish)
					if (!isException)
						modifiedPost.setFollowing(true);
					else
						modifiedPost.setFollowing(false);
				tempList.remove(index);
				tempList.add(index, modifiedPost);
			}
		}
		if (finish && !isException)
			updateDataBase(tempList, whichTab);
		return tempList;
	}

	private void updateDataBase(final List<PostViewModel> tempList,
			final int whichTab) {
		new Thread(new Runnable() {

			@Override
			public void run() {

				for (PostViewModel post : tempList) {
					switch (whichTab) {
					case FragmentInfo.LATEST_TAB:
						post.setLatest(true);
						UpdateWallDataBase(post, FragmentInfo.LATEST_TAB);
						break;
					case FragmentInfo.MY_PIC:
						post.setMyPics(true);
						UpdateWallDataBase(post, FragmentInfo.MY_PIC);
						break;
					case FragmentInfo.WALL:
						post.setWall(true);
						UpdateWallDataBase(post, FragmentInfo.WALL);
						break;
					default:
						break;
					}
				}
			}
		}).start();

	}

	List<PostViewModel> getPostsOfUser(int userId, int whichTab,
			boolean isException, boolean finish) {
		List<PostViewModel> posts = new ArrayList<PostViewModel>();
		switch (whichTab) {
		case FragmentInfo.LATEST_TAB:
			for (int x = 0; x < latestPosts.size(); x++) {
				if (userId == latestPosts.get(x).getUserId())
				posts.add(latestPosts.get(x));
			}
			break;
		case FragmentInfo.TOP_TEN:
			for (TopTenModel season : topTenSessons) {
				for (int x = 0; x < season.getPosts().size(); x++) {
					if (userId == season.getPosts().get(x).getUserId())
					posts.add(season.getPosts().get(x));
				}
			}

			// for (int x = 0; x < topTen.size() ; x++) {
			// if (userId == topTen.get(x).getUserId())
			// ;
			// posts.add(topTen.get(x));
			// }
			break;
		case FragmentInfo.MY_PIC:
			for (int x = 0; x < myPicsPosts.size(); x++) {
				if (userId == myPicsPosts.get(x).getUserId())
				posts.add(myPicsPosts.get(x));
			}
			break;
		case FragmentInfo.WALL:
			for (int x = 0; x < wallPosts.size(); x++) {
				if (userId == wallPosts.get(x).getUserId())
				posts.add(wallPosts.get(x));
			}
			break;
		case FragmentInfo.Filtered:
			for (int x = 0; x < filterList.size(); x++) {
				if (userId == filterList.get(x).getUserId())
				posts.add(filterList.get(x));
			}
			break;
		// case FragmentInfo.FULL_SCREEN:
		// for (int x = 0; x < postFullScreen.size(); x++) {
		// if (userId == postFullScreen.get(x).getUserId())
		// ;
		// posts.add(postFullScreen.get(x));
		// }
		// break;
		case FragmentInfo.TAG_LIST:
			for (int x = 0; x < tagPosts.size(); x++) {
				if (userId == tagPosts.get(x).getUserId())
				posts.add(tagPosts.get(x));
			}
			break;
		default:
			break;
		}

		return posts;
	}

	public void deleteComment(final int postid, final int commentId,
			final int commentCount) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					performeDelete(postid, commentId);
					onCommentProcessSuccess(postid, DELETE_COMMENT, false,
							true, commentCount);
				} catch (Exception e) {
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnCommentDeleted.class);
				}
			}

			private void performeDelete(int postid, int commentId)
					throws AppException {
				Map<String, Object> parametrs = new HashMap<String, Object>();
				parametrs.put(Params.Wall.POST_ID, postid);
				parametrs.put(Params.Wall.COMMENT_ID, commentId);
				BaseResponse response = submit(parametrs,
						URLs.Methods.DELETECOMMENT);
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
					notifyCommentDeleted(postid, commentId);
				if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
					if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
						throw new AppException(
								response.getValidationRule().errorMessage);
					else
						throw new AppException(BaseResponse
								.getExceptionType(response.getStatus()));
				}
			}
		});
	}

	public void addComment(final UserComment comment, final int commentCount) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					BaseResponse response = submit(comment,
							URLs.Methods.ADD_COMMENT, UserComment.class);
					int commentId = 0;
					try {
						JSONObject json = new JSONObject(response.getData());
						commentId = json.getInt("commentId");
					} catch (JSONException e) {
						e.printStackTrace();
					}
					comment.setCommentId(commentId);
					notifyCommentAdded(comment);
					onCommentProcessSuccess(comment.getPostId(), ADD_COMMENT,
							false, true, commentCount);
				} catch (AppException e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnCommentsListReceived.class);
					e.printStackTrace();
				}
			}

		});
	}

	private void onCommentProcessSuccess(int postId, int type,
			boolean isException, boolean finish, int commentCount) {
		if (!filterList.isEmpty())
			changeListsCommentProcess(postId, type, FragmentInfo.Filtered,
					isException, finish, commentCount);
		if (!latestPosts.isEmpty())
			changeListsCommentProcess(postId, type, FragmentInfo.LATEST_TAB,
					isException, finish, commentCount);
		if (!wallPosts.isEmpty())
			changeListsCommentProcess(postId, type, FragmentInfo.WALL,
					isException, finish, commentCount);
		if (!myPicsPosts.isEmpty())
			changeListsCommentProcess(postId, type, FragmentInfo.MY_PIC,
					isException, finish, commentCount);
		if (!topTenSessons.isEmpty()) {
			changeListsCommentProcess(postId, type, FragmentInfo.TOP_TEN,
					isException, finish, commentCount);
		}
		// if (!postFullScreen.isEmpty()) {
		// changeListsCommentProcess(postId, type, FragmentInfo.FULL_SCREEN,
		// isException, finish, commentCount);
		// }
		if (!tagPosts.isEmpty())
			changeListsCommentProcess(postId, type, FragmentInfo.TAG_LIST,
					isException, finish, commentCount);
	}

	private void changeListsCommentProcess(int postId, final int type,
			final int whichTab, boolean isException, boolean finish,
			int commentCount) {
		PostViewModel markedPost = new PostViewModel(postId);
		switch (whichTab) {
		case FragmentInfo.LATEST_TAB:
			index = latestPosts.indexOf(markedPost);
			if (index >= 0)
				post = latestPosts.get(index);
			break;
		case FragmentInfo.TOP_TEN:
			for (int x = 0; x < topTenSessons.size(); x++) {
				for (int j = 0; j < topTenSessons.get(x).getPosts().size(); j++) {
					if (topTenSessons.get(x).getPosts().get(j)
							.equals(markedPost)) {
						groupPosition = x;
						index = j;
						if (index >= 0)
							post = topTenSessons.get(x).getPosts().get(j);
						break;
					}
				}
			}
			break;
		case FragmentInfo.MY_PIC:
			index = myPicsPosts.indexOf(markedPost);
			if (index >= 0)
				post = myPicsPosts.get(index);
			break;
		case FragmentInfo.WALL:
			index = wallPosts.indexOf(markedPost);
			if (index >= 0)
				post = wallPosts.get(index);
			break;
		case FragmentInfo.Filtered:
			index = filterList.indexOf(markedPost);
			if (index >= 0)
				post = filterList.get(index);
			break;
		// case FragmentInfo.FULL_SCREEN:
		// index = postFullScreen.indexOf(markedPost);
		// post = postFullScreen.get(index);
		// break;
		case FragmentInfo.TAG_LIST:
			index = tagPosts.indexOf(markedPost);
			if (index >= 0)
				post = tagPosts.get(index);
			break;
		default:
			break;
		}
		if (post != null) {
			if (finish)
				if (!isException) {
					if (type == ADD_COMMENT)
						post.setCommentCount(commentCount + 1);
					else if (type == DELETE_COMMENT)
						post.setCommentCount(commentCount - 1);
				} else {
					if (type == ADD_COMMENT)
						post.setCommentCount(commentCount - 1);
					else if (type == DELETE_COMMENT)
						post.setCommentCount(commentCount + 1);
				}
			if (finish && !isException)
				UpdateWallDataBase(post, whichTab);
		}
	}

	// public void handleLike(final int actionType, final int currentUserId,
	// final int postId) {
	// App.getInstance().runInBackground(new Runnable() {
	// final Action action = new Action(ActionType.LIKE, postId);
	//
	// @Override
	// public void run() {
	// try {
	// if (needsProcessing(action)) {
	// notifyActionStarted(action);
	// performLike(actionType, currentUserId, postId);
	// notifyActionDone(action);
	// onLikeProcessSuccess(postId, actionType);
	// }
	// } catch (Exception e) {
	// notifyActionDone(action);
	// e.printStackTrace();
	// }
	// }
	// });
	// }
	//
	// /**
	// * @param type
	// */
	// private void performLike(int type, int userId, int postId)
	// throws AppException {
	// Map<String, Object> parametrs = new HashMap<String, Object>();
	// parametrs.put(Params.Wall.USER_ID, userId);
	// parametrs.put(Params.Wall.POST_ID, postId);
	//
	// if (type == LIKE_POST) {
	// BaseResponse response = submit(parametrs, URLs.Methods.LIKE_POST);
	// if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
	// && response.getStatus() !=
	// BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
	// if (response.getStatus() ==
	// BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
	// throw new AppException(
	// response.getValidationRule().errorMessage);
	// else
	// throw new AppException(
	// BaseResponse.getExceptionType(response.getStatus()));
	// }
	// } else if (type == DISLIKE_POST) {
	// BaseResponse response = submit(parametrs, URLs.Methods.DISLIKE_POST);
	// if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
	// && response.getStatus() !=
	// BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
	// if (response.getStatus() ==
	// BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
	// throw new AppException(
	// response.getValidationRule().errorMessage);
	// else
	// throw new AppException(
	// BaseResponse.getExceptionType(response.getStatus()));
	// }
	// }
	// }
	//
	// /**
	// * @param e
	// */
	// protected void onLikeProcessFailed(AppException e) {
	//
	// }
	//
	// /**
	// *
	// */
	// protected void onLikeProcessSuccess(int postId, int type) {
	// if (!filterList.isEmpty())
	// changeFilteredLikeProcess(type, postId);
	// if (!latestPosts.isEmpty())
	// changeLatestLikeProcess(type, postId);
	// if (!wallPosts.isEmpty())
	// changeWallLikeProcess(type, postId);
	// if (!myPicsPosts.isEmpty())
	// changeMyPicsLikeProcess(type, postId);
	// if (!topTenSessons.isEmpty()) {
	// changeTopTenLikeProcess(type, postId);
	// }
	// if (!postFullScreen.isEmpty()) {
	// changePostFullScreenLikeProcess(type, postId);
	// }
	// if (!tagPosts.isEmpty())
	// changeTagsLikeProcess(type, postId);
	// }
	//
	// private void changeTagsLikeProcess(int type, int postId) {
	// final int index = getPostTagspostPosition(postId);
	// final PostViewModel post = getPostTagsForId(postId);
	// if (post != null) {
	// if (type == LIKE_POST) {
	// post.setLiked(true);
	// post.setLikeCount(post.getLikeCount() + 1);
	// } else {
	// post.setLiked(false);
	// post.setLikeCount(post.getLikeCount() - 1);
	// }
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// tagPosts.remove(index);
	// tagPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changePostFullScreenLikeProcess(int type, int postId) {
	// final int index = getPostFullScreenpostPosition(postId);
	// final PostViewModel post = getPostFullScreenPostForId(postId);
	// if (post != null) {
	// if (type == LIKE_POST) {
	// post.setLiked(true);
	// post.setLikeCount(post.getLikeCount() + 1);
	// } else {
	// post.setLiked(false);
	// post.setLikeCount(post.getLikeCount() - 1);
	// }
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// postFullScreen.remove(index);
	// postFullScreen.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private PostViewModel getPostFullScreenPostForId(int postId) {
	// for (int x = 0; x < postFullScreen.size() - 1; x++) {
	// if (postId == postFullScreen.get(x).getPostId())
	// return postFullScreen.get(x);
	// }
	// return null;
	// }
	//
	// private int getPostFullScreenpostPosition(int postId) {
	// for (int x = 0; x < postFullScreen.size() - 1; x++) {
	// if (postId == postFullScreen.get(x).getPostId())
	// return x;
	// }
	// return 0;
	// }
	//
	// private PostViewModel getPostTagsForId(int postId) {
	// for (int x = 0; x < tagPosts.size() - 1; x++) {
	// if (postId == tagPosts.get(x).getPostId())
	// return tagPosts.get(x);
	// }
	// return null;
	// }
	//
	// private int getPostTagspostPosition(int postId) {
	// for (int x = 0; x < tagPosts.size() - 1; x++) {
	// if (postId == tagPosts.get(x).getPostId())
	// return x;
	// }
	// return 0;
	// }

	//
	// private void changeTopTenLikeProcess(int type, int postId) {
	// final int index = getTopTenpostPosition(postId);
	// final PostViewModel post = getTopTenPostForId(postId);
	// if (post != null) {
	// if (type == LIKE_POST) {
	// post.setLiked(true);
	// post.setLikeCount(post.getLikeCount() + 1);
	// } else {
	// post.setLiked(false);
	// post.setLikeCount(post.getLikeCount() - 1);
	// }
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// topTen.remove(index);
	// topTen.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeFilteredLikeProcess(int type, int postId) {
	// final int index = getFilterpostPosition(postId);
	// final PostViewModel post = getFilterPostForId(postId);
	// if (post != null) {
	// if (type == LIKE_POST) {
	// post.setLiked(true);
	// post.setLikeCount(post.getLikeCount() + 1);
	// } else {
	// post.setLiked(false);
	// post.setLikeCount(post.getLikeCount() - 1);
	// }
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// filterList.remove(index);
	// filterList.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeMyPicsLikeProcess(int actionType, int postId) {
	// final int index = getMyPicspostPosition(postId);
	// final PostViewModel post = getMyPicsPostForId(postId);
	// if (post != null) {
	// if (actionType == LIKE_POST) {
	// post.setLiked(true);
	// post.setLikeCount(post.getLikeCount() + 1);
	// } else {
	// post.setLiked(false);
	// post.setLikeCount(post.getLikeCount() - 1);
	// }
	// post.setMyPics(true);
	// UpdateWallDataBase(post, FragmentInfo.MY_PIC);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// myPicsPosts.remove(index);
	// myPicsPosts.add(index, post);
	// }
	// });
	// }
	// }

	private void UpdateWallDataBase(final PostViewModel post, final int whichTab) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				switch (whichTab) {
				case FragmentInfo.LATEST_TAB:
					WallTable.getInstance().update(post, post.getPostId());
					break;
				case FragmentInfo.WALL:
					WallTable.getInstance().update(post, post.getPostId());
					break;
				case FragmentInfo.MY_PIC:
					WallTable.getInstance().update(post, post.getPostId());
					break;
				default:
					break;
				}
			}
		}).start();
	}

	// private void changeWallLikeProcess(int actionType, int postId) {
	// final int index = getWallpostPosition(postId);
	// final PostViewModel post = getWallPostForId(postId);
	// if (post != null) {
	// if (actionType == LIKE_POST) {
	// post.setLiked(true);
	// post.setLikeCount(post.getLikeCount() + 1);
	// } else {
	// post.setLiked(false);
	// post.setLikeCount(post.getLikeCount() - 1);
	// }
	// post.setWall(true);
	// UpdateWallDataBase(post, FragmentInfo.WALL);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// wallPosts.remove(index);
	// wallPosts.add(index, post);
	// }
	// });
	// }
	// }

	// private void changeLatestLikeProcess(int actionType, int postId) {
	// final int index = getLatestpostPosition(postId);
	// final PostViewModel post = getLatestPostForId(postId);
	// if (post != null) {
	// if (actionType == LIKE_POST) {
	// post.setLiked(true);
	// post.setLikeCount(post.getLikeCount() + 1);
	// } else {
	// post.setLiked(false);
	// post.setLikeCount(post.getLikeCount() - 1);
	// }
	// post.setLatest(true);
	// UpdateWallDataBase(post, FragmentInfo.LATEST_TAB);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// latestPosts.remove(index);
	// latestPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private int getMyPicspostPosition(int postId) {
	// for (int x = 0; x < myPicsPosts.size() - 1; x++) {
	// if (postId == myPicsPosts.get(x).getPostId())
	// return x;
	// }
	// return 0;
	// }
	//
	public PostViewModel getMyPicsPostForId(int postId) {
		// for (int x = 0; x < myPicsPosts.size() - 1; x++) {
		// if (postId == myPicsPosts.get(x).getPostId())
		// return myPicsPosts.get(x);
		// }
		// return null;
		PostViewModel markedPost = new PostViewModel(postId);
		int index = myPicsPosts.indexOf(markedPost);
		if (index >= 0)
			return myPicsPosts.get(myPicsPosts.indexOf(markedPost));
		else
			return null;
	}

	//
	// private int getTopTenpostPosition(int postId) {
	// for (int x = 0; x < topTen.size() - 1; x++) {
	// if (postId == topTen.get(x).getPostId())
	// return x;
	// }
	// return -1;
	// }

	public PostViewModel getTopTenPostForId(int postId) {
		PostViewModel post = null;
		PostViewModel markedPost = new PostViewModel(postId);
		for (int x = 0; x < topTenSessons.size(); x++) {
			for (int j = 0; j < topTenSessons.get(x).getPosts().size(); j++) {
				if (topTenSessons.get(x).getPosts().get(j).equals(markedPost)) {
					// groupPosition = x;
					// index = j;
					post = topTenSessons.get(x).getPosts().get(j);
					break;
				}
			}
		}
		return post;
	}

	//
	// private int getFilterpostPosition(int postId) {
	// for (int x = 0; x < filterList.size() - 1; x++) {
	// if (postId == filterList.get(x).getPostId())
	// return x;
	// }
	// return 0;
	// }
	//
	public PostViewModel getFilterPostForId(int postId) {
		// for (int x = 0; x < filterList.size() - 1; x++) {
		// if (postId == filterList.get(x).getPostId())
		// return filterList.get(x);
		// }
		// return null;
		PostViewModel markedPost = new PostViewModel(postId);
		int index = filterList.indexOf(markedPost);
		if (index >= 0)
			return filterList.get(filterList.indexOf(markedPost));
		else
			return null;
	}

	//
	// private int getLatestpostPosition(int postId) {
	// for (int x = 0; x < latestPosts.size() - 1; x++) {
	// if (postId == latestPosts.get(x).getPostId())
	// return x;
	// }
	// return 0;
	// }
	//
	public PostViewModel getLatestPostForId(int postId) {
		// for (int x = 0; x < latestPosts.size() - 1; x++) {
		// if (postId == latestPosts.get(x).getPostId())
		// return latestPosts.get(x);
		// }
		// return null;
		PostViewModel markedPost = new PostViewModel(postId);
		int index = latestPosts.indexOf(markedPost);
		if (index >= 0)
			return latestPosts.get(index);
		else
			return null;
	}

	//
	// private int getWallpostPosition(int postId) {
	// for (int x = 0; x < wallPosts.size() - 1; x++) {
	// if (postId == wallPosts.get(x).getPostId())
	// return x;
	// }
	// return 0;
	// }
	//
	public PostViewModel getWallPostForId(int postId) {
		// for (int x = 0; x < wallPosts.size() - 1; x++) {
		// if (postId == wallPosts.get(x).getPostId())
		// return wallPosts.get(x);
		// }
		// return null;
		PostViewModel markedPost = new PostViewModel(postId);
		int index = wallPosts.indexOf(markedPost);
		if (index >= 0)
			return wallPosts.get(wallPosts.indexOf(markedPost));
		else
			return null;
	}

	// -------------------------------Repost----------------------

	// public void handleRepost(final int userId, final int postId) {
	// final Action action = new Action(ActionType.REPOST, postId);
	// App.getInstance().runInBackground(new Runnable() {
	//
	// @Override
	// public void run() {
	// try {
	// if (needsProcessing(action)) {
	// notifyActionStarted(action);
	// performRepost(userId, postId);
	// notifyActionDone(action);
	// onRepostSuccess(postId);
	// }
	// } catch (Exception e) {
	// notifyActionDone(action);
	// e.printStackTrace();
	// }
	// }
	// });
	// }
	//
	// /**
	// * @param type
	// * @param userId
	// * @param postId
	// */
	// protected void performRepost(int userId, int postId) throws AppException
	// {
	// Map<String, Object> parametrs = new HashMap<String, Object>();
	// parametrs.put(Params.Wall.USER_ID, userId);
	// parametrs.put(Params.Wall.POST_ID, postId);
	//
	// BaseResponse response;
	// try {
	// response = submit(parametrs, URLs.Methods.REPOST);
	// if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
	// && response.getStatus() !=
	// BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
	// if (response.getStatus() ==
	// BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
	// throw new AppException(
	// response.getValidationRule().errorMessage);
	// else
	// throw new AppException(
	// BaseResponse.getExceptionType(response.getStatus()));
	// }
	// } catch (AppException e) {
	// e.printStackTrace();
	// }
	//
	// }
	//
	// /**
	// * @param appException
	// */
	// protected void onRepostFailed(AppException appException) {
	//
	// }
	//
	// /**
	// *
	// */
	// protected void onRepostSuccess(int postId) {
	// if (!filterList.isEmpty())
	// changeFilterRepostProcess(postId);
	// if (!myPicsPosts.isEmpty())
	// changeMyPicsRepostProcess(postId);
	// if (!latestPosts.isEmpty())
	// changeLatestRepostProcess(postId);
	// if (!wallPosts.isEmpty())
	// changeWallRepostProcess(postId);
	// if (!topTen.isEmpty())
	// changeTopTenRepostProcess(postId);
	// if (!postFullScreen.isEmpty())
	// changePostFullScreenRepostProcess(postId);
	// if (!tagPosts.isEmpty())
	// changeTagsRepostProcess(postId);
	// }
	//
	// private void changeTagsRepostProcess(int postId) {
	// final int index = getPostTagspostPosition(postId);
	// final PostViewModel post = getPostTagsForId(postId);
	// if (post != null) {
	// post.setRePosted(true);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// tagPosts.remove(index);
	// tagPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changePostFullScreenRepostProcess(int postId) {
	// final int index = getPostFullScreenpostPosition(postId);
	// final PostViewModel post = getPostFullScreenPostForId(postId);
	// if (post != null) {
	// post.setRePosted(true);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// postFullScreen.remove(index);
	// postFullScreen.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeTopTenRepostProcess(int postId) {
	// final int index = getTopTenpostPosition(postId);
	// final PostViewModel post = getTopTenPostForId(postId);
	// if (post != null) {
	// post.setRePosted(true);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
	// topTen.remove(index);
	// topTen.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeFilterRepostProcess(int postId) {
	// final int index = getMyPicspostPosition(postId);
	// final PostViewModel post = getMyPicsPostForId(postId);
	// if (post != null) {
	// post.setRePosted(true);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// filterList.remove(index);
	// filterList.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeMyPicsRepostProcess(int postId) {
	// final int index = getMyPicspostPosition(postId);
	// final PostViewModel post = getMyPicsPostForId(postId);
	// if (post != null) {
	// post.setRePosted(true);
	// post.setMyPics(true);
	// UpdateWallDataBase(post, FragmentInfo.MY_PIC);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// myPicsPosts.remove(index);
	// myPicsPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeWallRepostProcess(int postId) {
	// final int index = getWallpostPosition(postId);
	// final PostViewModel post = getWallPostForId(postId);
	// if (post != null) {
	// post.setRePosted(true);
	// post.setWall(true);
	// UpdateWallDataBase(post, FragmentInfo.WALL);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// wallPosts.remove(index);
	// wallPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeLatestRepostProcess(int postId) {
	// final int index = getLatestpostPosition(postId);
	// final PostViewModel post = getLatestPostForId(postId);
	// if (post != null) {
	// post.setRePosted(true);
	// post.setLatest(true);
	// UpdateWallDataBase(post, FragmentInfo.LATEST_TAB);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// latestPosts.remove(index);
	// latestPosts.add(index, post);
	// }
	// });
	// }
	// }

	// -------------------------------repostEnd-------------------

	// ------------------------follow---------------------------

	// public void handleFollow(final int currentUserId, final int otherUserId,
	// final int postId) {
	// final Action action = new Action(ActionType.FOLLOW, postId);
	// App.getInstance().runInBackground(new Runnable() {
	//
	// @Override
	// public void run() {
	// try {
	// if (needsProcessing(action)) {
	// notifyActionStarted(action);
	// performFollow(currentUserId, otherUserId);
	// notifyActionDone(action);
	// onFollowSuccess(otherUserId, postId);
	// }
	// } catch (Exception e) {
	// notifyActionDone(action);
	// e.printStackTrace();
	// }
	// }
	// });
	// }
	//
	// /**
	// * @param currentUserId
	// * @param otherUserId
	// */
	// protected void performFollow(int currentUserId, int otherUserId)
	// throws AppException {
	// Map<String, Object> parametrs = new HashMap<String, Object>();
	// parametrs.put(Params.Wall.USER_ID, currentUserId);
	// parametrs.put(Params.Wall.FAN_ID, otherUserId);
	//
	// BaseResponse response;
	// try {
	// response = submit(parametrs, URLs.Methods.FOLLOW_FAN);
	// if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
	// && response.getStatus() !=
	// BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
	// if (response.getStatus() ==
	// BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
	// throw new AppException(
	// response.getValidationRule().errorMessage);
	// else
	// throw new AppException(
	// BaseResponse.getExceptionType(response.getStatus()));
	// }
	// } catch (AppException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// /**
	// * @param appException
	// */
	// protected void onFollowFailed(AppException appException) {
	//
	// }
	//
	// /**
	// * @param postId
	// *
	// */
	// protected void onFollowSuccess(int userId, int postId) {
	// if (!filterList.isEmpty())
	// changeFilterFollowProcess(postId);
	// if (!latestPosts.isEmpty())
	// changeLatestFollowProcess(userId);
	// if (!myPicsPosts.isEmpty())
	// changeMyPicsFollowProcess(userId);
	// if (!wallPosts.isEmpty())
	// changeWallFollowProcess(userId);
	// if (!topTen.isEmpty())
	// changeTopTenFollowProcess(postId);
	// if (!postFullScreen.isEmpty())
	// changePostFullScreenFollowProcess(postId);
	// if (!tagPosts.isEmpty())
	// changeTagList(postId);
	// }
	//
	// private void changeTagList(int postId) {
	// final int index = getPostTagspostPosition(postId);
	// final PostViewModel post = getPostTagsForId(postId);
	// if (post != null) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// tagPosts.remove(index);
	// tagPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changePostFullScreenFollowProcess(int postId) {
	// final int index = getPostFullScreenpostPosition(postId);
	// final PostViewModel post = getPostFullScreenPostForId(postId);
	// if (post != null) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// postFullScreen.remove(index);
	// postFullScreen.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeTopTenFollowProcess(int postId) {
	// final int index = getTopTenpostPosition(postId);
	// final PostViewModel post = getTopTenPostForId(postId);
	// if (post != null) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// topTen.remove(index);
	// topTen.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeFilterFollowProcess(int postId) {
	// final int index = getMyPicspostPosition(postId);
	// final PostViewModel post = getMyPicsPostForId(postId);
	// if (post != null) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// filterList.remove(index);
	// filterList.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeMyPicsFollowProcess(int userId) {
	// // final int index = getMyPicspostPosition(postId);
	// // final PostViewModel post = getMyPicsPostForId(postId);
	// // if (post != null) {
	// // post.setFollowing(true);
	// // post.setMyPics(true);
	// // UpdateWallDataBase(post, FragmentInfo.MY_PIC);
	// // App.getInstance().runOnUiThread(new Runnable() {
	// //
	// // @Override
	// // public void run() {
	// // // Toast.makeText(context, "Done",
	// // // Toast.LENGTH_SHORT).show();
	// // myPicsPosts.remove(index);
	// // myPicsPosts.add(index, post);
	// // }
	// // });
	// // }
	//
	// final List<PostViewModel> tempList = new ArrayList<PostViewModel>();
	// tempList.addAll(myPicsPosts);
	// List<PostViewModel> posts = getPostsOfUserInMyPics(userId);
	// for (int x = 0; x < posts.size(); x++) {
	// final PostViewModel post = posts.get(x);
	// final int index = getLatestpostPosition(post.getPostId());
	// final PostViewModel modifiedPost = getLatestPostForId(post
	// .getPostId());
	// if (modifiedPost != null) {
	// modifiedPost.setFollowing(true);
	// modifiedPost.setLatest(true);
	// UpdateWallDataBase(modifiedPost, FragmentInfo.MY_PIC);
	// tempList.remove(index);
	// tempList.add(index, modifiedPost);
	// }
	// }
	//
	// // final int index = getLatestpostPosition(postId);
	// // final PostViewModel post = getLatestPostForId(postId);
	// // if (post != null) {
	// // post.setFollowing(true);
	// // post.setLatest(true);
	// // UpdateWallDataBase(post, FragmentInfo.LATEST_TAB);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// myPicsPosts.clear();
	// myPicsPosts.addAll(tempList);
	// }
	// });
	// }
	//
	// // }
	//
	// private void changeWallFollowProcess(int userId) {
	// // final int index = getWallpostPosition(postId);
	// // final PostViewModel post = getWallPostForId(postId);
	// // if (post != null) {
	// // post.setFollowing(true);
	// // post.setWall(true);
	// // UpdateWallDataBase(post, FragmentInfo.WALL);
	// // App.getInstance().runOnUiThread(new Runnable() {
	// //
	// // @Override
	// // public void run() {
	// // // Toast.makeText(context, "Done",
	// // // Toast.LENGTH_SHORT).show();
	// // wallPosts.remove(index);
	// // wallPosts.add(index, post);
	// // }
	// // });
	// // }
	//
	// final List<PostViewModel> tempList = new ArrayList<PostViewModel>();
	// tempList.addAll(wallPosts);
	// List<PostViewModel> posts = getPostsOfUserInWall(userId);
	// for (int x = 0; x < posts.size(); x++) {
	// final PostViewModel post = posts.get(x);
	// final int index = getLatestpostPosition(post.getPostId());
	// final PostViewModel modifiedPost = getLatestPostForId(post
	// .getPostId());
	// if (modifiedPost != null) {
	// modifiedPost.setFollowing(true);
	// modifiedPost.setWall(true);
	// UpdateWallDataBase(modifiedPost, FragmentInfo.WALL);
	// tempList.remove(index);
	// tempList.add(index, modifiedPost);
	// }
	// }
	//
	// // final int index = getLatestpostPosition(postId);
	// // final PostViewModel post = getLatestPostForId(postId);
	// // if (post != null) {
	// // post.setFollowing(true);
	// // post.setLatest(true);
	// // UpdateWallDataBase(post, FragmentInfo.LATEST_TAB);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// wallPosts.clear();
	// wallPosts.addAll(tempList);
	// }
	// });
	// }
	//
	// // }
	//
	// private void changeLatestFollowProcess(int userId) {
	// final List<PostViewModel> tempList = new ArrayList<PostViewModel>();
	// tempList.addAll(latestPosts);
	// List<PostViewModel> posts = getPostsOfUserInLatest(userId);
	// for (int x = 0; x < posts.size(); x++) {
	// final PostViewModel post = posts.get(x);
	// final int index = getLatestpostPosition(post.getPostId());
	// final PostViewModel modifiedPost = getLatestPostForId(post
	// .getPostId());
	// if (modifiedPost != null) {
	// modifiedPost.setFollowing(true);
	// modifiedPost.setLatest(true);
	// UpdateWallDataBase(modifiedPost, FragmentInfo.LATEST_TAB);
	// tempList.remove(index);
	// tempList.add(index, modifiedPost);
	// }
	// }
	//
	// // final int index = getLatestpostPosition(postId);
	// // final PostViewModel post = getLatestPostForId(postId);
	// // if (post != null) {
	// // post.setFollowing(true);
	// // post.setLatest(true);
	// // UpdateWallDataBase(post, FragmentInfo.LATEST_TAB);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// latestPosts.clear();
	// latestPosts.addAll(tempList);
	// }
	// });
	// }

	// }

	// List<PostViewModel> getPostsOfUserInLatest(int userId) {
	// List<PostViewModel> posts = new ArrayList<PostViewModel>();
	// for (int x = 0; x < latestPosts.size() - 1; x++) {
	// if (userId == latestPosts.get(x).getUserId())
	// ;
	// posts.add(latestPosts.get(x));
	// }
	// return posts;
	// }
	//
	// List<PostViewModel> getPostsOfUserInWall(int userId) {
	// List<PostViewModel> posts = new ArrayList<PostViewModel>();
	// for (int x = 0; x < wallPosts.size() - 1; x++) {
	// if (userId == wallPosts.get(x).getUserId())
	// ;
	// posts.add(wallPosts.get(x));
	// }
	// return posts;
	// }
	//
	// List<PostViewModel> getPostsOfUserInMyPics(int userId) {
	// List<PostViewModel> posts = new ArrayList<PostViewModel>();
	// for (int x = 0; x < myPicsPosts.size() - 1; x++) {
	// if (userId == myPicsPosts.get(x).getUserId())
	// ;
	// posts.add(myPicsPosts.get(x));
	// }
	// return posts;
	// }

	// public void updateDialog(final List<Integer> list) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// for (UiListener listener : getListeners()) {
	// if (listener instanceof UpdateDialogListener) {
	// ((UpdateDialogListener) listener).countryList(list);
	// }
	// }
	// }
	// });
	// }

	// ---------------------------------------------------------

	// -------------------------INITIALSTATE-------------------------------

	public boolean checkIfInitialState() {
		return new File(context.getFilesDir() + "/" + INITIAL_STATE_HOME)
				.exists();
	}

	public void saveInitialHomeStateEnded() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					InternalFileSaveDataLayer.saveObject(context,
							INITIAL_STATE_HOME, "");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void fireMenuInitialState(final FragmentActivity fragmentActivity) {
		if (!checkIfInitialState()) {
			Timer mTimer = new Timer();
			mTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					if (fragmentActivity instanceof BaseActivity)
						((BaseActivity) fragmentActivity)
								.homeInitialMenuState();
					saveInitialHomeStateEnded();
				}
			}, 1000);
		}
	}

	// ---------- Report Post ----------------/
	public void ReportPost(final int userId, final int postId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					performReport(userId, postId);
					onReportSuccess();
				} catch (Exception e) {
					onReportFailed(AppException.getAppException(e));
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @param userId
	 * @param postId
	 */
	protected void performReport(int userId, int postId) throws AppException {

		Calendar c = Calendar.getInstance();
		Date dateToSend = c.getTime();

		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Wall.USER_ID, userId);
		parametrs.put(Params.Wall.POST_ID, postId);
		parametrs.put(Params.Wall.DATE, dateToSend.getTime());

		BaseResponse response;
		try {
			response = submit(parametrs, URLs.Methods.REPORT);
			if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID
					&& response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
					throw new AppException(
							response.getValidationRule().errorMessage);
				else
					throw new AppException(
							BaseResponse.getExceptionType(response.getStatus()));
			}
		} catch (AppException e) {
			e.printStackTrace();
		}

	}

	protected void onReportFailed(AppException appException) {

	}

	protected void onReportSuccess() {

	}

	// ------ End Of Report Post -------/

	// ------- Delete Post-------------/
	// public void deletePost(final int post_id) {
	// App.getInstance().runInBackground(new Runnable() {
	//
	// @Override
	// public void run() {
	// try {
	// performeDelete(post_id);
	// onDeleteResponse();
	// } catch (Exception e) {
	// e.printStackTrace();
	// notifyRetrievalException(AppException.getAppException(e),
	// OnSuccessVoidListener.class);
	// }
	// }
	//
	// private void onDeleteResponse() {
	// App.getInstance().runOnUiThread(new Runnable() {
	// @Override
	// public void run() {
	// for (UiListener temp : getListeners())
	// if (temp instanceof OnSuccessVoidListener)
	// ((OnSuccessVoidListener) temp).onSuccess();
	// }
	// });
	// }
	//
	// private void performeDelete(int post_id) {
	// Map<String, Object> parametrs = new HashMap<String, Object>();
	// parametrs.put(Params.IMAGE_DETAILS.POST_ID, post_id);
	// try {
	// BaseResponse response = submit(parametrs,
	// URLs.Methods.DELETEPOST);
	// if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
	// notifyPostDeleted(post_id);
	// if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
	// if (response.getStatus() ==
	// BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
	// throw new AppException(
	// response.getValidationRule().errorMessage);
	// else
	// throw new AppException(BaseResponse
	// .getExceptionType(response.getStatus()));
	// }
	// } catch (AppException e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }
	//
	// public void notifyPostDeleted(final int post_id) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// if (!latestPosts.isEmpty())
	// deletePostFromLatest(post_id);
	// if (!wallPosts.isEmpty())
	// deletePostFromWall(post_id);
	// if (!myPicsPosts.isEmpty())
	// deletePostFromMyPic(post_id);
	// if (!topTenSessons.isEmpty())
	// deletePostFromTopTen(post_id);
	// if (!filterList.isEmpty())
	// deletePostFromFiltered(post_id);
	// }
	// });
	// }
	//
	// protected void deletePostFromFiltered(int post_id) {
	// final int index = getFilterpostPosition(post_id);
	// if (index != -1) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// filterList.remove(index);
	// }
	// });
	// }
	// }
	//
	// protected void deletePostFromTopTen(final int post_id) {
	// final int index = getTopTenpostPosition(post_id);
	// if (index != -1) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// topTen.remove(index);
	// }
	// });
	// }
	// }
	//
	// protected void deletePostFromMyPic(final int post_id) {
	// final int index = getMyPicspostPosition(post_id);
	// if (index != -1) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// myPicsPosts.remove(index);
	// }
	// });
	// }
	// }
	//
	// protected void deletePostFromWall(final int post_id) {
	// final int index = getWallpostPosition(post_id);
	// if (index != -1) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// wallPosts.remove(index);
	// }
	// });
	// }
	// }
	//
	// protected void deletePostFromLatest(final int post_id) {
	// final int index = getLatestpostPosition(post_id);
	// if (index != -1) {
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// latestPosts.remove(index);
	// }
	// });
	// }
	// }

	public void deletePost(final int post_id,
			final FragmentActivity fragmentActivity) {

		final ProgressDialog dialog = new ProgressDialog(fragmentActivity,
				R.style.MyTheme);

		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				dialog.setCancelable(false);
				dialog.show();
			}
		});
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					performeDelete(post_id);
					notifyPostDeleted(post_id);
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							dialog.dismiss();
							Intent intent = new Intent("finish_activity");
							fragmentActivity.sendBroadcast(intent);
							fragmentActivity.finish();
						}
					});
				} catch (Exception e) {
					App.getInstance().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							dialog.dismiss();
						}
					});
					e.printStackTrace();
					notifyRetrievalException(AppException.getAppException(e),
							OnSuccessVoidListener.class);
				}
			}

			private void performeDelete(int post_id) {
				Map<String, Object> parametrs = new HashMap<String, Object>();
				parametrs.put(Params.IMAGE_DETAILS.POST_ID, post_id);
				try {
					BaseResponse response = submit(parametrs,
							URLs.Methods.DELETEPOST);
					if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
						if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
							throw new AppException(
									response.getValidationRule().errorMessage);
						else
							throw new AppException(BaseResponse
									.getExceptionType(response.getStatus()));
					}
				} catch (AppException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void onDeleteResponse() {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener temp : getListeners())
					if (temp instanceof OnSuccessVoidListener)
						((OnSuccessVoidListener) temp).onSuccess();
			}
		});
	}

	public void notifyPostDeleted(final int post_id) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (!filterList.isEmpty())
					deletePostFromLists(post_id, FragmentInfo.Filtered);
				if (!latestPosts.isEmpty())
					deletePostFromLists(post_id, FragmentInfo.LATEST_TAB);
				if (!myPicsPosts.isEmpty())
					deletePostFromLists(post_id, FragmentInfo.MY_PIC);
				if (!wallPosts.isEmpty())
					deletePostFromLists(post_id, FragmentInfo.WALL);
				if (!topTenSessons.isEmpty())
					deletePostFromLists(post_id, FragmentInfo.TOP_TEN);
				// if (!postFullScreen.isEmpty())
				// deletePostFromLists(post_id, FragmentInfo.FULL_SCREEN);
				if (!tagPosts.isEmpty())
					deletePostFromLists(post_id, FragmentInfo.TAG_LIST);
				deleteOriginalPostFromAll(post_id);
			}
		});
	}

	protected void deletePostFromLists(int post_id, final int whichTab) {
		PostViewModel markedPost = new PostViewModel(post_id);
		switch (whichTab) {
		case FragmentInfo.LATEST_TAB:
			index = latestPosts.indexOf(markedPost);
			break;
		case FragmentInfo.TOP_TEN:
			for (int x = 0; x < topTenSessons.size(); x++) {
				for (int j = 0; j < topTenSessons.get(x).getPosts().size(); j++) {
					if (topTenSessons.get(x).getPosts().get(j)
							.equals(markedPost)) {
						groupPosition = x;
						index = j;
						post = topTenSessons.get(x).getPosts().get(j);
						break;
					}
				}
			}
			// index = topTen.indexOf(markedPost);
			break;
		case FragmentInfo.MY_PIC:
			index = myPicsPosts.indexOf(markedPost);
			break;
		case FragmentInfo.WALL:
			index = wallPosts.indexOf(markedPost);
			break;
		case FragmentInfo.Filtered:
			index = filterList.indexOf(markedPost);
			break;
		// case FragmentInfo.FULL_SCREEN:
		// index = postFullScreen.indexOf(markedPost);
		// break;
		case FragmentInfo.TAG_LIST:
			index = tagPosts.indexOf(markedPost);
			break;
		default:
			break;
		}
		if (index != -1) {
			new deletePostFromDataBaseAsync(post_id, index, whichTab).execute();
		}
	}

	private class deletePostFromDataBaseAsync extends
			AsyncTask<Void, Void, Void> {

		int post_id;
		int whichTab;
		int index;

		public deletePostFromDataBaseAsync(int post_id, int index, int whichTab) {
			this.post_id = post_id;
			this.whichTab = whichTab;
			this.index = index;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			switch (whichTab) {
			case FragmentInfo.LATEST_TAB:
				latestPosts.remove(index);
				break;
			case FragmentInfo.TOP_TEN:
				topTenSessons.get(groupPosition).getPosts().remove(index);
				// topTen.remove(index);
				break;
			case FragmentInfo.MY_PIC:
				myPicsPosts.remove(index);
				if (myPicsPosts.isEmpty())
					notifyMyPicsEmptyList();
				break;
			case FragmentInfo.WALL:
				wallPosts.remove(index);
				break;
			case FragmentInfo.Filtered:
				filterList.remove(index);
				break;
			// case FragmentInfo.FULL_SCREEN:
			// postFullScreen.remove(index);
			// break;
			case FragmentInfo.TAG_LIST:
				tagPosts.remove(index);
				break;
			default:
				break;
			}
			onDeleteResponse();
		}

		@Override
		protected Void doInBackground(Void... params) {
			WallTable.getInstance().delete(post_id);
			return null;
		}

	}
	
	protected void deleteOriginalPostFromAll(int post_id) {
		
		if (!filterList.isEmpty()){
		final List<PostViewModel> temp = new ArrayList<PostViewModel>();
			for (int j = 0; j < filterList.size(); j++) {
				if (filterList.get(j).getOriginalPostId() == post_id) {
					temp.add(filterList.get(j));
				}
			}
			App.getInstance().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					filterList.removeAll(temp);
				}
			});
		}
		if (!latestPosts.isEmpty()){
			final List<PostViewModel> temp = new ArrayList<PostViewModel>();
			for (int j = 0; j < latestPosts.size(); j++) {
				if (latestPosts.get(j).getOriginalPostId() == post_id) {
					temp.add(latestPosts.get(j));
				}
			}
			App.getInstance().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					latestPosts.removeAll(temp);
				}
			});
		}
		if (!myPicsPosts.isEmpty()){
			final List<PostViewModel> temp = new ArrayList<PostViewModel>();
			for (int j = 0; j < myPicsPosts.size(); j++) {
				if (myPicsPosts.get(j).getOriginalPostId() == post_id) {
					temp.add(myPicsPosts.get(j));
				}
			}
			App.getInstance().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					myPicsPosts.removeAll(temp);
				}
			});
		}
		if (!wallPosts.isEmpty()){
			final List<PostViewModel> temp = new ArrayList<PostViewModel>();
			for (int j = 0; j < wallPosts.size(); j++) {
				if (wallPosts.get(j).getOriginalPostId() == post_id) {
					temp.add(wallPosts.get(j));
				}
			}
			App.getInstance().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					wallPosts.removeAll(temp);
				}
			});
		}
		if (!tagPosts.isEmpty()){
			final List<PostViewModel> temp = new ArrayList<PostViewModel>();
			for (int j = 0; j < tagPosts.size(); j++) {
				if (tagPosts.get(j).getOriginalPostId() == post_id) {
					temp.add(tagPosts.get(j));
				}
			}
			App.getInstance().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					tagPosts.removeAll(temp);
				}
			});
		}
	}

	// ----------- End Of Delete Post ----------/

	// -------------- Comments Part------------/
	// public void addComment(final UserComment comment) {
	// App.getInstance().runInBackground(new Runnable() {
	//
	// @Override
	// public void run() {
	// try {
	// BaseResponse response = submit(comment,
	// URLs.Methods.ADD_COMMENT, UserComment.class);
	// int commentId = 0;
	// try {
	// JSONObject json = new JSONObject(response.getData());
	// commentId = json.getInt("commentId");
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }
	// comment.setCommentId(commentId);
	// notifyCommentAdded(comment);
	// if (!filterList.isEmpty())
	// changeFilteredCommentProcess(comment.getPostId(),
	// ADD_COMMENT);
	// if (!latestPosts.isEmpty())
	// changeLatestCommentProcess(comment.getPostId(),
	// ADD_COMMENT);
	// if (!wallPosts.isEmpty())
	// changeWallCommentProcess(comment.getPostId(),
	// ADD_COMMENT);
	// if (!myPicsPosts.isEmpty())
	// changeMyPicsCommentProcess(comment.getPostId(),
	// ADD_COMMENT);
	// if (!topTenSessons.isEmpty()) {
	// changeTopTenCommentProcess(comment.getPostId(),
	// ADD_COMMENT);
	// }
	// if (!postFullScreen.isEmpty()) {
	// changePostFullScreenCommentProcess(comment.getPostId(),
	// ADD_COMMENT);
	// }
	// if (!tagPosts.isEmpty())
	// changeTagsCommentProcess(comment.getPostId(),
	// ADD_COMMENT);
	// } catch (AppException e) {
	// notifyRetrievalException(AppException.getAppException(e),
	// OnCommentsListReceived.class);
	// e.printStackTrace();
	// }
	// }
	//
	// });
	// }
	//
	// protected void changePostFullScreenCommentProcess(int postId, int type) {
	// final int index = getPostFullScreenpostPosition(postId);
	// final PostViewModel post = getPostFullScreenPostForId(postId);
	// if (post != null) {
	// if (type == ADD_COMMENT)
	// post.setCommentCount(post.getCommentCount() + 1);
	// else if (type == DELETE_COMMENT)
	// post.setCommentCount(post.getCommentCount() - 1);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// postFullScreen.remove(index);
	// postFullScreen.add(index, post);
	// }
	// });
	// }
	// }
	//
	// protected void changeTagsCommentProcess(int postid, int type) {
	// final int index = getPostTagspostPosition(postid);
	// final PostViewModel post = getPostTagsForId(postid);
	// if (post != null) {
	// if (type == ADD_COMMENT)
	// post.setCommentCount(post.getCommentCount() + 1);
	// else if (type == DELETE_COMMENT)
	// post.setCommentCount(post.getCommentCount() - 1);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// tagPosts.remove(index);
	// tagPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// protected void changeMyPicsCommentProcess(int id, int type) {
	// final int index = getMyPicspostPosition(id);
	// final PostViewModel post = getMyPicsPostForId(id);
	// if (post != null) {
	// if (type == ADD_COMMENT)
	// post.setCommentCount(post.getCommentCount() + 1);
	// else if (type == DELETE_COMMENT)
	// post.setCommentCount(post.getCommentCount() - 1);
	// post.setMyPics(true);
	// UpdateWallDataBase(post, FragmentInfo.MY_PIC);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// myPicsPosts.remove(index);
	// myPicsPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// protected void changeWallCommentProcess(int id, int type) {
	// final int index = getWallpostPosition(id);
	// final PostViewModel post = getWallPostForId(id);
	// if (post != null) {
	// if (type == ADD_COMMENT)
	// post.setCommentCount(post.getCommentCount() + 1);
	// else if (type == DELETE_COMMENT)
	// post.setCommentCount(post.getCommentCount() - 1);
	// post.setWall(true);
	// UpdateWallDataBase(post, FragmentInfo.WALL);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// wallPosts.remove(index);
	// wallPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// protected void changeLatestCommentProcess(int id, int type) {
	// final int index = getLatestpostPosition(id);
	// final PostViewModel post = getLatestPostForId(id);
	// if (post != null) {
	// if (type == ADD_COMMENT)
	// post.setCommentCount(post.getCommentCount() + 1);
	// else if (type == DELETE_COMMENT)
	// post.setCommentCount(post.getCommentCount() - 1);
	// post.setLatest(true);
	// UpdateWallDataBase(post, FragmentInfo.LATEST_TAB);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// latestPosts.remove(index);
	// latestPosts.add(index, post);
	// }
	// });
	// }
	// }
	//
	// protected void changeFilteredCommentProcess(int id, int type) {
	// final int index = getFilterpostPosition(id);
	// final PostViewModel post = getFilterPostForId(id);
	// if (post != null) {
	// if (type == ADD_COMMENT)
	// post.setCommentCount(post.getCommentCount() + 1);
	// else if (type == DELETE_COMMENT)
	// post.setCommentCount(post.getCommentCount() - 1);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// filterList.remove(index);
	// filterList.add(index, post);
	// }
	// });
	// }
	// }
	//
	// private void changeTopTenCommentProcess(int postId, int type) {
	// final int index = getTopTenpostPosition(postId);
	// final PostViewModel post = getTopTenPostForId(postId);
	// if (post != null) {
	// if (type == ADD_COMMENT)
	// post.setCommentCount(post.getCommentCount() + 1);
	// else if (type == DELETE_COMMENT)
	// post.setCommentCount(post.getCommentCount() - 1);
	// App.getInstance().runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // Toast.makeText(context, "Done",
	// // Toast.LENGTH_SHORT).show();
	// topTen.remove(index);
	// topTen.add(index, post);
	// }
	// });
	// }
	// }

	protected void notifyCommentAdded(final UserComment comment) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener temp : getListeners())
					if (temp instanceof OnCommentsListReceived)
						((OnCommentAdded) temp).commentAdded(comment);
			}
		});
	}

	public void notifyMyPicsEmptyList() {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof MyPicsEmptyList) {
						((MyPicsEmptyList) listener).onMyPicsBecomeEmpty();
					}
				}

			}
		});
	}

	public List<UserComment> getCommentsFromServer(int postId, int pageNum)
			throws AppException {
		final Map<String, Object> params = prepareGetCommetnsParams(postId,
				pageNum);
		ArrayList<UserComment> comments = (ArrayList<UserComment>) getObjectList(
				URLs.Methods.GET_COMMENTS, params, UserComment.class);
		if (pageNum == 1) {
			CommentTable.getInstance().deleteAll();
			CommentTable.getInstance().insert(comments);
		} else {
			CommentTable.getInstance().insert(comments);
		}

		return comments;
	}

	public void getComments(final int photoId, final int pageNo) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					List<UserComment> offlineList = CommentTable.getInstance()
							.getPostsComments(photoId);
					if (!NetworkingUtils.isNetworkConnected(context))
						handleCommentsResponse(offlineList, 0);
					List<UserComment> comments = getCommentsFromServer(photoId,
							pageNo);
					if (NetworkingUtils.isNetworkConnected(context))
						handleCommentsResponse(comments, 0);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnCommentsListReceived.class);
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * @param photoId
	 * @param pageNo
	 * @return
	 */
	private Map<String, Object> prepareGetCommetnsParams(int photoId, int pageNo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Wall.POST_ID, photoId);
		params.put(Params.Wall.PAGE_NUM, pageNo);
		return params;
	}

	private void handleCommentsResponse(final List<UserComment> comments,
			final int pageNo) {
		if (!comments.isEmpty()) {
			App.getInstance().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					for (UiListener temp : getListeners())
						if (temp instanceof OnCommentsListReceived)
							((OnCommentsListReceived) temp)
									.onEntitiesListReceived(comments, pageNo);
				}
			});
		}
	}

	// public void deleteComment(final int postid, final int commentId) {
	// App.getInstance().runInBackground(new Runnable() {
	//
	// @Override
	// public void run() {
	// try {
	// performeDelete(postid, commentId);
	// if (!filterList.isEmpty())
	// changeFilteredCommentProcess(postid, DELETE_COMMENT);
	// if (!latestPosts.isEmpty())
	// changeLatestCommentProcess(postid, DELETE_COMMENT);
	// if (!wallPosts.isEmpty())
	// changeWallCommentProcess(postid, DELETE_COMMENT);
	// if (!myPicsPosts.isEmpty())
	// changeMyPicsCommentProcess(postid, DELETE_COMMENT);
	// if (!topTenSessons.isEmpty()) {
	// changeTopTenCommentProcess(postid, DELETE_COMMENT);
	// }
	// if (!tagPosts.isEmpty())
	// changeTagsCommentProcess(postid, DELETE_COMMENT);
	// } catch (Exception e) {
	// e.printStackTrace();
	// notifyRetrievalException(AppException.getAppException(e),
	// OnCommentDeleted.class);
	// }
	// }
	//
	// private void performeDelete(int postid, int commentId)
	// throws AppException {
	// Map<String, Object> parametrs = new HashMap<String, Object>();
	// parametrs.put(Params.Wall.POST_ID, postid);
	// parametrs.put(Params.Wall.COMMENT_ID, commentId);
	// BaseResponse response = submit(parametrs,
	// URLs.Methods.DELETECOMMENT);
	// if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID)
	// notifyCommentDeleted(postid, commentId);
	// if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
	// if (response.getStatus() ==
	// BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
	// throw new AppException(
	// response.getValidationRule().errorMessage);
	// else
	// throw new AppException(BaseResponse
	// .getExceptionType(response.getStatus()));
	// }
	// }
	// });
	// }

	protected void notifyCommentDeleted(final int postid, final int commentId) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener temp : getListeners())
					if (temp instanceof OnCommentDeleted)
						((OnCommentDeleted) temp).onCommentDeleted(postid,
								commentId);
			}
		});
	}

	// ------------ End Of Comments Part -----------/

	public void getCountryList() {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					// added by Mohga
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(Params.Common.LANG, LanguageUtils.getInstance()
							.getDeviceLanguage());

					List<Country> countries = getObjectList(
							URLs.Methods.GET_COUNTRIES, params, Country.class);
					Collections.sort(countries, new Comparator<Country>() {

						@Override
						public int compare(Country lhs, Country rhs) {
							return lhs.getCountryName().compareTo(
									rhs.getCountryName());
						}
					});
					notifyEntityListReceviedSuccess(countries,
							OnCountriesRecieved.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnCountriesRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	public void getLikeData(final int userId, final int postId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				Map<String, Object> parametrs = new HashMap<String, Object>();
				parametrs.put(Params.Wall.USER_ID, userId);
				parametrs.put(Params.Wall.POST_ID, postId);

				try {
					List<PostLike> offlineLikes = LikeTable.getInstance()
							.getAll();
					if (!NetworkingUtils.isNetworkConnected(context))
						notifyEntityListReceviedSuccess(offlineLikes,
								OnLikeRecieved.class);
					ArrayList<PostLike> likes = (ArrayList<PostLike>) getObjectList(
							URLs.Methods.GET_LIKE_DETAILS, parametrs,
							PostLike.class);
					if (NetworkingUtils.isNetworkConnected(context)) {
						LikeTable.getInstance().deleteAll();
						LikeTable.getInstance().insert(likes);
					}
					notifyEntityListReceviedSuccess(likes, OnLikeRecieved.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnLikeRecieved.class);
					e.printStackTrace();
				}
			}
		});
	}

	// public PostViewModel getEmptyPost() {
	// return EMPTY_POST;
	// }

	public WALLPosts getFanNewerPosts(final int fanId, long currentUpdateTime) {
		WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
				WebServiceRequestInfo.ACTION_UPDATE, UserManager.getInstance()
						.getCurrentUserId(), WallManager.NUM_OF_POSTS,
				currentUpdateTime, WebServiceRequestInfo.TIME_FILTER_NEW, 0,
				fanId, WebServiceRequestInfo.GET_USER_POSTS,
				"wall/getUserPosts");
		Action action = new Action(ActionType.GET_POSTS, requestInfo);
		try {
			if (!needsProcessing(action))
				return null;

			notifyActionStarted(action);
			WALLPosts wallPosts = getPosts(requestInfo);
			notifyActionDone(action);
			return wallPosts;
		} catch (Exception e) {
			notifyActionDone(action);
			e.printStackTrace();
			return null;
		}
	}

	public WALLPosts getFanOlderPosts(final int fanId, long lastUpdateTime) {
		WebServiceRequestInfo requestInfo = new WebServiceRequestInfo(
				WebServiceRequestInfo.ACTION_GETMORE, UserManager.getInstance()
						.getCurrentUserId(), WallManager.NUM_OF_POSTS,
				lastUpdateTime, WebServiceRequestInfo.TIME_FILTER_OLD, 0,
				fanId, WebServiceRequestInfo.GET_USER_POSTS,
				"wall/getUserPosts");
		Action action = new Action(ActionType.GET_POSTS, requestInfo);
		try {
			if (!needsProcessing(action))
				return null;

			notifyActionStarted(action);
			WALLPosts wallPosts = getPosts(requestInfo);
			notifyActionDone(action);
			return wallPosts;
		} catch (Exception e) {
			notifyActionDone(action);
			e.printStackTrace();
			return null;
		}
	}

	public WALLPosts getNewerPosts(long currentUpdateTime, String methodName,
			boolean isFiltered, boolean isFavorite) {
		WebServiceRequestInfo requestInfo;
		if (isFiltered) {
			requestInfo = new WebServiceRequestInfo(
					WebServiceRequestInfo.ACTION_UPDATE, UserManager
							.getInstance().getCurrentUserId(),
					WallManager.NUM_OF_POSTS, currentUpdateTime,
					WebServiceRequestInfo.TIME_FILTER_NEW, 0, 0,
					WebServiceRequestInfo.GET_POSTS, methodName, WallManager
							.getInstance().getCountriesList(),
					isFavorite ? WebServiceRequestInfo.TURN_ON_FILTER
							: WebServiceRequestInfo.TURN_OFF_FILTER);
		} else {
			requestInfo = new WebServiceRequestInfo(
					WebServiceRequestInfo.ACTION_UPDATE, UserManager
							.getInstance().getCurrentUserId(),
					WallManager.NUM_OF_POSTS, currentUpdateTime,
					WebServiceRequestInfo.TIME_FILTER_NEW, 0, 0,
					WebServiceRequestInfo.GET_POSTS, methodName);
			Action action = new Action(ActionType.GET_POSTS, requestInfo);
			try {
				if (!needsProcessing(action))
					return null;

				notifyActionStarted(action);
				WALLPosts wallPosts = getPosts(requestInfo);
				notifyActionDone(action);
				return wallPosts;
			} catch (Exception e) {
				notifyActionDone(action);
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	public WALLPosts getOlderPosts(long lastUpdateTime, String methodName,
			boolean isFiltered, boolean isFavorite) {
		WebServiceRequestInfo requestInfo;
		if (filtered) {
			requestInfo = new WebServiceRequestInfo(
					WebServiceRequestInfo.ACTION_GETMORE, UserManager
							.getInstance().getCurrentUserId(),
					WallManager.NUM_OF_POSTS, lastUpdateTime,
					WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
					WebServiceRequestInfo.GET_POSTS, methodName, WallManager
							.getInstance().getCountriesList(),
					isFavorite ? WebServiceRequestInfo.TURN_ON_FILTER
							: WebServiceRequestInfo.TURN_OFF_FILTER);
		} else {
			requestInfo = new WebServiceRequestInfo(
					WebServiceRequestInfo.ACTION_GETMORE, UserManager
							.getInstance().getCurrentUserId(),
					WallManager.NUM_OF_POSTS, lastUpdateTime,
					WebServiceRequestInfo.TIME_FILTER_OLD, 0, 0,
					WebServiceRequestInfo.GET_POSTS, methodName);

			Action action = new Action(ActionType.GET_POSTS, requestInfo);
			try {
				if (!needsProcessing(action))
					return null;

				notifyActionStarted(action);
				WALLPosts wallPosts = getPosts(requestInfo);
				notifyActionDone(action);
				return wallPosts;
			} catch (Exception e) {
				notifyActionDone(action);
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	// public static boolean isEmptyPost(PostViewModel post) {
	// return EMPTY_POST.equals(post);
	// }

	public boolean isInitialPhotoDetails() {
		return SharedPrefrencesDataLayer.getBooleanPreferences(App
				.getInstance().getApplicationContext(),
				PHOTO_DETAILS_INITIAL_STATE_KEY, false);
	}

	public void markPhotoDetailsInitialized() {
		SharedPrefrencesDataLayer
				.saveBooleanPreferences(App.getInstance()
						.getApplicationContext(),
						PHOTO_DETAILS_INITIAL_STATE_KEY, true);
	}

	public void filterIsDiselected() {
		filtered = false;
	}

	public void updateCommentCount(final int count) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnCommentScreenFinish) {
						((OnCommentScreenFinish) listener).getCount(count);
					}
				}
			}
		});
	}

	public void updateCount(int postId, int whichTab, int commentCount) {
		PostViewModel markedPost = new PostViewModel(postId);
		switch (whichTab) {
		case FragmentInfo.LATEST_TAB:
			index = latestPosts.indexOf(markedPost);
			if (index >= 0)
				post = latestPosts.get(index);
			break;
		case FragmentInfo.TOP_TEN:
			for (int x = 0; x < topTenSessons.size(); x++) {
				for (int j = 0; j < topTenSessons.get(x).getPosts().size(); j++) {
					if (topTenSessons.get(x).getPosts().get(j)
							.equals(markedPost)) {
						groupPosition = x;
						index = j;
						if (index >= 0)
							post = topTenSessons.get(x).getPosts().get(j);
						break;
					}
				}
			}
			break;
		case FragmentInfo.MY_PIC:
			index = myPicsPosts.indexOf(markedPost);
			if (index >= 0)
				post = myPicsPosts.get(index);
			break;
		case FragmentInfo.WALL:
			index = wallPosts.indexOf(markedPost);
			if (index >= 0)
				post = wallPosts.get(index);
			break;
		case FragmentInfo.Filtered:
			index = filterList.indexOf(markedPost);
			if (index >= 0)
				post = filterList.get(index);
			break;
		// case FragmentInfo.FULL_SCREEN:
		// index = postFullScreen.indexOf(markedPost);
		// post = postFullScreen.get(index);
		// break;
		case FragmentInfo.TAG_LIST:
			index = tagPosts.indexOf(markedPost);
			if (index >= 0)
				post = tagPosts.get(index);
			break;
		default:
			break;
		}
		if (post != null) {
			post.setCommentCount(commentCount);
			UpdateWallDataBase(post, whichTab);
		}
	}

	public void notifyPostFullScreen(final List<Integer> actions,
			final PostViewModel modifiedPost) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof PostFullScreenListener) {
						((PostFullScreenListener) listener).handle(actions,
								modifiedPost);
					}
				}
			}
		});
	}

	public void getStudioPhoto(final int studioPhotoId , final Activity activity) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					String userCredit = "";
					StudioPhoto photo = getStudioPhotoFromServer(studioPhotoId);
					if (photo.getIsPurchased() == 1) {
						notifyStudioPhotoRecieved(photo);
					} else {
						if (photo.getRequiredPoints() > 0) {
							userCredit = executeGetUserCredit();
							if (Integer.parseInt(userCredit) > photo
									.getRequiredPoints()) {
								if (photo.getIsPermium() == 1) {
									if (UserManager.getInstance()
											.getCurrentUser().isPremium())
										notifyStudioPhotoRecieved(photo);
									else{
										notifyStudioPhotoException("This photo require a premium user");
										activity.startActivity(ShopActivityTabs
												.getIntent(activity,
														ShopActivityTabs.PREMIUM_TAB));
									}
								} else {
									notifyStudioPhotoRecieved(photo);
								}
							} else {
								notifyStudioPhotoException("You don't seem to have enough credit to use this photo");
								activity.startActivity(ShopActivityTabs
										.getIntent(activity,
												ShopActivityTabs.POINTS_PACKAGES));
							}
						} else {
							if (photo.getIsPermium() == 1) {
								if (UserManager.getInstance().getCurrentUser()
										.isPremium())
									notifyStudioPhotoRecieved(photo);
								else{
									notifyStudioPhotoException("This photo require a premium user");
									activity.startActivity(ShopActivityTabs
											.getIntent(activity,
													ShopActivityTabs.PREMIUM_TAB));
								}
							} else {
								notifyStudioPhotoRecieved(photo);
							}
						}
					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnStudioPhotoRecievedForUse.class);
					e.printStackTrace();
				}
			}
		});
	}

	protected void notifyStudioPhotoRecieved(final StudioPhoto photo) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnStudioPhotoRecievedForUse) {
						((OnStudioPhotoRecievedForUse) listener)
								.onStudioPhotoRecieved(photo);
					}
				}
			}
		});
	}

	protected void notifyStudioPhotoException(final String msg) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnStudioPhotoRecievedForUse) {
						((OnStudioPhotoRecievedForUse) listener)
								.onStudioPhotoRecievedFailed(msg);
					}
				}
			}
		});
	}

	public StudioPhoto getStudioPhotoFromServer(int studioPhotoId)
			throws AppException {
		Map<String, Object> parametrs = new HashMap<String, Object>();
		parametrs.put(Params.Studio.STUDIO_PHOTO_ID, studioPhotoId);
		parametrs.put(Params.Studio.USER_ID, UserManager.getInstance().getCurrentUserId());

		List<StudioPhoto> photo = getObjectList(
				URLs.Methods.GET_Studio_photo_Details, parametrs,
				StudioPhoto.class);
		return photo.get(0);

	}

	/* get the user credit and notify listener */
	private String executeGetUserCredit() throws AppException {
		String userCredit = "-1";
		if (UserManager.getInstance().getCurrentUserId() != 0) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(Params.Common.LANG, LanguageUtils.getInstance()
					.getDeviceLanguage());
			params.put(Params.User.USER_ID, UserManager.getInstance()
					.getCurrentUserId());
			BaseResponse response = submit(params, URLs.Methods.GET_USER_CREDIT);

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
				try {
					userCredit = new JSONObject(response.getData())
							.getString("credit");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {
				if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
					throw new AppException(
							response.getValidationRule().errorMessage);
				else
					throw new AppException(
							BaseResponse.getExceptionType(response.getStatus()));
			}
		}
		return userCredit;
	}

	public void editPost(final PostViewModel post) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					BaseResponse response;
					String path = post.getPostPhotoUrl();
					post.setPostPhotoUrl("");
					post.setPostId(post.getPostId());
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(Params.Share.POST,
							DataHelper.serialize(post, PostViewModel.class));

					response = submit(params, new File(path),
							URLs.Methods.EDIT_POST);

					if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
						PostViewModel post = DataHelper.deserialize(
								response.getData(), PostViewModel.class);
						notifyShare(post);
						notifyPostEdited(post);
					} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

						if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
							throw new AppException(
									response.getValidationRule().errorMessage);
						else
							throw new AppException(BaseResponse
									.getExceptionType(response.getStatus()));

					}
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnShareListener.class);
					e.printStackTrace();
				}
			}
		});
	}

	public void notifyPostEdited(final PostViewModel post) {
		App.getInstance().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (!filterList.isEmpty())
					notifyEditDone(post, FragmentInfo.Filtered);
				if (!latestPosts.isEmpty())
					notifyEditDone(post, FragmentInfo.LATEST_TAB);
				if (!myPicsPosts.isEmpty())
					notifyEditDone(post, FragmentInfo.MY_PIC);
				if (!wallPosts.isEmpty())
					notifyEditDone(post, FragmentInfo.WALL);
				if (!topTenSessons.isEmpty())
					notifyEditDone(post, FragmentInfo.TOP_TEN);
				// if (!postFullScreen.isEmpty())
				// deletePostFromLists(post_id, FragmentInfo.FULL_SCREEN);
				if (!tagPosts.isEmpty())
					notifyEditDone(post, FragmentInfo.TAG_LIST);
			}
		});
	}

	protected void notifyEditDone(PostViewModel post, int whichTab) {
		PostViewModel markedPost = new PostViewModel(post.getPostId());
		switch (whichTab) {
		case FragmentInfo.LATEST_TAB:
			index = latestPosts.indexOf(markedPost);
			if (index >= 0) {
				latestPosts.remove(index);
				latestPosts.add(index, post);
			}
			break;
		case FragmentInfo.TOP_TEN:
			for (int x = 0; x < topTenSessons.size(); x++) {
				for (int j = 0; j < topTenSessons.get(x).getPosts().size(); j++) {
					if (topTenSessons.get(x).getPosts().get(j)
							.equals(markedPost)) {
						groupPosition = x;
						index = j;
						if (index >= 0) {
							topTenSessons.get(x).getPosts().remove(index);
							topTenSessons.get(x).getPosts().add(index, post);
						}
						break;
					}
				}
			}
			break;
		case FragmentInfo.MY_PIC:
			index = myPicsPosts.indexOf(markedPost);
			if (index >= 0) {
				myPicsPosts.remove(index);
				myPicsPosts.add(index, post);
			}
			break;
		case FragmentInfo.WALL:
			index = wallPosts.indexOf(markedPost);
			if (index >= 0) {
				wallPosts.remove(index);
				wallPosts.add(index, post);
			}
			break;
		case FragmentInfo.Filtered:
			index = filterList.indexOf(markedPost);
			if (index >= 0) {
				filterList.remove(index);
				filterList.add(index, post);
			}
			break;
		// case FragmentInfo.FULL_SCREEN:
		// index = postFullScreen.indexOf(markedPost);
		// post = postFullScreen.get(index);
		// break;
		case FragmentInfo.TAG_LIST:
			index = tagPosts.indexOf(markedPost);
			if (index >= 0) {
				tagPosts.remove(index);
				tagPosts.add(index, post);
			}
			break;
		default:
			break;
		}
		UpdateWallDataBase(post, whichTab);
	}

	// notify the share listener
	private void notifyShare(final PostViewModel post) {
		App.getInstance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (UiListener listener : getListeners()) {
					if (listener instanceof OnShareListener) {
						((OnShareListener) listener).onSuccess(post);
					}
				}
			}
		});
	}

	public void getTags(final int postId , final int studioPhotoId) {
		App.getInstance().runInBackground(new Runnable() {

			@Override
			public void run() {
				try {
					// added by mohga
					// List<KeyValuePairs> tags = getTagsFromServer(postId);
					TagsData tagsData = getTagsFromServer(postId , studioPhotoId);
					notifyEntityReceviedSuccess(tagsData,
							OnTagsDataReceived.class);
				} catch (Exception e) {
					notifyRetrievalException(AppException.getAppException(e),
							OnTagsDataReceived.class);
					e.printStackTrace();
				}
			}
		});
	}

	protected TagsData getTagsFromServer(int postId , int studioPhotoId) throws AppException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Wall.POST_ID, postId);
		params.put(Params.Studio.STUDIO_PHOTO_ID, studioPhotoId); // added by mohga

		TagsData tagsData = getObject(params, URLs.Methods.GET_TAGS,
				TagsData.class);
		// List<KeyValuePairs> tags = getObjectList(URLs.Methods.GET_TAGS,
		// params,
		// KeyValuePairs.class);
		return tagsData;
	}

	public void EmailVerified() {
		User user = UserManager.getInstance().getCurrentUser();
		user.setEmailVerified(true);
		UserManager.getInstance().cacheUser(user);
		notifyEmailverified();
	}

	private void notifyEmailverified() {
		notifyVoidSuccess(OnEmailVerify.class);
	}

	public List<UserContacts> sendContactsToServer(int pageNum,
			List<UserContacts> contactsToSend) throws AppException {
		List<UserContacts> returnedContacts = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Params.Wall.USER_ID, UserManager.getInstance()
				.getCurrentUserId());
		params.put(Params.Wall.PAGE_NUM, pageNum);
		params.put(Params.Wall.CONTACT_LIST, contactsToSend);

		BaseResponse response = submit(params,
				URLs.Methods.FILTER_USER_CONTACTS);
		if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_SUCCES_WITH_DATA) {
			returnedContacts = DataHelper.deserializeList(response.getData(),
					UserContacts.class);
			if (returnedContacts.isEmpty())
				throw new AppException(AppException.NO_DATA_EXCEPTION);
		} else if (response.getStatus() != BaseResponse.STATUS_WEBSERVICE_SUCCES_VOID) {

			if (response.getStatus() == BaseResponse.STATUS_WEBSERVICE_VALIDATION_RULE_ERROR)
				throw new AppException(
						response.getValidationRule().errorMessage);
			else
				throw new AppException(BaseResponse.getExceptionType(response
						.getStatus()));

		}

		return returnedContacts;

	}
}
