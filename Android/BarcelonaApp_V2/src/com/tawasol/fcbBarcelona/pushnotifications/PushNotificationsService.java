package com.tawasol.fcbBarcelona.pushnotifications;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.entities.FriendsEntity;
import com.tawasol.fcbBarcelona.entities.SystemMessagesEntity;
import com.tawasol.fcbBarcelona.entities.User;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.managers.NotificationsManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.managers.WallManager;
import com.tawasol.fcbBarcelona.ui.activities.ChatActivity;
import com.tawasol.fcbBarcelona.ui.activities.ChatViewActivity;

public class PushNotificationsService extends IntentService {

	private static final String NOTIFICATION_MESSAGE_KEY = "type"; // TODO:
																	// update
																	// value
																	// when
																	// added to
																	// SDS

	public PushNotificationsService() {
		super("PushNotificationsService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
	}

}