package com.tawasol.fcbBarcelona.application;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.impl.ext.LruDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.utils.L;
import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.data.cache.DatabaseManager;
import com.tawasol.fcbBarcelona.data.cache.SharedPrefrencesDataLayer;
import com.tawasol.fcbBarcelona.data.cache.StudioFoldersTable;
import com.tawasol.fcbBarcelona.data.cache.StudioPhotosTable;
import com.tawasol.fcbBarcelona.data.cache.UserContactsTable;
import com.tawasol.fcbBarcelona.data.connection.MySSLSocketFactory;
import com.tawasol.fcbBarcelona.data.parsing.ParsingManager;
import com.tawasol.fcbBarcelona.managers.ChatManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.pushnotifications.NotificationsReceiver;
import com.tawasol.fcbBarcelona.pushnotifications.NotificationsService;
import com.tawasol.fcbBarcelona.responses.BaseResponse;
import com.tawasol.fcbBarcelona.ui.activities.SplashActivity;
import com.tawasol.fcbBarcelona.utils.LanguageUtils;
import com.tawasol.fcbBarcelona.utils.TypefaceUtil;

public class App extends Application {

	public static final String LANG_CHANGED = "langChanged";
	private static final int THREAD_POOL_SIZE = 3;
	private static final String VERSION_KEY = "versionKey";

	private ImageLoader imageLoader = ImageLoader.getInstance();
	private DisplayImageOptions option;
	private Handler mHandler;
	private ExecutorService mExecutor;

	private static final String PROPERTY_ID = "UA-62916651-1";

	// Logging TAG
	private static final String TAG = "MyApp";

	public static int GENERAL_TRACKER = 0;
	
	public static boolean appEnterBackGround = false;

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
						// roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
							// company.
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	ForegroundStatus.Listener backgroundListener = new ForegroundStatus.Listener() {
		@Override
		public void onBecameForeground() {
			changeChatStatus(true);
		}

		@Override
		public void onBecameBackground() {
			changeChatStatus(false);
		}
	};

	public synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics
					.newTracker(R.xml.app_tracker)
					: (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics
							.newTracker(PROPERTY_ID) : analytics
							.newTracker(R.xml.ecommerce_tracker);
			mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		barchelona = this;
		mHandler = new Handler();


		
		clearApplicationData();
		// Foreground init
		ForegroundStatus.init(this);
		ForegroundStatus.get().addListener(backgroundListener);
		DatabaseManager.getInstance(getApplicationContext()).initializeTables();
		// Image loader init
		initImageLoader(getApplicationContext());

		// Thread pool init
		mExecutor = Executors.newScheduledThreadPool(THREAD_POOL_SIZE,
				new ThreadFactory() {
					@Override
					public Thread newThread(Runnable runnable) {
						Thread thread = new Thread(runnable,
								"Background executor");
						thread.setPriority(Thread.MIN_PRIORITY);
						thread.setDaemon(true);
						return thread;
					}
				});

		// set the Typeface font for textViews , added by Mohga
		TypefaceUtil.overrideFont(getApplicationContext(), "SANS",
				"fonts/ufonts_com_interstate_light.ttf");


		if (UserManager.getInstance().getCurrentUserId() != 0) {
			UserContactsTable.getInstance().deleteAll();

		}
		
	}


	protected void changeChatStatus(boolean online) {
		// Only change status if the user is logged in
		if (UserManager.getInstance().getCurrentUserId() != 0)
			ChatManager.getInstance().changeStatusAsync(online ? 1 : 0);
	}

	@SuppressWarnings("deprecation")
	public void initImageLoader(Context context) {
		LruDiscCache cache = null;
		try {
			cache = new LruDiscCache(getApplicationContext().getCacheDir(),
					new Md5FileNameGenerator(), 50 * 1024 * 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		option = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error)
				.resetViewBeforeLoading(true)
				.cacheOnDisc(true)
				// .cacheOnDisk(true)
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300)).build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).threadPriority(Thread.NORM_PRIORITY)
				.denyCacheImageMultipleSizesInMemory()
				// .discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.FIFO)
				.diskCache(cache)
				.defaultDisplayImageOptions(option).build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);

	}

	public DisplayImageOptions getDisplayOption() {
		return option;
	}

	public ImageLoader getImageLoader() {
		return imageLoader;
	}

	public void runOnUiThread(Runnable runnable) {
		mHandler.post(runnable);
	}

	public void runInBackground(final Runnable runnable) {
		mExecutor.submit(new Runnable() {
			@Override
			public void run() {
				try {
					runnable.run();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static App barchelona;

	public static App getInstance() {
		return barchelona;
	}

	public void clearApplicationData() {
		String language = SharedPrefrencesDataLayer.getStringPreferences(
				getApplicationContext(), LanguageUtils.APP_LANGUAGE_CODE_KEY,
				LanguageUtils.LANGUAGE_ENGLISH);
		System.out.println("Clear Data");
		DatabaseManager.getInstance(barchelona).clearAppDatabase();
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String s : children) {
				System.out.println("- " + s);
				if (!s.equalsIgnoreCase("lib")
						&& !s.equalsIgnoreCase("databases")) {
					deleteDir(new File(appDir, s));
					Log.i("TAG", "File /data/data/APP_PACKAGE/" + s
							+ " DELETED");
				}
			}
		}
		SharedPrefrencesDataLayer.saveStringPreferences(
				getApplicationContext(), LanguageUtils.APP_LANGUAGE_CODE_KEY,
				language);
		SharedPrefrencesDataLayer.saveBooleanPreferences(this, SplashActivity.FCB_FIRST_INSTALL, false);
	}

	private static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();

			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}
}
