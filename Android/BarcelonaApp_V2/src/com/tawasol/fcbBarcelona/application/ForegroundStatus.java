package com.tawasol.fcbBarcelona.application;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import android.app.Activity;
import android.app.Application;
import android.os.Handler;
import android.util.Log;

public class ForegroundStatus {
	
	public static final long CHECK_DELAY = 200000;
	public static final String TAG = ForegroundStatus.class.getName();

	public interface Listener {

		public void onBecameForeground();

		public void onBecameBackground();

	}

	private static ForegroundStatus instance;

	private boolean foreground = false, paused = true;
	private Handler handler = new Handler();
	private List<Listener> listeners = new CopyOnWriteArrayList<Listener>();
	private Runnable check;

	/**
	 * @param application
	 * @return an initialised Foreground instance
	 */
	public static ForegroundStatus init(Application application) {
		if (instance == null) {
			instance = new ForegroundStatus();
		}
		return instance;
	}

	protected static ForegroundStatus get(Application application) {
		if (instance == null) {
			init(application);
		}
		return instance;
	}

	public static ForegroundStatus get() {
		if (instance == null) {
			throw new IllegalStateException(
					"Foreground is not initialised - invoke "
							+ "at least once with parameterised init/get");
		}
		return instance;
	}

	public boolean isForeground() {
		return foreground;
	}

	public boolean isBackground() {
		return !foreground;
	}

	public void addListener(Listener listener) {
	}

	public void removeListener(Listener listener) {
	}

	public void onActivityResumed(Activity activity) {

	}

	public void onActivityPaused(Activity activity) {
	
	}

}
