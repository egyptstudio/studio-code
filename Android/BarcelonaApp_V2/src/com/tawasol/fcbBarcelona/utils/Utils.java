package com.tawasol.fcbBarcelona.utils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.application.App;

public class Utils {
	
	
	public static boolean isLandscape() {
		Context cxt = App.getInstance().getApplicationContext();
		return cxt.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
	}
	
	public static boolean isPortrait() {
		return !isLandscape();
	}

	/** Delete file **/
	public static boolean deleteFileNoThrow(String path) {
		File file;
		try {
			file = new File(path);
		} catch (NullPointerException e) { return false;}
		
		if (file.exists()) 
			return file.delete();
		return false;
	}
	
	/** Generate random number **/
	public static int randomNumber(int max){
		Random random = new Random();
		return random.nextInt(max);
	}
	
	public static long dateStringToLong(String date)
	{
		if(ValidatorUtils.isRequired(date))
			return 0;
		SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");
		Date d= null;
		try {
			d = f.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return d.getTime()/1000;
	}
	public static boolean isFieldEmpty(String editableField)
	{
		return !(editableField != null && editableField.trim().length()>0) ?true :false;
	}
	
	/**
	 * Opens Google Play store on our application so the user can update the application and closes the passed in activity.
	 * @param activity
	 */
	public static void updateAppAction(FragmentActivity activity) {
		String packageName = activity.getPackageName();
		Intent updateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
		// See if Google Play Store is installed
		if (updateIntent.resolveActivity(activity.getPackageManager()) == null) {
			// Google Play Store is not installed, try with the browser
			updateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
			if (updateIntent.resolveActivity(activity.getPackageManager()) == null)
				// Browser is not installed, just show an error Toast
				updateIntent = null;
		}
		if (updateIntent != null)
			activity.startActivity(updateIntent);
		else
			UIUtils.showToast(activity, activity.getString(R.string.update_app_not_found));
		activity.finish();
	}
	
}
