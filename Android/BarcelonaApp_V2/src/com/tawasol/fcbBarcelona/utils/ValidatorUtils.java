package com.tawasol.fcbBarcelona.utils;

public class ValidatorUtils {

	public static boolean isValidEmail(CharSequence email) {
		if (email == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
					.matches();
		}
	}
	
	public static boolean isRequired(CharSequence field) {
		if (field == null || field.length() == 0 || field.equals(""))
			return true;
		else
			return false;
	}

}
