package com.tawasol.fcbBarcelona.utils;

import com.android.vending.billing.util.BarcaIabHelper;
import com.android.vending.billing.util.BarcaIabHelper.OnIabSetupFinishedListener;
import com.android.vending.billing.util.BarcaIabResult;
import com.tawasol.fcbBarcelona.data.connection.Params;
import com.tawasol.fcbBarcelona.entities.FragmentInfo;
import com.tawasol.fcbBarcelona.entities.WebServiceRequestInfo;
import com.tawasol.fcbBarcelona.managers.ShopManager;
import com.tawasol.fcbBarcelona.managers.UserManager;
import com.tawasol.fcbBarcelona.managers.WallManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkChangeReceiver extends BroadcastReceiver implements
		OnIabSetupFinishedListener {

	private BarcaIabHelper billingHelper;
	boolean isConnected;

	@Override
	public void onReceive(Context context, Intent intent) {
		ConnectivityManager connMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		isConnected = wifi != null && wifi.isConnectedOrConnecting()
				|| mobile != null && mobile.isConnectedOrConnecting();
	}

	@Override
	public void onIabSetupFinished(BarcaIabResult result) {
		// TODO Auto-generated method stub
		
	}

}
