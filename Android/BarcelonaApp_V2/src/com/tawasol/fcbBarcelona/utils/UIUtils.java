package com.tawasol.fcbBarcelona.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.InputType;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.Toast;

import com.tawasol.fcbBarcelona.R;
import com.tawasol.fcbBarcelona.ui.dialogs.CustomAlertDialog;
import com.tawasol.fcbBarcelona.ui.dialogs.DatePickerDialogFragment;

public class UIUtils {

	public static android.support.v4.app.DialogFragment getDatePicker(Date date,
			OnDateSetListener onDateSetListener) {
		return new DatePickerDialogFragment(date, onDateSetListener);

	}

	public static void showToast(Context context, String message) {
		Toast.makeText(context/*App.getInstance().getApplicationContext()*/, message, Toast.LENGTH_LONG).show();
	}
	
	/**
	 * Disable soft keyboard from appearing, use in conjunction with android:windowSoftInputMode="stateAlwaysHidden|adjustNothing"
	 * @param editText
	 */
	@SuppressLint("NewApi")
	public static void disableSoftInputFromAppearing(EditText editText) {
	    if (Build.VERSION.SDK_INT >= 11) {
	        editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
	        editText.setTextIsSelectable(true);
	    } else {
	        editText.setRawInputType(InputType.TYPE_NULL);
	        editText.setFocusable(true);
	    }
	}
	
	public static void showUpdatePopup(Context context, final Runnable updateAction) {
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				updateAction.run();
			}
		};
		Dialog dialog = new CustomAlertDialog.customBuilder(context)
				.setTitle(R.string.update)
				.setTitleGravity(Gravity.CENTER_HORIZONTAL)
				.setMessage(R.string.update_msg)
				.setMessageGravity(Gravity.CENTER)
				.setPositiveButton(R.string.update_now, listener)
				.create();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

//	public static int getActionBarHeight(Context context) {
//		int actionBarHeight = 0;
//		TypedValue tv = new TypedValue();
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//			if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv,
//					true))
//				actionBarHeight = TypedValue.complexToDimensionPixelSize(
//						tv.data, context.getResources().getDisplayMetrics());
//		} else {
//			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,
//					context.getResources().getDisplayMetrics());
//		}
//		return actionBarHeight;
//	}
	
	/**
	 * Provides a formatter object to be used across the application, which uses this pattern <code>yyyy-MM-dd</code>
	 * @return
	 */
	public static SimpleDateFormat getDateFormatter() {
		return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	}
	
	
	public static boolean isAlphaSpeed(String name) {
	    char[] chars = name.toCharArray();

	    for (char c : chars) {
	        if(!Character.isLetter(c)) {
	            return false;
	        }
	    }
	    return true;
	}
	

	public static boolean isAlphaSimplicity(String name) {
	    return name.matches("[a-zA-Z]+");
	}
	
	
	public static boolean isFullNameAlphabtic(String name) {
		String str = name;
		for (int i = 0; i < str.length(); i++) {
		   char ch = str.charAt(i);
		   if(Character.isLetter(ch) || Character.isSpaceChar(ch)){
			   continue;
		   }else{
			   return false;
		   }
		}
		return true;
	}	
	
}
