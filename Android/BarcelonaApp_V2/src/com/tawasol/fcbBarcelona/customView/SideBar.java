package com.tawasol.fcbBarcelona.customView;

import com.tawasol.fcbBarcelona.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.SectionIndexer;

/**
 * 
 * @author Basyouni
 *
 */
public class SideBar extends View {
	private char[] l;
	private SectionIndexer sectionIndexter = null;
	private ExpandableListView expandablelist;
	private ListView listView;
	private int m_nItemHeight = 30;
	Context context;
	boolean expandableList;
	boolean contacs;

	// HashMap<String, List<String>> child;
	public SideBar(Context context) {
		super(context);
		this.context = context;
		init();
	}

	public SideBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.SideBar);
		if (ta != null) {
			contacs = ta.getBoolean(R.styleable.SideBar_cotacts, false);
		}
		ta.recycle();
		init();
	}

	public void type(View v) {

	}

	private void init() {
			l = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
					'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
					'W', 'X', 'Y', 'Z' };
	}

	public SideBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}

	public void setListView(ListView _list, boolean expandable) {
		this.expandableList = expandable;
		listView = _list;
		sectionIndexter = (SectionIndexer) _list.getAdapter();
	}

	public void setListView(ExpandableListView _list, boolean expandable) {
		this.expandableList = expandable;
		expandablelist = _list;
		// this.child = child;
		// HeaderViewListAdapter ha = (HeaderViewListAdapter) _list
		// .getAdapter();
		// MyAdapter ad = (MyAdapter)ha.getWrappedAdapter();
		// sectionIndexter = (SectionIndexer) ad;
		if (expandableList)
			sectionIndexter = (SectionIndexer) _list.getExpandableListAdapter();
		else
			sectionIndexter = (SectionIndexer) _list.getAdapter();
	}

	@SuppressLint("ClickableViewAccessibility")
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		if (isEnabled()) {
			int i = (int) event.getY();
			int idx = i / m_nItemHeight;
			if (idx >= l.length) {
				idx = l.length - 1;
			} else if (idx < 0) {
				idx = 0;
			}
			if (event.getAction() == MotionEvent.ACTION_DOWN
					|| event.getAction() == MotionEvent.ACTION_MOVE) {
				if (sectionIndexter == null) {
					if (expandableList)
						sectionIndexter = (SectionIndexer) expandablelist
								.getAdapter();
					else
						sectionIndexter = (SectionIndexer) listView
								.getAdapter();
				}
			}
			int position = sectionIndexter.getPositionForSection(l[idx]);
			if (position == -1) {
				return true;
			}

			// list.setSelection(position);
			// mExListView.setSelection(position);
			if (expandableList) {
				expandablelist.setSelectedGroup(position);
				expandablelist.expandGroup(position);
			} else {
				listView.setSelection(position);
			}

			// if (child.get(position) != null &&
			// !child.get(position).isEmpty())
			// try {

			// } catch (Exception exception) {
			//
			// }
		}
		return true;
	}

	@SuppressLint("DrawAllocation")
	protected void onDraw(Canvas canvas) {
		Paint paint = new Paint();
		paint.setColor(getResources().getColor(android.R.color.white));
		int x = 0;
		if (this.getHeight() < 500)
			x = (this.getHeight()) / (l.length);
		else
			x = (this.getHeight()) / (l.length + 1);
		if (this.getHeight() < 500)
			paint.setTextSize(9);
		else
			paint.setTextSize(20);
		paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

		m_nItemHeight = x;
		paint.setTextAlign(Paint.Align.CENTER);
		float widthCenter = getMeasuredWidth() / 2;
		for (int i = 0; i < l.length; i++) {
			canvas.drawText(String.valueOf(l[i]), widthCenter, m_nItemHeight
					+ (i * m_nItemHeight), paint);
		}
		super.onDraw(canvas);
	}
}