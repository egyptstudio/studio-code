package com.tawasol.fcbBarcelona.customView;

import com.tawasol.fcbBarcelona.R;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
//import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
//import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

/**
 * Quickaction window.
 * 
 * @author Turki
 *
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class QuickAction extends PopupWindows {
	private ImageView mArrowUp;
	private ImageView mArrowDown;
	private Animation mTrackAnim;
	private LayoutInflater inflater;
	private ViewGroup mTrack;
	private OnActionItemClickListener mListener;

	private int animStyle;
	private int mChildPos;
	private boolean animateTrack;

	/**
	 * Constructor.
	 * 
	 * @param context
	 *            Context
	 */
	public QuickAction(Context context) {
		super(context);

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mTrackAnim = AnimationUtils.loadAnimation(context,
				R.anim.grow_from_bottom);

		mTrackAnim.setInterpolator(new Interpolator() {
			public float getInterpolation(float t) {
				// Pushes past the target area, then snaps back into place.
				// Equation for graphing: 1.2-((x*1.6)-1.1)^2
				final float inner = (t * 1.55f) - 1.1f;

				return 1.2f - inner * inner;
			}
		});

		setRootViewId(R.layout.quickaction);

		animateTrack = true;
		mChildPos = 0;
	}

	/**
	 * Set root view.
	 * 
	 * @param id
	 *            Layout resource id
	 */
	public void setRootViewId(int id) {
		mRootView = (ViewGroup) inflater.inflate(id, null);
		mTrack = (ViewGroup) mRootView.findViewById(R.id.tracks);

		mArrowDown = (ImageView) mRootView.findViewById(R.id.arrow_down);
		mArrowUp = (ImageView) mRootView.findViewById(R.id.arrow_up);

		setContentView(mRootView);
	}

	/**
	 * Animate track.
	 * 
	 * @param animateTrack
	 *            flag to animate track
	 */
	public void animateTrack(boolean animateTrack) {
		this.animateTrack = animateTrack;
	}

	/**
	 * Set animation style.
	 * 
	 * @param animStyle
	 *            animation style, default is set to ANIM_AUTO
	 */
	public void setAnimStyle(int animStyle) {
		this.animStyle = animStyle;
	}

	/**
	 * Add action item
	 * 
	 * @param action
	 *            {@link ActionItem}
	 */
	@SuppressLint("InflateParams")
	public void addActionItem(ActionItem action) {

		String title = action.getTitle();
		String content = action.getContent();

		View container = (View) inflater.inflate(R.layout.action_item, null);

		TextView conten = (TextView) container.findViewById(R.id.tv_content);
		TextView text = (TextView) container.findViewById(R.id.tv_title);

		if (content != null)
			conten.setText(content);
		else
			conten.setVisibility(View.GONE);

		if (title != null)
			text.setText(title);
		else
			text.setVisibility(View.GONE);

		final int pos = mChildPos;

		container.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener != null)
					mListener.onItemClick(pos);

				dismiss();
			}
		});

		container.setFocusable(true);
		container.setClickable(true);

		mTrack.addView(container, mChildPos + 1);

		mChildPos++;
	}

	public void setOnActionItemClickListener(OnActionItemClickListener listener) {
		mListener = listener;
	}

	/**
	 * Show popup mWindow
	 */
	@SuppressWarnings("deprecation")
	public void show(View anchor, boolean top) {
		preShow();

		int[] location = new int[2];

		anchor.getLocationOnScreen(location);

		Rect anchorRect = new Rect(location[0], location[1], location[0]
				+ anchor.getWidth(), location[1] + anchor.getHeight());


		mRootView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		mRootView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		int rootWidth = mRootView.getMeasuredWidth();
		int rootHeight = mRootView.getMeasuredHeight();

		int screenWidth = mWindowManager.getDefaultDisplay().getWidth();
		// int screenHeight = mWindowManager.getDefaultDisplay().getHeight();

		int xPos = 0;
		int yPos = 0;
		boolean onTop;
		if (top) {
			xPos = /* (screenWidth - rootWidth) / 2 */anchorRect.left;
			yPos = anchorRect.top - rootHeight - 10;

			onTop = true;
		}else{
			xPos = (screenWidth - rootWidth) / 2 - 20;
			 yPos = anchorRect.bottom;
			 onTop = false;
		}
		

		showArrow(((onTop) ? R.id.arrow_down : R.id.arrow_up),
				anchor.getLeft() , top , 0);

		mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, xPos, yPos);

		if (animateTrack)
			mTrack.startAnimation(mTrackAnim);
	}

	/**
	 * Show popup mWindow
	 */
	@SuppressWarnings("deprecation")
	public void show(View anchor,int parentLeftMargin ,  boolean top) {
		preShow();

		int[] location = new int[2];

		anchor.getLocationOnScreen(location);

		Rect anchorRect = new Rect(location[0], location[1], location[0]
				+ anchor.getWidth(), location[1] + anchor.getHeight());


		mRootView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		mRootView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		int rootWidth = mRootView.getMeasuredWidth();
		int rootHeight = mRootView.getMeasuredHeight();

		int screenWidth = mWindowManager.getDefaultDisplay().getWidth();
		// int screenHeight = mWindowManager.getDefaultDisplay().getHeight();

		int xPos = 0;
		int yPos = 0;
		boolean onTop;
		if (top) {
			xPos = /* (screenWidth - rootWidth) / 2 */anchorRect.left;
			yPos = anchorRect.top - rootHeight - 10;

			onTop = true;
		}else{
			xPos = (screenWidth - rootWidth) / 2 - 40;
			 yPos = anchorRect.bottom;
			 onTop = false;
		}
		

//		showArrow(((onTop) ? R.id.arrow_down : R.id.arrow_up),
//				anchorRect.centerX() , top , parentLeftMargin);

		mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, xPos, yPos);

		if (animateTrack)
			mTrack.startAnimation(mTrackAnim);
	}

	/**
	 * Show arrow
	 * 
	 * @param whichArrow
	 *            arrow type resource id
	 * @param requestedX
	 *            distance from left screen
	 */
	private void showArrow(int whichArrow, final int requestedX , final boolean show , final int parentLeftMargin) {
		final View showArrow = (whichArrow == R.id.arrow_up) ? mArrowUp
				: mArrowDown;
		final View hideArrow = (whichArrow == R.id.arrow_up) ? mArrowDown
				: mArrowUp;

//		final int arrowWidth = mArrowUp.getMeasuredWidth();
		final ViewTreeObserver observer = mArrowUp.getViewTreeObserver();
		observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			
			@Override
			public void onGlobalLayout() {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					mArrowUp.getViewTreeObserver()
							.removeOnGlobalLayoutListener(this);
				} else {
					mArrowUp.getViewTreeObserver()
							.removeGlobalOnLayoutListener(this);
				}
				MarginLayoutParams param = (MarginLayoutParams) showArrow
						.getLayoutParams();

//				param.leftMargin = 850;
				param.leftMargin = requestedX - mArrowUp.getWidth() - parentLeftMargin;
				showArrow.setLayoutParams(param);
				if(!show)
					showArrow.setVisibility(View.VISIBLE);


				hideArrow.setVisibility(View.INVISIBLE);
			}
		});

	}

	/**
	 * add by basyouni a method to change popup shape
	 * 
	 * @param id
	 */

	public void setBackground(int id) {
			mTrack.setBackgroundResource(id);
	}

	/**
	 * Listener for item click
	 *
	 */
	public interface OnActionItemClickListener {
		public abstract void onItemClick(int pos);
	}
}