package com.tawasol.fcbBarcelona.customView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.tawasol.fcbBarcelona.R;

public class FanMaskImageView extends ImageView {

	private Context context;

	private static Bitmap bitmapMask;
	private Paint paint;
	private PorterDuffXfermode xfermode;
	Matrix mMatrix = new Matrix();
	Rect rect;

	private Bitmap bitmap;

	public FanMaskImageView(Context context) {
		this(context, null);
	}

	public FanMaskImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		if (bitmapMask == null)
			bitmapMask = decodeBitmap(R.drawable.fan_mask);

		paint = new Paint();
		rect = new Rect(0,0,bitmapMask.getWidth(),bitmapMask.getHeight());
		xfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
	}

	public FanMaskImageView(Context context, AttributeSet attrs, int defStyle) {
		this(context, attrs);
	}

	public static void clearMask() {

		if (bitmapMask != null) {
			bitmapMask.recycle();
			bitmapMask = null;
		}

	}

	private Bitmap decodeBitmap(int resId) {
		return BitmapFactory.decodeResource(context.getResources(), resId);
	}

	// set bitmap from caller activity
	// added by basyouni 17/12
	public void setBitmap(Bitmap temp) {
		this.bitmap = temp;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (bitmapMask != null)
			setMeasuredDimension(bitmapMask.getWidth(), bitmapMask.getHeight());
		else
			setMeasuredDimension(0, 0);
	}
	
	public Bitmap getImage (Bitmap bmp, int width, int height) {
	    Bitmap img = Bitmap.createScaledBitmap( bmp, width, height, true );
//	    bmp.recycle();
	    return img;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// set bitmap from caller activity if bitmap is not set use the default
		// image added by basyouni
		Bitmap bmp = null;
		if (bitmap != null)
			bmp = bitmap;
		else
			bmp = ((BitmapDrawable) getDrawable()).getBitmap();
		int saveFlags = Canvas.MATRIX_SAVE_FLAG | Canvas.CLIP_SAVE_FLAG
				| Canvas.HAS_ALPHA_LAYER_SAVE_FLAG
				| Canvas.FULL_COLOR_LAYER_SAVE_FLAG
				| Canvas.CLIP_TO_LAYER_SAVE_FLAG;
		canvas.saveLayer(0, 0, getWidth(), getHeight(), null, saveFlags);
		canvas.drawBitmap(bitmapMask, 0, 0, paint);
		paint.setXfermode(xfermode);
//		bmp = getImage(bmp, bitmapMask.getWidth(), bitmapMask.getHeight());
//		int left = getWidth() / 2 - bitmapMask.getWidth() / 2;
//		int top = getHeight() / 2 - bitmapMask.getHeight() / 2;
//		canvas.drawBitmap(bmp, left, top, paint);
		canvas.drawBitmap(bmp, null, rect, paint);
		paint.setXfermode(null);
		canvas.restore();
	}
}
