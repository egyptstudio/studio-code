//
//  WebService.h
//  MoRe
//
//  Created by Ahmed Aly on 10/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h" 
//#import "LibrariesHeader.h"

@interface WebService : NSObject {
    BOOL connected;
	Reachability *reach;

}
@property (nonatomic) BOOL connected;

+ (WebService *)getObject;
- (BOOL)checkInternetWithData;
- (BOOL)checkInternet;
//ALrassas
+ (NSMutableURLRequest *)getRequestWithUploadParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL;

//
+ (NSURLRequest *)requestWithParameters:(NSDictionary *)urlParameters andBaseURL:(NSString *)baseURL;
+ (NSMutableURLRequest *)postRequestWithUploadParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL;
+ (NSMutableURLRequest *)postRequestForServicesWithUploadParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL;
+ (NSString *)encodedOAuthParameterForString:(NSString *)str;
-(void)releaseObject;
@end
