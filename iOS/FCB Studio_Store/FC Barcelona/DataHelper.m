//
//  DataHelper.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 9/7/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "DataHelper.h"
#import <objc/runtime.h>
#import "Post.h"
#import "User.h"
#import "StudioPhotoTag.h"
#import "UserComment.h"
@implementation DataHelper

static DataHelper* instance;
+(DataHelper *)getInstance{
    if (!instance) {
        instance=[[DataHelper alloc] init];
    }
    return instance;
}

-(NSString*)convertToJSONString:(id)input
{
    NSMutableDictionary* tempDic = [input mutableCopy];
    for(NSString* key in [input allKeys])
    {
        if([[tempDic objectForKey:key] isKindOfClass:[UIImage class]])
        {
            [tempDic removeObjectForKey:key];
        }
        if ([[tempDic objectForKey:key] isKindOfClass:[Post class]]
            || [[tempDic objectForKey:key] isKindOfClass:[User class]]
            || [[tempDic objectForKey:key] isKindOfClass:[UserComment class]])  {
            
            [tempDic setObject:[self dictionaryOfPropertiesForObject:[tempDic objectForKey:key]] forKey:key];

        }
    }
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tempDic options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return jsonString;
}

-(NSString*)convertDicToJSONString:(id)input
{
    NSMutableDictionary* tempDic = [input mutableCopy];
    for(NSString* key in [input allKeys])
    {
        if([[tempDic objectForKey:key] isKindOfClass:[UIImage class]])
        {
            [tempDic removeObjectForKey:key];
        }
    }
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tempDic options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return jsonString;
}

- (NSDictionary *)dictionaryOfPropertiesForObject:(id)object
{
    // somewhere to store the results
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    // we'll grab properties for this class and every superclass
    // other than NSObject
    Class classOfObject = [object class];
    while(![classOfObject isEqual:[NSObject class]])
    {
        // ask the runtime to give us a C array of the properties defined
        // for this class (which doesn't include those for the superclass)
        unsigned int numberOfProperties;
        objc_property_t  *properties =
        class_copyPropertyList(classOfObject, &numberOfProperties);
        
        // go through each property in turn...
        for(
            int propertyNumber = 0;
            propertyNumber < numberOfProperties;
            propertyNumber++)
        {
            // get the name and convert it to an NSString
            NSString *nameOfProperty = [NSString stringWithUTF8String:
                                        property_getName(properties[propertyNumber])];
            
            // use key-value coding to get the property value
            id propertyValue = [object valueForKey:nameOfProperty];
            
            // add the value to the dictionary —
            // we'll want to transmit NULLs, even though an NSDictionary
            // can't store nils
            [result
             setObject:propertyValue ? propertyValue : [NSNull null]
             forKey:nameOfProperty];
        }
        
        // we took a copy of the property list, so...
        free(properties);
        
        // we'll want to consider the superclass too
        classOfObject = [classOfObject superclass];
    }

   

    // return the dictionary
    return result;
}

@end
