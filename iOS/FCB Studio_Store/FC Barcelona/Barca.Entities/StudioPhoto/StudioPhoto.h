//
//  StudioPhoto.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StudioPhotoTag.h"


@interface StudioPhoto : NSObject
@property int studioPhotoId;
@property int folderId;
@property int photoOrientation;
@property int useCount;
@property int captureType;
@property int requiredPoints;
@property long uploadDate;
@property BOOL  isPremium;
@property BOOL  isPurchased;
@property NSString* photoUrl;
@property NSString* photoName;
@property NSMutableArray *tags;
@end
