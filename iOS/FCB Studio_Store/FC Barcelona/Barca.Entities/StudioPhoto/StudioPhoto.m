//
//  StudioPhoto.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "StudioPhoto.h"

@implementation StudioPhoto



-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:@(self.studioPhotoId) forKey:@"studioPhotoId"];
    [encoder encodeObject:@(self.folderId) forKey:@"folderId"];
    [encoder encodeObject:@(self.photoOrientation) forKey:@"photoOrientation"];
    [encoder encodeObject:@(self.useCount) forKey:@"useCount"];
    [encoder encodeObject:@(self.captureType) forKey:@"captureType"];
    [encoder encodeObject:@(self.requiredPoints) forKey:@"requiredPoints"];
    [encoder encodeObject:@(self.uploadDate) forKey:@"uploadDate"];
    [encoder encodeObject:@(self.isPremium) forKey:@"isPremuim"];
    [encoder encodeObject:@(self.isPurchased) forKey:@"isPurchased"];
    [encoder encodeObject:self.photoUrl forKey:@"photoUrl"];
    [encoder encodeObject:self.photoName forKey:@"photoName"];
    [encoder encodeObject:self.tags forKey:@"tags"];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self.studioPhotoId    =  [[decoder decodeObjectForKey:@"studioPhotoId"] intValue];
    self.folderId         =  [[decoder decodeObjectForKey:@"folderId"] intValue];
    self.photoOrientation =  [[decoder decodeObjectForKey:@"photoOrientation"] intValue];
    self.useCount         =  [[decoder decodeObjectForKey:@"useCount"] intValue];
    self.captureType      =  [[decoder decodeObjectForKey:@"captureType"] intValue];
    self.requiredPoints   =  [[decoder decodeObjectForKey:@"requiredPoints"] intValue];
    self.uploadDate       =  [[decoder decodeObjectForKey:@"uploadDate"] longValue];
    self.isPremium        =  [[decoder decodeObjectForKey:@"isPremuim"] intValue];
    self.isPurchased      =  [[decoder decodeObjectForKey:@"isPurchased"] intValue];
    self.photoUrl         =  [decoder decodeObjectForKey:@"photoUrl"];
    self.photoName        =  [decoder decodeObjectForKey:@"photoName"];
    self.tags             =  [decoder decodeObjectForKey:@"tags"];
    return self;
}
@end
