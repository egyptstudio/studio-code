//
//  StudioCategory.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessObject.h"

@interface StudioCategory : BusinessObject
@property (nonatomic) int catId;
@property(nonatomic , strong) NSString* categoryName;
@property(nonatomic , strong) NSMutableArray* photoList;
@end
