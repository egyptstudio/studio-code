//
//  UserComment.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "UserComment.h"

@implementation UserComment



-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:@(self.commentId) forKey:@"commentId"];
    [encoder encodeObject:@(self.postId) forKey:@"postId"];
    [encoder encodeObject:@(self.userId) forKey:@"userId"];
    [encoder encodeObject:@(self.isFollowing) forKey:@"isFollowing"];
    [encoder encodeObject:@(self.commentDate) forKey:@"commentDate"];
    [encoder encodeObject:self.commentText forKey:@"commentText"];
    [encoder encodeObject:self.fullName forKey:@"fullName"];
    [encoder encodeObject:self.profilePicURL forKey:@"profilePicURL"];
}


-(id)initWithCoder:(NSCoder *)decoder
{
    self.commentId      = [[decoder decodeObjectForKey:@"commentId"] intValue];
    self.postId         = [[decoder decodeObjectForKey:@"postId"] intValue];
    self.userId         = [[decoder decodeObjectForKey:@"userId"] intValue];
    self.isFollowing    = [[decoder decodeObjectForKey:@"isFollowing"] boolValue];
    self.commentDate    = [[decoder decodeObjectForKey:@"commentDate"] longValue];
    self.commentText    = [decoder decodeObjectForKey:@"commentText"];
    self.fullName       = [decoder decodeObjectForKey:@"fullName"];
    self.profilePicURL  = [decoder decodeObjectForKey:@"profilePicURL"];
    return self;
}

@end
