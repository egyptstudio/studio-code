//
//  UserComment.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserComment : NSObject
@property int commentId;
@property int postId;
@property int userId;
@property BOOL isFollowing;
@property NSString* commentText;
@property long long commentDate;
@property NSString* fullName;
@property NSString* profilePicURL;
@end
