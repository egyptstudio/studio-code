//
//  UserPhotoLikes.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessObject.h"
@interface UserPhotoLikes : BusinessObject
@property (nonatomic, assign) int likeId;
@property (nonatomic, assign) int userId;
@property (nonatomic, assign) int photoId;
@end
