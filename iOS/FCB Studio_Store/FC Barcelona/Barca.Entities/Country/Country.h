//
//  Country.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject
@property (nonatomic, assign) int countryId;
@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSString *phone_code;
@property (nonatomic, strong) NSString *gLat;
@property (nonatomic, strong) NSString *gLong;
@end
