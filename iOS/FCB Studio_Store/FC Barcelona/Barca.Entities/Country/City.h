//
//  City.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/13/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (nonatomic, assign) int cityId;
@property (nonatomic, strong) NSString *cityName;
@end
