//
//  UserPhoto.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessObject.h"
@interface UserPhoto : BusinessObject
@property (nonatomic,assign) int photoId; //
@property (nonatomic,assign) int studioPhotoId; //
@property (nonatomic,assign) int userId; //
@property (nonatomic, strong) NSString* uploadedDate;  //
@property (nonatomic,strong) NSString* caption;
@property (nonatomic, strong) NSString* imageUrl;//
@property (nonatomic , strong) NSString* privacy;
@property (nonatomic , assign) BOOL publishedToWall;
@property (nonatomic , assign) int contestRank;



@property (nonatomic , strong) UIImage* image;  //doesn't know it's need
-(UserPhoto*)initWithPhotoId:(int)photoId studioPhotoId:(int)studioPhotoId userId:(int)userId uploadedDate:(NSString*)uploadedDate caption:(NSString*)caption imageUrl:(NSString*)imageUrl privacy:(NSString*)privacy publishedToWall:(BOOL)publishedToWall contestRank:(int)contestRank image:(UIImage*)image;
@end
