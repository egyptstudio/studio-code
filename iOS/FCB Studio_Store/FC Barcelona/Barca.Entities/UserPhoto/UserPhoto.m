//
//  UserPhoto.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "UserPhoto.h"

@implementation UserPhoto
-(UserPhoto *)initWithPhotoId:(int)photoId studioPhotoId:(int)studioPhotoId userId:(int)userId uploadedDate:(NSString *)uploadedDate caption:(NSString *)caption imageUrl:(NSString*)imageUrl privacy:(NSString *)privacy publishedToWall:(BOOL)publishedToWall contestRank:(int)contestRank image:(UIImage*)image{
    UserPhoto* userPhoto=[[UserPhoto alloc] init];
    userPhoto.photoId=photoId; //
    userPhoto.studioPhotoId=studioPhotoId; //
    userPhoto.userId=userId; //
    userPhoto.uploadedDate=uploadedDate;  //
    userPhoto.caption=caption;
    userPhoto.imageUrl=imageUrl;//
    userPhoto.privacy=privacy;
    userPhoto.publishedToWall=publishedToWall;
    userPhoto.contestRank=contestRank;
    userPhoto.image=image;;
    return userPhoto;
}

-(void)addValidationRules{
    
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:[NSString stringWithFormat:@"%i",self.photoId] forKey:@"photoId"];
    [encoder encodeObject:[NSString stringWithFormat:@"%i",self.studioPhotoId] forKey:@"studioPhotoId"];
    [encoder encodeObject:[NSString stringWithFormat:@"%i",self.userId] forKey:@"userId"];

    [encoder encodeObject:self.uploadedDate forKey:@"uploadedDate"];
    [encoder encodeObject:self.caption forKey:@"caption"];
    [encoder encodeObject:self.imageUrl forKey:@"imageUrl"];
    [encoder encodeObject:self.privacy forKey:@"privacy"];
    [encoder encodeObject:[NSString stringWithFormat:@"%i",self.publishedToWall] forKey:@"publishedToWall"];
    [encoder encodeObject:[NSString stringWithFormat:@"%i",self.contestRank] forKey:@"contestRank"];
    
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if ( self != nil )
    {
        self.photoId      = [[decoder decodeObjectForKey:@"photoId"] intValue];
        
        self.studioPhotoId      = [[decoder decodeObjectForKey:@"studioPhotoId"] intValue];
        self.userId      = [[decoder decodeObjectForKey:@"userId"] intValue];
        self.uploadedDate         = [decoder decodeObjectForKey:@"uploadedDate"];
        self.caption   = [decoder decodeObjectForKey:@"caption"];
        self.imageUrl        = [decoder decodeObjectForKey:@"imageUrl"];
        self.privacy     = [decoder decodeObjectForKey:@"privacy"];
        
        self.publishedToWall     = [[decoder decodeObjectForKey:@"publishedToWall"] boolValue];
        self.contestRank     = [[decoder decodeObjectForKey:@"contestRank"] intValue];
        
    }
    return self;
}


@end
