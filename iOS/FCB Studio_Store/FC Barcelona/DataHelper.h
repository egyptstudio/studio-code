//
//  DataHelper.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 9/7/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataHelper : NSObject
+(DataHelper *)getInstance;
-(NSString*)convertToJSONString:(id)input;
-(NSString*)convertDicToJSONString:(id)input;
-(NSDictionary *)dictionaryOfPropertiesForObject:(id)object;
//+(NSDictionary *)dictionaryOfPropertiesForObject:(id)object;

@end
