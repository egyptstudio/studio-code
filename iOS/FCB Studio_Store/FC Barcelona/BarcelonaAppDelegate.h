//
//  BarcelonaAppDelegate.h
//  FC Barcelona
////
//  Created by Mohammed Alrassas on 8/26/14.
//  updated by mohamed metwaly
//  Copyright (c) 2016 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"
#import <StoreKit/StoreKit.h>

@interface BarcelonaAppDelegate : UIResponder <UIApplicationDelegate,SKPaymentTransactionObserver>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString* BaseURL;
@property (strong, nonatomic) Contact *userCD;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

