//
//  UsersManager.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "UsersManager.h"
#import "User.h"
#import "GetAndPushCashedData.h"
#import "HelpScreen.h"
@implementation UsersManager

static UsersManager* instance;
+(UsersManager *)getInstance{
    if (!instance) {
        instance=[[UsersManager alloc] init];
    }
    return instance;
}


-(void)loginWithUserName:(NSString*)userName andPassword:(NSString*)password and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    User* userObject = [[User alloc] init];
    NSMutableArray* vars = [[NSMutableArray alloc] init];
    [vars addObject:userName];
    [vars addObject:password];
    [vars addObject:@(2)];
    [vars addObject:[[Language sharedInstance] langAbbreviation]];

    
    NSMutableArray* keys = [[NSMutableArray alloc] init];
    [keys addObject:@"email"];
    [keys addObject:@"password"];
    [keys addObject:@"deviceType"];
    [keys addObject:@"lang"];

    if ([GetAndPushCashedData getObjectFromCash:@"deviceToken"]) {
        [vars addObject:[GetAndPushCashedData getObjectFromCash:@"deviceToken"]];
        [keys addObject:@"deviceToken"];
    }
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
   
    @try {
        [self submitWithAction:@"user/login" andParams:dic andIam:userObject and:^(id currentClient) {
            NSLog(@"%@",currentClient);
            @try {
                handler(currentClient);

            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception.name);
            }
            
        } onFailure:^(NSError *error) {
            NSLog(@"%@",error.domain);
            failure(error);
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.name);
    }
        

}


-(void)loginSocialMedia:(NSString*)mediaId
                  email:(NSString*)email
               fullName:(NSString*)fullName
                  phone:(NSString*)phone
                country:(NSString*)country
                   city:(NSString*)city
               district:(NSString*)district
               birthday:(NSString*)birthday
                 gender:(NSString*)gender
                 profilePicURL:(NSString*)pic and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{

    
    NSMutableArray* keys = [[NSMutableArray alloc]init];
    [keys addObject:@"lang"];
    [keys addObject:@"socialMediaId"];
    [keys addObject:@"email"];
    [keys addObject:@"fullName"];
    [keys addObject:@"phone"];
    [keys addObject:@"country"];
    [keys addObject:@"city"];
    [keys addObject:@"district"];
    [keys addObject:@"dateOfBirth"];
    [keys addObject:@"gender"];
    [keys addObject:@"profilePicURL"];
    [keys addObject:@"deviceType"];

    NSMutableArray* vars = [[NSMutableArray alloc]init];
    [vars addObject:[[Language sharedInstance] langAbbreviation]];
    [vars addObject:mediaId];
    [vars addObject:email?email:@" "];
    [vars addObject:fullName];
    [vars addObject:phone?phone:@" "];
    [vars addObject:country?country:@" "];
    [vars addObject:city?city:@" "];
    [vars addObject:district?district:@" "];
    [vars addObject:birthday?birthday:@" "];
    [vars addObject:gender?gender:@" "];
    [vars addObject:pic?pic:@" "];
    [vars addObject:@"2"];
    
    if ([GetAndPushCashedData getObjectFromCash:@"deviceToken"]) {
        [vars addObject:[GetAndPushCashedData getObjectFromCash:@"deviceToken"]];
        [keys addObject:@"deviceToken"];
    }
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    @try {
        [self submitWithAction:@"user/loginSocialMedia" andParams:dic andIam:[[User alloc] init] and:^(id currentClient) {
            NSLog(@"%@",currentClient);
            @try {
                handler(currentClient);
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception.name);
            }
        } onFailure:^(NSError *error) {
            NSLog(@"%@",error.domain);
            failure(error);
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.name);
    }
}

-(void)registerUser:(NSString*)fullname email:(NSString*)email password:(NSString*)password photoURL:(NSString*)url photo:(UIImage*)photo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{

    NSMutableArray* vars = [[NSMutableArray alloc] init];
    NSMutableArray* keys = [[NSMutableArray alloc] init];
    [vars addObject:fullname];
    [keys addObject:@"fullname"];

    [vars addObject:email];
    [keys addObject:@"email"];

    [vars addObject:password];
    [keys addObject:@"password"];
    
    [vars addObject:url?url:@""];
    [keys addObject:@"photoURL"];
    
    [vars addObject:[[Language sharedInstance] langAbbreviation]];
    [keys addObject:@"lang"];
    
    if (photo&&!url) {
        [vars addObject:photo];
        [keys addObject:@"profilePic"];
    }
    
    if ([GetAndPushCashedData getObjectFromCash:@"deviceToken"]) {
        [vars addObject:[GetAndPushCashedData getObjectFromCash:@"deviceToken"]];
        [keys addObject:@"deviceToken"];
    }
    
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    @try {
        [self submitWithAction:@"user/registerUser" andParams:dic andIam:[[User alloc] init] and:^(id currentClient) {
            NSLog(@"%@",currentClient);
            @try {
                handler(currentClient);
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception.name);
            }
            
        } onFailure:^(NSError *error) {
            NSLog(@"%@",error.domain);
            failure(error);
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.name);
    }
}

-(void)forgotPasswordWithEmail:(NSString*)email and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSDictionary* tempDic = [NSDictionary dictionaryWithObject:email forKey:@"email"];
    
    [self submitWithAction:@"user/sendVerificationCode" andParams:tempDic andIam:nil and:^(id currentClient)
    {
        NSLog(@"%@",currentClient);
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.domain);
        failure(error);
    }];
}

-(void)getUserByID:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/getUserByID" andParams:dic andIam:[[User alloc] init]  and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)uploadProfilePic:(int)userId andImage:(UIImage*)profilePic and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],profilePic, nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"profilePic", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];

    [self submitWithAction:@"updateProfilePic" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)uploadUserPhoto:(NSString*)userId andImage:(UIImage*)profilePic and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:userId,profilePic, nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"profilePic", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/uploadUserPhoto" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)changePassword:(int)userId verificationCode:(NSString*)code newPassword:(NSString*)pass and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     @(userId),
                     code,
                     pass, nil];
    
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"userId",
                     @"verificationCode",
                     @"newPassword", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/changePassword" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)changePassword:(int)userId oldPassword:(NSString*)oldPassword currentPassword:(NSString*)currentPassword and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     @(userId),
                     oldPassword,
                     currentPassword, nil];
    
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"userId",
                     @"oldPassword",
                     @"currentPassword", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"settings/changePassword" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)sendInviteSMS:(int)userId phoneNumbers:(NSArray*)phoneNumbers invitationText:(NSString*)invitationText and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     @(userId),
                     phoneNumbers,
                     invitationText, nil];
    
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"userId",
                     @"phoneNumbers",
                     @"invitationText", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/sendInviteSMS" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}



-(void)updateUserProfile:(User*)user witImage:(UIImage*)userImage and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if (userImage) {
        [user setProfilePic:@""];
        [dic setObject:userImage forKey:@"profilePic"];
    }
    [dic setObject:user forKey:@"user"];
    [dic setObject:[[Language sharedInstance] langAbbreviation] forKey:@"lang"];
    
    [self submitWithAction:@"profile/updateUserProfile" andParams:dic andIam:[[User alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(NSString*)jsonStringFromArray:(NSArray*)info
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:info
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                       withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\"
                                                       withString:@""];
    return jsonString;
}

-(void)sendContactUs:(NSString*)email messageTitle:(NSString*)messageTitle messageText:(NSString*)messageText and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     email,
                     messageTitle,
                     messageText, nil];
    
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"email",
                     @"messageTitle",
                     @"messageText", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"settings/sendContactUs" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)verifyUserEmail:(int)userId email:(NSString*)email and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     @(userId),
                     email, nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"userId",
                     @"email", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"profile/verifyUserEmail" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}
-(void)verifyUserPhone:(int)userId phone:(NSString*)phone and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     @(userId),
                     phone, nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"userId",
                     @"phone", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"profile/verifyUserPhone" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}
-(void)sendSMSverificationCode:(int)userId code:(NSString*)code and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     @(userId),
                     code, nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"userId",
                     @"code", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"profile/sendSMSverificationCode" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)infoLinks:(NSString*)page and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     page, nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"page", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"settings/info_links" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}
-(void)getLinks:(NSString*)page and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     page, nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"name", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"settings/getLinks" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)getBaseURL:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    //1.0.8
    NSArray* vars = [NSArray arrayWithObjects:
                     [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"],
                     @"2", nil];
    
    NSArray* keys = [NSArray arrayWithObjects:
                     @"versionNumber",
                     @"deviceType", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"getBaseURL" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)getHelpScreens:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation], nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"help/getHelpScreen" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)getBlockedUsers:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],@(userId), nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",@"userId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"settings/getBlockedUsers" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}
-(void)unblockUser:(int)userId  fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],@(userId),@(fanId), nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",@"userId",@"fanId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"settings/unblockUser" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)sendInviteInvitationText:(int)userId  invitationText:(NSString*)invitationText and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],@(userId),invitationText, nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",@"userId",@"invitationText", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/sendInviteInvitationText" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)updateToken:(int)userID  token:(NSString*)token and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation],@(userID),token, nil];
    NSArray* keys = [NSArray arrayWithObjects: @"lang",@"userID",@"token", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/updateToken" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)pullMessages:(int)userID  fanID:(int)fanID messageID:(int)messageID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation],@(userID),@(fanID),@(messageID), nil];
    NSArray* keys = [NSArray arrayWithObjects: @"lang",@"userID",@"fanID",@"messageID", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/pullMessage" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void) generateInviteToken:(int)userId
             invitationText:(NSString*)invitationText
                        and:(void (^)(id currentClient))handler
                  onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects: @(userId),invitationText, nil];
    NSArray* keys = [NSArray arrayWithObjects: @"userID",@"messageText", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/generateInviteToken"
                 andParams:dic
                    andIam:nil
                       and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void) userIsActive:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure{
    NSArray* vars = [NSArray arrayWithObjects: [[Language sharedInstance] langAbbreviation],@(userId), nil];
    NSArray* keys = [NSArray arrayWithObjects: @"lang",@"userID", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/userIsActive"
                 andParams:dic
                    andIam:nil
                       and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];

}

-(void) userIsNotActive:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure{
    NSArray* vars = [NSArray arrayWithObjects: [[Language sharedInstance] langAbbreviation],@(userId), nil];
    NSArray* keys = [NSArray arrayWithObjects: @"lang",@"userID", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/userIsNotActive"
                 andParams:dic
                    andIam:nil
                       and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];

}
-(void)clearNotification:(int)userId notificationId:(int)notificationId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure{
    NSArray* vars = [NSArray arrayWithObjects: [[Language sharedInstance] langAbbreviation],@(userId),@(notificationId), nil];
    NSArray* keys = [NSArray arrayWithObjects: @"lang",@"userId",@"notificationId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"notification/clearNotification"
                 andParams:dic
                    andIam:nil
                       and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
    
}


@end
