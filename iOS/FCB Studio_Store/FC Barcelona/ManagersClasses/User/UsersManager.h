//
//  UsersManager.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "BusinessManager.h"
#import "User.h"
@interface UsersManager : BusinessManager
+(UsersManager *)getInstance;
-(void)loginWithUserName:(NSString*)userName andPassword:(NSString*)password and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)forgotPasswordWithEmail:(NSString*)email and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)uploadProfilePic:(int)userId andImage:(UIImage*)profilePic and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)registerUser:(NSString*)fullname email:(NSString*)email password:(NSString*)password photoURL:(NSString*)url photo:(UIImage*)photo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)uploadUserPhoto:(NSString*)userId andImage:(UIImage*)profilePic and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)loginSocialMedia:(NSString*)mediaId
                  email:(NSString*)email
               fullName:(NSString*)fullName
                  phone:(NSString*)phone
                country:(NSString*)country
                   city:(NSString*)city
               district:(NSString*)district
               birthday:(NSString*)birthday
                 gender:(NSString*)gender
          profilePicURL:(NSString*)pic and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)changePassword:(int)userId verificationCode:(NSString*)code newPassword:(NSString*)pass and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)sendInviteSMS:(int)userId phoneNumbers:(NSArray*)phoneNumbers invitationText:(NSString*)invitationText and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)updateUserProfile:(User*)user witImage:(UIImage*)userImage and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)changePassword:(int)userId oldPassword:(NSString*)oldPassword currentPassword:(NSString*)currentPassword and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)sendContactUs:(NSString*)email messageTitle:(NSString*)messageTitle messageText:(NSString*)messageText and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)verifyUserEmail:(int)userId email:(NSString*)email and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)verifyUserPhone:(int)userId phone:(NSString*)phone and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)sendSMSverificationCode:(int)userId code:(NSString*)code and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)infoLinks:(NSString*)page and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getUserByID:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getBaseURL:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getHelpScreens:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getBlockedUsers:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)unblockUser:(int)userId  fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)sendInviteInvitationText:(int)userId  invitationText:(NSString*)invitationText and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)updateToken:(int)userID  token:(NSString*)token and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failur;
-(void)pullMessages:(int)userID  fanID:(int)fanID messageID:(int)messageID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void) generateInviteToken:(int)userId invitationText:(NSString*)invitationText and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void) userIsActive:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void) userIsNotActive:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getLinks:(NSString*)page and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)clearNotification:(int)userId notificationId:(int)notificationId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
@end
