//
//  User.h
//  FCB
//
//  Created by Eissa on 8/26/14.
//  Copyright (c) 2014 Tawasolit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessObject.h"
#import "ValidationRule.h"
//#import "UserNameValidator.h"
@interface User : BusinessObject

@property(nonatomic, assign) int userId;
@property(nonatomic,assign) int  noOfPics;
@property(nonatomic,assign) int  userType;
@property(nonatomic,assign) int  gender;
@property(nonatomic,assign) int  credit;
@property(nonatomic,assign) int countryId;
@property(nonatomic,assign) int cityId;
@property(nonatomic,assign) int followersCount;
@property(nonatomic,assign) int followingCount;
@property(nonatomic,assign) int income;
@property(nonatomic,assign) int relationship;
@property(nonatomic,assign) int receiveFriendRequest;
@property(nonatomic,assign) int viewPersonalInfo;
@property(nonatomic,assign) int viewContactsInfo;
@property(nonatomic,assign) int deviceType;
@property(nonatomic,assign) int registrationLevelThreePoints;
@property(nonatomic,assign) int registrationLevelTwoPoints;
@property(nonatomic,assign) int registrationLevelOnePoints;
@property(nonatomic,assign) int rePostMyPostNormalPoints;
@property(nonatomic,assign) int rePostMyPostPremiumPoints;
@property(nonatomic,assign) int education;
@property(nonatomic,assign) int job;
@property(nonatomic,assign) int jobRole;

@property(nonatomic,assign) long dateOfBirth;

@property(nonatomic) BOOL  allowLocation;
@property(nonatomic) BOOL  isOnline;
@property(nonatomic) BOOL  isPremium;
@property(nonatomic) BOOL  emailVerified;
@property(nonatomic) BOOL  phoneVerified;
@property(nonatomic) BOOL  receivePushNotification;

@property(strong,nonatomic) NSString * profilePic;
@property(strong,nonatomic) NSString * fullName;
@property(strong,nonatomic) NSString * password;
@property(strong,nonatomic) NSString * email;
@property(strong,nonatomic) NSString * latitude;
@property(strong,nonatomic) NSString * longitude;
@property(strong,nonatomic) NSString * registrationDate;
@property(strong,nonatomic) NSString * lastLoginDate;
@property(strong,nonatomic) NSString * socialMediaId;
@property(strong,nonatomic) NSString * brief;
@property(strong,nonatomic) NSString * countryName;
@property(strong,nonatomic) NSString * cityName;
@property(strong,nonatomic) NSString * district;
@property(strong,nonatomic) NSString * phone;
@property(strong,nonatomic) NSString * status;
@property(strong,nonatomic) NSString * aboutMe;
@property(strong,nonatomic) NSString * religion;
@property(strong,nonatomic) NSString * company;
@property(strong,nonatomic) NSString * deviceToken;
@property(strong,nonatomic) NSString * countryCode;
@property(strong,nonatomic) NSString * phoneCode;

-(int)calculateProfileCompletion;




@end
