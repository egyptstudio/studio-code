//
//  User.m
//  FCB
//
//  Created by Eissa on 8/26/14.
//  Copyright (c) 2014 Tawasolit. All rights reserved.
//

#import "User.h"

@implementation User


-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.userId] forKey:@"userId"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.gender] forKey:@"gender"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.countryId] forKey:@"countryId"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.noOfPics] forKey:@"noOfPics"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.userType] forKey:@"userType"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.followersCount] forKey:@"followersCount"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.followingCount] forKey:@"followingCount"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.income] forKey:@"income"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.relationship] forKey:@"relationship"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.credit] forKey:@"credit"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.cityId] forKey:@"cityId"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.receiveFriendRequest] forKey:@"recieveFriendRequest"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.viewPersonalInfo] forKey:@"viewPersonalInfo"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.viewContactsInfo] forKey:@"viewContactsInfo"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.deviceType] forKey:@"deviceType"];

    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.registrationLevelOnePoints] forKey:@"registrationLevelOnePoints"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.registrationLevelTwoPoints] forKey:@"registrationLevelTwoPoints"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.registrationLevelThreePoints] forKey:@"registrationLevelThreePoints"];

    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.rePostMyPostNormalPoints] forKey:@"rePostMyPostNormalPoints"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.rePostMyPostPremiumPoints] forKey:@"rePostMyPostPremiumPoints"];
    
    [encoder encodeObject:self.profilePic forKey:@"profilePic"];
    [encoder encodeObject:self.fullName forKey:@"fullName"];
    [encoder encodeObject:self.password forKey:@"passWord"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%lu",self.dateOfBirth] forKey:@"dateOfBirth"];
    [encoder encodeObject:self.socialMediaId forKey:@"socialMediaId"];
    [encoder encodeObject:self.lastLoginDate forKey:@"lastLoginDate"];
    [encoder encodeObject:self.registrationDate forKey:@"registrationDate"];
    [encoder encodeObject:self.brief forKey:@"brief"];
    [encoder encodeObject:self.latitude forKey:@"latitude"];
    [encoder encodeObject:self.longitude forKey:@"longitude"];
    [encoder encodeObject:self.phone forKey:@"phone"];
    [encoder encodeObject:self.status forKey:@"status"];
    [encoder encodeObject:self.aboutMe forKey:@"aboutMe"];
    [encoder encodeObject:self.religion forKey:@"religion"];
    [encoder encodeObject:@(self.education) forKey:@"education"];
    [encoder encodeObject:@(self.job) forKey:@"job"];
    [encoder encodeObject:@(self.jobRole) forKey:@"jobRole"];
    [encoder encodeObject:self.company forKey:@"company"];
    [encoder encodeObject:self.countryName forKey:@"countryName"];
    [encoder encodeObject:self.cityName forKey:@"cityName"];
    [encoder encodeObject:self.district forKey:@"district"];
    [encoder encodeObject:self.deviceToken forKey:@"deviceToken"];
    [encoder encodeObject:self.countryCode forKey:@"countryCode"];
    [encoder encodeObject:self.phoneCode forKey:@"phoneCode"];

    
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.allowLocation] forKey:@"allowLocation"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.isPremium] forKey:@"isPremium"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.isOnline] forKey:@"isOnline"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.emailVerified] forKey:@"emailVeified"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.phoneVerified] forKey:@"phonelVeified"];
    [encoder encodeObject:[@"" stringByAppendingFormat:@"%d",self.receivePushNotification] forKey:@"recievePushNotification"];

    
    
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if ( self != nil )
    {
       
        

        
        self.userId           = [[decoder decodeObjectForKey:@"userId"] intValue];
        self.gender           = [[decoder decodeObjectForKey:@"gender"] intValue];
        self.countryId        = [[decoder decodeObjectForKey:@"countryId"] intValue];
        self.noOfPics         = [[decoder decodeObjectForKey:@"noOfPics"] intValue];
        self.userType         = [[decoder decodeObjectForKey:@"userType"] intValue];
        self.followingCount   = [[decoder decodeObjectForKey:@"followingCount"]intValue];
        self.followersCount   = [[decoder decodeObjectForKey:@"followersCount"]intValue];
        self.income           = [[decoder decodeObjectForKey:@"income"]intValue];
        self.relationship     = [[decoder decodeObjectForKey:@"relationship"]intValue];
        self.credit           = [[decoder decodeObjectForKey:@"credit"]intValue];
        self.cityId           = [[decoder decodeObjectForKey:@"cityId"]intValue];
        self.receiveFriendRequest = [[decoder decodeObjectForKey:@"recieveFriendRequest"]intValue];
        self.viewPersonalInfo     = [[decoder decodeObjectForKey:@"viewPersonalInfo"]intValue];
        self.viewContactsInfo     = [[decoder decodeObjectForKey:@"viewContactsInfo"]intValue];
        self.deviceType           = [[decoder decodeObjectForKey:@"deviceType"]intValue];
        self.registrationLevelThreePoints         = [[decoder decodeObjectForKey:@"registrationLevelThreePoints"]intValue];
        self.registrationLevelTwoPoints           = [[decoder decodeObjectForKey:@"registrationLevelTwoPoints"]intValue];
        self.registrationLevelOnePoints           = [[decoder decodeObjectForKey:@"registrationLevelOnePoints"]intValue];
        self.rePostMyPostNormalPoints           = [[decoder decodeObjectForKey:@"rePostMyPostNormalPoints"]intValue];
        self.rePostMyPostPremiumPoints           = [[decoder decodeObjectForKey:@"rePostMyPostPremiumPoints"]intValue];

        self.isOnline         = [[decoder decodeObjectForKey:@"isOnline"] boolValue];
        self.emailVerified     = [[decoder decodeObjectForKey:@"emailVeified"] boolValue];
        self.phoneVerified    = [[decoder decodeObjectForKey:@"phonelVeified"] boolValue];
        self.allowLocation    = [[decoder decodeObjectForKey:@"allowLocation"] boolValue];
        self.isPremium        = [[decoder decodeObjectForKey:@"isPremium"] boolValue];
        self.receivePushNotification = [[decoder decodeObjectForKey:@"recievePushNotification"] boolValue];
        self.brief            = [decoder decodeObjectForKey:@"brief"];
        self.latitude         = [decoder decodeObjectForKey:@"latitude"];
        self.longitude        = [decoder decodeObjectForKey:@"longitude"];
        self.countryName      = [decoder decodeObjectForKey:@"countryName"];
        self.profilePic       = [decoder decodeObjectForKey:@"profilePic"];
        self.fullName         = [decoder decodeObjectForKey:@"fullName"];
        self.password         = [decoder decodeObjectForKey:@"passWord"];
        self.email            = [decoder decodeObjectForKey:@"email"];
        self.dateOfBirth      = [[decoder decodeObjectForKey:@"dateOfBirth"] longLongValue];
        self.cityName         = [decoder decodeObjectForKey:@"cityName"];
        self.lastLoginDate    = [decoder decodeObjectForKey:@"lastLoginDate"];
        self.registrationDate = [decoder decodeObjectForKey:@"registrationDate"];
        self.socialMediaId    = [decoder decodeObjectForKey:@"socialMediaId"];
        self.phone            = [decoder decodeObjectForKey:@"phone"];
        self.status           = [decoder decodeObjectForKey:@"status"];
        self.aboutMe          = [decoder decodeObjectForKey:@"aboutMe"];
        self.religion         = [decoder decodeObjectForKey:@"religion"];
        self.education        = [[decoder decodeObjectForKey:@"education"] intValue];
        self.job              = [[decoder decodeObjectForKey:@"job"] intValue];
        self.jobRole          = [[decoder decodeObjectForKey:@"jobRole"] intValue];
        self.company          = [decoder decodeObjectForKey:@"company"];
        self.district         = [decoder decodeObjectForKey:@"district"];
        self.deviceToken      = [decoder decodeObjectForKey:@"deviceToken"];
        self.countryCode      = [decoder decodeObjectForKey:@"countryCode"];
        self.phoneCode      = [decoder decodeObjectForKey:@"phoneCode"];
    }
    return self;
}

-(int)calculateProfileCompletion{

    float completedParameters = 0;

    if (self.fullName && ![[self.fullName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        completedParameters++ ;
    }if (self.profilePic && ![[self.profilePic stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        completedParameters++ ;
    }if (self.countryName && ![[self.countryName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        completedParameters++ ;
    }if (self.cityId != 0 ){
        completedParameters++ ;
    }if (self.district && ![[self.district stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        completedParameters++ ;
    }if (self.phone && ![[self.phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]){
        completedParameters++ ;
    }if (self.email && ![[self.email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        completedParameters++ ;
    }if (self.dateOfBirth != 0){
        completedParameters++ ;
    }if (self.status && ![[self.status stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        completedParameters++ ;
    }if (self.aboutMe && ![[self.aboutMe stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]){
        completedParameters++ ;
    }if (self.relationship != 0){
        completedParameters++ ;
    }if (self.education != 0){
        completedParameters++ ;
    }if (self.job != 0){
        completedParameters++ ;
    }if (self.jobRole != 0){
        completedParameters++ ;
    }if (self.company  && ![[self.company stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]){
        completedParameters++ ;
    }if (self.income != 0){
        completedParameters++ ;
    }if (self.gender != 0){
        completedParameters++ ;
    }
    
    
    float num = completedParameters/17.0 ;
    float completion =  num * 100;
    return (int)completion;
}
@end
