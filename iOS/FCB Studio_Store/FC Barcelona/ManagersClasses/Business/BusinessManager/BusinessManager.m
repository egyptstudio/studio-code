//
//  BusinessManager.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "BusinessManager.h"
//#import "DCKeyValueObjectMapping.h"
//#import <objc/runtime.h>
#import "DataHelper.h"
#import "SBJsonWriter.h"
#import "RestServiceAgent.h"
#import "BarcelonaAppDelegate.h"
#import <KeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "ValidationRule.h"
#import "GetAndPushCashedData.h"
#import "Post.h"
#import "Fan.h"
#import "Friend.h"
#import "Season.h"

@implementation BusinessManager


-(void)saveObjectWithAction:(NSString*)action andObject:(UserComment*)obj and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSDictionary* dic=[[DataHelper getInstance] dictionaryOfPropertiesForObject:obj];
    NSString* jsonString =[[DataHelper getInstance] convertToJSONString:dic];
    NSArray* tempKeysArray;
    NSArray* tempValuesArray;
    if([dic objectForKey:@"image"])
    {
        tempKeysArray =[NSArray arrayWithObjects:@"data",@"img" ,nil];
        tempValuesArray = [NSArray arrayWithObjects:jsonString,UIImageJPEGRepresentation ([dic objectForKey:@"image"],1.0), nil];
    }
    else
    {
        tempKeysArray =[NSArray arrayWithObject:@"data"];
        tempValuesArray = [NSArray arrayWithObject:jsonString];
    }
    
    [RestServiceAgent setPostRequestByAction:action andWSDL:[(BarcelonaAppDelegate*) [[UIApplication sharedApplication] delegate] BaseURL] andParamsKeys:tempKeysArray andParamsValues:tempValuesArray and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         if([currentClient isKindOfClass:[NSString class]])
         {
//             @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                            reason:@"Timeout exception"
//                                          userInfo:nil];
             NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] code:1 userInfo:nil];
             failure(error);
         }
         else
         {
             if([[currentClient objectForKey:@"status"] intValue] == 1)
             {
                 handler(currentClient);
             }
             else if([currentClient isKindOfClass:[NSDictionary class]]&&[currentClient objectForKey:@"commentId"])
             {
                 handler(currentClient);
             }
             else
             {
                 if([[currentClient objectForKey:@"status"] intValue] == -3)
                 {
                     //dispatch_async(dispatch_get_main_queue(), ^{

                     ValidationRule* valObject = [[ValidationRule alloc] init];
                     DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[valObject class]];
                     valObject = [parser parseDictionary:[currentClient objectForKey:@"data"]];
                     NSLog(@"%@",[valObject class]);
                   
                     NSError* error = [NSError errorWithDomain:valObject.errorMessage code:1 userInfo:nil];
                     failure(error);
                     
//                     @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                    reason:valObject.errorMessage
//                                                  userInfo:nil];
                    // });
                     
                 }
                 else
                 {
                     if([[currentClient objectForKey:@"status"] intValue] == -2)
                     {
//                         @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                        reason:@"webservice server exception"
//                                                      userInfo:nil];
                         NSError* error = [NSError errorWithDomain:@"webservice server exception" code:1 userInfo:nil];
                         failure(error);
                     }
                     else if([[currentClient objectForKey:@"status"] intValue] == -1)
                     {
//                         @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                        reason:@"webservice parameters error"
//                                                      userInfo:nil];
                         NSError* error = [NSError errorWithDomain:@"webservice parameters error" code:1 userInfo:nil];
                         failure(error);
                     }
                     else
                     {
//                         @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                        reason:@"unknown Exception"
//                                                      userInfo:nil];
                         NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"error"] code:1 userInfo:nil];
                         failure(error);
                     }
                     
                 }
             }
         }
     }];
    
}

-(void )submitWithAction:(NSString*)action andParams:(NSDictionary*)dic andIam:(NSObject*)obj and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    __block NSObject* busObj = [[NSObject alloc] init];
    
    NSMutableDictionary* essam = [[NSMutableDictionary alloc] initWithDictionary:dic];
    NSMutableDictionary* forMetwlly;
    UIImage* forMetwallyImage;
    NSString* jsonString;
    NSArray* tempKeysArray;
    NSArray* tempValuesArray;
    
    if ([essam objectForKey:@"profilePic"]) {
        //UIImage * myIZmage = [essam objectForKey:@"profilePic"] ;
        NSData* profilePic = UIImageJPEGRepresentation([essam objectForKey:@"profilePic"],1.0);
        [essam removeObjectForKey:@"profilePic"];
        tempKeysArray =[NSArray arrayWithObjects:@"data",@"photo" ,nil];
        jsonString =[[DataHelper getInstance] convertToJSONString:essam];
        tempValuesArray = [NSArray arrayWithObjects:jsonString ,profilePic, nil];
    }
     else if([dic objectForKey:@"photo"])
     {
         [[dic objectForKey:@"photo"] validateObject];
         NSDictionary* dic1=[[DataHelper getInstance] dictionaryOfPropertiesForObject:[dic objectForKey:@"photo"]];
         forMetwlly = [[NSMutableDictionary alloc] initWithDictionary:dic];
         forMetwallyImage =[dic1 objectForKey:@"image"];
         [forMetwlly setObject:dic1 forKey:@"photo"];
         [[forMetwlly objectForKey:@"photo"] removeObjectForKey:@"image"];
         jsonString =[[DataHelper getInstance] convertToJSONString:forMetwlly];
         tempKeysArray =[NSArray arrayWithObjects:@"data",@"img" ,nil];
         tempValuesArray = [NSArray arrayWithObjects:jsonString ,UIImageJPEGRepresentation (forMetwallyImage,1.0), nil];
     
     }
     else
     {
        jsonString =[[DataHelper getInstance] convertToJSONString:dic];
        tempKeysArray =[NSArray arrayWithObject:@"data"];
        tempValuesArray = [NSArray arrayWithObject:jsonString];
     }
    
    [RestServiceAgent setRequestType:typePost toUrl:[action isEqualToString:@"getBaseURL"] ? @"http://barca.tawasoldev.com/barca/index.php/api/version/": [(BarcelonaAppDelegate*) [[UIApplication sharedApplication] delegate] BaseURL] andAction:action withParamsKeys:tempKeysArray andParamsValues:tempValuesArray andTimeout:1200 and:^(id currentClient) {
        NSLog(@"%@",currentClient);
        if([currentClient isKindOfClass:[NSString class]])
        {
           // @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Timeout exception" userInfo:nil];
            NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"connection_noConnection"]  code:1 userInfo:nil];
            failure(error);
        }
        else
        {
            if([[currentClient objectForKey:@"status"] intValue] == 2)
            {
                if([currentClient objectForKey:@"data"] && obj != nil)
                {
                    if([[currentClient objectForKey:@"data"] count]>0)
                    {
                        NSLog(@"%@",tempValuesArray);
                      DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[obj class]];
                      busObj = [parser parseDictionary:[currentClient objectForKey:@"data"]];
                        NSLog(@"%@",[obj class]);
                        
                        handler(busObj);
                    }
                    else
                    {
                        handler(nil);
                    }
                    
                }else{
                    handler(currentClient);

                }
            }
            else if([[currentClient objectForKey:@"status"] intValue] == 1)
            {
                handler(currentClient);
            }
            else
            {
                if([[currentClient objectForKey:@"status"] intValue] == -3)
                {
                    ValidationRule* valObject = [[ValidationRule alloc] init];
                    DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[valObject class]];
                    valObject = [parser parseDictionary:currentClient];
                    NSLog(@"%@",[valObject class]);
                    
//                    NSException *exception = [NSException exceptionWithName:NSInternalInconsistencyException
//                                                                     reason:valObject.errorMessage
//                                                                   userInfo:nil];
//
//                    @throw exception;
                    NSError* error = [NSError errorWithDomain:valObject.errorMessage code:1 userInfo:nil];
                    failure(error);
                    
                }
                else
                {
                    if([[currentClient objectForKey:@"status"] intValue] == -2)
                    {
                       // @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"webservice server exception" userInfo:nil];
                        NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"no_data"] code:1 userInfo:nil];
                        failure(error);
                    }
                    else if([[currentClient objectForKey:@"status"] intValue] == -1)
                    {
                        //@throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"webservice parameters error" userInfo:nil];
                        NSError* error = [NSError errorWithDomain:@"webservice parameters error" code:1 userInfo:nil];
                        failure(error);

                        
                    }
                    else
                    {
                        //@throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"unknown Exception" userInfo:nil];
                        NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"error"] code:1 userInfo:nil];
                        failure(error);

                    }
                    
                }
            }
        }
        
        
    }];
    
}


-(void )getObjectListWithAction:(NSString*)action andParams:(NSDictionary*)dic andIam:(NSObject*)obj and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    __block NSObject* busObj = [[NSObject alloc] init];
    __block NSMutableArray* busObjsArray = [[NSMutableArray alloc] init];
    NSString* jsonString;
    
 
    jsonString =[[DataHelper getInstance] convertToJSONString:dic];
   
    
    [RestServiceAgent setRequestType:typePost toUrl:[(BarcelonaAppDelegate*) [[UIApplication sharedApplication] delegate] BaseURL] andAction:action withParamsKeys:[NSArray arrayWithObject:@"data"] andParamsValues:[NSArray arrayWithObject:jsonString] andTimeout:120 and:^(id currentClient) {
        NSLog(@"%@ %@",action,currentClient);
        if([currentClient isKindOfClass:[NSString class]])
        {
            NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] code:1 userInfo:nil];
            failure(error);
        }
        else
        {
            if([[currentClient objectForKey:@"status"] intValue] == 2)
            {
                if([currentClient objectForKey:@"data"])
                {
//                    NSLog(@"%@",action);
//                    NSLog(@"%@",dic);
//
//                    if([dic objectForKey:@"pageNo"])
//                    {
//                        [GetAndPushCashedData cashObject:currentClient withAction:action];
//                    }
//                    else
//                    {
//                        if([action isEqualToString:@"getTopTen"])
//                        {
//                            [GetAndPushCashedData cashObject:currentClient withAction:action];
//                        }
//                    }
                    
                    if([currentClient objectForKey:@"data"] && obj != nil)
                    {
                        if ([obj isKindOfClass:[Post class]])
                        {
                            if ([[currentClient objectForKey:@"data"] objectForKey:@"currentRequestTime"] ) {
                                NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
                                [defaults setObject:[[currentClient objectForKey:@"data"] objectForKey:@"currentRequestTime"] forKey:@"currentRequestTime"];
                                [defaults synchronize];
                            }
                        }
                        DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[obj class]];
                        if ([obj isKindOfClass:[Post class]]) {
                            for(NSDictionary* tempDic in [currentClient objectForKey:@"data"][@"posts"])
                            {
                                busObj = [parser parseDictionary:tempDic];
                                [busObjsArray addObject:busObj];
                            }
                            NSLog(@"%@",[obj class]);
                            handler(busObjsArray);
                        }
                        else {
                            if ([obj isKindOfClass:[Fan class]]) {
                                
                                if ([action isEqualToString:@"fans/loadFans"] || [action isEqualToString:@"fans/getSuggestionsFans"] || [action isEqualToString:@"fans/loadFollowersForFan"] || [action isEqualToString:@"fans/loadFollowingForFan"] || [action isEqualToString:@"fans/loadFollowingSuggestion"]) {
                                    for(NSDictionary* tempDic in [[currentClient objectForKey:@"data"] objectForKey:@"data"])
                                    {
                                        busObj = [parser parseDictionary:tempDic];
                                        [busObjsArray addObject:busObj];
                                    }
                                    NSLog(@"%@",[obj class]);
                                    
                                    NSString *str = [[currentClient objectForKey:@"data"] objectForKey:@" nextStartingLimit"];
                                    str = (str == nil) ? @"" : str;
                                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:busObjsArray, @"fansList", str, @"nextStartingLimit", nil];
                                    handler(dict);
                                }
                                else if ([action isEqualToString:@"fans/loadPremuimBar"] || [action isEqualToString:@"fans/loadFriendRequests"] || [action isEqualToString:@"fans/loadContacts"]) {
                                    for(NSDictionary* tempDic in [currentClient objectForKey:@"data"])
                                    {
                                        busObj = [parser parseDictionary:tempDic];
                                        [busObjsArray addObject:busObj];
                                    }
                                    NSLog(@"%@",[obj class]);
                                    handler(busObjsArray);
                                }
                                else if ([action isEqualToString:@"fans/whoInstalledApp"]) {
                                    for(NSDictionary* tempDic in [currentClient objectForKey:@"installed"])
                                    {
                                        busObj = [parser parseDictionary:tempDic];
                                        [busObjsArray addObject:busObj];
                                    }
                                    NSLog(@"%@",[obj class]);
                                    handler(busObjsArray);
                                }
                                else if ([action isEqualToString:@"fans/getPublicProfileFan"]) {
                                    busObj = [parser parseDictionary:[currentClient objectForKey:@"data"]];
                                    NSLog(@"%@",[obj class]);
                                    handler(busObj);
                                }
                            }
                            else if ([obj isKindOfClass:[Friend class]]) {
                                if ([action isEqualToString:@"chat/loadFriends"]) {
                                    for(NSDictionary* tempDic in [[currentClient objectForKey:@"data"] objectForKey:@"data"])
                                    {
                                        busObj = [parser parseDictionary:tempDic];
                                        [busObjsArray addObject:busObj];
                                    }
                                    NSLog(@"%@",[obj class]);
                                    
                                    NSString *nextStartingLimit = (currentClient[@"data"][@"nextStartingLimit"] == nil) ? @"" : currentClient[@"data"][@"nextStartingLimit"];
                                    [GetAndPushCashedData cashObject:nextStartingLimit withAction:@"nextStartingLimit"];
                                    

                                    NSString *friendRequests = (currentClient[@"data"][@" friendRequests"] == nil) ? @"" : currentClient[@"data"][@" friendRequests"];
                                    
                                    NSArray *fansListDB = [[currentClient objectForKey:@"data"] objectForKey:@"data"];
                                    
                                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:fansListDB, @"fansListDB", busObjsArray, @"fansList", nextStartingLimit, @"nextStartingLimit", friendRequests, @"friendRequests", nil];
                                    handler(dict);
                                }
                            }
                            else {
                                for(NSDictionary* tempDic in [currentClient objectForKey:@"data"] )
                                {
                                    busObj = [parser parseDictionary:tempDic];
                                    [busObjsArray addObject:busObj];
                                }
                                NSLog(@"%@",[obj class]);
                                handler(busObjsArray);
                            }
                        }
                    }
                    else if([currentClient objectForKey:@"data"])
                    {
                        handler([currentClient objectForKey:@"data"]);
                    }
                    else
                    {
                        handler(busObjsArray);
                    }
                    //handler();
                }
            }
            else if([[currentClient objectForKey:@"status"] intValue] == 1)
            {
                //sucess;
                handler(currentClient);
            }
            else
            {
                if([[currentClient objectForKey:@"status"] intValue] == -3)
                {
                    ValidationRule* valObject = [[ValidationRule alloc] init];
                    DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[valObject class]];
                    valObject = [parser parseDictionary:currentClient];
                    NSLog(@"%@",[valObject class]);
                    NSError* error = [NSError errorWithDomain:valObject.errorMessage code:1 userInfo:nil];
                    failure(error);

                    //handler(valObject);
                    
                }
                else
                {
                    if([[currentClient objectForKey:@"status"] intValue] == -2)
                    {
//                        @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                       reason:@"webservice server exception"
//                                                     userInfo:nil];
                        NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"no_data"] code:1 userInfo:nil];
                        failure(error);
                    }
                    else if([[currentClient objectForKey:@"status"] intValue] == -1)
                    {
//                        @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                       reason:@"webservice parameters error"
//                                                     userInfo:nil];
                        NSError* error = [NSError errorWithDomain:@"webservice parameters error" code:1 userInfo:nil];
                        failure(error);
                    }
                    else
                    {
//                        @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                       reason:@"unknown Exception"
//                                                     userInfo:nil];
                        NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"error"] code:1 userInfo:nil];
                        failure(error);
                    }
                    
                }
            }
        }
        
    }];
    
}
-(void )getObjectListWithAction:(NSString*)action andIam:(NSObject*)obj and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
{
    __block NSObject* busObj = [[NSObject alloc] init];
    __block NSMutableArray* busObjsArray = [[NSMutableArray alloc] init];
    //NSString* jsonString =[[DataHelper getInstance] convertToJSONString:dic];
    [RestServiceAgent setRequestType:typePost toUrl:[(BarcelonaAppDelegate*) [[UIApplication sharedApplication] delegate] BaseURL] andAction:action withParamsKeys:[NSArray arrayWithObject:@""] andParamsValues:[NSArray arrayWithObject:@""] andTimeout:120 and:^(id currentClient) {
        NSLog(@"%@",currentClient);
        if([currentClient isKindOfClass:[NSString class]])
        {
//            @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                           reason:@"Timeout exception"
//                                         userInfo:nil];
            NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] code:1 userInfo:nil];
            failure(error);
        }
        else
        {
            if([[currentClient objectForKey:@"status"] intValue] == 2)
            {
                if([currentClient objectForKey:@"data"])
                {
                    if([[currentClient objectForKey:@"data"] count]>0)
                    {
                        DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[obj class]];
                        for(NSDictionary* tempDic in [currentClient objectForKey:@"data"])
                        {
                            busObj = [parser parseDictionary:tempDic];
                            [busObjsArray addObject:busObj];
                        }
                        NSLog(@"%@",[obj class]);
                        handler(busObjsArray);
                        //handler();
                    }
                    else
                    {
                        handler(busObjsArray);
                    }
                }
            }
            else if([[currentClient objectForKey:@"status"] intValue] == 1)
            {
                //sucess;
                handler(currentClient);
            }
            else
            {
                if([[currentClient objectForKey:@"status"] intValue] == -3)
                {
                    ValidationRule* valObject = [[ValidationRule alloc] init];
                    DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[valObject class]];
                    valObject = [parser parseDictionary:[currentClient objectForKey:@"data"]];
                    NSLog(@"%@",[valObject class]);
                    NSError* error = [NSError errorWithDomain:valObject.errorMessage code:1 userInfo:nil];
                    failure(error);
                    //handler(valObject);
                    
                }
                else
                {
                    if([[currentClient objectForKey:@"status"] intValue] == -2)
                    {
//                        @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                     reason:@"webservice server exception"
//                                                     userInfo:nil];
                        NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"no_data"] code:1 userInfo:nil];
                        failure(error);
                    }
                    else if([[currentClient objectForKey:@"status"] intValue] == -1)
                    {
//                        @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                     reason:@"webservice parameters error"
//                                                     userInfo:nil];
                        NSError* error = [NSError errorWithDomain:@"webservice parameters error" code:1 userInfo:nil];
                        failure(error);
                    }
                    else
                    {
//                        @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                                       reason:@"unknown Exception"
//                                                     userInfo:nil];
                        NSError* error = [NSError errorWithDomain:[[Language sharedInstance] stringWithKey:@"error"] code:1 userInfo:nil];
                        failure(error);

                    }
                    
                }
            }
            
        }
        
    }];
    
}

//-(NSString*)convertToJSONString:(id)input
//{
//    NSMutableDictionary* tempDic = [input mutableCopy];
//    for(NSString* key in [input allKeys])
//    {
//        if([[tempDic objectForKey:key] isKindOfClass:[NSArray class]])
//        {
//            [tempDic removeObjectForKey:key];
//        }
//    }
//
//    NSError *error;
//
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tempDic options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
//                                                         error:&error];
//    NSString *jsonString;
//    if (! jsonData) {
//        NSLog(@"Got an error: %@", error);
//    } else {
//       jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    }
//    return jsonString;
//}
//
//- (NSDictionary *)dictionaryOfPropertiesForObject:(id)object
//{
//    // somewhere to store the results
//    NSMutableDictionary *result = [NSMutableDictionary dictionary];
//
//    // we'll grab properties for this class and every superclass
//    // other than NSObject
//    Class classOfObject = [object class];
//    while(![classOfObject isEqual:[NSObject class]])
//    {
//        // ask the runtime to give us a C array of the properties defined
//        // for this class (which doesn't include those for the superclass)
//        unsigned int numberOfProperties;
//        objc_property_t  *properties =
//        class_copyPropertyList(classOfObject, &numberOfProperties);
//
//        // go through each property in turn...
//        for(
//            int propertyNumber = 0;
//            propertyNumber < numberOfProperties;
//            propertyNumber++)
//        {
//            // get the name and convert it to an NSString
//            NSString *nameOfProperty = [NSString stringWithUTF8String:
//                                        property_getName(properties[propertyNumber])];
//
//            // use key-value coding to get the property value
//            id propertyValue = [object valueForKey:nameOfProperty];
//            
//            // add the value to the dictionary —
//            // we'll want to transmit NULLs, even though an NSDictionary
//            // can't store nils
//            [result
//             setObject:propertyValue ? propertyValue : [NSNull null]
//             forKey:nameOfProperty];
//        }
//        
//        // we took a copy of the property list, so...
//        free(properties);
//        
//        // we'll want to consider the superclass too
//        classOfObject = [classOfObject superclass];
//    }
//    
//    // return the dictionary
//    return result;
//}

-(void)deleteObject:(int)objId
{
    
}

+(BusinessObject*)getObject:(int)objId
{
    BusinessObject* busObj = [[BusinessObject alloc] init];
    return busObj;
}
+(NSArray*)getObjectList
{
    return [[NSArray alloc] init];
}
@end
