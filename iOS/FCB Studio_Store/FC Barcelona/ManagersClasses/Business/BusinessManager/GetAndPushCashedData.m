//
//  GetAndPushCashedData.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 10/11/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "GetAndPushCashedData.h"

@implementation GetAndPushCashedData

+(void)cashObject:(id)object withAction:(NSString*)action
{
    NSUserDefaults* def = [[NSUserDefaults alloc] initWithSuiteName:@"FCB_Data"];
    [def setObject:[NSKeyedArchiver archivedDataWithRootObject:object] forKey:action];
    [def synchronize];
}

+(id)getObjectFromCash:(NSString*)action
{
    id info;
    NSUserDefaults* def = [[NSUserDefaults alloc] initWithSuiteName:@"FCB_Data"];
    info = [NSKeyedUnarchiver unarchiveObjectWithData:[def objectForKey:action]];
    return info;
}

+(void)removeObjectWithAction:(NSString*)action
{
    NSUserDefaults* def = [[NSUserDefaults alloc] initWithSuiteName:@"FCB_Data"];
    [def removeObjectForKey:action];
    [def synchronize];
}


@end
