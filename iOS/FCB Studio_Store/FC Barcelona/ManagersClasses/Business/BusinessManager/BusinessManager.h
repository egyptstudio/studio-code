//
//  BusinessManager.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessObject.h"
#import "UserComment.h"

@interface BusinessManager : NSObject
-(void)saveObjectWithAction:(NSString*)action andObject:(UserComment*)obj and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failur;
-(void)deleteObject:(int)objId;
//-(NSObject *)submitWithAction:(NSString*)action andParams:(NSDictionary*)dic andIam:(NSObject*)obj;
//-(void )submitWithAction:(NSString*)action andParams:(NSDictionary*)dic andIam:(NSObject*)obj and:(void (^)(id currentClient))handler;
-(void )submitWithAction:(NSString*)action andParams:(NSDictionary*)dic andIam:(NSObject*)obj and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

+(BusinessObject*)getObject:(int)objId;
-(void )getObjectListWithAction:(NSString*)action andParams:(NSDictionary*)dic andIam:(NSObject*)obj and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;;
-(void )getObjectListWithAction:(NSString*)action andIam:(NSObject*)obj and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;;

+(NSArray*)getObjectList;
@end
