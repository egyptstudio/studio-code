//
//  GetAndPushCashedData.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 10/11/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
// to essam
//
#import <Foundation/Foundation.h>

@interface GetAndPushCashedData : NSObject
+(void)cashObject:(id)object withAction:(NSString*)action;
+(id)getObjectFromCash:(NSString*)action;
+(void)removeObjectWithAction:(NSString*)action;
@end
