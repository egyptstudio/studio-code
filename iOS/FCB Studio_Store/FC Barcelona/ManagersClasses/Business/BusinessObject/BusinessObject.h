//
//  BusinessObject.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ValidationRule.h"


typedef enum {
	New = 1,
	loaded,
    cached,
} ObjectStateEnum;


@interface BusinessObject : NSObject


@property (nonatomic, assign) int Id;
@property (nonatomic, assign) ObjectStateEnum state;
@property (nonatomic, strong) NSMutableArray *childs;
@property (nonatomic, strong) NSMutableArray* validationRules;
@property (nonatomic, strong) NSString * saveMethodName;
@property (nonatomic, strong) NSString * deleteMethodName;
@property (nonatomic, strong) NSString * getMethodName;
@property (nonatomic) BOOL isValid;

-(void)addValidationRules;
-(void)validateObject;
@end
