//
//  BusinessObject.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "BusinessObject.h"

@implementation BusinessObject

-(void)addValidationRules
{
//    #warning you must overwrite this function;
//    @throw [NSException exceptionWithName:NSInternalInconsistencyException
//                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
//                                 userInfo:nil];
}

-(void)validateObject
{
    [self.validationRules removeAllObjects];
    [self addValidationRules];
    for ( ValidationRule* rule in self.validationRules)
    {
        SEL sel = (SEL)rule.validateRule;
        [self performSelector:sel];
    }
}
@end
