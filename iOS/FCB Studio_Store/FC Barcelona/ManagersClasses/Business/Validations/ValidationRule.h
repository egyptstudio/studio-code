//
//  ValidationRule.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 9/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValidationRule : NSObject
@property (nonatomic, strong) NSString *ruleName;
@property (nonatomic, strong) NSString *errorMessage;
@property (nonatomic) BOOL passed;
@property (nonatomic)SEL validateRule;

@end
