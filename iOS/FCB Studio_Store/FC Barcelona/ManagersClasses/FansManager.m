//
//  FansManager.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FansManager.h"
#import "Fan.h"
#import "DataHelper.h"

@implementation FansManager
static FansManager* instance;
+(FansManager *)getInstance{
    if (!instance) {
        instance=[[FansManager alloc] init];
    }
    return instance;
}

-(void)getFan:(int)userId and:(void (^)(id))handler onFailure:(void (^)(NSError *))failure{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"getFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];

}

-(void)getFans:(int)userId pageNo:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                    [NSString stringWithFormat:@"%d",pageNo], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"pageNo", nil];

    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"getFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}




-(void)getFans:(int)userId pageNo:(int)pageNo searchText:(NSString*)searchText sortBy:(int)sortEnum filters:(NSMutableDictionary*)filters countriesIds:(NSArray*)countriesIds favoriteFans:(BOOL)favorateFan and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                                              [NSString stringWithFormat:@"%d",pageNo],
                                              searchText,
                                              [NSString stringWithFormat:@"%d",sortEnum],
                                              [DataHelper convertToJSONString:filters],
                                              [DataHelper convertToJSONString:countriesIds],
                                              favorateFan,nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"userId",
                                              @"pageNo",
                                              @"searchText",
                                              @"sortBy",
                                              @"filters",
                                              @"countriesIds",
                                              @"favoriteFans",nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"getFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];

}
-(void)followFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"followFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)unFollowFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"unfollowFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)addFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"addFriendRequest" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)acceptFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"acceptFriendRequest" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)rejectFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"rejectFriendRequest" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)removeFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"removeFriend" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)addFavoriteFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"addFavoriteFriend" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)removeFavoriteFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"removeFavoriteFriend" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];

}
-(void)blockFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"blockFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)unBlockFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                                              [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"unBlockFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

@end
