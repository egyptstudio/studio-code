//
//  ShareViewController.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/1/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <GoogleMobileAds/GADInterstitial.h>
#import "ShareViewController.h"
#import "ShareLibrary.h"
#import "User.h"
#import "ShareManagerViewController.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
#import "ShareLibrary.h"
#import <objc/message.h>
#import "CustomButton.h"
#import "CurrentUser.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "StudioDetailsViewController.h"
#import "StudioManager.h"
#import <FacebookSDK/FacebookSDK.h>
//#import "ExtraItemsViewController.h"
#import "StudioManager.h"
#import "EditProfileViewController.h"

@interface ShareViewController ()<UITextViewDelegate,UITextFieldDelegate,UIDocumentInteractionControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,FBLoginViewDelegate,GADInterstitialDelegate>{
    NSMutableArray* tags;
    __weak IBOutlet UISlider *slider;
    __weak IBOutlet UIView *tagsView;
    __weak IBOutlet UITextView *tagsField;
    float numberOfRemainingCharacters;
    __weak IBOutlet UITextView *captionTextView;
    IBOutletCollection(UIImageView) NSArray *serverTags;
    IBOutletCollection(UILabel) NSArray *serverLabels;
    NSMutableArray* serverPredefinedTags;
    ALAssetsLibrary* library;
    UIAlertView* saveAlert;
    TwitterLogin *tLogin;
    BOOL isTweeting;
    BOOL isFacebooking;
    FBLoginView *facebookLogin;
    NSString *postSharingUrl;
    int creditPoints;
    BOOL isStartEditing;
    
}
@property(nonatomic, strong) GADInterstitial *interstitial;
@property(nonatomic, strong) UIDocumentInteractionController* docController;
@end

@implementation ShareViewController
@synthesize currentPost,delegate;

-(void)showInterstitial{

    if ([self.interstitial isReady]) {
        [self.interstitial presentFromRootViewController:self];
    }
    else{
    
        [self interstitialDidDismissScreen:nil];
    }

}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    NSString* successMessage;
    switch ((int)slider.value) {
        case 1:
            successMessage= [[Language sharedInstance] stringWithKey:@"share2_affirm1"];
            break;
        case 2:
            successMessage= [[Language sharedInstance] stringWithKey:@"share2_affirm2"];
            break;
        case 3:
            successMessage= [[Language sharedInstance] stringWithKey:@"share2_affirm3"];
            break;
        default:
            break;
    }
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:successMessage delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
    [alert setTag:100];
    [alert show];
}


-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-2924334778141972/3060492441"];
    [self.interstitial setDelegate:self];
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"abc7f676a40f3514a5a8c71ad28dfd27"];
    [self.interstitial loadRequest:request];
    
    
    isStartEditing = NO;
    _tagText.delegate = self;
    library=[[ALAssetsLibrary alloc] init];
    [_shareTwitterButton setBackgroundColor:[UIColor clearColor]];
    CALayer * l = [self.postButton layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:3];
    
    CALayer * l1 = [self.captionView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:3];
    
    CALayer * l2 = [self.tagsView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:3];
    
    CALayer * l3 = [self.imageView2 layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:2];
    [slider setThumbImage:[UIImage imageNamed:@"seen.png"] forState:UIControlStateNormal];
    
    tLogin = [TwitterLogin getInstance];
    tLogin.delegate =  self;
    
    [self scaleImage:_editedImage toSize:500*1024];
    objc_msgSend([UIDevice currentDevice], @selector(setOrientation:),UIInterfaceOrientationPortrait );
    for (int x=0; x<[serverTags count]; x++)
    {
        [(UIImageView*)serverTags[x] setAlpha:0];
        [(UILabel*)serverLabels[x] setAlpha:0];
    }
    if (!self.isEditImage)
    {
        captionTextView.text = [[Language sharedInstance] stringWithKey:@"EditMyPhoto_Caption"];

        serverPredefinedTags=[_studioPhoto tags];
        
        if (!tags)
        {
            tags=[[NSMutableArray alloc]init];
        }
        if (!serverPredefinedTags)
        {
            serverPredefinedTags=[[NSMutableArray alloc] init];
        }
        for (int x=0; x<[serverPredefinedTags count]; x++) {
            StudioPhotoTag * tag =(StudioPhotoTag *)[serverPredefinedTags objectAtIndex:x];
            [self appendSubView:[self prepareViewForTagWithText:tag.tagName isServer:YES] onParentView:_tagsScroll ];
            [tags addObject:[self dictionaryOfPropertiesForObject:tag]];
        }
    }
    else
    {
        
        serverPredefinedTags=[currentPost tags];
        
        if (!tags)
        {
            tags=[[NSMutableArray alloc]init];
        }
        if (!serverPredefinedTags)
        {
            serverPredefinedTags=[[NSMutableArray alloc] init];
        }
        for (int x=0; x<[serverPredefinedTags count]; x++)
        {
            NSDictionary *tag1 = [serverPredefinedTags objectAtIndex:x];
            StudioPhotoTag * tag = [[StudioPhotoTag alloc] init];
            tag.tagId = [tag1[@"tagId"] intValue];
            tag.tagType = [tag1[@"tagType"] intValue];
            tag.tagName = tag1[@"tagName"];
            [self appendSubView:[self prepareViewForTagWithText:tag.tagName isServer:YES] onParentView:_tagsScroll ];
            [tags addObject:[self dictionaryOfPropertiesForObject:tag]];
            
            switch ([currentPost privacy]) {
                case 4:
                    [slider setValue:1];
                    break;
                case 3:
                    [slider setValue:2];
                    break;
                case 2:
                    [slider setValue:3];
                    break;
                default:
                    break;
            }
        }
    }
    numberOfRemainingCharacters=200;
    [_imageView2 setImage:_editedImage];
    tagsView.userInteractionEnabled=NO;
    _mainView.userInteractionEnabled=YES;
    
    if([[CurrentUser getObject] isUser] && [[[CurrentUser getObject] getUser] calculateProfileCompletion] < 100)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"complete_profile"] delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles:nil];
        alert.tag = 200;
        [alert show];
    }
    if (self.isEditImage)
    {
        self.shareTwitterButton.hidden=YES;
        self.shareFacebookButton.hidden=YES;
        self.lbl_Point.hidden=YES;
    }
    _contetsLabel.text = [[Language sharedInstance] stringWithKey:@"EditMyPhoto_contestEligabilityState3"];
    _contestImage.image = [UIImage imageNamed:@"contest_yellow.png"];
    
    
    UITapGestureRecognizer *editProfileTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenEditProfile)];
    [editProfileTap setNumberOfTapsRequired:1];
    [_contestImage setUserInteractionEnabled:YES];
    [_contestImage addGestureRecognizer:editProfileTap];
    
    [slider setValue:3];
    [self setPrivacy:slider];
}

-(void)OpenEditProfile{
    EditProfileViewController* view = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    [self.navigationController pushViewController:view animated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    //[SVProgressHUD dismiss];
    
    
    [_lbl_onlyMe  setText:[[Language sharedInstance] stringWithKey:@"me"]];
    [_lbl_Friends setText:[[Language sharedInstance] stringWithKey:@"menu_Friends"]];
    [_lbl_Public  setText:[[Language sharedInstance] stringWithKey:@"EditMyPhoto_privacy3"]];
    [_lbl_ShareOnFaceTwitter setText:[[Language sharedInstance] stringWithKey:@"ShareOnFaceTwitter"]];
    [_lbl_Title   setText:[[Language sharedInstance] stringWithKey:@"AboutUs_Share"]];
    [_tagText     setPlaceholder:[[Language sharedInstance] stringWithKey:@"EditMyPhoto_addTagDefaultValue"]];
    [_postButton   setTitle:[[Language sharedInstance] stringWithKey:@"Comments_post"] forState:UIControlStateNormal];
    [_cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareToApp:(id)sender {
    [library saveImage:_imageView2.image toAlbum:@"FCB" withCompletionBlock:^(NSError *error) {
        saveAlert = [[UIAlertView alloc] initWithTitle:nil message:@"your image has been saved to your device" delegate:nil cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles: nil];
        [saveAlert show];
        [saveAlert setDelegate:self];
        
    }];
    
    
    //////////////////////////////////////////
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==100)
    {
        [[CurrentUser getObject] updateUserPoints];
        [self.navigationController popToRootViewControllerAnimated:NO];
        switch ((int)slider.value) {
            case 1:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postingDone" object:@(3)];
                break;
            case 2:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postingDone" object:@(2)];
                break;
            case 3:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postingDone" object:@(1)];
                break;
            default:
                break;
        }
    }
    
    if(buttonIndex != alertView.cancelButtonIndex){
        if ( alertView.tag == 200 ) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
        }
        else{
            
            BOOL fromUse = YES;

            for (int i = 0 ; i < [self.navigationController.viewControllers count]; i++) {
                if ([self.navigationController.viewControllers[i] isKindOfClass:[StudioDetailsViewController class]]) {
                    fromUse = NO;
                    [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                }
            }
            
            if (fromUse) {
                [self.navigationController popViewControllerAnimated:YES];
            }

        }
    }
}
-(void)showNoInternet{
    [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"please check internet connection" delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:nil] show];
}
- (IBAction)shareInstagram:(id)sender {
    
    
    
    
}



- (IBAction)showTagView:(id)sender {
    [UIView animateWithDuration:.7 animations:^{
        _mainView.alpha=0.5;
        tagsView.alpha=1;
        _mainView.userInteractionEnabled=0;
        tagsView.userInteractionEnabled=1;
    }];
}



-(BOOL)shouldAutorotate{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)setPrivacy:(id)sender {
    float roundedValue = roundf(slider.value / 1.0f) * 1.0f;
    [slider setValue:roundedValue];
    switch ((int)roundedValue) {
        case 1:
        {
            if([[CurrentUser getObject] isUser] && [[[CurrentUser getObject] getUser] calculateProfileCompletion] < 100)
            {
                _contetsLabel.text = [[Language sharedInstance] stringWithKey:@"EditMyPhoto_contestEligabilityState4"];
                _contetsLabel.textColor = [UIColor blueColor];
                _contestImage.image = [UIImage imageNamed:@"contest_blue.png"];
                
            }
            else{
                _contetsLabel.text = [[Language sharedInstance] stringWithKey:@"EditMyPhoto_contestEligabilityState3"];
                _contetsLabel.textColor = [UIColor yellowColor];
                _contestImage.image = [UIImage imageNamed:@"contest_yellow.png"];
            }
        }
            break;
        case 2:
        {
            if([[CurrentUser getObject] isUser] && [[[CurrentUser getObject] getUser] calculateProfileCompletion] < 100)
            {
                _contetsLabel.text = [[Language sharedInstance] stringWithKey:@"EditMyPhoto_contestEligabilityState4"];
                _contetsLabel.textColor = [UIColor blueColor];
                _contestImage.image = [UIImage imageNamed:@"contest_blue.png"];
            }
            else{
                _contestImage.image = [UIImage imageNamed:@"contest_yellow.png"];
                _contetsLabel.textColor = [UIColor yellowColor];
                _contetsLabel.text = [[Language sharedInstance] stringWithKey:@"EditMyPhoto_contestEligabilityState3"];
            }
            
        }
            break;
        case 3:
        {
            if([[CurrentUser getObject] isUser] && [[[CurrentUser getObject] getUser] calculateProfileCompletion] < 100)
            {
                _contestImage.image = [UIImage imageNamed:@"contest_red.png"];
                _contetsLabel.textColor = [UIColor redColor];
                _contetsLabel.text = [[Language sharedInstance] stringWithKey:@"EditMyPhoto_contestEligabilityState2"];
            }
            else
            {
                _contestImage.image = [UIImage imageNamed:@"contest_green.png"];
                _contetsLabel.textColor = [UIColor greenColor];
                _contetsLabel.text = [[Language sharedInstance] stringWithKey:@"EditMyPhoto_contestEligabilityState1"];
            }
        }
            break;
        default:
            break;
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (textView == captionTextView)
    {
        NSUInteger newLength = [textView.text length] + [text length] - range.length;
        return (newLength > 200) ? NO : YES;
    }
    else if(textView == tagsField)
    {
        NSUInteger newLength = [textView.text length] + [text length] - range.length;
        return (newLength > numberOfRemainingCharacters) ? NO : YES;
    }
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    return (newLength > 200) ? NO : YES;
}


- (IBAction)addTag:(id)sender {
    
    if (_tagText.text.length == 0)
    {
        UIAlertView* error = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[[Language sharedInstance] stringWithKey:@"tag_text_required"]
                                                       delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [error show];
    }
    else if ( _tagText.text.length < 4) {
        UIAlertView* error = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[[Language sharedInstance] stringWithKey:@"validateTag_1"]
                                                       delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [error show];
    }else if ([tags count]==5)
    {
        UIAlertView* error = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[[Language sharedInstance] stringWithKey:@"validateTag_2"]
                                                       delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [error show];
    }
    else
    {
        StudioPhotoTag * tag = [[StudioPhotoTag alloc] init];
        tag.tagId = -1;
        tag.tagType = 2;
        tag.tagName = _tagText.text;
        [tags addObject:[self dictionaryOfPropertiesForObject:tag]];
        [self appendSubView:[self prepareViewForTagWithText:_tagText.text isServer:NO] onParentView:_tagsScroll ];
        _tagText.text=@"";
    }
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(UIView*)prepareViewForTagWithText:(NSString*)tagText isServer:(BOOL)isServer
{
    UIFont *font = [UIFont systemFontOfSize:8] ;
    
    UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, 40, 18)];
    [tagLabel setFont:font];
    [tagLabel setTextAlignment:NSTextAlignmentLeft];
    [tagLabel setTextColor:[UIColor blackColor]];
    [tagLabel setBackgroundColor:[UIColor clearColor]];
    [tagLabel setText:tagText];
    
    UIImage *tagBackground = isServer ? [UIImage imageNamed:@"tag.png"]:[UIImage imageNamed:@"tag.png"];
    
    UIButton *deleteTag = [[UIButton alloc] initWithFrame:CGRectMake(tagBackground.size.width-18, 0,18,18)];
    [deleteTag setImage:[UIImage imageNamed:@"tag_close.png"] forState:UIControlStateNormal];
    [deleteTag addTarget:self action:@selector(removeTagFromView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *tagView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,tagBackground.size.width,tagBackground.size.height)];
    
    [tagView addSubview:tagLabel];
    
    if (!isServer) {
        [tagView addSubview:deleteTag];
    }
    
    UIGraphicsBeginImageContext(tagView.frame.size);
    [tagBackground drawInRect:tagView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    tagView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    return tagView;
}

- (void) textViewDidBeginEditing:(UITextView *) textView {
    if (!isStartEditing) {
        [textView setText:@""];
        isStartEditing=YES;
    }
    //[textView setText:@""];
}

-(void)appendSubView:(UIView*)subSiew onParentView:(UIScrollView*)parentView
{
    
    for (NSObject * obj in parentView.subviews) {
        if (![obj isKindOfClass:[UIImageView class]]) {
            [(UIView*)obj setFrame:CGRectMake([(UIView*)obj frame].origin.x+2+subSiew.frame.size.width, 0, [(UIView*)obj frame].size.width, [(UIView*)obj frame].size.height)];
        }
    }
    subSiew.tag = [parentView.subviews count]-1;
    subSiew.frame = CGRectMake(0, 0, subSiew.frame.size.width, subSiew.frame.size.height);
    [parentView addSubview:subSiew];
    parentView.contentSize = CGSizeMake(parentView.contentSize.width+subSiew.frame.size.width+2, 0);
    
}
-(void)removeTagFromView:(UIButton*) sender
{
    ///////////Remove Tag From Array///////////
    for (NSObject* obj in [[sender superview] subviews] ) {
        if ([obj isKindOfClass:[UILabel class]]) {
            for (NSDictionary *tag in tags) {
                if ([tag[@"tagName"] isEqualToString:[(UILabel*)obj text]])
                {
                    [tags removeObject:tag];
                    break;
                }
            }
        }
    }
    ///////////ReArrange Tags///////////
    for (int i = [_tagsScroll.subviews count]-1 ; i > 0 ; i--) {
        if ([[_tagsScroll.subviews objectAtIndex:i] isEqual:[sender superview]]) {
            for (int j = i-1 ; j >= 0 ; j--)
            {
                [(UIView*)[_tagsScroll.subviews objectAtIndex:j] setFrame:CGRectMake([(UIView*)[_tagsScroll.subviews objectAtIndex:j] frame].origin.x-2-[[sender superview] frame].size.width, 0, [(UIView*)[_tagsScroll.subviews objectAtIndex:j] frame].size.width, [(UIView*)[_tagsScroll.subviews objectAtIndex:j] frame].size.height)];
            }
        }
        
    }
    ///////////Remove Tag From Scroll///////////
    for (NSObject * obj in _tagsScroll.subviews) {
        if (![obj isKindOfClass:[UIImageView class]]) {
            if ([(UIView*)obj tag] == [[sender superview] tag]) {
                [(UIView*)obj removeFromSuperview];
            }
        }
    }
    _tagsScroll.contentSize = CGSizeMake(_tagsScroll.contentSize.width - [sender superview].frame.size.width+2, 0);
    
}

- (IBAction)post_Action:(id)sender{
    [sender setEnabled:NO];
    // if ([[CurrentUser getObject] isUser]) {
    int privacy ;
    switch ((int)slider.value) {
        case 1:
            privacy = 4;
            break;
        case 2:
            privacy = 3;
            break;
        case 3:
            privacy = 2;
            break;
        default:
            break;
    }
    
    
    
    if (self.isEditImage)
    {
        [currentPost setTags:tags];
        [currentPost setPrivacy:privacy];
        [currentPost setPostPhotoCaption:[captionTextView.text isEqualToString:[[Language sharedInstance] stringWithKey:@"EditMyPhoto_Caption"]]?@"":captionTextView.text];
        
        @try {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            if([RestServiceAgent internetAvailable])
            {
                [[WallManager getInstance] editPost:currentPost and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [sender setEnabled:YES];
                        if (currentClient)
                        {
                            [SVProgressHUD dismiss];
                            if([currentClient isKindOfClass:[Post class]])
                            {
                                [self.delegate didViewEditedWith:currentPost];
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                            else{
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Server Error." delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [sender setEnabled:YES];
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else
            {
                [sender setEnabled:YES];
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            
            
        }
        @catch (NSException *exception) {
            [sender setEnabled:YES];
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
                        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
        Post* post = [[Post alloc] init];
        [post setUserId:[[CurrentUser getObject] getUser].userId];
        [post setTags:tags];
        [post setStudioPhotoId:_studioPhoto.studioPhotoId];
        [post setPrivacy:privacy];
        [post setPostPhotoCaption:[captionTextView.text isEqualToString:[[Language sharedInstance] stringWithKey:@"EditMyPhoto_Caption"]]?@"":captionTextView.text];
        [post setContestRank:0];
        @try {
            
            if([RestServiceAgent internetAvailable])
            {
                [[StudioManager getInstance] addPost:post witImage:_editedImage and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            [sender setEnabled:YES];
                            [SVProgressHUD dismiss];
                            if([currentClient isKindOfClass:[Post class]])
                            {
                                [self checkSocialShareForPostId:[(Post*)currentClient postId]];
                                [self showInterstitial];
                                
                                /*ExtraItemsViewController* extraItems = [self.storyboard instantiateViewControllerWithIdentifier:@"ExtraItemsViewController"];
                                 [extraItems setEditedImage:_editedImage];
                                 [extraItems setOriginalImage:_originalImage];
                                 [self.navigationController pushViewController:extraItems animated:YES];
                                 */
                            }
                            else{
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Server Error." delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [sender setEnabled:YES];
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else
            {
                [sender setEnabled:YES];
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            
            
        }
        @catch (NSException *exception) {
            [sender setEnabled:YES];
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
    
    
    //    }else{
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    //    }
    
}

- (IBAction)cancel_Action:(id)sender{
    [[[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"CaptureOption1_cancleAlert"] delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"NO"] otherButtonTitles:[[Language sharedInstance] stringWithKey:@"YES"], nil] show];
}

-(UIImage *) scaleImage:(UIImage*)originalImage toSize:(long)size{
    long originalSize = [UIImageJPEGRepresentation(originalImage, 1) length]/1024;
    CGFloat compressionRatio = size / originalSize;
    UIImage *smallImage = [UIImage imageWithCGImage:originalImage.CGImage
                                              scale:compressionRatio
                                        orientation:originalImage.imageOrientation];
    return smallImage;
}
- (IBAction)shareFacebook:(id)sender {
    if (!isFacebooking) {
        if (!FBSession.activeSession.isOpen) {
            NSArray *permissions = [[NSArray alloc] initWithObjects:@"public_profile",@"email",@"publish_actions",@"status_update", nil];
            [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:
             ^(FBSession *session,FBSessionState state, NSError *error) {
                 if (error) {
                     
                 } else {
                     [_shareFacebookButton setImage:[UIImage imageNamed:@"facebook_sc.png"] forState:UIControlStateNormal];
                     isFacebooking = YES;
                 }
             }];
        }else{
            [_shareFacebookButton setImage:[UIImage imageNamed:@"facebook_sc.png"] forState:UIControlStateNormal];
            isFacebooking = YES;
        }

        
    }
    else{
        isFacebooking = NO;
        [_shareFacebookButton setImage:[UIImage imageNamed:@"facebook"] forState:UIControlStateNormal];
    }
}
-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)fuser{
    [_shareFacebookButton setImage:[UIImage imageNamed:@"facebook_sc.png"] forState:UIControlStateNormal];
    isFacebooking = YES;
}


- (IBAction)shareTwitter:(id)sender {
    if([RestServiceAgent internetAvailable])
    {
        if (!isTweeting) {
            [tLogin openLogger];
        }
        else{
            isTweeting = NO;
            [_shareTwitterButton setImage:[UIImage imageNamed:@"twitter.png"] forState:UIControlStateNormal];
        }
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
- (void)twitterFinishedTweeting:(NSDictionary *)user error: (NSError *)error{
    
    
}
- (void)twitterFinishedWithAuth:(NSDictionary *)user error: (NSError *)error{
    if (!error) {
        [_shareTwitterButton setImage:[UIImage imageNamed:@"twitter_sc.png"] forState:UIControlStateNormal];
        isTweeting = YES;
    }
}
-(void)checkSocialShareForPostId:(int)postId{
    NSString* link = [NSString stringWithFormat:@"http://fcbstudio.mobi/share.php?postid=%i",postId];
    
    if (isTweeting) {
        [tLogin sendTweetWithStatus:link] ;
        [self performSelectorInBackground:@selector(shareSocialMedia) withObject:nil];
        
    }
    if (isFacebooking) {
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"FCB Stuido", @"name",
                                       @"", @"caption",
                                       @"", @"description",
                                       link, @"link",
                                       nil];
        // Make the request
        [FBRequestConnection startWithGraphPath:@"/me/feed"
                                     parameters:params
                                     HTTPMethod:@"POST"
                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                  if (!error) {
                                      
                                      [self performSelectorInBackground:@selector(shareSocialMedia) withObject:nil];
                                  } else {
                                      // An error occurred, we need to handle the error
                                      // See: https://developers.facebook.com/docs/ios/errors
                                      NSLog(@"%@", error.description);
                                  }
                              }];
    }
    
    
}

- (NSDictionary *)dictionaryOfPropertiesForObject:(id)object
{
    // somewhere to store the results
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    // we'll grab properties for this class and every superclass
    // other than NSObject
    Class classOfObject = [object class];
    while(![classOfObject isEqual:[NSObject class]])
    {
        // ask the runtime to give us a C array of the properties defined
        // for this class (which doesn't include those for the superclass)
        unsigned int numberOfProperties;
        objc_property_t  *properties =
        class_copyPropertyList(classOfObject, &numberOfProperties);
        
        // go through each property in turn...
        for(
            int propertyNumber = 0;
            propertyNumber < numberOfProperties;
            propertyNumber++)
        {
            // get the name and convert it to an NSString
            NSString *nameOfProperty = [NSString stringWithUTF8String:
                                        property_getName(properties[propertyNumber])];
            
            // use key-value coding to get the property value
            id propertyValue = [object valueForKey:nameOfProperty];
            
            // add the value to the dictionary —
            // we'll want to transmit NULLs, even though an NSDictionary
            // can't store nils
            [result
             setObject:propertyValue ? propertyValue : [NSNull null]
             forKey:nameOfProperty];
        }
        
        // we took a copy of the property list, so...
        free(properties);
        
        // we'll want to consider the superclass too
        classOfObject = [classOfObject superclass];
    }
    
    [result removeObjectForKey:@"deletedFromRealm"];
    [result removeObjectForKey:@"invalidated"];
    [result removeObjectForKey:@"objectSchema"];
    [result removeObjectForKey:@"realm"];
    
    // return the dictionary
    return result;
}

-(void)shareSocialMedia{
    
    @try {
        if([RestServiceAgent internetAvailable])
        {
            [[StudioManager getInstance] shareSocailMedia:[[CurrentUser getObject] getUser].userId and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (currentClient[@"status"]) {
                        if([currentClient[@"status"] intValue]==1)
                        {
                            //Done
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                
            }];
        }
    }
    @catch (NSException *exception)     {
        
    }
}
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    return newLength <= MAXLENGTH || returnKey;
}


@end
