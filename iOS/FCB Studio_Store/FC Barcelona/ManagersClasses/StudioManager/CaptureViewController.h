//
//  CaptureViewController.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#include <ImageIO/ImageIOBase.h>
#import <ImageIO/CGImageProperties.h>
#import "MMGDrag_Drop.h"
#import "StudioPhoto.h"
#import <AviarySDK/AviarySDK.h>

@interface CaptureViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UIView *mainCaptureView;
- (IBAction)switchCamera:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *photoImage;
@property(nonatomic , strong)UIImage* logoImage;
@property (strong, nonatomic) AVCaptureSession *session;
@property (strong) AVCaptureDevice *videoDevice;
@property (strong) AVCaptureDeviceInput *videoInput;
//@property (strong) AVCaptureStillImageOutput *stillImageOutput;
@property BOOL isUsingFrontFacingCamera;
@property (weak, nonatomic) IBOutlet UIButton *captureButton;
- (AVCaptureDevice *) backCamera;
- (AVCaptureDevice *) frontCamera;
@property(nonatomic , strong) StudioPhoto* studioPhoto;
@property (strong, nonatomic) IBOutlet UILabel *pageTitle;
@property (strong, nonatomic) IBOutlet UIButton *flash;

@end
