//
//  TransparentView.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/30/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransparentView : UIView
@property(assign) UIImageView* imageView;
@property(nonatomic ,assign) int captureType;
@end
