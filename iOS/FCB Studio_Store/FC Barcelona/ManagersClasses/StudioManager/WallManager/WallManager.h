//
//  UserPhotoManager.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserPhoto.h"
#import "UserComment.h"
#import "UserPhotoLikes.h"
#import "Post.h"
#import "BusinessManager.h"
@interface WallManager : BusinessManager

+(WallManager*)getInstance;
-(void)addComment:(UserComment*)comment and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getComments:(NSInteger)photoid pageNo:(NSInteger)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)removeComment:(NSInteger)commentId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)likePost:(NSInteger)userId postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)dislikePost:(NSInteger)userId postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)getWall:(NSInteger)userid postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter countryList:(NSArray*)countryList favoritePostsFlag:(BOOL)favoritePostsFlag and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getLatest:(NSInteger)userid postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter countryList:(NSArray*)countryList and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getMyPosts:(NSInteger)userid postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getPost:(NSInteger)postId userId:(NSInteger)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getTopTen:(NSInteger)userid and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)shareToWall:(UserPhoto*)userPhoto and:(NSMutableArray*)tags and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)deletePost:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)rePost:(NSInteger)postId userId:(NSInteger)userId date:(NSString*)date and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getUserPosts:(NSInteger)userid fanId:(NSInteger)fanId postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getLikeDetails:(NSInteger)userid postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)reportPost:(NSInteger)postId userId:(NSInteger)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)getChatFriends:(NSString*)filter userId:(NSInteger)userId limit:(NSInteger)limit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getLoadMessagesUserID:(int)userid and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)sendMessagesUserID:(int)userid andFriendID:(int)fanid andMessage:(NSString *)message and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)deleteMessagesID:(NSString*)msgid and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getTagPosts:(NSInteger *)userid tagId:(NSString *)tagid postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getPostTag:(NSInteger *)studioPhotoId postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)deleteComment:(NSInteger)commentId postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)editPost:(Post*)post and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getChatContacts:(NSInteger)userId Contacts:(NSMutableArray *)contactArray Language:(NSString *)lang and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getNotificationPosts:(NSInteger *)userid pageNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)logOutUser:(int)userID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
@end
