//
//  ShareManagerViewController.m
//  FC Barcelona
//
//  Created by Eissa on 9/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "ShareManagerViewController.h"
#import "ShareLibrary.h"

@interface ShareManagerViewController ()<UIDocumentInteractionControllerDelegate>
@property(nonatomic, strong) UIDocumentInteractionController* docController;

@end

@implementation ShareManagerViewController
@synthesize sharedImage;
@synthesize shareTitle;
@synthesize closeButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bg.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
  
}

- (IBAction)shareFacebook:(id)sender {
    [ShareLibrary initFrom:self shareContentToMedia:shareFacebook withText:@"" andImage:sharedImage];
}

- (IBAction)shareTwitter:(id)sender {
    [ShareLibrary initFrom:self shareContentToMedia:shareTwitter withText:@"" andImage:sharedImage];
}

- (IBAction)shareInstgram:(id)sender {
    NSString* imagePath = [NSString stringWithFormat:@"%@/image.igo", [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
    [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
    [UIImagePNGRepresentation(sharedImage) writeToFile:imagePath atomically:YES];
    _docController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:imagePath]];
    _docController.delegate=self;
    _docController.UTI = @"com.instagram.exclusivegram";
    [_docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
