//
//  WallTableViewCell.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/3/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "WallTableViewCell.h"

@implementation WallTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)sharePressed:(id)sender {
}

- (IBAction)commentPressed:(id)sender {
}

- (IBAction)likePressed:(id)sender {
}
@end
