//
//  WallViewController.m
//  FC Barcelona
//
//  Created by Essam on 9/9/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "WallViewController.h"
#import "UserComment.h"
#import "WallManager.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "ShareManagerViewController.h"
#import "CommentsViewController.h"
#import "Post.h"
#import "WebImage.h"
#import "UserPhoto.h"
#import "PostDetailsViewController.h"
#import "CurrentUser.h"

@interface WallViewController ()

@end

@implementation WallViewController
{
    BOOL isWall;
    id resultsDictionary;
    ShareManagerViewController * shareView;
    CommentsViewController * commentsView;

}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isWall = YES;

    
}

-(void)viewDidAppear:(BOOL)animated{
    //[self getPhotos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
//- (IBAction)segmentPressed:(id)sender {
//    if ([sender selectedSegmentIndex] == 0) {
//        isWall = YES;
//        resultsDictionary = nil ;
//        [_tableView reloadData];
//        [self getPhotos];
//    }else if ([sender selectedSegmentIndex] == 1) {
//        isWall = NO;
//        resultsDictionary = nil ;
//        [_tableView reloadData];
//        [self getPhotos];
//    }
//}
//
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return 1;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return [resultsDictionary count];
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    static NSString *CellIdentifier = @"myCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if(cell == nil)
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    
//    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:6] ;
//    UIImageView  * image    = (UIImageView* )[cell viewWithTag:5] ;
//    UILabel  * name    = (UILabel* )[cell viewWithTag:1] ;
//    UIButton * share   = (UIButton*)[cell viewWithTag:2] ;
//    UIButton * comment = (UIButton*)[cell viewWithTag:3] ;
//    UIButton * like    = (UIButton*)[cell viewWithTag:4] ;
//
//    CALayer * l = [image layer];
//    [l setMasksToBounds:YES];
//    [l setCornerRadius:7.0];
//    
//    name.text = [(Post*)[resultsDictionary objectAtIndex:indexPath.row] userName] ;
//
//    [WebImage processImageDataWithURLString:[(Post*)[resultsDictionary objectAtIndex:indexPath.row] postPhotoUrl] cacheImage:YES andBlock:^(NSData *imageData) {
//        image.image = [UIImage imageWithData:imageData];
//        [loader stopAnimating];
//        [loader setHidden:YES];
//    }];
//    
//
//    
//    
//    share.tag = indexPath.row;
//    comment.tag = indexPath.row;
//    like.tag = indexPath.row;
//    image.tag = indexPath.row;
//
//    [share   addTarget:self action:@selector(share:)   forControlEvents:UIControlEventTouchUpInside];
//    [comment addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
//    [like    addTarget:self action:@selector(like:)    forControlEvents:UIControlEventTouchUpInside];
//    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openImage:)];
//    tap.cancelsTouchesInView = YES;
//    tap.numberOfTapsRequired = 1;
//
//    [image addGestureRecognizer:tap];
//    image.userInteractionEnabled = YES;
//    
//    return cell;
//
//}
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
////    if ([tableView.restorationIdentifier isEqualToString:@"wallTableView"]) {
////        
////    }else if ([tableView.restorationIdentifier isEqualToString:@"latestTableView"]){
////        
////    }
////    
//}
//-(IBAction)refreshPressed:(id)sender
//{
//    resultsDictionary = nil;
//    [self getPhotos];
//}
//-(IBAction)backPressed:(id)sender
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}
//-(void)getPhotos
//{
//    NSInteger pageNumber = resultsDictionary==nil ? 1 : [resultsDictionary count] / 20;
//    
//    if (resultsDictionary) {
//        if (isWall) {
//            [self getWall:[[CurrentUser getObject] getUser].userId pageNo:pageNumber+1];
//        }
//        else{
//            [self getLatest:pageNumber+1];
//        }
//    }
//    else
//    {
//        if (isWall) {
//            [self getWall:[[CurrentUser getObject] getUser].userId pageNo:1];
//        }
//        else{
//            [self getLatest:1];
//        }
//    }
//    
//}
//-(void)getWall:(NSInteger)userid pageNo:(NSInteger)pageNo
//{
//    
//    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//    @try {
//        if ([RestServiceAgent internetAvailable]) {
//            [[WallManager getInstance]getWall:userid pageNo:pageNo and:^(id currentClient) {
//               
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    if (currentClient) {
//                        resultsDictionary = currentClient;
//                        [_tableView reloadData];
//                        
//                    }
//                });
//            }onFailure:^(NSError *error) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    
//                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                    [alert show];
//                });
//            }];
//        }
//        else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            });
//
//        }
//        
//    }
//    @catch (NSException *exception) {
//        [SVProgressHUD dismiss];
//        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//        [alert show];
//    }
//}
//-(void)getLatest:(NSInteger)postsNo
//{
//    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//    @try {
//        if ([RestServiceAgent internetAvailable]) {
//            [[WallManager getInstance] getLatest:[[CurrentUser getObject] getUser].userId postsNo:postsNo lastRequestTime: and:^(id currentClient) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    if (currentClient) {
//                        resultsDictionary = currentClient;
//                        [_tableView reloadData];
//                        
//                    }
//                });
//
//            }onFailure:^(NSError *error) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    
//                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                    [alert show];
//                });
//            }];
//        }
//        else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            });
//
//        }
//        
//    }
//    @catch (NSException *exception) {
//        [SVProgressHUD dismiss];
//        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//        [alert show];
//    }
//}
//-(void)share:(id)sender
//{
//    
//    [WebImage processImageDataWithURLString:[(Post*)[resultsDictionary objectAtIndex:[sender tag]] postPhotoUrl] cacheImage:YES andBlock:^(NSData *imageData) {
//        shareView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareManagerViewController"];
//        shareView.shareTitle=@"";
//        shareView.sharedImage = [UIImage imageWithData:imageData];
//        [self addChildViewController:shareView];
//        shareView.view.frame = self.view.bounds;
//        [self.view addSubview:shareView.view];
//        [shareView didMoveToParentViewController:self];
//        
//        [shareView.closeButton addTarget:self action:@selector(closeShare) forControlEvents:UIControlEventTouchUpInside];
//    }];
//    
//    
//}
//-(void)closeShare
//{
//    
//    [shareView willMoveToParentViewController:nil];
//    [shareView.view removeFromSuperview];
//    [shareView removeFromParentViewController];
//}
//-(void)comment:(id)sender
//{
//    
////    commentsView = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
////    commentsView.photo = (UserPhoto*)[(Post*)[resultsDictionary objectAtIndex:[sender tag]] photo] ;
////    commentsView.userID  = currentUser.userId;
////    commentsView.userName = [(Post*)[resultsDictionary objectAtIndex:[sender tag]] userName];
////    [self addChildViewController:commentsView];
////    commentsView.view.frame = self.view.bounds;
//    
////    [self.view addSubview:commentsView.view];
////    [commentsView didMoveToParentViewController:self];
////    
////    [commentsView.close addTarget:self action:@selector(closeComment) forControlEvents:UIControlEventTouchUpInside];
////
//}
//-(void)closeComment
//{
//    [commentsView willMoveToParentViewController:nil];
//    [commentsView.view removeFromSuperview];
//    [commentsView removeFromParentViewController];
//}
//
//-(void)like:(id)sender
//{
//    
//    int postId = [(Post*)[resultsDictionary objectAtIndex:[sender tag]] postId];
//    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//    @try {
//        if ([RestServiceAgent internetAvailable]) {
//            [[WallManager getInstance] likePost:[[CurrentUser getObject] getUser].userId photoid:postId and:^(id currentClient) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    if([[currentClient objectForKey:@"status"] intValue] == 1)
//                    {
//                        
//                    }
//                });
//                
//            }onFailure:^(NSError *error) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                    [alert show];
//                });
//            }];
//        }
//        else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            });
//
//        }
//    }
//    @catch (NSException *exception) {
//        [SVProgressHUD dismiss];
//        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//        [alert show];
//    }
//}
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    NSLog(@"%f ----- %f",([scrollView contentOffset].y + scrollView.frame.size.height) , [scrollView contentSize].height);
//    
//    if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height) {
//        NSLog(@"scrolled to bottom");
//        [self getPhotos];
//        return;
//    }
//}
//-(void)openImage:(id)sender
//{
//    PostDetailsViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
//    view.userPost = (Post*)[resultsDictionary objectAtIndex:[[sender view] tag]];
//    [self.navigationController pushViewController:view animated:YES];
//}
@end
