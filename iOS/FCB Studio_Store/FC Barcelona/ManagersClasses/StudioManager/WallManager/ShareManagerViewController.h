//
//  ShareManagerViewController.h
//  FC Barcelona
//
//  Created by Eissa on 9/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareManagerViewController : UIViewController
@property(strong,nonatomic) UIImage * sharedImage;
@property(strong,nonatomic) NSString * shareTitle;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIView *contentView;

- (IBAction)shareFacebook:(id)sender;
- (IBAction)shareTwitter:(id)sender;
- (IBAction)shareInstgram:(id)sender;
- (IBAction)back_Action:(id)sender;

@end
