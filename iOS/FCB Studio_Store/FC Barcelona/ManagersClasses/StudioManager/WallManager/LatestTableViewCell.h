//
//  LatestTableViewCell.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/3/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LatestTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@end
