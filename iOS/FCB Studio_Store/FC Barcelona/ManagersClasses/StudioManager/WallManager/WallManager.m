//
//  UserPhotoManager.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//


#import "WallManager.h"
#import "Post.h"
#import "Season.h"
#import "DataHelper.h"
#import "RestServiceAgent.h"
#import <KeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "UserPhoto.h"
#import "PostLike.h"
#import "Fan.h"
@implementation WallManager
static WallManager* instance;

+(WallManager*)getInstance{
    if (!instance) {
        instance=[[WallManager alloc] init];
    }
    return instance;
}


//////Essam////////////////////////////
-(void)addComment:(UserComment*)comment and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{

    [self saveObjectWithAction:@"wall/addComments" andObject:comment and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.domain);
        failure(error);
    }];
}

-(void)getComments:(NSInteger)photoid pageNo:(NSInteger)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:@(photoid),@(pageNo), nil];
    NSArray* keys = [NSArray arrayWithObjects:@"postId",@"pageNo", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"wall/getComments" andParams:dic andIam:[[UserComment alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.domain);
        failure(error);
    }];
}

-(void)deleteComment:(NSInteger)commentId postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:@(commentId),@(postId), nil];
    NSArray* keys = [NSArray arrayWithObjects:@"commentId",@"postId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"wall/deleteComment" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.domain);
        failure(error);
    }];

}
-(void)likePost:(NSInteger)userId postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userId],[@"" stringByAppendingFormat:@"%ld",(long)postId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"postId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"wall/likePost" andParams:dic andIam:[[UserPhotoLikes alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.domain);
        failure(error);
    }];
}
-(void)dislikePost:(NSInteger)userId postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userId],[@"" stringByAppendingFormat:@"%ld",(long)postId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"postId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"wall/dislikePost" andParams:dic andIam:[[UserPhotoLikes alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.domain);
        failure(error);
    }];
}


-(void)getWall:(NSInteger)userid postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter countryList:(NSArray*)countryList favoritePostsFlag:(BOOL)favoritePostsFlag and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],
                     [@"" stringByAppendingFormat:@"%ld",(long)postsNo],
                     lastRequestTime,
                     timeFilter,
                     countryList,
                     [@"" stringByAppendingFormat:@"%i",favoritePostsFlag],nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"userId",
                     @"n",
                     @"lastRequestTime",
                     @"timeFilter",
                     @"countryList",
                     @"favoritePostsFlag",nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"wall/getWall" andParams:dic andIam:[[Post alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getLatest:(NSInteger)userid postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter countryList:(NSArray*)countryList and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],
                                              [@"" stringByAppendingFormat:@"%ld",(long)postsNo],
                                              lastRequestTime,
                                              timeFilter,
                                              countryList,nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"userId",
                                             @"n",
                                             @"lastRequestTime",
                                             @"timeFilter",
                                             @"countryList",nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    
    NSLog(@"getLatest:%@",dic);
    [self getObjectListWithAction:@"wall/getLatest" andParams:dic andIam:[[Post alloc] init] and:^(id currentClient)
     {
         handler(currentClient);
     }onFailure:^(NSError *error) {
         failure(error);
     }];
}
//1433582968
-(void)getMyPosts:(NSInteger)userid postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],[@"" stringByAppendingFormat:@"%ld",(long)postsNo],lastRequestTime, timeFilter,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"n",@"lastRequestTime",@"timeFilter", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"wall/getMyPosts" andParams:dic andIam:[[Post alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)getUserPosts:(NSInteger)userid fanId:(NSInteger)fanId postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],[@"" stringByAppendingFormat:@"%ld",(long)fanId],[@"" stringByAppendingFormat:@"%ld",(long)postsNo],lastRequestTime, timeFilter,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId",@"n",@"lastRequestTime",@"timeFilter", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"wall/getUserPosts" andParams:dic andIam:[[Post alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getTopTen:(NSInteger)userid and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"wall/getTopTen" andParams:dic andIam:[[Season alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(NSString*)jsonStringFromDic:(NSDictionary*)info
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:info
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                       withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\"
                                                       withString:@""];
    return jsonString;
}
-(NSString*)jsonStringFromArray:(NSArray*)info
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:info
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                       withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\"
                                                       withString:@""];
    return jsonString;
}

-(void)deletePost:(NSInteger)postId and:(void (^)(id))handler onFailure:(void (^)(NSError *))failure{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)postId],nil];
    NSArray* keys = [NSArray arrayWithObjects:@"postId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"wall/deletePost" andParams:dic andIam:[[UserPhotoLikes alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.domain);
        failure(error);
    }];
    
}
-(void)rePost:(NSInteger)postId userId:(NSInteger)userId date:(NSString*)date and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure{
    
    NSArray* vars = [NSArray arrayWithObjects:
                     [@"" stringByAppendingFormat:@"%ld",(long)postId]
                     ,[@"" stringByAppendingFormat:@"%ld",(long)userId]
                     ,date
                     ,nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"postId",@"userId",@"date", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"wall/rePost" andParams:dic andIam:[[UserPhotoLikes alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.domain);
        failure(error);
    }];
    
    
}

-(void)getLikeDetails:(NSInteger)userid postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure{
    
    NSArray* vars = [NSArray arrayWithObjects:
                     [@"" stringByAppendingFormat:@"%ld",(long)userid],
                     [@"" stringByAppendingFormat:@"%ld",(long)postId],nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"postId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"wall/getLikeDetails" andParams:dic andIam:[[PostLike alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)reportPost:(NSInteger)postId userId:(NSInteger)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [@"" stringByAppendingFormat:@"%ld",(long)postId]
                     ,[@"" stringByAppendingFormat:@"%ld",(long)userId]
                     ,nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"postId",@"userId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"wall/reportPost" andParams:dic andIam:[[UserPhotoLikes alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.description);
        failure(error);
    }];
}

-(void)getChatFriends:(NSString*)filter userId:(NSInteger)userId limit:(NSInteger)limit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [@"" stringByAppendingFormat:@"%ld",(long)userId]
                     ,filter, [@"" stringByAppendingFormat:@"%ld",(long)limit]
                     ,nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"userID",@"filter",@"startingLimit", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"chat/loadFriends" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.description);
        failure(error);
    }];
}
-(void)getChatContacts:(NSInteger)userId Contacts:(NSMutableArray *)contactArray Language:(NSString *)lang and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userId], contactArray
                     ,lang,nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"contacts",@"lang", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    NSLog(@"Parameters %@",dic);
    [self submitWithAction:@"chat/loadChatContacts" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.description);
        failure(error);
    }];
}

-(void)getLoadMessagesUserID:(int)userid and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSString* date;
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    date = ![defaults objectForKey:@"lastTimeStampChecked"] ? @"0" : [defaults objectForKey:@"lastTimeStampChecked"];
    
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],
                     !date? @"":@([date longLongValue]),nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"myID",@"lastTimeStampChecked", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"chat/loadMessage" andParams:dic andIam:nil and:^(id currentClient) {
        [defaults setObject:@([[NSDate date] timeIntervalSince1970]) forKey:@"lastTimeStampChecked"];
        [defaults synchronize];
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.description);
        failure(error);
    }];
}

-(void)sendMessagesUserID:(int)userid andFriendID:(int)fanid andMessage:(NSString *)message and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:@"en",@(userid),@(fanid), message,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"lang",@"myID",@"fanID",@"messageText", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    NSLog(@"%@",dic);
    [self submitWithAction:@"chat/sendMessage" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.description);
        failure(error);
    }];
}

-(void)deleteMessagesID:(NSString*)msgid and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:msgid,nil];
    
    NSArray* keys = [NSArray arrayWithObjects:@"messageID",nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"chat/deleteMessage" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.description);
        failure(error);
    }];
}

-(NSString *)convertDateToString:(NSDate *)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd"];
    return [dateFormat stringFromDate:date];
}

-(void)getTagPosts:(NSInteger *)userid tagId:(NSString *)tagid postsNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime timeFilter:(NSString*)timeFilter and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],tagid,[@"" stringByAppendingFormat:@"%ld",(long)postsNo],lastRequestTime, timeFilter,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"tagName",@"n",@"lastRequestTime",@"timeFilter", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    
    NSLog(@"Parameters %@",dic);
    [self getObjectListWithAction:@"wall/getTagPosts" andParams:dic andIam:[[Post alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getPostTag:(NSInteger *)studioPhotoId postId:(NSInteger)postId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)studioPhotoId],[@"" stringByAppendingFormat:@"%ld",(long)postId],nil];
    NSArray* keys = [NSArray arrayWithObjects:@"studioPhotoId",@"postId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    
    NSLog(@"Parameters %@",dic);
    [self getObjectListWithAction:@"studio/gettags" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getPost:(NSInteger)postId userId:(NSInteger)userId and:(void (^)(id))handler onFailure:(void (^)(NSError *))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)postId],[@"" stringByAppendingFormat:@"%ld",(long)userId],nil];
    NSArray* keys = [NSArray arrayWithObjects:@"postId",@"userId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    
    NSLog(@"Parameters %@",dic);
    [self getObjectListWithAction:@"wall/getPost" andParams:dic andIam:[[Post alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)editPost:(Post*)post and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:post forKey:@"post"];
    
    [self submitWithAction:@"wall/editPost" andParams:dic andIam:[[Post alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}


-(void)getNotificationPosts:(NSInteger *)userid pageNo:(NSInteger)postsNo lastRequestTime:(NSString*)lastRequestTime and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],lastRequestTime,[@"" stringByAppendingFormat:@"%ld",(long)postsNo],nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"lastRequestTime",@"pageNo", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    
    NSLog(@"Parameters %@",dic);
    [self getObjectListWithAction:@"notification/getNotifications" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}



-(void)logOutUser:(int)userID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:@(userID),nil];

    NSArray* keys = [NSArray arrayWithObjects:@"userID", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"user/logout" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.description);
        failure(error);
    }];
}

@end
