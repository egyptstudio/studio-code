//
//  UserPhotoManager.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessManager.h"
#import "UserPhoto.h"
@interface UserPhotoManager : BusinessManager

@end
