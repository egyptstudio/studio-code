//
//  TransparentView.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/30/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "TransparentView.h"

@implementation TransparentView
@synthesize imageView;
-(void)drawRect:(CGRect)rect{
    if (_captureType == 1) {
//        [[UIColor blackColor] setFill];
        UIRectFill(rect);
        CGRect holeRectIntersection = CGRectIntersection(CGRectMake(imageView.frame.origin.x , imageView.frame.origin.y, imageView.frame.size.width, imageView.frame.size.height), rect );
        [[UIColor clearColor] setFill];
        UIRectFill( holeRectIntersection );
        
    }else{
        [super drawRect:rect];
    }
}
@end
