//
//  StudioManager.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "StudioManager.h"
#import "StudioPhoto.h"
#import "StudioFolder.h"
#import "DataHelper.h"
#import "GetAndPushCashedData.h"
@implementation StudioManager{
    NSMutableArray* currentPageArray;
    NSMutableArray* previousPageArray;
    NSMutableArray* nextPageArray;
    BOOL firstTimeFlag;
    
}
static StudioManager* instance;
static int dummyCatIndex;
+(StudioManager *)getInstance{
    if (!instance) {
        instance=[[StudioManager alloc] init];
        dummyCatIndex=1;
    }
    return instance;
}


-(void)getStudioPhotos:(int)categoryId :(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    if (!firstTimeFlag || (dummyCatIndex != categoryId)) { //first time call
        dummyCatIndex=categoryId;
        firstTimeFlag=YES;
        NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",categoryId],[NSString stringWithFormat:@"%d",pageNo], nil];
        NSArray* keys = [NSArray arrayWithObjects:@"categoryId",@"pageNo", nil];
        NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
        [self getObjectListWithAction:@"getStudioPhotos" andParams:dic andIam:[[StudioPhoto alloc] init] and:^(id currentclient)
         {
             currentPageArray=currentclient;
             handler(currentPageArray);
             NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",categoryId],[NSString stringWithFormat:@"%d",(pageNo+1)], nil];
              NSArray* keys = [NSArray arrayWithObjects:@"categoryId",@"pageNo", nil];
              NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
              [self getObjectListWithAction:@"getStudioPhotos" andParams:dic andIam:[[StudioPhoto alloc] init] and:^(id currentclient){
              nextPageArray=currentclient;
              }onFailure:^(NSError *error) {
                  failure(error);
              }];
         }onFailure:^(NSError *error) {
             failure(error);
         }];
    }else {
        previousPageArray=currentPageArray;
        currentPageArray=nextPageArray;
        NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",categoryId],[NSString stringWithFormat:@"%d",pageNo], nil];
        NSArray* keys = [NSArray arrayWithObjects:@"categoryId",@"pageNo", nil];
        NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
        [self getObjectListWithAction:@"getStudioPhotos" andParams:dic andIam:[[StudioPhoto alloc] init] and:^(id currentclient){
            nextPageArray=currentclient;
        }onFailure:^(NSError *error) {
            failure(error);
        }];
        handler(currentPageArray);
    }
}

-(void)getStudioPhoto:(int)studioPhotoId andUerId:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)studioPhotoId],@(userId), nil];
    NSArray* keys = [NSArray arrayWithObjects:@"studioPhotoId",@"userId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"studio/getStudioPhoto" andParams:dic andIam:nil and:^(id currentClient)
    {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)getStudioFolders:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
{
    [self getObjectListWithAction:@"studio/getStudioFolders" andParams:[[NSDictionary alloc] init] andIam:[[StudioFolder alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];

}
-(void)getStudioFolderPhotos:(int)userId folderId:(int)folderId pageNo:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userId],
                                              [@"" stringByAppendingFormat:@"%ld",(long)folderId],
                                              [@"" stringByAppendingFormat:@"%ld",(long)pageNo]  ,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"folderId",@"pageNo", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"studio/getStudioFolderPhotos" andParams:dic andIam:[[StudioPhoto alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getStudioFolderPhotos:(int)userId folderId:(int)folderId pageNo:(int)pageNo filterBy:(int)filterBy seasons:(NSArray*)seasons and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:@(userId),@(folderId),@(pageNo),@(filterBy),seasons,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"folderId",@"pageNo",@"filterBy",@"seasons", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"studio/getStudioFolderPhotos" andParams:dic andIam:[[StudioPhoto alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getSeasons:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:@"" ,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];

    [self getObjectListWithAction:@"studio/getSeasons" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)getUserPurchasedPhotos:(int)userId pageNo:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userId],
                     [@"" stringByAppendingFormat:@"%ld",(long)pageNo]  ,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"pageNo", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"studio/getUserPurchasedPhotos" andParams:dic andIam:[[StudioPhoto alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)purchaseStudioPhoto:(int)userId studioID:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userId],
                     [@"" stringByAppendingFormat:@"%ld",(long)pageNo]  ,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"studioPhotoId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"studio/purchaseStudioPhoto" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(NSString*)jsonStringFromArray:(NSArray*)info
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:info
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                       withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\"
                                                       withString:@""];
    return jsonString;
}

-(void)getFCBPhotos:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:@"" ,nil];
    NSArray* keys = [NSArray arrayWithObjects:@"", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"user/getFCBPhotos" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)addPost:(Post*)post witImage:(UIImage*)postImage and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:post forKey:@"post"];
    [dic setObject:postImage forKey:@"profilePic"];

    [self submitWithAction:@"studio/addPost" andParams:dic andIam:[[Post alloc] init] and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)updateUserCredit:(int)userId creditValue:(int)creditValue transactionId:(NSString* )transactionId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:@(userId) forKey:@"userId"];
    [dic setObject:@(creditValue) forKey:@"creditValue"];
    [dic setObject:transactionId forKey:@"TransID"];
    [dic setObject:@(2) forKey:@"deviceType"];
    [dic setObject:[GetAndPushCashedData getObjectFromCash:@"deviceToken"]?[GetAndPushCashedData getObjectFromCash:@"deviceToken"]:@"" forKey:@"deviceToken"];

    

    [self submitWithAction:@"studio/updateUserCredit" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getUserCredit:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:@(userId) forKey:@"userId"];
    
    [self submitWithAction:@"studio/getUserCredit" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)shareSocailMedia:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:
                     [[Language sharedInstance] langAbbreviation],
                     @(userId), nil];
    NSArray* keys = [NSArray arrayWithObjects:
                     @"lang",
                     @"userId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"studio/shareSocailMedia" andParams:dic andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

-(void)getLibVer:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    [self submitWithAction:@"studio/getLibVer" andParams:[[NSDictionary alloc] init] andIam:nil and:^(id currentClient)
     {
         NSLog(@"%@",currentClient);
         handler(currentClient);
     } onFailure:^(NSError *error) {
         NSLog(@"%@",error.domain);
         failure(error);
     }];
}

@end
