//
//  StudioManager.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessManager.h"
#import "Post.h"
@interface StudioManager : BusinessManager
typedef enum {
	latestPhotos=1,
    mostUsedPhotos,
} FolderSortEnum;

+(StudioManager*)getInstance;


//-(NSMutableArray*)getStudioPhotos:(int)categoryId :(int)pageNo;
-(void)getStudioPhotos:(int)categoryId :(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getStudioPhoto:(int)studioPhotoId andUerId:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getStudioFolders:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getStudioFolderPhotos:(int)userId folderId:(int)folderId pageNo:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getStudioFolderPhotos:(int)userId folderId:(int)folderId pageNo:(int)pageNo filterBy:(int)filterBy seasons:(NSArray*)seasons and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getUserPurchasedPhotos:(int)userId pageNo:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)purchaseStudioPhoto:(int)userId studioID:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;


-(void)getFCBPhotos:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getSeasons:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;


-(void)addPost:(Post*)post witImage:(UIImage*)postImage and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)updateUserCredit:(int)userId creditValue:(int)creditValue transactionId:(NSString* )transactionId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getUserCredit:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)shareSocailMedia:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)getLibVer:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

@end
