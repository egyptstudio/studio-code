//
//  MMGDrag&Drop.m
//  MOVEME
//
//  Created by Mohamed Mitwaly on 7/20/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "MMGDrag_Drop.h"

@implementation MMGDrag_Drop{
    UIView* mainOperatingView;
    CGRect zoomedMainRect;
    BOOL zoomOnly;
    UIView* transparentView;
}
static MMGDrag_Drop* instance;
+(MMGDrag_Drop *)getInstance{
    if (!instance) {
        instance=[[MMGDrag_Drop alloc]init];  //returning object from class
    }
    return instance;
}

-(int)arrayOfViewsToBeZoom:(NSArray *)viewsArray onView:(UIView *)mainView{
    zoomOnly=YES;
    if (mainView) {
        mainOperatingView=mainView;
        if (viewsArray) {
           // for (int i =0; i<viewsArray.count; i++) {
                UIPinchGestureRecognizer* pinchGes = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
                [pinchGes setDelegate:self];
                [viewsArray[0] addGestureRecognizer:pinchGes];
            transparentView=viewsArray[1];
            //}
            return 1;
        }else{
            return 2;
        }
    }else{
        return 0;
    }
}

-(int)arrayOfViewsToBeDragged_DropAndZoom:(NSArray*)viewsArray onView:(UIView*)mainView{
    if (mainView) {
        mainOperatingView=mainView;
        if (viewsArray) {
            for (int i =0; i<viewsArray.count; i++) {
                UIPanGestureRecognizer* panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
                [panGes setDelegate:self];
                [viewsArray[i] addGestureRecognizer:panGes];
                UIPinchGestureRecognizer* pinchGes = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
                [pinchGes setDelegate:self];
                [viewsArray[i] addGestureRecognizer:pinchGes];
                UIRotationGestureRecognizer* rotateGes = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
                [rotateGes setDelegate:self];
                [viewsArray[i] addGestureRecognizer:rotateGes];
            }
            return 1;
        }else{
            return 2;
        }
    }else{
        return 0;
    }
}
-(int)initWithViews:(NSArray*)viewsArray onView:(UIView *)mainView canDrag:(BOOL)drag canZoom:(BOOL)zoom canRotate:(BOOL)rotate{

    if (mainView) {
        mainOperatingView=mainView;
        if (viewsArray) {
            for (int i =0; i<viewsArray.count; i++) {
                
                UIPanGestureRecognizer* panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
                [panGes setDelegate:self];
                UIPinchGestureRecognizer* pinchGes = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
                [pinchGes setDelegate:self];
                UIRotationGestureRecognizer* rotateGes = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
                [rotateGes setDelegate:self];
                
                if (drag)
                    [viewsArray[i] addGestureRecognizer:panGes];
                if (zoom)
                    [viewsArray[i] addGestureRecognizer:pinchGes];
                if (rotate)
                    [viewsArray[i] addGestureRecognizer:rotateGes];
            }
            return 1;
        }else{
            return 2;
        }
    }else{
        return 0;
    }


}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:mainOperatingView];
    if ((recognizer.view.center.x + translation.x) > 0
        &&(recognizer.view.center.y + translation.y) > 0
        &&(recognizer.view.center.x + translation.x) <= [UIScreen mainScreen].bounds.size.width
        &&(recognizer.view.center.y + translation.y) <= [UIScreen mainScreen].bounds.size.height) {
        
            recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                             recognizer.view.center.y + translation.y);
        [recognizer setTranslation:CGPointMake(0, 0) inView:mainOperatingView];
    }
}

- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    if (zoomOnly) {
        [transparentView setNeedsDisplay];
    }
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
}

- (void)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    
    recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
    recognizer.rotation = 0;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


@end
