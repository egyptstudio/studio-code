//
//  ShareViewController.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/1/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WallManager.h"
#import "StudioPhoto.h"
#import "TwitterLogin.h"
@protocol EditViewDelegate <NSObject>
@optional
- (void)didViewEditedWith:(Post*)object;
@end
@interface ShareViewController : UIViewController<UITextFieldDelegate,TwitterLoginDelegate>
@property (nonatomic,assign) id <EditViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageView2;
@property (nonatomic , strong) UIImage* editedImage;
@property (nonatomic , strong) UIImage* originalImage;
@property (nonatomic , strong) StudioPhoto* studioPhoto;
@property (nonatomic , assign) BOOL isEditImage;

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIView *captionView;
@property (weak, nonatomic) IBOutlet UIView *tagsView;
- (IBAction)addTag:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *tagsScroll;


@property (weak, nonatomic) IBOutlet UIButton *shareTwitterButton;
@property (weak, nonatomic) IBOutlet UIButton *shareFacebookButton;

@property (weak, nonatomic) IBOutlet UIView *contestView;
@property (weak, nonatomic) IBOutlet UIImageView *contestImage;
@property (weak, nonatomic) IBOutlet UILabel *contetsLabel;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Point;
@property (nonatomic, strong) Post *currentPost;

@property (weak, nonatomic) IBOutlet UILabel *lbl_onlyMe;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Friends;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Public;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ShareOnFaceTwitter;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UITextField *tagText;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;

- (IBAction)post_Action:(id)sender;
- (IBAction)cancel_Action:(id)sender;

@end
