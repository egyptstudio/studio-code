//  CaptureViewController.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 8/31/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <GoogleMobileAds/GADInterstitial.h>
#import "CaptureViewController.h"
#import "ShareViewController.h"
#import "SVProgressHUD.h"
#import <objc/message.h>
#import "TransparentView.h"
#import "WebImage.h"
#import "ShareViewController.h"
#import "RestServiceAgent.h"
#import "StudioManager.h"
#import "UIImage+Brightness.h"
#import "UIImage+Contrast.h"

@interface CaptureViewController ()<AFPhotoEditorControllerDelegate>{
    AVCaptureStillImageOutput *stillImageOutput;
    __weak IBOutlet UIImageView *finalImage;
    UIImage* combinedImage;
    UIImage* editedImage;
    UIImage* originalImage;

    __weak IBOutlet TransparentView *containerView;
    float newWidth;
    float newHeight;
    float oldWidth;
    float oldHeight;
    float oldX;
    float oldY;
    UIImage* aviaryImage;
    UIImage* tempImage;
    __weak IBOutlet UIImageView *waterMark;
    float waterMarkRatio;
    int flashState;
    UISlider* brightnessSlider;
    UISlider* contrastSlider;
    UIImage *imageToBeModefied;
    BOOL camInitBefore;
    BOOL firstTimeGranting;
}

@end

@implementation CaptureViewController
static NSString * const kAviaryAPIKey = @"d51f73582387470c";
static NSString * const kAviarySecret = @"28f46520495fe7bf";
// Run session setup after subviews are laid out.


-(void)viewDidLoad{
    [super viewDidLoad];
    
    
    NSLog(@"[_studioPhoto captureType] %d",[_studioPhoto captureType]);
    if ([_studioPhoto captureType]==1) {
        [[MMGDrag_Drop getInstance] initWithViews:[NSArray arrayWithObjects:_photoImage, nil]
                                           onView:self.mainCaptureView
                                          canDrag:YES
                                          canZoom:YES
                                        canRotate:YES];
    } else if ([_studioPhoto captureType] ==2){
        [[MMGDrag_Drop getInstance] initWithViews:[NSArray arrayWithObjects:_photoImage, nil]
                                           onView:self.mainCaptureView
                                          canDrag:YES
                                          canZoom:YES
                                        canRotate:NO];
    }
    if ([_studioPhoto captureType]==2) {
        [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification  object:nil];
    }
    brightnessSlider = [[UISlider alloc] initWithFrame:CGRectMake(0,
                                                                  80,
                                                                  [UIScreen mainScreen].bounds.size.width,
                                                                  50)];
    brightnessSlider.minimumValue = -0.5;
    brightnessSlider.maximumValue =  0.5;
    [brightnessSlider addTarget:self action:@selector(setBrightness:) forControlEvents:UIControlEventValueChanged];
    
    brightnessSlider.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleWidth ;

    [self.view addSubview:brightnessSlider];
    
    
    contrastSlider = [[UISlider alloc] initWithFrame:CGRectMake(0,
                                                                  130,
                                                                  [UIScreen mainScreen].bounds.size.width,
                                                                  50)];
    contrastSlider.minimumValue = -1;
    contrastSlider.maximumValue =  1;
    [contrastSlider addTarget:self action:@selector(setContrast:) forControlEvents:UIControlEventValueChanged];
    //[self.view addSubview:contrastSlider];
    [self setLocalisables];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reinitCamFeed) name:@"applicationDidBecomeActive" object:nil];

}
-(void)reinitCamFeed{
    if ([self.navigationController.topViewController isKindOfClass:[self class]]) {
        [self intCamFeed];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self prefersStatusBarHidden];
    if ([_studioPhoto photoOrientation]==1) {
        objc_msgSend([UIDevice currentDevice], @selector(setOrientation:),UIInterfaceOrientationPortrait ); //cportrait
        
    } else {
        objc_msgSend([UIDevice currentDevice], @selector(setOrientation:),UIInterfaceOrientationLandscapeLeft ); //landscape
        
    }
    finalImage.alpha=0;
    _photoImage.alpha=1;
    
    
    if (!camInitBefore) {
        [WebImage processImageDataWithURLString:[_studioPhoto photoUrl] cacheImage:YES andBlock:^(NSData *imageData) {
            containerView.center=self.previewView.center;
            tempImage=[UIImage imageWithData:imageData];
            imageToBeModefied=[UIImage imageWithData:imageData];
            float x=(_previewView.frame.size.width/2)-(tempImage.size.width/2);
            float y=(_previewView.frame.size.height/2)-(tempImage.size.height/2);
            _photoImage.frame=CGRectMake(x,y,tempImage.size.width ,tempImage.size.height);
            if (_photoImage.frame.origin.x <0 || _photoImage.frame.origin.y<0) {
                CGSize tempSize=CGSizeMake(tempImage.size.width/3, tempImage.size.height/3);
                tempImage=[self imageWithImage:tempImage scaledToSize:tempSize];
                float x=(_previewView.frame.size.width/2)-(tempImage.size.width/2);
                float y=(_previewView.frame.size.height/2)-(tempImage.size.height/2);
                _photoImage.frame=CGRectMake(x,y,tempImage.size.width ,tempImage.size.height);
            }
            [_photoImage setImage:tempImage];
            //_photoImage.frame=[self centeredFrameForScrollView:self.view andUIView:_photoImage];
            oldHeight=_photoImage.frame.size.height;
            oldWidth=_photoImage.frame.size.width;
            
            [containerView setImageView:_photoImage];
            [containerView setCaptureType:[_studioPhoto  captureType]];
            [containerView setNeedsDisplay];
            oldX=_photoImage.frame.origin.x;
            oldY=_photoImage.frame.origin.y;
        }];
        camInitBefore=YES;
    }
    [self intCamFeed];
}

- (void)intCamFeed
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized && !firstTimeGranting) {
        [self initVideoSession];
    } else if(authStatus == AVAuthorizationStatusDenied&& !firstTimeGranting){
        [[[UIAlertView alloc] initWithTitle:nil message:@"Camera is required, Please enable it from settings and reopen the application" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil] show];
        [self.navigationController popViewControllerAnimated:YES];
    }else if(authStatus == AVAuthorizationStatusNotDetermined){
        firstTimeGranting = YES;
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(granted){
                    firstTimeGranting = NO;
                    [self initVideoSession];
                } else {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            });
        }];
    }
}

-(void)initVideoSession{
    @try {
        // Create AVCaptureSession and set the quality of the output
        self.session = [[AVCaptureSession alloc] init];
        self.session.sessionPreset = AVCaptureSessionPresetPhoto;
        
        // Get the Back Camera Device, init a AVCaptureDeviceInput linking the Device and add the input to the session.
        self.videoDevice = [self backCamera];
        //  self.isUsingFrontFacingCamera=YES;
        self.videoInput = [AVCaptureDeviceInput deviceInputWithDevice:self.videoDevice error:nil];
        
        [self.session addInput:self.videoInput];
        
        // Insert code to add still image output here
        
        // Init the AVCaptureVideoPreviewLayer with our created session. Get the UIView layer
        AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
        CALayer *viewLayer = self.previewView.layer;
        
        // Set the AVCaptureVideoPreviewLayer bounds to the main view bounds and fill it accordingly. Add as sublayer to the main UIView
        [viewLayer setMasksToBounds:YES];
        captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        captureVideoPreviewLayer.frame = [viewLayer bounds];
        [viewLayer addSublayer:captureVideoPreviewLayer];
        
        stillImageOutput = [[AVCaptureStillImageOutput alloc] init];  //meto
        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];    //meto
        [stillImageOutput setOutputSettings:outputSettings];   //meto
        [self.session addOutput:stillImageOutput];   //meto
        
        ///////////////
        // Start Running the Session
        [self.session startRunning];
        if ([_studioPhoto captureType]==1) {
            if ([_studioPhoto  photoOrientation]==1) {
                [captureVideoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortrait];  //comment if no orientation
            } else {
                [captureVideoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];  //comment if no orientation
            }
        }else{
            if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
                [captureVideoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortrait];  //comment if no orientation
            }else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft){
                [captureVideoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];  //comment if no orientation
            }else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
                [captureVideoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];  //comment if no orientation
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }

   
}

// Utility Function to get the front camera device
- (AVCaptureDevice *)frontCamera
{
    
    //Get all available devices, loop through and get the front position camera
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == AVCaptureDevicePositionFront)
        {
            return device;
        }
    }
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [device setTorchMode:AVCaptureTorchModeOff];
    [device setFlashMode:AVCaptureFlashModeOff];
    return nil;
}

// Utility Function to get the back camera device
- (AVCaptureDevice *)backCamera
{
    //Get all available devices, loop through and get the back position camera
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == AVCaptureDevicePositionBack)
        {
            return device;
        }
    }
    return nil;
}
-(void)viewDidAppear:(BOOL)animated{
    [self refreshBackCamera];
}
-(void)refreshBackCamera{

    AVCaptureDevicePosition desiredPosition;
    desiredPosition = AVCaptureDevicePositionBack;
    
    for (AVCaptureDevice *device in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo])
    {
        if ([device position] == desiredPosition)
        {
            // Begin a Configuration Change
            [self.session beginConfiguration];
            
            // Init new input as AVCaptureDeviceInput and remove the old input/.
            AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
            for (AVCaptureInput *oldInput in [self.session inputs])
            {
                [self.session removeInput:oldInput];
            }
            // Add new input to session and commit the configuartion changes.
            [self.session addInput:input];
            [self.session commitConfiguration];
            break;
        }
    }
}
- (IBAction)switchCamera:(id)sender {
    AVCaptureDevicePosition desiredPosition;
    // Get the opposite camera device of current
    if (self.isUsingFrontFacingCamera)
        desiredPosition = AVCaptureDevicePositionBack;
    else
        desiredPosition = AVCaptureDevicePositionFront;
    //Loop through available devices and select the one of our desiredPosition
    for (AVCaptureDevice *device in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo])
    {
        if ([device position] == desiredPosition)
        {
            // Begin a Configuration Change
            [self.session beginConfiguration];
            
            // Init new input as AVCaptureDeviceInput and remove the old input/.
            AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
            for (AVCaptureInput *oldInput in [self.session inputs])
            {
                [self.session removeInput:oldInput];
            }
            // Add new input to session and commit the configuartion changes.
            [self.session addInput:input];
            [self.session commitConfiguration];
            break;
        }
    }
    self.isUsingFrontFacingCamera = !self.isUsingFrontFacingCamera;
}
- (IBAction)capture:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        AVCaptureConnection *videoConnection = nil;
        for (AVCaptureConnection *connection in stillImageOutput.connections)
        {
            for (AVCaptureInputPort *port in [connection inputPorts])
            {
                if ([[port mediaType] isEqual:AVMediaTypeVideo] )
                {
                    videoConnection = connection;
                    if ([_studioPhoto captureType] == 1) {
                        if([_studioPhoto photoOrientation] == 1){
                            [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                        }
                        else{
                            [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                        }
                    }else{
                        if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
                            [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                        }else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft){
                            [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                        }else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
                                                     [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
                        }
                    }
                    
                    break;
                }
            }
            if (videoConnection) { break; }
        }
        NSLog(@"about to request a capture from: %@", stillImageOutput);
        [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                      completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
         {
             [SVProgressHUD dismiss];
             if (imageSampleBuffer != NULL) {
                 NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
                 UIImage *image = [[UIImage alloc] initWithData:imageData];
                 ///Essam Was Here/////
                 if (self.isUsingFrontFacingCamera)
                 image = [self flipImageWithImage:image];
                 
                 /////////////////////
                 newWidth=_photoImage.frame.size.width;
                 newHeight=_photoImage.frame.size.height;
                 UIImageView *dummyView =[[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                 dummyView.contentMode = UIViewContentModeScaleAspectFill;
                 dummyView.image = image;
                 
                 aviaryImage = [self CombineImage:[self imageWithView:dummyView]];
                 [self displayEditor];
             }else{
                 [self capture:nil];
             }
             
         }];
    });
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



-(UIImage *)CombineImage :(UIImage*)myCapturedImage
{
//    CGSize newSize = CGSizeMake(_mainCaptureView.frame.size.width, _mainCaptureView.frame.size.height);
//    UIGraphicsBeginImageContext(newSize);
//    [myCapturedImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    [_photoImage.image drawInRect:CGRectMake(_photoImage.frame.origin.x,
//                                     _photoImage.frame.origin.y,
//                                     _photoImage.frame.size.width,
//                                     _photoImage.frame.size.height) blendMode:kCGBlendModeNormal alpha:1];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
    

    UIView *dummyView =[[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [dummyView addSubview:[[UIImageView alloc] initWithImage:myCapturedImage]];
    [dummyView addSubview:_photoImage];
    
    UIImage * returnImage = [self imageWithView:dummyView];
    
    [self.mainCaptureView addSubview:_photoImage];

    return returnImage;
}


- (IBAction)backPressed:(id)sender {
    [self.session stopRunning];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)displayEditor {
    
    // kAviaryAPIKey and kAviarySecret are developer defined
    // and contain your API key and secret respectively
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [AFPhotoEditorController setAPIKey:kAviaryAPIKey secret:kAviarySecret];
        [AFPhotoEditorController setPremiumAddOns:AVYPhotoEditorPremiumAddOnWhiteLabel];
    });
    [AVYPhotoEditorCustomization setCropToolCustomEnabled:NO];
    [AVYPhotoEditorCustomization setCropToolOriginalEnabled:YES];
    [AVYPhotoEditorCustomization setCropToolInvertEnabled:NO];
    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:aviaryImage];
    [editorController setDelegate:self];

    UIGraphicsBeginImageContext(self.mainCaptureView.frame.size);
    [[UIImage imageNamed:@"app_bg.png"] drawInRect:self.mainCaptureView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    editorController.view.backgroundColor = [UIColor colorWithPatternImage:image];
    [[[(UIViewController*)editorController navigationController] navigationBar] setBarTintColor:[UIColor clearColor]];
    [[[(UIViewController*)editorController navigationController] navigationBar] setBackgroundColor:[UIColor clearColor]];
    
    for(NSObject *obj in [editorController.view subviews])
    {
        [(UIView*)obj setBackgroundColor:[UIColor clearColor]];
    }

    [self listSubviewsOfView:editorController.view];
    [self.navigationController pushViewController:editorController animated:NO];
}
-(void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image{
    
    UIImage* theImage;
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    UIImage* waterMarkImage = image.size.width > image.size.height ? [UIImage imageNamed:@"landscape_waterMark.png"]:[UIImage imageNamed:@"portrat_waterMark.png"];
    
    [waterMarkImage drawInRect:CGRectMake(0, 0,image.size.width, image.size.height)];
    
    theImage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    waterMark.alpha=0;
    editedImage=theImage;
    
    ShareViewController* share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
    [share setEditedImage:editedImage];
    [share setOriginalImage:originalImage];
    [share setStudioPhoto:_studioPhoto];
    [share setIsEditImage:NO];
    
    [self.session stopRunning];
    [self.navigationController pushViewController:share animated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)photoEditorCanceled:(AFPhotoEditorController *)editor{
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:NO];
    finalImage.alpha=0;
    _photoImage.alpha=1;
}

- (void)listSubviewsOfView:(UIView *)view {
    NSArray *subviews = [view subviews];
    if ([subviews count] == 0) return;
    for (UIView *subview in subviews) {
        if ([NSStringFromClass([subview class]) isEqualToString:@"AVYAceLowerTrayView"]) {
//            for (UIView *subview2 in [(UIView*)(subview.subviews[0]) subviews]) {
//                [subview2 setAlpha:0];
//            }
            [subview setAlpha:0];
        }
        [self listSubviewsOfView:subview];
    }
}

-(void)goBack{
    [self.navigationController.topViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (IBAction)switchChanged:(id)sender {
    if (!self.isUsingFrontFacingCamera) {
        [self.session startRunning];
        Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
        if (captureDeviceClass != nil) {
            AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
            if ([device hasTorch] && [device hasFlash]){
                [device lockForConfiguration:nil];
                
                if (flashState == 0) {
                    flashState = 1;
                    [device setTorchMode:AVCaptureTorchModeOn];
                    [device setFlashMode:AVCaptureFlashModeOn];
                    [(UIButton*)sender setTitle:[[Language sharedInstance] stringWithKey:@"flash_mode_On_capital"] forState:UIControlStateNormal];

                } else if (flashState == 1){
                    flashState = 2;
                    [device setTorchMode:AVCaptureTorchModeOff];
                    [device setFlashMode:AVCaptureFlashModeOff];
                    [(UIButton*)sender setTitle:[[Language sharedInstance] stringWithKey:@"flash_mode_Off_capital"] forState:UIControlStateNormal];

                } else if (flashState == 2){
                    flashState = 0;
                    [device setTorchMode:AVCaptureTorchModeOff];
                    [device setFlashMode:AVCaptureFlashModeAuto];
                    [(UIButton*)sender setTitle:[[Language sharedInstance] stringWithKey:@"flash_mode_Auto"] forState:UIControlStateNormal];
                }
                [device unlockForConfiguration];
            }
        }
    }
}
/*  5s error*/
-(BOOL)shouldAutorotate{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    if ([_studioPhoto captureType]==1) {
        if ([_studioPhoto photoOrientation]==1) {
            return UIInterfaceOrientationMaskPortrait;
        }else{
            return UIInterfaceOrientationMaskLandscapeLeft;
        }
    }else{
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight ;
    }
    
}

-(UIImage*)flipImageWithImage:(UIImage*)oldImage
{
    if ([_studioPhoto captureType]==1) {
        if ([_studioPhoto  photoOrientation]==1){
            UIImage* flippedImage = [UIImage imageWithCGImage:oldImage.CGImage
                                                        scale:oldImage.scale
                                                  orientation:UIImageOrientationLeftMirrored]; // portrait
            return flippedImage;
        }else{
            UIImage* flippedImage = [UIImage imageWithCGImage:oldImage.CGImage
                                                        scale:oldImage.scale
                                                  orientation:UIImageOrientationUpMirrored]; // landscape
            return flippedImage;
        }
    }else{
        if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
            UIImage* flippedImage = [UIImage imageWithCGImage:oldImage.CGImage
                                                        scale:oldImage.scale
                                                  orientation:UIImageOrientationLeftMirrored];
            return flippedImage;
        }else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft) {
            UIImage* flippedImage = [UIImage imageWithCGImage:oldImage.CGImage
                                                        scale:oldImage.scale
                                                  orientation:UIImageOrientationUpMirrored]; // landscape
            return flippedImage;
        }else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
            UIImage* flippedImage = [UIImage imageWithCGImage:oldImage.CGImage
                                                        scale:oldImage.scale
                                                  orientation:UIImageOrientationDownMirrored]; // landscape
            return flippedImage;
        }else
            return 0;
    }
}

-(BOOL)prefersStatusBarHidden
{
    return NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [_session stopRunning];
    _session=nil;
    _videoDevice=nil;
    _videoInput=nil;
    
    //_previewView = nil;
    objc_msgSend([UIDevice currentDevice], @selector(setOrientation:),UIInterfaceOrientationPortrait ); //cportrait
}

- (void)orientationChanged:(NSNotification *)notification{
    [self intCamFeed];
}

- (IBAction)setBrightness:(id)sender {
    UIImage* brightened = [imageToBeModefied imageWithBrightness:brightnessSlider.value] ;
    _photoImage.image = brightened;
}
- (IBAction)setContrast:(id)sender {
    UIImage* contrasted = [imageToBeModefied imageWithBrightness:contrastSlider.value] ;
    _photoImage.image = contrasted;
}

-(void)setLocalisables{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"CaptureOption1_title"]];
    [_flash setTitle:[[Language sharedInstance] stringWithKey:@"flash_mode_Auto"] forState:UIControlStateNormal];
}
-(UIImage*) scaleImage:(UIImage*)image {
    
    CGSize scaledSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    CGFloat scaleFactor;
    
    if (image.size.width > image.size.height) {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.width = scaledSize.width;
        scaledSize.height = scaledSize.width * scaleFactor;
    } else {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.height = scaledSize.height;
        scaledSize.width = scaledSize.height * scaleFactor;
    }
    
    UIGraphicsBeginImageContext(scaledSize);
    [image drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    UIImage * scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
@end
