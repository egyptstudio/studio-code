//
//  FansManager.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "BusinessManager.h"

@interface FansManager : BusinessManager


-(void)getFan:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getFans:(int)userId pageNo:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getFans:(int)userId pageNo:(int)pageNo searchText:(NSString*)searchText sortBy:(int)sortEnum filters:(NSMutableDictionary*)filters countriesIds:(NSArray*)countriesIds favoriteFans:(BOOL)favorateFan and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)followFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)unFollowFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)addFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)acceptFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)rejectFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)removeFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)addFavoriteFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)removeFavoriteFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)blockFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)unBlockFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;


+(FansManager*)getInstance;
@end
