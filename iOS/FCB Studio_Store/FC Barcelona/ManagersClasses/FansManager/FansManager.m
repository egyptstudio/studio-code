//
//  FansManager.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FansManager.h"
#import "Fan.h"
#import "Friend.h"
#import "DataHelper.h"
#import "RestServiceAgent.h"

@implementation FansManager
static FansManager* instance;
+(FansManager *)getInstance{
    if (!instance) {
        instance=[[FansManager alloc] init];
    }
    return instance;
}

-(void)getFan:(int)userId and:(void (^)(id))handler onFailure:(void (^)(NSError *))failure{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"fans/loadFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];

}

-(void)getFans:(int)userId
        pageNo:(int)pageNo
        searchText:(NSString*)searchText
        sortBy:(int)sortEnum
       filters:(NSMutableDictionary*)filters
       countriesIds:(NSArray*)countriesIds
       favoriteFans:(BOOL)favorateFan and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSMutableArray* vars = [[NSMutableArray alloc] init];
    [vars addObject:[NSString stringWithFormat:@"%d",userId]];
    [vars addObject:[NSString stringWithFormat:@"%d",pageNo]];
    NSMutableArray* keys =[[NSMutableArray alloc] init];
    [keys addObject:@"userId"];
    [keys addObject:@"pageNo"];
    
    if (![searchText isEqual:@""] && ![searchText isEqual:nil]) {
        [vars addObject:searchText];
        [keys addObject:@"searchText"];

    }
    if (sortEnum != 0) {
        [vars addObject:[NSString stringWithFormat:@"%d",sortEnum]];
        [keys addObject:@"sortBy"];
    }
    if (favorateFan) {
        [vars addObject:favorateFan?@"1":@"0"];
        [keys addObject:@"favoriteFans"];
    }
    if ([countriesIds count]>0) {
        [vars addObject:countriesIds];
        [keys addObject:@"countriesIds"];
    }
    
    BOOL isEnum = NO;
    
    for (NSString* key in [filters allKeys]) {
        if (![[filters objectForKey:key]  isEqual: @"0"])
            isEnum = YES;
    }
    if (isEnum) {
        [vars addObject:filters];
        [keys addObject:@"filters"];
        
    }
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"fans/loadFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];

}

#pragma mark - Methods by Bhavin. Start.

#define BaseURL @"http://barca.tawasoldev.com/barca/index.php/api/"

-(void)getFans:(int)userId
        pageNo:(int)pageNo
    searchText:(NSString*)searchText
        sortBy:(int)sortEnum
       filters:(NSMutableDictionary*)filters
  countriesIds:(NSArray*)countriesIds
       premium:(BOOL)isPremium
           and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSString *valMaleFemaleFilter = @"0";
    if ([filters[@"male"] intValue] == 1 && [filters[@"female"] intValue] == 0) {
        valMaleFemaleFilter = @"1";
    }
    else if ([filters[@"female"] intValue] == 1 && [filters[@"male"] intValue] == 0) {
        valMaleFemaleFilter = @"2";
    }
    else {
        valMaleFemaleFilter = @"3";
    }
    
    NSString *valOnlineOfflineFilter = @"0";
    if ([filters[@"online"] intValue] == 1 && [filters[@"offline"] intValue] == 0) {
        valOnlineOfflineFilter = @"1";
    }
    else if ([filters[@"offline"] intValue] == 1 && [filters[@"online"] intValue] == 0) {
        valOnlineOfflineFilter = @"2";
    }
    else {
        valOnlineOfflineFilter = @"3";
    }
    
    NSString *valFavoritesFilter = ([filters[@"favoriteFriends"] intValue] == 1) ? @"1" : @"2";
    
    NSString *valFollowingFilter = @"0";
    if ([filters[@"following"] intValue] == 1 && [filters[@"followers"] intValue] == 1) {
        valFollowingFilter = @"3";
    }
    else if ([filters[@"following"] intValue] == 1 && [filters[@"followers"] intValue] == 0) {
        valFollowingFilter = @"1";
    }
    else if ([filters[@"followers"] intValue] == 1 && [filters[@"following"] intValue] == 0) {
        valFollowingFilter = @"2";
    }
    
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userId],
                      [NSString stringWithFormat:@"%d",pageNo],
                      valMaleFemaleFilter,
                      valOnlineOfflineFilter,
                      valFavoritesFilter,
                      valFollowingFilter,
                      countriesIds,
                      [NSNumber numberWithBool:isPremium],
                      [NSNumber numberWithInt:sortEnum],
                      searchText];
    NSArray* keys = @[@"userID",@"startingLimit",@"maleFemaleFilter",@"onlineOfflineFilter",@"favoritesFilter",@"followingFilter",@"countryFilter",@"onlyPremuim",@"sortedBy",@"keyword"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/loadFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)loadPremiumBar:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    [self getObjectListWithAction:@"fans/loadPremuimBar" andParams:@{} andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)getSuggestionsFans:(NSString *)keyword and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[keyword];
    NSArray* keys = @[@"keyword"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/getSuggestionsFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)getPublicProfileFanWithUserID:(int)userID andFanID:(int)fanID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID], [NSString stringWithFormat:@"%d",fanID]];
    NSArray* keys = @[@"userID",@"fanID"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/getPublicProfileFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)changeFavoriteForUserID:(int)userID fanID:(int)fanID favorite:(BOOL)isFavorite and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID], [NSString stringWithFormat:@"%d",fanID], [NSNumber numberWithBool:isFavorite]];
    NSArray* keys = @[@"userID",@"fanID",@"favorite"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/changeFavorite" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)changeFollowForUserID:(int)userID fanID:(int)fanID follow:(BOOL)isFollow and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID], [NSString stringWithFormat:@"%d",fanID], [NSNumber numberWithBool:isFollow]];
    NSArray* keys = @[@"userID",@"fanID",@"follow"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/changeFollow" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)changeFriendForUserID:(int)userID fanID:(int)fanID friend:(BOOL)isFriend and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[@(userID), @(fanID), @(isFriend)];
    NSArray* keys = @[@"userID",@"fanID",@"friend"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self submitWithAction:@"fans/changeFriend" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)changeBlockForUserID:(int)userID fanID:(int)fanID block:(BOOL)isBlock and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID], [NSString stringWithFormat:@"%d",fanID], [NSNumber numberWithBool:isBlock]];
    NSArray* keys = @[@"userID",@"fanID",@"block"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/changeBlock" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)loadFollowingSuggestionWithStartingLimit:(int)nextStartingLimit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",nextStartingLimit]];
    NSArray* keys = @[@"nextStartingLimit"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/loadFollowingSuggestion" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)loadFollowingForFanWithUserID:(int)userID fanID:(int)fanID andStartingLimit:(int)nextStartingLimit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID],
                      [NSString stringWithFormat:@"%d",nextStartingLimit],
                      [NSString stringWithFormat:@"%d",fanID]];
    NSArray* keys = @[@"userID",@"startingLimit",@"fanID"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/loadFollowingForFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)loadFollowersForFanWithUserID:(int)userID fanID:(int)fanID andStartingLimit:(int)nextStartingLimit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID],
                      [NSString stringWithFormat:@"%d",nextStartingLimit],
                      [NSString stringWithFormat:@"%d",fanID]];
    NSArray* keys = @[@"userID",@"startingLimit",@"fanID"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/loadFollowersForFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

#pragma mark - Friends

- (void)loadFriendsWithUserID:(int)userID andStartingLimit:(int)nextStartingLimit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID],
                      [NSString stringWithFormat:@"%d",nextStartingLimit]];
    NSArray* keys = @[@"userID",@"startingLimit"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"chat/loadFriends" andParams:dic andIam:[[Friend alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)loadFriendRequestsWithUserID:(int)userID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID]];
    NSArray* keys = @[@"userID"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/loadFriendRequests" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)declineFriendRequestsWithUserID:(int)userID fanID:(int)fanID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID],
                      [NSString stringWithFormat:@"%d",fanID]];
    NSArray* keys = @[@"userID",@"fanID"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/declineFriendRequests" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)whoInstalledAppWithUserID:(int)userID contacts:(NSArray *)contactsList and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[contactsList,[NSString stringWithFormat:@"%d",userID],@"1"];
    NSArray* keys = @[@"contacts",@"userID",@"pageNo"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"user/filterUserContacts" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)inviteToAppWithMessage:(NSString *)message contacts:(NSArray *)contactsList and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[message, contactsList];
    NSArray* keys = @[@"message",@"contacts"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/inviteToApp" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

- (void)loadContactsWithUserID:(int)userID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = @[[NSString stringWithFormat:@"%d",userID]];
    NSArray* keys = @[@"userID"];
    NSDictionary* dic = [[NSDictionary alloc] initWithObjects:vars forKeys:keys];
    
    [self getObjectListWithAction:@"fans/loadContacts" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

#pragma mark - Methods by Bhavin. End.

-(void)followFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"barca/followFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)unFollowFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"barca/unfollowFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)addFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"addFriendRequest" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)acceptFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"acceptFriendRequest" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)rejectFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"rejectFriendRequest" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)removeFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"removeFriend" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)addFavoriteFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"addFavoriteFriend" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)removeFavoriteFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"removeFavoriteFriend" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];

}
-(void)blockFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                     [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"blockFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)unBlockFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],
                                              [NSString stringWithFormat:@"%d",fanId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fanId", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"unBlockFan" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getTopFans:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    
    NSArray* vars = [NSArray arrayWithObjects:@"", nil];
    NSArray* keys = [NSArray arrayWithObjects:@"", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"getTopFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}
-(void)followFans:(int)userId fansIds:(NSArray*)fansIds and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userId],[self jsonStringFromArray:fansIds], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"userId",@"fansIds", nil];
    
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"followFans" andParams:dic andIam:[[Fan alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(NSString*)jsonStringFromDic:(NSDictionary*)info
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:info
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                       withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\"
                                                       withString:@""];
    return jsonString;
}
-(NSString*)jsonStringFromArray:(NSArray*)info
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:info
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n"
                                                       withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\"
                                                       withString:@""];
    return jsonString;
}
@end
