//
//  Friend.h
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/3/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Friend : NSObject

#pragma mark - Added by Bhavin. START.

@property(nonatomic) int friendID;
@property(nonatomic) int friendPicNumber;

@property(nonatomic , strong) NSString* friendName;
@property(nonatomic , strong) NSString* friendPicture;

@property(nonatomic ) int friendOnline;
@property(nonatomic ) BOOL isFavorite;
@property(nonatomic ) BOOL isFollowed;
@property(nonatomic ) BOOL isPremium;

//@property(nonatomic ) BOOL isFriend;
//@property(nonatomic ) BOOL isBlocked;

//@property(nonatomic) int fanFollowers;
//@property(nonatomic) int fanFollows;
//
//@property(nonatomic , strong) NSString* fanBrief;
//@property(nonatomic , strong) NSString* fanCity;
//@property(nonatomic , strong) NSString* fanCountry;
//@property(nonatomic , strong) NSString* fanDistrict;
//@property(nonatomic , strong) NSArray* moreInfo;

#pragma mark - Added by Bhavin. END.

@end
