//
//  Friend.m
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/3/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "Friend.h"

@implementation Friend

@synthesize friendID;
@synthesize friendPicNumber;
@synthesize friendName;
@synthesize friendPicture;
@synthesize friendOnline;
@synthesize isFavorite;
@synthesize isFollowed;
@synthesize isPremium;
//@synthesize isFriend;
//@synthesize isBlocked;
//
//@synthesize fanFollowers;
//@synthesize fanFollows;
//
//@synthesize fanBrief;
//@synthesize fanCity;
//@synthesize fanCountry;
//@synthesize fanDistrict;
//@synthesize moreInfo;

-(id)init
{
    self = [super init];
    if (self != nil) {
        friendID = 0;
        friendPicNumber = 0;
        friendName = @"";
        friendPicture = @"";
        friendOnline = NO;
        isFavorite = NO;
        isFollowed = NO;
        isPremium = NO;
//        isFriend = NO;
//        isBlocked = NO;
//        
//        fanFollowers = 0;
//        fanFollows = 0;
//        fanBrief = @"";
//        fanCity = @"";
//        fanCountry = @"";
//        fanDistrict = @"";
//        moreInfo = @[];
    }
    return self;
}

-(id)initWithCoder:(NSCoder*)decoder
{
    if (self = [super init]) {
        
        friendID = [decoder decodeIntForKey:@"friendID"];
        friendPicNumber = [decoder decodeIntForKey:@"friendPicNumber"];
        friendName = [decoder decodeObjectForKey:@"friendName"];
        friendPicture   = [decoder decodeObjectForKey:@"friendPicture"];
        friendOnline = [decoder decodeBoolForKey:@"friendOnline"];
        isFavorite = [decoder decodeBoolForKey:@"isFavorite"];
        isFollowed = [decoder decodeBoolForKey:@"isFollowed"];
        isPremium   = [decoder decodeBoolForKey:@"isPremium"];
//        isFriend   = [decoder decodeBoolForKey:@"isFriend"];
//        isBlocked   = [decoder decodeBoolForKey:@"isBlocked"];
//        
//        fanFollowers = [decoder decodeIntForKey:@"fanFollowers"];
//        fanFollows = [decoder decodeIntForKey:@"fanFollows"];
//        fanBrief = [decoder decodeObjectForKey:@"fanBrief"];
//        fanCity   = [decoder decodeObjectForKey:@"fanCity"];
//        fanCountry = [decoder decodeObjectForKey:@"fanCountry"];
//        fanDistrict   = [decoder decodeObjectForKey:@"fanDistrict"];
//        moreInfo = [decoder decodeObjectForKey:@"moreInfo"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder*)encoder
{
    [encoder encodeInt:friendID forKey:@"friendID"];
    [encoder encodeInt:friendPicNumber forKey:@"friendPicNumber"];
    [encoder encodeObject:friendName forKey:@"friendName"];
    [encoder encodeObject:friendPicture forKey:@"friendPicture"];
    [encoder encodeBool:friendOnline forKey:@"friendOnline"];
    [encoder encodeBool:isFavorite forKey:@"isFavorite"];
    [encoder encodeBool:isFollowed forKey:@"isFollowed"];
    [encoder encodeBool:isPremium forKey:@"isPremium"];
//    [encoder encodeBool:isFriend forKey:@"isFriend"];
//    [encoder encodeBool:isBlocked forKey:@"isBlocked"];
//    
//    [encoder encodeInt:fanFollowers forKey:@"fanFollowers"];
//    [encoder encodeInt:fanFollows forKey:@"fanFollows"];
//    [encoder encodeObject:fanBrief forKey:@"fanBrief"];
//    [encoder encodeObject:fanCity forKey:@"fanCity"];
//    [encoder encodeObject:fanCountry forKey:@"fanCountry"];
//    [encoder encodeObject:fanDistrict forKey:@"fanDistrict"];
//    [encoder encodeObject:moreInfo forKey:@"moreInfo"];
}

@end
