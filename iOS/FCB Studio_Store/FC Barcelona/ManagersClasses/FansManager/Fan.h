//
//  Fan.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fan : NSObject

#pragma mark - Added by Bhavin. START.

@property(nonatomic) int fanID;
@property(nonatomic) int fanPicNumber;

@property(nonatomic , strong) NSString* fanName;
@property(nonatomic , strong) NSString* fanPicture;

@property(nonatomic ) int fanOnline;
@property(nonatomic ) BOOL isFavorite;
@property(nonatomic ) BOOL isFollowed;
@property(nonatomic ) BOOL isFriend;
@property(nonatomic ) BOOL isPremium;
@property(nonatomic ) BOOL isBlocked;

@property(nonatomic) int fanFollowers;
@property(nonatomic) int fanFollows;

@property(nonatomic , strong) NSString* fanBrief;
@property(nonatomic , strong) NSString* fanCity;
@property(nonatomic , strong) NSString* fanCountry;
@property(nonatomic , strong) NSString* fanDistrict;
@property(nonatomic , strong) NSString* contactnName;

@property(nonatomic , strong) NSArray* moreInfo;

#pragma mark - Added by Bhavin. END.

@property(nonatomic) int userId;
@property(nonatomic , strong) NSString* userName;
@property(nonatomic , strong) NSString* profilePic;
//@property(nonatomic ) BOOL online;
//@property(nonatomic ) BOOL isFollow;
//@property(nonatomic ) BOOL isFriend;
//@property(nonatomic ) BOOL isPremium;
//@property(nonatomic ) BOOL isBlocked;
@property(nonatomic , strong) NSString* countryText;
//@property(nonatomic , strong) NSString* fanBrief;
@property(nonatomic , strong) NSString* followingNo;
@property(nonatomic , strong) NSString* followersNo;
//@property(nonatomic ) BOOL bestFriend;
//@property(nonatomic ) BOOL privacyPublic;

@end
