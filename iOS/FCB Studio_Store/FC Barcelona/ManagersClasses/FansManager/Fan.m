//
//  Fan.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "Fan.h"

@implementation Fan

@synthesize fanID;
@synthesize fanPicNumber;
@synthesize fanName;
@synthesize fanPicture;
@synthesize fanOnline;
@synthesize isFavorite;
@synthesize isFollowed;
@synthesize isFriend;
@synthesize isPremium;
@synthesize isBlocked;

@synthesize fanFollowers;
@synthesize fanFollows;

@synthesize fanBrief;
@synthesize fanCity;
@synthesize fanCountry;
@synthesize fanDistrict;
@synthesize contactnName;
@synthesize moreInfo;

-(id)init
{
    self = [super init];
    if (self != nil) {
        fanID = 0;
        fanPicNumber = 0;
        fanName = @"";
        fanPicture = @"";
        fanOnline = NO;
        isFavorite = NO;
        isFollowed = NO;
        isFriend = NO;
        isPremium = NO;
        isBlocked = NO;

        fanFollowers = 0;
        fanFollows = 0;
        fanBrief = @"";
        fanCity = @"";
        fanCountry = @"";
        fanDistrict = @"";
        contactnName = @"";
        moreInfo = @[];
    }
    return self;
}

-(id)initWithCoder:(NSCoder*)decoder
{
    if (self = [super init]) {
        
        fanID = [decoder decodeIntForKey:@"fanID"];
        fanPicNumber = [decoder decodeIntForKey:@"fanPicNumber"];
        fanName = [decoder decodeObjectForKey:@"fanName"];
        fanPicture   = [decoder decodeObjectForKey:@"fanPicture"];
        fanOnline = [decoder decodeBoolForKey:@"fanOnline"];
        isFavorite = [decoder decodeBoolForKey:@"isFavorite"];
        isFollowed = [decoder decodeBoolForKey:@"isFollowed"];
        isFriend   = [decoder decodeBoolForKey:@"isFriend"];
        isPremium   = [decoder decodeBoolForKey:@"isPremium"];
        isBlocked   = [decoder decodeBoolForKey:@"isBlocked"];

        fanFollowers = [decoder decodeIntForKey:@"fanFollowers"];
        fanFollows = [decoder decodeIntForKey:@"fanFollows"];
        fanBrief = [decoder decodeObjectForKey:@"fanBrief"];
        fanCity   = [decoder decodeObjectForKey:@"fanCity"];
        fanCountry = [decoder decodeObjectForKey:@"fanCountry"];
        fanDistrict   = [decoder decodeObjectForKey:@"fanDistrict"];
        contactnName   = [decoder decodeObjectForKey:@"contactnName"];
        moreInfo = [decoder decodeObjectForKey:@"moreInfo"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder*)encoder
{
    [encoder encodeInt:fanID forKey:@"fanID"];
    [encoder encodeInt:fanPicNumber forKey:@"fanPicNumber"];
    [encoder encodeObject:fanName forKey:@"fanName"];
    [encoder encodeObject:fanPicture forKey:@"fanPicture"];
    [encoder encodeBool:fanOnline forKey:@"fanOnline"];
    [encoder encodeBool:isFavorite forKey:@"isFavorite"];
    [encoder encodeBool:isFollowed forKey:@"isFollowed"];
    [encoder encodeBool:isFriend forKey:@"isFriend"];
    [encoder encodeBool:isPremium forKey:@"isPremium"];
    [encoder encodeBool:isBlocked forKey:@"isBlocked"];

    [encoder encodeInt:fanFollowers forKey:@"fanFollowers"];
    [encoder encodeInt:fanFollows forKey:@"fanFollows"];
    [encoder encodeObject:fanBrief forKey:@"fanBrief"];
    [encoder encodeObject:fanCity forKey:@"fanCity"];
    [encoder encodeObject:fanCountry forKey:@"fanCountry"];
    [encoder encodeObject:fanDistrict forKey:@"fanDistrict"];
    [encoder encodeObject:contactnName forKey:@"contactnName"];
    [encoder encodeObject:moreInfo forKey:@"moreInfo"];
}

@end
