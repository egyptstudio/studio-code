//
//  FansManager.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "BusinessManager.h"

@interface FansManager : BusinessManager


-(void)getFan:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
//-(void)getFans:(int)userId pageNo:(int)pageNo and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getFans:(int)userId pageNo:(int)pageNo searchText:(NSString*)searchText sortBy:(int)sortEnum filters:(NSMutableDictionary*)filters countriesIds:(NSArray*)countriesIds favoriteFans:(BOOL)favorateFan and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)getFans:(int)userId
        pageNo:(int)pageNo
    searchText:(NSString*)searchText
        sortBy:(int)sortEnum
       filters:(NSMutableDictionary*)filters
  countriesIds:(NSArray*)countriesIds
       premium:(BOOL)isPremium
           and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

- (void)loadPremiumBar:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)getSuggestionsFans:(NSString *)keyword and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)getPublicProfileFanWithUserID:(int)userID andFanID:(int)fanID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)changeFavoriteForUserID:(int)userID fanID:(int)fanID favorite:(BOOL)isFavorite and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)changeFollowForUserID:(int)userID fanID:(int)fanID follow:(BOOL)isFollow and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)changeFriendForUserID:(int)userID fanID:(int)fanID friend:(BOOL)isFriend and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)changeBlockForUserID:(int)userID fanID:(int)fanID block:(BOOL)isBlock and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)loadFollowingSuggestionWithStartingLimit:(int)nextStartingLimit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)loadFollowingForFanWithUserID:(int)userID fanID:(int)fanID andStartingLimit:(int)nextStartingLimit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)loadFollowersForFanWithUserID:(int)userID fanID:(int)fanID andStartingLimit:(int)nextStartingLimit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

- (void)loadFriendsWithUserID:(int)userID andStartingLimit:(int)nextStartingLimit and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)loadFriendRequestsWithUserID:(int)userID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)declineFriendRequestsWithUserID:(int)userID fanID:(int)fanID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)whoInstalledAppWithUserID:(int)userID contacts:(NSArray *)contactsList and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)inviteToAppWithMessage:(NSString *)message contacts:(NSArray *)contactsList and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
- (void)loadContactsWithUserID:(int)userID and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)followFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)unFollowFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)addFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)acceptFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)rejectFriendRequest:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)removeFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)addFavoriteFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)removeFavoriteFriend:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)blockFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)unBlockFan:(int)userId fanId:(int)fanId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

-(void)getTopFans:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)followFans:(int)userId fansIds:(NSArray*)fansIds and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

+(FansManager*)getInstance;
@end
