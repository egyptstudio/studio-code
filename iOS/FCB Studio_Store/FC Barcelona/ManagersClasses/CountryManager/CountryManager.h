//
//  CountryManager.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Country.h"
#import "BusinessManager.h"
@interface CountryManager : BusinessManager
-(Country*)getCountry:(int)countryId;
-(void)getCountriesListand:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getCitiesListForCountry:(int)countryId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;

+(CountryManager*)getInstance;
@end
