//
//  CountryManager.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "CountryManager.h"
#import "Country.h"
#import "City.h"
@implementation CountryManager
static CountryManager* instance;

+(CountryManager *)getInstance{
    if (!instance) {
        instance=[[CountryManager alloc] init];
    }return instance;
}
-(Country*)getCountry:(int)countryId
{
    Country* countryObject = [[Country alloc] init];
    return countryObject;
}

-(void)getCountriesListand:(void (^)(id))handler onFailure:(void (^)(NSError *))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"lang", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"user/getCountryList" andParams:dic andIam:[[Country alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
}

-(void)getCitiesListForCountry:(int)countryId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure{
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation],[NSString stringWithFormat:@"%d",countryId], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"lang",@"countryId", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self getObjectListWithAction:@"user/getCountryCities" andParams:dic andIam:[[City alloc] init] and:^(id currentClient) {
        handler(currentClient);
    } onFailure:^(NSError *error) {
        failure(error);
    }];
    
}
@end
