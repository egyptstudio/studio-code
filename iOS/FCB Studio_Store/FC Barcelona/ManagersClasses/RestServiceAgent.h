//
//  WebRequests.h
//  Westwing
//
//  Created by Eissa on 8/22/14.
//  Copyright (c) 2014 Westwing. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	typePost = 1,
	typeGet,
    typeMultiPart,
} requestType;

@interface RestServiceAgent : NSObject<NSURLConnectionDelegate>
+(void)setPostRequestByAction:(NSString*)action andWSDL:(NSString*)WSDLURL andParamsKeys:(id)keys andParamsValues:(id)values and:(void (^)(id currentClient))handler;

+(void)setRequestType:(requestType)type toUrl:(NSString*)url andAction:(NSString*)action withParamsKeys:(id)keys andParamsValues:(id)values andTimeout:(NSInteger)timeOut and:(void (^)(id currentClient))handler;

+ (NSMutableURLRequest *)postRequestWithParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL andTimeout:(NSInteger)timeOut;

+ (NSMutableURLRequest *)getRequestWithParameters:(NSDictionary *)urlParameters andBaseURL:(NSString *)baseURL andTimeout:(NSInteger)timeOut;

+ (NSMutableURLRequest *)multiPartRequestWithParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL andTimeout:(NSInteger)timeOut;

+(BOOL)internetAvailable;

@end
