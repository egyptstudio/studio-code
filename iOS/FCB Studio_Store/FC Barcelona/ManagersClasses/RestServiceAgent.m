//
//  WebRequests.m
//  Westwing
//
//  Created by Eissa on 8/22/14.
//  Copyright (c) 2014 Westwing. All rights reserved.
//


#import "RestServiceAgent.h"
#import "JSONKit.h"
#import "WebService.h"
#import "Reachability.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
@interface NSURLRequest (NSURLRequestWithIgnoreSSL)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host;
@end

@implementation RestServiceAgent


-(id)init {
    self = [super init];
    return self;
}

+(void)setPostRequestByAction:(NSString*)action andWSDL:(NSString*)WSDLURL andParamsKeys:(id)keys andParamsValues:(id)values and:(void (^)(id currentClient))handler
{
    
    
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjects:values forKeys:keys];
    //NSMutableURLRequest *request = [WebService postRequestWithUploadParameters:params andBaseURL:[NSString stringWithFormat:@"%@%@",WSDLURL,action]];
    
    
    NSMutableURLRequest *request = [WebService postRequestForServicesWithUploadParameters:params andBaseURL:[NSString stringWithFormat:@"%@%@",WSDLURL,action]];
    

    

    
    __block NSMutableData* responseData = [[NSMutableData alloc] init];
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    @try
    {
        [NSURLRequest allowsAnyHTTPSCertificateForHost:@""];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error) {
                 responseData = nil;
                 
                 NSLog(@"error:%@", error.localizedDescription);
             }
             
             if(responseData)
             {
                 [responseData appendData:data];
                 JSONDecoder *decoder = [JSONDecoder decoder];
                 handler([decoder objectWithData:responseData]);
             }
             else
             {
                 handler(@"timeout");
             }
         }];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    //return result;
}


+(void)setRequestType:(requestType)type toUrl:(NSString*)url andAction:(NSString*)action withParamsKeys:(id)keys andParamsValues:(id)values andTimeout:(NSInteger)timeOut and:(void (^)(id currentClient))handler
{
    url = [url stringByAppendingString:action];
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjects:values forKeys:keys];
    NSMutableURLRequest *request ;
    if(type==typePost)
        //NSMutableURLRequest *
        request = [WebService postRequestForServicesWithUploadParameters:params andBaseURL:[NSString stringWithFormat:@"%@",url]];
    
    //request = [self postRequestWithParameters:params andBaseURL:[NSString stringWithFormat:@"%@",url] andTimeout:timeOut];
    else if(type==typeGet)
        request = [self getRequestWithParameters:params andBaseURL:[NSString stringWithFormat:@"%@",url] andTimeout:timeOut];
    else if(type==typeMultiPart)
        request = [self multiPartRequestWithParameters:params andBaseURL:[NSString stringWithFormat:@"%@",url] andTimeout:timeOut];
    
    
    
    __block NSMutableData* responseData = [[NSMutableData alloc] init];
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [NSURLRequest allowsAnyHTTPSCertificateForHost:@""];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error) {
             responseData = nil;
             
             NSLog(@"error:%@", error.localizedDescription);
         }
         if(responseData)
         {
             [responseData appendData:data];
             //JSONDecoder *decoder = [JSONDecoder decoder];
             //decoder objectWithData:responseData error:&tempError]
             NSLog(@"ResponseString:=======|||||%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
             NSError* jsonError;
             id jsonObj = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             handler(jsonObj);
         }
         else
         {
             handler(@"timeout");
         }
     }];
}

+ (NSMutableURLRequest *)getRequestWithParameters:(NSDictionary *)urlParameters andBaseURL:(NSString *)baseURL andTimeout:(NSInteger)timeOut{
    NSURL *urlObject = nil;
    
    NSString *urlString = [NSString stringWithString:baseURL];
    
    for (id param in urlParameters) {
        if ([param isKindOfClass:[NSString class]] && [[urlParameters objectForKey:param] isKindOfClass:[NSString class]]) {
            urlString = [urlString stringByAppendingFormat:@"%@=%@&",param,[urlParameters objectForKey:param]];
        } else if ([param isKindOfClass:[NSString class]] && [[urlParameters objectForKey:param] isKindOfClass:[NSArray class]]) {
            NSArray *arrayParams = [urlParameters objectForKey:param];
            for (id subParam in arrayParams) {
                if ([subParam isKindOfClass:[NSString class]]) {
                    urlString = [urlString stringByAppendingFormat:@"%@=%@&",param,subParam];
                }
            }
        }
    }
    if ([[urlString substringFromIndex:([urlString length] - 1)] isEqualToString:@"&"]) {
        urlString = [urlString substringToIndex:([urlString length] - 1)];
    }
    
    urlObject = [NSURL URLWithString:urlString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:urlObject cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeOut];
    return urlRequest;
}

+ (NSMutableURLRequest *)postRequestWithParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL andTimeout:(NSInteger)timeOut
{
    NSURL *urlObject = [NSURL URLWithString:baseURL];
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlObject cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeOut];
    
    [request setHTTPMethod:@"POST"];
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType =[NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary] ;
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    NSArray *paramsKeys=[[NSArray alloc] initWithArray:[urlParameters allKeys]];
    
    for (int i=0; i<[paramsKeys count]; i++)
    {
        id value = [urlParameters objectForKey:[paramsKeys objectAtIndex:i]] ;
        
        if ([value isKindOfClass:[NSString class]])
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithString:value] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        else if ([value isKindOfClass:[NSNumber class]])
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%i",[value intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [request setHTTPBody:body];
    
    return request;
}

+ (NSMutableURLRequest *)multiPartRequestWithParameters:(NSMutableDictionary *)urlParameters andBaseURL:(NSString *)baseURL andTimeout:(NSInteger)timeOut{
    
    NSString * params =@"";
    NSURL *urlObject = [NSURL URLWithString:[baseURL stringByAppendingString:params]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlObject cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeOut];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    NSArray *paramsKeys=[[NSArray alloc] initWithArray:[urlParameters allKeys]];
    
    for (int i=0; i<[paramsKeys count]; i++)
    {
        id value = [urlParameters objectForKey:[paramsKeys objectAtIndex:i]] ;
        
        if ([value isKindOfClass:[NSData class]])
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"%@\"; filename=\"aaaa.png\"\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[urlParameters objectForKey:[paramsKeys objectAtIndex:i]]]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        else if ([value isKindOfClass:[NSString class]])
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithString:value] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        else if ([value isKindOfClass:[NSNumber class]])
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[paramsKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%i",[value intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [request setHTTPBody:body];
    
    return request;
}

+ (BOOL)internetAvailable{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

@end
@implementation NSURLRequest (NSURLRequestWithIgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end
