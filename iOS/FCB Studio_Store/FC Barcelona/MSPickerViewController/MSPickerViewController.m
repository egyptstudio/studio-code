//
//  MSPickerViewController.m
//  ToDoTeam
//
//  Created by Salem on 6/19/13.
//  Copyright (c) 2013 Salem. All rights reserved.
//

#import "MSPickerViewController.h"

@interface MSPickerViewController () {
    UIView *backgroundView;
    UIButton *backgroundBtn;
    BOOL pickerForDate;
    BOOL isPickerAlife;
}

@end

@implementation MSPickerViewController

#pragma mark - Picker view methods

- (void)showPickerViewInView2:(UIView *)view {
    [view endEditing:YES];
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    backgroundBtn = [[UIButton alloc] initWithFrame:backgroundView.frame];
    
    [backgroundBtn addTarget:self action:@selector(dismissPickerView) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:backgroundView];
    [backgroundView addSubview:backgroundBtn];
    
    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            self.view.frame = CGRectMake(0, screenSize.height-295, screenSize.width, 202);
        }
        else {
            self.view.frame = CGRectMake(0, screenSize.height-20-295, screenSize.width, 202);
        }
        
        [view addSubview:self.view];
        
    } completion:nil];
    
}


- (void)showPickerViewInView:(UIView *)view {
    [view endEditing:YES];
    if (!isPickerAlife) {
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        
        backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
        backgroundBtn = [[UIButton alloc] initWithFrame:backgroundView.frame];
        
        [backgroundBtn addTarget:self action:@selector(dismissPickerView) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:backgroundView];
        [backgroundView addSubview:backgroundBtn];
        
        [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                self.view.frame = CGRectMake(0, screenSize.height-202, screenSize.width, 202);
            }
            else {
                self.view.frame = CGRectMake(0, screenSize.height-20-202, screenSize.width, 202);
            }
            
            [view addSubview:self.view];
            isPickerAlife = YES;
            
        } completion:nil];
    }
    
    
}

- (void)dismissPickerView {
    
    [backgroundView removeFromSuperview];
    
    [UIView animateWithDuration:.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
        
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            self.view.frame = CGRectMake(0, screenSize.height, screenSize.width, 202);
        }
        else {
            self.view.frame = CGRectMake(0, screenSize.height-20, screenSize.width, 202);
        }
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        [self.delegate pickerViewDidDismiss];
        isPickerAlife = NO;
        
    }];
    
}

- (IBAction)done:(id)sender {
    
    if (pickerForDate) {
        if (self.delegate != nil &&
            [self.delegate conformsToProtocol:@protocol(MSPickerViewControllerDelegate)] &&
            [self.delegate respondsToSelector:@selector(pickerView:didSelectDate:)]) {
            [self.delegate pickerView:self didSelectDate:_datePickerView.date ];
        }
    }
    else{
        if (self.delegate != nil &&
            [self.delegate conformsToProtocol:@protocol(MSPickerViewControllerDelegate)] &&
            [self.delegate respondsToSelector:@selector(pickerView:didSelectItem:)]) {
            [self.delegate pickerView:self didSelectItem:self.selectedItem];
        }
    }
    
    
    [self dismissPickerView];
    
}

- (IBAction)cancel:(id)sender {
    [self dismissPickerView];
}

#pragma mark - Picker view data source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.items.count;
}

#pragma mark - Picker view delegate

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:self.items[row] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attString;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.items[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectedItem = row;
}

#pragma mark - Picker view initializers

- (id)initWithTitle:(NSString *)title items:(NSArray *)items selectedItem:(int)row  {
    
    self = [super initWithNibName:@"MSPickerViewController" bundle:nil];
    
    if (self) {
        
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            self.view.frame = CGRectMake(0, screenSize.height, screenSize.width, 202);
        }
        else {
            self.view.frame = CGRectMake(0, screenSize.height-20, screenSize.width, 202);
        }
        
        self.titleLabel.text = title;
        self.items = items;
        
        if (row == -1)
            self.selectedItem = 0;
        else
            self.selectedItem = row;
        
        [self.pickerView selectRow:row inComponent:0 animated:NO];
        
    }
    [_datePickerView setHidden:YES];
    return self;
    
}

- (id)initWithTitle:(NSString *)title{
    
    self = [super initWithNibName:@"MSPickerViewController" bundle:nil];
    if (self) {
        
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            self.view.frame = CGRectMake(0, screenSize.height, screenSize.width, 202);
        }
        else {
            self.view.frame = CGRectMake(0, screenSize.height-20, screenSize.width, 202);
        }
        
        self.titleLabel.text = title;
    }
    
    pickerForDate = YES ;
    [_datePickerView setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    [_pickerView setHidden:YES];
    return self;
    
}

+ (id)datePickerViewWithTitle:(NSString *)title{
    
    MSPickerViewController *pickerViewController = [[MSPickerViewController alloc] initWithTitle:title];
    return pickerViewController;
    
}
+ (id)pickerViewWithTitle:(NSString *)title forItems:(NSArray *)items selectedItem:(int)row {
    
    MSPickerViewController *pickerViewController = [[MSPickerViewController alloc] initWithTitle:title items:items selectedItem:row];
    
    return pickerViewController;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
