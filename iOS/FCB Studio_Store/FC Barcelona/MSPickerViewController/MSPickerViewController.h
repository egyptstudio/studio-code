//
//  MSPickerViewController.h
//  ToDoTeam
//
//  Created by Salem on 6/19/13.
//  Copyright (c) 2013 Salem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MSPickerViewControllerDelegate;

@interface MSPickerViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, weak) id delegate;

@property (nonatomic, weak) IBOutlet UIPickerView *pickerView;
@property (nonatomic, weak) IBOutlet UIDatePicker *datePickerView;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIButton *doneButton;

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, assign) int selectedItem;
@property (nonatomic, assign) int tag;

+ (MSPickerViewController *)pickerViewWithTitle:(NSString *)title forItems:(NSArray *)items selectedItem:(int)row;
+ (MSPickerViewController *)datePickerViewWithTitle:(NSString *)title;
- (void)showPickerViewInView2:(UIView *)view;
- (void)showPickerViewInView:(UIView *)view;
- (void)dismissPickerView;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;

@end

@protocol MSPickerViewControllerDelegate
- (void)pickerView:(MSPickerViewController *)pickerView didSelectItem:(int)row;
- (void)pickerView:(MSPickerViewController *)pickerView didSelectDate:(NSDate*)date;
- (void)pickerViewDidDismiss;
@end
