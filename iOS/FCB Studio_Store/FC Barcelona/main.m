    //  main.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/26/14.
//  Copyright (c) 2016 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BarcelonaAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BarcelonaAppDelegate class]));
    }
}
