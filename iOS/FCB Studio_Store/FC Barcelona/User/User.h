//
//  User.h
//  FCB
//
//  Created by Eissa on 8/26/14.
//  Copyright (c) 2014 Tawasolit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessObject.h"
#import "ValidationRule.h"
//#import "UserNameValidator.h"
@interface User : BusinessObject

@property (nonatomic, assign) int userId;
@property(strong,nonatomic) NSString * profilePic;
@property(strong,nonatomic) NSString * userName;
@property(strong,nonatomic) NSString * passWord;
@property(strong,nonatomic) NSString * email;
@property(strong,nonatomic) NSString * dateOfBirth;
@property(nonatomic) BOOL  gender;
@property(nonatomic) NSInteger  countryId;
@property(nonatomic) BOOL  isOnline;
@property(strong,nonatomic) NSString * lastLoginDate;

@end
