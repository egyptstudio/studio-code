//
//  BarcelonaAppDelegate.m
//  FC Barcelona
//
//  updated by mohamed metwaly
//  Created by Mohammed Alrassas on 8/26/14.
//  Copyright (c) 2016 Tawasol. All rights reserved.
//
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>
#import "InstagramLogin.h"
#import "TwitterLogin.h"
#import "BarcelonaAppDelegate.h"
#import "CurrentUser.h"
#import "SVProgressHUD.h"
#import "GetAndPushCashedData.h"
#import "MPNotificationView.h"
#import <objc/runtime.h>
#import "Fan.h"
#import "Message.h"
#define BG_TOP_IMG @"navigationbar.png"
#import "MessagingViewController.h"
#import "JSQSystemSoundPlayer.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "GAI.h"
#import "DataHelper.h"
#import "CachedImagesCleaner.h"
#import "StudioManager.h"
#import "ShopManager.h"
#import "SystemMessage.h"

@implementation BarcelonaAppDelegate
{
    int messageCounter;
}
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 300;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-62916651-1"];
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"Chatting"];
    [defaults synchronize];
    
    messageCounter = 0 ;
    _BaseURL = @"http://barca1.tawasoldev.com/barca/index.php/api/";
    
    [self copyDatabaseIfNeeded];
    //[self applicationDocumentsDirectory];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
#ifdef __IPHONE_8_0
        UIUserNotificationType types = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        UIUserNotificationSettings *mySettings =
        [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
#endif
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
    [FBLoginView class];
    [FBProfilePictureView class];
    [InstagramLogin class];
    [TwitterLogin class];
    

    UIImage *imageNav                       = [[UIImage imageNamed:BG_TOP_IMG] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    //    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearance] setBackgroundImage:imageNav forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    @try {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    }
    @catch (NSException *exception) {
        NSLog(@"Unable to set status bar text color to white!!");
    }
    

    if (launchOptions!=nil) {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        [self performSelector:@selector(openPushWithInfo:) withObject:userInfo afterDelay:4];
    }
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    [[CurrentUser getObject] getLibVer];
    
    [[CachedImagesCleaner sharedInstance] clearImagesAfterTimeInterval:3600];

    
//    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
//    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];

    [self addPoints];
    [self addPremium];
    return YES;
}
-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

#pragma mark completed Transactions call back
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    [self updateToken:[GetAndPushCashedData getObjectFromCash:@"deviceToken"]];
//    [[CurrentUser getObject] updateNotifications];
    [[CurrentUser getObject] updateUser];
    [[CurrentUser getObject] getLibVer];
}


-(void)openPushWithInfo:(NSDictionary *)userInfo{
    @try {
        switch ([userInfo[@"aps"][@"type"] intValue])
        {
            case 1:
            {
                if ([[CurrentUser getObject] isUser]) {
                    int messageId            = [userInfo[@"aps"][@"mID"] intValue];
                    NSString *messageText    = userInfo[@"aps"][@"alert"][@"body"];
                    int senderId             = [userInfo[@"aps"][@"sID"] intValue];
                    NSString *senderName     = userInfo[@"aps"][@"sName"];
                    NSString *senderPic      = userInfo[@"aps"][@"sPic"];
                    NSDate *sentAt           =[NSDate dateWithTimeIntervalSince1970:[userInfo[@"sAt"] longLongValue]];
                    
                    
                    
                    Fan *sender = [[Fan alloc] init];
                    sender.userId = senderId;
                    sender.fanName = senderName;
                    sender.fanPicture = senderPic;
                    
                    Message *message = [Message messageWithId:messageId message:messageText from:sender to:[[CurrentUser getObject] getUser] atTime:sentAt seen:NO];
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    if ([defaults boolForKey:@"Chatting"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewMessageNotification" object:self userInfo:@{@"Message":message}];
                    }
                    else {
                        [MPNotificationView notifyWithText:message.sender.userName
                                                    detail:message.message
                                             andTouchBlock:^(MPNotificationView *notificationView) {
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"openMessaging" object:sender userInfo:nil];
                                             }];
                    }
                }
                break;
            }
            case 2:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"emailVerified" object:nil];
                break;
            case 8:
                [GetAndPushCashedData removeObjectWithAction:@"StudioFolders"];
                [[SDImageCache sharedImageCache] cleanDisk];
                [[SDImageCache sharedImageCache] clearDisk];
                break;
            case 10:
                [[CurrentUser getObject] updateNotifications];
                break;
            case 100:
                [[[UIAlertView alloc] initWithTitle:nil message:@"Your Premium Package Is Expired" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil] show];
                break;
            case 7:
            {
                SystemMessage *message;
                message = [NSEntityDescription insertNewObjectForEntityForName:@"SystemMessage" inManagedObjectContext:__managedObjectContext];
                message.message = userInfo[@"aps"][@"alert"][@"body"];
                message.seen = 0;
                message.date = [NSDate date];
                break;
            }
             default:
                break;
        }
    }
    @catch (NSException *exception) {
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    @try {
        if ([[CurrentUser getObject] isUser]) {
            [[UsersManager getInstance] userIsNotActive:[CurrentUser getObject].getUser.userId and:^(id currentClient) {
                if (currentClient) {
                    if([currentClient[@"status"] intValue]==2)
                    {
                        [CurrentUser getObject].getUser.credit = [currentClient[@"credit"] intValue];
                        [[CurrentUser getObject] saveCurrentUser];
                    }
                }
            } onFailure:^(NSError *error) {
            }];
        }
    }
    @catch (NSException *exception) {}
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

    

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[CachedImagesCleaner sharedInstance] clearImagesAfterTimeInterval:3600];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"applicationDidBecomeActive" object:nil];
    [SVProgressHUD dismiss];
    
    @try {
        if ([[CurrentUser getObject] isUser]) {
            [[UsersManager getInstance] userIsActive:[CurrentUser getObject].getUser.userId and:^(id currentClient) {
                if (currentClient) {
                    if([currentClient[@"status"] intValue]==2)
                    {
                        [CurrentUser getObject].getUser.credit = [currentClient[@"credit"] intValue];
                        [[CurrentUser getObject] saveCurrentUser];
                    }
                }
            } onFailure:^(NSError *error) {
            }];
        }
    }
    @catch (NSException *exception) {}

}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self saveContext];
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString* newToken = [deviceToken description];
    newToken =[newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    [GetAndPushCashedData cashObject:newToken withAction:@"deviceToken"];
    [self updateToken:newToken];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{

        
    NSLog(@"userInfo %@",userInfo);
    UIApplicationState state = [application applicationState];
    
    if (state == UIApplicationStateActive)
    {
        @try {
            switch ([userInfo[@"aps"][@"type"] intValue])
            {
                case 1:
                {
                    if ([[CurrentUser getObject] isUser]) {
                        int messageId            = [userInfo[@"aps"][@"mID"] intValue];
                        NSString *messageText    = userInfo[@"aps"][@"alert"][@"body"];
                        int senderId             = [userInfo[@"aps"][@"sID"] intValue];
                        NSString *senderName     = userInfo[@"aps"][@"sName"];
                        NSString *senderPic      = userInfo[@"aps"][@"sPic"];
                        NSDate *sentAt           =[NSDate dateWithTimeIntervalSince1970:[userInfo[@"aps"][@"sAt"] longLongValue]];
                        
                        Fan *sender = [[Fan alloc] init];
                        sender.userId = senderId;
                        sender.fanName = senderName;
                        sender.fanPicture = senderPic;
                        
                        Message *message = [Message messageWithId:messageId message:messageText from:sender to:[[CurrentUser getObject] getUser] atTime:sentAt seen:YES];
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        if ([defaults boolForKey:@"Chatting"]) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewMessageNotification" object:self userInfo:@{@"Message":message}];
                        }
                        else {
                            [MPNotificationView notifyWithText:message.sender.userName
                                                        detail:message.message
                                                 andTouchBlock:^(MPNotificationView *notificationView) {
                                                     [[NSNotificationCenter defaultCenter] postNotificationName:@"openMessaging" object:sender userInfo:nil];
                                                 }];
                        }
                    }
                }
                break;
                case 2:
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"emailVerified" object:nil];
                    break;
                case 8:
                    [GetAndPushCashedData removeObjectWithAction:@"StudioFolders"];
                    [[SDImageCache sharedImageCache] cleanDisk];
                    [[SDImageCache sharedImageCache] clearDisk];
                    break;
                case 10:
                    [[CurrentUser getObject] updateNotifications];
                    break;
                case 100:
                    [[[UIAlertView alloc] initWithTitle:nil message:@"Your Premium Package Is Expired" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil] show];
                    break;
                case 7:
                {
                    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;

                    SystemMessage *message = (SystemMessage*)[NSEntityDescription insertNewObjectForEntityForName:@"SystemMessage" inManagedObjectContext:managedObjectContext];
                    
                    message.message = userInfo[@"aps"][@"alert"][@"body"];
                    message.seen = 0;
                    message.date = [NSDate date];
                    [MPNotificationView notifyWithText:@"FCB" andDetail:userInfo[@"aps"][@"alert"][@"body"]];
                }
                    break;
                default:
                    break;
            }
        }
        @catch (NSException *exception) {
        }
       
    }
    else  if (state == UIApplicationStateInactive)
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [self performSelector:@selector(openPushWithInfo:) withObject:userInfo afterDelay:4];
    }
}

- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication
         annotation: (id)annotation {
    return  [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation]|
            [[InstagramLogin getInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation]|
            [[TwitterLogin getInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation]|
           [FBAppCall handleOpenURL:url sourceApplication:sourceApplication] ;
}

- (void) copyDatabaseIfNeeded
{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"fcb-chat.sqlite"];
    //NSLog(@"Project Path %@",writableDBPath);
    //printf(writableDBPath);
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) return;
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fcb-chat.sqlite"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success)
    {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FCBModel" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FCBModel.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)updateToken:(NSString*)token{
    
    if (token) {
        if([RestServiceAgent internetAvailable] && [[CurrentUser getObject] isUser])
        {
            [[UsersManager getInstance] updateToken:[[CurrentUser getObject] getUser].userId
                                              token:token and:^(id currentClient) {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                  });
                                              }onFailure:^(NSError *error) {
                                              }];
        }
    }
}
/////////////
-(void)addPoints
{
    @try {
        if([RestServiceAgent internetAvailable])
        {
            [[StudioManager getInstance] updateUserCredit:[[[CurrentUser getObject] getUser] userId] creditValue:10000 transactionId:@"1111111"
            and:^(id currentClient) {
                NSLog(@"Points Added");
            }onFailure:^(NSError *error) {
            
            }];
            
        }
    }
    @catch (NSException *exception) {}
}
-(void)addPremium
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if([RestServiceAgent internetAvailable])
        {
            [[ShopManager getInstance] subscripeInPremium:[[[CurrentUser getObject] getUser] userId] subscriptionDuration:90 transactionId:@"1111111"
            and:^(id currentClient) {
                NSLog(@"Premium Added");

            }onFailure:^(NSError *error) {
            
            }];
            
        }
    }
    @catch (NSException *exception) {}
}


@end

