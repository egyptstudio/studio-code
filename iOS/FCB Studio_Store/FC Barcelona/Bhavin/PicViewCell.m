//
//  PicViewCell.m
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "PicViewCell.h"

#import "WebImage.h"
#import "fansManager.h"
#import "WallManager.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "CommentsViewController.h"
#import "UserPhoto.h"
#import "PostDetailsViewController.h"
#import "ShareManagerViewController.h"
#import "User.h"
#import "studioManager.h"
#import "CaptureViewController.h"
#import "ShareViewController.h"
#import "ShareLibrary.h"
#import "CurrentUser.h"
#import "ImageToZoomViewController.h"
#import "FullScreenViewController.h"
#import "LikesViewController.h"
#import "ImageDetailViewController.h"

#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@implementation PicViewCell
{
    Post * currentPost;
    NSUserDefaults * defaults;
    UIViewController *mainController;
    __weak IBOutlet UIView *topView;
    UIAlertView* deleteAlertView;
}

@synthesize premiumImage;
@synthesize postImage;
@synthesize userImage;
@synthesize userName;
@synthesize country;
@synthesize likesCount;
@synthesize commentsCount;
@synthesize useCount;
@synthesize shareCount;
@synthesize likeButton;
@synthesize bigLoader;
@synthesize smallLoader;
@synthesize ranks;
@synthesize isEditable;
@synthesize allPosts;
@synthesize walltype;
@synthesize postDate;
@synthesize lastRequestTime;
@synthesize timeFilter;
//@synthesize countriesArray;
//@synthesize favoritePostsFlag;

- (void)awakeFromNib
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:NO animated:animated];
}

-(void)initImages
{
    [postImage sd_setImageWithURL:[NSURL URLWithString:[currentPost postPhotoUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [bigLoader stopAnimating];
        [bigLoader setHidden:YES];
    }];
    
    
    [userImage sd_setImageWithURL:[NSURL URLWithString:[currentPost userProfilePicUrl]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [smallLoader stopAnimating];
        [smallLoader setHidden:YES];
    }];
    
}

-(void)initWithPost:(Post*)post andController:(UIViewController*) mainView
{
    allPosts = [[NSMutableArray alloc] init];
    mainController = mainView;
    currentPost = post;
    
    userImage.borderColor = [UIColor whiteColor];
    userImage.borderWidth = 1.0;
    
    userName.text = [currentPost userName];
    country.text = [currentPost userCountry];
    
    likesCount.text = [currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
    commentsCount.text = [currentPost commentCount]>999? [NSString stringWithFormat:@"%d k",[currentPost commentCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost commentCount]];

    shareCount.text = @"0";
    ranks.text = [NSString stringWithFormat:@"%d",[currentPost contestRank]];
    
    NSDate* theDate = [NSDate dateWithTimeIntervalSince1970:[currentPost creationTime]];
    postDate.text = [self relativeDateStringForDate:theDate];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(postFullScreen)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    [postImage addGestureRecognizer:tap];
    postImage.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(postLikesDetails)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    [likesCount addGestureRecognizer:tap1];
    likesCount.userInteractionEnabled = YES;
    
    if ([currentPost contestRank]>0 &&[currentPost contestRank]<=10) {
        [_rankView setHidden:NO];
    }
    //    for (int x=0; x<[currentPost.tags count]; x++) {
    //        [self appendSubView:[self prepareViewForTagWithText:[[[currentPost tags] objectAtIndex:x] objectForKey:@"tagName"] isServer:YES] onParentView:_tagsScroll ];
    //    }
    
    if ([[mainController.navigationController topViewController] isKindOfClass:[PostDetailsViewController class]]) {
        topView.hidden = !isEditable;
    }
    else
    {topView.hidden = YES;}
    
    if ([currentPost liked]) {
        [likeButton setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
    }
    else{
        [likeButton setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
    }
    
    [useCount setHidden:YES];
    
    if ([currentPost originalPostId]!=0 && [currentPost userId]!=[[[CurrentUser getObject] getUser] userId])
        [self addReposter];
    
    if ([currentPost userId]!=[[[CurrentUser getObject] getUser] userId])
    {
        CGSize textSize = [[currentPost userName] sizeWithFont:userName.font
                                             constrainedToSize:CGSizeMake(200, userName.frame.size.height) lineBreakMode: NSLineBreakByWordWrapping];
        
        UIButton * reposterState = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(userName.frame)+textSize.width,
                                                                              CGRectGetMinY(userName.frame),
                                                                              userName.frame.size.height,
                                                                              userName.frame.size.height)];
        [reposterState addTarget:self action:@selector(followUser:) forControlEvents:UIControlEventTouchUpInside];
        UIImage *state = [currentPost following] ? nil:[UIImage imageNamed:@"unfollowed.png"];
        [reposterState setBackgroundImage:state forState:UIControlStateNormal];
        [self addSubview:reposterState];
    }
    
    if ([currentPost likeCount] <= 0 ) {
        likesCount.text = @"";
    }
    
    if ([currentPost commentCount] <= 0 ) {
        commentsCount.text = @"";
    }
    
    if ([currentPost rePosted]) {
        [_repostButton setImage:[UIImage imageNamed:@"repost_sc.png"] forState:UIControlStateNormal];
    }
    else{
        [_repostButton setImage:[UIImage imageNamed:@"repost.png"] forState:UIControlStateNormal];
    }
}

-(void)addReposter
{
    UILabel* reposter = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(userName.frame),
                                                                  CGRectGetMaxY(userName.frame),
                                                                  userName.frame.size.width+10,
                                                                  userName.frame.size.height)];
    reposter.font = [UIFont systemFontOfSize:13];
    [reposter setTextColor:[UIColor blackColor]];
    reposter.text = [NSString stringWithFormat:@"%@, %@",[[Language sharedInstance] stringWithKey:@"reposted_by"], [currentPost rePostedBy]];
    [self addSubview:reposter];
}

-(void)postLikesDetails
{
//    if ([[CurrentUser getObject] isUser]) {
//        UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
//        
//        LikesViewController * likes = [sb instantiateViewControllerWithIdentifier:@"LikesViewController"];
//        likes.currentPost = currentPost;
//        [mainController.navigationController pushViewController:likes animated:YES];
//    }
//    else {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
//    }
}

-(void)postFullScreen
{
//    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
//
//    FullScreenViewController * fullScreen = [sb instantiateViewControllerWithIdentifier:@"FullScreenViewController"];
//    [fullScreen setCurrentPost:currentPost];
//    [fullScreen setAllPosts:allPosts];
//    [fullScreen setWallType:walltype];
//    [fullScreen setLastRequestTime:lastRequestTime];
//    [fullScreen setTimeFilter:timeFilter];
//    [fullScreen setCountriesArray:countriesArray];
//    [fullScreen setFavoritePostsFlag:favoritePostsFlag];
//    [fullScreen setSelectedPostIndex:self.tag];
//    [mainController.navigationController pushViewController:fullScreen animated:YES];
}

- (IBAction)like:(id)sender
{
//    if ([[CurrentUser getObject] isUser]) {
//        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//        if (![currentPost liked]) {
//            @try {
//                if ([RestServiceAgent internetAvailable]) {
//                    [[WallManager getInstance] likePost:[[CurrentUser getObject] getUser].userId postId:[currentPost postId]
//                                                    and:^(id currentClient) {
//                                                        dispatch_async(dispatch_get_main_queue(), ^{
//                                                            [SVProgressHUD dismiss];
//                                                            if (currentClient) {
//                                                                if([[currentClient objectForKey:@"status"] intValue] == 1)
//                                                                {
//                                                                    [sender setImage:[UIImage imageNamed:@"like_sc.png"] forState:UIControlStateNormal];
//                                                                    
//                                                                    currentPost.liked = YES;
//                                                                    currentPost.likeCount++;
//                                                                    likesCount.text =[currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
//                                                                }
//                                                            }
//                                                        });
//                                                    }onFailure:^(NSError *error) {
//                                                        dispatch_async(dispatch_get_main_queue(), ^{
//                                                            [SVProgressHUD dismiss];
//                                                            
//                                                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                                                            [alert show];
//                                                        });
//                                                    }];
//                }
//                else{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [SVProgressHUD dismiss];
//                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                        [alert show];
//                    });
//                    
//                }
//                
//            }
//            @catch (NSException *exception) {
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            }
//        }
//        else
//        {
//            @try {
//                if ([RestServiceAgent internetAvailable]) {
//                    [[WallManager getInstance] dislikePost:[[CurrentUser getObject] getUser].userId postId:[currentPost postId]
//                                                       and:^(id currentClient) {
//                                                           dispatch_async(dispatch_get_main_queue(), ^{
//                                                               [SVProgressHUD dismiss];
//                                                               if (currentClient) {
//                                                                   if([[currentClient objectForKey:@"status"] intValue] == 1)
//                                                                   {
//                                                                       [sender setImage:[UIImage imageNamed:@"like.png"] forState:UIControlStateNormal];
//                                                                       
//                                                                       currentPost.liked = NO;
//                                                                       currentPost.likeCount--;
//                                                                       likesCount.text =[currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
//                                                                       
//                                                                   }
//                                                               }
//                                                           });
//                                                       }onFailure:^(NSError *error) {
//                                                           dispatch_async(dispatch_get_main_queue(), ^{
//                                                               [SVProgressHUD dismiss];
//                                                               
//                                                               UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                                                               [alert show];
//                                                           });
//                                                       }];
//                }
//                else {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [SVProgressHUD dismiss];
//                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                        [alert show];
//                    });
//                    
//                }
//                
//            }
//            @catch (NSException *exception) {
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            }
//        }
//    }
//    else {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
//    }
}

- (IBAction)comment:(id)sender
{
//    if ([[CurrentUser getObject] isUser]) {
//        UIGraphicsBeginImageContext(self.window.bounds.size);
//        [self.window.layer renderInContext:UIGraphicsGetCurrentContext()];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        
//        UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
//
//        CommentsViewController * commentsView = [sb instantiateViewControllerWithIdentifier:@"CommentsViewController"];
//        commentsView.post = currentPost;
//        commentsView.userID  = [[[CurrentUser getObject] getUser] userId];
//        commentsView.userName = [currentPost userName];
//        commentsView.delegate = self;
//        commentsView.backGround = image;
//        [mainController.navigationController pushViewController:commentsView animated:YES];
//        
//    }
//    else {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
//    }
}

-(void)commentAdded
{
    currentPost.commentCount++;
    commentsCount.text = [currentPost commentCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost commentCount]];
}

- (IBAction)use:(id)sender
{
    
    [self rePost:sender];
}

- (IBAction)share:(id)sender
{
//    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
//
//    ImageDetailViewController * commentsView = [sb instantiateViewControllerWithIdentifier:@"ImageDetailViewController"];
//    commentsView.currentPost = currentPost;
//    //    [mainController presentViewController:commentsView animated:YES completion:nil];
//    [mainController.navigationController pushViewController:commentsView animated:YES];
}

-(UIView*)prepareViewForTagWithText:(NSString*)tagText isServer:(BOOL)isServer
{
    UIFont *font = [UIFont systemFontOfSize:16] ;
    CGSize stringBox = [tagText sizeWithFont:font];
    
    UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, stringBox.width, stringBox.height)];
    [tagLabel setFont:font];
    [tagLabel setTextAlignment:NSTextAlignmentLeft];
    [tagLabel setTextColor:[UIColor blackColor]];
    [tagLabel setBackgroundColor:[UIColor clearColor]];
    [tagLabel setText:tagText];
    
    UIView *tagView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, stringBox.width+20 , _tagsScroll.frame.size.height)];
    UIImage *tagBackground = [UIImage imageNamed:@"tag_empty_field.png"];
    
    tagLabel.center = CGPointMake(tagView.frame.size.width/2, _tagsScroll.frame.size.height/2);

    [tagView addSubview:tagLabel];
    
    UIGraphicsBeginImageContext(tagView.frame.size);
    [tagBackground drawInRect:tagView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    tagView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    return tagView;
}

-(void)appendSubView:(UIView*)subSiew onParentView:(UIScrollView*)parentView
{
    for (NSObject * obj in parentView.subviews) {
        if (![obj isKindOfClass:[UIImageView class]]) {
            [(UIView*)obj setFrame:CGRectMake([(UIView*)obj frame].origin.x+2+subSiew.frame.size.width, 0, [(UIView*)obj frame].size.width, [(UIView*)obj frame].size.height)];
        }
    }
    subSiew.tag = [parentView.subviews count]-1;
    subSiew.frame = CGRectMake(0, 0, subSiew.frame.size.width, subSiew.frame.size.height);
    [parentView addSubview:subSiew];
    parentView.contentSize = CGSizeMake(parentView.contentSize.width+subSiew.frame.size.width+2, 0);
}

- (IBAction)editBtnPressed:(id)sender
{
}

- (IBAction)deletBtnPressed:(id)sender
{
//    deleteAlertView=[[UIAlertView alloc] initWithTitle:@"Warning" message:@"are sure you want to delete the selected image" delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"],nil];
//    [deleteAlertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView isEqual:deleteAlertView]) {
        if (buttonIndex == 1) {
            [self deletePhoto];
        }}
}

-(void)deletePhoto
{
//    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//    @try {
//        if ([RestServiceAgent internetAvailable]) {
//            [[WallManager getInstance] deletePost:[currentPost postId] and:^(id currentClient) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    if([[currentClient objectForKey:@"status"] intValue] == 1)
//                    {
//                        [[[UIAlertView alloc] initWithTitle:nil message:@"Image Deleted Successfully" delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles:nil] show];
//                        [mainController.navigationController popViewControllerAnimated:YES];
//                        [[NSNotificationCenter defaultCenter] postNotificationName:@"imageDeleted" object:nil];
//                    }
//                });
//            }onFailure:^(NSError *error) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    
//                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                    [alert show];
//                });
//            }];
//        }
//        else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            });
//            
//        }
//        
//    }
//    @catch (NSException *exception) {
//        [SVProgressHUD dismiss];
//        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//        [alert show];
//    }
}

-(void)rePost:(id)sender
{
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSString *date = [dateFormat stringFromDate:[NSDate date] ];
//    
//    if([[CurrentUser getObject] isUser]){
//        if ([[CurrentUser getObject] getUser].userId != [currentPost userId]) {
//            if (![currentPost rePosted]) {
//                [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//                @try {
//                    if ([RestServiceAgent internetAvailable]) {
//                        [[WallManager getInstance] rePost:[currentPost postId] userId:[[CurrentUser getObject] getUser].userId date:date and:^(id currentClient) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [SVProgressHUD dismiss];
//                                if([[currentClient objectForKey:@"status"] intValue] == 1)
//                                {
//                                    [currentPost setRePosted:YES];
//                                    [currentPost setUseCount:[currentPost useCount]+1] ;
//                                    useCount.text = [currentPost useCount]>999? [NSString stringWithFormat:@"%d k",[currentPost useCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost useCount]];
//                                    
//                                    [_repostButton setImage:[UIImage imageNamed:@"repost_sc.png"] forState:UIControlStateNormal];
//                                    
//                                    [[[UIAlertView alloc] initWithTitle:nil message:@"Image Reposted Successfully" delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles:nil] show];
//                                }
//                            });
//                        }onFailure:^(NSError *error) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [SVProgressHUD dismiss];
//                                
//                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                                [alert show];
//                            });
//                        }];
//                    }
//                    else{
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [SVProgressHUD dismiss];
//                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                            [alert show];
//                        });
//                        
//                    }
//                    
//                }
//                @catch (NSException *exception) {
//                    [SVProgressHUD dismiss];
//                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                    [alert show];
//                }
//            }
//            else {
//                UIAlertView* error = [[UIAlertView alloc] initWithTitle:nil message:@"You have reposted this post before" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [error show];
//            }
//            
//        }
//        else {
//            UIAlertView* error = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot repost you own posts" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//            [error show];
//        }
//    }
//    else {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
//    }
}

-(UIImage*)cropImage:(UIImage*)image toRect:(CGRect)rect
{
    UIGraphicsBeginImageContextWithOptions(rect.size, false, [image scale]);
    [image drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];
    UIImage *cropped_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cropped_image;
}

-(void)followUser:(UIButton*)sender
{
//    if ([[CurrentUser getObject] isUser]) {
//        if (![currentPost following]) {
//            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//            @try {
//                if ([RestServiceAgent internetAvailable]) {
//                    [[FansManager getInstance] followFan:[[CurrentUser getObject] getUser].userId
//                                                   fanId:[currentPost userId]
//                                                     and:^(id currentClient) {
//                                                         dispatch_async(dispatch_get_main_queue(), ^{
//                                                             [SVProgressHUD dismiss];
//                                                             if (currentClient) {
//                                                                 if([[currentClient objectForKey:@"status"] intValue] == 1)
//                                                                 {
//                                                                     User *tempUser=[[CurrentUser getObject] getUser];
//                                                                     tempUser.followingCount=tempUser.followingCount-1;
//                                                                     [[CurrentUser getObject] setUser:tempUser];
//                                                                     [[CurrentUser getObject] saveCurrentUser];
//                                                                     [currentPost setFollowing:YES];
//                                                                     [sender setBackgroundImage:[UIImage imageNamed:@"followed.png" ] forState:UIControlStateNormal];
//                                                                 }
//                                                             }
//                                                         });
//                                                     }onFailure:^(NSError *error) {
//                                                         dispatch_async(dispatch_get_main_queue(), ^{
//                                                             [SVProgressHUD dismiss];
//                                                             UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                                                             [alert show];
//                                                         });
//                                                     }];
//                }
//                else{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [SVProgressHUD dismiss];
//                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                        [alert show];
//                    });
//                    
//                }
//            }
//            @catch (NSException *exception) {
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            }
//        }
//    }
//    else {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
//    }
}

- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateFormat stringFromDate:date];
    
    //    NSCalendarUnit units = NSDayCalendarUnit | NSWeekOfYearCalendarUnit |
    //    NSMonthCalendarUnit | NSYearCalendarUnit;
    //
    //    // if `date` is before "now" (i.e. in the past) then the components will be positive
    //    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
    //                                                                   fromDate:date
    //                                                                     toDate:[NSDate date]
    //                                                                    options:0];
    //
    //    if (components.year > 0) {
    //        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
    //    } else if (components.month > 0) {
    //        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
    //    } else if (components.weekOfYear > 0) {
    //        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
    //    } else if (components.day > 0) {
    //        if (components.day > 1) {
    //            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
    //        } else {
    //            return @"Yesterday";
    //        }
    //    } else {
    //        return @"Today";
    //    }
}

-(UIImage *) imageWithImage:(UIImage*)sourceImage scaledToWidth: (float) i_width {//method to scale image accordcing to width
    
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
