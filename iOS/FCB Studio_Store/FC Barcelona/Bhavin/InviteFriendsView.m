//
//  InviteFriendsView.m
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/5/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "InviteFriendsView.h"

#import <AddressBook/AddressBook.h>

#import "FansManager.h"
#import "UsersManager.h"
#import "CurrentUser.h"

#import "SVProgressHUD.h"
#import "RestServiceAgent.h"

@interface InviteFriendsView ()

@end

@implementation InviteFriendsView
{
    NSString* messagePlaceHolder;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _inviteText.delegate = self;
    
    CALayer * l2 = [self.messageView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    
    messagePlaceHolder = [[Language sharedInstance] stringWithKey:@"before_sharing"];
    _inviteText.text = messagePlaceHolder;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_titleLabel setText:[[Language sharedInstance] stringWithKey:@"smsInvite_title"]];
    [_inviteText setText:[[Language sharedInstance] stringWithKey:@"before_sharing"]];
    [_comingSoon setText:[[Language sharedInstance] stringWithKey:@"coming_soon"]];
    
    [_inviteButton setTitle:[[Language sharedInstance] stringWithKey:@"initialMessages_Invite"] forState:UIControlStateNormal];
    
    [_lbl_invite1 setText:[[Language sharedInstance] stringWithKey:@"Invite_1"]];
    [_lbl_invite2 setText:[[Language sharedInstance] stringWithKey:@"Invite_2"]];
    [_lbl_invite3 setText:[[Language sharedInstance] stringWithKey:@"Invite_3"]];
    
    CALayer * l = [self.inviteButton layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    CALayer * l1 = [self.messageView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (IBAction)share_Action:(id)sender
{
    
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
        @try {
            if ([RestServiceAgent internetAvailable]) {
                [[UsersManager getInstance] generateInviteToken:[[CurrentUser getObject] getUser].userId
                                                 invitationText:[_inviteText.text isEqualToString:[[Language sharedInstance] stringWithKey:@"before_sharing"]] ? @"" : _inviteText.text
                                                            and:^(id currentClient)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                         if ([currentClient[@"status"] integerValue] == 2) {
                             [self showIntentShareWithText:[currentClient objectForKey:@"data"]];
                         }
                     });
                 }
                                                      onFailure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                         [alert show];
                     });
                 }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:messagePlaceHolder]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = messagePlaceHolder;
        textView.textColor = [UIColor lightGrayColor];
    }
    [textView resignFirstResponder];
}

- (void)inviteFriendsWithMessage:(NSString*)textToShare
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[UsersManager getInstance] sendInviteInvitationText:[[CurrentUser getObject] getUser].userId invitationText:textToShare and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient[@"status"] integerValue] == 1) {
                        //[self showIntentShareWithText:textToShare];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

-(void)showIntentShareWithText:(NSString*)urlToShare{
    NSString *sharedLink = urlToShare;
    sharedLink = [sharedLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSArray *objectsToShare = @[@"",[NSURL URLWithString:urlToShare]];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToTencentWeibo];
    activityVC.excludedActivityTypes = excludeActivities;
    [self presentViewController:activityVC animated:YES completion:nil];
}
                               
#pragma mark - Actions
- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
