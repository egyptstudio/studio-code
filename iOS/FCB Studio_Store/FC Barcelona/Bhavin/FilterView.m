//
//  FilterView.m
//  FCB
//
//  Created by Bhavin Chitroda on 1/17/15.
//  Copyright (c) 2015 Bhavin Chitroda. All rights reserved.
//

#import "FilterView.h"

//#import "CountryListView.h"

#import "Fan.h"
#import "FansManager.h"

#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "WebImage.h"
#import "UIImageView+WebCache.h"
#import "CurrentUser.h"

#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define ICON_BACK_IMG               @"back.png"

#define SORT_BUTTON_IMG             @"fan_radio_btn.png"
#define SORT_BUTTON_SEL_IMG         @"fan_radio_btn_sc.png"

#define CHECK_BUTTON_IMG            @"cheackbtn.png"
#define CHECK_BUTTON_SEL_IMG        @"cheackbtn_sc.png"

@implementation FilterView

@synthesize FansFiltersEnum;
@synthesize FansSortingEnum;

@synthesize arrFansSuggestions;

@synthesize citiesFilter;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Filter Fans";
    
    arrFansSuggestions = [NSMutableArray array];
    arrCountries = [NSArray array];

    NSArray *keys = @[@"online",@"offline",@"male",@"female",@"followers",@"following",@"favoriteFriends"];
    NSArray *values = @[[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0]];
    
    FansFiltersEnum = [[NSMutableDictionary alloc] initWithObjects:values forKeys:keys];
    FansSortingEnum = 1;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countrySelectionTapped:)];
    [viewCountry addGestureRecognizer:tap];
    
    _tblFansSuggestions.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _txtSearchFans.text = (self.searchText == nil) ? @"" : self.searchText;
    
    [_loaderView setHidden:YES];
    
    UIStoryboard *sb = (!isPad) ? [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]];
    
    
    citiesFilter = [sb instantiateViewControllerWithIdentifier:@"CitiesFilterViewController"];
    
//    [self prepareImageForFilters];
}

- (void)viewWillAppear:(BOOL)animated
{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"PhotoFilter_title"]];
    [super viewWillAppear:animated];
    
//    if ([[[CurrentUser getObject] getUser] isOnline]) {
        self.lblOfflinePlaceHolder.hidden = YES;
        self.viewMain.hidden = NO;
//    }
//    else {
//        // Show offline placeholder.
//        self.lblOfflinePlaceHolder.hidden = NO;
//        self.viewMain.hidden = YES;
//    }
    
    /// Manar
    //[_lblOfflinePlaceHolder setText:[[Language sharedInstance] stringWithKey:@"noLocalizationTextExist"]];
    //[_txtSearchFans setPlaceholder:[[Language sharedInstance] stringWithKey:@"noLocalizationTextExist"]];//Search Fan Names
    [(UILabel *)[self.view viewWithTag:55] setText:[[Language sharedInstance] stringWithKey:@"PhotoFilter_Sortby"]];
    [(UILabel *)[self.view viewWithTag:56] setText:[[Language sharedInstance] stringWithKey:@"filter_by"]];
    [(UILabel *)[self.view viewWithTag:58] setText:[[Language sharedInstance] stringWithKey:@"Filter1_country"]];
    
    
    [(UIButton *)[self.view viewWithTag:57] setTitle:[[Language sharedInstance] stringWithKey:@"Filter1_Apply"] forState:UIControlStateNormal];
    
    [(UIButton *)[self.view viewWithTag:101] setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_sortOption1"] forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:102] setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_sortOption2"] forState:UIControlStateNormal];
    
    [(UIButton *)[self.view viewWithTag:201] setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_Online"] forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:203] setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_Offline"] forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:205] setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_Male"] forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:207] setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_Female"] forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:209] setTitle:[[Language sharedInstance] stringWithKey:@"fan_filter_followers_only"] forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:211] setTitle:[[Language sharedInstance] stringWithKey:@"fan_filter_following_only"] forState:UIControlStateNormal];
    [(UIButton *)[self.view viewWithTag:213] setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_FavoritesFriendsOnly"] forState:UIControlStateNormal];
    /////////
}

- (IBAction)popBack:(id)sender
{
    [self.view endEditing:YES];
    NSArray *keys = @[@"online",@"offline",@"male",@"female",@"followers",@"following",@"favoriteFriends"];
    NSArray *values = @[[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0]];
    [self.delegateFilter applyFilterWithOptions:[[NSMutableDictionary alloc] initWithObjects:values forKeys:keys] sortingOption:1 countries:@[] andSearchText:@""];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)applyButtonTapped:(id)sender
{
//    if ([[_txtSearchFans.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]
//        && [arrCountries count]<=0
//        &&) {
//        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"no_Filter"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//        [alert show];
//    }
//    else{
        [self.delegateFilter applyFilterWithOptions:FansFiltersEnum sortingOption:FansSortingEnum countries:arrCountries andSearchText:_txtSearchFans.text];
        [self.navigationController popViewControllerAnimated:YES];
//    }
    
   
}

- (void)didSelectCountries:(NSMutableArray *)array
{
    _numberOfSelectedCountries.text = [NSString stringWithFormat:@"%i Selected", (int)[array count]];
    arrCountries = array;
}

- (IBAction)countrySelectionTapped:(id)sender
{
//    if (!isPad) {
    citiesFilter.delegate = self;
    [self.navigationController pushViewController:citiesFilter animated:YES];
//    }
}

- (IBAction)sortSelectionTapped:(id)sender
{
    UIButton *btnSel = ((UIButton *)sender);
    UIButton *btnOther = nil;
    if (btnSel.tag == 101) {
        btnOther = (UIButton *)[self.view viewWithTag:102];
        FansSortingEnum = 1;
    }
    else {
        btnOther = (UIButton *)[self.view viewWithTag:101];
        FansSortingEnum = 2;
    }
    
    [btnOther setImage:[UIImage imageNamed:SORT_BUTTON_IMG] forState:UIControlStateNormal];
    [btnSel setImage:[UIImage imageNamed:SORT_BUTTON_SEL_IMG] forState:UIControlStateNormal];
}

- (IBAction)filterSelectionTapped:(id)sender
{
    UIButton *btnSel = ((UIButton *)sender);
    if ([btnSel.currentImage isEqual:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG]]) {
        [btnSel setImage:[UIImage imageNamed:CHECK_BUTTON_IMG] forState:UIControlStateNormal];
        NSString *key = [self filterKeyFromSelection:btnSel.tag];
        [FansFiltersEnum setObject:[NSNumber numberWithInt:0] forKey:key];
    }
    else {
        [btnSel setImage:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG] forState:UIControlStateNormal];
        NSString *key = [self filterKeyFromSelection:btnSel.tag];
        [FansFiltersEnum setObject:[NSNumber numberWithInt:1] forKey:key];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSInteger length = [searchText length];
    if (length > 3) {
        [self getSuggestionsFansWithPrefix:searchText];
    }
    else {
        [_tblFansSuggestions setHidden:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [_tblFansSuggestions setHidden:YES];
    return YES;
}


- (void)getSuggestionsFansWithPrefix:(NSString *)prefix
{
    [_loaderView setHidden:NO];
    [_loaderView startAnimating];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] getSuggestionsFans:prefix and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{

                    [_loaderView setHidden:YES];
                    [_loaderView stopAnimating];
                    if (currentClient) {
                        NSLog(@"currentClient :: %@",currentClient);
                        
                        arrFansSuggestions= [NSMutableArray array];

                        NSArray *fanList = ([currentClient objectForKey:@"fansList"] == nil) ? @[] : [currentClient objectForKey:@"fansList"];
                        NSLog(@"fansList :: %@",fanList);
                        
                        if (fanList != nil && [fanList count] > 0) {
                            [arrFansSuggestions addObjectsFromArray:fanList];
                            
                            [_tblFansSuggestions setHidden:NO];
                            [_tblFansSuggestions reloadData];
                        }
                        else {
                            [_tblFansSuggestions setHidden:YES];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                [_loaderView setHidden:YES];
                [_loaderView stopAnimating];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            [_loaderView setHidden:YES];
            [_loaderView stopAnimating];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [_loaderView setHidden:YES];
        [_loaderView stopAnimating];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark - Utilities

- (NSString *)filterKeyFromSelection:(NSInteger)tag
{
    switch (tag) {
        case 201:
            return @"online";
        case 203:
            return @"offline";
        case 205:
            return @"male";
        case 207:
            return @"female";
        case 209:
            return @"followers";
        case 211:
            return @"following";
        case 213:
            return @"favoriteFriends";
            
        default:
            return @"unknown";
    }
}

- (void)prepareImageForFilters
{
    UIButton *btnSort1 = (UIButton *)[self.view viewWithTag:101];
    (FansSortingEnum != 1) ? [btnSort1 setImage:[UIImage imageNamed:SORT_BUTTON_IMG] forState:UIControlStateNormal] : [btnSort1 setImage:[UIImage imageNamed:SORT_BUTTON_SEL_IMG] forState:UIControlStateNormal];
    
    UIButton *btnSort2 = (UIButton *)[self.view viewWithTag:102];
    (FansSortingEnum != 2) ? [btnSort2 setImage:[UIImage imageNamed:SORT_BUTTON_IMG] forState:UIControlStateNormal] : [btnSort2 setImage:[UIImage imageNamed:SORT_BUTTON_SEL_IMG] forState:UIControlStateNormal];
    
    UIButton *btnOnline = (UIButton *)[self.view viewWithTag:201];
    ([FansFiltersEnum[@"online"] intValue] == 0) ? [btnOnline setImage:[UIImage imageNamed:CHECK_BUTTON_IMG] forState:UIControlStateNormal] : [btnOnline setImage:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG] forState:UIControlStateNormal];
    
    UIButton *btnOffline = (UIButton *)[self.view viewWithTag:203];
    ([FansFiltersEnum[@"offline"] intValue] == 0) ? [btnOffline setImage:[UIImage imageNamed:CHECK_BUTTON_IMG] forState:UIControlStateNormal] : [btnOffline setImage:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG] forState:UIControlStateNormal];
    
    UIButton *btnMale = (UIButton *)[self.view viewWithTag:205];
    ([FansFiltersEnum[@"male"] intValue] == 0) ? [btnMale setImage:[UIImage imageNamed:CHECK_BUTTON_IMG] forState:UIControlStateNormal] : [btnMale setImage:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG] forState:UIControlStateNormal];
    
    UIButton *btnFemale = (UIButton *)[self.view viewWithTag:207];
    ([FansFiltersEnum[@"female"] intValue] == 0) ? [btnFemale setImage:[UIImage imageNamed:CHECK_BUTTON_IMG] forState:UIControlStateNormal] : [btnFemale setImage:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG] forState:UIControlStateNormal];
    
    UIButton *btnFollowers = (UIButton *)[self.view viewWithTag:209];
    ([FansFiltersEnum[@"followers"] intValue] == 0) ? [btnFollowers setImage:[UIImage imageNamed:CHECK_BUTTON_IMG] forState:UIControlStateNormal] : [btnFollowers setImage:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG] forState:UIControlStateNormal];
    
    UIButton *btnFollowing = (UIButton *)[self.view viewWithTag:211];
    ([FansFiltersEnum[@"following"] intValue] == 0) ? [btnFollowing setImage:[UIImage imageNamed:CHECK_BUTTON_IMG] forState:UIControlStateNormal] : [btnFollowing setImage:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG] forState:UIControlStateNormal];
    
    UIButton *btnFavFriends = (UIButton *)[self.view viewWithTag:213];
    ([FansFiltersEnum[@"favoriteFriends"] intValue] == 0) ? [btnFavFriends setImage:[UIImage imageNamed:CHECK_BUTTON_IMG] forState:UIControlStateNormal] : [btnFavFriends setImage:[UIImage imageNamed:CHECK_BUTTON_SEL_IMG] forState:UIControlStateNormal];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.tblFansSuggestions) {
        [self.view endEditing:YES];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrFansSuggestions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *myIdentifier = @"cellFanList";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIdentifier];
    }
    
    Fan *currentFan = (Fan*)[arrFansSuggestions objectAtIndex:indexPath.row];

    UILabel *lbl = (UILabel *)[cell viewWithTag:2];
    lbl.text = (currentFan.fanName == nil) ? @"" : currentFan.fanName;
    
    UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:3];
    mainImage.image =  [UIImage imageNamed:@"avatar"];
    
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:1] ;
    [loader startAnimating];
    
    
    [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentFan fanPicture]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Fan *currentFan = (Fan*)[arrFansSuggestions objectAtIndex:indexPath.row];
    _txtSearchFans.text = currentFan.fanName;
    
    _tblFansSuggestions.hidden = YES;
    
    [self.view endEditing:YES];
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSInteger length = [currentString length];
    if (length > 3) {
        [self getSuggestionsFansWithPrefix:currentString];
    }
    else {
        [_tblFansSuggestions setHidden:YES];
        return YES;
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
@end
