//
//  PicsListView.m
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "PicsListView.h"

#import "CurrentUser.h"
#import "User.h"
#import "Fan.h"
#import "Friend.h"
#import "PostCell.h"

#import "FansManager.h"
#import "WallManager.h"

#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "WebImage.h"

#import "PicViewCell.h"

#import "FullScreenViewController.h"

#import "UserComment.h"
#import "CommentsViewController.h"
#import "Post.h"

#import "UserPhoto.h"

#import "PostDetailsViewController.h"
#import "ShareManagerViewController.h"

#import "GetAndPushCashedData.h"

#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface PicsListView ()
{
    NSString *lastRequestTime;
    NSString *timeFilter;
    
    NSMutableArray * arrPosts;
    
    CGFloat lastContentOffset;
    BOOL isWall;
    
    NSMutableArray* postsArray;
    
    ShareManagerViewController * shareView;
    CommentsViewController * commentsView;
    
    UIRefreshControl * refreshControl;
    int postsNo;

    NSMutableArray* countriesArray;
    BOOL favoritePostsFlag;
    BOOL isFrefreshing;
}

@end

@implementation PicsListView

-(void)scrollTopTop {
    @try {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
    @catch (NSException *exception) {
        lastRequestTime = @"";
        timeFilter = @"0";
        [self getPics];
    }
}

- (void)viewDidLoad
{
    lastRequestTime = @"";
    timeFilter = @"0";
    [super viewDidLoad];

    arrPosts = [NSMutableArray array];
    self.tableView.hidden = YES;
    
    self.lblTitle.text = self.viewTitle;
    
    postsNo = 0;
    countriesArray = [[NSMutableArray alloc] init];
    postsArray = [[NSMutableArray alloc] init];
    
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    self.tableView.tableFooterView = footerView;
    [self.tableView setSeparatorStyle:(UITableViewCellSeparatorStyleNone)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshPosts)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

-(void)viewWillAppear:(BOOL)animated
{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"Fan_Pics"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    postsNo = postsNo > 0 ? postsNo : 10;
    [self getPics];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)showEmptyPhotosViewWithMessage:(NSString*)msg
{
    if(![arrPosts count] > 0)
    {
        [self.noPhotosLabel setText:msg];
        [self.noPhotosView setHidden:NO];
        [self.tableView setHidden:YES];
    }
}

-(void)hideEmptyPhotosView
{
    [self.noPhotosView setHidden:YES];
    [self.tableView setHidden:NO];
}

-(void)refreshPosts
{
    isFrefreshing = YES;
    timeFilter = @"1";
    lastRequestTime = [arrPosts count]==0 ?@"" :
    [NSString stringWithFormat:@"%ld",[(Post*)[arrPosts firstObject]  lastUpdateTime]];
    postsNo = 10;
    [self getPics];
}

- (void)getPics
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance] getUserPosts:[[CurrentUser getObject] getUser].userId fanId:self.fanID postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [refreshControl endRefreshing];

                    if ([currentClient count]>0) {
                        if (isFrefreshing) {
                            for (int i = 0 ; i < [currentClient count]; i++) {
                                [arrPosts insertObject:currentClient[i] atIndex:0];
                            }
                        }else{
                            [arrPosts addObjectsFromArray:currentClient] ;
                        }
                        [self prepareAndCacheSectionWithName:@"Gallery"];
                        [_tableView reloadData];
                        [self hideEmptyPhotosView];
                    }
                    else{
                        [self showEmptyPhotosViewWithMessage:[[Language sharedInstance] stringWithKey:@"filterResult_NoResults"]];
                        postsNo-=10;
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [refreshControl endRefreshing];

                    [arrPosts addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Gallery"]];
                    postsNo = [arrPosts count];
                    [_tableView reloadData];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    postsNo-=10;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [refreshControl endRefreshing];

                [arrPosts addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Gallery"]];
                postsNo = [arrPosts count];
                [_tableView reloadData];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        [refreshControl endRefreshing];

        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
    
//    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//    @try {
//        if ([RestServiceAgent internetAvailable]) {
//            [[WallManager getInstance]getMyPosts:self.fanID postsNo:10 lastRequestTime:@"" timeFilter:@"0" and:^(id currentClient) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    if ([currentClient count] > 0) {
//                        
//                        NSLog(@"currentClient :: %@",currentClient);
//                        NSLog(@"currentRequestTime :: %@",[[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] objectForKey:@"currentRequestTime"]);
//                        [arrPosts addObjectsFromArray:currentClient];
//                        [self.tableView reloadData];
//
//                        [self hideEmptyPhotosView];
//                    }
//                    else {
//                        [self showEmptyPhotosViewWithMessage:@"No photos found,          please try another section"];
////                        postsNo-=10;
//                    }
//                });
//            }onFailure:^(NSError *error) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                    [alert show];
//                });
//            }];
//        }
//        else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            });
//        }
//        
//    }
//    @catch (NSException *exception) {
//        [SVProgressHUD dismiss];
//        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//        [alert show];
//    }
}

-(void)prepareAndCacheSectionWithName:(NSString*)section{
    for (int i = 0 ; i < [arrPosts count]; i++) {
        NSMutableArray * allPhotosTags = [[NSMutableArray alloc] init];
        for (int j = 0 ; j < [[(Post*)arrPosts[i] tags] count]; j++) {
            if (![[(Post*)arrPosts[i] tags][j] isKindOfClass:[StudioPhotoTag class]]) {
                NSDictionary* dic = [[NSDictionary alloc] initWithDictionary:[(Post*)arrPosts[i] tags][j]];
                StudioPhotoTag* tag = [[StudioPhotoTag alloc] init];
                tag.tagName =  dic[@"tagName"];
                tag.tagId   = [dic[@"tagId"] intValue];
                tag.tagType = [dic[@"tagType"]intValue];
                [allPhotosTags addObject:tag];
            }
        }
        [(Post*)arrPosts[i] setTags:allPhotosTags];
    }
    [GetAndPushCashedData cashObject:arrPosts withAction:section];
}

- (IBAction)popBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height + 6) {
        if ([arrPosts count] >= 6)
        {
            isFrefreshing = NO;
            timeFilter = @"0";
            lastRequestTime = [arrPosts count]==0 ?@"" :
            [NSString stringWithFormat:@"%ld",[(Post*)[arrPosts lastObject]  lastUpdateTime]];
            postsNo =[arrPosts count]==0 ? 10 : [arrPosts count]+10 ;
            [self getPics];
        }
        return;
    }else{
        lastContentOffset = scrollView.contentOffset.y;
    }
}

#pragma mark - UITableView Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrPosts count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PicViewCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PicViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        Post *currentPost = (Post*)[arrPosts objectAtIndex:indexPath.row];
        cell.isEditable = YES;
        [cell initWithPost:currentPost andController:self];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setTag:indexPath.row];
        [cell setAllPosts:arrPosts];
        [cell setWalltype:3];
        [cell setLastRequestTime:lastRequestTime];
        [cell setTimeFilter:timeFilter];
//        [cell setCountriesArray:countriesArray];
//        [cell setFavoritePostsFlag:favoritePostsFlag];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [(PicViewCell*)cell performSelectorInBackground:@selector(initImages) withObject:nil];
}

@end
