//
//  FansCollectionView.h
//  FCB
//
//  Created by Bhavin Chitroda on 1/19/15.
//  Copyright (c) 2015 Bhavin Chitroda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FansCollectionView : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>
{
    IBOutlet UILabel *lblTitle;
    int pageNumber;
    
    BOOL loaderBefore;
}
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UIView *adBanner;

@property (strong, nonatomic) NSString *viewTitle;
@property (assign, nonatomic) int viewCount;

@property (nonatomic, assign) int fanID;
@property (nonatomic, assign) BOOL isFollowers;

@property (nonatomic, strong) NSMutableArray *arrFans;

@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;

@property (strong,  nonatomic) IBOutlet UILabel *lblOfflinePlaceHolder;
@property (strong, nonatomic) IBOutlet UILabel *lblFollowersPlaceHolder;

- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;
@end
