//
//  PicsDetailView.m
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "PicsDetailView.h"

#import "FullScreenImageViewController.h"

@interface PicsDetailView ()
{
    NSMutableArray* userPostsList;

    Post *post;
    int pageNumber;
    
    RGMPagingScrollView *horizontalScroll;
    UIRefreshControl * refreshControl;
    
    UIViewController* mainController;
    AvatarImageView *userImage;
    
    UILabel *userName;
    UIView *rankView;
    UILabel *likesCount;
    UIButton *likeButton;
    
    UIActivityIndicatorView *smallLoader;
    UILabel *ranks;
    
    NSUInteger _numPages;
    int postsNo;
    int postsNo2;
    
//    Post* _currentPost ;
    NSMutableArray *allPostsPhotos;
    int currentPage;
    BOOL isRefreshing;
}

@end

@implementation PicsDetailView

@synthesize allPosts;
@synthesize currentPost;
@synthesize wallType;
@synthesize lastRequestTime;
@synthesize timeFilter;
@synthesize countriesArray;
@synthesize favoritePostsFlag;
@synthesize selectedPostIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userPostsList = [[NSMutableArray alloc] init];
    allPostsPhotos = [[NSMutableArray alloc] init];
    
    _scrollView.scrollDirection = RGMScrollDirectionVertical;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshPosts)
             forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationChanged:)
                                                name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
    [self performSelector:@selector(checkOrientation) withObject:nil afterDelay:.7];
    
    [_scrollView reloadData];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [_scrollView setCurrentPage:selectedPostIndex];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"Fan_Pics"]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getPosts
{
    
}

-(void)refreshPosts
{
    timeFilter = @"1";
    lastRequestTime = [allPosts count] == 0 ? @"" :
    [NSString stringWithFormat:@"%lu",[(Post*)[allPosts firstObject] lastUpdateTime]];
    [self getPosts];
}

-(void)checkOrientation
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation != UIInterfaceOrientationPortrait )
    {
        UIImageView* image   = (UIImageView*)[[(RGMPagingScrollView*)_scrollView.subviews[0] subviews][0] viewWithTag:1];
        FullScreenImageViewController * fullScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"FullScreenImageViewController"];
        fullScreen.orienaiton = UIImageOrientationRight;
        fullScreen.image = image.image;
        [self.navigationController pushViewController:fullScreen animated:NO];
    }
}

-(void) orientationChanged:(NSNotification *)note
{
    if (![self.navigationController.topViewController isKindOfClass:[FullScreenImageViewController class]]) {
        UIDevice * device = note.object;
        switch(device.orientation)
        {
            case UIDeviceOrientationLandscapeLeft :
            {
                UIImageView* image   = (UIImageView*)[[(RGMPagingScrollView*)_scrollView.subviews[0] subviews][0] viewWithTag:1];
                FullScreenImageViewController * fullScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"FullScreenImageViewController"];
                fullScreen.orienaiton = UIImageOrientationRight;
                fullScreen.image = image.image;
                [self.navigationController pushViewController:fullScreen animated:NO];
                break;
            }
            case UIDeviceOrientationLandscapeRight :
            {
                UIImageView* image   = (UIImageView*)[[(RGMPagingScrollView*)_scrollView.subviews[0] subviews][0] viewWithTag:1];
                FullScreenImageViewController * fullScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"FullScreenImageViewController"];
                fullScreen.orienaiton = UIImageOrientationLeft;
                fullScreen.image = image.image;
                [self.navigationController pushViewController:fullScreen animated:NO];
                break;
            }
                break;
            default:
                break;
        };
    }
}

//-(NSInteger)pagingScrollViewNumberOfPages:(RGMPagingScrollView *)pagingScrollView
//{
//    return pagingScrollView ==_scrollView ? wallType==3 ? 1 : [allPosts count] : _numPages;
//}

@end
