//
//  FriendsView.m
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/2/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "FriendsView.h"

#import "CurrentUser.h"
#import "User.h"
#import "Fan.h"
#import "Friend.h"
#import "FansManager.h"

#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "WebImage.h"

#import "FansDetailView.h"

#import "InviteFriendsViewController.h"
#import "InviteFriendsView.h"

#import "FMDBHelper.h"

#import <AddressBook/AddressBook.h>

#import <GoogleMobileAds/DFPBannerView.h>
#import <GoogleMobileAds/DFPRequest.h>

#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define SYNCED_CONTACT_BEFORE       @"syncedContactBefore"
#define LAST_CONTACT_SYNC           @"lastTimeContactsSynced"

#define CURRENT_TIME_STAMP          [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]

@interface FriendsView ()
{
    BOOL isUserOnline;
    BOOL isSuggestion;
    BOOL loaderBefore;
    BOOL isSearch;
    int pageNumber;
    NSMutableArray* searchResult;
}

@end

@implementation FriendsView

@synthesize arrFriends;
@synthesize arrRequests;
@synthesize arrSuggestions;

@synthesize refreshControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    searchResult = [[NSMutableArray alloc] init];
    //////////////////////
    [_segmentsView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:84/255.0f blue:164/255.0f alpha:1]];
    [_segmentedControl setFrame:_segmentsView.bounds];
    
    UIFont *font = [UIFont systemFontOfSize:12.0f];
    UIFont *fontBold = [UIFont boldSystemFontOfSize:12.0f];
    
    UIColor *color = [UIColor whiteColor];
    UIColor *colorDisabled = [UIColor darkGrayColor];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesBold = [NSDictionary dictionaryWithObjectsAndKeys:
                                    fontBold,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesDisabled = [NSDictionary dictionaryWithObjectsAndKeys:
                                        font,NSFontAttributeName,colorDisabled,NSForegroundColorAttributeName,nil];
    
    [_segmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [_segmentedControl setTitleTextAttributes:attributesBold forState:UIControlStateSelected];
    [_segmentedControl setTitleTextAttributes:attributesDisabled forState:UIControlStateDisabled];
    
    [_segmentedControl setBackgroundImage:[UIImage imageNamed:@"tab_normal.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segmentedControl setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"]
                         forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    UIView *devider = [[UIView alloc] initWithFrame:CGRectMake(0, 0, .5, _segmentedControl.frame.size.height)];
    [devider setBackgroundColor:[UIColor whiteColor]];
    UIGraphicsBeginImageContextWithOptions(devider.bounds.size, devider.opaque, 0.0);
    [devider.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * deviderImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [_segmentedControl setDividerImage:deviderImg forLeftSegmentState:UIControlStateSelected
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segmentedControl setDividerImage:deviderImg forLeftSegmentState:UIControlStateNormal
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    //////////////////
    
    arrFriends = [NSMutableArray array];
    arrRequests = [NSMutableArray array];
    
    pageNumber = 0;
    
    isUserOnline = [[[CurrentUser getObject] getUser] isOnline];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self action:@selector(pullToRefreshSuggestions) forControlEvents:UIControlEventValueChanged];
    
    self.cvFriends.alwaysBounceVertical = YES;
    
    isSearch = NO;
    [self addAdBanner];

}

- (void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Friends Screen";

    isSearch = NO;
    self.searchView.hidden=YES;
    _theSearchBar.text = @"";

    [_cvFriends reloadData];
    [_segmentedControl setTitle:[[Language sharedInstance] stringWithKey:@"Messages_Friends"] forSegmentAtIndex:0];
    [_segmentedControl setTitle:[[Language sharedInstance] stringWithKey:@"request"] forSegmentAtIndex:1];
    [_segmentedControl setTitle:[[Language sharedInstance] stringWithKey:@"suggestion"] forSegmentAtIndex:2];
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"menu_Friends"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_comingSoon setText:[[Language sharedInstance] stringWithKey:@"coming_soon"]];

    /// Manar
    [_btnInvite setTitle:[[Language sharedInstance] stringWithKey:@"initialMessages_Invite"] forState:UIControlStateNormal];

    [(UIButton *)[self.view viewWithTag:56] setTitle:[[Language sharedInstance] stringWithKey:@"InviteFriends_facebook"] forState:UIControlStateNormal]; // Facebook btn
    [(UIButton *)[self.view viewWithTag:57] setTitle:[[Language sharedInstance] stringWithKey:@"InviteFriends_Twitter"] forState:UIControlStateNormal]; // Twitter btn
    [(UIButton *)[self.view viewWithTag:58] setTitle:[[Language sharedInstance] stringWithKey:@"InviteFriends_SMS"] forState:UIControlStateNormal]; // SMS btn
    [(UIButton *)[self.view viewWithTag:55] setTitle:[[Language sharedInstance] stringWithKey:@"TWShare_Cancel"] forState:UIControlStateNormal]; // Cancel btn
    
    [_lblRequestPlaceHolder setText:[[Language sharedInstance] stringWithKey:@"NoFriendRequests"]];
    
    
    [_lblFriendsPlaceHolder setText:[[Language sharedInstance] stringWithKey:@"initialFriends_noFriendsYet"]]; //You don't have any Friends, start inviting friends to share the fun.
    

    
    [super viewWillAppear:animated];
    
    
//    if (!loaderBefore) {
//        [self loadFriends];
//        loaderBefore = YES;
//    }
    [self segmentValueChanged:_segmentedControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)prepareFriendsFromDB:(NSArray *)dataDB
{
    NSMutableArray *arrParsed = [NSMutableArray array];
    for (NSDictionary *dict in dataDB) {
        
        Friend *friend = [[Friend alloc] init];
        friend.friendID = (dict[@"userId"] == nil) ? 0 : [dict[@"userId"] intValue];
        friend.friendName = (dict[@"userName"] == nil) ? @"" : dict[@"userName"];
        friend.friendPicture = (dict[@"profilePic"] == nil) ? @"" : dict[@"profilePic"];
        friend.friendOnline = (dict[@"online"] == nil) ? FALSE : [dict[@"online"] boolValue];
        friend.isFavorite = (dict[@"isFavorite"] == nil) ? FALSE : [dict[@"isFavorite"] boolValue];
        friend.isPremium = (dict[@"premium"] == nil) ? FALSE : [dict[@"premium"] boolValue];
        friend.friendPicNumber = (dict[@"posts"] == nil) ? 0 : [dict[@"posts"] intValue];
        friend.isFollowed = (dict[@"isFollowing"] == nil) ? FALSE : [dict[@"isFollowing"] boolValue];
        
        [arrParsed addObject:friend];
    }
    return arrParsed;
}


#pragma mark - Actions

- (IBAction)openMenu:(id)sender
{
    if (!isPad) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openMenu" object:nil];
    }
}

-(void)authorizeAddressBook
{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted)
    {
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted) {
                self.lblOfflinePlaceHolder.hidden = YES;
                self.lblRequestPlaceHolder.hidden = YES;
                self.lblFriendsPlaceHolder.hidden = YES;
                self.lblSuggestionsPlaceHolder.hidden = NO;
                return;
            }
            else {
                self.lblOfflinePlaceHolder.hidden = YES;
                self.lblRequestPlaceHolder.hidden = YES;
                self.lblFriendsPlaceHolder.hidden = YES;
                self.lblSuggestionsPlaceHolder.hidden = YES;

//                if (isUserOnline) {
                    [self getContactsFromAddressBook];
//                }
//                else {
//                    
//                }
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        self.lblOfflinePlaceHolder.hidden = YES;
        self.lblRequestPlaceHolder.hidden = YES;
        self.lblFriendsPlaceHolder.hidden = YES;
        self.lblSuggestionsPlaceHolder.hidden = YES;

//        if (isUserOnline) {
            [self getContactsFromAddressBook];
//        }
//        else {
//            
//        }
    }
    else{
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (granted) {
                self.lblOfflinePlaceHolder.hidden = YES;
                self.lblRequestPlaceHolder.hidden = YES;
                self.lblFriendsPlaceHolder.hidden = YES;
                self.lblSuggestionsPlaceHolder.hidden = YES;

//                if (isUserOnline) {
                    [self getContactsFromAddressBook];
//                }
//                else {
//                    
//                }
            }
            else {
                self.lblOfflinePlaceHolder.hidden = YES;
                self.lblRequestPlaceHolder.hidden = YES;
                self.lblFriendsPlaceHolder.hidden = YES;
                self.lblSuggestionsPlaceHolder.hidden = NO;
                return;
            }
        });
    }
}

- (void)gotoDetailWithFan:(int)fanID
{
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
    fansDetailView.fanID = fanID;
    fansDetailView.isFriendDetail = YES;
    [self.navigationController pushViewController:fansDetailView animated:YES];
}

- (void)pullToRefreshSuggestions
{
    NSLog(@"Pull to Refresh");
//    if (isUserOnline) {
        self.lblOfflinePlaceHolder.hidden = YES;
        self.lblRequestPlaceHolder.hidden = YES;
        self.lblFriendsPlaceHolder.hidden = YES;
        self.lblSuggestionsPlaceHolder.hidden = NO;

        [self authorizeAddressBook];
//    }
//    else {
//        self.lblOfflinePlaceHolder.hidden = NO;
//        self.lblRequestPlaceHolder.hidden = YES;
//        self.lblFriendsPlaceHolder.hidden = YES;
//        self.lblSuggestionsPlaceHolder.hidden = YES;
//    }
}

#pragma mark - WS methods

// Get the contacts.
- (void)getContactsFromAddressBook
{
    self.contactList = [[NSMutableArray alloc] init];

    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, nil);
    
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (int i=0;i < nPeople;i++) {
        
        NSMutableDictionary *dOfPerson=[NSMutableDictionary dictionary];
        
        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople,i);
        
        CFStringRef firstName, lastName;
        firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        lastName  = ABRecordCopyValue(ref, kABPersonLastNameProperty);
        [dOfPerson setObject:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:@"name"];

        NSMutableArray *contactEmails = [NSMutableArray array];
        ABMultiValueRef multiEmails = ABRecordCopyValue(ref, kABPersonEmailProperty);
        for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
            CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
            NSString *contactEmail = (__bridge NSString *)contactEmailRef;
            [contactEmails addObject:contactEmail];
        }
        
        if ([contactEmails count] > 0) {
            [dOfPerson setObject:[contactEmails componentsJoinedByString:@","] forKey:@"emails"];
        }
        else {
            [dOfPerson setObject:@"" forKey:@"emails"];
        }

        NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
        ABMultiValueRef multiPhones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++) {
            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
            NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
            [phoneNumbers addObject:phoneNumber];
        }
        
        if ([phoneNumbers count] > 0) {
            [dOfPerson setObject:[phoneNumbers componentsJoinedByString:@","] forKey:@"phones"];
        }
        else {
            [dOfPerson setObject:@"" forKey:@"phones"];
        }

        [self.contactList addObject:dOfPerson];
    }
    NSLog(@"Contacts = %@",self.contactList);
    
    [self whoInstalledAppFromContacts:self.contactList];
}

- (void)whoInstalledAppFromContacts:(NSArray *)arrContacts
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] whoInstalledAppWithUserID:[[[CurrentUser getObject] getUser] userId] contacts:arrContacts and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        NSLog(@"currentClient :: %@",currentClient);
                        [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setBool:TRUE forKey:SYNCED_CONTACT_BEFORE];
                        [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setObject:CURRENT_TIME_STAMP forKey:LAST_CONTACT_SYNC];
                        [arrSuggestions addObjectsFromArray:currentClient];
                        
                        if ([arrSuggestions count] == 0) {
                            self.lblFriendsPlaceHolder.hidden = NO;
                            
                            self.btnInvite.hidden = NO;
                            self.cvFriends.hidden = YES;
                        }
                        else {
                            self.lblFriendsPlaceHolder.hidden = YES;
                            
                            self.btnInvite.hidden = YES;
                            self.cvFriends.hidden = NO;
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)loadContacts
{
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] loadContactsWithUserID:[[[CurrentUser getObject] getUser] userId] and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (currentClient) {
                        NSLog(@"currentClient :: %@",currentClient);
                        [arrSuggestions addObjectsFromArray:currentClient];
                        
                        if ([arrSuggestions count] == 0) {
                            self.lblFriendsPlaceHolder.hidden = NO;
                            
                            self.btnInvite.hidden = NO;
                            self.cvFriends.hidden = YES;
                        }
                        else {
                            self.lblFriendsPlaceHolder.hidden = YES;
                            
                            self.btnInvite.hidden = YES;
                            self.cvFriends.hidden = NO;
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)loadFriends
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] loadFriendsWithUserID:[[[CurrentUser getObject] getUser] userId] andStartingLimit:pageNumber and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        
                        NSString *friendRequests = [currentClient objectForKey:@"friendRequests"];
                        
                        if([friendRequests intValue] == 0)
                            [self.segmentedControl setTitle:[[Language sharedInstance] stringWithKey:@"request"] forSegmentAtIndex:1];
                        else
                            [self.segmentedControl setTitle:[NSString stringWithFormat:@"%@ %@", [[Language sharedInstance] stringWithKey:@"request"], friendRequests] forSegmentAtIndex:1];
                        
                        NSString *startingLimit = [currentClient objectForKey:@"nextStartingLimit"];
                        pageNumber = (startingLimit == nil) ? 0 : [startingLimit intValue];
                        NSLog(@"pageNumber :: %d",pageNumber);

                        NSArray *fanList = ([currentClient objectForKey:@"fansList"] == nil) ? @[] : [currentClient objectForKey:@"fansList"];
                        NSLog(@"fansList :: %@",fanList);

                        NSArray *fanListDB = ([currentClient objectForKey:@"fansListDB"] == nil) ? @[] : [currentClient objectForKey:@"fansListDB"];
                        if ([fanListDB count] > 0) {
                            FMDBHelper *helper = [FMDBHelper new];
                            for (NSDictionary *dict in fanListDB)
                            {
                                [helper insertOrReplaceFriends:(NSMutableDictionary *)dict];
                            }
                        }
                        arrFriends = [[NSMutableArray alloc] init];
                        [arrFriends addObjectsFromArray:fanList];
                        /// Manar
                        [self.segmentedControl setTitle:[NSString stringWithFormat:@"%@ %ld", [[Language sharedInstance] stringWithKey:@"Messages_Friends"], [arrFriends count]] forSegmentAtIndex:0];

                        if ([arrFriends count] == 0) {
                            self.btnInvite.hidden = NO;
                            self.lblFriendsPlaceHolder.hidden = NO;

                            self.cvFriends.hidden = YES;
                        }
                        else {
                            self.btnInvite.hidden = YES;
                            self.lblFriendsPlaceHolder.hidden = YES;

                            self.cvFriends.hidden = NO;
                        }
                        
                        [self.cvFriends reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)loadFriendRequests
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] loadFriendRequestsWithUserID:[[[CurrentUser getObject] getUser] userId] and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        NSLog(@"currentClient :: %@",currentClient);
                        arrRequests = [[NSMutableArray alloc] init];
                        [arrRequests addObjectsFromArray:currentClient];
                        
                        if([arrRequests count] <= 0 )
                            [self.segmentedControl setTitle:[[Language sharedInstance] stringWithKey:@"request"] forSegmentAtIndex:1];
                        else
                           [self.segmentedControl setTitle:[NSString stringWithFormat:@"%@ %ld", [[Language sharedInstance] stringWithKey:@"request"], [arrRequests count]] forSegmentAtIndex:1];
                        
               
                        if ([arrRequests count] == 0) {
                            self.lblRequestPlaceHolder.hidden = NO;
                            self.tblFriendRequests.hidden = YES;
                        }
                        else {
                            self.lblRequestPlaceHolder.hidden = YES;
                            self.tblFriendRequests.hidden = NO;
                        }
                        
                        [self.tblFriendRequests reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)declineFriendRequestForFan:(Fan *)fan
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] declineFriendRequestsWithUserID:[[[CurrentUser getObject] getUser] userId] fanID:fan.fanID and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                            [arrRequests removeObject:fan];
                            [self.tblFriendRequests reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
- (void)acceptFriendRequestForFan:(Fan *)fan
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[FansManager getInstance] changeFriendForUserID:[[[CurrentUser getObject] getUser] userId] fanID:fan.fanID friend:YES and:^(id currentClient) {
                
                NSLog(@"%@",currentClient);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                            [arrRequests removeObject:fan];
                            [self.tblFriendRequests reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}


#pragma mark - Actions

- (IBAction)facebook_Action:(id)sender
{
    [self.viewInvite removeFromSuperview];
    self.viewInvite.hidden = YES;

    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [mySLComposerSheet setInitialText:@"Install this cool app"];
        [mySLComposerSheet addImage:[UIImage imageNamed:@"FCBStudio_icon.png"]];
        [mySLComposerSheet addURL:[NSURL URLWithString:@"http://tawasolit.com/FCB/"]];
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
}

- (IBAction)twitter_Action:(id)sender
{
    [self.viewInvite removeFromSuperview];
    self.viewInvite.hidden = YES;

    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [mySLComposerSheet setInitialText:@"Install this cool app"];
        [mySLComposerSheet addImage:[UIImage imageNamed:@"FCBStudio_icon.png"]];
        [mySLComposerSheet addURL:[NSURL URLWithString:@"http://tawasolit.com/FCB/"]];
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
}

- (IBAction)sms_Action:(id)sender
{
    [self.viewInvite removeFromSuperview];
    self.viewInvite.hidden = YES;

    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    InviteFriendsView* invite = [sb instantiateViewControllerWithIdentifier:@"InviteFriendsView"];
    [self.navigationController pushViewController:invite animated:YES];
}

- (IBAction)cancel_Action:(id)sender
{
    [self.viewInvite removeFromSuperview];
    self.viewInvite.hidden = YES;
}

- (IBAction)inviteTapped:(id)sender
{
//    [self.navigationController.view addSubview:self.viewInvite];
//    self.viewInvite.hidden = NO;
    
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    InviteFriendsView* invite = [sb instantiateViewControllerWithIdentifier:@"InviteFriendsView"];
    [self.navigationController pushViewController:invite animated:YES];
}

- (IBAction)segmentValueChanged:(id)sender
{
    UISegmentedControl *segmControl = ((UISegmentedControl *)sender);
    if (segmControl.selectedSegmentIndex == 0) {
        [self loadFriends];
        [refreshControl removeFromSuperview];
        isSuggestion = NO;
        _comingSoonView.hidden = YES;
        self.tblFriendRequests.hidden = YES;

//        if (isUserOnline) {
            self.lblOfflinePlaceHolder.hidden = YES;
            self.lblRequestPlaceHolder.hidden = YES;
            self.lblFriendsPlaceHolder.hidden = YES;
            self.lblSuggestionsPlaceHolder.hidden = YES;
            
            if ([arrFriends count] == 0) {
                self.btnInvite.hidden = NO;
                self.lblFriendsPlaceHolder.hidden = NO;
                
                self.cvFriends.hidden = YES;
            }
            else {
                self.btnInvite.hidden = YES;
                self.lblFriendsPlaceHolder.hidden = YES;
                
                self.cvFriends.hidden = NO;
            }
//        }
//        else {
//            // Show offline placeholder.
//            self.lblOfflinePlaceHolder.hidden = NO;
//            self.lblRequestPlaceHolder.hidden = YES;
//            self.lblFriendsPlaceHolder.hidden = YES;
//            self.lblSuggestionsPlaceHolder.hidden = YES;
//        }
    }
    else if (segmControl.selectedSegmentIndex == 1) {
        
//        if (isUserOnline) {
        _comingSoonView.hidden = YES;
            self.cvFriends.hidden = YES;
            self.btnInvite.hidden = YES;
            
            self.lblOfflinePlaceHolder.hidden = YES;
            self.lblRequestPlaceHolder.hidden = YES;
            self.lblFriendsPlaceHolder.hidden = YES;
            self.lblSuggestionsPlaceHolder.hidden = YES;

            [self loadFriendRequests];
//        }
//        else {
//            // Show offline placeholder.
//            self.lblOfflinePlaceHolder.hidden = NO;
//            self.lblRequestPlaceHolder.hidden = YES;
//            self.lblFriendsPlaceHolder.hidden = YES;
//            self.lblSuggestionsPlaceHolder.hidden = YES;
//        }
    }
    else if (segmControl.selectedSegmentIndex == 2) {
        
            [self.cvFriends addSubview:refreshControl];

            isSuggestion = YES;
            self.tblFriendRequests.hidden = YES;
            self.cvFriends.hidden = YES;
           self.btnInvite.hidden = YES;

            self.lblOfflinePlaceHolder.hidden = YES;
            self.lblRequestPlaceHolder.hidden = YES;
            self.lblFriendsPlaceHolder.hidden = YES;
            self.lblSuggestionsPlaceHolder.hidden = YES;

        _comingSoonView.hidden = NO;

//            if ([[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] boolForKey:LAST_CONTACT_SYNC]) {
//                    [self loadContacts];
//            }
//            else {
//                [self authorizeAddressBook];
//            }
    }
}

- (IBAction)friend_Request_Action_Accept_Decline:(id)sender
{
    UIButton *btn = ((UIButton *)sender);
    CGPoint buttonPosition = [btn convertPoint:CGPointZero toView:self.tblFriendRequests];
    NSIndexPath *indexPath = [self.tblFriendRequests indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        if (btn.tag == 113) {
            Fan *currentFan = (Fan*)[arrRequests objectAtIndex:indexPath.row];
            [self acceptFriendRequestForFan:currentFan];
        }
        else if (btn.tag == 114) {
            Fan *currentFan = (Fan*)[arrRequests objectAtIndex:indexPath.row];
            [self declineFriendRequestForFan:currentFan];
        }
    }
}

-(void)acceptFriendRequest:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFriendRequests];
    NSIndexPath *indexPath = [self.tblFriendRequests indexPathForRowAtPoint:buttonPosition];
    Fan *currentFan = (Fan*)[arrRequests objectAtIndex:indexPath.row];
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[FansManager getInstance] changeFriendForUserID:[[[CurrentUser getObject] getUser] userId] fanID:currentFan.fanID friend:YES and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        [arrRequests removeObjectAtIndex:indexPath.row];
                        [self.segmentedControl setTitle:[NSString stringWithFormat:@"%@ %ld",[[Language sharedInstance] stringWithKey:@"request"] ,[arrRequests count]] forSegmentAtIndex:1];
                        [self.tblFriendRequests reloadData];
                    }
                });
                
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
                
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }
}
-(void)rejectFriendRequest:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFriendRequests];
    NSIndexPath *indexPath = [self.tblFriendRequests indexPathForRowAtPoint:buttonPosition];
    Fan *currentFan = (Fan*)[arrRequests objectAtIndex:indexPath.row];
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[FansManager getInstance] changeFriendForUserID:[[[CurrentUser getObject] getUser] userId] fanID:currentFan.fanID friend:NO and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        [arrRequests removeObjectAtIndex:indexPath.row];
                        [self.segmentedControl setTitle:[NSString stringWithFormat:@"%@ %ld",[[Language sharedInstance] stringWithKey:@"request"], [arrRequests count]] forSegmentAtIndex:1];
                        [self.tblFriendRequests reloadData];
                    }
                });
                
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });

            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }
}

- (IBAction)friend_Request_Action_fansView:(id)sender
{
    UIButton *btn = ((UIButton *)sender);
    CGPoint buttonPosition = [btn convertPoint:CGPointZero toView:self.tblFriendRequests];
    NSIndexPath *indexPath = [self.tblFriendRequests indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        if (btn.tag == 2) {
            NSLog(@"Friend icon tapped for row :: %ld",(long)indexPath.row);
            Fan *currentFan = (Fan*)[arrRequests objectAtIndex:indexPath.row];
            [self gotoDetailWithFan:currentFan.fanID];
        }
    }
}

#pragma mark - UICollectionView Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger rowCount = isSearch?[searchResult count]+1:  (isSuggestion) ? [arrSuggestions count]+1 : [arrFriends count]+1;
    return  rowCount == 1 ? 0 : rowCount;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (isSuggestion) {
        return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? CGSizeMake(150, 150) : CGSizeMake(155.0, 80.0);
    }
    else {
        return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? CGSizeMake(150, 150) : CGSizeMake(80.0, 80.0);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (isSuggestion) {
        return 1.0;
    }
    else {
        return 0.0;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (isSuggestion) {
        return 2.0;
    }
    else {
        return 0.0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;

    
    if (indexPath.row == 0) {
        static NSString *myIdentifier = @"inviteCell";
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier forIndexPath:indexPath];
        [(UILabel *) [cell viewWithTag:1] setText:[[Language sharedInstance] stringWithKey:@"initialMessages_Invite"]];
        [(UIButton *) [cell viewWithTag:2] addTarget:self action:@selector(inviteTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (isSuggestion) {
        static NSString *myIdentifier = @"cellSuggestion";
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier forIndexPath:indexPath];
        
        Fan *currentFan = (Fan*)[arrSuggestions objectAtIndex:indexPath.row-1];
        
        /// Manar
        [(UILabel *) [cell viewWithTag:55] setText:[[Language sharedInstance] stringWithKey:@"InviteFriends_AddFriends"]]; // Add Friend
        /////////
        
        UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:2];
        mainImage.image =  [UIImage imageNamed:@"avatar"];
        
        UIImageView* maskImage    = (UIImageView*)[cell viewWithTag:3];
        maskImage.image =  (currentFan.fanOnline) ? [UIImage imageNamed:@"online_grid_frame.png"] : [UIImage imageNamed:@"offline_grid_frame.png"];
        
        UIImageView* favImage    = (UIImageView*)[cell viewWithTag:4];
        favImage.image =  (currentFan.isFavorite) ? [UIImage imageNamed:@"favourit.png"] : [UIImage imageNamed:@"unfavourit.png"];
        
        UIImageView* followImage    = (UIImageView*)[cell viewWithTag:7];
        followImage.image =  (currentFan.isFollowed) ? [UIImage imageNamed:@"followed.png"] : [UIImage imageNamed:@"unfollowed.png"];
        
        UILabel* fanName = (UILabel*)[cell viewWithTag:5];
        fanName.text = (currentFan.fanName == nil) ? @"" : currentFan.fanName;
        
        UILabel* numOfPics = (UILabel*)[cell viewWithTag:6];
        numOfPics.text = [NSString stringWithFormat:@"%i",currentFan.fanPicNumber];
        
        UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:1] ;
        [loader startAnimating];
        
        [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentFan fanPicture]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [loader stopAnimating];
            [loader setHidden:YES];
        }];
    }
    else {
        static NSString *myIdentifier = @"cellFan";
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier forIndexPath:indexPath];
        
        Friend *currentFriend = (Friend*)[arrFriends objectAtIndex:indexPath.row-1];
        
        UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:2];
        mainImage.image =  [UIImage imageNamed:@"avatar"];
        
        UIImageView* maskImage = (UIImageView*)[cell viewWithTag:3] ;
        [maskImage setImage:nil];
        [maskImage setBackgroundColor:[UIColor clearColor]];
        //[maskImage setHidden:YES];////////////////////////Essam
        
        UIImageView* favImage    = (UIImageView*)[cell viewWithTag:4];
        favImage.image =  (currentFriend.isFavorite) ? [UIImage imageNamed:@"favourit.png"] : [UIImage imageNamed:@"unfavourit.png"];
        [favImage setHidden:YES];////////////////////////Essam
        
        UIImageView* followImage    = (UIImageView*)[cell viewWithTag:7];
        followImage.image =  (currentFriend.isFollowed) ? [UIImage imageNamed:@"followed.png"] : [UIImage imageNamed:@"unfollowed.png"];
        
        UILabel* fanName = (UILabel*)[cell viewWithTag:5];
        fanName.text = (currentFriend.friendName == nil) ? @"" : currentFriend.friendName;
        
        UILabel* numOfPics = (UILabel*)[cell viewWithTag:6];
        numOfPics.text = [NSString stringWithFormat:@"%i",currentFriend.friendPicNumber];
        
        UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:1] ;
        [loader startAnimating];
        
        [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentFriend friendPicture]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [loader stopAnimating];
            [loader setHidden:YES];
        }];
        
        /////Essam///////
        UIView* onlineState    = (UIView*)[cell viewWithTag:100];
        switch ([currentFriend friendOnline]) {
            case 0:
                [onlineState setBackgroundColor:[UIColor clearColor]];
                break;
            case 1:
                [onlineState setBackgroundColor:[UIColor greenColor]];
                break;
            case 2:
                [onlineState setBackgroundColor:[UIColor orangeColor]];
                break;
            default:
                break;
        }
        CALayer * l2 = [onlineState layer];
        [l2 setMasksToBounds:YES];
        [l2 setCornerRadius:5];
        ////Essam/////////
        CALayer * l3 = [maskImage layer];
        [l3 setMasksToBounds:YES];
        [l3 setCornerRadius:3];
        [l3 setBorderWidth:2];
        if ([currentFriend isPremium]) {
            [l3 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
        }else{
            [l3 setBorderColor:[[UIColor whiteColor] CGColor]];
        }
        
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isSuggestion) {
        Friend *currentFriend = (Friend*)[arrFriends objectAtIndex:indexPath.row-1];
        [self gotoDetailWithFan:currentFriend.friendID];
    }
}

#pragma mark - UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrRequests count];
}

-(void)tableView:(UITableView *)tableView
 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *myIdentifier = @"cellFriendRequest";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIdentifier];
    }
    
//    UIView *viewFriend = (UIView *)[cell viewWithTag:111];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(friend_Request_Action_fansView:)];
//    [viewFriend addGestureRecognizer:tap];

    Fan *currentFan = (Fan*)[arrRequests objectAtIndex:indexPath.row];
    
    UIButton* mainImage    = (UIButton*)[cell viewWithTag:2];
//    [mainImage addTarget:self action:@selector(friend_Request_Action_fansView:) forControlEvents:UIControlEventTouchUpInside];
    [mainImage setBackgroundImage:[UIImage imageNamed:@"avatar"] forState:UIControlStateNormal];
//    mainImage.image =  [UIImage imageNamed:@"avatar"];
    
    UIButton* accept    = (UIButton*)[cell viewWithTag:113];
    UIButton* reject    = (UIButton*)[cell viewWithTag:114];
    [accept addTarget:self action:@selector(acceptFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
    [reject addTarget:self action:@selector(rejectFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView* maskImage    = (UIImageView*)[cell viewWithTag:3];
    maskImage.image =  (currentFan.fanOnline) ? [UIImage imageNamed:@"online_grid_frame.png"] : [UIImage imageNamed:@"offline_grid_frame.png"];
    
    UIImageView* favImage    = (UIImageView*)[cell viewWithTag:4];
    favImage.image =  (currentFan.isFavorite) ? [UIImage imageNamed:@"favourit.png"] : [UIImage imageNamed:@"unfavourit.png"];
    
    UIImageView* followImage    = (UIImageView*)[cell viewWithTag:7];
    followImage.image =  (currentFan.isFollowed) ? [UIImage imageNamed:@"followed.png"] : [UIImage imageNamed:@"unfollowed.png"];
    
    UILabel* fanName = (UILabel*)[cell viewWithTag:5];
    fanName.text = (currentFan.fanName == nil) ? @"" : currentFan.fanName;
    
    UILabel* numOfPics = (UILabel*)[cell viewWithTag:6];
    numOfPics.text = [NSString stringWithFormat:@"%i",currentFan.fanPicNumber];
    
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:1] ;
    [loader startAnimating];

    [mainImage sd_setBackgroundImageWithURL:[NSURL URLWithString:[currentFan fanPicture]]
                                   forState:UIControlStateNormal
                           placeholderImage:[UIImage imageNamed:@"avatar"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    
    
    UILabel* fanNameNew = (UILabel*)[cell viewWithTag:112];
    fanNameNew.text = (currentFan.fanName == nil) ? @"" : currentFan.fanName;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}


- (IBAction)searchAction:(id)sender
{
    if (isSearch)
    {
        isSearch = NO;
        self.searchView.hidden=YES;
    }
    else
    {
        isSearch = YES;
        self.searchView.hidden=NO;
        [self.view bringSubviewToFront:self.searchView];
    }
    [self.theSearchBar becomeFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)aSearchBar
{
    isSearch = NO;
    self.searchView.hidden=YES;
    aSearchBar.text = @"";
    [aSearchBar resignFirstResponder];
    [_cvFriends reloadData];

}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)aSearchBar
{
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)aSearchBar
{
    return YES;
}


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    [searchResult  removeAllObjects];
    [_cvFriends reloadData];
    

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"friendName contains[c] %@",text];
    
    @try {
        if (isSuggestion) {
            [searchResult addObjectsFromArray:[arrSuggestions filteredArrayUsingPredicate:predicate]] ;
        }
        else{
            [searchResult addObjectsFromArray:[arrFriends filteredArrayUsingPredicate:predicate]] ;
        }
    }
    @catch (NSException *exception) {}
    
    [_cvFriends reloadData];
    

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
}

-(void)addAdBanner{
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    bannerView.rootViewController = self;
    bannerView.adUnitID = @"ca-app-pub-2924334778141972/3060492441";
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"abc7f676a40f3514a5a8c71ad28dfd27"];
    [bannerView loadRequest:request];
    
    [_adBanner addSubview:bannerView];
}

@end
