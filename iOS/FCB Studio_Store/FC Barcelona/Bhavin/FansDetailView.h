//
//  FansDetailView.h
//  FCB
//
//  Created by Bhavin Chitroda on 1/17/15.
//  Copyright (c) 2015 Bhavin Chitroda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FansDetailView : UIViewController<UIPopoverControllerDelegate,UIAlertViewDelegate>
{
    IBOutlet UIActivityIndicatorView *loader;

    IBOutlet UIScrollView *scrollView;
    IBOutlet UIScrollView *scrollInfo;

    IBOutlet UIImageView *imageViewFan;
    IBOutlet UIImageView *imageViewMask;

    IBOutlet UILabel *lblFanName;
    IBOutlet UILabel *lblCountryCity;

    IBOutlet UIButton *btnChat;
    IBOutlet UIButton *btnFriend;
    IBOutlet UIButton *btnFollow;
    IBOutlet UIButton *btnFavorite;
    IBOutlet UIButton *btnBlock;
    IBOutlet UIButton *btnMore;
    
    IBOutlet UIView *viewBack;
    
//    UIPopoverController *popover;
}
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (nonatomic, assign) int fanID;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (nonatomic, assign) BOOL isFriendDetail;

@property (weak, nonatomic) IBOutlet UIView *followingView;
@property (weak, nonatomic) IBOutlet UIView *followerView;
@property (weak, nonatomic) IBOutlet UIView *picsView;

@property (weak, nonatomic) IBOutlet UILabel *following;
@property (weak, nonatomic) IBOutlet UILabel *follower;
@property (weak, nonatomic) IBOutlet UILabel *pics;
@property (weak, nonatomic) IBOutlet UILabel *followingCount;
@property (weak, nonatomic) IBOutlet UILabel *followerCount;
@property (weak, nonatomic) IBOutlet UILabel *picsCount;

@property (weak, nonatomic) IBOutlet UIView *viewInfo;


- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chatBar_Action:(id)sender;

- (IBAction)popBack:(id)sender;

@end
