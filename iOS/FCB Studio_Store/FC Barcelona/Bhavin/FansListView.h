//
//  FansListView.h
//  FCB
//
//  Created by Bhavin Chitroda on 1/17/15.
//  Copyright (c) 2015 Bhavin Chitroda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

#import "FilterView.h"

//typedef enum ScrollDir {
//    ScrollDirectionNone,
//    ScrollDirectionRight,
//    ScrollDirectionLeft,
//    ScrollDirectionUp,
//    ScrollDirectionDown,
//    ScrollDirectionCrazy
//} ScrollDir;

@interface FansListView : GAITrackedViewController<FanFilterDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *buttonBar;
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UIView *adBanner;

@property (strong, nonatomic) NSMutableDictionary *FansFilterEnum;
@property (strong, nonatomic) NSArray *citiesArray;
@property (strong, nonatomic) NSString *searchText;

@property (strong,  nonatomic) IBOutlet UILabel *lblOfflinePlaceHolder;
@property (weak,    nonatomic) IBOutlet UIView *viewMain;

@property (strong, nonatomic) FilterView *filterView;

- (IBAction)gotoFilter:(id)sender;
- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

@end
