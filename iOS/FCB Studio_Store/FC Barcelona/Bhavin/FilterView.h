//
//  FilterView.h
//  FCB
//
//  Created by Bhavin Chitroda on 1/17/15.
//  Copyright (c) 2015 Bhavin Chitroda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CitiesFilterViewController.h"

@protocol FanFilterDelegate <NSObject>

- (void)applyFilterWithOptions:(NSMutableDictionary *)filterOptions sortingOption:(int)sortEnum countries:(NSArray *)countryList andSearchText:(NSString *)strSearchText;

@end

@interface FilterView : UIViewController<CityFilterDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    IBOutlet UIView *viewCountry;
    
    NSArray *arrCountries;
}
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UILabel *numberOfSelectedCountries;
@property (nonatomic, strong) IBOutlet NSString *searchText;

@property (nonatomic, strong) IBOutlet UITextField *txtSearchFans;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *loaderView;
@property (nonatomic, strong) IBOutlet UITableView *tblFansSuggestions;

@property (strong,  nonatomic) IBOutlet UILabel *lblOfflinePlaceHolder;
@property (weak,    nonatomic) IBOutlet UIView *viewMain;

@property (nonatomic, strong) NSMutableArray *arrFansSuggestions;

@property (nonatomic, strong) NSMutableDictionary *FansFiltersEnum;
@property (nonatomic, assign) int FansSortingEnum;

@property (nonatomic, weak) id <FanFilterDelegate> delegateFilter;

@property (nonatomic, strong) CitiesFilterViewController *citiesFilter;

- (IBAction)popBack:(id)sender;
- (IBAction)countrySelectionTapped:(id)sender;

@end
