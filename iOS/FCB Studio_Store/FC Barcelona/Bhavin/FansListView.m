//
//  FansListView.m
//  FCB
//
//  Created by Bhavin Chitroda on 1/17/15.
//  Copyright (c) 2015 Bhavin Chitroda. All rights reserved.
//

#import "FansListView.h"

#import "FansDetailView.h"

#import "CurrentUser.h"
#import "User.h"
#import "Fan.h"
#import "FansManager.h"

#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "WebImage.h"

#import "FMDBHelper.h"

#import "GetAndPushCashedData.h"

#define ICON_FILTER_IMG             @"filter.png"

#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#import <GoogleMobileAds/DFPBannerView.h>
#import <GoogleMobileAds/DFPRequest.h>

@interface FansListView () <UIAlertViewDelegate>{
    
    IBOutlet UICollectionView *cvPremiumList;
    IBOutlet UICollectionView *cvFansList;
    
    IBOutlet UIActivityIndicatorView *loaderPremium;

    IBOutlet UIImageView *ivBGPremium;
    IBOutlet UILabel *lblTitle;
    
    IBOutlet UIView *viewPremium;

//    IBOutlet UIButton *btnPremium;
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnUpgrade;

    BOOL isUserOnline;
    BOOL loaderBefore;
    BOOL isFan;
    /// Manar
    BOOL delegateMethodFlag;
    int pageNumber;
    int premPageNumber;
    int FansSortingEnum;
    
    CGFloat lastContentOffset;

    NSMutableArray * results;
    NSMutableArray * premiumUsers;
    NSMutableArray * premiumUsersHeader;
    UILabel * noFans;
}

@end

@implementation FansListView

@synthesize FansFilterEnum;
@synthesize citiesArray;
@synthesize searchText;

@synthesize filterView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    noFans = [[UILabel alloc] initWithFrame:cvFansList.frame];
    [noFans setFont:[UIFont systemFontOfSize:18]];
    [noFans setTextAlignment:NSTextAlignmentCenter];
    [noFans setTextColor:[UIColor whiteColor]];
    
    
    lblTitle.text = @"Fans";
    [viewPremium addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(upgradeToPremium:)]];
    isFan = YES;
    /// Manar
    pageNumber = 0;
    premPageNumber = 0;
    FansSortingEnum = 1;
    
    isUserOnline = [[[CurrentUser getObject] getUser] isOnline];

    results = [NSMutableArray array];
    premiumUsers = [NSMutableArray array];
    premiumUsersHeader = [NSMutableArray array];
    
    citiesArray = @[];
    searchText = @"";

    NSArray *keys = @[@"online",@"offline",@"male",@"female",@"followers",@"following",@"favoriteFriends"];
    NSArray *values = @[[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0]];

    FansFilterEnum = [[NSMutableDictionary alloc] initWithObjects:values forKeys:keys];
    
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    filterView = (FilterView *)[sb instantiateViewControllerWithIdentifier:@"FilterView"];

    if ([[[CurrentUser getObject] getUser] isPremium]) {
        [viewPremium setHidden:YES];
        [btnUpgrade setEnabled:NO];
        cvPremiumList.frame = CGRectMake(viewPremium.frame.origin.x,
                                         viewPremium.frame.origin.y,
                                         cvPremiumList.frame.size.width+viewPremium.frame.size.width,
                                         cvPremiumList.frame.size.height);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.screenName = @"Fans Screen";
    [noFans setText:[[Language sharedInstance] stringWithKey:@"FilterResults_NoFans"]];

    /// Manar
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"menu_Fans"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];

    [btnUpgrade setTitle:[[Language sharedInstance] stringWithKey:@"Fans_Upgrade"] forState:UIControlStateNormal];
    
    [(UILabel *)[self.view viewWithTag:55] setText:[[Language sharedInstance] stringWithKey:@"BecomePremium"]];
    
    if (!loaderBefore)
    {
//        if (YES) {
//                /// Manar
//                [self loadPremiumBar];
//                
//                if (!isPad) {
//                    cvFansList.frame = CGRectMake(0.0, 84.0, cvFansList.frame.size.width, cvFansList.frame.size.height);
//                }
//            }
//            else {
//                [loaderPremium setHidesWhenStopped:YES];
//                [loaderPremium stopAnimating];
//                
//                viewPremium.hidden = YES;
//                ivBGPremium.hidden = YES;
//                cvPremiumList.hidden = YES;
//                if (!isPad) {
//                    cvFansList.frame = CGRectMake(0.0, 0.0, cvFansList.frame.size.width, cvFansList.frame.size.height + 70);
//                }
        
        [self loadPremiumBar];
        
        if (!isPad) {
            cvFansList.frame = CGRectMake(0.0, 84.0, cvFansList.frame.size.width, cvFansList.frame.size.height);
        }
        
    
        if (!delegateMethodFlag){
               [self loadFansPremium:NO];
                self.lblOfflinePlaceHolder.hidden = YES;
                self.viewMain.hidden = NO;
            }
            else {
                // Show offline placeholder.
                self.lblOfflinePlaceHolder.hidden = NO;
                self.viewMain.hidden = YES;
            }
            loaderBefore = YES;
    }
    
    
    [self addAdBanner];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)loadPremiumBar
{

    [loaderPremium setHidesWhenStopped:YES];
    [loaderPremium startAnimating];
    @try {
        if ([RestServiceAgent internetAvailable]) {

            [[FansManager getInstance] loadPremiumBar:^(id currentClient) {
                NSLog(@"loadPremiumBar data :: %@",currentClient);
                dispatch_async(dispatch_get_main_queue(), ^{
                [loaderPremium stopAnimating];
                if (currentClient) {
                    [premiumUsersHeader addObjectsFromArray:currentClient];
                    [premiumUsersHeader addObject:[[Language sharedInstance] stringWithKey:@"all_premium_users"]];
                    [cvPremiumList reloadData];
                }
                });
            } onFailure:^(NSError *error) {
                [loaderPremium stopAnimating];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            [loaderPremium stopAnimating];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [loaderPremium stopAnimating];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)loadPremiumFans
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    FMDBHelper *helper = [FMDBHelper new];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            //searchText = (searchText == nil) ? @"" : searchText;
            [[FansManager getInstance] getFans:[[[CurrentUser getObject] getUser] userId] pageNo:premPageNumber searchText:searchText sortBy:FansSortingEnum filters:FansFilterEnum countriesIds:citiesArray premium:@"1" and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        

                        
                       // NSArray *fanList = ([currentClient objectForKey:@"fansList"] == nil) ? @[] : [currentClient objectForKey:@"fansList"];
                        NSMutableArray* tempArray = [[NSMutableArray alloc] initWithArray:[currentClient objectForKey:@"fansList"]];
                        pageNumber = [tempArray count];

                        //premiumUsers = [[NSMutableArray alloc] init];
                        
                        /// Manar
                        if (delegateMethodFlag) delegateMethodFlag = NO;
                        
                        for(Fan* tempFan in [currentClient objectForKey:@"fansList"])
                        {
                            for(Fan* tempPrem in premiumUsers)
                            {
                                if (tempPrem.fanID == tempFan.fanID)
                                {
                                    [tempArray removeObject:tempFan];
                                }
                            }
                        }
                        [premiumUsers addObjectsFromArray:tempArray] ;
                        

                        NSLog(@"results :: %@\npremiumUsers :: %@",results,premiumUsers);
                        [cvFansList reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }

}
-(void)loadFansPremium:(BOOL)isPremium
{
    [noFans removeFromSuperview];
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    FMDBHelper *helper = [FMDBHelper new];
    BOOL isEnum;
    for (NSString* key in [FansFilterEnum allKeys]) {
        if ([FansFilterEnum objectForKey:key] != [NSNumber numberWithInt:0])
            isEnum = YES;
    }
    
    @try {
        if ([RestServiceAgent internetAvailable]) {

            searchText = (searchText == nil) ? @"" : searchText;
            [[FansManager getInstance] getFans:[[[CurrentUser getObject] getUser] userId] pageNo:pageNumber searchText:searchText sortBy:FansSortingEnum filters:FansFilterEnum countriesIds:citiesArray premium:isPremium and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        
                        


                        NSArray *fanList = ([currentClient objectForKey:@"fansList"] == nil) ? @[] : [currentClient objectForKey:@"fansList"];
                        

                        
                        [results addObjectsFromArray:fanList] ;
                        pageNumber = [results count];

                        /// Manar
                        if (delegateMethodFlag) delegateMethodFlag = NO;
                        ///////////
                        
                        
                        for (int i = 0 ; i<results.count; i++)
                        {
                            Fan *obj = (Fan*)[results objectAtIndex:i];
                            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",obj.fanID],@"fanID",obj.fanName,@"fanName",obj.fanPicture,@"fanPicture", nil];
                            BOOL isSuccess = [helper insertFan:dic];
                            if (isSuccess)
                            {
                                //NSLog(@"Insert Successfully");
                            }
                            else
                            {
                                //NSLog(@"Insert NOT Successfully");
                            }
                        }
                        
                        if ([results count]==0) {
                            //[self.view addSubview:noFans];
                            [[[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"FilterResults_NoFans"] delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] otherButtonTitles:nil] show];
                        }
                        [cvFansList reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self gotoFilter:nil];
}

- (IBAction)backTapped:(id)sender
{
//    UIButton *btn = ((UIButton *)sender);
    
//    if (btn.tag == 0) {
        isFan = YES;
        lblTitle.text = @"Fans";
        btnBack.hidden = YES;
        btnUpgrade.hidden = YES;
    if ([[[CurrentUser getObject] getUser] isPremium]) {
        viewPremium.hidden = YES;
    }
    else{
        viewPremium.hidden = NO;

    }
        ivBGPremium.hidden = NO;
        cvPremiumList.hidden = NO;
        [cvFansList reloadData];
//    }
//    else {
//        lblTitle.text = @"Fans";
//        btnBack.hidden = YES;
//        isFan = YES;
//        btnUpgrade.hidden = YES;
//        viewPremium.hidden = YES;
//        ivBGPremium.hidden = YES;
//        cvPremiumList.hidden = YES;
//
//        btn.tag = 0;
//    }
    
//    [cvFansList scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
}

- (void)gotoDetailWithFan:(int)fanID
{
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
    fansDetailView.fanID = fanID;
    [self.navigationController pushViewController:fansDetailView animated:YES];
}

- (IBAction)gotoFilter:(id)sender
{
    filterView.delegateFilter = self;
//    filterView.searchText = searchText;
//    filterView.FansFiltersEnum = FansFilterEnum;
//    filterView.FansSortingEnum = FansSortingEnum;

    [self.navigationController pushViewController:filterView animated:YES];
}

- (IBAction)upgradeToPremium:(id)sender
{
    [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPremTab"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
}

//-(void)didScrollToDirection:(ScrollDir)direction{
//    if (direction==ScrollDirectionUp) {
//        [_tipView setHidden:YES];
//        
//        if (_buttonBar.frame.origin.y <= [UIScreen mainScreen].bounds.size.height) {
//            //[_buttonBar setCenter:CGPointMake(_buttonBar.center.x, _buttonBar.center.y+3)];
//            
//            [UIView animateWithDuration:.5f animations:^{
//                [_buttonBar setCenter:CGPointMake(_buttonBar.center.x, _buttonBar.center.y+(_buttonBar.frame.size.height/2))];
//            }];
//        }
//        
//    }
//    else if (direction==ScrollDirectionDown){
//        if (_buttonBar.center.y > barCenter.y) {
//            //[_buttonBar setCenter:CGPointMake(_buttonBar.center.x, _buttonBar.center.y-3)];
//            [UIView animateWithDuration:.5f animations:^{
//                _buttonBar.center = barCenter;
//            }];
//        }
//        
//    }  else if (direction==ScrollDirectionCrazy){
//        _buttonBar.center = barCenter;
//    }
//}

#pragma mark - Scrollview Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == cvFansList) {
        if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height + 6) {
            NSLog(@"Out");
            if (isFan) {
                [self loadFansPremium:NO];
            }
            else {
                //[self loadFansPremium:YES];
                [self loadPremiumFans];
            }
        }
        else{
            NSLog(@"In");
//            if(!isFan)
//            {
//                if(premPageNumber != [premiumUsers count])
//                {
//                    [self loadPremiumFans];
//                }
//            }
//            if (isFan) {
//                [self loadFansPremium:NO];
//            }
//            else {
//                //[self loadFansPremium:YES];
//                [self loadPremiumFans];
//            }
//            if (isFan) {
//                if ([results count] < 12) {
//                    [self loadFansPremium:NO];
//                }
//            }
//            else {
//                if ([premiumUsers count] < 12) {
//                    [self loadFansPremium:YES];
//                }
//            }
        }
        
        if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height + 300)
        {
            if (isFan) {
                [self loadFansPremium:NO];
            }
            else {
                //[self loadFansPremium:YES];
                [self loadPremiumFans];
            }
            //LOAD MORE
            // you can also add a isLoading bool value for better dealing :D
        }
    }
    else
    {
        NSLog(@"another");
    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    CGPoint offset = scrollView.contentOffset;
//    CGRect bounds = scrollView.bounds;
//    CGSize size = scrollView.contentSize;
//    UIEdgeInsets inset = scrollView.contentInset;
//    float y = offset.y + bounds.size.height - inset.bottom;
//    float h = size.height;
//    
//    float reload_distance = 10;
//    if(y > h + reload_distance)
//    {
//        // Call the method.
//        if (scrollView == cvFansList) {
//            if (isFan) {
//                [self loadFansPremium:NO];
//            }
//            else {
//                [self loadFansPremium:YES];
//            }
//        }
//    }
//}

#pragma mark - Filter Delegate

- (void)applyFilterWithOptions:(NSMutableDictionary *)filterOptions sortingOption:(int)sortEnum countries:(NSArray *)countryList andSearchText:(NSString *)strSearchText
{
    
    ///Essam
    [results removeAllObjects];
    [cvFansList reloadData];

    /// Manar
    delegateMethodFlag = YES;
    
    NSLog(@"filterOptions :: %@\nsortingOptions :: %d\ncountries :: %@\nsearchtext :: %@",filterOptions,sortEnum,countryList,strSearchText);
    FansSortingEnum = sortEnum;
    FansFilterEnum = filterOptions;
    citiesArray = countryList;
    searchText = (strSearchText == nil) ? @"" : strSearchText;
    pageNumber = 0;
    premPageNumber = 0;
    
    if (isFan) {
        results = [NSMutableArray array];
        [self loadFansPremium:NO];
    }
    else {
        premiumUsers = [NSMutableArray array];
        //[self loadFansPremium:YES];
        [self loadPremiumFans];
    }
}

#pragma mark - Actions

- (IBAction)openMenu:(id)sender
{
    if (!isPad) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openMenu" object:nil];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == cvFansList) {
        if (isFan) {
            return [results count];
        }
        else {
            return [premiumUsers count];
        }
    }
    else {
        return [premiumUsersHeader count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    
    if (collectionView == cvFansList) {
        static NSString *myIdentifier = @"cellFan";
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier forIndexPath:indexPath];
        
        Fan *currentFan = (isFan) ? (Fan*)[results objectAtIndex:indexPath.row] : (Fan*)[premiumUsers objectAtIndex:indexPath.row];
        
        UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:2];
        mainImage.image =  [UIImage imageNamed:@"avatar"];

        UIImageView* maskImage = (UIImageView*)[cell viewWithTag:3] ;
        [maskImage setImage:nil];
        [maskImage setBackgroundColor:[UIColor clearColor]];
        
        UIImageView* favImage    = (UIImageView*)[cell viewWithTag:4];
        favImage.image =  (currentFan.isFavorite) ? [UIImage imageNamed:@"favourit.png"] : [UIImage imageNamed:@"unfavourit.png"];
        [favImage setHidden:YES];////////////////////////Essam

        UIImageView* followImage    = (UIImageView*)[cell viewWithTag:7];
        followImage.image =  (currentFan.isFollowed) ? [UIImage imageNamed:@"followed.png"] : [UIImage imageNamed:@"unfollowed.png"];

        UILabel* fanName = (UILabel*)[cell viewWithTag:5];
        fanName.text = (currentFan.fanName == nil) ? @"" : currentFan.fanName;
        
        UILabel* numOfPics = (UILabel*)[cell viewWithTag:6];
        numOfPics.text = [NSString stringWithFormat:@"%i",currentFan.fanPicNumber];

        UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:1] ;
        [loader startAnimating];
        
        [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentFan fanPicture]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [loader stopAnimating];
            [loader setHidden:YES];
        }];
        /////Essam///////
        UIView* onlineState    = (UIView*)[cell viewWithTag:100];
        switch ([currentFan fanOnline]) {
            case 0:
                [onlineState setBackgroundColor:[UIColor clearColor]];
                break;
            case 1:
                [onlineState setBackgroundColor:[UIColor greenColor]];
                break;
            case 2:
                [onlineState setBackgroundColor:[UIColor orangeColor]];
                break;
            default:
                break;
        }
        CALayer * l2 = [onlineState layer];
        [l2 setMasksToBounds:YES];
        [l2 setCornerRadius:5];
        ////Essam/////////
        CALayer * l3 = [maskImage layer];
        [l3 setMasksToBounds:YES];
        [l3 setCornerRadius:3];
        [l3 setBorderWidth:2];
        if ([currentFan isPremium]) {
            [l3 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
        }else{
            [l3 setBorderColor:[[UIColor whiteColor] CGColor]];
        }
    }
    else {
        static NSString *myIdentifier1 = @"cellNorm";
        static NSString *myIdentifier2 = @"cellFan";
        
        NSObject *obj = premiumUsersHeader[indexPath.row];
        if ([obj isKindOfClass:[Fan class]]) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier2 forIndexPath:indexPath];
            
            Fan *currentFan = (Fan*)obj;
            
            UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:2];
            mainImage.image =  [UIImage imageNamed:@"avatar"];
            
            UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:1] ;
            [loader startAnimating];
            
            [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentFan fanPicture]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [loader stopAnimating];
                [loader setHidden:YES];
            }];
        }
        else {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier1 forIndexPath:indexPath];
            
            UIImageView *iv = (UIImageView *)[cell viewWithTag:1];
            iv.hidden = YES;
            
            UILabel *lbl = (UILabel *)[cell viewWithTag:2];
            [lbl setText:[[Language sharedInstance] stringWithKey:@"all_premium_users"]];
            lbl.hidden = NO;
        }
        
//        if (indexPath.row == 8) {
//            cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier1 forIndexPath:indexPath];
//            
//            UIImageView *iv = (UIImageView *)[cell viewWithTag:1];
//            iv.hidden = YES;
//            
//            UILabel *lbl = (UILabel *)[cell viewWithTag:2];
//            lbl.hidden = NO;
//        }
//        else {
//            cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier2 forIndexPath:indexPath];
//            
//            Fan *currentFan = (Fan*)
//            
//            UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:2];
//            mainImage.image =  [UIImage imageNamed:@"avatar"];
//
//            UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:1] ;
//            [loader startAnimating];
//            [WebImage processImageDataWithURLString:[currentFan fanPicture] cacheImage:YES andBlock:^(NSData *imageData) {
//                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    mainImage.image = (imageData == nil) ? [UIImage imageNamed:@"avatar"] : [UIImage imageWithData:imageData];
//                    [loader stopAnimating];
//                    [loader setHidden:YES];
//                });
//            }];
//        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == cvPremiumList) {
        
        NSObject *obj = premiumUsersHeader[indexPath.row];
        if (![obj isKindOfClass:[Fan class]]) {
//
//        if (indexPath.row == 8) {
            isFan = NO;
            lblTitle.text = [[Language sharedInstance] stringWithKey:@"PremiumList_title"];
            btnBack.hidden = NO;
            if ([[[CurrentUser getObject] getUser] isPremium]) {
                btnUpgrade.enabled = NO;
            }
            
            btnUpgrade.hidden = NO;
            viewPremium.hidden = YES;
            ivBGPremium.hidden = YES;
            cvPremiumList.hidden = YES;
            premPageNumber = 0;
            [self loadPremiumFans];
            [cvFansList reloadData];
            
//            [cvFansList scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
        }
        else {
            Fan *currentFan = (Fan*)[premiumUsersHeader objectAtIndex:indexPath.row];

            if ([[CurrentUser getObject] getUser].userId ==[currentFan fanID] ) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];

            }
            else{
                [self gotoDetailWithFan:currentFan.fanID];

            }
        }
    }
    else {
        Fan *currentFan = (isFan) ? (Fan*)[results objectAtIndex:indexPath.row] : (Fan*)[premiumUsers objectAtIndex:indexPath.row];
        if ([[CurrentUser getObject] getUser].userId ==[currentFan fanID] ) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
            
        }else{
            [self gotoDetailWithFan:currentFan.fanID];
            
        }
    }
}

-(void)addAdBanner{
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    bannerView.rootViewController = self;
    bannerView.adUnitID = @"ca-app-pub-2924334778141972/3060492441";
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"abc7f676a40f3514a5a8c71ad28dfd27"];
    [bannerView loadRequest:request];
    
    [_adBanner addSubview:bannerView];
}


- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
  [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
}
@end
