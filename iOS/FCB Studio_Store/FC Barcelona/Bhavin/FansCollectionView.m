//
//  FansCollectionView.m
//  FCB
//
//  Created by Bhavin Chitroda on 1/19/15.
//  Copyright (c) 2015 Bhavin Chitroda. All rights reserved.
//

#import "FansCollectionView.h"

#import "FansDetailView.h"

#import "FansManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"

#import "CurrentUser.h"
#import "Fan.h"

#import "WebImage.h"

#define ICON_BACK_IMG               @"back.png"

#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#import <GoogleMobileAds/DFPBannerView.h>
#import <GoogleMobileAds/DFPRequest.h>

@implementation FansCollectionView
{
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    pageNumber = 0;
    _arrFans = [NSMutableArray array];
    lblTitle.text = [NSString stringWithFormat:@"%@ %i",self.viewTitle, self.viewCount];
}

- (void)viewWillAppear:(BOOL)animated
{
    [_lblFollowersPlaceHolder setText:[[Language sharedInstance] stringWithKey:@"FollowersInitalState_no_followers"]];
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"menu_Fans"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];

    
    [super viewWillAppear:YES];
    
    if (self.fanID != 0) {
        
    }
    _viewCount = self.fanID != 0 ? _viewCount : [CurrentUser getObject].getUser.followingCount;

    if (self.isFollowers) {
        lblTitle.text =[[ Language sharedInstance] stringWithKey:@"menu_Followers"] ;
        [self loadFollowersForFan];
    }
    else {
        if (self.viewCount == 0 && self.fanID == 0) {
            lblTitle.text =[[ Language sharedInstance] stringWithKey:@"suggestion"] ;
            [self loadFollowingSuggestion];
        }
        else {
            lblTitle.text = [NSString stringWithFormat:@"%@ %i",self.viewTitle, self.viewCount];
            [self loadFollowingsForFan];
        }
    }
    [self addAdBanner];

}

- (IBAction)popBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)gotoDetailWithFan:(int)fanID
{
    if(![[CurrentUser getObject] getUser].userId) {
        UIStoryboard * sb = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ?
        [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] :
        [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
        fansDetailView.fanID = fanID;
        [self.navigationController pushViewController:fansDetailView animated:YES];
    }
    else {
        if (fanID != [[CurrentUser getObject] getUser].userId) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getBlockedUsersNCompareWith" object:[NSString stringWithFormat:@"%i", fanID]];
        } else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
        }
    }
}


- (void)loadFollowersForFan
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] loadFollowersForFanWithUserID:[[[CurrentUser getObject] getUser] userId] fanID:self.fanID andStartingLimit:pageNumber and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        NSLog(@"loadFollowersForFan data :: %@",currentClient);
                        
                        NSString *startingLimit = [currentClient objectForKey:@"nextStartingLimit"];
                        
                        
                        /// Manar
//                        lastPageNumber = (pageNumber) ? pageNumber : lastPageNumber;
                        pageNumber = (startingLimit == nil) ? 0 : [startingLimit intValue];
                        ///////////
                        
                        NSLog(@"startingLimit :: %d",pageNumber);
                        
                        NSArray *fanList = ([currentClient objectForKey:@"fansList"] == nil) ? @[] : [currentClient objectForKey:@"fansList"];
                        /// Manar
//                        if (lastPageNumber != pageNumber) {
                            if ([_arrFans count]) {
                                _arrFans = [NSMutableArray array];
                            }
                            [_arrFans addObjectsFromArray:fanList] ;
//                        }
                        pageNumber = 0;
                        ///////////
                        
                        if ([_arrFans count] > 0) {
                            self.lblOfflinePlaceHolder.hidden = YES;
                            self.lblFollowersPlaceHolder.hidden = YES;
                            self.collectionView.hidden = NO;
                        }
                        else {
                            self.lblOfflinePlaceHolder.hidden = YES;
                            self.lblFollowersPlaceHolder.hidden = NO;
                            self.collectionView.hidden = YES;
                        }
                        
                        NSLog(@"fanList :: %@",_arrFans);
                        

                        for (int i = 0; i < [_arrFans count]; i++) {
                            Fan* fan = (Fan*) _arrFans[i];
                            if (fan.fanID==0) {
                                [_arrFans removeObject:fan];
                            }
                        }
                        lblTitle.text = [NSString stringWithFormat:@"%@ %i",self.viewTitle, (int)self.arrFans.count];
                        [_collectionView reloadData];
                    }
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)loadFollowingsForFan
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] loadFollowingForFanWithUserID:[[[CurrentUser getObject] getUser] userId] fanID:self.fanID andStartingLimit:pageNumber and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        NSLog(@"loadFolloweignsForFan data :: %@",currentClient);
                        
                        NSString *startingLimit = [currentClient objectForKey:@"nextStartingLimit"];
                        /// Manar
//                        lastPageNumber = (pageNumber) ? pageNumber : lastPageNumber;
                        pageNumber = (startingLimit == nil) ? 0 : [startingLimit intValue];
                        ///////////
                        
                        NSLog(@"startingLimit :: %d",pageNumber);
                        
                        NSArray *fanList = ([currentClient objectForKey:@"fansList"] == nil) ? @[] : [currentClient objectForKey:@"fansList"];
                        /// Manar
//                        if (lastPageNumber != pageNumber) {
                        if ([_arrFans count]) {
                            _arrFans = [NSMutableArray array];
                        
                            NSUserDefaults * defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
                            User * user =  (User*)[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"user"]];
                            [user setFollowingCount:(int)[fanList count]];
                            [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"user"];
                            NSLog(@"User after :: %@", user);
                            
                        }
                        
                        [_arrFans addObjectsFromArray:fanList] ;
//                        }
                        pageNumber = 0;
                        ///////////
                        
                        
                        NSLog(@"fanList :: %@",_arrFans);
                        for (int i = 0; i < [_arrFans count]; i++) {
                            Fan* fan = (Fan*) _arrFans[i];
                            if (fan.fanID==0) {
                                [_arrFans removeObject:fan];
                            }
                        }
                        lblTitle.text = [NSString stringWithFormat:@"%@ %i",self.viewTitle, (int)self.arrFans.count];
                        [_collectionView reloadData];
                    }
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)loadFollowingSuggestion
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] loadFollowingSuggestionWithStartingLimit:pageNumber and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    if (currentClient) {
                        NSLog(@"loadFollowingSuggestion data :: %@",currentClient);
                        
                        NSString *startingLimit = [currentClient objectForKey:@"nextStartingLimit"];
                       
                        pageNumber = (startingLimit == nil) ? 0 : [startingLimit intValue];
                        
                        NSLog(@"startingLimit :: %d",pageNumber);
                        
                        NSArray *fanList = ([currentClient objectForKey:@"fansList"] == nil) ? @[] : [currentClient objectForKey:@"fansList"];
                        /// Manar
//                        if (lastPageNumber != pageNumber) {
                            if ([_arrFans count]) {
                                _arrFans = [NSMutableArray array];
                            }
                            [_arrFans addObjectsFromArray:fanList] ;
//                        }
                        pageNumber = 0;
                        ///////////
                        [_collectionView reloadData];
                    }
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark - Actions

- (IBAction)openMenu:(id)sender
{
    if (!isPad) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openMenu" object:nil];
    }
}

#pragma mark - Scrollview Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _collectionView) {
        if (pageNumber != self.viewCount) {
            
            if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height + 6) {
                NSLog(@"Out");
                if (self.isFollowers) {
                    [self loadFollowersForFan];
                }
                else {
                    if (self.viewCount == 0) {
                        [self loadFollowingSuggestion];
                    }
                    else {
                        [self loadFollowingsForFan];
                    }
                }
            }
            else{
                NSLog(@"In");
            }
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_arrFans count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *myIdentifier = @"cellFan";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:myIdentifier forIndexPath:indexPath];
    
    Fan *currentFan = (Fan*)[_arrFans objectAtIndex:indexPath.row];
    
    UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:2];
    mainImage.image =  [UIImage imageNamed:@"avatar"];
    
    UIImageView* maskImage = (UIImageView*)[cell viewWithTag:3] ;
    [maskImage setImage:nil];
    [maskImage setBackgroundColor:[UIColor clearColor]];
    //[maskImage setHidden:YES];////////////////////////Essam

    UIImageView* favImage    = (UIImageView*)[cell viewWithTag:4];
    favImage.image =  (currentFan.isFavorite) ? [UIImage imageNamed:@"favourit.png"] : [UIImage imageNamed:@"unfavourit.png"];
    [favImage setHidden:YES];////////////////////////Essam

    UIImageView* followImage    = (UIImageView*)[cell viewWithTag:7];
    followImage.image =  (currentFan.isFollowed) ? [UIImage imageNamed:@"followed.png"] : [UIImage imageNamed:@"unfollowed.png"];
    
    UILabel* fanName = (UILabel*)[cell viewWithTag:5];
    fanName.text = (currentFan.fanName == nil) ? @"" : currentFan.fanName;
    
    UILabel* numOfPics = (UILabel*)[cell viewWithTag:6];
    numOfPics.text = [NSString stringWithFormat:@"%i",currentFan.fanPicNumber];
    
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:1] ;
    [loader startAnimating];


    [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentFan fanPicture]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    /////Essam///////
    UIView* onlineState    = (UIView*)[cell viewWithTag:100];
    switch ([currentFan fanOnline]) {
        case 0:
            [onlineState setBackgroundColor:[UIColor clearColor]];
            break;
        case 1:
            [onlineState setBackgroundColor:[UIColor greenColor]];
            break;
        case 2:
            [onlineState setBackgroundColor:[UIColor orangeColor]];
            break;
        default:
            break;
    }
    CALayer * l2 = [onlineState layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    ////Essam/////////
    CALayer * l3 = [maskImage layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:3];
    [l3 setBorderWidth:2];
    if ([currentFan isPremium]) {
        [l3 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
    }else{
        [l3 setBorderColor:[[UIColor whiteColor] CGColor]];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Fan *currentFan = (Fan*)[_arrFans objectAtIndex:indexPath.row];
    if ([[CurrentUser getObject] getUser].userId ==[currentFan fanID] ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
    }
    else{
        [self gotoDetailWithFan:currentFan.fanID];
    }
}



-(void)addAdBanner{
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    bannerView.rootViewController = self;
    bannerView.adUnitID = @"ca-app-pub-2924334778141972/3060492441";
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"abc7f676a40f3514a5a8c71ad28dfd27"];
    [bannerView loadRequest:request];
    
    [_adBanner addSubview:bannerView];
}

- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
}

- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
}

- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
@end
