//
//  FansDetailView.m
//  FCB
//
//  Created by Bhavin Chitroda on 1/17/15.
//  Copyright (c) 2015 Bhavin Chitroda. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "FansDetailView.h"
#import "MessagingViewController.h"
#import "FansManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"

#import "CurrentUser.h"
#import "Fan.h"

#import "WebImage.h"
#import <objc/runtime.h>
#import "FansCollectionView.h"
#import "PicsListView.h"

#import "FMDBHelper.h"
//#import "DemoMessagesViewController.h"

#define ICON_BACK_IMG               @"back.png"

#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@implementation FansDetailView
{
    Fan* currentFanObject;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Profile";
    
    scrollView.contentSize = CGSizeMake(scrollInfo.frame.size.width, 2 * scrollInfo.frame.size.height);
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followers_Action:)];
    [_followerView addGestureRecognizer:tap1];

    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(following_Action:)];
    [_followingView addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pics_Action:)];
    [_picsView addGestureRecognizer:tap3];

       [self prepareFanImageViewWithMask];
    
    CALayer * l0 = [self.followerView layer];
    [l0 setMasksToBounds:YES];
    [l0 setCornerRadius:3];
    CALayer * l1 = [self.followingView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:3];
    CALayer * l2 = [self.picsView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:3];
    
//    [_followerCount setFont:[UIFont fontWithName:@"stencilie" size:22]];
//    [_followingCount setFont:[UIFont fontWithName:@"stencilie" size:22]];
//    [_picsCount setFont:[UIFont fontWithName:@"stencilie" size:22]];
    
    _viewInfo.hidden = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    [btnChat setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_Chat"] forState:UIControlStateNormal];
    [btnFriend setTitle:[[Language sharedInstance] stringWithKey:@"InitialMessagesBetweenFans_AddFriend"] forState:UIControlStateNormal];
    [btnFavorite setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_Favorate"] forState:UIControlStateNormal];
    [btnMore setTitle:[[Language sharedInstance] stringWithKey:@"user_profile_more"] forState:UIControlStateNormal];
    [btnBlock setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_Block"] forState:UIControlStateNormal];
    [btnFollow setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_Follow"] forState:UIControlStateNormal];
    
    if (self.isFriendDetail) {
        if (![[[CurrentUser getObject] getUser] isOnline]) {
            FMDBHelper *helper = [FMDBHelper new];
            NSArray *array = [helper getFriendWithID:self.fanID];
            if (array != nil  && [array count] > 0) {
                
                NSDictionary *dict = array[0];
                NSLog(@"dict offline :: %@",dict);
                
                Fan *fan = [[Fan alloc] init];
                fan.fanID = (dict[@"userId"] == nil) ? 0 : [dict[@"userId"] intValue];
                fan.fanName = (dict[@"userName"] == nil) ? @"" : dict[@"userName"];
                fan.fanPicture = (dict[@"profilePic"] == nil) ? @"" : dict[@"profilePic"];
                fan.fanOnline = (dict[@"online"] == nil) ? FALSE : [dict[@"online"] boolValue];
                fan.isFavorite = (dict[@"isFavorite"] == nil) ? FALSE : [dict[@"isFavorite"] boolValue];
                fan.isPremium = (dict[@"premium"] == nil) ? FALSE : [dict[@"premium"] boolValue];
                fan.fanPicNumber = (dict[@"posts"] == nil) ? 0 : [dict[@"posts"] intValue];
                fan.isFollowed = (dict[@"isFollowing"] == nil) ? FALSE : [dict[@"isFollowing"] boolValue];
                
                [self updateUIWithFan:fan];
            }
        }
        else {
            [self getFanDetails];
        }
    }
    else {
        [self getFanDetails];
    }
    

    [_following setText:[[Language sharedInstance] stringWithKey:@"menu_Following"]];
    [_follower setText:[[Language sharedInstance] stringWithKey:@"menu_Followers"]];
    [_pics setText:[[Language sharedInstance] stringWithKey:@"Fan_Pics"]];
    
    //[(UILabel *)[self.view viewWithTag:55] setText:[[Language sharedInstance] stringWithKey:@"noLocalizationTextExist"]]; //I'm using FCB Studio
    /////////
    
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"menu_Fans"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
}
- (IBAction)popBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareFanImageViewWithMask
{
    CALayer * l3 = [imageViewFan layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:5];
    [l3 setBorderWidth:2];
    [l3 setBorderColor:[[UIColor whiteColor] CGColor]];
}

- (void)prepareMoreInfo:(NSArray *)arrInfo
{
    float newY = 10.0;

    @try {
        for (int i = 0; i < [arrInfo count]; i++) {
            
            NSDictionary *dict = arrInfo[i];
            NSString *title = dict[@"title"];
            NSString *value ;
            
            if ([title isEqualToString:@"income"]) {
                switch ([dict[@"value"] intValue]) {
                    case 1:
                        value = [[Language sharedInstance] stringWithKey:@"income_list_item1"];
                        break;
                    case 2:
                        value = [[Language sharedInstance] stringWithKey:@"income_list_item2"];
                        break;
                    case 3:
                        value = [[Language sharedInstance] stringWithKey:@"income_list_item3"];
                        break;
                    default:
                        value = @"NONE";
                        break;
                }
            }
            else if ([title isEqualToString:@"relationship"]) {
                switch ([dict[@"value"] intValue]) {
                    case 1:
                        value = [[Language sharedInstance] stringWithKey:@"relationship_list_item1"];
                        break;
                    case 2:
                        value = [[Language sharedInstance] stringWithKey:@"relationship_list_item2"];
                        break;
                    case 3:
                        value = [[Language sharedInstance] stringWithKey:@"relationship_list_item3"];
                        break;
                    case 4:
                        value = [[Language sharedInstance] stringWithKey:@"relationship_list_item4"];
                        break;
                    default:
                        value = @"NONE";
                        break;
                }
                
            }else if ([title isEqualToString:@"education"]) {
                value = [[[Language sharedInstance] arrayWithKey:@"Educations"] objectAtIndex:[dict[@"value"] intValue]];
            }
            else if ([title isEqualToString:@"Profession"]) {
                value = [[[Language sharedInstance] arrayWithKey:@"Jobs"] objectAtIndex:[dict[@"value"] intValue]];

            } else if ([title isEqualToString:@"job role"]) {
                value = [[[Language sharedInstance] arrayWithKey:@"JobRoles"] objectAtIndex:[dict[@"value"] intValue]];
            }
            else{
                value =  dict[@"value"];
            }
            
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10.0, newY, scrollInfo.frame.size.width - 20.0, 20.0)];
            lblTitle.text = [NSString stringWithFormat:@"%@ :",title];
            lblTitle.font = [UIFont boldSystemFontOfSize:12.0];
            [scrollInfo addSubview:lblTitle];
            
            UILabel *lblValue = [[UILabel alloc] initWithFrame:CGRectMake(10.0, newY + 30.0, scrollInfo.frame.size.width - 20.0, 0.0)];
            lblValue.text = value;
            lblValue.font = [UIFont systemFontOfSize:12.0];
            lblValue.numberOfLines = 0; // allows label to have as many lines as needed
            [lblValue sizeToFit];
            [scrollInfo addSubview:lblValue];
            
            newY = newY + 30.0 + 10.0 + lblValue.frame.size.height;
            scrollInfo.contentSize = CGSizeMake(scrollInfo.frame.size.width, newY);
        }
    }
    @catch (NSException *exception) {
        scrollInfo.contentSize = CGSizeMake(scrollInfo.frame.size.width, newY+30);
        NSLog(@"====|%@",exception.description);
    }

}

- (void)changeFavorite:(BOOL)isFavorite
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] changeFavoriteForUserID:[[[CurrentUser getObject] getUser] userId] fanID:self.fanID favorite:isFavorite and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        NSLog(@"Favorite data :: %@",currentClient);
                        BOOL success;
                        if (currentClient[@"status"]) {
                            success = ([currentClient[@"status"] intValue] == 1) ? YES: NO;
                            if (success) {
                                if (isFavorite) {
                                    [btnFavorite setTitle:[[Language sharedInstance] stringWithKey:@"FriendsLevel2Profile_unFavorate"] forState:UIControlStateNormal];
                                    [btnFavorite setBackgroundImage:[UIImage imageNamed:@"fan_unfavourity.png"] forState:UIControlStateNormal];
                                }
                                else {
                                    [btnFavorite setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_Favorate"] forState:UIControlStateNormal];
                                    [btnFavorite setBackgroundImage:[UIImage imageNamed:@"fan_favourit.png"] forState:UIControlStateNormal];
                                }
                            }
                        }
                    }
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [loader stopAnimating];
                [loader setHidden:YES];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)changeFriend:(BOOL)isFriend
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] changeFriendForUserID:[[[CurrentUser getObject] getUser] userId] fanID:self.fanID friend:isFriend and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        NSLog(@"Friend data :: %@",currentClient);
                        BOOL success;
                        if (currentClient[@"status"]) {
                            success = ([currentClient[@"status"] intValue] == 1) ? YES: NO;
                            if (success) {
                                if (isFriend) {
                                    [btnFriend setTitle:[[Language sharedInstance] stringWithKey:@"InitialMessagesBetweenFans_AddFriend"] forState:UIControlStateNormal];
                                    [btnFriend setBackgroundImage:[UIImage imageNamed:@"removefriends.png"] forState:UIControlStateNormal];
                                }
                                else {
                                    [btnFriend setTitle:[[Language sharedInstance] stringWithKey:@"InitialMessagesBetweenFans_AddFriend"]  forState:UIControlStateNormal];
                                    [btnFriend setBackgroundImage:[UIImage imageNamed:@"fan_addfriends.png"] forState:UIControlStateNormal];
                                }
                            }
                        }
                    }
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)changeBlock:(BOOL)isBlock
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] changeBlockForUserID:[[[CurrentUser getObject] getUser] userId] fanID:self.fanID block:isBlock and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        BOOL success;
                        if (currentClient[@"status"]) {
                            success = ([currentClient[@"status"] intValue] == 1) ? YES: NO;
                            if (success) {
                                if (isBlock) {
                                    [self.navigationController popViewControllerAnimated:YES];
                                    [btnBlock setBackgroundImage:[UIImage imageNamed:@"fan_unblock.png"] forState:UIControlStateNormal];
                                }
                                else
                                {
                                    [btnBlock setBackgroundImage:[UIImage imageNamed:@"fan_block.png"] forState:UIControlStateNormal];
                                }
                            }
                        }
                    }
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (NSMutableDictionary *)prepareFriendForDB:(Fan *)myFan
{
    NSArray *key = @[@"friendOnline",@"isPremium",@"isFavorite",@"isFollowed",@"friendID",@"friendPicNumber",@"friendName",@"friendPicture"];
    NSArray *values = @[[NSString stringWithFormat:@"%d",myFan.fanOnline],[NSString stringWithFormat:@"%d",myFan.isPremium],[NSString stringWithFormat:@"%d",myFan.isFavorite],[NSString stringWithFormat:@"%d",myFan.isFollowed],[NSString stringWithFormat:@"%d",myFan.fanID],[NSString stringWithFormat:@"%d",myFan.fanPicNumber],myFan.fanName,myFan.fanPicture];

    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] initWithObjects:values forKeys:key];
    NSLog(@"dictData :: %@",dictData);

    return dictData;
}

- (void)getFanDetails
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];

    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] getPublicProfileFanWithUserID:[[[CurrentUser getObject] getUser] userId] andFanID:self.fanID and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    NSLog(@"fan Details data :: %@",currentClient);
                    if (currentClient) {
                        
                        currentFanObject = (Fan *)currentClient;
                        [self updateUIWithFan:currentFanObject];

                        if (self.isFriendDetail) {
                            NSMutableDictionary *dictFan = [self prepareFriendForDB:currentFanObject];
                            FMDBHelper *helper = [FMDBHelper new];
                            [helper insertOrReplaceFriends:dictFan];
                        }
                    }
                });

            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)updateUIWithFan:(Fan *)currentFan
{
    lblFanName.text = currentFan.fanName;
    
    NSString *country = (currentFan.fanCountry == nil || [currentFan.fanCountry isEqualToString:@""]) ? @"" : [NSString stringWithFormat:@"%@",currentFan.fanCountry];
    
    NSString *district = (currentFan.fanDistrict == nil || [currentFan.fanDistrict isEqualToString:@""]) ? @"" : [NSString stringWithFormat:@", %@",currentFan.fanDistrict];
    
    NSString *city = (currentFan.fanCity == nil || [currentFan.fanCity isEqualToString:@""]) ? @"" : [NSString stringWithFormat:@", %@",currentFan.fanCity];
    
    NSString *countryDistrict = [country stringByAppendingString:city];
    lblCountryCity.text = [countryDistrict stringByAppendingString:district];
    
    
    [imageViewMask setHidden:YES];
    
    _followingCount.text = [NSString stringWithFormat:@"%i",currentFan.fanFollows];
    _followerCount.text = [NSString stringWithFormat:@"%i",currentFan.fanFollowers];
    _picsCount.text = [NSString stringWithFormat:@"%i",currentFan.fanPicNumber];
    
    if (currentFan.isFriend || _isFriendDetail) {
        btnFriend.tag = 1;
        // Remove Friend
        //[btnFriend setTitle:[[Language sharedInstance] stringWithKey:@"noLocalizationTextExist"]  forState:UIControlStateNormal];
        [btnFriend setTitle:[[Language sharedInstance] stringWithKey:@"FriendsLevel2Profile_Unfriend"] forState:UIControlStateNormal];
        [btnFriend setBackgroundImage:[UIImage imageNamed:@"removefriends.png"] forState:UIControlStateNormal];
    }
    else {
        btnFriend.tag = 0;
        [btnFriend setTitle:[[Language sharedInstance] stringWithKey:@"InitialMessagesBetweenFans_AddFriend"]  forState:UIControlStateNormal];
        [btnFriend setBackgroundImage:[UIImage imageNamed:@"fan_addfriends.png"] forState:UIControlStateNormal];
    }
    
    if (currentFan.isFollowed) {
        btnFollow.tag = 1;
        [btnFollow setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_unFollow"] forState:UIControlStateNormal];
        [btnFollow setBackgroundImage:[UIImage imageNamed:@"fan_unfollow.png"] forState:UIControlStateNormal];
    }
    else {
        btnFollow.tag = 0;
        [btnFollow setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_Follow"] forState:UIControlStateNormal];
        [btnFollow setBackgroundImage:[UIImage imageNamed:@"fan_follow.png"] forState:UIControlStateNormal];
    }
    
    if (currentFan.isBlocked) {
        btnBlock.tag = 1;
        //                            [btn setTitle:@"UNBLOCK" forState:UIControlStateNormal];
        [btnBlock setBackgroundImage:[UIImage imageNamed:@"fan_unblock.png"] forState:UIControlStateNormal];
    }
    else {
        btnBlock.tag = 0;
        //                            [btn setTitle:@"BLOCK" forState:UIControlStateNormal];
        [btnBlock setBackgroundImage:[UIImage imageNamed:@"fan_block.png"] forState:UIControlStateNormal];
    }
    
    if (currentFan.isFavorite) {
        btnFavorite.tag = 1;
        [btnFavorite setTitle:[[Language sharedInstance] stringWithKey:@"FriendsLevel2Profile_unFavorate"] forState:UIControlStateNormal];
        [btnFavorite setBackgroundImage:[UIImage imageNamed:@"fan_unfavourity.png"] forState:UIControlStateNormal];
    }
    else {
        btnFavorite.tag = 0;
        [btnFavorite setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_Favorate"] forState:UIControlStateNormal];
        [btnFavorite setBackgroundImage:[UIImage imageNamed:@"fan_favourit.png"] forState:UIControlStateNormal];
    }
    
    if (currentFan.isPremium) {
        CALayer * l2 = [imageViewFan layer];
        [l2 setMasksToBounds:YES];
        [l2 setCornerRadius:3];
        [l2 setBorderWidth:3];
        [l2 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
    }

    [imageViewFan sd_setImageWithURL:[NSURL URLWithString:[currentFan fanPicture]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    NSArray *moreInfo = (currentFan.moreInfo == nil) ? @[] : currentFan.moreInfo;
    if ([moreInfo count] > 0) {
        [self prepareMoreInfo:moreInfo];
    }
    else
    {
        [btnMore setEnabled:NO];
    }
}


#pragma mark - Actions

- (IBAction)openMenu:(id)sender
{
    if (!isPad) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openMenu" object:nil];
    }
}

- (IBAction)gotoFanCollectionTapped:(id)sender
{
//    UIButton *btn = (UIButton *)sender;
//    FansCollectionView *fansCollectionView = (FansCollectionView *)[self.storyboard instantiateViewControllerWithIdentifier:@"FansCollectionView"];
//    fansCollectionView.viewTitle = btn.currentTitle;
//    [self.navigationController pushViewController:fansCollectionView animated:YES];
}

- (IBAction)followers_Action:(id)sender
{
    if([[CurrentUser getObject] isUser]){
        UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansCollectionView *fansCollectionView = (FansCollectionView *)[sb instantiateViewControllerWithIdentifier:@"FansCollectionView"];
        fansCollectionView.viewTitle = [[Language sharedInstance] stringWithKey:@"menu_Followers"];
        fansCollectionView.viewCount = [_followerCount.text intValue];
        fansCollectionView.fanID = self.fanID;
        fansCollectionView.isFollowers = YES;
        [self.navigationController pushViewController:fansCollectionView animated:YES];
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}

- (IBAction)following_Action:(id)sender
{
    if([[CurrentUser getObject] isUser]){
        UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansCollectionView *fansCollectionView = (FansCollectionView *)[sb instantiateViewControllerWithIdentifier:@"FansCollectionView"];
        fansCollectionView.viewTitle =[[Language sharedInstance] stringWithKey:@"menu_Following"];
        fansCollectionView.viewCount = [_followingCount.text intValue];
        fansCollectionView.fanID = self.fanID;
        fansCollectionView.isFollowers = NO;
        [self.navigationController pushViewController:fansCollectionView animated:YES];
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
    

}

- (IBAction)pics_Action:(id)sender
{
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    PicsListView *picsListView = (PicsListView *)[sb instantiateViewControllerWithIdentifier:@"PicsListView"];
    picsListView.fanID = self.fanID;
    picsListView.viewTitle = [NSString stringWithFormat:@"%@'s Pics",lblFanName.text];
    [self.navigationController pushViewController:picsListView animated:YES];
}

- (IBAction)chat_Action:(id)sender
{
    if ([[CurrentUser getObject] isUser]) {
        Fan *sender = [[Fan alloc] init];
        sender.userId = currentFanObject.fanID;
        sender.fanName = currentFanObject.fanName;
        sender.fanPicture = currentFanObject.fanPicture;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openMessaging" object:sender userInfo:nil];
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
    
  
}

- (NSDictionary *)dictionaryOfPropertiesForObject:(id)object
{
    // somewhere to store the results
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    // we'll grab properties for this class and every superclass
    // other than NSObject
    Class classOfObject = [object class];
    while(![classOfObject isEqual:[NSObject class]])
    {
        // ask the runtime to give us a C array of the properties defined
        // for this class (which doesn't include those for the superclass)
        unsigned int numberOfProperties;
        objc_property_t  *properties =
        class_copyPropertyList(classOfObject, &numberOfProperties);
        
        // go through each property in turn...
        for(
            int propertyNumber = 0;
            propertyNumber < numberOfProperties;
            propertyNumber++)
        {
            // get the name and convert it to an NSString
            NSString *nameOfProperty = [NSString stringWithUTF8String:
                                        property_getName(properties[propertyNumber])];
            
            // use key-value coding to get the property value
            id propertyValue = [object valueForKey:nameOfProperty];
            
            // add the value to the dictionary —
            // we'll want to transmit NULLs, even though an NSDictionary
            // can't store nils
            [result
             setObject:propertyValue ? propertyValue : [NSNull null]
             forKey:nameOfProperty];
        }
        
        // we took a copy of the property list, so...
        free(properties);
        
        // we'll want to consider the superclass too
        classOfObject = [classOfObject superclass];
    }
    
    
    
    // return the dictionary
    return result;
}

- (IBAction)add_Friend_Action:(id)sender
{

    if ([[CurrentUser getObject] isUser]) {
        UIButton *btn = ((UIButton *)sender);
        if (btn.tag == 0) {
            btn.tag = 1;
            //        [btn setTitle:@"REMOVE FRIEND" forState:UIControlStateNormal];
            //        [btn setBackgroundImage:[UIImage imageNamed:@"removefriends.png"] forState:UIControlStateNormal];
            [self changeFriend:YES];
        }
        else {
            btn.tag = 0;
            //        [btn setTitle:@"ADD FRIEND" forState:UIControlStateNormal];
            //        [btn setBackgroundImage:[UIImage imageNamed:@"fan_addfriends.png"] forState:UIControlStateNormal];
            [self changeFriend:NO];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}

- (void)changeFollow:(BOOL)isFollow
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] changeFollowForUserID:[[[CurrentUser getObject] getUser] userId] fanID:self.fanID follow:isFollow and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        NSLog(@"Follow data :: %@",currentClient);
                        BOOL success;
                        if (currentClient[@"status"]) {
                            success = ([currentClient[@"status"] intValue] == 1) ? YES: NO;
                            if (success) {
                                if (isFollow) {
                                    [[[CurrentUser getObject] getUser] setFollowingCount:[CurrentUser getObject].getUser.followingCount + 1];
                                    [[CurrentUser getObject] saveCurrentUser];
                                    [btnFollow setBackgroundImage:[UIImage imageNamed:@"fan_unfollow.png"] forState:UIControlStateNormal];
                                    [btnFollow setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_unFollow"] forState:UIControlStateNormal];
                                }
                                else {
                                    [[[CurrentUser getObject] getUser] setFollowingCount:[CurrentUser getObject].getUser.followingCount - 1];
                                    [[CurrentUser getObject] saveCurrentUser];
                                    [btnFollow setBackgroundImage:[UIImage imageNamed:@"fan_follow.png"] forState:UIControlStateNormal];
                                    
                                    [btnFollow setTitle:[[Language sharedInstance] stringWithKey:@"FanProfileLevel2_Follow"] forState:UIControlStateNormal];
                                }
                            }
                        }
                    }
                });
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)follow_Action:(id)sender
{
    if ([[CurrentUser getObject] isUser]) {
        UIButton *btn = ((UIButton *)sender);
        if (btn.tag == 0) {
            btn.tag = 1;
            //        [btn setTitle:@"UNFOLLOW" forState:UIControlStateNormal];
            //        [btn setBackgroundImage:[UIImage imageNamed:@"fan_follow.png"] forState:UIControlStateNormal];
            [self changeFollow:YES];
        }
        else {
            btn.tag = 0;
            //        [btn setTitle:@"FOLLOW" forState:UIControlStateNormal];
            //        [btn setBackgroundImage:[UIImage imageNamed:@"fan_follow.png"] forState:UIControlStateNormal];
            [self changeFollow:NO];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==123) {
        if (buttonIndex == 0 )
        {
            NSLog(@"YES");
            if (btnBlock.tag == 0) {
                btnBlock.tag = 1;
                //        [btn setTitle:@"UNBLOCK" forState:UIControlStateNormal];
                //        [btn setBackgroundImage:[UIImage imageNamed:@"fan_block.png"] forState:UIControlStateNormal];
                [self changeBlock:YES];
            }
            else {
                btnBlock.tag = 0;
                //        [btn setTitle:@"BLOCK" forState:UIControlStateNormal];
                //        [btn setBackgroundImage:[UIImage imageNamed:@"fan_block.png"] forState:UIControlStateNormal];
                [self changeBlock:NO];
            }

        }
        else
        {
            NSLog(@"NO");
        }
    }
}

- (IBAction)block_Action:(id)sender
{
    if ([[CurrentUser getObject] isUser]) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"fan_Block_msg"] delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"YES"] otherButtonTitles:[[Language sharedInstance] stringWithKey:@"NO"], nil];
        alert.tag = 123;
        
        [alert show];
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
    

    
//    UIButton *btn = ((UIButton *)sender);
//    if (btn.tag == 0) {
//        btn.tag = 1;
////        [btn setTitle:@"UNBLOCK" forState:UIControlStateNormal];
////        [btn setBackgroundImage:[UIImage imageNamed:@"fan_block.png"] forState:UIControlStateNormal];
//        [self changeBlock:YES];
//    }
//    else {
//        btn.tag = 0;
////        [btn setTitle:@"BLOCK" forState:UIControlStateNormal];
////        [btn setBackgroundImage:[UIImage imageNamed:@"fan_block.png"] forState:UIControlStateNormal];
//        [self changeBlock:NO];
//    }
}

- (IBAction)favorites_Action:(id)sender
{
    if ([[CurrentUser getObject] isUser]) {
        UIButton *btn = ((UIButton *)sender);
        if (btn.tag == 0) {
            btn.tag = 1;
            //        [btn setTitle:@"UNFAVORITE" forState:UIControlStateNormal];
            //        [btn setBackgroundImage:[UIImage imageNamed:@"fan_unfavourity.png"] forState:UIControlStateNormal];
            [self changeFavorite:YES];
        }
        else {
            btn.tag = 0;
            //        [btn setTitle:@"FAVORITE" forState:UIControlStateNormal];
            //        [btn setBackgroundImage:[UIImage imageNamed:@"fan_favourit.png"] forState:UIControlStateNormal];
            [self changeFavorite:NO];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}

- (IBAction)more_Action:(id)sender
{
    UIButton *btn = ((UIButton *)sender);
    if (btn.tag == 0) {
        btn.tag = 1;
        
        if (isPad) {
            [scrollView setContentOffset:CGPointMake(0, scrollView.frame.size.height - btnMore.frame.size.height-100) animated:YES];
        }
        else{
            [scrollView setContentOffset:CGPointMake(0, scrollView.frame.size.height - btnMore.frame.size.height - 44.0) animated:YES];

        }
        
        [scrollView setScrollEnabled:NO];
         _viewInfo.hidden = NO;
    }
    else {
        btn.tag = 0;
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [scrollView setScrollEnabled:YES];
        _viewInfo.hidden = YES;
    }
}


-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chatBar_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}
@end
