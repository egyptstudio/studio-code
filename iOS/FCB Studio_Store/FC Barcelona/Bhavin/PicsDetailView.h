//
//  PicsDetailView.h
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AvatarImageView.h"
#import "FullScreenCell.h"
#import "Post.h"
#import "RGMPagingScrollView.h"

@interface PicsDetailView : UIViewController<UIScrollViewDelegate,RGMPagingScrollViewDelegate,RGMPagingScrollViewDatasource>

@property (assign) Post* currentPost;
@property (assign) int wallType;
@property (assign) NSMutableArray * allPosts;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (assign) NSString *lastRequestTime;
@property (assign) NSString *timeFilter;
@property (assign) NSArray* countriesArray;
@property (assign) BOOL favoritePostsFlag;
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (assign) int selectedPostIndex;

@property (weak, nonatomic) IBOutlet RGMPagingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITableView *doomsTabel;

- (IBAction)back_Action:(id)sender;

@end
