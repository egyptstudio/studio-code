//
//  FriendsView.h
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/2/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface FriendsView : GAITrackedViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UILabel *comingSoon;
@property (weak, nonatomic) IBOutlet UIView *comingSoonView;
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UIView *adBanner;

@property (strong, nonatomic) IBOutlet UIView *viewInvite;
@property (strong, nonatomic) IBOutlet UIScrollView *segmentsView;

@property (strong, nonatomic) IBOutlet UIButton *btnInvite;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (strong, nonatomic) IBOutlet UICollectionView *cvFriends;

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (strong, nonatomic) IBOutlet UITableView *tblFriendRequests;

@property (strong, nonatomic) IBOutlet UILabel *lblRequestPlaceHolder;
@property (strong, nonatomic) IBOutlet UILabel *lblFriendsPlaceHolder;
@property (strong, nonatomic) IBOutlet UILabel *lblOfflinePlaceHolder;
@property (strong, nonatomic) IBOutlet UILabel *lblSuggestionsPlaceHolder;

@property (strong, nonatomic) NSMutableArray *arrFriends;
@property (strong, nonatomic) NSMutableArray *arrRequests;
@property (strong, nonatomic) NSMutableArray *arrSuggestions;

@property (strong, nonatomic) NSMutableArray *contactList;


@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property IBOutlet UISearchBar *theSearchBar;

- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;
@end
