//
//  PicsListView.h
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "PostsViewController.h"
#import "RGMPagingScrollView.h"

//typedef enum ScrollDirection {
//    ScrollDirectionNone,
//    ScrollDirectionRight,
//    ScrollDirectionLeft,
//    ScrollDirectionUp,
//    ScrollDirectionDown,
//    ScrollDirectionCrazy,
//} ScrollDirection;

//@protocol PostsListDelegate <NSObject>
//@required
//- (void)didScrollToDirection:(ScrollDirection)direction;
//@end

//@interface PostsViewController : UIViewController<UIScrollViewDelegate>

@interface PicsListView : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet RGMPagingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *noPhotosLabel;
@property (weak, nonatomic) IBOutlet UIView *noPhotosView;

//@property (nonatomic,assign) id <PostsListDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(assign) int requestType;

@property (nonatomic, assign) int fanID;

@property (strong, nonatomic) NSString *viewTitle;

//@property (strong, nonatomic) IBOutlet UITableView *tableView;
//@property (weak, nonatomic) IBOutlet UIView *mainContainer;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

//@property (weak, nonatomic) IBOutlet UILabel *noPhotosLabel;
//@property (weak, nonatomic) IBOutlet UIView *noPhotosView;

-(void)scrollTopTop;
//-(void)applyFilerWithIsFavorate:(BOOL)favorate andCountries:(NSArray*)array;

@end
