//
//  PicViewCell.h
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AvatarImageView.h"
#import "Post.h"

@interface PicViewCell : UITableViewCell<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIImageView *premiumImage;
@property (weak, nonatomic) IBOutlet UIImageView *postImage;
@property (weak, nonatomic) IBOutlet AvatarImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *country;
@property (weak, nonatomic) IBOutlet UIView *rankView;
@property (weak, nonatomic) IBOutlet UILabel *likesCount;
@property (weak, nonatomic) IBOutlet UILabel *postDate;

@property (weak, nonatomic) IBOutlet UILabel *commentsCount;
@property (weak, nonatomic) IBOutlet UILabel *useCount;
@property (weak, nonatomic) IBOutlet UILabel *shareCount;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *repostButton;

@property (weak, nonatomic) IBOutlet UIScrollView *tagsScroll;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *bigLoader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *smallLoader;
@property (weak, nonatomic) IBOutlet UILabel *ranks;
@property (assign)  int walltype;

@property (assign) NSString *lastRequestTime;
@property (assign) NSString *timeFilter;
//@property (assign) NSArray* countriesArray;
//@property (assign) BOOL favoritePostsFlag;

@property (strong) NSMutableArray * allPosts;

@property(assign) BOOL isEditable;

- (IBAction)like:(id)sender;
- (IBAction)comment:(id)sender;
- (IBAction)use:(id)sender;
- (IBAction)share:(id)sender;

-(void)initWithPost:(Post*)post andController:(UIViewController*)mainView;
-(void)initImages;


@end
