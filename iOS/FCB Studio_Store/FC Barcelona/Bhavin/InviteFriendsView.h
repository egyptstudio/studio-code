//
//  InviteFriendsView.h
//  FC Barcelona
//
//  Created by Bhavin Chitroda on 2/5/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendsView : UIViewController<UIAlertViewDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *inviteText;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIView *messageView;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;

@property (weak, nonatomic) IBOutlet UILabel *comingSoon;

@property (weak, nonatomic) IBOutlet UILabel *lbl_invite1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_invite2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_invite3;



- (IBAction)back_Action:(id)sender;
- (IBAction)share_Action:(id)sender;

@end
