//
//  WebImage.h
//  Westwing
//
//  Created by Eissa on 8/22/14.
//  Copyright (c) 2014 Westwing. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface WebImage : NSObject
+ (void)processImageDataWithURLString:(NSString *)urlString cacheImage:(BOOL)cache andBlock:(void (^)(NSData *imageData))processImage;
@end
