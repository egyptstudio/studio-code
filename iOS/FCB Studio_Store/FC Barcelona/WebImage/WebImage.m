//
//  WebImage.m
//  Westwing
//
//  Created by Eissa on 8/22/14.
//  Copyright (c) 2014 Westwing. All rights reserved.
//

#import "WebImage.h"
#import <QuartzCore/QuartzCore.h>

@implementation WebImage
+ (void)processImageDataWithURLString:(NSString *)urlString cacheImage:(BOOL)cache andBlock:(void (^)(NSData *imageData))processImage
{
    NSUserDefaults * defaults= [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    {
        if (cache) {
            @try {
                NSArray *listItems = [urlString componentsSeparatedByString:@"/"];
                NSString * imageName = [listItems objectAtIndex:[listItems count]-1];

                NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                documentDirectory = [documentDirectory stringByAppendingPathComponent:@"fcb_photos"];
                NSString *imageLocation = [documentDirectory stringByAppendingPathComponent:imageName];
                NSFileManager *filemgr = [NSFileManager defaultManager];
                [filemgr createDirectoryAtPath:documentDirectory
                                          withIntermediateDirectories:YES
                                                           attributes:nil
                                                                error:nil];
                if ([filemgr fileExistsAtPath: imageLocation ])
                {
                    NSURL *url = [[NSURL alloc]initFileURLWithPath:imageLocation];
                    
                    NSMutableData * imageData = [NSMutableData dataWithContentsOfURL:url];
                    
                    NSData *firstPart = [imageData subdataWithRange:NSMakeRange(0, 100)];
                    NSData *secondPart = [imageData
                                          subdataWithRange:NSMakeRange([imageData length]-100,100)];
                    
                    
                    [imageData replaceBytesInRange:NSMakeRange(0, 100)
                                            withBytes:[secondPart bytes]];
                    [imageData replaceBytesInRange:NSMakeRange([imageData length]-100,100)
                                            withBytes:[firstPart bytes]];

                    [defaults setObject:[NSDate date] forKey:imageName];
                    processImage(imageData);
                }
                else
                {
                    NSURL *url = [NSURL URLWithString:urlString];
                    dispatch_queue_t callerQueue = dispatch_get_main_queue();
                    dispatch_queue_t downloadQueue = dispatch_queue_create("WebImage.Queue", NULL);
                    dispatch_async(downloadQueue, ^{
                        NSData * imageData = [NSData dataWithContentsOfURL:url];
                        dispatch_async(callerQueue, ^{
                            if(imageData==nil)
                                processImage(nil);
                            else{
                                
                                NSMutableData *decodedImage = [NSMutableData dataWithData:imageData] ;
                                NSData *firstPart  = [decodedImage subdataWithRange:NSMakeRange(0, 100)];
                                NSData *secondPart = [decodedImage
                                                      subdataWithRange:NSMakeRange([decodedImage length]-100,100)];

                                
                                [decodedImage replaceBytesInRange:NSMakeRange(0, 100)
                                                 withBytes:[secondPart bytes]];
                                [decodedImage replaceBytesInRange:NSMakeRange([decodedImage length]-100,100)
                                                        withBytes:[firstPart bytes]];

                                
                                [defaults setObject:[NSDate date] forKey:imageName];
                                [decodedImage writeToURL:[NSURL fileURLWithPath:imageLocation] atomically:NO];
                                NSLog(@"%@",[self contentTypeForImageData:imageData]);
                                processImage(imageData);
                            }
                            
                        });
                    });
                }
                
            }
            @catch (NSException *exception) {
                processImage(nil);
            }
            
        }
        else{
            NSURL *url = [NSURL URLWithString:urlString];
            dispatch_queue_t callerQueue = dispatch_get_main_queue();
            dispatch_queue_t downloadQueue = dispatch_queue_create("WebImage.Queue", NULL);
            dispatch_async(downloadQueue, ^{
                NSData * imageData = [NSData dataWithContentsOfURL:url];
                dispatch_async(callerQueue, ^{
                    processImage(imageData);
                });
            });
            
        }
        
    }
    [defaults synchronize];
}

+ (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}
@end
