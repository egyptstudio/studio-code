//
//  FullScreenHorizontalViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/28/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FullScreenHorizontalViewController.h"
#import "FullScreenPageViewController.h"
@interface FullScreenHorizontalViewController ()

@end

@implementation FullScreenHorizontalViewController
{
    NSInteger count;
}
@synthesize allPosts;

- (void)viewDidLoad {
    [super viewDidLoad];
    _scrollView.scrollDirection = RGMScrollDirectionHorizontal;
       
}
-(NSInteger)pagingScrollViewNumberOfPages:(RGMPagingScrollView *)pagingScrollView{
    return [allPosts count];
}

- (UIView *)pagingScrollView:(RGMPagingScrollView *)pagingScrollView viewForIndex:(NSInteger)idx
{
    FullScreenPageViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FullScreenPageViewController"];
    [view viewPost:allPosts[idx]];
    return [view view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareWithPost:(Post*)post{
//    [allPosts addObject:post];
//    [_scrollView reloadData];
}

@end
