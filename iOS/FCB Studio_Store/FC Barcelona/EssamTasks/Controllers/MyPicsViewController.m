//
//  MyPicsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/14/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "MyPicsViewController.h"
#import "WebImage.h"
#import "RestServiceAgent.h"
#import "WallManager.h"
#import "CurrentUser.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "StudioManager.h"
#import "PECropView.h"

@interface MyPicsViewController ()

@end

@implementation MyPicsViewController
{
    id allImages;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [_pageTiles setText:[[Language sharedInstance] stringWithKey:@"Home_tab4"]];
    [self getMyPhotos];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getMyPhotos
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance]getMyPosts:[[CurrentUser getObject] getUser].userId postsNo:50 lastRequestTime:@"" timeFilter:@"0" and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient count]<=0) {
                        [self noImages];
                    }
                    else{
                        allImages = currentClient;
                        [_collection reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [allImages count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    UIImageView* image   = (UIImageView*)[cell viewWithTag:1];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:2] ;
    
    
    Post *currentPost = (Post*)[allImages objectAtIndex:indexPath.row];

    [image sd_setImageWithURL:[NSURL URLWithString:[currentPost postPhotoUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    
    CALayer * l8 = [cell layer];
    [l8 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
    [l8 setBorderWidth:2.0];
    [l8 setMasksToBounds:YES];
    [l8 setCornerRadius:4];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    UIImageView* image   = (UIImageView*)[cell viewWithTag:1];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:2] ;
    Post *currentPost = (Post*)[allImages objectAtIndex:indexPath.row];

    if (![loader isAnimating]) {
        [_delegate didSelectImage:image.image withUrl:[currentPost postPhotoUrl]];
        [self back_Action:nil];
    }
}

-(IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)noImages
{
    
    UILabel* errorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    [errorLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [errorLabel setTextAlignment:NSTextAlignmentCenter];
    [errorLabel setTextColor:[UIColor whiteColor]];
    [errorLabel setText:[[Language sharedInstance] stringWithKey:@"HomeInitialState_MypicsNoPhotosMessage"]];
    errorLabel.center = CGPointMake([[UIScreen mainScreen] bounds].size.width/2, [[UIScreen mainScreen] bounds].size.height/2);
    [self.view addSubview:errorLabel];
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
