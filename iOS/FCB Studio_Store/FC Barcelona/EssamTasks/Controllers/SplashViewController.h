//
//  SplashViewController.h
//  FC Barcelona
//
//  Created by Eissa on 10/7/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView   *transperentView;
@property (weak, nonatomic) IBOutlet UIView   *dialog;
@property (weak, nonatomic) IBOutlet UILabel  *dialogTitle;
@property (weak, nonatomic) IBOutlet UILabel  *dialogMessage;
@property (weak, nonatomic) IBOutlet UIButton *dialogButton;
@property (weak, nonatomic) IBOutlet UIButton *dialogCloseButton;

- (IBAction)update_Aciton:(id)sender;
- (IBAction)close_Aciton:(id)sender;

@end
