//
//  HelpScreen.h
//  FC Barcelona
//
//  Created by Essam Eissa on 3/5/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelpScreen : NSObject
@property NSString* screenTitle;
@property NSString* screenImageUrl;
@property NSString* screenIconUrl;
@property NSString* screenDescription;
@end
