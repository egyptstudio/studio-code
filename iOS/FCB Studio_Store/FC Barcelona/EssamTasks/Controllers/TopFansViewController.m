//
//  TopFansViewController.m
//  FC Barcelona
//
//  Created by Eissa on 10/6/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "TopFansViewController.h"

#import "Top10ViewController.h"
#import "FansManager.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "Fan.h"
#import "AvatarImageView.h"
#import "WebImage.h"
#import "CurrentUser.h"

@interface TopFansViewController ()
{
    id resultsDictionary;
    NSUserDefaults * defaults;

}
@end

@implementation TopFansViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}
-(void)viewDidAppear:(BOOL)animated
{
    [self getTopFans];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)getTopFans
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[FansManager getInstance]getTopFans:^(id currentClient) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        resultsDictionary = currentClient;
                        [_tableView reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)followFan:(UIButton*)sender
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        @try {
            if ([RestServiceAgent internetAvailable]) {
                [[FansManager getInstance] followFan:[[CurrentUser getObject] getUser].userId
                                                 fanId:[(Fan*)[resultsDictionary objectAtIndex:indexPath.row] userId]
                                                   and:^(id currentClient) {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           [SVProgressHUD dismiss];
                                                           if (currentClient) {
                                                               if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                               {
                                                                   [sender setHidden:YES];
                                                               }
                                                           }
                                                       });
                                                   }onFailure:^(NSError *error) {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           [SVProgressHUD dismiss];
                                                           
                                                           UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                           [alert show];
                                                       });
                                                   }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
}
- (IBAction)followAll:(id)sender {
    NSMutableArray* allFans = [[NSMutableArray alloc] init];
    for (int i=0;i<[resultsDictionary count];i++) {
        [allFans addObject:[NSString stringWithFormat:@"%d",[(Fan*)[resultsDictionary objectAtIndex:i] userId]]];
    }
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[FansManager getInstance] followFans:[[CurrentUser getObject] getUser].userId
                                           fansIds:allFans
                                             and:^(id currentClient) {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [SVProgressHUD dismiss];
                                                     if (currentClient) {
                                                         if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                         {
                                                             _tableView.hidden = YES;
                                                         }
                                                     }
                                                 });
                                             }onFailure:^(NSError *error) {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [SVProgressHUD dismiss];
                                                     
                                                     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                     [alert show];
                                                 });
                                             }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }

}
-(void)saveUserData:(User*)user
{
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"user"];
    [defaults synchronize];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [resultsDictionary count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];

    UILabel *fanName = (UILabel*)[cell viewWithTag:1];
    UILabel *fanCity = (UILabel*)[cell viewWithTag:2];
    AvatarImageView* playerImage     = (AvatarImageView*)[cell viewWithTag:3];
    UIButton *fanFollow = (UIButton*)[cell viewWithTag:4];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:5] ;

    fanName.text =[(Fan*)[resultsDictionary objectAtIndex:indexPath.row] userName];
    fanCity.text =[(Fan*)[resultsDictionary objectAtIndex:indexPath.row] countryText];

    playerImage.borderColor = [UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1];
    playerImage.borderWidth = 3.0;
    
    [playerImage sd_setImageWithURL:[NSURL URLWithString:[(Fan*)[resultsDictionary objectAtIndex:indexPath.row] profilePic]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    [fanFollow addTarget:self action:@selector(followFan:) forControlEvents:UIControlEventTouchUpInside];
  
    return cell;
}
@end
