//
//  PostDetailsViewController.m
//  FC Barcelona
//
//  Created by Eissa on 9/10/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "PostDetailsViewController.h"
#import "ShareManagerViewController.h"
#import "CommentsViewController.h"
#import "WebImage.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "WallManager.h"
#import "RestServiceAgent.h"
#import "CaptureViewController.h"
#import "PostCell.h"
#import "CurrentUser.h"

@interface PostDetailsViewController ()

@end

@implementation PostDetailsViewController
{
    ShareManagerViewController * shareView;
    CommentsViewController * commentsView;
    __weak IBOutlet UITextView *captionTextView;
}
@synthesize userPost;
@synthesize isEditable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMenus)];
//    tap.cancelsTouchesInView = YES;
//    tap.numberOfTapsRequired = 1;
//    [_userImage addGestureRecognizer:tap];
//    _userImage.userInteractionEnabled = YES;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    _imageName.text = [userPost userName];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *theDate = [dateFormat dateFromString:[[userPost photo] uploadedDate]];
//    _postDate.text = [@"posted " stringByAppendingFormat:@"%@",[self relativeDateStringForDate:theDate]];
//    captionTextView.text=userPost.photo.caption;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PostCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PostCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PostCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    Post *currentPost = userPost;
    cell.isEditable = isEditable ;
    [cell initWithPost:currentPost andController:self];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.postImage setContentMode:UIViewContentModeScaleAspectFit];

    return cell;
}

- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    return [dateFormat stringFromDate:date];
    
    
//    NSCalendarUnit units = NSDayCalendarUnit | NSWeekOfYearCalendarUnit |
//    NSMonthCalendarUnit | NSYearCalendarUnit;
//    
//    // if `date` is before "now" (i.e. in the past) then the components will be positive
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
//                                                                   fromDate:date
//                                                                     toDate:[NSDate date]
//                                                                    options:0];
//    
//    if (components.year > 0) {
//        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
//    } else if (components.month > 0) {
//        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
//    } else if (components.weekOfYear > 0) {
//        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
//    } else if (components.day > 0) {
//        if (components.day > 1) {
//            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
//        } else {
//            return @"Yesterday";
//        }
//    } else {
//        return @"Today";
//    }
}

-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
