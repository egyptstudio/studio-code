//
//  ShopViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/2/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "ShopViewController.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "ShopManager.h"
#import "CurrentUser.h"
#import "FCBPurchase.h"
#import "GetAndPushCashedData.h"
#import "FreePointsViewController.h"
#import "StudioManager.h"
#import "UsersManager.h"
#import "FreePointsDetailsViewController.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@interface ShopViewController ()<NSURLConnectionDataDelegate,NSURLConnectionDelegate,UIWebViewDelegate>

@end

@implementation ShopViewController
{
    BOOL nikeStore;
    BOOL premium;
    BOOL points;
    BOOL redem;
    BOOL loadedOnce;
    int premuimDuration;
    int pointsCount;
    NSString* appIdentifier;
    
    __weak IBOutlet UILabel *duration6Labels;
    __weak IBOutlet UILabel *duration1Labels;
    __weak IBOutlet UILabel *duration3Labels;
    __weak IBOutlet UILabel *duration12Labels;
    NSURLConnection* premiumStatusConnection;
    NSURLConnection* subscribePremiumConnection;
    NSURLConnection* buyPointsConnection;
    NSURLConnection* getProductsConnection;
    NSMutableArray* products;
    NSMutableArray* identifiers;
    NSArray* InAppProducts;
    __weak IBOutlet UIView *premiumPackagesView;
    __weak IBOutlet UILabel *goldLabell;
    __weak IBOutlet UILabel *basicLabell;
    __weak IBOutlet UILabel *bronzeLabell;
    __weak IBOutlet UILabel *silverLabell;
    __weak IBOutlet UILabel *price1Label;
    __weak IBOutlet UILabel *price3Label;
    __weak IBOutlet UILabel *price6Label;
    __weak IBOutlet UILabel *price12Label;
    __weak IBOutlet UIView *pointsView;
    __weak IBOutlet UILabel *Points100Labell;
    __weak IBOutlet UILabel *Points300Labell;
    __weak IBOutlet UILabel *Points500Labell;
    __weak IBOutlet UILabel *Points800Labell;
    __weak IBOutlet UILabel *Points100PriceLabell;
    __weak IBOutlet UILabel *Points300PriceLabell;
    __weak IBOutlet UILabel *Points500PriceLabell;
    __weak IBOutlet UILabel *Points800PriceLabell;
    
    __weak IBOutlet UILabel *Points1FreeLabell;
    __weak IBOutlet UILabel *Points2FreeLabell;
    __weak IBOutlet UILabel *Points3FreeLabell;
    __weak IBOutlet UILabel *Points4FreeLabell;
    __weak IBOutlet UILabel *premiumPoints1Labell;
    __weak IBOutlet UILabel *premiumPoints2Labell;
    __weak IBOutlet UILabel *premiumPoints3Labell;
    __weak IBOutlet UILabel *premiumPoints4Labell;
    __weak IBOutlet UIView *redemptionImage;
    
    __weak IBOutlet UILabel *remainingSub;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    /*[[ShopManager getInstance] LoadInAppProductIDs:^(id currentClient) {
        NSLog(@"%@",currentClient);
    } onFailure:^(NSError *error) {
        NSLog(@"%@",error.description);
    }];*/

    [_segmentsView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:84/255.0f blue:164/255.0f alpha:1]];
    [_segments setFrame:_segmentsView.bounds];
    [_segmentsViewTap setFrame:_segments.bounds];
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    UIFont *fontBold = [UIFont boldSystemFontOfSize:14.0f];
    
    UIColor *color = [UIColor whiteColor];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesBold = [NSDictionary dictionaryWithObjectsAndKeys:
                                    fontBold,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    [_segments setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [_segments setTitleTextAttributes:attributesBold forState:UIControlStateSelected];
    
    
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_normal.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"]
                         forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    UIView *devider = [[UIView alloc] initWithFrame:CGRectMake(0, 0, .5, _segments.frame.size.height)];
    [devider setBackgroundColor:[UIColor whiteColor]];
    UIGraphicsBeginImageContextWithOptions(devider.bounds.size, devider.opaque, 0.0);
    [devider.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * deviderImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateSelected
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateNormal
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    //////////////////

    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollToTop:)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    [_segmentsViewTap addGestureRecognizer:tap1];
    

}
-(void)scrollToTop:(UITapGestureRecognizer*)tap{
    CGPoint tapPoint = [tap locationInView:tap.view ];
    NSInteger index ;
    if (tapPoint.x > 0 && tapPoint.x < tap.view.frame.size.width/4) {
        index = 0;
    }else if (tapPoint.x > tap.view.frame.size.width/4 && tapPoint.x < (tap.view.frame.size.width/4)*2) {
        index = 1;
    }else if (tapPoint.x > (tap.view.frame.size.width/4)*2 && tapPoint.x < (tap.view.frame.size.width/4)*3) {
        index = 2;
    }else if (tapPoint.x > (tap.view.frame.size.width/4)*3 && tapPoint.x < (tap.view.frame.size.width/4)*4) {
        index = 3;
    }
    [_segments setSelectedSegmentIndex:index];
    [self segmentChanged:_segments];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self initView];

    self.screenName = @"Shop Screen";
    
    if ([GetAndPushCashedData getObjectFromCash:@"OpenPremTab"]) {
        [GetAndPushCashedData removeObjectWithAction:@"OpenPremTab"];
        _segments.selectedSegmentIndex = 1;
        [self segmentChanged:_segments];
    }else if ([GetAndPushCashedData getObjectFromCash:@"OpenPointsTab"]) {
        [GetAndPushCashedData removeObjectWithAction:@"OpenPointsTab"];
        _segments.selectedSegmentIndex = 2;
        [self segmentChanged:_segments];
    }
//    else{
//        _segments.selectedSegmentIndex = 0;
//        [self segmentChanged:_segments];
//    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotificationn object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productCanceled:) name:IAPHelperProductFailedNotificationn object:nil];
    [self adjustSegments];

}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initView{

    [duration1Labels  setText:[[Language sharedInstance] stringWithKey:@"PremiumPackages_month"]];
    [duration3Labels  setText:[[Language sharedInstance] stringWithKey:@"PremiumPackages_3month"]];
    [duration6Labels setText:[[Language sharedInstance] stringWithKey:@"PremiumPackages_6month"]];
    [duration12Labels   setText:[[Language sharedInstance] stringWithKey:@"PremiumPackages_year"]];
    
    
    [basicLabell  setText:[[Language sharedInstance] stringWithKey:@"basic"]];
    [bronzeLabell setText:[[Language sharedInstance] stringWithKey:@"bronze"]];
    [silverLabell setText:[[Language sharedInstance] stringWithKey:@"silver"]];
    [goldLabell   setText:[[Language sharedInstance] stringWithKey:@"gold"]];
    
    [basicLabell setFont:[UIFont fontWithName:@"Barcelona2012byDaniloSL" size:14]];
    [bronzeLabell setFont:[UIFont fontWithName:@"Barcelona2012byDaniloSL" size:14]];
    [silverLabell setFont:[UIFont fontWithName:@"Barcelona2012byDaniloSL" size:14]];
    [goldLabell setFont:[UIFont fontWithName:@"Barcelona2012byDaniloSL" size:14]];
    
    [duration1Labels setFont:[UIFont fontWithName:@"Interstate-Light" size:18]];
    [duration3Labels setFont:[UIFont fontWithName:@"Interstate-Light" size:18]];
    [duration6Labels setFont:[UIFont fontWithName:@"Interstate-Light" size:18]];
    [duration12Labels setFont:[UIFont fontWithName:@"Interstate-Light" size:18]];
    
    
    [price1Label setFont:[UIFont fontWithName:@"Interstate-Light" size:16]];
    [price3Label setFont:[UIFont fontWithName:@"Interstate-Light" size:16]];
    [price6Label setFont:[UIFont fontWithName:@"Interstate-Light" size:16]];
    [price12Label setFont:[UIFont fontWithName:@"Interstate-Light" size:16]];

    
    [Points100Labell setFont:[UIFont fontWithName:@"Interstate-Light" size:14]];
    [Points300Labell setFont:[UIFont fontWithName:@"Interstate-Light" size:14]];
    [Points500Labell setFont:[UIFont fontWithName:@"Interstate-Light" size:14]];
    [Points800Labell setFont:[UIFont fontWithName:@"Interstate-Light" size:14]];
    
    [Points100PriceLabell setFont:[UIFont fontWithName:@"Interstate-Light" size:14]];
    [Points300PriceLabell setFont:[UIFont fontWithName:@"Interstate-Light" size:14]];
    [Points500PriceLabell setFont:[UIFont fontWithName:@"Interstate-Light" size:14]];
    [Points800PriceLabell setFont:[UIFont fontWithName:@"Interstate-Light" size:14]];
    
    [_freePointsButton setTitle:[[Language sharedInstance] stringWithKey:@"FreePoints"] forState:UIControlStateNormal];
    //////
    
    NSString* rgbValue = @"#464646";
//    [basicLabell setTextColor:[self colorWithHexString:rgbValue]];
//    [bronzeLabell setTextColor:[self colorWithHexString:rgbValue]];
//    [silverLabell setTextColor:[self colorWithHexString:rgbValue]];
//    [goldLabell setTextColor:[self colorWithHexString:rgbValue]];
//    
//    [duration1Labels setTextColor:[self colorWithHexString:rgbValue]];
//    [duration3Labels setTextColor:[self colorWithHexString:rgbValue]];
//    [duration6Labels setTextColor:[self colorWithHexString:rgbValue]];
//    [duration12Labels setTextColor:[self colorWithHexString:rgbValue]];
//    
//    
//    [price1Label setTextColor:[self colorWithHexString:rgbValue]];
//    [price3Label setTextColor:[self colorWithHexString:rgbValue]];
//    [price6Label setTextColor:[self colorWithHexString:rgbValue]];
//    [price12Label setTextColor:[self colorWithHexString:rgbValue]];
    
    
    [Points100Labell setTextColor:[self colorWithHexString:rgbValue]];
    [Points300Labell setTextColor:[self colorWithHexString:rgbValue]];
    [Points500Labell setTextColor:[self colorWithHexString:rgbValue]];
    [Points800Labell setTextColor:[self colorWithHexString:rgbValue]];

    [Points100PriceLabell setTextColor:[self colorWithHexString:rgbValue]];
    [Points300PriceLabell setTextColor:[self colorWithHexString:rgbValue]];
    [Points500PriceLabell setTextColor:[self colorWithHexString:rgbValue]];
    [Points800PriceLabell setTextColor:[self colorWithHexString:rgbValue]];
    
    
    CALayer * l = [_premuimPacView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    [l setBorderWidth:2];
    [l setBorderColor:[[UIColor whiteColor] CGColor]];

    CALayer * l1 = [_pointsPacView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    [l1 setBorderWidth:2];
    [l1 setBorderColor:[[UIColor whiteColor] CGColor]];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenPremiumPackage)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    [_premuimPacView addGestureRecognizer:tap];
    _premuimPacView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenPointsPackage)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    [_pointsPacView addGestureRecognizer:tap1];
    _pointsPacView.userInteractionEnabled = YES;
    

    [_premuimPac setFont:[UIFont fontWithName:@"Barcelona2012byDaniloSL" size:27]];
    [_pointsPac setFont:[UIFont fontWithName:@"Barcelona2012byDaniloSL" size:27]];
    
    [self adjustSegments];
    [_segments setSelectedSegmentIndex:0];
    [premiumPackagesView setAlpha:0];
    [pointsView setAlpha:0];
    [redemptionImage setAlpha:0];
    redem = NO;
    premium = NO;
    loadedOnce = NO;
    nikeStore = YES;
    points = NO;
    redem = NO;
    _webView.opaque=NO;
    _webView.backgroundColor=[UIColor clearColor];
    if([RestServiceAgent internetAvailable])
    {
        [self getInAppProducts];
        [self getStoreUrl];

    }else
    {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    _pageTitle.text = [[Language sharedInstance] stringWithKey:@"Home_Shop"] ;
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_comingSoon setText:[[Language sharedInstance] stringWithKey:@"coming_soon"]];

    [_pack1Label setText:@""];
    [_pack2Label setText:@""];
    [_pack3Label setText:@""];
    [_pack4Label setText:@""];

}
-(void)getInAppProducts{

    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        [[ShopManager getInstance] LoadInAppProductIDs:^(id currentClient) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (currentClient) {
                    [SVProgressHUD dismiss];
                    
                    products = [[NSMutableArray alloc]initWithArray:currentClient[@"data"]];
                    identifiers = [[NSMutableArray alloc]init];
                    NSArray* pointsProducts =  products[0][@"Points"];
                    NSArray* premuimProducts = products[0][@"Premium"];


                    
                    for(NSDictionary* productInApp in pointsProducts)
                    {
                        [identifiers addObject:[productInApp objectForKey:@"inAppId"]];
                    }
                    
                    for(NSDictionary* productInApp in premuimProducts)
                    {
                        [identifiers addObject:[productInApp objectForKey:@"inAppId"]];
                    }
                    
                    [self loadInAppProductsFromAppStore];
                    
                    [price1Label setText:[NSString stringWithFormat:@"%@%@",[[premuimProducts objectAtIndex:0] objectForKey:@"price"],@" $"]];
                    [price3Label setText:[NSString stringWithFormat:@"%@%@",[[premuimProducts objectAtIndex:1] objectForKey:@"price"],@" $"]];
                    [price6Label setText:[NSString stringWithFormat:@"%@%@",[[premuimProducts objectAtIndex:2] objectForKey:@"price"],@" $"]];
                    [price12Label setText:[NSString stringWithFormat:@"%@%@",[[premuimProducts objectAtIndex:3] objectForKey:@"price"],@" $"]];
                    
                    [premiumPoints1Labell setText:[[[premuimProducts objectAtIndex:0] objectForKey:@"description"] stringByReplacingOccurrencesOfString:@" + " withString:@" +\n"]];
                    [premiumPoints2Labell setText:[[[premuimProducts objectAtIndex:1] objectForKey:@"description"] stringByReplacingOccurrencesOfString:@" + " withString:@" +\n"]];
                    [premiumPoints3Labell setText:[[[premuimProducts objectAtIndex:2] objectForKey:@"description"] stringByReplacingOccurrencesOfString:@" + " withString:@" +\n"]];
                    [premiumPoints4Labell setText:[[[premuimProducts objectAtIndex:3] objectForKey:@"description"] stringByReplacingOccurrencesOfString:@" + " withString:@" +\n"]];
                    
                    
                    @try {
                        [Points100Labell setText:[[[[pointsProducts objectAtIndex:0] objectForKey:@"description"] componentsSeparatedByString:@" + "] objectAtIndex:0]];
                        
                        [Points100PriceLabell setText:[NSString stringWithFormat:@"%@%@",[[pointsProducts objectAtIndex:0] objectForKey:@"price"],@" $"]];
                    }
                    @catch (NSException *exception) {}
                    @try {
                        [Points300Labell setText:[[[[pointsProducts objectAtIndex:1] objectForKey:@"description"] componentsSeparatedByString:@" + "] objectAtIndex:0]];
                        
                        [Points300PriceLabell setText:[NSString stringWithFormat:@"%@%@",[[pointsProducts objectAtIndex:1] objectForKey:@"price"],@" $"]];
                    }
                    @catch (NSException *exception) {}
                    @try {
                        [Points500Labell setText:[[[[pointsProducts objectAtIndex:2] objectForKey:@"description"] componentsSeparatedByString:@" + "] objectAtIndex:0]];
                        
                        [Points500PriceLabell setText:[NSString stringWithFormat:@"%@%@",[[pointsProducts objectAtIndex:2] objectForKey:@"price"],@" $"]];
                    }
                    @catch (NSException *exception) {}
                    @try {
                        [Points800Labell setText:[[[[pointsProducts objectAtIndex:3] objectForKey:@"description"] componentsSeparatedByString:@" + "] objectAtIndex:0]];
                        
                        [Points800PriceLabell setText:[NSString stringWithFormat:@"%@%@",[[pointsProducts objectAtIndex:3] objectForKey:@"price"],@" $"]];
                    }
                    @catch (NSException *exception) {}
                    
                    @try {
                        [Points1FreeLabell setText:[[[[pointsProducts objectAtIndex:0] objectForKey:@"description"] componentsSeparatedByString:@" + "] objectAtIndex:1]];
                    }
                    @catch (NSException *exception) {}
                    
                    @try {
                        [Points2FreeLabell setText:[[[[pointsProducts objectAtIndex:1] objectForKey:@"description"] componentsSeparatedByString:@" + "] objectAtIndex:1]];
                    }
                    @catch (NSException *exception) {}
                    
                    @try {
                        [Points3FreeLabell setText:[[[[pointsProducts objectAtIndex:2] objectForKey:@"description"] componentsSeparatedByString:@" + "] objectAtIndex:1]];
                    }
                    @catch (NSException *exception) {}
                    
                    @try {
                        [Points4FreeLabell setText:[[[[pointsProducts objectAtIndex:3] objectForKey:@"description"] componentsSeparatedByString:@" + "] objectAtIndex:1]];
                    }
                    @catch (NSException *exception) {}
                    
                }
            });
        }onFailure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }];
}
-(void)getStoreUrl{
    [[ShopManager getInstance] getStoreLink:^(id currentClient) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (currentClient) {
                [SVProgressHUD dismiss];
                [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:currentClient[@"data"]]]];
            }
        });
    }onFailure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }];
}
-(void)OpenPremiumPackage{
    
    if([RestServiceAgent internetAvailable])
    {
        [self getMyPremiumStatus];
    }else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
    
    if(nikeStore)
    {
        [premiumPackagesView setAlpha:1];
        [_webView setAlpha:0.0];
        premium = YES;
        nikeStore = NO;
    }else if(points)
    {
        [pointsView setAlpha:0.0];
        [premiumPackagesView setAlpha:1.0];
        points = NO;
        premium = YES;
    }else if(redem)
    {
        [redemptionImage setAlpha:0.0];
        [premiumPackagesView setAlpha:1.0];
        redem = NO;
        premium = YES;
    }
}

-(void)OpenPointsPackage{
    if(nikeStore)
    {
        [pointsView setAlpha:1];
        [_webView setAlpha:0.0];
        points = YES;
        nikeStore = NO;
    }else if(premium)
    {
        [pointsView setAlpha:1.0];
        [premiumPackagesView setAlpha:0.0];
        points = YES;
        premium = NO;
    }else if(redem)
    {
        [pointsView setAlpha:1.0];
        [redemptionImage setAlpha:0.0];
        points = YES;
        redem = NO;
    }
}

-(void)OpenRedem{
    if(nikeStore)
    {
        [redemptionImage setAlpha:1];
        [_webView setAlpha:0.0];
        redem = YES;
        nikeStore = NO;
    }else if(premium)
    {
        [redemptionImage setAlpha:1.0];
        [premiumPackagesView setAlpha:0.0];
        redem = YES;
        premium = NO;
    }else if(points)
    {
        [redemptionImage setAlpha:1.0];
        [pointsView setAlpha:0.0];
        points = NO;
        redem = YES;
    }
}

-(void)OpenNikeStore{
    
    if(premium)
    {
        [premiumPackagesView setAlpha:0];
        [_webView setAlpha:1.0];
        premium = NO;
        nikeStore = YES;
    }else if(points)
    {
        [pointsView setAlpha:0];
        [_webView setAlpha:1.0];
        points = NO;
        nikeStore = YES;
    }else if(redem)
    {
        [redemptionImage setAlpha:0];
        [_webView setAlpha:1.0];
        redem = NO;
        nikeStore = YES;
    }
    if(!loadedOnce)
    {
        [self getInAppProducts];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)segmentChanged:(id)sender{
    if([RestServiceAgent internetAvailable])
    {
        if([self.segments selectedSegmentIndex] == 0)
        {
            [self OpenNikeStore];
        }else if([self.segments selectedSegmentIndex] == 1)
        {
            [self OpenPremiumPackage];
        }else if([self.segments selectedSegmentIndex] == 2)
        {
            [self OpenPointsPackage];
        }else if([self.segments selectedSegmentIndex] == 3)
        {
            [self OpenRedem];
        }
    }else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
    loadedOnce = YES;
    [webView.scrollView setContentSize:CGSizeMake(webView.scrollView.contentSize.width,
                                                  webView.scrollView.contentSize.height+65)];
}

- (IBAction)buyPremiumPackage:(UIButton*)sender {
    
    
    premuimDuration = sender.tag;
    if(premuimDuration == 30)
    {
        appIdentifier = [identifiers objectAtIndex:4];
    }else if(premuimDuration == 90)
    {
        appIdentifier = [identifiers objectAtIndex:5];
    }else if(premuimDuration == 180)
    {
        appIdentifier = [identifiers objectAtIndex:6];
    }else
    {
        appIdentifier = [identifiers objectAtIndex:7];
    }
    pointsCount = -1;
    if([RestServiceAgent internetAvailable])
    {
        [self getMyPremiumStatus];
    }else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)getMyPremiumStatus{
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if([RestServiceAgent internetAvailable])
        {
            [[ShopManager getInstance] myPremiumStatus:[[CurrentUser getObject]getUser].userId and:^(id currentClient) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [SVProgressHUD dismiss];
                  if([[[currentClient objectForKey:@"data"] objectForKey:@"premiumEnd"] intValue] > 0)
                  {
                      NSTimeInterval epoch = [[[currentClient objectForKey:@"data"] objectForKey:@"premiumEnd"] doubleValue];
                      NSDate * date = [NSDate dateWithTimeIntervalSince1970:epoch];
                      NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
                      [formatter setDateFormat:@"dd-MM-yyyy"];
                      NSString* message = [NSString stringWithFormat:@"%@ : %@",[[Language sharedInstance] stringWithKey:@"ValidSubscription"],[formatter stringFromDate:date]];
                      
                      remainingSub.text = message;
                      UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                      [alert show];
                  }else
                  {
                      remainingSub.text = @"";
                      [self buyAProductWithIdentifier:appIdentifier];
                  }
              });
          }onFailure:^(NSError *error) {
              [SVProgressHUD dismiss];
              UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
              [alert show];
              
          }];
            
        }
    }
    @catch (NSException *exception) {}

}

- (IBAction)buyPoints:(UIButton*)sender {
    pointsCount = sender.tag;
    if(pointsCount == 100)
    {
        appIdentifier = [identifiers objectAtIndex:0];
    }else if(pointsCount == 300)
    {
        appIdentifier = [identifiers objectAtIndex:1];
    }else if(pointsCount == 500)
    {
        appIdentifier = [identifiers objectAtIndex:2];
    }else
    {
        appIdentifier = [identifiers objectAtIndex:3];
    }
    premuimDuration = -1;
    if([RestServiceAgent internetAvailable])
    {
        [self buyAProductWithIdentifier:appIdentifier];
    }else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(connection == premiumStatusConnection)
    {
        [SVProgressHUD dismiss];
        NSError* error2;
        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error2];
        
        if([[dict objectForKey:@"status"] intValue] == 2)
        {
            if([[[dict objectForKey:@"data"] objectForKey:@"premiumEnd"] intValue] > 0)
            {
                NSTimeInterval epoch = [[[dict objectForKey:@"data"] objectForKey:@"premiumEnd"] doubleValue];
                NSDate * date = [NSDate dateWithTimeIntervalSince1970:epoch];
                NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
                [formatter setDateFormat:@"dd-MM-yyyy"];
                NSString* message = [NSString stringWithFormat:@"%@ : %@",[[Language sharedInstance] stringWithKey:@"ValidSubscription"],[formatter stringFromDate:date]];
                
                remainingSub.text = message;
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }else
            {
                remainingSub.text = @"";
                [self buyAProductWithIdentifier:appIdentifier];
            }
        }else if([[dict objectForKey:@"status"] intValue] == -1)
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"webservice parameters error" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }else if([[dict objectForKey:@"status"] intValue] == -2)
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"no_data"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"error"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }else if(connection == subscribePremiumConnection || connection == buyPointsConnection)
    {
        [SVProgressHUD dismiss];
        NSError* error2;
        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error2];
        
        if([[dict objectForKey:@"status"] intValue] == 2)
        {
            if(connection == buyPointsConnection)
            {
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:[NSString stringWithFormat:@"%@%i%@",@"You have purchased, ",pointsCount,@" Points"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
                [[[CurrentUser getObject] getUser] setCredit:[CurrentUser getObject].getUser.credit + pointsCount];
                [[CurrentUser getObject] saveCurrentUser];
                pointsCount = -1 ;
            }else if(connection == subscribePremiumConnection)
            {
                NSString* messagePart = @"";
                if(premuimDuration == 30)
                {
                    messagePart = [[Language sharedInstance] stringWithKey:@"PremiumPackages_month"] ;
                }else if(premuimDuration == 90)
                {
                    messagePart = [[Language sharedInstance] stringWithKey:@"PremiumPackages_3month"];
                }else if(premuimDuration == 160)
                {
                    messagePart = [[Language sharedInstance] stringWithKey:@"PremiumPackages_6month"];
                }else if(premuimDuration == 365)
                {
                    messagePart = [[Language sharedInstance] stringWithKey:@"PremiumPackages_year"];
                }
                
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:[NSString stringWithFormat:@"%@%@%@",@"You have subscribed successfully, ",messagePart,@" Package"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
                [[[CurrentUser getObject] getUser] setIsPremium:YES];
                [[CurrentUser getObject] saveCurrentUser];
                premuimDuration = -1;
            }
        }else if([[dict objectForKey:@"status"] intValue] == -1)
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"webservice parameters error" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }else if([[dict objectForKey:@"status"] intValue] == -2)
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"no_data"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"error"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
}

-(void)loadInAppProductsFromAppStore
{
    InAppProducts = nil;
    [[FCBPurchase sharedInstance:identifiers] requestProductsWithCompletionHandler:^(BOOL success, NSArray *productss) {
        if (success) {
            InAppProducts = productss;
        }
    }];
}

-(void)buyAProductWithIdentifier:(NSString*)identifer
{
    for(SKProduct* prod in InAppProducts)
    {
        if([prod.productIdentifier isEqualToString:identifer])
        {
            [[FCBPurchase sharedInstance:identifiers] buyProduct:prod];
            break;
        }
    }
}

#pragma mark - i got the event result of purchasing
- (void)productPurchased:(NSNotification *)notification {
    if(premuimDuration != -1)
    {
        [self addPremiumForUserwithTransaction:(SKPaymentTransaction *)notification.object];
        
    }else if(pointsCount != -1)
    {
        [self addPointsForUserwithTransaction:(SKPaymentTransaction *)notification.object];
    }
}

- (void)productCanceled:(NSNotification *)notification{

}



-(void)addPremiumForUserwithTransaction:(SKPaymentTransaction *)transaction
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if([RestServiceAgent internetAvailable])
        {
            [[ShopManager getInstance] subscripeInPremium:[[CurrentUser getObject]getUser].userId subscriptionDuration:premuimDuration
             transactionId:transaction.transactionIdentifier
              and:^(id currentClient) {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [SVProgressHUD dismiss];
                      if ([[currentClient objectForKey:@"status"] intValue]==1) {
                          NSString* messagePart = @"";
                          if(premuimDuration == 30)
                          {
                              messagePart = [[Language sharedInstance] stringWithKey:@"PremiumPackages_month"] ;
                          }else if(premuimDuration == 90)
                          {
                              messagePart = [[Language sharedInstance] stringWithKey:@"PremiumPackages_3month"];
                          }else if(premuimDuration == 160)
                          {
                              messagePart = [[Language sharedInstance] stringWithKey:@"PremiumPackages_6month"];
                          }else if(premuimDuration == 365)
                          {
                              messagePart = [[Language sharedInstance] stringWithKey:@"PremiumPackages_year"];
                          }
                          UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:[NSString stringWithFormat:@"%@%@%@",@"You have subscribed successfully, ",messagePart,@" Package"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                          [alert show];
                          [[[CurrentUser getObject] getUser] setIsPremium:YES];
                          [[CurrentUser getObject] saveCurrentUser];
                          premuimDuration = -1;
                      }

                  });
              }onFailure:^(NSError *error) {
                  [SVProgressHUD dismiss];
                  UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                  [alert show];
                  
              }];
            
        }
    }
    @catch (NSException *exception) {}
}

-(void)addPointsForUserwithTransaction:(SKPaymentTransaction *)transaction
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if([RestServiceAgent internetAvailable])
        {
            [[StudioManager getInstance] updateUserCredit:[[[CurrentUser getObject] getUser] userId]
                                              creditValue:(int)pointsCount
                                            transactionId:transaction.transactionIdentifier
          and:^(id currentClient) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [SVProgressHUD dismiss];
                  if ([[currentClient objectForKey:@"status"] intValue]==1) {
                      UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:[NSString stringWithFormat:@"%@%i%@",@"You have purchased, ",pointsCount,@" Points"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                      [alert show];
                      [[[CurrentUser getObject] getUser] setCredit:[CurrentUser getObject].getUser.credit+pointsCount];
                      [[CurrentUser getObject] saveCurrentUser];
                      pointsCount = -1;
                  }
              });
          }onFailure:^(NSError *error) {
              [SVProgressHUD dismiss];
              UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
              [alert show];

          }];
            
        }
    }
    @catch (NSException *exception) {}
}



-(void)adjustSegments{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString* FCBStore= [Language sharedInstance].currentLanguage==Arabic?@"متجر برشلونة":@"FCB Store";
        NSString* PremiumPackages=[[Language sharedInstance] stringWithKey:@"premium_packages"];
        NSString* PointsPackages=[[Language sharedInstance] stringWithKey:@"Shop_package4"];
        NSString* Redemption=[[Language sharedInstance]stringWithKey:@"Redemption"];
        
        [_segments setTitle:FCBStore forSegmentAtIndex:0];
        [_segments setTitle:PremiumPackages forSegmentAtIndex:1];
        [_segments setTitle:PointsPackages forSegmentAtIndex:2];
        [_segments setTitle:Redemption forSegmentAtIndex:3];
        
        if ( UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad )
        {
            NSMutableArray *stringsWidths =[[NSMutableArray alloc] init] ;
            [stringsWidths addObject:@([self widthForString:FCBStore])];
            [stringsWidths addObject:@([self widthForString:PremiumPackages])];
            [stringsWidths addObject:@([self widthForString:PointsPackages])];
            [stringsWidths addObject:@([self widthForString:Redemption])];
            
            float sementsWidth = 0.0;
            for (int i=0; i <_segments.numberOfSegments; i++) {
                [_segments setWidth:[stringsWidths[i] floatValue]+10 forSegmentAtIndex:i];
                sementsWidth += [stringsWidths[i] floatValue]+10;
            }
            
            _segments.frame = CGRectMake(_segments.frame.origin.x,
                                         _segments.frame.origin.y,
                                         sementsWidth+30,
                                         _segments.frame.size.height);
            
            _segmentsViewTap.frame = CGRectMake(_segmentsViewTap.frame.origin.x,
                                                _segmentsViewTap.frame.origin.y,
                                                sementsWidth+30,
                                                _segmentsViewTap.frame.size.height);
            
            [_segmentsView setContentSize:CGSizeMake(sementsWidth+30,50)];
        }

    });
}

-(CGFloat)widthForString:(NSString*)string{
    UIFont *myFont = [UIFont systemFontOfSize:17.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:myFont, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

- (UIColor *) colorWithHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
- (IBAction)freePointsButton_Action:(id)sender{
    FreePointsViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FreePointsViewController"];
    [self.navigationController pushViewController:view animated:YES];
}
@end
