//
//  AboutFCBarcelonaViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/19/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "AboutFCBarcelonaViewController.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
@interface AboutFCBarcelonaViewController ()<UIWebViewDelegate>

@end

@implementation AboutFCBarcelonaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _webView.delegate = self;
    _webView.opaque=NO;
    _webView.backgroundColor=[UIColor clearColor];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    self.pageTitle.text = [[Language sharedInstance] stringWithKey:@"i_AboutFC"];

    [self getInfoUrl];
}
-(void)getInfoUrl{
    
    @try {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if([RestServiceAgent internetAvailable])
        {
            [[UsersManager getInstance] infoLinks:@"about_fcb" and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (currentClient) {
                        if([[currentClient objectForKey:@"status"] intValue] == 2)
                        {
                            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:currentClient[@"data"][@"link"]]]];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
}

@end
