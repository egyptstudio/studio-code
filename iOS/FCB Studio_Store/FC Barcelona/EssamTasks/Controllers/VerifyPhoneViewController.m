//
//  VerificationViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/2/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "VerifyPhoneViewController.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
#import "CurrentUser.h"
@interface VerifyPhoneViewController ()<UITextFieldDelegate>

@end

@implementation VerifyPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
}
-(void)initView{

    CALayer * l = [_validationCodeView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    CALayer * l1 = [_send layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    
    CALayer * l2 = [_reSend layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    
    _validationCode.delegate = self;
    
    
    [_titleLabel setText:[[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_verifyTitle"]];
    [_descLabel setText:[[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_verifyMessage"]];
    _validationCode.placeholder = [[Language sharedInstance] stringWithKey:@"validation_code"];
    
    [_send setTitle:[[Language sharedInstance] stringWithKey:@"MessagesWindow_Send"] forState:UIControlStateNormal];
    [_reSend setTitle:[[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_resendSMS"] forState:UIControlStateNormal];

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (IBAction)send_Action:(id)sender{
    @try {
        if ([[self.validationCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_verifyMessage"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            if([RestServiceAgent internetAvailable])
            {
                [[UsersManager getInstance] sendSMSverificationCode:[[CurrentUser getObject] getUser].userId code:self.validationCode.text and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            [SVProgressHUD dismiss];
                            if([[currentClient objectForKey:@"status"] intValue] == 1)
                            {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"phoneVerified" object:nil];
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else
            {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }

}
- (IBAction)reSend_Action:(id)sender{
    @try {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            if([RestServiceAgent internetAvailable])
            {
                [[UsersManager getInstance] verifyUserPhone:[[CurrentUser getObject] getUser].userId phone:[[CurrentUser getObject] getUser].phone and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            [SVProgressHUD dismiss];
                            if([[currentClient objectForKey:@"status"] intValue] == 1)
                            {
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_verifyMessage"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else
            {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)back_Action:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
@end
