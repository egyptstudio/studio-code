
//
//  Top10ViewController.m
//  FC Barcelona
//
//  Created by Eissa on 9/10/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "Top10ViewController.h"
#import "UserComment.h"
#import "WallManager.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "ShareManagerViewController.h"
#import "CommentsViewController.h"
#import "SectionCell.h"
#import "Season.h"
#import "Post.h"
#import "PostCell.h"
#import <KeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "CurrentUser.h"
#import "GetAndPushCashedData.h"

@interface Top10ViewController ()
@end
@implementation Top10ViewController
{
    CGFloat lastContentOffset;
    id resultsDictionary;
    ShareManagerViewController * shareView;
    CommentsViewController * commentsView;
    NSMutableSet* _collapsedSections;
    UIRefreshControl * refreshControl;
    
    int pageNumber;
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    pageNumber = 0 ;
    _collapsedSections = [NSMutableSet new];
    
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    self.tableView.tableFooterView = footerView;
    [self.tableView setSeparatorStyle:(UITableViewCellSeparatorStyleNone)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshSeasons)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}
-(void)refreshSeasons{
    [_tableView reloadData];
    [refreshControl endRefreshing];
    [self getTopTen];
}
-(void)viewWillAppear:(BOOL)animated
{
    resultsDictionary = [NSMutableArray array];
    [resultsDictionary addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Top10"]];
    if ([resultsDictionary count] > 0) {
        [_tableView reloadData];
    }
    else{
        [self getTopTen];
    }
}

-(void)getTopTen
{
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            pageNumber++;
            [[WallManager getInstance]getTopTen:[[CurrentUser getObject] getUser].userId and:^(id currentClient) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        resultsDictionary = currentClient;
                        [self prepareAndCacheTop10];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    pageNumber--;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)prepareAndCacheTop10{
    for (int i = 0 ; i < [resultsDictionary count]; i++) {
        NSMutableArray * allSeasonPosts = [[NSMutableArray alloc] init];
        for (int j = 0 ; j < [[(Season*)resultsDictionary[i] posts] count]; j++) {
            NSDictionary* dic = [[NSDictionary alloc] initWithDictionary:[(Season*)resultsDictionary[i] posts][j]];
            [allSeasonPosts addObject:[self postObjectFromDic:dic]];
        }
        [(Season*)resultsDictionary[i] setPosts:allSeasonPosts];
    }
    [GetAndPushCashedData cashObject:resultsDictionary withAction:@"Top10"];
    [_tableView reloadData];
}

-(Post*)postObjectFromDic:(NSDictionary*)dic{
    Post* post = [[Post alloc] init];
    post.originalPostId =  [dic[@"originalPostId"] intValue];
    post.studioPhotoId =   [dic[@"studioPhotoId"] intValue];
    post.postId = [dic[@"postId"] intValue];
    post.commentCount =   [dic[@"commentCount"] intValue];
    post.likeCount =  [dic[@"likeCount"] intValue];
    post.shareCount =  [dic[@"shareCount"] intValue];
    post.contestRank =  [dic[@"contestRank"] intValue];
    post.userId =  [dic[@"userId"] intValue];
    post.photoOrientation   = [dic[@"photoOrientation"] intValue];
    post.creationTime       = [dic[@"creationTime"] longLongValue] ;
    post.lastUpdateTime     = [dic[@"lastUpdateTime"] longLongValue];
    post.postPhotoUrl       = dic[@"postPhotoUrl"] ;
    post.postPhotoCaption   = dic[@"postPhotoCaption"];
    post.userName           = dic[@"userName"];
    post.userCountry        = dic[@"userCountry"];
    post.userProfilePicUrl  = dic[@"userProfilePicUrl"];
    post.rePostedBy         = dic[@"rePostedBy"];
    post.rePosterProfilePic = dic[@"rePosterProfilePic"];
    post.reported= [dic[@"reported"] boolValue];
    post.following= [dic[@"following"] boolValue];
    post.liked= [dic[@"liked"] boolValue];
    post.rePosted= [dic[@"rePosted"] boolValue];
    post.rePosterIsFollowing= [dic[@"rePosterIsFollowing"] boolValue];
    NSMutableArray * allTasArray = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < [dic[@"tags"] count]; i++) {
        StudioPhotoTag* tag = [[StudioPhotoTag alloc] init];
        tag.tagName =  dic[@"tags"][i][@"tagName"];
        tag.tagId   = [dic[@"tags"][i][@"tagId"] intValue];
        tag.tagType = [dic[@"tags"][i][@"tagType"]intValue];
        [allTasArray addObject:tag];
    }
    [_tableView reloadData];
    return post;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [resultsDictionary count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    section==0?[_collapsedSections addObject:@(section)]:nil;
    Season* season = (Season*)[resultsDictionary objectAtIndex:section] ;
    return ![_collapsedSections containsObject:@(section)] ? 0 : [[season posts] count];
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SectionCell * cell  = [tableView dequeueReusableCellWithIdentifier:@"SectionCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SectionCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    [cell.sectionButton addTarget:self action:@selector(expandSeason:) forControlEvents:UIControlEventTouchUpInside];
    NSString* headerTitle = [(Season*)[resultsDictionary objectAtIndex:section] seasonName];
    [cell.sectionButton setTitle:[NSString stringWithFormat:@"\t %@",headerTitle] forState:UIControlStateNormal];
    cell.sectionButton.tag = section;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Season* season = (Season*)[resultsDictionary objectAtIndex:indexPath.section] ;
    Post *currentPost = season.posts[indexPath.row];
    PostCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PostCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PostCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        [cell initWithPost:currentPost andController:self];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setTag:indexPath.row];
        [cell setAllPosts:(NSMutableArray*)[season posts]];
        [cell setWalltype:4];
    }
    
    return cell;
}


/// Manar
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [(PostCell*)cell performSelectorInBackground:@selector(initImages) withObject:nil];
}
/////////

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return 38;
}

-(void)expandSeason:(UIButton*)sender {
    int section = sender.tag;
    bool shouldCollapse = [_collapsedSections containsObject:@(section)];
    if ([_collapsedSections containsObject:@(section)]) {
        [_collapsedSections removeObject:@(section)];
    }
    else {
        [_collapsedSections addObject:@(section)];
    }
    [_tableView reloadData];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection;
    if (lastContentOffset > scrollView.contentOffset.y)
        scrollDirection = ScrollDirectionUp;
    else if (lastContentOffset < scrollView.contentOffset.y)
        scrollDirection = ScrollDirectionDown;
    
    lastContentOffset = scrollView.contentOffset.y;
    [_delegate didScrollToDirection:scrollDirection];
}
//Alrassas
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    if (scrollOffset == 0)
    {
        [_delegate didScrollToDirection:ScrollDirectionUp];
    }
    else
    {
        
    }
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
