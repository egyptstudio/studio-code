//
//  VerificationViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/2/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyPhoneViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@property (weak, nonatomic) IBOutlet UITextField *validationCode;
@property (weak, nonatomic) IBOutlet UIView *validationCodeView;

@property (weak, nonatomic) IBOutlet UIButton *send;
@property (weak, nonatomic) IBOutlet UIButton *reSend;

- (IBAction)send_Action:(id)sender;
- (IBAction)reSend_Action:(id)sender;
- (IBAction)back_Action:(id)sender;


@end
