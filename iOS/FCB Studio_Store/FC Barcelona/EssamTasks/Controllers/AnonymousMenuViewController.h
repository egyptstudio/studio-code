//
//  AnonymousMenuViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/1/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnonymousMenuViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIView *fcbView;
@property (weak, nonatomic) IBOutlet UIView *fansView;
@property (weak, nonatomic) IBOutlet UIView *contestView;
@property (weak, nonatomic) IBOutlet UIView *friendsView;
@property (weak, nonatomic) IBOutlet UIView *sponsorsView;
@property (weak, nonatomic) IBOutlet UIView *profileView;

@property (weak, nonatomic) IBOutlet UILabel *fcbLabel;
@property (weak, nonatomic) IBOutlet UILabel *fansLabel;
@property (weak, nonatomic) IBOutlet UILabel *contestLabel;
@property (weak, nonatomic) IBOutlet UILabel *friendsLabel;
@property (weak, nonatomic) IBOutlet UILabel *sponsorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *profileLabel;

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

- (IBAction)openProfile:(id)sender;
- (IBAction)openFans:(id)sender;
- (IBAction)openContest:(id)sender;
- (IBAction)openFriends:(id)sender;
- (IBAction)openSponsers:(id)sender;
- (IBAction)openHome:(id)sender;

- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

@end
