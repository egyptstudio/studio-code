//
//  FullScreenImageViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/7/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
@interface FullScreenImageViewController : UIViewController
@property(weak,nonatomic) IBOutlet UIImageView *fullScreenImage;
@property(assign) UIImageOrientation orienaiton;
@property(strong) UIImage *image;
@end
