//
//  InviteFriendsViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/23/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
- (IBAction)skip_Action:(id)sender;
- (IBAction)done_Action:(id)sender;
- (IBAction)selectAll_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *inviteLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectAllButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
