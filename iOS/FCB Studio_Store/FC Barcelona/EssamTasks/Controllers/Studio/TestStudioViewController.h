//
//  StudioViewController.h
//  FC Barcelona
//
//  Created by Eissa on 9/1/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestStudioViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *tableView;
- (IBAction)back_Action:(id)sender;

@end
