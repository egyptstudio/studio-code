//
//  StudioViewController.m
//  FC Barcelona
//
//  Created by Eissa on 9/1/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "TestStudioViewController.h"

@interface TestStudioViewController ()

@end

@implementation TestStudioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 20;
}
- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    for (UIView *obj in cell.contentView.subviews) {
        [obj removeFromSuperview];
    }
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"myCell";
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    UIImage * cellImage = [UIImage imageNamed:@"bg.png"];
    
    UIView * baseView = [[UIView alloc] initWithFrame:cell.contentView.frame];
    UIView * frameView = [[UIView alloc] initWithFrame:cell.contentView.frame];

    CALayer *baseLayer = [[CALayer alloc] init];
    baseLayer = indexPath.row%2==0 ? [self getLeftLayerWithImage:cellImage forView:cell.contentView] : [self getRightLayerWithImage:cellImage forView:cell.contentView];
    
    CALayer *frameLayer = [[CALayer alloc] init];
    frameLayer = indexPath.row%2==0 ? [self getLeftLayerFrameForView:cell.contentView] : [self getRightLayerFrameForView:cell.contentView];

    
    [[frameView layer]addSublayer:frameLayer];
    [baseView addSubview:frameView];
    [[baseView layer]addSublayer:baseLayer];
    [cell.contentView addSubview:baseView];
    return cell;
    
}

-(CALayer*)getLeftLayerWithImage:(UIImage*)image forView:(UIView*)cellView
{
        UIBezierPath *aPath = [UIBezierPath bezierPath];
    
        [aPath moveToPoint:CGPointMake(cellView.frame.origin.x+2, cellView.frame.origin.y+5)];
        [aPath addLineToPoint:CGPointMake(cellView.frame.size.width-2, 2)];
        [aPath addLineToPoint:CGPointMake(cellView.frame.size.width-2, cellView.frame.size.height-2)];
        [aPath addLineToPoint:CGPointMake(2, cellView.frame.size.height-5)];
        [aPath addLineToPoint:CGPointMake(cellView.frame.origin.x+2, cellView.frame.origin.y+5)];
        [aPath closePath];
    
    
        CAShapeLayer *shapeView = [CAShapeLayer layer];
        shapeView.path = [aPath CGPath];
    
    
        CALayer* contentLayer = [CALayer layer];
        contentLayer.frame = cellView.frame;
        [contentLayer setContents:(id)[image CGImage]];
        [contentLayer setMask:shapeView];
         return contentLayer;
}

-(CALayer*)getRightLayerWithImage:(UIImage*)image forView:(UIView*)cellView
{
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    
    [aPath moveToPoint:CGPointMake(cellView.frame.origin.x+2, cellView.frame.origin.y+2)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.size.width-2, 5)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.size.width-2, cellView.frame.size.height-5)];
    [aPath addLineToPoint:CGPointMake(2, cellView.frame.size.height-2)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.origin.x+2, cellView.frame.origin.y+2)];
    [aPath closePath];
    
    
    CAShapeLayer *shapeView = [CAShapeLayer layer];
    shapeView.path = [aPath CGPath];
    
    
    CALayer* contentLayer = [CALayer layer];
    contentLayer.frame = cellView.frame;
    [contentLayer setContents:(id)[image CGImage]];
    [contentLayer setMask:shapeView];
    return contentLayer;
}

-(CALayer*)getLeftLayerFrameForView:(UIView*)cellView
{
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    
    [aPath moveToPoint:CGPointMake(cellView.frame.origin.x, cellView.frame.origin.y+3)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.size.width, 0)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.size.width, cellView.frame.size.height)];
    [aPath addLineToPoint:CGPointMake(0, cellView.frame.size.height-3)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.origin.x, cellView.frame.origin.y+3)];
    [aPath closePath];
    
    
    CAShapeLayer *shapeView = [CAShapeLayer layer];
    shapeView.path = [aPath CGPath];
    
    
    CALayer* contentLayer = [CALayer layer];
    contentLayer.frame = cellView.frame;
    [contentLayer setMask:shapeView];
    [contentLayer setBackgroundColor:[[UIColor orangeColor] CGColor]];
    return contentLayer;
}

-(CALayer*)getRightLayerFrameForView:(UIView*)cellView
{
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    
    [aPath moveToPoint:CGPointMake(cellView.frame.origin.x, cellView.frame.origin.y)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.size.width, 3)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.size.width, cellView.frame.size.height-3)];
    [aPath addLineToPoint:CGPointMake(0, cellView.frame.size.height)];
    [aPath addLineToPoint:CGPointMake(cellView.frame.origin.x, cellView.frame.origin.y)];
    [aPath closePath];
    
    
    CAShapeLayer *shapeView = [CAShapeLayer layer];
    shapeView.path = [aPath CGPath];
    
    
    CALayer* contentLayer = [CALayer layer];
    contentLayer.frame = cellView.frame;
    [contentLayer setMask:shapeView];
    [contentLayer setBackgroundColor:[[UIColor orangeColor] CGColor]];
    return contentLayer;
}
@end
