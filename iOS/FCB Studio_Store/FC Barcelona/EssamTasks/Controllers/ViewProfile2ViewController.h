//
//  ViewProfile2ViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/13/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewProfile2ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *aboutMe;
@property (weak, nonatomic) IBOutlet UILabel *relationShip;
@property (weak, nonatomic) IBOutlet UILabel *religion;
@property (weak, nonatomic) IBOutlet UILabel *education;
@property (weak, nonatomic) IBOutlet UILabel *jop;
@property (weak, nonatomic) IBOutlet UILabel *jopRole;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UILabel *income;

- (IBAction)back_Action:(id)sender;

@end
