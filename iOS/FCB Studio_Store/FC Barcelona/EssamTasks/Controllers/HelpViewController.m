//
//  HelpViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 3/5/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "HelpViewController.h"
#import "TutorialViewController.h"

@interface HelpViewController ()
@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self displayAppGuide];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    self.pageTitle.text = [[Language sharedInstance] stringWithKey:@"i_Help"];
}



- (void) displayAppGuide {
    
    TutorialViewController * tutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
    tutorialViewController.helpFlag = YES;
    tutorialViewController.view.frame = _helpView.frame;
    tutorialViewController.scrollView.frame = CGRectMake(0, 0,
                                                         tutorialViewController.view.frame.size.width,
                                                         tutorialViewController.view.frame.size.height);
    tutorialViewController.pageNumber.frame = CGRectMake((tutorialViewController.view.frame.size.width - tutorialViewController.pageNumber.frame.size.width)/ 2 ,
                                                         tutorialViewController.view.frame.size.height - tutorialViewController.pageNumber.frame.size.height+5,
                                                         tutorialViewController.pageNumber.frame.size.width,
                                                         tutorialViewController.pageNumber.frame.size.height);
    
    [self addChildViewController:tutorialViewController];
    [tutorialViewController.skipButton setHidden: YES];
    [self.view insertSubview:tutorialViewController.view atIndex:[self.view.subviews count]];
    [tutorialViewController didMoveToParentViewController:self];
}


- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}


- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}


- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
}


-(BOOL)shouldAutorotate
{
    return NO;
}


-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
