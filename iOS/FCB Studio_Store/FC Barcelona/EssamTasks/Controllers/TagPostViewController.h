//
//  TagPostViewController.h
//  FC Barcelona
//
//  Created by Saad Ansari on 2/9/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagPostViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) NSString *tagID;
@property (strong, nonatomic) NSString *tagName;

@end
