//
//  MainWallViewController.h
//  FC Barcelona
//
//  Created by Eissa on 3/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

#define sandBoxReceiptUrl @"https://sandbox.itunes.apple.com/verifyReceipt"
#define onlineBoxReceiptUrl @"https://buy.itunes.apple.com/verifyReceipt"

typedef enum {
    wall = 1,
    latest,
    myPics,
} CurrentContent;

@interface MainWallViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UILabel *needLoginLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *segmentsView;
@property (weak, nonatomic) IBOutlet UIView *segmentsViewTap;

@property (weak, nonatomic) IBOutlet UIView *mainContainer;
@property (weak, nonatomic) IBOutlet UIView *needLoginView;
@property (weak, nonatomic) IBOutlet UIButton *goLoginButton;
@property (weak, nonatomic) IBOutlet UIView *tipView;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
- (IBAction)filterButton_Action:(id)sender;


@property (weak, nonatomic) IBOutlet UIImageView *tabImage;
- (IBAction)latest_Action:(id)sender;
- (IBAction)top10_Action:(id)sender;
- (IBAction)wall_Action:(id)sender;
- (IBAction)myPics_Action:(id)sender;
- (IBAction)filter_Action:(id)sender;

- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *latest;
@property (weak, nonatomic) IBOutlet UIButton *top10;
@property (weak, nonatomic) IBOutlet UIButton *wall;
@property (weak, nonatomic) IBOutlet UIButton *myPics;
@property (weak, nonatomic) IBOutlet UIView *studioView;
- (IBAction)segmentChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *buttonBar;

- (IBAction)openMenu:(id)sender;
- (IBAction)openStudio:(id)sender;
@end
