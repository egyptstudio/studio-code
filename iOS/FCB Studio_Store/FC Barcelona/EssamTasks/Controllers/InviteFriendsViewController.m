//
//  InviteFriendsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/23/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "InviteFriendsViewController.h"
#import <AddressBook/AddressBook.h>
#import "ConfirmInviteFriendsViewController.h"
@interface InviteFriendsViewController ()

@end

@implementation InviteFriendsViewController
{
    NSArray *allContacts;
    NSMutableArray *contactsAlphabets;
    NSMutableDictionary *allContactsObject;
    NSMutableArray * selectedContacts;
    BOOL allSelected;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    selectedContacts  = [[NSMutableArray alloc] init];
    allContacts = [[NSMutableArray alloc] init];
    allContactsObject = [[NSMutableDictionary alloc] init];
    contactsAlphabets = [[NSMutableArray alloc] init];
    CALayer * l1 = [self.selectAllButton layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{

    [self authorizeAddressBook];
    
}
-(void)loadContacts{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    for (int i = 0 ; i < [allContacts count] ; i++ ){
        NSString *firstLetter = [[(__bridge NSString*)ABRecordCopyCompositeName((__bridge ABRecordRef)(allContacts[i])) substringToIndex:1] uppercaseString];
       NSLog(@"%@",ABRecordCopyCompositeName((__bridge ABRecordRef)(allContacts[i])));

        if(![contactsAlphabets containsObject:firstLetter])
        {
            [contactsAlphabets addObject:firstLetter];
            [allContactsObject setObject:allContacts[i] forKey:firstLetter];
            for (int j = 0 ; j < [allContacts count] ; j++ ){
                if ([[[(__bridge NSString*)ABRecordCopyCompositeName((__bridge ABRecordRef)(allContacts[j])) substringToIndex:1] uppercaseString] isEqualToString:firstLetter])
                {
                    NSMutableArray* arr;
                    if ([[allContactsObject objectForKey:firstLetter] isKindOfClass:[NSArray class]])
                    {
                        arr = [[NSMutableArray alloc] initWithArray:[allContactsObject objectForKey:firstLetter]];
                    }
                    else
                    {
                        arr = [[NSMutableArray alloc] init];
                    }
                    
                    [arr addObject:allContacts[j]];
                    [allContactsObject setObject:arr forKey:firstLetter];
                }
            }
        }
    }
    contactsAlphabets = [contactsAlphabets sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [_tableView reloadData];
    
}
-(void)authorizeAddressBook{
 
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted)
    {
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted){return;}
            else{[self loadContacts];}
        });
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        [self loadContacts];
    } else{
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (granted){[self loadContacts];}
        });
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [contactsAlphabets count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [contactsAlphabets objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return[NSArray arrayWithObjects:@"A", @"B", @"C", @"D", @"E",
           @"F", @"G", @"H", @"I", @"J",
           @"K", @"L", @"M", @"N", @"O",
           @"P", @"Q", @"R", @"S", @"T",
           @"U", @"V", @"W", @"X", @"Y",@"Z",@"#",nil];
//    return contactsAlphabets;
    
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [contactsAlphabets indexOfObject:title];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"myHeader"];
    UILabel *headerTitle = (UILabel*)[cell viewWithTag:1];
    headerTitle.text = [contactsAlphabets objectAtIndex:section];
    return cell.contentView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionTitle = [contactsAlphabets objectAtIndex:section];
    NSArray *sectionList = [allContactsObject objectForKey:sectionTitle];
    return [sectionList count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *sectionTitle = [contactsAlphabets objectAtIndex:indexPath.section];
    NSArray *sectionList = [allContactsObject objectForKey:sectionTitle];
    ABRecordRef *contact = (__bridge ABRecordRef)[sectionList objectAtIndex:indexPath.row];

    
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"myCell"];
    UILabel *name = (UILabel*)[cell viewWithTag:3];
    name.text = (__bridge NSString *)(ABRecordCopyCompositeName(contact)) ;
    
    UILabel *number = (UILabel*)[cell viewWithTag:4];
    ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(contact, kABPersonPhoneProperty);
    NSArray* phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
    CFRelease(phoneNumberProperty);
    number.text = [phoneNumbers count] > 0 ? [NSString stringWithFormat:@"%@",phoneNumbers[0]] : @" ";
    
    
    UIImageView *user = (UIImageView*)[cell viewWithTag:2];
    UIImage *userImage = [UIImage imageWithData:(__bridge NSData *)(ABPersonCopyImageData(contact))] ? [UIImage imageWithData:(__bridge NSData *)(ABPersonCopyImageData(contact))] : [UIImage imageNamed:@"avatar.png"];
    
    user.image = userImage;
    CALayer * l = [user layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:user.frame.size.width/2];
    [l setBorderColor:[[UIColor whiteColor] CGColor]];
    [l setBorderWidth:1];
    
    
    UIButton *check = (UIButton*)[cell viewWithTag:1];
    [check addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
    [check setEnabled: [phoneNumbers count] > 0 ? YES : NO];
    if (allSelected) {
        if ([phoneNumbers count]>0) {
            [check setBackgroundImage:[UIImage imageNamed:@"cheackbtn_sc.png"] forState:UIControlStateNormal];
            NSString *theNumber = [NSString stringWithFormat:@"%@",phoneNumbers[0] ] ;
        }
        else{
            [check setBackgroundImage:[UIImage imageNamed:@"cheackbtn.png"] forState:UIControlStateNormal];
        }
    }
    else{
        NSString *theNumber     = [NSString stringWithFormat:@"%@",phoneNumbers[0] ] ;
        if ([selectedContacts containsObject:theNumber]) {
            [check setBackgroundImage:[UIImage imageNamed:@"cheackbtn_sc.png"] forState:UIControlStateNormal];
        }else{
            [check setBackgroundImage:[UIImage imageNamed:@"cheackbtn.png"] forState:UIControlStateNormal];
        }
        
    }
  
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addFriend:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    
    NSString *sectionTitle = [contactsAlphabets objectAtIndex:indexPath.section];
    NSArray *sectionList = [allContactsObject objectForKey:sectionTitle];
    ABRecordRef *contact = (__bridge ABRecordRef)[sectionList objectAtIndex:indexPath.row];
    
    ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(contact, kABPersonPhoneProperty);
    NSArray* phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
    CFRelease(phoneNumberProperty);

    
    if ([phoneNumbers count]>0) {
        allSelected = NO;
        NSString *number     = [NSString stringWithFormat:@"%@",phoneNumbers[0] ] ;
        NSData *checked      = UIImagePNGRepresentation([UIImage imageNamed:@"cheackbtn_sc.png"]);
        NSData *unchecked    = UIImagePNGRepresentation([UIImage imageNamed:@"cheackbtn.png"]);
        if ([selectedContacts containsObject:number])
        {
            [selectedContacts removeObject:number];
            [sender setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
            
        }
        else
        {
            [selectedContacts addObject:number];
            [sender setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
        }
    }
    else{
        [[[UIAlertView alloc] initWithTitle:@"No Number" message:@"This contact does not have any phone numbers" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil] show];
    }
   
    
}

- (IBAction)skip_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)done_Action:(id)sender {

    if ([selectedContacts count]>0) {
          ConfirmInviteFriendsViewController *confirm = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmInviteFriendsViewController"];
        confirm.phoneNumbers = selectedContacts;
        [self.navigationController pushViewController:confirm animated:YES];
    }
    else{
     [[[UIAlertView alloc] initWithTitle:@"No Number" message:@"To continue, select at least one contact" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil] show];
    }
  
    
}

- (IBAction)selectAll_Action:(id)sender {
    allSelected = YES;
    [selectedContacts removeAllObjects];
    
    for (id contact in allContacts) {
        ABMultiValueRef phoneNumberProperty = ABRecordCopyValue((__bridge ABRecordRef)contact, kABPersonPhoneProperty);
        NSArray* phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
        CFRelease(phoneNumberProperty);
        if ([phoneNumbers count]>0) {
            NSString *number     = [NSString stringWithFormat:@"%@",phoneNumbers[0] ] ;
            [selectedContacts addObject:number];
        }
    }
    [_tableView reloadData];
}


-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
@end
