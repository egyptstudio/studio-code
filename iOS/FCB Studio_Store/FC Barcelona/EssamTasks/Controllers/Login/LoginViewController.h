//
//  Login2ViewController.h
//  FC Barcelona
//
//  Created by Eissa on 3/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
@protocol LoginDelegate <NSObject>
@required
- (void)didLoginSuccessfully:(CLPlacemark*)place;
@end
//3lshan el commit
@interface LoginViewController  : UIViewController<UITextFieldDelegate,UITextFieldDelegate>

@property (nonatomic,assign) id <LoginDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *loginWithLabel;

@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *passWord;
@property (weak, nonatomic) IBOutlet UIButton *login;
@property (weak, nonatomic) IBOutlet UIView *userNameView;
@property (weak, nonatomic) IBOutlet UIView *passWordView;
@property (weak, nonatomic) IBOutlet UILabel *forgotPassword;
@property (weak, nonatomic) IBOutlet FBLoginView *facebookLogin;
@property (weak, nonatomic) IBOutlet UILabel *registerUser;
@property (strong, nonatomic) NSString *objectID;

- (IBAction)login_Action:(id)sender;
- (IBAction)back_Action:(id)sender;

- (IBAction)googleLogin_Action:(id)sender;
- (IBAction)twitterLogin_Action:(id)sender;
- (IBAction)facebookLogin_Action:(id)sender;
- (IBAction)instagramLogin_Action:(id)sender;


@end
