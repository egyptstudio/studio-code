//
//  LoginViewController.m
//  FC Barcelona
//
//  Created by Eissa on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterV2ViewController.h"

#import "ForgotPasswordViewController.h"
#import "UsersManager.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "WallViewController.h"
#import "RestServiceAgent.h"
#import "CurrentUser.h"
#import "GetAndPushCashedData.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <QuartzCore/QuartzCore.h>
#import "InstagramLogin.h"
#import "TwitterLogin.h"
#import <CoreLocation/CoreLocation.h>



//3lshan el commit
//dfsdfsdf
#define degreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )

@interface LoginViewController ()<GPPSignInDelegate,FBLoginViewDelegate,InstagramLoginDelegate,TwitterLoginDelegate,RegisterDelegate>
@end

@implementation LoginViewController
{
    UIView * rotary;
    NSUserDefaults* defaults;
    GPPSignIn *signIn;
    GPPSignInButton *gLogin;
    InstagramLogin *iLogin;
    TwitterLogin *tLogin;
    RegisterV2ViewController * regView;
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    [self initView];
    
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    signIn.clientID = @"65048645003-gjrm6hopnvbtr1pfjf9d7sl4qh2m8li2.apps.googleusercontent.com";
    signIn.scopes = @[ kGTLAuthScopePlusLogin];
    signIn.scopes = @[ @"profile" ];
    signIn.delegate = self;
    gLogin = [[GPPSignInButton alloc] initWithFrame:CGRectZero];
    
    iLogin = [InstagramLogin getInstance];
    iLogin.delegate =  self;
    
    tLogin = [TwitterLogin getInstance];
    tLogin.delegate =  self;

    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }

}

-(void)viewWillAppear:(BOOL)animated{
    [self setLocalisbles];
}

-(void)setLocalisbles{
    self.titleLabel.text = [[Language sharedInstance]stringWithKey:@"HomeInitialState_WallLogin"];
    self.userName.placeholder = [[Language sharedInstance]stringWithKey:@"chat_user_name"];
    self.passWord.placeholder = [[Language sharedInstance]stringWithKey:@"Login_passwordDefaultMessage"];
    [self.login setTitle:[[Language sharedInstance]stringWithKey:@"HomeInitialState_WallLogin"] forState:UIControlStateNormal];
    self.forgotPassword.text = [[Language sharedInstance]stringWithKey:@"Login_forgotPassword"];
    self.registerUser.text = [[Language sharedInstance]stringWithKey:@"Login_register"];
    self.loginWithLabel.text = [[Language sharedInstance]stringWithKey:@"Login_social"];
}

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth error: (NSError *) error {
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];

    if (error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Username and password do not match" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }
    else
    {
        GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
        plusService.retryEnabled = YES;
        [plusService setAuthorizer:auth];
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
                        GTMLoggerError(@"Error: %@", error);
                    } else {
                        if ([RestServiceAgent internetAvailable]&& ![[CurrentUser getObject] isUser]) {
                            @try {
                                __block User* userObject = [[User alloc] init];
                                [[UsersManager getInstance] loginSocialMedia:person.identifier
                                                                       email:auth.userEmail
                                                                    fullName:person.displayName
                                                                       phone:@""
                                                                     country:@""
                                                                        city:@""
                                                                    district:@""
                                                                    birthday:@""
                                                                      gender:[person.gender isEqualToString:@"male"] ? @"1":@"0"
                                                               profilePicURL:person.image.url
                                                                         and:^(id currentClient) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         
                                         [SVProgressHUD dismiss];
                                         if ([currentClient isKindOfClass:[User class]]){
                                             userObject = (User*)currentClient;
                                             if(userObject)
                                             {
                                                 
                                                 [[CurrentUser getObject] setUser:userObject];
                                                 [[CurrentUser getObject] saveCurrentUser];
                                                 [_delegate didLoginSuccessfully:nil];
                                                 [self.navigationController popViewControllerAnimated:YES];
                                                 
                                             }
                                         }
                                         else if([currentClient[@"status"] integerValue] == 1) {
                                             NSMutableDictionary *obj = [[NSMutableDictionary alloc] init];
                                             [obj setObject:person.identifier forKey:@"userId"];
                                             [obj setObject:person.displayName forKey:@"fullName"];
                                             [obj setObject:auth.userEmail forKey:@"email"];
                                             [obj setObject:[person.gender isEqualToString:@"male"] ? @"1":@"0" forKey:@"gender"];
                                             [obj setObject:person.image.url forKey:@"picUrl"];

                                             regView = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterV2ViewController"];
                                             regView.socialProfile = obj;
                                             regView.delegate = self;
                                             [self.navigationController pushViewController:regView animated:YES];
                                             
                                         }
                                         
                                         
                                     });
                                 } onFailure:^(NSError *error)
                                 {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [SVProgressHUD dismiss];
                                         
                                         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                         [alert show];
                                     });
                                 }];                            }
                            @catch (NSException *exception)
                            {
                                NSLog(@"nothing");
                            }
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [SVProgressHUD dismiss];
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            });
                        }
                        
                    }
                }];

    }
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    [[self navigationController] pushViewController:viewController animated:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
//    self.userName.text = @"";
//    self.passWord.text = @"";
}

-(void)initView
{
    _facebookLogin.hidden = YES;
    [_facebookLogin setReadPermissions:@[@"public_profile",@"email",@"publish_actions"]];
    [_facebookLogin setDelegate:self];
    _objectID = nil;
    
    self.userName.delegate = self;
    self.passWord.delegate = self;
    
    CALayer * l = [self.login layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    CALayer * l1 = [self.userNameView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    
    CALayer * l2 = [self.passWordView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgetPressed)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    
    [self.forgotPassword addGestureRecognizer:tap];
    self.forgotPassword.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(registerPressed)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    
    [self.registerUser addGestureRecognizer:tap1];
    self.registerUser.userInteractionEnabled = YES;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)forgetPressed
{
    
    ForgotPasswordViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    //WallViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"WallViewController"];
    [self.navigationController pushViewController:view animated:YES];
    
}

-(void)registerPressed
{
    RegisterV2ViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterV2ViewController"];
    view.delegate = self;
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)login_Action:(id)sender
{
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    if ([[self.userName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
    {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Login_emailMissingAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else if (![self isValidEmail:self.userName.text])
    {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Registration_IncorrectEmailFormatAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else if ([[self.passWord.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
    {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Login_PasswordMissingMlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }else if ([self.passWord.text length]<=4) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Registration_passwordLengthAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        if ([RestServiceAgent internetAvailable]) {
            @try {
                __block User* userObject = [[User alloc] init];
                [[UsersManager getInstance] loginWithUserName:self.userName.text andPassword:self.passWord.text and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        userObject = (User*)currentClient;
                        if(userObject)
                        {
                            
                            [[CurrentUser getObject] setUser:userObject];
                            [[CurrentUser getObject] saveCurrentUser];
                            [_delegate didLoginSuccessfully:nil];
                            [self.navigationController popViewControllerAnimated:YES];
                            
                        }
                        [SVProgressHUD dismiss];
                        
                    });
                } onFailure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                         
                         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                         [alert show];
                     });
                 }];
            }
            @catch (NSException *exception)
            {
                NSLog(@"nothing");
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
}


- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)facebookLogin_Action:(id)sender {
    if ([RestServiceAgent internetAvailable]) {
        for (id obj in self.facebookLogin.subviews)
        {
            if ([obj isKindOfClass:[UIButton class]])
            {
                UIButton * loginButton =  obj;
                [loginButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
        }
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }
  
}

- (IBAction)twitterLogin_Action:(id)sender {
    if ([RestServiceAgent internetAvailable]) {
        [tLogin openLogger];
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }
    
}
- (IBAction)googleLogin_Action:(id)sender {
    
    if ([RestServiceAgent internetAvailable]) {
        [gLogin sendActionsForControlEvents:UIControlEventTouchUpInside];
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }
    

}

- (IBAction)instagramLogin_Action:(id)sender {
    if ([RestServiceAgent internetAvailable]) {
        [iLogin openLogger];
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }
    
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)fuser{
    
    if ([RestServiceAgent internetAvailable] && ![[CurrentUser getObject] isUser]) {
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    __block NSString *profilePic ;
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"false", @"redirect",
                            @"300", @"height",
                            @"large", @"type",
                            @"300", @"width",
                            nil
                            ];
    [FBRequestConnection startWithGraphPath:@"/me/picture"
                                 parameters:params
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              profilePic = result[@"data"][@"url"];
                                  @try {
                                      __block User* userObject = [[User alloc] init];
                                      [[UsersManager getInstance] loginSocialMedia:fuser[@"id"]
                                                                             email:@""
                                                                          fullName:fuser[@"name"]
                                                                             phone:@""
                                                                           country:@""
                                                                              city:@""
                                                                          district:@""
                                                                          birthday:@""
                                                                            gender:[fuser[@"gender"] isEqualToString:@"male"] ? @"1":@"0"
                                                                     profilePicURL:profilePic
                                      and:^(id currentClient) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [SVProgressHUD dismiss];
                                              if ([currentClient isKindOfClass:[User class]]){
                                                  userObject = (User*)currentClient;
                                                  if(userObject)
                                                  {
                                                      
                                                      [[CurrentUser getObject] setUser:userObject];
                                                      [[CurrentUser getObject] saveCurrentUser];
                                                      [_delegate didLoginSuccessfully:nil];
                                                      [self.navigationController popViewControllerAnimated:YES];
                                                      
                                                  }
                                                  
                                              }

                                              else if([currentClient[@"status"] integerValue] == 1) {
                                                  NSMutableDictionary *obj = [[NSMutableDictionary alloc] init];
                                                  [obj setObject:fuser[@"id"] forKey:@"userId"];
                                                  [obj setObject:fuser[@"name"] forKey:@"fullName"];
                                                  [obj setObject:fuser[@"email"] forKey:@"email"];
                                                  [obj setObject:[fuser[@"gender"] isEqualToString:@"male"] ? @"1":@"0" forKey:@"gender"];
                                                  [obj setObject:profilePic forKey:@"picUrl"];
                                                  
                                                 regView = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterV2ViewController"];
                                                  regView.socialProfile = obj;
                                                  regView.delegate = self;

                                                  [self.navigationController pushViewController:regView animated:YES];
                                                  
                                              }
                                               [GetAndPushCashedData cashObject:@"YES" withAction:@"LoggedWithSocialMedia"];
                                          });
                                      } onFailure:^(NSError *error)
                                       {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [SVProgressHUD dismiss];
                                               
                                               UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                               [alert show];
                                           });
                                       }];
                                  }
                                  @catch (NSException *exception)
                                  {
                                      NSLog(@"nothing");
                                  }
                          }];
                          }
     else{
         dispatch_async(dispatch_get_main_queue(), ^{
             
             [SVProgressHUD dismiss];
             UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
             [alert show];
         });
     }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) isValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)instagramFinishedWithAuth:(NSDictionary *)user error: (NSError *)error{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];

    if (user) {
    
    }
}
- (void)twitterFinishedWithAuth:(NSDictionary *)user error: (NSError *)error {
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    if (user) {
        if ([RestServiceAgent internetAvailable] && ![[CurrentUser getObject] isUser] ) {
            @try {
                __block User* userObject = [[User alloc] init];
                [[UsersManager getInstance] loginSocialMedia:user[@"id"]
                                                       email:@""
                                                    fullName:user[@"name"]
                                                       phone:@""
                                                     country:@""
                                                        city:@""
                                                    district:@""
                                                    birthday:@""
                                                      gender:@""
                                               profilePicURL:user[@"profile_image_url"]
                                                         and:^(id currentClient) {
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 [SVProgressHUD dismiss];
                                                                 
                                                                 if ([currentClient isKindOfClass:[User class]]){
                                                                     userObject = (User*)currentClient;
                                                                     if(userObject)
                                                                     {
                                                                         
                                                                         [[CurrentUser getObject] setUser:userObject];
                                                                         [[CurrentUser getObject] saveCurrentUser];
                                                                         [_delegate didLoginSuccessfully:nil];
                                                                         [self.navigationController popViewControllerAnimated:YES];
                                                                         
                                                                     }
                                                                 }
                                                                 else if([currentClient[@"status"] integerValue] == 1) {
                                                                     NSMutableDictionary *obj = [[NSMutableDictionary alloc] init];
                                                                     [obj setObject:user[@"id"] forKey:@"userId"];
                                                                     [obj setObject:user[@"name"] forKey:@"fullName"];
                                                                     [obj setObject:@"" forKey:@"email"];
                                                                     [obj setObject:@"" forKey:@"gender"];
                                                                     [obj setObject:user[@"profile_image_url"] forKey:@"picUrl"];
                                                                     
                                                                     regView = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterV2ViewController"];
                                                                     regView.socialProfile = obj;
                                                                     regView.delegate = self;
                                                                     [self.navigationController pushViewController:regView animated:YES];
                                                                     
                                                                 }
                                                                 
                                                                 [GetAndPushCashedData cashObject:@"YES" withAction:@"LoggedWithSocialMedia"];
                                                                 
                                                             });
                                                         } onFailure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                         
                         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                         [alert show];
                     });
                 }];
            }
            @catch (NSException *exception)
            {
                NSLog(@"nothing");
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }else{
        [SVProgressHUD dismiss];
    }
    
}

- (void)didLoginSuccessfully{
    _userName.text = @"";
    _passWord.text = @"";
    [self.view endEditing:YES];
    [_delegate didLoginSuccessfully:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didRegSuccessfully:(CLPlacemark*)place{
///////////////////////////
    [_delegate didLoginSuccessfully:place];
    [self.navigationController popViewControllerAnimated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
@end