//
//  ForgotPasswordViewController.m
//  FC Barcelona
//
//  Created by Eissa on 8/28/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
#import "ConfirmForgotPasswordViewController.h"
@interface ForgotPasswordViewController ()
@property (weak, nonatomic) IBOutlet UIView *emailView;

@end

@implementation ForgotPasswordViewController
{
    NSString* currentEmail;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    // Do any additional setup after loading the view.
    [self setLocalisabels];
}
-(void)setLocalisabels{
    _titleLabel.text = [[Language sharedInstance]stringWithKey:@"Login_forgotPassword"];
    _descLabel.text = [[Language sharedInstance]stringWithKey:@"ForgotPassword_preEmailMessage"];
    _noteLabel.text = [[Language sharedInstance]stringWithKey:@"ForgotPassword_postEmailMessage"];
    _email.placeholder = [[Language sharedInstance]stringWithKey:@"Registration_EmailDefaultValue"];
    [_send setTitle:[[Language sharedInstance]stringWithKey:@"MessagesWindow_Send"] forState:UIControlStateNormal];
}

-(void)initView
{
    
    self.email.delegate = self;
    CALayer * l = [self.send layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    CALayer * l1 = [self.emailView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)send_Action:(id)sender
{
    @try {
        if ([[self.email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Login_emailMissingAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if (![self isValidEmail:self.email.text])
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Registration_IncorrectEmailFormatAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            if([RestServiceAgent internetAvailable])
            {
                [[UsersManager getInstance] forgotPasswordWithEmail:self.email.text and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            [SVProgressHUD dismiss];
                            if([[currentClient objectForKey:@"status"] intValue] == 2)
                            {
                             
                                ConfirmForgotPasswordViewController* confirm = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmForgotPasswordViewController"];
                                confirm.userId = currentClient[@"data"][@"userId"];
                                confirm.userEmail = self.email.text;
                                [self.navigationController pushViewController:confirm animated:YES];
                            }
                            else{
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance]stringWithKey:@"ForgotPassword_preEmailMessage"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else
            {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            
            
        }

    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
    
}
-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
@end
