//
//  ConfirmForgotPasswordViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/15/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmForgotPasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *resendLabel;

@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UITextField *code;

@property (weak, nonatomic) IBOutlet UIView *passView;
@property (weak, nonatomic) IBOutlet UITextField *pass;

@property (weak, nonatomic) IBOutlet UIView *confirmView;
@property (weak, nonatomic) IBOutlet UITextField *confirm;
@property(strong,nonatomic) NSString* userId;
@property(strong,nonatomic) NSString* userEmail;

@property (weak, nonatomic) IBOutlet UIView *resend;

@property (weak, nonatomic) IBOutlet UIButton *send;

- (IBAction)send_Action:(id)sender;
- (IBAction)back_Action:(id)sender;

@end
