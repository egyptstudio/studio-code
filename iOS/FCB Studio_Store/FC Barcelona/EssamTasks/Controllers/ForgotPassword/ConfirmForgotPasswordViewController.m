//
//  ConfirmForgotPasswordViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/15/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "ConfirmForgotPasswordViewController.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
#import "LoginViewController.h"
@interface ConfirmForgotPasswordViewController ()

@end

@implementation ConfirmForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    [self setLocalisabels];
}

-(void)setLocalisabels{
    _titleLabel.text = [[Language sharedInstance]stringWithKey:@"Login_forgotPassword"];
    _code.placeholder = [[Language sharedInstance]stringWithKey:@"HomeUnconfirmedEmailAndPhone_codeFieldDefault"];
    _pass.placeholder = [[Language sharedInstance]stringWithKey:@"ForgotPassword_NewPassword"];
    _confirm.placeholder = [[Language sharedInstance]stringWithKey:@"Registration_ConfirmPasswordDefaultValue"];
    _resendLabel.text = [[Language sharedInstance]stringWithKey:@"Resend_Email"];
    [_send setTitle:[[Language sharedInstance]stringWithKey:@"MessagesWindow_Send"] forState:UIControlStateNormal];
}

-(void)initView
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resendCode)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    
    [self.resend addGestureRecognizer:tap];
    self.resend.alpha=.3;
    self.resend.userInteractionEnabled = NO;
    
    [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(activateResendView) userInfo:nil repeats:NO];
    
    self.code.delegate = self;
    self.pass.delegate = self;
    self.confirm.delegate = self;

    
    
    CALayer * l = [self.send layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    CALayer * l1 = [self.codeView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    
    CALayer * l2 = [self.passView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    
    CALayer * l3 = [self.confirmView layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:5];
    

    
}
-(void)activateResendView{
    self.resend.alpha=1;
    self.resend.userInteractionEnabled = YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)resendCode{

    @try {
        {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            if([RestServiceAgent internetAvailable])
            {
                [[UsersManager getInstance] forgotPasswordWithEmail:_userEmail and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            [SVProgressHUD dismiss];
                            if([[currentClient objectForKey:@"status"] intValue] == 2)
                            {
                                _userId = currentClient[@"data"][@"userId"];
                                self.resend.alpha=.3;
                                self.resend.userInteractionEnabled = NO;
                                
                                [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(activateResendView) userInfo:nil repeats:NO];
                                
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Verification code sent to your email." delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];

                            }
                            else{
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"This email does not exist in our database, Please make sure you entered the registration email." delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else
            {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)send_Action:(id)sender
{
    @try {
        if ([[self.code.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"ForgotPassword_VerificationCodeAlert1"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if ([[self.pass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"ForgotPassword_passwordAlert1"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if (self.pass.text.length <= 4)
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"ChangePassword_passwordLengthAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if ([[self.confirm.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"ChangePassword_ConfirmPasswordAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if (![self.pass.text isEqualToString:self.confirm.text])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"ChangePassword_Password_confirmDontMatchAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            if([RestServiceAgent internetAvailable])
            {
                [[UsersManager getInstance] changePassword:[_userId intValue] verificationCode:self.code.text newPassword:self.pass.text and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            [SVProgressHUD dismiss];
                            if([[currentClient objectForKey:@"status"] intValue] == 1)
                            {
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                message:[[Language sharedInstance] stringWithKey:@"ChangePassword_successChange"]
                                                                               delegate:nil
                                                                      cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                                [self backToLogin];
                                //[self.navigationController popViewControllerAnimated:YES];
                            }
                            else{
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Password Not Sent Successfuly" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else
            {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
    
}

-(void)backToLogin{

    for (int i = 0 ; i < [self.navigationController.viewControllers count] ; i++) {
        UIViewController* obj =self.navigationController.viewControllers[i];
        if ([obj isKindOfClass:[LoginViewController class]]) {
            [self.navigationController popToViewController:obj animated:YES];
            break;
        }
    }

}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
@end
