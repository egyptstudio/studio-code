//
//  PostsViewController.m
//  FC Barcelona
//
//  Created by Eissa on 10/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "PostsViewController.h"
#import "UserComment.h"
#import "WallManager.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "CommentsViewController.h"
#import "Post.h"
#import "WebImage.h"
#import "UserPhoto.h"
#import "PostDetailsViewController.h"
#import "ShareManagerViewController.h"
#import "PostCell.h"
#import "CurrentUser.h"
#import "GetAndPushCashedData.h"
#import <GoogleMobileAds/DFPBannerView.h>
#import <GoogleMobileAds/DFPRequest.h>

@interface PostsViewController ()<UITableViewDataSource,UITableViewDelegate>
@end
@implementation PostsViewController
{
    CGFloat lastContentOffset;
    BOOL isWall;
    NSMutableArray * resultsDictionary;
    NSMutableArray* postsArray;
    ShareManagerViewController * shareView;
    CommentsViewController * commentsView;
    UIRefreshControl * refreshControl;
    int postsNo;
    NSString *lastRequestTime;
    NSString *timeFilter;
    NSMutableArray* countriesArray;
    BOOL favoritePostsFlag;
    BOOL isFrefreshing;
}

@synthesize requestType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)scrollTopTop{

    @try {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

    }
    @catch (NSException *exception) {
        lastRequestTime = @"";
        timeFilter = @"0";
        [self getPosts];

    }


}
- (void)viewDidLoad
{

  
   lastRequestTime = @"";
    timeFilter = @"0";
    [super viewDidLoad];
    postsNo = 0;
    countriesArray = [[NSMutableArray alloc] init];
    resultsDictionary = [[NSMutableArray alloc] init];
    postsArray = [[NSMutableArray alloc] init];

    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    self.tableView.tableFooterView = footerView;
    [self.tableView setSeparatorStyle:(UITableViewCellSeparatorStyleNone)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                            action:@selector(refreshPosts)
                  forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];

}
-(void)viewWillAppear:(BOOL)animated{
    postsNo = postsNo > 0 ? postsNo : 10;
    [self getPosts];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)refreshPosts{
    isFrefreshing = YES;
    timeFilter = @"1";
    if (requestType==2) {
        lastRequestTime = [resultsDictionary count]==0 ?@"" :
        [NSString stringWithFormat:@"%ld",[(Post*)[resultsDictionary firstObject]  lastUpdateTime]];
    }
    else{
        lastRequestTime = [resultsDictionary count]==0 ?@"" :
        [NSString stringWithFormat:@"%ld",[(Post*)[resultsDictionary firstObject]  lastUpdateTime]];
        
    }
    
    postsNo = 10;
    [self getPosts];
}

-(void)getPosts
{
    [self hideEmptyPhotosView];

    switch (requestType) {
        case 1:
        {
            [self getWall];
            break;
        }
        case 2:
        {
            if (postsNo<=100) {
                [self getLatest];
            }
            break;
        }
        case 3:
        {
            [self getGallery];
            break;
        }
        default:
            break;
    }
    [refreshControl endRefreshing];
}

-(void)getWall
{
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance]getWall:[[CurrentUser getObject] getUser].userId postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter countryList:countriesArray favoritePostsFlag:favoritePostsFlag and:^(id currentClient) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (favoritePostsFlag || [countriesArray count]>0) {
                        if ([currentClient count]>0) {
                                [resultsDictionary removeAllObjects];
                                [resultsDictionary addObjectsFromArray:currentClient] ;
                            [_tableView reloadData];
                            [self hideEmptyPhotosView];
                        }
                        else{
                            [resultsDictionary removeAllObjects];
                            [_tableView reloadData];
                            [self showEmptyPhotosViewWithMessage:[[Language sharedInstance] stringWithKey:@"filterResult_NoResults"]];
                            postsNo-=10;
                        }
                    }
                    else{
                        if ([currentClient count]>0) {
                            if (isFrefreshing) {
                                for (int i = 0 ; i < [currentClient count]; i++) {
                                    [resultsDictionary insertObject:currentClient[i] atIndex:0];
                                }
                            }else{
                                [resultsDictionary addObjectsFromArray:currentClient] ;
                            }
                            [self prepareAndCacheSectionWithName:@"Wall"];
                            [_tableView reloadData];
                            [self hideEmptyPhotosView];
                            
                        }
                        else{
                            [self showEmptyPhotosViewWithMessage:[[Language sharedInstance] stringWithKey:@"HomeInitialState_MypicsNoPhotosMessage"]];
                            postsNo-=10;
                        }
                    }
                    
                    
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [resultsDictionary removeAllObjects];
                    [resultsDictionary addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Wall"]];
                    postsNo = [resultsDictionary count];
                    [_tableView reloadData];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    postsNo-=10;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [resultsDictionary removeAllObjects];
                [resultsDictionary addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Wall"]];
                postsNo = [resultsDictionary count];
                [_tableView reloadData];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)getLatest
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance] getLatest:[[CurrentUser getObject] getUser].userId postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter countryList:countriesArray and:^(id currentClient) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([countriesArray count]>0) {
                        if ([currentClient count]>0) {
                            [resultsDictionary removeAllObjects];
                            [resultsDictionary addObjectsFromArray:currentClient] ;
                            [_tableView reloadData];
                            [self hideEmptyPhotosView];
                        }
                        else{
                            [resultsDictionary removeAllObjects];
                            [_tableView reloadData];
                            [self showEmptyPhotosViewWithMessage:[[Language sharedInstance] stringWithKey:@"filterResult_NoResults"]];
                            postsNo-=10;
                        }
                    }
                    else{
                        if ([currentClient count]>0) {
                            if (isFrefreshing) {
                                for (int i = 0 ; i < [currentClient count]; i++) {
                                    [resultsDictionary insertObject:currentClient[i] atIndex:0];
                                }
                            }else{
                                [resultsDictionary addObjectsFromArray:currentClient] ;
                            }
                            [self prepareAndCacheSectionWithName:@"Latest"];
                            [_tableView reloadData];
                            [self hideEmptyPhotosView];
                            
                        }
                        else{
                            [self showEmptyPhotosViewWithMessage:[[Language sharedInstance] stringWithKey:@"HomeInitialState_MypicsNoPhotosMessage"]];
                            postsNo-=10;
                        }
                    }

                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [resultsDictionary removeAllObjects];
                    [resultsDictionary addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Latest"]];
                    postsNo = [resultsDictionary count];
                    [_tableView reloadData];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    postsNo-=10;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [resultsDictionary removeAllObjects];
                [resultsDictionary addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Latest"]];
                postsNo = [resultsDictionary count];
                [_tableView reloadData];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

-(void)showEmptyPhotosViewWithMessage:(NSString*)msg{
    if(![resultsDictionary count]>0)
    {
        [self.noPhotosLabel setText:msg];
        [self.noPhotosView setHidden:NO];
    }
}


-(void)getGallery
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance]getMyPosts:[[CurrentUser getObject] getUser].userId postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient count]>0) {
                        if (isFrefreshing) {
                            for (int i = 0 ; i < [currentClient count]; i++) {
                                [resultsDictionary insertObject:currentClient[i] atIndex:0];

                            }
                        }else{
                            [resultsDictionary removeAllObjects];
                            [resultsDictionary addObjectsFromArray:currentClient] ;
                        }
                        [self prepareAndCacheSectionWithName:@"Gallery"];
                        [_tableView reloadData];
                        [self hideEmptyPhotosView];
                    }
                    else{
                        [self showEmptyPhotosViewWithMessage:[[Language sharedInstance] stringWithKey:@"HomeInitialState_MypicsNoPhotosMessage"]];
                        postsNo-=10;
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [resultsDictionary removeAllObjects];
                    [resultsDictionary addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Gallery"]];
                    postsNo = [resultsDictionary count];
                    [_tableView reloadData];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    postsNo-=10;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [resultsDictionary removeAllObjects];
                [resultsDictionary addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:@"Gallery"]];
                postsNo = [resultsDictionary count];
                [_tableView reloadData];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:
                                      [[Language sharedInstance] stringWithKey:@"connection_noConnection"]
                                                               delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}



-(void)prepareAndCacheSectionWithName:(NSString*)section{
    for (int i = 0 ; i < [resultsDictionary count]; i++) {
        NSMutableArray * allPhotosTags = [[NSMutableArray alloc] init];
        for (int j = 0 ; j < [[(Post*)resultsDictionary[i] tags] count]; j++) {
            if (![[(Post*)resultsDictionary[i] tags][j] isKindOfClass:[StudioPhotoTag class]]) {
                NSDictionary* dic = [[NSDictionary alloc] initWithDictionary:[(Post*)resultsDictionary[i] tags][j]];
                StudioPhotoTag* tag = [[StudioPhotoTag alloc] init];
                tag.tagName =  dic[@"tagName"];
                tag.tagId   = [dic[@"tagId"] intValue];
                tag.tagType = [dic[@"tagType"]intValue];
                [allPhotosTags addObject:tag];
            }
        }
        [(Post*)resultsDictionary[i] setTags:allPhotosTags];
    }
    [GetAndPushCashedData cashObject:resultsDictionary withAction:section];
}

-(void)hideEmptyPhotosView{
    [self.noPhotosView setHidden:YES];
}
//Alrassas
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    
    if (scrollOffset == 0)
    {
        // then we are at the top
        [_delegate didScrollToDirection:ScrollDirectionUp];
    }
    else
    {
        
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height + 6) {
        [_delegate didScrollToDirection:ScrollDirectionCrazy];
        if ([resultsDictionary count] >= 6)
        {
            isFrefreshing = NO;
            timeFilter = @"0";
            postsNo =[resultsDictionary count]==0 ? 10 : [resultsDictionary count]+10 ;
            
            if (requestType==2) {
                lastRequestTime = [resultsDictionary count]==0 ?@"" :
                [NSString stringWithFormat:@"%ld",[(Post*)[resultsDictionary lastObject]  lastUpdateTime]];

            }
            else{
                lastRequestTime = [resultsDictionary count]==0 ?@"" :
                [NSString stringWithFormat:@"%ld",[(Post*)[resultsDictionary lastObject]  lastUpdateTime]];

            }
            
            
            [self getPosts];
        }

        return;
    }else{
        ScrollDirection scrollDirection;
        if (lastContentOffset > scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionUp;
        else if (lastContentOffset < scrollView.contentOffset.y)
            scrollDirection = ScrollDirectionDown;
        
        lastContentOffset = scrollView.contentOffset.y;
        [_delegate didScrollToDirection:scrollDirection];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [resultsDictionary count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PostCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"PostCell"];
    
    UITableViewCell *adsCell ;
    if ([self adsIndex:indexPath.row]) {
        //        GADAdSize customAdSize = GADAdSizeFromCGSize(CGSizeMake(250, 250));
        adsCell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 90 :50)];
        [adsCell setBackgroundColor:[UIColor blackColor]];
        UIView *adsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 90 :50)];
        [adsCell addSubview:adsView];
        
        
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        bannerView.rootViewController = self;
        bannerView.adUnitID = @"ca-app-pub-2924334778141972/3060492441";
        
        
        GADRequest *request = [GADRequest request];
        request.testDevices = @[@"abc7f676a40f3514a5a8c71ad28dfd27"];
        [bannerView loadRequest:request];
        [adsView addSubview:bannerView];
    }
    else{
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PostCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.isEditable = requestType == 3 ? YES : NO ;
            [cell initWithPost:(Post*)[resultsDictionary objectAtIndex:indexPath.row] andController:self];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setTag:indexPath.row];
            [cell setAllPosts:resultsDictionary];
            [cell setWalltype:requestType];
            [cell setLastRequestTime:lastRequestTime];
            [cell setTimeFilter:timeFilter];
            [cell setCountriesArray:countriesArray];
            [cell setFavoritePostsFlag:favoritePostsFlag];
        }
    
    }

    
    
    return [self adsIndex:indexPath.row] ? adsCell : cell ;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  [self adsIndex:indexPath.row] ? UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 90 :50 : UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 700 :310 ;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (![self adsIndex:indexPath.row]) {
        [(PostCell*)cell performSelectorInBackground:@selector(initImages) withObject:nil];
    }
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)applyFilerWithIsFavorate:(BOOL)favorate andCountries:(NSArray*)array{

    if (!array) {
        [countriesArray removeAllObjects];
        favoritePostsFlag = favorate;
    }
    else{
        [countriesArray removeAllObjects];
        [countriesArray addObjectsFromArray:array];
        favoritePostsFlag = favorate;
    }
    [resultsDictionary removeAllObjects];
    [_tableView reloadData];
    postsNo = 10;
    [self getPosts];
}
-(BOOL)adsIndex:(int)index{
    return (index+1)%6==0;
}
@end


    