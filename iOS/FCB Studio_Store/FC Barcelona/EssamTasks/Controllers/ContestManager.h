//
//  ContestManager.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessManager.h"

@interface ContestManager : BusinessManager

+(ContestManager*)getInstance;
-(void)getContests:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
@end
