//
//  InfoViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/19/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (weak, nonatomic) IBOutlet UIButton *helpButton;
@property (weak, nonatomic) IBOutlet UIButton *privacyButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutFCBStudioButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutFCBarcelonaButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutTawasolitButton;
@property (weak, nonatomic) IBOutlet UIButton *freePointsButton;
@property (weak, nonatomic) IBOutlet UIButton *faqButton;

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

- (IBAction)helpButton_Action:(id)sender;
- (IBAction)privacyButton_Action:(id)sender;
- (IBAction)aboutFCBStudioButton_Action:(id)sender;
- (IBAction)aboutFCBarcelonaButton_Action:(id)sender;
- (IBAction)aboutTawasolitButton_Action:(id)sender;
- (IBAction)faqButton_Action:(id)sender;
- (IBAction)freePointsButton_Action:(id)sender;

- (IBAction)openStudio:(id)sender;
- (IBAction)back_Action:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

@end
