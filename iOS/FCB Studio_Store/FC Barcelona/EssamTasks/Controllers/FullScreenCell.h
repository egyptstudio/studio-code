//
//  FullScreenCell.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/4/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import <Foundation/Foundation.h>
#import "AvatarImageView.h"

@protocol FullScreenlDelegate <NSObject>
@required
- (void)didViewPost:(Post*)viewedPost;
@end
@interface FullScreenCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
{
//    id <FullScreenlDelegate> _delegate;
}
@property (nonatomic,strong) id delegate;
@property (nonatomic,assign) id <FullScreenlDelegate> fullScreenlDelegate;

@property(assign) Post* currentPost;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong) NSMutableArray * allPosts;

-(void)initWithPost:(Post*)post andController:(UIViewController*)controller;


- (void)like:(id)sender;
- (void)details:(id)sender;
- (void)stopAnyOperations;
@end
