//
//  ShopManager.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "ShopManager.h"
#import "GetAndPushCashedData.h"
@implementation ShopManager

static ShopManager* instance;
+(ShopManager *)getInstance{
    if (!instance) {
        instance=[[ShopManager alloc] init];
    }
    return instance;
}



-(void)LoadInAppProductIDs:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation],@"IOS", nil];
    NSArray* keys = [NSArray arrayWithObjects:@"lang",@"deviceType", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"store/LoadInAppProductIDs" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)getStoreLink:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation], nil];
    NSArray* keys = [NSArray arrayWithObjects:@"lang", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"store/getStoreLink" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)myPremiumStatus:(int)userID  and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation],@(userID), nil];
    NSArray* keys = [NSArray arrayWithObjects:@"lang",@"userID", nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"store/myPremiumStatus" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

-(void)subscripeInPremium:(int)userID subscriptionDuration:(int)subscriptionDuration transactionId:(NSString* )transactionId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    

    
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation],
                     @(userID),
                     @(subscriptionDuration),
                     transactionId,
                     @(2),
                     [GetAndPushCashedData getObjectFromCash:@"deviceToken"]?[GetAndPushCashedData getObjectFromCash:@"deviceToken"]:@"",nil];
    NSArray* keys = [NSArray arrayWithObjects:@"lang",
                     @"userID",
                     @"subscriptionDuration",
                     @"TransID",
                     @"deviceType",
                     @"deviceToken",nil];
    
    

    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"store/subscripeInPremium" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
    
}

@end
