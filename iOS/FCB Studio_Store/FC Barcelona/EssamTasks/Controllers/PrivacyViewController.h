//
//  PrivacyViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/18/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

@property (weak, nonatomic) IBOutlet UILabel *friendRequestsLabel;
@property (weak, nonatomic) IBOutlet UILabel *friendRequestsValue;

@property (weak, nonatomic) IBOutlet UILabel *personalInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalInfoValue;

@property (weak, nonatomic) IBOutlet UILabel *contactsInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactsInfoValue;

@property (weak, nonatomic) IBOutlet UILabel *pushStateLabel;
@property (weak, nonatomic) IBOutlet UISwitch *pushStateButton;



- (IBAction)friendRequests_Action:(id)sender;
- (IBAction)personalInfo_Action:(id)sender;
- (IBAction)contactsInfo_Action:(id)sender;
- (IBAction)pushState_Action:(UISwitch*)sender;


- (IBAction)back_Action:(id)sender;
- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;
@end
