//
//  BenefitsViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/2/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BenefitsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

- (IBAction)back_Action:(id)sender;

@end
