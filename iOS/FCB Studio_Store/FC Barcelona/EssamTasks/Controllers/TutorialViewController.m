//
//  TutorialViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 3/5/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "TutorialViewController.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
#import "WebImage.h"
#import "HelpScreen.h"
#import "GetAndPushCashedData.h"
@interface TutorialViewController ()<RGMPagingScrollViewDatasource,RGMPagingScrollViewDelegate>

@end

@implementation TutorialViewController
{
    NSMutableArray *helpScreens;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _scrollView.scrollDirection = RGMScrollDirectionHorizontal;
    [_scrollView setDelegate:self];
    [_scrollView setDatasource:self];
    helpScreens = [[NSMutableArray alloc] init];

}

-(void)viewWillAppear:(BOOL)animated{
    [_skipButton setTitle:[[Language sharedInstance] stringWithKey:@"AppEntry_Skip"] forState:UIControlStateNormal];
    [self getHelpScreens];
}

-(void)getHelpScreens{
    
    @try {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if([RestServiceAgent internetAvailable])
        {
            [[UsersManager getInstance] getHelpScreens:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        if ([currentClient count]>0) {
                            [helpScreens addObjectsFromArray:currentClient];
                            _pageNumber.text = [NSString stringWithFormat:@"1 Of %lu",(unsigned long)[helpScreens count]];
                            [_scrollView reloadData];
                        }
                        else{
                            [self skip_Action:nil];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

-(NSInteger)pagingScrollViewNumberOfPages:(RGMPagingScrollView *)pagingScrollView{
    return [helpScreens count];
}

-(UIView *)pagingScrollView:(RGMPagingScrollView *)pagingScrollView viewForIndex:(NSInteger)idx{
    
    NSDictionary *currentScreen = helpScreens[idx];
    
    NSString* nibName = (self.helpFlag) ? @"HelpView" : @"TutorialView";
    
    UIViewController *cell = [[UIViewController alloc] initWithNibName:nibName bundle:nil];
    
    UIImageView* screenImage   = (UIImageView*)[cell.view viewWithTag:1];
    UIActivityIndicatorView *screenImageLoader = (UIActivityIndicatorView* )[cell.view viewWithTag:2] ;
    [screenImage sd_setImageWithURL:[NSURL URLWithString:currentScreen[@"screenImageUrl"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [screenImageLoader stopAnimating];
    }];
    
    
    UIImageView* screenIcon   = (UIImageView*)[cell.view viewWithTag:3];
    UIActivityIndicatorView *screenIconLoader = (UIActivityIndicatorView* )[cell.view viewWithTag:4] ;
    [screenIcon sd_setImageWithURL:[NSURL URLWithString:currentScreen[@"screenIconUrl"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [screenIconLoader stopAnimating];
    }];
    
    UILabel* screenTitle            = (UILabel*)[cell.view viewWithTag:5];
    UILabel* screenDesc             = (UILabel*)[cell.view viewWithTag:6];
    screenTitle.text = currentScreen[@"screenTitle"];
    screenDesc.text =  currentScreen[@"description"];
    [screenDesc sizeToFit];
    return cell.view;
}


- (void)pagingScrollView:(RGMPagingScrollView *)pagingScrollView scrolledToPage:(NSInteger)idx{
    _pageNumber.text = [NSString stringWithFormat:@"%li Of %lu",idx+1,(unsigned long)[helpScreens count]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)shouldAutorotate{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)skip_Action:(id)sender{
    [GetAndPushCashedData cashObject:@(YES) withAction:@"tutorialOpend"];

    [self.navigationController popViewControllerAnimated:YES];
}

@end
