//
//  FullScreenCell.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/4/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FullScreenCell.h"
#import "WebImage.h"
#import "RestServiceAgent.h"
#import "WallManager.h"
#import "CurrentUser.h"
#import "User.h"
#import "SVProgressHUD.h"


@implementation FullScreenCell
{
    BOOL isScrolling;
    UIViewController* mainController;
    AvatarImageView *userImage;
    UILabel *userName;
    UIView *rankView;
    UILabel *likesCount;
    UIButton *likeButton;
    UIActivityIndicatorView *smallLoader;
    UILabel *ranks;
    
    int postsNo;
    NSString *lastRequestTime;
    NSString *timeFilter;
    UIRefreshControl * refreshControl;
    BOOL hidden;
    UISwipeGestureRecognizer *swipe;
}
//andPost:

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andPost:(Post*)post;
{
    return [self initWithStyle:style reuseIdentifier:reuseIdentifier];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        refreshControl = [[UIRefreshControl alloc] init];
        refreshControl.tintColor = [UIColor whiteColor];
        [refreshControl addTarget:self
                           action:@selector(refreshPosts)
                 forControlEvents:UIControlEventValueChanged];
        [_collectionView addSubview:refreshControl];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)initWithPost:(Post*)post andController:(UIViewController*)controller{
   
    hidden = NO;
    
    lastRequestTime = @"";
    timeFilter = @"0";
    postsNo = 0;
        
    mainController = controller;
    _allPosts = [[NSMutableArray alloc] init];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _currentPost = post;
    
    [_allPosts addObject:post];
    [_collectionView reloadData];
    swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(getUserPosts)];
    swipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:swipe];

    
}
- (void)stopAnyOperations;
{
    _collectionView.delegate = nil;
    _collectionView.dataSource = nil;
    hidden = YES;
}
-(void)refreshPosts{
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    timeFilter = @"1";
    lastRequestTime = [_allPosts count]==0 ?@"" : [defaults objectForKey:@"currentRequestTime"];
    [self getUserPosts];
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_allPosts count];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.frame.size;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];

    
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:2] ;
    UIImageView* image   = (UIImageView*)[cell viewWithTag:1];

    
    [image sd_setImageWithURL:[NSURL URLWithString:[(Post*)[_allPosts objectAtIndex:indexPath.row] postPhotoUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    userImage     =  (AvatarImageView*)[cell viewWithTag:3];
    userName      =  (UILabel*)[cell viewWithTag:4];
    
    rankView      =  (UIView*)[cell viewWithTag:5];
    
    likesCount    =  (UILabel*)[cell viewWithTag:6];
    
    likeButton    =  (UIButton*)[cell viewWithTag:7];
    
    smallLoader   =  (UIActivityIndicatorView*)[cell viewWithTag:8];
    ranks         =  (UILabel*)[cell viewWithTag:9];
    
    UIButton *backButton = (UIButton*)[cell viewWithTag:10];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    [likeButton addTarget:self action:@selector(like:) forControlEvents:UIControlEventTouchUpInside];
    userImage.borderColor = [UIColor whiteColor];
    
    [userImage sd_setImageWithURL:[NSURL URLWithString:[(Post*)[_allPosts objectAtIndex:indexPath.row] userProfilePicUrl]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [smallLoader stopAnimating];
        [smallLoader setHidden:YES];
    }];
    
    userName.text = [(Post*)[_allPosts objectAtIndex:indexPath.row] userName];
    
    likesCount.text =[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]>999? [NSString stringWithFormat:@"%d k",[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]/1000]:[NSString stringWithFormat:@"%d",[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]];
    
    if ([(Post*)[_allPosts objectAtIndex:indexPath.row] contestRank]>0 &&[(Post*)[_allPosts objectAtIndex:indexPath.row] contestRank]<=10) {
        [rankView setHidden:NO];
    }
    
    if ([(Post*)[_allPosts objectAtIndex:indexPath.row]  liked]) {
        [likeButton setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
    }
    else {
        [likeButton setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
    }
    
    if ([(Post*)[_allPosts objectAtIndex:indexPath.row]  originalPostId]!=0) {
        UILabel* reposter = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(userName.frame),
                                                                      CGRectGetMaxY(userName.frame),
                                                                      userName.frame.size.width+10,
                                                                      userName.frame.size.height)];
        reposter.font = [UIFont systemFontOfSize:13];
        [reposter setTextColor:[UIColor blackColor]];
        reposter.text = [NSString stringWithFormat:@"Reposted By, %@",[(Post*)[_allPosts objectAtIndex:indexPath.row]  rePostedBy]];
        [cell addSubview:reposter];
        
        UIImageView * reposterState = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(userName.frame),
                                                                                    CGRectGetMinY(userName.frame),
                                                                                    userName.frame.size.height,
                                                                                    userName.frame.size.height)];
        
        UIImage *state = [(Post*)[_allPosts objectAtIndex:indexPath.row] following] ? [UIImage imageNamed:@"followed.png" ]:[UIImage imageNamed:@"unfollowed.png"];
        
        reposterState.image = state;
        [cell addSubview:reposterState];
    }
    
    return cell;
}
-(void)back{
    [mainController.navigationController popViewControllerAnimated:YES];
}
-(void)getUserPosts
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];

    @try {
        if ([[CurrentUser getObject] isUser]) {
            if ([RestServiceAgent internetAvailable]) {
                postsNo+=10;
                [[WallManager getInstance] getUserPosts:[[CurrentUser getObject] getUser].userId fanId:[_currentPost userId] postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if (!hidden) {
                            if ([currentClient count]>0) {
                                if ([_allPosts count]==1) {
                                    [currentClient removeObjectAtIndex:0];
                                    [_allPosts addObjectsFromArray:currentClient] ;
                                    [_collectionView reloadData];
                                    
                                    [self removeGestureRecognizer:swipe];
                                    
                                    @try {
                                        [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]  atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
                                    }
                                    @catch (NSException *exception) {
                                        
                                    }
                                    
                                    
                                }
                                else
                                {
                                    [_allPosts addObjectsFromArray:currentClient] ;
                                    [_collectionView reloadData];
                                }
                            }
                        }
                        
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];

                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                        postsNo--;
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];

                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];

        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}






- (IBAction)like:(id)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:buttonPosition];
    
    
    if ([[CurrentUser getObject] isUser]) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if (![(Post*)[_allPosts objectAtIndex:indexPath.row] liked]) {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] likePost:[[CurrentUser getObject] getUser].userId postId:[(Post*)[_allPosts objectAtIndex:indexPath.row] postId]
                                                    and:^(id currentClient) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [SVProgressHUD dismiss];
                                                            if (currentClient) {
                                                                if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                                {
                                                                    [sender setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
                                                                    
                                                                    [(Post*)[_allPosts objectAtIndex:indexPath.row] setLiked:YES] ;
                                                                    [(Post*)[_allPosts objectAtIndex:indexPath.row] setLikeCount:[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]+1];
                                                                    
                                                                    likesCount.text =[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]>999? [NSString stringWithFormat:@"%d k",[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]/1000]:[NSString stringWithFormat:@"%d",[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]];
                                                                }
                                                            }
                                                        });
                                                    }onFailure:^(NSError *error) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [SVProgressHUD dismiss];
                                                            
                                                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                            [alert show];
                                                        });
                                                    }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
        else
        {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] dislikePost:[[CurrentUser getObject] getUser].userId postId:[(Post*)[_allPosts objectAtIndex:indexPath.row] postId]
                                                       and:^(id currentClient) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [SVProgressHUD dismiss];
                                                               if (currentClient) {
                                                                   if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                                   {
                                                                       [sender setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
                                                                       
                                                                       [(Post*)[_allPosts objectAtIndex:indexPath.row] setLiked:NO] ;
                                                                       [(Post*)[_allPosts objectAtIndex:indexPath.row] setLikeCount:[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]-1];
                                                                       
                                                                       likesCount.text =[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]>999? [NSString stringWithFormat:@"%d k",[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]/1000]:[NSString stringWithFormat:@"%d",[(Post*)[_allPosts objectAtIndex:indexPath.row] likeCount]];
                                                                       
                                                                   }
                                                               }
                                                           });
                                                       }onFailure:^(NSError *error) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [SVProgressHUD dismiss];
                                                               
                                                               UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                               [alert show];
                                                           });
                                                       }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];

    }
    
    
    
}
- (IBAction)details:(id)sender{
    
    
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView contentOffset].x + _collectionView.frame.size.width== [scrollView contentSize].width + 20) {
        if ([_allPosts count] >= 9)
        {
            timeFilter = @"0";
            lastRequestTime = [_allPosts count]==0 ?@"" :
            [NSString stringWithFormat:@"%lld",[(Post*)[_allPosts lastObject]  lastUpdateTime]];
            [self getUserPosts];
        }
        return;
    }
}


@end
