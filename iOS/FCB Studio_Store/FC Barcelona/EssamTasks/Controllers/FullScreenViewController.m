//
//  FullScreenViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 11/30/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FullScreenViewController.h"
#import "PostsViewController.h"
#import "UserComment.h"
#import "WallManager.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "CommentsViewController.h"
#import "Post.h"
#import "WebImage.h"
#import "UserPhoto.h"
#import "PostDetailsViewController.h"
#import "ShareManagerViewController.h"
#import  "PostCell.h"
#import "CurrentUser.h"
#import "GetAndPushCashedData.h"
#import "FullScreenImageViewController.h"
#import "FullScreenPageViewController.h"
#import "FullScreenHorizontalViewController.h"
#import "ImageDetailViewController.h"
#import "FansDetailView.h"
@interface FullScreenViewController ()

@end

@implementation FullScreenViewController
{


    NSMutableArray* userPostsList;
    Post *post;
    int pageNumber;
    RGMPagingScrollView *horizontalScroll;
    UIRefreshControl * refreshControl;

    
    UIViewController* mainController;
    AvatarImageView *userImage;
    UILabel *userName;
    UIView *rankView;
    UILabel *likesCount;
    UIButton *likeButton;
    UIActivityIndicatorView *smallLoader;
    UILabel *ranks;

    
    NSUInteger _numPages;
    int postsNo;
    int postsNo2;

    Post* _currentPost ;
    NSMutableArray *allPostsPhotos;
    int currentPage;
    BOOL isFrefreshing;

}

@synthesize allPosts;
@synthesize currentPost;
@synthesize wallType;
@synthesize lastRequestTime;
@synthesize timeFilter;
@synthesize countriesArray;
@synthesize favoritePostsFlag;
@synthesize selectedPostIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];



    userPostsList = [[NSMutableArray alloc] init];
    _scrollView.scrollDirection = RGMScrollDirectionVertical;
    allPostsPhotos = [[NSMutableArray alloc] init];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshPosts)
             forControlEvents:UIControlEventValueChanged];
    

    [self performSelector:@selector(checkOrientation) withObject:nil afterDelay:.7];
    [_scrollView reloadData];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [_scrollView setCurrentPage:wallType==3 ? 0:selectedPostIndex];
}
-(void)viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationChanged:)
                                                name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
}

-(void)refreshPosts{
    timeFilter = @"1";
    lastRequestTime = [allPosts count]==0 ?@"" :
    [NSString stringWithFormat:@"%lu",[(Post*)[allPosts firstObject] lastUpdateTime]];
    [self getPosts];
}

-(NSInteger)pagingScrollViewNumberOfPages:(RGMPagingScrollView *)pagingScrollView{
    return pagingScrollView ==_scrollView ? wallType==3 ? 1 : [allPosts count] : _numPages;
}


- (UIView *)pagingScrollView:(RGMPagingScrollView *)pagingScrollView viewForIndex:(NSInteger)idx
{
    if (pagingScrollView == _scrollView ) {
        [allPostsPhotos removeAllObjects];
        if (wallType==3)
        {
            [allPostsPhotos addObjectsFromArray:allPosts];
            _numPages = [allPosts count];

        }
        else
        {
            [allPostsPhotos addObject:(Post*)[allPosts objectAtIndex:idx]];
            _numPages = 1;

        }
        _currentPost = (Post*)[allPosts objectAtIndex:idx];
        horizontalScroll = [[RGMPagingScrollView alloc] initWithFrame:_scrollView.bounds];
        horizontalScroll.delegate = self;
        horizontalScroll.datasource = self;
        horizontalScroll.backgroundColor = [UIColor clearColor];
        horizontalScroll.scrollDirection = RGMScrollDirectionHorizontal;
        [horizontalScroll reloadData];
        if (wallType==3)
            [horizontalScroll setCurrentPage:selectedPostIndex];
        return horizontalScroll;
    }
    else{
        Post *tmpPost = (Post*)[allPostsPhotos objectAtIndex:idx] ;
        NSArray *allDock = [[NSArray alloc] initWithArray:[[NSBundle mainBundle]
                                                           loadNibNamed:@"FullScreenView"
                                                           owner:self options:nil]];
        UIView *cell = (UIView*)[allDock objectAtIndex:0];
        
        UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:2] ;
        UIImageView* image   = (UIImageView*)[cell viewWithTag:1];
        

        [image sd_setImageWithURL:[NSURL URLWithString:[tmpPost postPhotoUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [loader stopAnimating];
            [loader setHidden:YES];
        }];
        
        userImage     =  (AvatarImageView*)[cell viewWithTag:3];
        userName      =  (UILabel*)[cell viewWithTag:4];
        userName.text = [tmpPost userName];
        likesCount    =  (UILabel*)[cell viewWithTag:6];
        
        likeButton    =  (UIButton*)[cell viewWithTag:7];
        
        smallLoader   =  (UIActivityIndicatorView*)[cell viewWithTag:8];
        ranks         =  (UILabel*)[cell viewWithTag:9];
        
        UIButton *backButton = (UIButton*)[cell viewWithTag:10];
        [backButton addTarget:self action:@selector(back_Action:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *detailsButton = (UIButton*)[cell viewWithTag:20];
        [detailsButton addTarget:self action:@selector(postDetails) forControlEvents:UIControlEventTouchUpInside];
        
        
        [likeButton addTarget:self action:@selector(like:) forControlEvents:UIControlEventTouchUpInside];
        userImage.borderColor = [UIColor whiteColor];
        
        [userImage sd_setImageWithURL:[NSURL URLWithString:[tmpPost userProfilePicUrl]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [smallLoader stopAnimating];
            [smallLoader setHidden:YES];
        }];
        
        userName.text = [tmpPost userName];
        likesCount.text =[tmpPost likeCount]>999? [NSString stringWithFormat:@"%d k",[tmpPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[tmpPost likeCount]];
        if ([tmpPost liked]) {
            [likeButton setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
        }
        else {
            [likeButton setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
        }
        [cell setFrame:_scrollView.frame];
        
        if ([allPostsPhotos count] == 1) {
            [pagingScrollView setContentSize:CGSizeMake(_scrollView.bounds.size.width+1, 0)];
        }
        
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFanDetails)];
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFanDetails)];
        tap2.cancelsTouchesInView = YES;
        tap2.numberOfTapsRequired = 1;
        tap3.cancelsTouchesInView = YES;
        tap3.numberOfTapsRequired = 1;
        [userImage addGestureRecognizer:tap2];
        userImage.userInteractionEnabled = YES;
        [userName addGestureRecognizer:tap3];
        userName.userInteractionEnabled = YES;
        
        
        
        
        if ([tmpPost originalPostId]!=0 /*&& [currentPost userId]!=[[[CurrentUser getObject] getUser] userId]*/)
        {
            @try {
                UILabel* reposter = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(userName.frame),
                                                                              CGRectGetMaxY(userName.frame),
                                                                              userName.frame.size.width+50,
                                                                              userName.frame.size.height)];
                reposter.font = [UIFont systemFontOfSize:10];
                [reposter setTextColor:[UIColor whiteColor]];
                NSString* repostedBy = [[[Language sharedInstance] stringWithKey:@"Home_repostedBy"] stringByAppendingString:@", "];
                reposter.text = [repostedBy stringByAppendingString:[tmpPost rePostedBy]];
                [cell addSubview:reposter];
            }
            @catch (NSException *exception) {
                
            }
        }
        
        
        UIButton* commentButton    =  (UIButton*)[cell viewWithTag:100];
        UIButton* repostButton     =  (UIButton*)[cell viewWithTag:101];
        UIButton* shareButton      =  (UIButton*)[cell viewWithTag:102];
        
          NSString* commentCount = [tmpPost commentCount]>999? [NSString stringWithFormat:@"  %dk",[tmpPost likeCount]/1000]:[NSString stringWithFormat:@"  %d",[tmpPost commentCount]];
        [commentButton setTitle:commentCount forState:UIControlStateNormal];
        [commentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([tmpPost rePosted]) {
            [repostButton setImage:[UIImage imageNamed:@"repost_sc.png"] forState:UIControlStateNormal];
        }
        [repostButton  addTarget:self action:@selector(rePost:)  forControlEvents:UIControlEventTouchUpInside];
        
        [shareButton   addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];

        return cell;
    
    }
    
}

- (void)openFanDetails
{
    Post* selectedPost = (Post*)allPostsPhotos[[horizontalScroll currentPage]];

    if([selectedPost userId]!=[[CurrentUser getObject] getUser].userId) {
        UIStoryboard * sb = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ?
        [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] :
        [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
        fansDetailView.fanID = [selectedPost userId];
        [self.navigationController pushViewController:fansDetailView animated:YES];
    }
    else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
    }
}

-(void)postDetails{
    ImageDetailViewController * commentsView = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageDetailViewController"];
    commentsView.isWall = NO;

//    if (wallType==3)
//    {
//        commentsView.currentPost = _currentPost;
//    }
//    else{
        commentsView.currentPost = (Post*)allPostsPhotos[[horizontalScroll currentPage]];
//    }
    
    [self.navigationController pushViewController:commentsView animated:YES];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView != _scrollView) {
        float scrollViewWidth = scrollView.frame.size.width;
        float scrollContentSizeWidth = scrollView.contentSize.width;
        float scrollOffset = scrollView.contentOffset.x;

        if (scrollOffset + scrollViewWidth == scrollContentSizeWidth+2) {
            if ([allPostsPhotos count]==1) {
                [self getUserPosts];
            }
        }
    }else{
    
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == -2)
        {
            isFrefreshing = YES;
            postsNo = 10 ;
           [self refreshPosts];
        }
        else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight+2)
        {
            isFrefreshing = NO;
            timeFilter = @"0";
            lastRequestTime = [allPostsPhotos count]==0 ?@"" :
            [NSString stringWithFormat:@"%ld",[(Post*)[allPostsPhotos lastObject]  lastUpdateTime]];
            
            postsNo =[allPosts count]==0 ? 10 : [allPosts count]+10;
            [self getPosts];
        }
    }
    
}
-(void)getUserPosts{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([[CurrentUser getObject] isUser]) {
            if ([RestServiceAgent internetAvailable]) {
                postsNo+=10;
                [[WallManager getInstance] getUserPosts:[[CurrentUser getObject] getUser].userId fanId: [(Post*)allPostsPhotos[[horizontalScroll currentPage]] userId] postsNo:50 lastRequestTime:@"" timeFilter:@"0" and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                            if ([currentClient count]>0) {
                                Post* readyPost = (Post*)allPostsPhotos[0];
                                allPostsPhotos = currentClient ;
                                [allPostsPhotos insertObject:readyPost atIndex:0];
                                int readyPostId = [(Post*)allPostsPhotos[0] postId];
                                for (int i = 1 ; i < [allPostsPhotos count]; i++) {
                                    if (readyPostId == [(Post*)allPostsPhotos[i] postId]) {
                                        [allPostsPhotos removeObjectAtIndex:i];
                                        break;
                                    }
                                }
                                    _numPages =[allPostsPhotos count];
                                    [horizontalScroll reloadData];
                                
                                if ([allPostsPhotos count]>0 && [allPostsPhotos count] ==1)
                                        [horizontalScroll setCurrentPage:0];
                                if ([allPostsPhotos count]>0 && [allPostsPhotos count] > 1)
                                    [horizontalScroll setCurrentPage:1];
                            }
                        
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                        postsNo2--;
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
- (IBAction)like:(id)sender{
    
    if ([[CurrentUser getObject] isUser]) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if (![(Post*)allPostsPhotos[[horizontalScroll currentPage]] liked]) {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] likePost:[[CurrentUser getObject] getUser].userId postId:[(Post*)allPostsPhotos[[horizontalScroll currentPage]] postId]
                                                    and:^(id currentClient) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [SVProgressHUD dismiss];
                                if (currentClient) {
                                    if([[currentClient objectForKey:@"status"] intValue] == 1)
                                    {
                                        [sender setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
                                        
                                        [(Post*)allPostsPhotos[[horizontalScroll currentPage]] setLiked:YES] ;
                                        [(Post*)allPostsPhotos[[horizontalScroll currentPage]] setLikeCount:[(Post*)allPostsPhotos[[horizontalScroll currentPage]] likeCount]+1];
                                        
                                        likesCount.text =[(Post*)allPostsPhotos[[horizontalScroll currentPage]] likeCount]>999? [NSString stringWithFormat:@"%d k",[(Post*)allPostsPhotos[[horizontalScroll currentPage]] likeCount]/1000]:[NSString stringWithFormat:@"%d",[(Post*)allPostsPhotos[[horizontalScroll currentPage]] likeCount]];
                                    }
                                }
                            });
                        }onFailure:^(NSError *error) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [SVProgressHUD dismiss];
                                    
                                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                    [alert show];
                                });
                            }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
        else
        {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] dislikePost:[[CurrentUser getObject] getUser].userId postId:[(Post*)allPostsPhotos[[horizontalScroll currentPage]] postId]
                                                       and:^(id currentClient) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [SVProgressHUD dismiss];
                           if (currentClient) {
                               if([[currentClient objectForKey:@"status"] intValue] == 1)
                               {
                                   [sender setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
                                   
                                   [(Post*)allPostsPhotos[[horizontalScroll currentPage]] setLiked:NO] ;
                                   [(Post*)allPostsPhotos[[horizontalScroll currentPage]] setLikeCount:[(Post*)allPostsPhotos[[horizontalScroll currentPage]] likeCount]-1];
                                   
                                   likesCount.text =[(Post*)allPostsPhotos[[horizontalScroll currentPage]] likeCount]>999? [NSString stringWithFormat:@"%d k",[(Post*)allPostsPhotos[[horizontalScroll currentPage]] likeCount]/1000]:[NSString stringWithFormat:@"%d",[(Post*)allPostsPhotos[[horizontalScroll currentPage]] likeCount]];
                                   
                               }
                           }
                       });
                   }onFailure:^(NSError *error) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [SVProgressHUD dismiss];
                           
                           UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                           [alert show];
                       });
                   }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}

-(void)getPosts{
    switch (wallType) {
        case 1:
            [self getWall];
            break;
        case 2:
            if (postsNo<=100) {
                [self getLatest];
            }
            break;
        case 3:
            [self getGallery];
            break;
        default:
            break;
    }
    [refreshControl endRefreshing];
}
-(void)getWall{
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            postsNo+=10;
            [[WallManager getInstance]getWall:[[CurrentUser getObject] getUser].userId postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter countryList:countriesArray favoritePostsFlag:favoritePostsFlag and:^(id currentClient) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient count]>0) {
                        
                        if (isFrefreshing) {
                            for (int i = 0 ; i < [currentClient count]; i++) {
                                [allPosts insertObject:currentClient[i] atIndex:0];
                            }
                        }else{
                            [allPosts addObjectsFromArray:currentClient] ;
                        }
                        [_scrollView reloadData];
                    }
                    else{
                        postsNo-=10;
                    }
                    
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    postsNo-=10;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                {
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                }
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)getLatest{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            postsNo+=10;
            [[WallManager getInstance] getLatest:[[CurrentUser getObject] getUser].userId postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter countryList:countriesArray and:^(id currentClient) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient count]>0) {
                        if (isFrefreshing) {
                            for (int i = 0 ; i < [currentClient count]; i++) {
                                [allPosts insertObject:currentClient[i] atIndex:0];
                            }
                        }else{
                            [allPosts addObjectsFromArray:currentClient] ;
                        }
                        [_scrollView reloadData];

                    }
                    else{
                        postsNo-=10;
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    postsNo-=10;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                {
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                }
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)getGallery{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            postsNo+=10;
            [[WallManager getInstance]getMyPosts:[[CurrentUser getObject] getUser].userId postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient count]>0) {
                        if (isFrefreshing) {
                            for (int i = 0 ; i < [currentClient count]; i++) {
                                [allPosts insertObject:currentClient[i] atIndex:0];
                            }
                        }else{
                            [allPosts addObjectsFromArray:currentClient] ;
                        }
                        [_scrollView reloadData];
                    }
                    else{
                        postsNo-=10;
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    postsNo-=10;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                {
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                }
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)checkOrientation{
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation != UIInterfaceOrientationPortrait )
    {

        [[[UIImageView alloc] init] sd_setImageWithURL:[NSURL URLWithString:[(Post*)allPostsPhotos[[horizontalScroll currentPage]] postPhotoUrl]]  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                FullScreenImageViewController * fullScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"FullScreenImageViewController"];
                fullScreen.orienaiton = UIImageOrientationRight;
                fullScreen.image = image;
                [self.navigationController pushViewController:fullScreen animated:NO];
        }];
        
    }
}
-(void) orientationChanged:(NSNotification *)note{
    
    if (![self.navigationController.topViewController isKindOfClass:[FullScreenImageViewController class]]) {
        UIDevice * device = note.object;
        switch(device.orientation)
        {
            case UIDeviceOrientationLandscapeLeft :
            {
                [[[UIImageView alloc] init] sd_setImageWithURL:[NSURL URLWithString:[(Post*)allPostsPhotos[[horizontalScroll currentPage]] postPhotoUrl]]  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    FullScreenImageViewController * fullScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"FullScreenImageViewController"];
                    fullScreen.orienaiton = UIImageOrientationRight;
                    fullScreen.image = image;
                    [self.navigationController pushViewController:fullScreen animated:NO];
                }];
                break;
            }
            case UIDeviceOrientationLandscapeRight :
            {
                [[[UIImageView alloc] init] sd_setImageWithURL:[NSURL URLWithString:[(Post*)allPostsPhotos[[horizontalScroll currentPage]] postPhotoUrl]]  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    FullScreenImageViewController * fullScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"FullScreenImageViewController"];
                    fullScreen.orienaiton = UIImageOrientationLeft;
                    fullScreen.image = image;
                    [self.navigationController pushViewController:fullScreen animated:NO];
                }];
                break;
            }
            default:
                break;
        };
        
    }
}
-(void)viewDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)comment:(UIButton*)sender {
    Post* selectedPost = (Post*)allPostsPhotos[[horizontalScroll currentPage]];
    if ([[CurrentUser getObject] isUser]) {
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CommentsViewController * commentsView = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
        commentsView.post = selectedPost;
        commentsView.userID  = [[[CurrentUser getObject] getUser] userId];
        commentsView.userName = [selectedPost userName];
        commentsView.delegate = self;
        commentsView.backGround = image;
        [self.navigationController pushViewController:commentsView animated:YES];
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
    
}

-(void)commentAdded{
    Post* selectedPost = (Post*)allPostsPhotos[[horizontalScroll currentPage]];
    selectedPost.commentCount++;
    [horizontalScroll reloadData];
    //commentsCount.text = [selectedPost commentCount]>999? [NSString stringWithFormat:@"%d k",[selectedPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[selectedPost commentCount]];
}

- (void)commentDeleted{
    Post* selectedPost = (Post*)allPostsPhotos[[horizontalScroll currentPage]];
    selectedPost.commentCount--;
    [horizontalScroll reloadData];
    //commentsCount.text = [selectedPost commentCount]>999? [NSString stringWithFormat:@"%d k",[selectedPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[selectedPost commentCount]];
}

- (void)share:(UIButton*)sender {
    
    Post* selectedPost = (Post*)allPostsPhotos[[horizontalScroll currentPage]];
    NSString *sharedLink = [NSString stringWithFormat:@"http://fcbstudio.mobi/share.php?postid=%i",selectedPost.postId];
    sharedLink = [sharedLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSArray *itemsToShare = @[@"",[NSURL URLWithString:sharedLink]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes =@[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact]; //or whichever you don't need
    [self presentViewController:activityVC animated:YES completion:nil];
}
-(void)rePost:(UIButton*)sender
{
    if([[CurrentUser getObject] isUser]){
        if ([GetAndPushCashedData getObjectFromCash:@"RememberCostAlert"]) {
            [self promotePost:sender];
        }
        else{
            
            NSString *title = [[Language sharedInstance] stringWithKey:@"RepostMessage"];
            NSString *message = [[Language sharedInstance] stringWithKey:@"DonAskAgain"];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] otherButtonTitles:[[Language sharedInstance] stringWithKey:@"Confirm"],nil];
            
            UIButton *checkBox = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30.0)];
            [checkBox setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
            [checkBox addTarget:self action:@selector(setDonOpenAgain:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 270, 30)];
            [view addSubview:checkBox];
            [checkBox setCenter:CGPointMake(view.frame.size.width/2, view.frame.size.height/2)];
            
            [alert setValue:view  forKey:@"accessoryView"];
            [alert setTag:1];
            [alert show];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}
-(void)setDonOpenAgain:(UIButton*)sender{
    
    if (![GetAndPushCashedData getObjectFromCash:@"RememberCostAlert"]) {
        [sender setBackgroundImage:[UIImage imageNamed:@"popup_checkbox_sc.png"] forState:UIControlStateNormal];
        [GetAndPushCashedData cashObject:@(YES) withAction:@"RememberCostAlert"];
    }
    else {
        [sender setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
        [GetAndPushCashedData removeObjectWithAction:@"RememberCostAlert"];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (alertView.tag) {
        case 1:
        {
            if (alertView.cancelButtonIndex != buttonIndex) {
                [self promotePost:nil];
            }
            break;
        }
        default:
            break;
    }
}
-(void)promotePost:(UIButton*)sender{
    Post* selectedPost = (Post*)allPostsPhotos[[horizontalScroll currentPage]];
    
    int requiredPoints = 50 ;
    
    if (requiredPoints > [[CurrentUser getObject] getUser].credit){
        [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPointsTab"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    }
    else{
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *date = [dateFormat stringFromDate:[NSDate date] ];
        
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        @try {
            if ([RestServiceAgent internetAvailable]) {
                [[WallManager getInstance] rePost:[selectedPost postId] userId:[[CurrentUser getObject] getUser].userId date:date and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if([[currentClient objectForKey:@"status"] intValue] == 1)
                        {
                            [selectedPost setRePosted:YES];
                            if (sender) {
                                [sender setImage:[UIImage imageNamed:@"repost_sc.png"] forState:UIControlStateNormal];
                            }
                            
                            [[[CurrentUser getObject] getUser] setCredit:[CurrentUser getObject].getUser.credit - requiredPoints];
                            [[CurrentUser getObject] saveCurrentUser];
                            
                            [[[UIAlertView alloc] initWithTitle:nil
                                                        message:[[Language sharedInstance] stringWithKey:@"Home_repostAlert"]
                                                       delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"] otherButtonTitles:nil] show];
                            [horizontalScroll reloadData];
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
}
@end
