//
//  BlockedUsersViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 3/8/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockedUsersViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView  *noBlockedUsersView;
@property (weak, nonatomic) IBOutlet UIView  *noBlockedUsersIcon;
@property (weak, nonatomic) IBOutlet UILabel *noBlockedUsersLabel1;
@property (weak, nonatomic) IBOutlet UILabel *noBlockedUsersLabel2;


- (IBAction)back_Action:(id)sender;
- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

@end
