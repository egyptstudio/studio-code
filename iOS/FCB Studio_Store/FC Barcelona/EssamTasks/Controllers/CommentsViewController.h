//
//  CommentsViewController.h
//  FC Barcelona
//
//  Created by Eissa on 9/9/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
@protocol commentDelegate <NSObject>
@required
- (void) commentAdded;
- (void) commentDeleted;
@end

@interface CommentsViewController : UIViewController<UIScrollViewDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    id <commentDelegate> _delegate;
}
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (weak, nonatomic) IBOutlet UILabel *noCommentsLabel;
@property (weak, nonatomic) IBOutlet UIView *noCommentsView;
@property (weak, nonatomic) IBOutlet UILabel *commentsCountLabel;
@property (nonatomic,strong) id delegate;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *commentText;

@property (weak, nonatomic) IBOutlet UIButton *close;
@property (weak, nonatomic) IBOutlet UIButton *postButton;

- (IBAction)back_Action:(id)sender;
@property (assign) Post* post;
@property (assign) int userID;
@property (assign) NSString* userName;
@property (assign) UIImage* backGround;

- (IBAction)post:(id)sender;

@end
