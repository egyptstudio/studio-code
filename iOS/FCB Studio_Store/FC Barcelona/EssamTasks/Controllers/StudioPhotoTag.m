//
//  StudioPhotoTag.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/11/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "StudioPhotoTag.h"

@implementation StudioPhotoTag
-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:@(self.tagId) forKey:@"tagId"];
    [encoder encodeObject:@(self.tagType) forKey:@"tagType"];
    [encoder encodeObject:self.tagName forKey:@"tagName"];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self.tagId        =  [[decoder decodeObjectForKey:@"tagId"] intValue];
    self.tagType      =  [[decoder decodeObjectForKey:@"tagType"] intValue];
    self.tagName      =  [decoder decodeObjectForKey:@"tagName"];
    return self;
}
@end
