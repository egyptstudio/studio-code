//
//  ExtraItemsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/12/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "ExtraItemsViewController.h"
#import "StudioDetailsViewController.h"
@interface ExtraItemsViewController ()<UITextFieldDelegate>

@end

@implementation ExtraItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CALayer * l = [self.continueButton layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    CALayer * l1 = [self.itemView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:3];
    
    
    _itemImage.image = _editedImage;
    _itemQuantity.delegate = self ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)continueButton_Action:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)back_Action:(id)sender {
//    for (int i = 0 ; i < [self.navigationController.viewControllers count]; i++) {
//        if ([self.navigationController.viewControllers[i] isKindOfClass:[StudioDetailsViewController class]]) {
//            [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
//        }
//    }
    [self.navigationController popToRootViewControllerAnimated:YES];

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
  [self.view endEditing:YES];
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
@end
