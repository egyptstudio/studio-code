//
//  StudioPhotoTag.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/11/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StudioPhotoTag : NSObject
@property int tagId;
@property NSString *tagName;
@property  int tagType;
@end
