//
//  MenuViewController.h
//  FC Barcelona
//
//  Created by Eissa on 10/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImageView.h"
@interface MenuViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

- (IBAction)closeMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *completionView;
@property (weak, nonatomic) IBOutlet UILabel *completionPercentage;
@property (weak, nonatomic) IBOutlet UILabel *completionTitle;
@property (weak, nonatomic) IBOutlet UIProgressView *completionProgress;

- (IBAction)settings_Action:(id)sender;



@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIView *userImageView;

@property (weak, nonatomic) IBOutlet UIView *followingView;
@property (weak, nonatomic) IBOutlet UIView *followerView;
@property (weak, nonatomic) IBOutlet UILabel *following;
@property (weak, nonatomic) IBOutlet UILabel *follower;
@property (weak, nonatomic) IBOutlet UILabel *followingCount;
@property (weak, nonatomic) IBOutlet UILabel *followerCount;
@property (weak, nonatomic) IBOutlet UILabel *xps;

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userCountry;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;


@property (weak, nonatomic) IBOutlet UIView *fcbView;
@property (weak, nonatomic) IBOutlet UIView *fansView;
@property (weak, nonatomic) IBOutlet UIView *contestView;
@property (weak, nonatomic) IBOutlet UIView *friendsView;
@property (weak, nonatomic) IBOutlet UIView *sponsorsView;

@property (weak, nonatomic) IBOutlet UILabel *fcbLabel;
@property (weak, nonatomic) IBOutlet UILabel *fansLabel;
@property (weak, nonatomic) IBOutlet UILabel *contestLabel;
@property (weak, nonatomic) IBOutlet UILabel *friendsLabel;
@property (weak, nonatomic) IBOutlet UILabel *sponsorsLabel;

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UIView *bigView;


@property (weak, nonatomic) IBOutlet UILabel *notificationNumber;

- (IBAction)openHome:(id)sender;
- (IBAction)openFans:(id)sender;
- (IBAction)openContest:(id)sender;
- (IBAction)openFriends:(id)sender;
- (IBAction)openSponsers:(id)sender;
- (IBAction)info_Action:(id)sender;

- (IBAction)openProfile:(id)sender;


- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;
@end
