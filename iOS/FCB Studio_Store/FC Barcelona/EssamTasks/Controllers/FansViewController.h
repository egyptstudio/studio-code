//
//  FansViewController.h
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/3/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface FansViewController : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *tabImage;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet UITextField *searchText;
@property (weak, nonatomic) IBOutlet UITextView *searchTextView;

@property (weak, nonatomic) IBOutlet UIButton *applyButton;
- (IBAction)applyButton_Action:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *nearButton;
@property (weak, nonatomic) IBOutlet UIButton *latestButton;
@property (weak, nonatomic) IBOutlet UIButton *mostButton;
@property (weak, nonatomic) IBOutlet UIButton *onlineButton;
@property (weak, nonatomic) IBOutlet UIButton *offlineButton;
@property (weak, nonatomic) IBOutlet UIButton *maleButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleButton;

@property (weak, nonatomic) IBOutlet UIView *citiesView;
@property (weak, nonatomic) IBOutlet UITableView *citiesTableView;

- (IBAction)nearButton_Action:(id)sender;
- (IBAction)latestButton_Action:(id)sender;
- (IBAction)mostButton_Action:(id)sender;
- (IBAction)onlineButton_Action:(id)sender;
- (IBAction)offlineButton_Action:(id)sender;
- (IBAction)maleButton_Action:(id)sender;
- (IBAction)femaleButton_Action:(id)sender;




- (IBAction)back_Action:(id)sender;
- (IBAction)fans_Action:(id)sender;
- (IBAction)premuim_Action:(id)sender;
- (IBAction)filter_Action:(id)sender;
- (IBAction)closePopup:(id)sender;
- (IBAction)closeCitiesPopup:(id)sender;
- (IBAction)selectCountry_Action:(id)sender;

@end
