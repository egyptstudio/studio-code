//
//  FreePointsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 6/13/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "FreePointsViewController.h"
#import "CurrentUser.h"
#import "StudioManager.h"
#import "RestServiceAgent.h"
#import "UsersManager.h"
#import "SVProgressHUD.h"
#import "FreePointsDetailsViewController.h"
#import "EditProfileViewController.h"
#import "InviteFriendsView.h"
#import "GetAndPushCashedData.h"


@interface FreePointsViewController ()

@end

@implementation FreePointsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    for (UIView *view in _mainScroll.subviews) {
        [_mainScroll setContentSize:CGSizeMake(0, _mainScroll.contentSize.height + view.frame.size.height+5)];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [(CALayer*)[_view1 layer] setCornerRadius:3];
    [(CALayer*)[_view2 layer] setCornerRadius:3];
    [(CALayer*)[_view3 layer] setCornerRadius:3];
    [(CALayer*)[_view4 layer] setCornerRadius:3];
    [(CALayer*)[_view5 layer] setCornerRadius:3];
    [(CALayer*)[_view6 layer] setCornerRadius:3];


    [(CALayer*)[_button1 layer] setCornerRadius:3];
    [(CALayer*)[_button2 layer] setCornerRadius:3];
    [(CALayer*)[_button3 layer] setCornerRadius:3];
    [(CALayer*)[_button4 layer] setCornerRadius:3];
    [(CALayer*)[_button5 layer] setCornerRadius:3];

    [self localise];
    
}
-(void)localise{
    
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"FreePoints"]];

    [_title0 setText:[[Language sharedInstance] stringWithKey:@"FP_Do_more_activities"]];
    [_title1 setText:[[Language sharedInstance] stringWithKey:@"FP_Complete_profile"]];
    [_title2 setText:[[Language sharedInstance] stringWithKey:@"FP_Invite"]];
    [_title3 setText:[[Language sharedInstance] stringWithKey:@"FP_Public_social"]];
    [_title4 setText:[[Language sharedInstance] stringWithKey:@"FP_Share_Images"]];
    [_title5 setText:[[Language sharedInstance] stringWithKey:@"FP_Daily"]];
    [_title6 setText:[[Language sharedInstance] stringWithKey:@"FP_Purchased"]];
    
    [_desc1 setText:[[Language sharedInstance] stringWithKey:@"FP_One_time"]];
    [_desc2 setText:[[Language sharedInstance] stringWithKey:@"FP_Friend"]];
    [_desc3 setText:[[Language sharedInstance] stringWithKey:@"FP_Post"]];
    [_desc4 setText:[[Language sharedInstance] stringWithKey:@"FP_Share"]];
    [_desc5 setText:[[Language sharedInstance] stringWithKey:@"FP_Day"]];
    
    
    [_button1 setTitle:[[Language sharedInstance] stringWithKey:@"FP_GO"] forState:UIControlStateNormal];
    [_button2 setTitle:[[Language sharedInstance] stringWithKey:@"FP_GO"] forState:UIControlStateNormal];
    [_button3 setTitle:[[Language sharedInstance] stringWithKey:@"FP_Show"] forState:UIControlStateNormal];
    [_button4 setTitle:[[Language sharedInstance] stringWithKey:@"FP_Show"] forState:UIControlStateNormal];
    [_button5 setTitle:[[Language sharedInstance] stringWithKey:@"FP_Check"] forState:UIControlStateNormal];
}

- (IBAction)button1_Action:(id)sender{
    EditProfileViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    view.fromPoints = YES;
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)button2_Action:(id)sender{
    UIStoryboard *sb =  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    InviteFriendsView* invite = [sb instantiateViewControllerWithIdentifier:@"InviteFriendsView"];
    [self.navigationController pushViewController:invite animated:YES];
}

- (IBAction)button3_Action:(id)sender{
    UIImageView* image = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    image.image = [UIImage imageNamed:@"posting.jpg"];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissImage:)];
    [tap setNumberOfTapsRequired:1];
    [image setUserInteractionEnabled:YES];
    [image addGestureRecognizer:tap];
    
    [self.view addSubview:image];
}

- (IBAction)button4_Action:(id)sender{
    UIImageView* image = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    image.image = [UIImage imageNamed:@"sharing.jpg"];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissImage:)];
    [tap setNumberOfTapsRequired:1];
    [image setUserInteractionEnabled:YES];
    [image addGestureRecognizer:tap];
    
    [self.view addSubview:image];
}

-(void)dismissImage:(UITapGestureRecognizer*)tap{
    [tap.view removeFromSuperview];
}
- (IBAction)button5_Action:(id)sender{
    [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPurchasedTab"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
}

- (IBAction)back_Action:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
