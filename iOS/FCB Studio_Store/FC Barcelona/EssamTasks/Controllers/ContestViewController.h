//
//  ContestViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

typedef enum {
    prizesTap = 1,
    rulesTap,
    winnersTap,
} ContestTapType;


@interface ContestViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segments;
@property (weak, nonatomic) IBOutlet UILabel *noContestsLabel;
@property (weak, nonatomic) IBOutlet UIView *noContestsView;
@property (weak, nonatomic) IBOutlet UIButton *top10Button;
@property (weak, nonatomic) IBOutlet UIScrollView *segmentsView;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

- (IBAction)openStudio:(id)sender;
- (IBAction)segmentChanged:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;
- (IBAction)top10_Action:(id)sender;

@end
