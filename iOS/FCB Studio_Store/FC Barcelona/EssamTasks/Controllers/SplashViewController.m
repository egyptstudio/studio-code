//
//  SplashViewController.m
//  FC Barcelona
//
//  Created by Eissa on 10/7/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "SplashViewController.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "BarcelonaAppDelegate.h"
#import "CNavigationController.h"
#import "MainWallViewController.h"
@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    CALayer *l = [_dialog layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10];
    
    CALayer *l1 = [_dialogButton layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    
    [self hideUpdateDialog];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self getBaseURL];
}

-(void)getBaseURL
{
    @try {
            if([RestServiceAgent internetAvailable])
            {
                [[UsersManager getInstance] getBaseURL:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            if([currentClient[@"status"] intValue] == 2)
                            {
                                [(BarcelonaAppDelegate*) [[UIApplication sharedApplication] delegate] setBaseURL:currentClient[@"data"][@"baseURL"]];
                                switch ([currentClient[@"data"][@"updateStatus"] intValue]) {
                                    case 1:
                                        [self gotoMain];
                                        break;
                                    case 2:
                                        [self showUpdateDialogWithCanClose:YES];
                                        break;
                                    case 3:
                                        [self showUpdateDialogWithCanClose:NO];
                                        break;
                                    default:
                                        break;
                                }
                            }
                            else{
                                [self gotoMain];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    [self gotoMain];
                }];
            }
            else
            {
                [self gotoMain];
            }
    }
    @catch (NSException *exception) {
        [self gotoMain];
    }
    
}
-(void)gotoMain
{
    [self performSegueWithIdentifier:@"mainSegue" sender:self];
//    CNavigationController* navController = [self.storyboard instantiateViewControllerWithIdentifier:@"CNavigationController"];
//    [navController setNavigationBarHidden:YES];
//    [[(BarcelonaAppDelegate *)[[UIApplication sharedApplication] delegate] window] setRootViewController:navController];
//    [[(BarcelonaAppDelegate *)[[UIApplication sharedApplication] delegate] window] makeKeyAndVisible];
}
-(void)showUpdateDialogWithCanClose:(BOOL)canClose
{
    [_dialog setHidden:NO];
    [_dialogCloseButton setHidden:!canClose];
    [_transperentView setHidden:NO];
}
-(void)hideUpdateDialog
{
    [_dialog setHidden:YES];
    [_dialogCloseButton setHidden:YES];
    [_transperentView setHidden:YES];
}

- (IBAction)update_Aciton:(id)sender
{
    NSString *iTunesLink = @"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=com.tawasol.FC-Barcelona&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}
- (IBAction)close_Aciton:(id)sender
{
    [self gotoMain];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
