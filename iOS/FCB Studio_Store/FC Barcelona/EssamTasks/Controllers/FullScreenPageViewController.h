//
//  FullScreenPageViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/28/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImageView.h"
#import "Post.h"
@interface FullScreenPageViewController : UIViewController
- (IBAction)close_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *likesNumber;
@property (weak, nonatomic) IBOutlet AvatarImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *postPhoto;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
- (IBAction)likeButton_Action:(id)sender;
- (IBAction)info_Action:(id)sender;

-(void)viewPost:(Post*)post;
@end
