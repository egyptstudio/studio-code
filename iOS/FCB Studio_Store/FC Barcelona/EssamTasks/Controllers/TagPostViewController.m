//
//  TagPostViewController.m
//  FC Barcelona
//
//  Created by Saad Ansari on 2/9/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "TagPostViewController.h"
#import "WebImage.h"
#import "fansManager.h"
#import "WallManager.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "CommentsViewController.h"
#import "UserPhoto.h"
#import "PostDetailsViewController.h"
#import "ShareManagerViewController.h"
#import "User.h"
#import "studioManager.h"
#import "CaptureViewController.h"
#import "ShareViewController.h"
#import "ShareLibrary.h"
#import "CurrentUser.h"
#import "ImageToZoomViewController.h"
#import "FullScreenViewController.h"
#import "LikesViewController.h"
#import "ImageDetailViewController.h"
#import "TagPostCell.h"
@interface TagPostViewController ()
{
    int postsNo;
    NSString *lastRequestTime;
    NSString *timeFilter;
    NSMutableArray *_allPosts;
}
@end

@implementation TagPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    postsNo = 0;
    timeFilter = @"1";
    _allPosts = [NSMutableArray new];
    lastRequestTime = [_allPosts count]==0 ?@"" : [defaults objectForKey:@"currentRequestTime"];
    [self getUserPosts];
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.titleLabel.text = self.tagName;

}

-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*{
 lastRequestTime = "";
 n = 10;
 tagId = 573;
 timeFilter = 1;
 userId = 711;
 }*/
-(void)getUserPosts
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([[CurrentUser getObject] isUser]) {
            if ([RestServiceAgent internetAvailable]) {
                postsNo+=10;
                [[WallManager getInstance] getTagPosts:[[CurrentUser getObject] getUser].userId tagId:self.tagName postsNo:postsNo lastRequestTime:lastRequestTime timeFilter:timeFilter and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                            if ([currentClient count]>0)
                            {
                                //NSLog(@"currentClient %@",currentClient);
                                if ([_allPosts count]==1)
                                {
                                    //[currentClient removeObjectAtIndex:0];
                                    [_allPosts addObjectsFromArray:currentClient] ;
                                    [self.tableView reloadData];
                                    //[self removeGestureRecognizer:swipe];
                                    
                                    @try
                                    {
//                                        [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]  atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
                                    }
                                    @catch (NSException *exception) {
                                        
                                    }
                                    
                                    
                                }
                                else
                                {
                                    [_allPosts addObjectsFromArray:currentClient] ;
                                    [self.tableView reloadData];
                                }
                            }
                        
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                        postsNo--;
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_allPosts count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 326;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier=@"TagPostCell";
    TagPostCell *cell=[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //cell = [topLevelObjects objectAtIndex:0];
        Post *currentPost = (Post*)[_allPosts objectAtIndex:indexPath.row];
        cell.isEditable =  NO ;
        [cell initWithPost:currentPost andController:self];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setTag:indexPath.row];
        [cell setAllPosts:_allPosts];
        [cell setWalltype:0];
        [cell setLastRequestTime:lastRequestTime];
        [cell setTimeFilter:timeFilter];
//        [cell setCountriesArray:countriesArray];
//        [cell setFavoritePostsFlag:favoritePostsFlag];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [(TagPostCell*)cell performSelectorInBackground:@selector(initImages) withObject:nil];
}



-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
