//
//  LikesViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/14/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "LikesViewController.h"
#import "WallManager.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "AvatarImageView.h"
#import "WebImage.h"
#import "SVProgressHUD.h"
#import "CurrentUser.h"
#import "AvatarImageView.h"
#import "PostLike.h"
#import "FansManager.h"
#import "GetAndPushCashedData.h"
@interface LikesViewController ()

@end

@implementation LikesViewController
{
    NSMutableArray * likes;

}
@synthesize currentPost;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    likes = [[NSMutableArray alloc] init];
}

-(void)viewWillAppear:(BOOL)animated{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"notifications_likes"]];
    _likesCount.text = [NSString stringWithFormat:@"%d Likes",[currentPost likeCount]];
    [self getLikeDetails];
}

-(void)getLikeDetails{

    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance] getLikeDetails:[[CurrentUser getObject] getUser].userId postId:[currentPost postId] and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([currentClient count]>0) {
                        [likes  addObjectsFromArray:currentClient] ;
                        [_tableView reloadData];
                    }
                    [SVProgressHUD dismiss];
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [likes addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:[NSString stringWithFormat:@"Likes_%i",[currentPost postId]]]];
                
                if ([likes count] > 0) {
                    [_tableView reloadData];
                }
                
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }

}
-(void)prepareAndCacheLikes{
    [_tableView reloadData];
    _likesCount.text = [NSString stringWithFormat:@"%lu Likes",(unsigned long)[likes count]];
    [GetAndPushCashedData cashObject:likes withAction:[NSString stringWithFormat:@"Likes_%i",[currentPost postId]]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [likes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    AvatarImageView *userImage = (AvatarImageView*)[cell viewWithTag:1];
    userImage.borderWidth = 2 ;
    userImage.borderColor = [UIColor whiteColor];
    userImage.image = [UIImage imageNamed:@"avatar.png"];

    [userImage sd_setImageWithURL:[NSURL URLWithString:[(PostLike*)[likes objectAtIndex:indexPath.row] profilePicURL]] placeholderImage:[UIImage imageNamed:@"avatar"]];
    
    UILabel *userName = (UILabel*)[cell viewWithTag:2];
    userName.text = [(PostLike*)[likes objectAtIndex:indexPath.row] fullName] ;
    
    
    UIImage *state = [(PostLike*)[likes objectAtIndex:indexPath.row] isFollowing] ? [UIImage imageNamed:@"followed.png" ]:[UIImage imageNamed:@"unfollowed.png"];
    UIButton *followUser = (UIButton*)[cell viewWithTag:3];
    [followUser setBackgroundImage:state forState:UIControlStateNormal];
    if (![(PostLike*)[likes objectAtIndex:indexPath.row] isFollowing]) {
        [followUser addTarget:self action:@selector(followUser:) forControlEvents:UIControlEventTouchUpInside];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)followUser:(UIButton*)sender{
    if ([[CurrentUser getObject] isUser]) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        if ([(PostLike*)[likes objectAtIndex:indexPath.row] isFollowing]) {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[FansManager getInstance] unFollowFan:[[CurrentUser getObject] getUser].userId
                                                     fanId:[(PostLike*)[likes objectAtIndex:indexPath.row] userId]
                           and:^(id currentClient) {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [SVProgressHUD dismiss];
                                   if (currentClient) {
                                       if([[currentClient objectForKey:@"status"] intValue] == 1)
                                       {
                                           [[[CurrentUser getObject] getUser] setFollowingCount:[CurrentUser getObject].getUser.followingCount + 1];
                                           [[CurrentUser getObject] saveCurrentUser];
                                           
                                           [[likes objectAtIndex:indexPath.row] setIsFollowing:NO];
                                           [sender setBackgroundImage:[UIImage imageNamed:@"unfollowed.png" ] forState:UIControlStateNormal];
                                       }
                                   }
                               });
                           }onFailure:^(NSError *error) {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [SVProgressHUD dismiss];
                                   UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                   [alert show];
                               });
                           }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
        else
        {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[FansManager getInstance] followFan:[[CurrentUser getObject] getUser].userId
                                                   fanId:[(PostLike*)[likes objectAtIndex:indexPath.row] userId]
                                 and:^(id currentClient) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [SVProgressHUD dismiss];
                                         if (currentClient) {
                                             if([[currentClient objectForKey:@"status"] intValue] == 1)
                                             {
                                                 User *tempUser=[[CurrentUser getObject] getUser];
                                                 tempUser.followingCount=tempUser.followingCount+1;
                                                 [[CurrentUser getObject] setUser:tempUser];
                                                 [[CurrentUser getObject] saveCurrentUser];
                                                 [[likes objectAtIndex:indexPath.row] setIsFollowing:YES];
                                                 
                                                 [sender setBackgroundImage:[UIImage imageNamed:@"followed.png" ] forState:UIControlStateNormal];
                                             }
                                         }
                                     });
                                 }onFailure:^(NSError *error) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [SVProgressHUD dismiss];
                                         
                                         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                         [alert show];
                                     });
                                 }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
