//
//  ContestViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "ContestViewController.h"
#import "ContestManager.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "WebImage.h"
#import "AvatarImageView.h"
#import "SKBounceAnimation.h"
#import "CurrentUser.h"
#import "GetAndPushCashedData.h"
#import "WebImage.h"
#import "FansDetailView.h"

@interface ContestViewController ()<UIWebViewDelegate>

@end

@implementation ContestViewController
{
    NSDictionary* contest;
    NSMutableArray *prizes;
    NSMutableArray *winners;
    NSString *contestRules;
    ContestTapType tap;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setOpaque:NO];
    
    //////////////////////
    [_segmentsView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:84/255.0f blue:164/255.0f alpha:1]];
    [_segments setFrame:_segmentsView.bounds];
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    UIFont *fontBold = [UIFont boldSystemFontOfSize:14.0f];
    
    UIColor *color = [UIColor whiteColor];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesBold = [NSDictionary dictionaryWithObjectsAndKeys:
                                    fontBold,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    [_segments setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [_segments setTitleTextAttributes:attributesBold forState:UIControlStateSelected];
    
    
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_normal.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"]
                         forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    UIView *devider = [[UIView alloc] initWithFrame:CGRectMake(0, 0, .5, _segments.frame.size.height)];
    [devider setBackgroundColor:[UIColor whiteColor]];
    UIGraphicsBeginImageContextWithOptions(devider.bounds.size, devider.opaque, 0.0);
    [devider.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * deviderImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateSelected
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateNormal
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    //////////////////
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"menu_Contest"]];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Contest_Prizes"] forSegmentAtIndex:0];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Contest_Rules"] forSegmentAtIndex:1];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"contest_winners"]   forSegmentAtIndex:2];
    [_segments setSelectedSegmentIndex:0];
    
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 65)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    _tableView.tableFooterView = footerView;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    tap = prizesTap;
}

-(void)viewWillAppear:(BOOL)animated{
    self.screenName = @"Contest Screen";

    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_top10Button setTitle:[[Language sharedInstance] stringWithKey:@"top_ten_photos"] forState:UIControlStateNormal];
    
     [self hideTop10Dialog];
    
    [self getContests];
}
-(void)getContests
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[ContestManager getInstance]getContests:[[CurrentUser getObject] getUser].userId and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient[@"status"] intValue]==2) {
                        [self prepareAndCacheContest:currentClient];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [self showNoContestsViewWithMessage:error.domain andShowTop10:NO];
                    [_segments setEnabled:NO];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

-(void)prepareAndCacheContest:(id)response{
    contest = [[NSDictionary alloc] initWithDictionary:response[@"data"][@"Contest"]];
    prizes  = [[NSMutableArray alloc] initWithArray:response[@"data"][@"ContestPrize"]];
    winners = [[NSMutableArray alloc] initWithArray:response[@"data"][@"ContestWinner"]];
    contestRules = response[@"data"][@"contestRules"];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:contestRules]]];
    [self segmentChanged:_segments];
    [GetAndPushCashedData cashObject:response withAction:@"Contest"];
}

-(void)prepareFromCache:(id)response{
    contest = [[NSDictionary alloc] initWithDictionary:response[@"data"][@"Contest"]];
    prizes  = [[NSMutableArray alloc] initWithArray:response[@"data"][@"ContestPrize"]];
    winners = [[NSMutableArray alloc] initWithArray:response[@"data"][@"ContestWinner"]];
    contestRules = response[@"data"][@"contestRules"];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:contestRules]]];
    [self segmentChanged:_segments];
}

- (IBAction)segmentChanged:(id)sender {
    switch (_segments.selectedSegmentIndex) {
        case 0:
            [self preparePrizesTab];
            break;
        case 1:
            [self prepareRulesTab];
            break;
        case 2:
            [self prepareWinnersTab];
            break;
        default:
            break;
    }
}
-(void)preparePrizesTab{
    tap = prizesTap;
    [self hideTop10Dialog];
    [self hideNoContestsView];
    [_tableView setHidden:NO];
    [_webView setHidden:YES];
    [_tableView reloadData];
}
-(void)prepareWinnersTab{
    tap = winnersTap;
    [self showTop10Dialog];
    [self hideNoContestsView];

    [_tableView setHidden:NO];
    [_webView setHidden:YES];
    
    if ([contest[@"status"] intValue] != 4) {
        [_tableView setHidden:YES];
        [self hideTop10Dialog];
        [self showNoContestsViewWithMessage:[[Language sharedInstance] stringWithKey:@"Contest_Winners_running_contest"] andShowTop10:YES];
    }
    else{
        [_tableView setHidden:NO];
        [_tableView reloadData];
    }
}
-(void)prepareRulesTab{
    [[[UIAlertView alloc] initWithTitle:[[Language sharedInstance] stringWithKey:@"Attention"] message:[[Language sharedInstance] stringWithKey:@"AppleAttention"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles:nil] show];
    tap = rulesTap;
    [self hideTop10Dialog];
    [self hideNoContestsView];

    [_tableView setHidden:YES];
    [_webView setHidden:NO];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numOfRows ;
    switch (tap) {
        case prizesTap:
            numOfRows = [prizes count];
            break;
        case winnersTap:
            numOfRows = [winners count];
            break;
        default:
            numOfRows = 0;
            break;
    }
    return numOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    switch (tap) {
        case prizesTap:
            cell = [self cellForPrizesAtIndexPath:indexPath];
            break;
        case winnersTap:
            cell = [self cellForWinnersAtIndexPath:indexPath];
            break;
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(UITableViewCell *)cellForPrizesAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* prize = [[NSDictionary alloc] initWithDictionary:prizes[indexPath.row]];
    UITableViewCell *cell  = [_tableView dequeueReusableCellWithIdentifier:@"prizesCell"];
    UIImageView* mainImage      = (UIImageView*)[cell viewWithTag:2];
    UILabel* prizeName          = (UILabel*)[cell viewWithTag:1];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:3] ;
    
    prizeName.text = prize[@"prizeText"];
    

    [mainImage sd_setImageWithURL:[NSURL URLWithString:prize[@"prizeImageUrl"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int cellHeight;
    switch (tap) {
        case prizesTap:
        {
            cellHeight = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)? 300 : 175;
            break;
        }
        case winnersTap:
        {
            cellHeight = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)? 200 : 100;
            break;
        }
        default:
            break;
    }
    return cellHeight;
}
- (CGFloat)tableView:(UITableView *)tableView widthForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.frame.size.width;
}
-(UITableViewCell *)cellForWinnersAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* winner = [[NSDictionary alloc] initWithDictionary:winners[indexPath.row]];
    NSDictionary* prize ;
    
    for (NSDictionary* dic in prizes) {
        if (dic[@"winnerId"] == winner[@"winnerId"] ) {
            prize = [[NSDictionary alloc] initWithDictionary:dic];
            break;
        }
    }
    
    UITableViewCell *cell  = [_tableView dequeueReusableCellWithIdentifier:@"winnersCell"];
    UIView *leftView       = (UIView*)[cell viewWithTag:1];
    UIView *rightView      = (UIView*)[cell viewWithTag:2];
    UILabel* prizeRank     = (UILabel*)[cell viewWithTag:5];
    UIImageView* mainImage      = (UIImageView*)[cell viewWithTag:3];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:4] ;
    
    CALayer * l = [leftView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    [l setBorderWidth:1];
    [l setBorderColor:[winner[@"isFollowed"] boolValue]?[UIColor greenColor].CGColor:[UIColor redColor].CGColor];

    CALayer * l1 = [rightView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    [l1 setBorderWidth:1];
    [l1 setBorderColor:[UIColor whiteColor].CGColor];

    prizeRank.text = [NSString stringWithFormat:@"%i",[prize[@"prizeRank"] intValue]];

    
    [mainImage sd_setImageWithURL:[NSURL URLWithString:prize[@"prizeImageUrl"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    ////////////////////////////////////////////////////////////////////////////////////
    
    UIImageView* winnerImage      = (UIImageView*)[cell viewWithTag:10];
    UILabel* winnerName     = (UILabel*)[cell viewWithTag:11];
    UIImageView* isFavorate      = (UIImageView*)[cell viewWithTag:12];
    UIImageView* isFollowed     = (UIImageView*)[cell viewWithTag:13];
    UILabel* numberOfPhotos     = (UILabel*)[cell viewWithTag:14];
    UIActivityIndicatorView *loader2 = (UIActivityIndicatorView* )[cell viewWithTag:15] ;

    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoDetailWithFan:)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    [leftView addGestureRecognizer:tap1];
    leftView.userInteractionEnabled = YES;
    
    [winnerImage sd_setImageWithURL:[NSURL URLWithString:winner[@"profilePicUrl"]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader2 stopAnimating];
        [loader2 setHidden:YES];
    }];
    
    
    winnerName.text = winner[@"winnerName"];
    isFavorate.image =  [winner[@"isFavorite"] boolValue] ? [UIImage imageNamed:@"favourit.png"] : [UIImage imageNamed:@"unfavourit.png"];
    isFollowed.image =  [winner[@"isFollowed"] boolValue] ? [UIImage imageNamed:@"followed.png"] : [UIImage imageNamed:@"unfollowed.png"];
    numberOfPhotos.text = [NSString stringWithFormat:@"%i",[winner[@"noOfPics"] intValue]];

    return cell;
}
- (void)gotoDetailWithFan:(UITapGestureRecognizer*)sender
{
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

    UIStoryboard *sb =  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ?
    [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] :
    [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
    fansDetailView.fanID = [winners[indexPath.row][@"winnerId"] intValue];
    [self.navigationController pushViewController:fansDetailView animated:YES];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"headerCell"];
    UILabel *headerTitle = (UILabel*)[cell viewWithTag:1];
    headerTitle.text = contest[@"name"];
    headerTitle.text = [headerTitle.text stringByAppendingString:[[Language sharedInstance] stringWithKey:@"ContestTest"]];
    return cell.contentView;
}
-(void)showTop10Dialog{
    
    UIView *dialogView = [[UIView alloc] initWithFrame:CGRectMake(_tableView.frame.origin.x,
                                                                  _tableView.frame.origin.y,
                                                                  _tableView.frame.size.width,50)];
    [dialogView setTag:101];
    dialogView.backgroundColor = [UIColor whiteColor];
    CALayer *l = [dialogView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10];
    
    UILabel *dialogTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10,160, 30)];
    dialogTitle.textAlignment = NSTextAlignmentLeft;
    dialogTitle.font = [UIFont systemFontOfSize:14];
    dialogTitle.textColor = [UIColor colorWithRed:0.0980392 green:0.403922 blue:0.909804 alpha:1];
    dialogTitle.text = [[Language sharedInstance] stringWithKey:@"check_winners_photos"];
    [dialogView addSubview:dialogTitle];
    
    
    UIButton *viewTop10 = [[UIButton alloc] initWithFrame:CGRectMake(170,10,130,30)];
    dialogTitle.textAlignment = NSTextAlignmentLeft;
    [viewTop10.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [viewTop10 setTitle:[[Language sharedInstance] stringWithKey:@"top_ten_photos"] forState:UIControlStateNormal];
    [viewTop10 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [viewTop10 setBackgroundColor:[UIColor colorWithRed:0.0980392 green:0.403922 blue:0.909804 alpha:1]];
    CALayer *l3 = [viewTop10 layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:5];
    [viewTop10 addTarget:self action:@selector(top10_Action:) forControlEvents:UIControlEventTouchUpInside];
    [dialogView addSubview:viewTop10];
    
    
    
    _tableView.frame = CGRectMake(_tableView.frame.origin.x,
                                      _tableView.frame.origin.y+dialogView.frame.size.height+5,
                                      _tableView.frame.size.width,
                                      _tableView.frame.size.height-dialogView.frame.size.height-5);
    
    [self.view insertSubview:dialogView belowSubview:_tableView];
}
-(void)hideTop10Dialog{
    
    for (UIView* view in self.view.subviews) {
        if (view.tag == 101) {
            _tableView.frame = _webView.frame;
            [_tableView.subviews[0] setFrame:_tableView.bounds];
            [view removeFromSuperview];
        }
    }
}

-(void)showNoContestsViewWithMessage:(NSString*)msg andShowTop10:(BOOL)state{
        [self.noContestsLabel setText:msg];
        [self.noContestsView setHidden:NO];
        [self.top10Button setHidden:!state];
}
-(void)hideNoContestsView{
    [self.noContestsView setHidden:YES];
    [self.top10Button setHidden:YES];
}

- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}

- (IBAction)top10_Action:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openTop10" object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [webView.scrollView setContentSize:CGSizeMake(webView.scrollView.contentSize.width,
                                                  webView.scrollView.contentSize.height+65)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}


@end
