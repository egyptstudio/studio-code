//
//  MenuViewController.m
//  FC Barcelona
//
//  Created by Eissa on 10/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "MenuViewController.h"
#import "SKBounceAnimation.h"
#import "StudioV2ViewController.h"
#import "FansViewController.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "FansManager.h"
#import "User.h"
#import "Fan.h"
#import "WebImage.h"
#import "KxMenu.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "UsersManager.h"
#import "CurrentUser.h"
#import "SettingsViewController.h"
#import "EditProfileViewController.h"
#import "FansCollectionView.h"
#import "NotificationViewController.h"
#import "GetAndPushCashedData.h"
#import "SystemMessage.h"
#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface MenuViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *dummyImage;

@end

@implementation MenuViewController
{
    User * currentUser;
    Fan * currentFan;
    UIImagePickerController* cameraPicker;
    NSUserDefaults * defaults;
    UIDynamicAnimator* animator;
    UIGravityBehavior *gravityBehaviour;
    UIPushBehavior *pushBehavior;
    UICollisionBehavior *collision;
    UIPanGestureRecognizer * dragGesture;
    UIAttachmentBehavior *panAttachmentBehaviour;
    

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)logoutAction:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logOut" object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self closeMenu:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CALayer * l0 = [self.followerView layer];
    [l0 setMasksToBounds:YES];
    [l0 setCornerRadius:3];
    
    [self.notificationNumber setHidden:YES];
    CALayer * lll = [self.notificationNumber layer];
    [lll setMasksToBounds:YES];
    [lll setCornerRadius:10];
    
    CALayer * l1 = [self.followingView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:3];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animationStopped) name:@"animationStopped" object:nil];

    CALayer * l3 = [_userImageView layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:5];
    [l3 setBorderWidth:2];
    [l3 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
    
    UITapGestureRecognizer *profileTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProfile:)];
    profileTap.cancelsTouchesInView = YES;
    profileTap.numberOfTapsRequired = 1;
    [_bigView addGestureRecognizer:profileTap];
    _bigView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *fcbTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openHome:)];
    fcbTap.cancelsTouchesInView = YES;
    fcbTap.numberOfTapsRequired = 1;
    [self.fcbView addGestureRecognizer:fcbTap];
    self.fcbView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *fansTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFans:)];
    fansTap.cancelsTouchesInView = YES;
    fansTap.numberOfTapsRequired = 1;
    [self.fansView addGestureRecognizer:fansTap];
    self.fansView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *contestTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openContest:)];
    contestTap.cancelsTouchesInView = YES;
    contestTap.numberOfTapsRequired = 1;
    [self.contestView addGestureRecognizer:contestTap];
    self.contestView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *friendsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFriends:)];
    friendsTap.cancelsTouchesInView = YES;
    friendsTap.numberOfTapsRequired = 1;
    [self.friendsView addGestureRecognizer:friendsTap];
    self.friendsView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *sponsorsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSponsers:)];
    sponsorsTap.cancelsTouchesInView = YES;
    sponsorsTap.numberOfTapsRequired = 1;
    [self.sponsorsView addGestureRecognizer:sponsorsTap];
    self.sponsorsView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *followingTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(following_Action:)];
    followingTap.cancelsTouchesInView = YES;
    followingTap.numberOfTapsRequired = 1;
    [_followingView addGestureRecognizer:followingTap];
    _followingView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *followerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followers_Action:)];
    followerTap.cancelsTouchesInView = YES;
    followerTap.numberOfTapsRequired = 1;
    [_followerView addGestureRecognizer:followerTap];
    _followerView.userInteractionEnabled = YES;
    
    [self setLocalisables];
}

-(void)viewWillAppear:(BOOL)animated
{
  
    [self checkForNotifications];

    
    @try {
        NSLog(@"%@",[[[CurrentUser getObject] getUser] profilePic]);

        [_userImage sd_setImageWithURL:[NSURL URLWithString:[[[CurrentUser getObject] getUser] profilePic]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [_loader stopAnimating];
            [_loader setHidden:YES];
        }];
        
        if ([[CurrentUser getObject] isUser]) {
            [[CurrentUser getObject] refreshUser];
            _userName.text    = [[[CurrentUser getObject] getUser] fullName];
            [_followerCount setFont:[UIFont fontWithName:@"stencilie" size:22]];
            [_followingCount setFont:[UIFont fontWithName:@"stencilie" size:22]];
            [_xps setFont:[UIFont fontWithName:@"Barcelona2012byDaniloSL" size:20]];
            _followingCount.text =[NSString stringWithFormat:@"%i",[[CurrentUser getObject] getUser].followingCount];
            _followerCount.text =[NSString stringWithFormat:@"%i",[[CurrentUser getObject] getUser].followersCount];
            _xps.text = [NSString stringWithFormat:@"%i XP",[[CurrentUser getObject] getUser].credit];
            _status.text = [[CurrentUser getObject] getUser].status;
            [_completionTitle setText:[[Language sharedInstance] stringWithKey:@"user_profile_completion"]];
            [self prepareProfileCompletion];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

-(void) selectImageSource:(id)sender
{
    
    CGRect frame;
    frame = self.userImage.frame;
    
    [KxMenu showMenuInView:self.view
                  fromRect:frame
                 menuItems:@[
                             [KxMenuItem menuItem:@"From Camera"
                                            image:nil
                                           target:self
                                           action:@selector(takePhoto_Cam:)],
                             [KxMenuItem menuItem:@"From Gallery"
                                            image:nil
                                           target:self
                                           action:@selector(takePhoto_gal:)]
                             ]];
    
}

- (void)takePhoto_Cam:(id)sender
{
    
    
    [(UIView*)[self.navigationController.topViewController.view.subviews objectAtIndex:[self.navigationController.topViewController.view.subviews count]-1 ] setHidden:YES];
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"]
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        cameraPicker.delegate = self;
        cameraPicker.showsCameraControls = YES;
        cameraPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
        [self.view addSubview:cameraPicker.view];
        [self.view bringSubviewToFront:cameraPicker.view];
    }
}

- (void)takePhoto_gal:(id)sender
{
    [(UIView*)[self.navigationController.topViewController.view.subviews objectAtIndex:[self.navigationController.topViewController.view.subviews count]-1 ] setHidden:YES];
    cameraPicker = [[UIImagePickerController alloc] init];
    cameraPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    cameraPicker.delegate = self;
    cameraPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
    [self.view addSubview:cameraPicker.view];
    [self.view bringSubviewToFront:cameraPicker.view];

    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.userImage.image = image;
    [cameraPicker.view removeFromSuperview];
    
    [(UIView*)[self.navigationController.topViewController.view.subviews objectAtIndex:[self.navigationController.topViewController.view.subviews count]-1 ] setHidden:NO];
    [self updateProfile:image];
    
    
}
-(void)updateProfile:(UIImage*)profilePic
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    if ([RestServiceAgent internetAvailable]) {
        @try {
            [[UsersManager getInstance] uploadProfilePic:[[CurrentUser getObject] getUser].userId andImage:profilePic and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if(currentClient)
                    {
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                                        message:@"Profile picture updated successfully"
                                                                       delegate:nil
                                                              cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                        
                        [[CurrentUser getObject] getUser].profilePic = [[currentClient objectForKey:@"data"] objectForKey:@"profilePic"];
                        [[CurrentUser getObject] saveCurrentUser];
                        
                        
                        [_userImage sd_setImageWithURL:[NSURL URLWithString:[[[CurrentUser getObject] getUser] profilePic]] placeholderImage:[UIImage imageNamed:@"avatar"]];
                        
                        
                    }
                    
                });
            } onFailure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                     
                     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                     [alert show];
                 });
             }];
        }
        @catch (NSException *exception)
        {
            NSLog(@"nothing");
        }
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }

}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [(UIView*)[self.navigationController.topViewController.view.subviews objectAtIndex:[self.navigationController.topViewController.view.subviews count]-1 ] setHidden:NO];
    [cameraPicker.view removeFromSuperview];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeMenu:(id)sender {
    [UIView animateWithDuration:0.6f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)openHome:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openHome" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}

- (IBAction)openFans:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openFans" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

- (IBAction)openContest:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openContest" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

- (IBAction)openFriends:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openFriends" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];

    }];
}

- (IBAction)openSponsers:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openSponsers" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];

    }];

}

-(void)hideview
{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
}

- (void)dragMenu:(UIPanGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:sender.view];
    location.x = CGRectGetMidX(self.view.bounds);
    CGPoint velocity = [sender velocityInView:self.view];

    if (sender.state == UIGestureRecognizerStateBegan) {
    }
    else if (sender.state == UIGestureRecognizerStateChanged) {
        if (velocity.y > 0) { // down
            CGPoint newCenter = sender.view.center;
            newCenter.y += [sender translationInView:self.view].y;
            if (newCenter.y + (self.view.frame.size.height/2) <= [UIScreen mainScreen].bounds.size.height) {
                self.view.center = newCenter;
            }
            [dragGesture setTranslation:CGPointZero inView:self.view];
        } else if (velocity.y < 0) { //up
            CGPoint newCenter = sender.view.center;
            newCenter.y += [sender translationInView:self.view].y;
            self.view.center = newCenter;
            [dragGesture setTranslation:CGPointZero inView:self.view];
        }
    
    }
    else if (sender.state == UIGestureRecognizerStateEnded) {

        if (velocity.y > 0) {
            CGFloat yPoints = [UIScreen mainScreen].bounds.size.width;
            NSTimeInterval duration = yPoints / velocity.y;            
            [UIView animateWithDuration:duration animations:^{
                self.view.frame =CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
            } completion:^(BOOL finished) {
            }];
        }
        else {
            [UIView animateWithDuration:0.5f animations:^{
                self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
            } completion:^(BOOL finished) {
                [self willMoveToParentViewController:nil];
                [self.view removeFromSuperview];
                [self removeFromParentViewController];
            }];
        }
        
    }
}

-(void)animationStopped{
    dragGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragMenu:)];
    dragGesture.minimumNumberOfTouches = 1;
    dragGesture.maximumNumberOfTouches = 1;
    [self.view addGestureRecognizer:dragGesture];
}

-(void)prepareProfileCompletion{

    float completion = [[[CurrentUser getObject] getUser] calculateProfileCompletion]/100.0;
    if (completion<1) {
        [_completionPercentage setText:[NSString stringWithFormat:@"%.0f%%",completion*100.0]];
        [_completionProgress setProgress:completion];
        [_completionView setHidden:NO];
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openEditProfile)];
        tap1.cancelsTouchesInView = YES;
        tap1.numberOfTapsRequired = 1;
        [_completionView addGestureRecognizer:tap1];
    }
    else{
        _xps.frame = CGRectMake(CGRectGetMinX(_completionView.frame),
                                _xps.frame.origin.y,
                                CGRectGetMaxX(_xps.frame),
                                _xps.frame.size.height);
    }
}

-(void)openEditProfile{
    EditProfileViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)openProfile:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}

- (IBAction)settings_Action:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openSettings" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

- (IBAction)info_Action:(id)sender{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"openInfo" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

-(void)setLocalisables{
    [_pageTitle     setText:[[Language sharedInstance] stringWithKey:@"app_name"]];
    [_fcbLabel      setText:[[Language sharedInstance] stringWithKey:@"app_name"]];
    [_fansLabel     setText:[[Language sharedInstance] stringWithKey:@"menu_Fans"]];
    [_contestLabel  setText:[[Language sharedInstance] stringWithKey:@"menu_Contest"]];
    [_friendsLabel  setText:[[Language sharedInstance] stringWithKey:@"menu_Friends"]];
    [_sponsorsLabel setText:[[Language sharedInstance] stringWithKey:@"menu_Sponsors"]];
    [_following     setText:[[Language sharedInstance] stringWithKey:@"menu_Following"]];
    [_follower      setText:[[Language sharedInstance] stringWithKey:@"menu_Followers"]];
}

- (IBAction)followers_Action:(id)sender
{
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    FansCollectionView *fansCollectionView = (FansCollectionView *)[sb instantiateViewControllerWithIdentifier:@"FansCollectionView"];
    fansCollectionView.viewTitle = [[Language sharedInstance] stringWithKey:@"menu_Followers"];
    fansCollectionView.viewCount = [_followerCount.text intValue];
    fansCollectionView.fanID = [[CurrentUser getObject] getUser].userId;
    fansCollectionView.isFollowers = YES;
    [self.navigationController pushViewController:fansCollectionView animated:YES];
}

- (IBAction)following_Action:(id)sender
{
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    FansCollectionView *fansCollectionView = (FansCollectionView *)[sb instantiateViewControllerWithIdentifier:@"FansCollectionView"];
    fansCollectionView.viewTitle = [[Language sharedInstance] stringWithKey:@"menu_Following"];
    fansCollectionView.viewCount = [_followingCount.text intValue];
    fansCollectionView.fanID = [[CurrentUser getObject] getUser].userId;
    fansCollectionView.isFollowers = NO;
    [self.navigationController pushViewController:fansCollectionView animated:YES];
}

- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}

- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

- (IBAction)openNotification:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openNotifications" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

-(void)checkForNotifications
{
    NSInteger newNotifications = [[defaults objectForKey:@"NotificationsNumber"] integerValue] + [SystemMessage unseenMessagesNumber] ;
    defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    if (newNotifications > 0) {
        [_notificationNumber setHidden:NO];
        [_notificationNumber setText:[NSString stringWithFormat:@"%li",(long)newNotifications]];
    }else{
        [_notificationNumber setHidden:YES];
    }
}


@end
