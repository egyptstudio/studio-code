//
//  PostCell.m
//  FC Barcelona
//
//  Created by Eissa on 10/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "PostCell.h"
#import "WebImage.h"
#import "fansManager.h"
#import "WallManager.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "CommentsViewController.h"
#import "UserPhoto.h"
#import "PostDetailsViewController.h"
#import "ShareManagerViewController.h"
#import "User.h"
#import "studioManager.h"
#import "CaptureViewController.h"
#import "ShareViewController.h"
#import "ShareLibrary.h"
#import "CurrentUser.h"
#import "ImageToZoomViewController.h"
#import "FullScreenViewController.h"
#import "LikesViewController.h"
#import "FansDetailView.h"
#import "ImageDetailViewController.h"
#import "GetAndPushCashedData.h"
@implementation PostCell
{
    NSUserDefaults * defaults;
    UIViewController *mainController;
    __weak IBOutlet UIView *topView;
    UIAlertView* deleteAlertView;
}
@synthesize premiumImage;
@synthesize postImage;
@synthesize userImage;
@synthesize userName;
@synthesize country;
@synthesize likesCount;
@synthesize commentsCount;
@synthesize useCount;
@synthesize shareCount;
@synthesize likeButton;
@synthesize bigLoader;
@synthesize smallLoader;
@synthesize ranks;
@synthesize isEditable;
@synthesize allPosts;
@synthesize walltype;
@synthesize postDate;
@synthesize lastRequestTime;
@synthesize timeFilter;
@synthesize countriesArray;
@synthesize favoritePostsFlag;
@synthesize currentPost;
@synthesize upperView;
- (void)awakeFromNib{}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:NO animated:animated];
}

-(void)initImages{
    
    [postImage sd_setImageWithURL:[NSURL URLWithString:[currentPost postPhotoUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [bigLoader stopAnimating];
        [bigLoader setHidden:YES];
    }];
    
    
    [userImage sd_setImageWithURL:[NSURL URLWithString:[currentPost userProfilePicUrl]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [smallLoader stopAnimating];
        [smallLoader setHidden:YES];
    }];
    
}

-(void)initWithPost:(Post*)post andController:(UIViewController*) mainView{
    allPosts = [[NSMutableArray alloc] init];
    mainController = mainView;
    currentPost = post;
    
    userImage.borderColor = [UIColor whiteColor];
    userImage.borderWidth = 1.0;
    
    
    userName.text = [currentPost userName];
    country.text = [currentPost userCountry];
    
    likesCount.text =[currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
    
    useCount.text =[currentPost repostCount]>999? [NSString stringWithFormat:@"%d k",[currentPost repostCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost repostCount]];
    
    commentsCount.text = [currentPost commentCount]>999? [NSString stringWithFormat:@"%d k",[currentPost commentCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost commentCount]];
    
    
    
    shareCount.text=@"0";
    ranks.text = [NSString stringWithFormat:@"%d",[currentPost contestRank]];
    
    
    NSDate* theDate = [NSDate dateWithTimeIntervalSince1970:[currentPost creationTime]];
    postDate.text = [self relativeDateStringForDate:theDate];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(postFullScreen)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    [postImage addGestureRecognizer:tap];
    postImage.userInteractionEnabled = YES;
    
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(postLikesDetails)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    //[likesCount addGestureRecognizer:tap1];
    //
    likesCount.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFanDetails)];
    tap2.cancelsTouchesInView = YES;
    tap2.numberOfTapsRequired = 1;
    [upperView addGestureRecognizer:tap2];
    upperView.userInteractionEnabled = YES;
    
    
    if ([currentPost contestRank]>0) {
        [_rankView setHidden:NO];
    }
    //    for (int x=0; x<[currentPost.tags count]; x++) {
    //        [self appendSubView:[self prepareViewForTagWithText:[[[currentPost tags] objectAtIndex:x] objectForKey:@"tagName"] isServer:YES] onParentView:_tagsScroll ];
    //    }
    
    if ([[mainController.navigationController topViewController] isKindOfClass:[PostDetailsViewController class]]) {
        topView.hidden = !isEditable;
    }
    else
    {topView.hidden = YES;}
    
    
    if ([currentPost liked]) {
        [likeButton setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
    }
    else{
        [likeButton setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
        
    }
    
    
    if ([currentPost originalPostId]!=0 /*&& [currentPost userId]!=[[[CurrentUser getObject] getUser] userId]*/)
        [self addReposter];
    
    if ([currentPost userId]!=[[[CurrentUser getObject] getUser] userId])
    {
        CGSize textSize = [[currentPost userName] sizeWithFont:userName.font
                                             constrainedToSize:CGSizeMake(200, userName.frame.size.height) lineBreakMode: NSLineBreakByWordWrapping];
        
        UIButton * reposterState = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(userName.frame)+textSize.width,
                                                                              CGRectGetMinY(userName.frame),
                                                                              userName.frame.size.height,
                                                                              userName.frame.size.height)];
        [reposterState addTarget:self action:@selector(followUser:) forControlEvents:UIControlEventTouchUpInside];
        UIImage *state = [currentPost following] ? nil:[UIImage imageNamed:@"unfollowed.png"];
        [reposterState setBackgroundImage:state forState:UIControlStateNormal];
        [self addSubview:reposterState];
    }
    if ([currentPost repostCount] <= 0 ) {
        useCount.text = @"";
    }
    
    if ([currentPost likeCount] <= 0 ) {
        likesCount.text = @"";
    }
    
    if ([currentPost commentCount] <= 0 ) {
        commentsCount.text = @"";
    }
    
    if ([currentPost rePosted]) {
        [_repostButton setImage:[UIImage imageNamed:@"repost_sc.png"] forState:UIControlStateNormal];
    }
    else{
        [_repostButton setImage:[UIImage imageNamed:@"repost.png"] forState:UIControlStateNormal];
        
    }
    
}

-(void)postLikesDetails{
    
    if ([[CurrentUser getObject] isUser]) {
        LikesViewController * likes = [mainController.storyboard instantiateViewControllerWithIdentifier:@"LikesViewController"];
        likes.currentPost = currentPost;
        [mainController.navigationController pushViewController:likes animated:YES];
        
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
    
}

-(void)addReposter{
    @try {
        UILabel* reposter = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(userName.frame),
                                                                      CGRectGetMaxY(userName.frame),
                                                                      userName.frame.size.width+50,
                                                                      userName.frame.size.height)];
        reposter.font = [UIFont systemFontOfSize:10];
        [reposter setTextColor:[UIColor whiteColor]];
        NSString* repostedBy = [[[Language sharedInstance] stringWithKey:@"Home_repostedBy"] stringByAppendingString:@", "];
        reposter.text = [repostedBy stringByAppendingString:[currentPost rePostedBy]];
        [self addSubview:reposter];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)postFullScreen
{
    FullScreenViewController * fullScreen = [mainController.storyboard instantiateViewControllerWithIdentifier:@"FullScreenViewController"];
    [fullScreen setCurrentPost:currentPost];
    [fullScreen setAllPosts:allPosts];
    [fullScreen setWallType:walltype];
    [fullScreen setLastRequestTime:lastRequestTime];
    [fullScreen setTimeFilter:timeFilter];
    [fullScreen setCountriesArray:countriesArray];
    [fullScreen setFavoritePostsFlag:favoritePostsFlag];
    [fullScreen setSelectedPostIndex:self.tag];
    [mainController.navigationController pushViewController:fullScreen animated:YES];
}

- (IBAction)like:(id)sender {
    
    if ([[CurrentUser getObject] isUser]) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if (![currentPost liked]) {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] likePost:[[CurrentUser getObject] getUser].userId postId:[currentPost postId]
                                                    and:^(id currentClient) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [SVProgressHUD dismiss];
                                                            if (currentClient) {
                                                                if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                                {
                                                                    [sender setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
                                                                    
                                                                    currentPost.liked = YES;
                                                                    currentPost.likeCount++;
                                                                    likesCount.text =[currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
                                                                }
                                                            }
                                                        });
                                                    }onFailure:^(NSError *error) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [SVProgressHUD dismiss];
                                                            
                                                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                            [alert show];
                                                        });
                                                    }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
        else
        {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] dislikePost:[[CurrentUser getObject] getUser].userId postId:[currentPost postId]
                                                       and:^(id currentClient) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [SVProgressHUD dismiss];
                                                               if (currentClient) {
                                                                   if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                                   {
                                                                       [sender setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
                                                                       
                                                                       currentPost.liked = NO;
                                                                       currentPost.likeCount--;
                                                                       likesCount.text =[currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
                                                                       if ([currentPost likeCount] <= 0 ) {
                                                                           likesCount.text = @"";
                                                                       }
                                                                       
                                                                   }
                                                               }
                                                           });
                                                       }onFailure:^(NSError *error) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [SVProgressHUD dismiss];
                                                               
                                                               UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                               [alert show];
                                                           });
                                                       }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
}

- (IBAction)comment:(id)sender {
    if ([[CurrentUser getObject] isUser]) {
        UIGraphicsBeginImageContext(self.window.bounds.size);
        [self.window.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CommentsViewController * commentsView = [mainController.storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
        commentsView.post = currentPost;
        commentsView.userID  = [[[CurrentUser getObject] getUser] userId];
        commentsView.userName = [currentPost userName];
        commentsView.delegate = self;
        commentsView.backGround = image;
        [mainController.navigationController pushViewController:commentsView animated:YES];
        
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
    
}

-(void)commentAdded{
    currentPost.commentCount++;
    commentsCount.text = [currentPost commentCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost commentCount]];
}

- (void)commentDeleted{
    currentPost.commentCount--;
    commentsCount.text = [currentPost commentCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost commentCount]];
}

- (IBAction)use:(id)sender {
    
    
    //    if ([[CurrentUser getObject] isUser]) {
    //
    //        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    //        @try {
    //            if ([RestServiceAgent internetAvailable]) {
    //                [[StudioManager getInstance] getStudioPhoto:[[currentPost photo] studioPhotoId] and:^(id currentClient) {
    //                    dispatch_async(dispatch_get_main_queue(), ^{
    //                        [SVProgressHUD dismiss];
    //                        if (currentClient) {
    //                            currentPost.usedCount++;
    //                            useCount.text = [currentPost usedCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likesCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost usedCount]];
    //
    //                            CaptureViewController* captureView = [mainController.storyboard instantiateViewControllerWithIdentifier:@"CaptureViewController"];
    //                            captureView.studioPhotoViewModel =  (StudioPhotoViewModel*)currentClient;
    //                            [mainController.navigationController pushViewController:captureView animated:YES];
    //
    //                        }
    //                    });
    //                }onFailure:^(NSError *error) {
    //                    dispatch_async(dispatch_get_main_queue(), ^{
    //                        [SVProgressHUD dismiss];
    //
    //                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
    //                        [alert show];
    //                    });
    //                }];
    //            }
    //            else{
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    [SVProgressHUD dismiss];
    //                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
    //                    [alert show];
    //                });
    //
    //            }
    //
    //        }
    //        @catch (NSException *exception) {
    //            [SVProgressHUD dismiss];
    //            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
    //            [alert show];
    //        }
    //    }
    //    else{
    //        UIAlertView* error = [[UIAlertView alloc] initWithTitle:nil message:@"Please login to use this feature" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
    //        [error show];
    //    }
    
    [self rePost:sender];
}

- (IBAction)share:(id)sender {
    
    NSString *sharedLink = [NSString stringWithFormat:@"http://fcbstudio.mobi/share.php?postid=%i",currentPost.postId];
    sharedLink = [sharedLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSArray *itemsToShare = @[@"",[NSURL URLWithString:sharedLink]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes =@[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact]; //or whichever you don't need
    
    [mainController presentViewController:activityVC animated:YES completion:nil];


}
- (IBAction)imageDetails:(id)sender{
    
    ImageDetailViewController * commentsView = [mainController.storyboard instantiateViewControllerWithIdentifier:@"ImageDetailViewController"];
    commentsView.currentPost = currentPost;
    commentsView.isWall = NO;
    [mainController.navigationController pushViewController:commentsView animated:YES];
}



-(UIView*)prepareViewForTagWithText:(NSString*)tagText isServer:(BOOL)isServer
{
    UIFont *font = [UIFont systemFontOfSize:16] ;
    CGSize stringBox = [tagText sizeWithFont:font];
    
    UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, stringBox.width, stringBox.height)];
    [tagLabel setFont:font];
    [tagLabel setTextAlignment:NSTextAlignmentLeft];
    [tagLabel setTextColor:[UIColor blackColor]];
    [tagLabel setBackgroundColor:[UIColor clearColor]];
    [tagLabel setText:tagText];
    
    
    UIView *tagView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, stringBox.width+20 , _tagsScroll.frame.size.height)];
    UIImage *tagBackground = [UIImage imageNamed:@"tag_empty_field.png"];
    
    tagLabel.center = CGPointMake(tagView.frame.size.width/2, _tagsScroll.frame.size.height/2);
    
    
    [tagView addSubview:tagLabel];
    
    
    UIGraphicsBeginImageContext(tagView.frame.size);
    [tagBackground drawInRect:tagView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    tagView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    return tagView;
}

-(void)appendSubView:(UIView*)subSiew onParentView:(UIScrollView*)parentView
{
    
    for (NSObject * obj in parentView.subviews) {
        if (![obj isKindOfClass:[UIImageView class]]) {
            [(UIView*)obj setFrame:CGRectMake([(UIView*)obj frame].origin.x+2+subSiew.frame.size.width, 0, [(UIView*)obj frame].size.width, [(UIView*)obj frame].size.height)];
        }
    }
    subSiew.tag = [parentView.subviews count]-1;
    subSiew.frame = CGRectMake(0, 0, subSiew.frame.size.width, subSiew.frame.size.height);
    [parentView addSubview:subSiew];
    parentView.contentSize = CGSizeMake(parentView.contentSize.width+subSiew.frame.size.width+2, 0);
    
    
}
- (IBAction)editBtnPressed:(id)sender {
}

- (IBAction)deletBtnPressed:(id)sender {
    deleteAlertView=[[UIAlertView alloc] initWithTitle:@"Warning" message:@"are sure you want to delete the selected image" delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"],nil];
    [deleteAlertView show];
    
}



-(void)deletePhoto
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance] deletePost:[currentPost postId] and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if([[currentClient objectForKey:@"status"] intValue] == 1)
                    {
                        [[[UIAlertView alloc] initWithTitle:nil message:@"Image Deleted Successfully" delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles:nil] show];
                        [mainController.navigationController popViewControllerAnimated:YES];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"imageDeleted" object:nil];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
}

-(void)rePost:(id)sender
{
 
    if([[CurrentUser getObject] isUser]){
        if ([GetAndPushCashedData getObjectFromCash:@"RememberCostAlert"]) {
            [self promotePost];
        }
        else{
        
            NSString *title = [[Language sharedInstance] stringWithKey:@"RepostMessage"];
            NSString *message = [[Language sharedInstance] stringWithKey:@"DonAskAgain"];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] otherButtonTitles:[[Language sharedInstance] stringWithKey:@"Confirm"],nil];
            
            UIButton *checkBox = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30.0)];
           [checkBox setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
            [checkBox addTarget:self action:@selector(setDonOpenAgain:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 270, 30)];
            [view addSubview:checkBox];
            [checkBox setCenter:CGPointMake(view.frame.size.width/2, view.frame.size.height/2)];
            
            [alert setValue:view  forKey:@"accessoryView"];
            [alert setTag:1];
            [alert show];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}
-(void)setDonOpenAgain:(UIButton*)sender{
    
    if (![GetAndPushCashedData getObjectFromCash:@"RememberCostAlert"]) {
        [sender setBackgroundImage:[UIImage imageNamed:@"popup_checkbox_sc.png"] forState:UIControlStateNormal];
        [GetAndPushCashedData cashObject:@(YES) withAction:@"RememberCostAlert"];
    }
    else {
        [sender setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
        [GetAndPushCashedData removeObjectWithAction:@"RememberCostAlert"];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (alertView.tag) {
        case 1:
        {
            if (alertView.cancelButtonIndex != buttonIndex) {
                [self promotePost];
            }
            break;
        }
        case 2:
        {
            [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPointsTab"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
            break;
        }
        default:
            break;
    }
}

-(void)promotePost{
    int requiredPoints = 50 ;
    
    if (requiredPoints > [[CurrentUser getObject] getUser].credit){
        [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPointsTab"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    }
    else{
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *date = [dateFormat stringFromDate:[NSDate date] ];
        
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        @try {
            if ([RestServiceAgent internetAvailable]) {
                [[WallManager getInstance] rePost:[currentPost postId] userId:[[CurrentUser getObject] getUser].userId date:date and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if([[currentClient objectForKey:@"status"] intValue] == 1)
                        {
                            [currentPost setRePosted:YES];
                            [_repostButton setImage:[UIImage imageNamed:@"repost_sc.png"] forState:UIControlStateNormal];
                            
                            [[[CurrentUser getObject] getUser] setCredit:[CurrentUser getObject].getUser.credit - requiredPoints];
                            [[CurrentUser getObject] saveCurrentUser];
                            
                            [[[UIAlertView alloc] initWithTitle:nil
                                                        message:[[Language sharedInstance] stringWithKey:@"Home_repostAlert"]
                                                       delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"] otherButtonTitles:nil] show];
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
}

-(UIImage*)cropImage:(UIImage*)image toRect:(CGRect)rect {
    UIGraphicsBeginImageContextWithOptions(rect.size, false, [image scale]);
    [image drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];
    UIImage *cropped_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cropped_image;
}
-(void)followUser:(UIButton*)sender{
    if ([[CurrentUser getObject] isUser]) {
        if (![currentPost following]) {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[FansManager getInstance] followFan:[[CurrentUser getObject] getUser].userId
                                                   fanId:[currentPost userId]
                                                     and:^(id currentClient) {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             [SVProgressHUD dismiss];
                                                             if (currentClient) {
                                                                 if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                                 {
                                                                     [[[CurrentUser getObject] getUser] setFollowingCount:[CurrentUser getObject].getUser.followingCount + 1];
                                                                     [[CurrentUser getObject] saveCurrentUser];
                                                                     [currentPost setFollowing:YES];
                                                                     [sender setBackgroundImage:[UIImage imageNamed:@"followed.png" ] forState:UIControlStateNormal];
                                                                     for (int x=0 ; x < [allPosts count]; x++) {
                                                                         if ([(Post*)[allPosts objectAtIndex:x] userId]==[currentPost userId]) {
                                                                             [(Post*)[allPosts objectAtIndex:x] setFollowing:YES];
                                                                         }
                                                                     }
                                                                 }
                                                                 
                                                             }
                                                         });
                                                     }onFailure:^(NSError *error) {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             [SVProgressHUD dismiss];
                                                             UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                             [alert show];
                                                         });
                                                     }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}

- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateFormat stringFromDate:date];
    
    //    NSCalendarUnit units = NSDayCalendarUnit | NSWeekOfYearCalendarUnit |
    //    NSMonthCalendarUnit | NSYearCalendarUnit;
    //
    //    // if `date` is before "now" (i.e. in the past) then the components will be positive
    //    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
    //                                                                   fromDate:date
    //                                                                     toDate:[NSDate date]
    //                                                                    options:0];
    //
    //    if (components.year > 0) {
    //        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
    //    } else if (components.month > 0) {
    //        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
    //    } else if (components.weekOfYear > 0) {
    //        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
    //    } else if (components.day > 0) {
    //        if (components.day > 1) {
    //            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
    //        } else {
    //            return @"Yesterday";
    //        }
    //    } else {
    //        return @"Today";
    //    }
}
-(UIImage *) imageWithImage:(UIImage*)sourceImage scaledToWidth: (float) i_width {//method to scale image accordcing to width
    
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)openFanDetails
{
    if(![[CurrentUser getObject] getUser].userId) {
        UIStoryboard * sb = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ?
        [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] :
        [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
        fansDetailView.fanID = [currentPost userId];
        [mainController.navigationController pushViewController:fansDetailView animated:YES];
    }
    else {
        if ([currentPost userId]!=[[CurrentUser getObject] getUser].userId) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getBlockedUsersNCompareWith" object:[NSString stringWithFormat:@"%i", [currentPost userId]]];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
        }
    }
}

@end
