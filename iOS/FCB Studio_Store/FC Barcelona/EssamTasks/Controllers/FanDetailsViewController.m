//
//  FanDetailsViewController.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/3/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FanDetailsViewController.h"
#import "WebImage.h"
#import "FansManager.h"
#import "SVProgressHUD.h"
#import "User.h"
@interface FanDetailsViewController ()

@end

@implementation FanDetailsViewController{
    User * currentUser;
    NSUserDefaults * defaults;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    defaults = [NSUserDefaults standardUserDefaults];
    currentUser =  (User*)[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"user"]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
}

@end