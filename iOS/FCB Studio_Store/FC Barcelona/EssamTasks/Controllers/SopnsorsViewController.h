//
//  SopnsorsViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 5/3/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SopnsorsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UILabel *comingSoon;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;
- (IBAction)studio_Action:(id)sender;

@end
