//
//  StudioFilterViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/12/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "StudioFilterViewController.h"
#import "SVProgressHUD.h"
#import "StudioManager.h"
#import "RestServiceAgent.h"
#import "StudioFolder.h"
#import "WebImage.h"
#import "StudioPhoto.h"
#import "CaptureViewController.h"
#import "MenuViewController.h"
#import "SKBounceAnimation.h"
#import "CurrentUser.h" 
#import "StudioDetailsViewController.h"
@interface StudioFilterViewController ()

@end

@implementation StudioFilterViewController
{
    id seasonsDictionary;
    NSMutableArray *seasonFilter;
    BOOL isLatest;
    BOOL isMostUsed;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    seasonFilter = [[NSMutableArray alloc] init];

    CALayer * l3 = [self.applyButton layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:10.0];
    
    [self getSeasons];
}
-(void)viewWillAppear:(BOOL)animated{
    [self setLocalisables];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)latestButton_Action:(id)sender
{
    
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"popup_radio_btn_sc.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"popup_radio_btn.png"]);
    [_latestButton setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    [_mostButton setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    isLatest = YES;
    isMostUsed = NO;
    
}
- (IBAction)mostButton_Action:(id)sender
{
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"popup_radio_btn_sc.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"popup_radio_btn.png"]);
    [_latestButton setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    [_mostButton setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    isLatest = NO;
    isMostUsed = YES;
}
- (IBAction)applyButton_Action:(id)sender
{

    if (!isLatest && !isMostUsed && [seasonFilter count]<=0) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"no_Filter"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else{
        StudioDetailsViewController* details = [self.storyboard instantiateViewControllerWithIdentifier:@"StudioDetailsViewController"];
        details.currentFolder = 0  ;
        details.seasons = seasonFilter ;
        details.filterBy = isLatest ? 1 : isMostUsed ? 2 : 0;
        [self.navigationController pushViewController:details animated:YES];
    }
}
-(void)getSeasons
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[StudioManager getInstance] getSeasons:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        seasonsDictionary = currentClient;
                        [_seasonTableView reloadData];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)addSeasonFilter:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.seasonTableView];
    NSIndexPath *indexPath = [self.seasonTableView indexPathForItemAtPoint:buttonPosition];
    
    id filterId = [[seasonsDictionary objectAtIndex:indexPath.row] objectForKey:@"seasonId"] ;
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"cheackbox_sc.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"cheackbox.png"]);
    
    if ([seasonFilter containsObject:filterId])
    {
        [seasonFilter removeObject:filterId];
        [sender setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
        
    }
    else
    {
        [seasonFilter addObject:filterId];
        [sender setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    }
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [seasonsDictionary count] ;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    UILabel* seasonText  = (UILabel*)[cell viewWithTag:1];
    UIButton* checkButton  = (UIButton*)[cell viewWithTag:2];
    seasonText.text = [[seasonsDictionary objectAtIndex:indexPath.row] objectForKey:@"seasonName"];
    [checkButton addTarget:self action:@selector(addSeasonFilter:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (IBAction)back_Action:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)setLocalisables{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"PhotoFilter_title"]];
    [_latestLabel setText:[[Language sharedInstance] stringWithKey:@"Home_tab1"]];
    [_mostLabel setText:[[Language sharedInstance] stringWithKey:@"PhotoFilter_MostUsed"]];
    [_sortBy setText:[[Language sharedInstance] stringWithKey:@"PhotoFilter_Sortby"]];
    [_applyButton setTitle:[[Language sharedInstance] stringWithKey:@"Filter1_Apply"] forState:UIControlStateNormal];
    [(UILabel*)[self.view viewWithTag:1] setText:[[Language sharedInstance] stringWithKey:@"PhotoFilter_Season"]];
}


@end
