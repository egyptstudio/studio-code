//
//  Season.h
//  FC Barcelona
//
//  Created by Eissa on 10/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"
@interface Season : NSObject
@property int  seasonId;
@property NSString * seasonName;
@property NSMutableArray *posts;
@end
