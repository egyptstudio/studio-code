//
//  CitiesFilterViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/14/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "CitiesFilterViewController.h"
#import "CountryManager.h"
#import "Country.h"
#import "SVProgressHUD.h"

@interface CitiesFilterViewController ()
{
    id citiesDictionary;
    NSMutableArray * citiesArray;

}
@end

@implementation CitiesFilterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    citiesArray  = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [_titleLabel setText:[[Language sharedInstance] stringWithKey:@"Filter1_title2"]];
    [self requestCountries];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [citiesDictionary count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    UILabel *cityName = (UILabel*)[cell viewWithTag:1];
    cityName.text = [(Country*)[citiesDictionary objectAtIndex:indexPath.row] countryName] ;
    
    UIButton *selectCity = (UIButton*)[cell viewWithTag:2];
    [selectCity addTarget:self action:@selector(addCity:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor = [UIColor clearColor];
    

    NSString *cityId     = [NSString stringWithFormat:@"%i",[(Country*)[citiesDictionary objectAtIndex:indexPath.row] countryId]] ;
    NSData *checked      = UIImagePNGRepresentation([UIImage imageNamed:@"cheackbox_sc.png"]);
    NSData *unchecked    = UIImagePNGRepresentation([UIImage imageNamed:@"cheackbox.png"]);
    if (![citiesArray containsObject:cityId])
    {
        [selectCity setImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    }
    else
    {
        [selectCity setImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    }
    
    return cell;
}
-(void)addCity:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    NSString *cityId     = [NSString stringWithFormat:@"%i",[(Country*)[citiesDictionary objectAtIndex:indexPath.row] countryId]] ;
    NSData *checked      = UIImagePNGRepresentation([UIImage imageNamed:@"cheackbox_sc.png"]);
    NSData *unchecked    = UIImagePNGRepresentation([UIImage imageNamed:@"cheackbox.png"]);
    if ([citiesArray containsObject:cityId])
    {
        [citiesArray removeObject:cityId];
        [sender setImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
        
    }
    else
    {
        [citiesArray addObject:cityId];
        [sender setImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    }
    
}
- (void)requestCountries {
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    [[CountryManager getInstance] getCountriesListand:^(id currentClient) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            citiesDictionary = currentClient;
            [_tableView reloadData];
        });
    } onFailure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)done_Action:(id)sender {

    [_delegate didSelectCountries:citiesArray];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
