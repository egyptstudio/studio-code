//
//  FollowersExchangePurchase.h
//  twitterExampleIII
//
//  Created by ousamaMacBook on 6/19/13.
//  Copyright (c) 2013 MacBook. All rights reserved.
//

#import "ShopIAPHelper.h"

@interface FCBPurchase : ShopIAPHelper

+ (FCBPurchase *)sharedInstance:(NSMutableArray*)identiferes;

@end
