//
//  ViewProfileViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/13/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "ViewProfileViewController.h"
#import "CurrentUser.h"
#import "WebImage.h"
#import "ViewProfile2ViewController.h"
#import "EditProfileViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "FansCollectionView.h"
#import "GetAndPushCashedData.h"
#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface ViewProfileViewController ()<CLLocationManagerDelegate>

@end

@implementation ViewProfileViewController
{
    CLLocationManager *locationManager;
    CLGeocoder*geocoder;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    geocoder = [[CLGeocoder alloc] init];
//    locationManager = [[CLLocationManager alloc] init];
//    locationManager.delegate = self;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    locationManager.distanceFilter = kCLDistanceFilterNone;
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
//        [locationManager requestWhenInUseAuthorization];
//    }
//    [locationManager startUpdatingLocation];
//    
    
    UITapGestureRecognizer *followingTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(following_Action:)];
    followingTap.cancelsTouchesInView = YES;
    followingTap.numberOfTapsRequired = 1;
    [_followingView addGestureRecognizer:followingTap];
    _followingView.userInteractionEnabled = YES;
    
    
    UITapGestureRecognizer *followerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followers_Action:)];
    followerTap.cancelsTouchesInView = YES;
    followerTap.numberOfTapsRequired = 1;
    [_followerView addGestureRecognizer:followerTap];
    _followerView.userInteractionEnabled = YES;

    
    
    // Do any additional setup after loading the view.
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self prepareView];
    [self setLocalisables];

}
-(void)prepareView
{
    
    @try {

        
        CALayer * l0 = [self.followerView layer];
        [l0 setMasksToBounds:YES];
        [l0 setCornerRadius:3];
        CALayer * l1 = [self.followingView layer];
        [l1 setMasksToBounds:YES];
        [l1 setCornerRadius:3];
        
        //CALayer *mask = [CALayer layer];
        //mask.contents = (id)[[UIImage imageNamed:@"photo_mask.png"] CGImage];
        //mask.frame = CGRectMake(0, 0, _userImageView.frame.size.width, _userImageView.frame.size.height);
        //_userImageView.layer.mask = mask;
        //_userImageView.layer.masksToBounds = YES;
        

        [_userImage sd_setImageWithURL:[NSURL URLWithString:[[[CurrentUser getObject] getUser] profilePic]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [_loader stopAnimating];

        }];
        
        
        [_followerCount setFont:[UIFont fontWithName:@"stencilie" size:32]];
        [_followingCount setFont:[UIFont fontWithName:@"stencilie" size:32]];
        [_xps setFont:[UIFont fontWithName:@"Barcelona2012byDaniloSL" size:20]];
        _followingCount.text =[NSString stringWithFormat:@"%i",[[CurrentUser getObject] getUser].followingCount];
        _followerCount.text =[NSString stringWithFormat:@"%i",[[CurrentUser getObject] getUser].followersCount];
        _xps.text = [NSString stringWithFormat:@"%i XP",[[CurrentUser getObject] getUser].credit];
        float completion = [[[CurrentUser getObject] getUser] calculateProfileCompletion]/100.0;
        
        [_completionPercentage setText:[NSString stringWithFormat:@"%.0f%%",completion*100.0]];
        [_completionProgress setProgress:completion];
        [_completionView setHidden:NO];
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editButton_Action:)];
        tap1.cancelsTouchesInView = YES;
        tap1.numberOfTapsRequired = 1;
        [_completionView addGestureRecognizer:tap1];
        if (completion >= 1) {
            _completeToWin.hidden = YES;
        }
        /// Manar
        [[CurrentUser getObject] refreshUser];
        
        _userName.text    = [[[CurrentUser getObject] getUser] fullName];
        [_userName sizeToFit];
        [_userName setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2, _userName.center.y)];

        _editButton.frame = CGRectMake(CGRectGetMaxX(_userName.frame)+2,
                                       _editButton.frame.origin.y,
                                       _editButton.frame.size.width,
                                       _editButton.frame.size.height);

        _email.text       = [[[[CurrentUser getObject] getUser] email] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _country.text     = ![[[[[CurrentUser getObject] getUser] countryName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] ?
                            [[[CurrentUser getObject] getUser] countryName]:[[Language sharedInstance] stringWithKey:@"profile_country_initial"];
        _city.text        = [[[CurrentUser getObject] getUser] cityName] ?
                            [[[CurrentUser getObject] getUser] cityName]:[[Language sharedInstance] stringWithKey:@"profile_city_initial"] ;
        _district.text    = ![[[[[CurrentUser getObject] getUser] district] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] ?
                            [[[CurrentUser getObject] getUser] district]:[[Language sharedInstance] stringWithKey:@"profile_district_initial"] ;
        _phone.text       = ![[[[[CurrentUser getObject] getUser] phone] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] ?
                            [[[CurrentUser getObject] getUser] phone]:[[Language sharedInstance] stringWithKey:@"profile_phone_initial"];
        
        NSDate* theDate = [NSDate dateWithTimeIntervalSince1970:[[[CurrentUser getObject] getUser] dateOfBirth]];
        _birthDate.text   = [[[CurrentUser getObject] getUser] dateOfBirth] != 0 ?
                            [self relativeDateStringForDate:theDate]:[[Language sharedInstance] stringWithKey:@"profile_birthdate_initial"] ;
        
        
        
        
        _gender.text      = [[[CurrentUser getObject] getUser] gender] == 0 ?
                            [[Language sharedInstance] stringWithKey:@"MyProfile_Gender"] : [[[CurrentUser getObject] getUser] gender] == 1? [[Language sharedInstance] stringWithKey:@"FansFilter_Male"]:[[Language sharedInstance] stringWithKey:@"FansFilter_Female"]  ;
        [_premiumButton setHidden:[[[CurrentUser getObject] getUser] isPremium]];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception:\n %@",exception.description);
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];

}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}
- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    return [dateFormat stringFromDate:date];
    
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)editButton_Action:(id)sender
{
    EditProfileViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    [self.navigationController pushViewController:viewController animated:YES];

}
- (IBAction)moreButton_Action:(id)sender
{
    ViewProfile2ViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewProfile2ViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)premiumButton_Action:(id)sender
{
    [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPremTab"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (locations == nil)
        return;
    [geocoder reverseGeocodeLocation:[locations objectAtIndex:0] completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (placemarks == nil)
             return;
    }];
}
- (IBAction)followers_Action:(id)sender
{
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    FansCollectionView *fansCollectionView = (FansCollectionView *)[sb instantiateViewControllerWithIdentifier:@"FansCollectionView"];
    fansCollectionView.viewTitle = [[Language sharedInstance] stringWithKey:@"menu_Followers"];
    fansCollectionView.viewCount = [_followerCount.text intValue];
    fansCollectionView.fanID = [[CurrentUser getObject] getUser].userId;
    fansCollectionView.isFollowers = YES;
    [self.navigationController pushViewController:fansCollectionView animated:YES];
}

- (IBAction)following_Action:(id)sender
{
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    FansCollectionView *fansCollectionView = (FansCollectionView *)[sb instantiateViewControllerWithIdentifier:@"FansCollectionView"];
    fansCollectionView.viewTitle = [[Language sharedInstance] stringWithKey:@"menu_Following"];
    fansCollectionView.viewCount = [_followingCount.text intValue];
    fansCollectionView.fanID = [[CurrentUser getObject] getUser].userId;
    fansCollectionView.isFollowers = NO;
    [self.navigationController pushViewController:fansCollectionView animated:YES];
}

-(void)setLocalisables{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"User_Profile"]];
    [_following setText:[[Language sharedInstance] stringWithKey:@"menu_Following"]];
    [_follower setText:[[Language sharedInstance] stringWithKey:@"menu_Followers"]];
    [_completeToWin setText:[[Language sharedInstance] stringWithKey:@"complete_profile"]];
    [_completionTitle setText:[[Language sharedInstance] stringWithKey:@"user_profile_completion"]];
    
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_moreButton setTitle:[[Language sharedInstance] stringWithKey:@"user_profile_more"] forState:UIControlStateNormal];
    [_premiumButton setTitle:[[Language sharedInstance] stringWithKey:@"Fans_Upgrade"] forState:UIControlStateNormal];
}
@end

@implementation UILabel (dynamicSizeMeWidth)
-(void)resizeToStretch{
    float width = [self expectedWidth];
    CGRect newFrame = [self frame];
    newFrame.size.width = width;
    [self setFrame:newFrame];
}

-(float)expectedWidth{
    [self setNumberOfLines:1];
    
    CGSize maximumLabelSize = CGSizeMake( CGFLOAT_MAX, CGRectGetWidth(self.bounds) );
    
    CGSize expectedLabelSize = [[self text] sizeWithFont:[self font]
                                       constrainedToSize:maximumLabelSize
                                           lineBreakMode:[self lineBreakMode]];
    return expectedLabelSize.width;
}
@end