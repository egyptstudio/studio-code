//
//  ConfirmInviteFriendsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/24/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "ConfirmInviteFriendsViewController.h"
#import "CurrentUser.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"

@interface ConfirmInviteFriendsViewController ()

@end

@implementation ConfirmInviteFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CALayer * l1 = [self.messageView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    _userName.text = [[CurrentUser getObject] getUser].fullName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)done_Action:(id)sender {
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[UsersManager getInstance] sendInviteSMS:[[CurrentUser getObject] getUser].userId phoneNumbers:_phoneNumbers invitationText:@"One of your friends invites you to check this cool app \n  http://tawasolit.com/FCB/"
          and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient[@"status"] integerValue] ==1 ) {
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Invitation will delivered shortly." delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }

}

-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
@end
