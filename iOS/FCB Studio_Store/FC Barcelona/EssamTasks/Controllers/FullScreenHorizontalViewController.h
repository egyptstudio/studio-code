//
//  FullScreenHorizontalViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/28/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import "RGMPagingScrollView.h"
@interface FullScreenHorizontalViewController : UIViewController
@property (weak, nonatomic) IBOutlet RGMPagingScrollView *scrollView;
@property (assign) NSMutableArray * allPosts;

-(void)prepareWithPost:(Post*)post;
@end
