//
//  EditProfileViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/13/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
@interface EditProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *profilePicView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *addPhoto_Caption;
- (IBAction)addPhoto_Action:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *moreButton;
- (IBAction)moreButton_Action:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *userNameView;
@property (weak, nonatomic) IBOutlet UIView *countryView;
@property (weak, nonatomic) IBOutlet UIView *cityView;
@property (weak, nonatomic) IBOutlet UIView *districtView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *dateOfBirthView;
@property (weak, nonatomic) IBOutlet UIView *dateOfBirthPickerView;
@property (weak, nonatomic) IBOutlet UIView *countryCodeView;

@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLabel;

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirthPickerLabel;

@property (weak, nonatomic) IBOutlet UILabel *countryTitle;
@property (weak, nonatomic) IBOutlet UILabel *cityTitle;
@property (weak, nonatomic) IBOutlet UILabel *birthDateTitle;



@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *districtField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *dateOfBirthField;

@property (weak, nonatomic) IBOutlet UIButton *maleButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleButton;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *mainScroll;

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (nonatomic)  BOOL fromPoints;

- (IBAction)maleButton_Action:(id)sender;
- (IBAction)femaleButton_Action:(id)sender;
- (IBAction)saveButton_Action:(id)sender;

@end
