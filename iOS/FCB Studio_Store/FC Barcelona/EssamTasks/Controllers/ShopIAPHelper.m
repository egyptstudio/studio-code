//
//  IAPHelper.m
//  twitterExampleIII
//
//  Created by ousamaMacBook on 6/19/13.
//  Copyright (c) 2013 MacBook. All rights reserved.
//

#import "ShopIAPHelper.h"

@interface ShopIAPHelper () <SKProductsRequestDelegate,SKPaymentTransactionObserver>
@end

NSString *const IAPHelperProductPurchasedNotificationn = @"IAPHelperProductPurchasedNotification";
NSString *const IAPHelperProductRestoreNotificationn = @"IAPHelperProductRestoredNotification";
NSString *const IAPHelperProductFailedNotificationn = @"IAPHelperProductFailedNotification";
NSString *const shallIBuy = @"shallIBuy";

static SKPaymentTransaction* currentTrans;

@implementation ShopIAPHelper
{
    SKProductsRequest * _productsRequest;
    // 4
    NSMutableSet * _productIdentifiers;
    NSMutableSet * _purchasedProductIdentifiers;
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    if ((self = [super init])) {
        //delegate for purchasing
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        // Store product identifiers
        _productIdentifiers = [[NSMutableSet alloc]initWithSet:productIdentifiers];
    }
    return self;
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    _completionHandler = completionHandler;
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
}


- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"Loaded list of products...");
    _productsRequest = nil;
    
    NSArray * skProducts = response.products;
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",skProduct.productIdentifier,skProduct.localizedTitle,skProduct.price.floatValue);
    }
    
    if ([skProducts count]>0) {
        _completionHandler(YES, skProducts);
    }
    else{
        _completionHandler(NO, nil);
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    _completionHandler(NO, nil);
}

#pragma mark - purchasing methods
- (BOOL)productPurchased:(NSString *)productIdentifier {
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

- (void)buyProduct:(SKProduct *)product {
    [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"]setValue:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"] forKey:shallIBuy];
    NSLog(@"Buying %@...", product.productIdentifier);
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];

}

#pragma mark SKPaymentTransactionOBserver
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

- (void)provideContentForTransaction:(SKPaymentTransaction *)transaction {
    [_purchasedProductIdentifiers addObject:transaction.payment.productIdentifier];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotificationn object:transaction userInfo:nil];
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    [self provideContentForTransaction:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    [self provideContentForTransaction:transaction];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductRestoreNotificationn object:transaction userInfo:nil];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductFailedNotificationn object:transaction userInfo:nil];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}



@end
