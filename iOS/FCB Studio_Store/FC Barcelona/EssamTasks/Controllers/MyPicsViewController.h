//
//  MyPicsViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/14/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyPhotosDelegate <NSObject>
@required
- (void)didSelectImage:(UIImage*)image withUrl:(NSString*)url;
@end
@interface MyPicsViewController : UIViewController
@property (nonatomic,assign) id <MyPhotosDelegate> delegate;

- (IBAction)back_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collection;
@property (weak, nonatomic) IBOutlet UILabel *pageTiles;

@end
