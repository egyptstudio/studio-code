//
//  SettingsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/18/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "SettingsViewController.h"
#import "MSPickerViewController.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "WallManager.h"
#import "CurrentUser.h"
#import "GetAndPushCashedData.h"

@interface SettingsViewController ()<MSPickerViewControllerDelegate>

@end

@implementation SettingsViewController
{
    MSPickerViewController *pickerView;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLocalisables];
    
    if ([[GetAndPushCashedData getObjectFromCash:@"LoggedWithSocialMedia"] isEqualToString:@"YES"]) {
        [_changPassButton setHidden:YES];
        [_logoutButton setFrame:_changPassButton.frame];
        [(UIView *)[self.view viewWithTag:10] setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)pickerView:(MSPickerViewController *)PickerView didSelectItem:(int)row {
    [[Language sharedInstance] setLanguage:row];
    _languageCurrent.text = [[Language sharedInstance] arrayWithKey:@"Languages"][row];
    [self setLocalisables];
    [[CurrentUser getObject] updateUser];
}

- (void)pickerViewDidDismiss {
    pickerView = nil;
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)language_Action:(id)sender {
    pickerView = [MSPickerViewController pickerViewWithTitle:
                  [[Language sharedInstance] stringWithKey:@"Settings_Language"]
                  forItems:
                  [[Language sharedInstance] arrayWithKey:@"Languages"]
                  selectedItem:[[Language sharedInstance] getLanguage]];
    
    pickerView.delegate = self;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Close"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
}

- (IBAction)privacy_Action:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openPrivacySettings" object:nil];
}

- (IBAction)blockedUsers_Action:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openBlockedUsers" object:nil];
}

- (IBAction)messgas_Action:(id)sender {
}

- (IBAction)contact_Action:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openContactUs" object:nil];
}

- (IBAction)changePass_Action:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChangePassword" object:nil];
}
- (IBAction)logout_Action:(id)sender {
    @try {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if([RestServiceAgent internetAvailable])
        {
            [[WallManager  getInstance] logOutUser:[[CurrentUser getObject] getUser].userId and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (currentClient) {
                        if([[currentClient objectForKey:@"status"] intValue] == 1)
                        {
                            
                            [self performSelector:@selector(sendLogOut) withObject:nil afterDelay:0];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    

}
-(void)sendLogOut{
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logOut" object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)setLocalisables{
    [_pageTitle       setText:[[Language sharedInstance] stringWithKey:@"Settings_title"]];
    [_languageTitle   setText:[[Language sharedInstance] stringWithKey:@"Settings_Language"]];
    [_languageCurrent setText:[[Language sharedInstance] arrayWithKey:@"Languages"][[[Language sharedInstance] getLanguage]]];
    [_privacyButton setTitle:[[Language sharedInstance] stringWithKey:@"Settings_Privacy"] forState:UIControlStateNormal];
    [_messageButton setTitle:[[Language sharedInstance] stringWithKey:@"Settings_SystemMessages"] forState:UIControlStateNormal];
    [_contactButton setTitle:[[Language sharedInstance] stringWithKey:@"settings_contact_us"] forState:UIControlStateNormal];
    [_changPassButton setTitle:[[Language sharedInstance] stringWithKey:@"Settings_ChangePassword"] forState:UIControlStateNormal];
    [_logoutButton setTitle:[[Language sharedInstance] stringWithKey:@"Settings_LogOut"] forState:UIControlStateNormal];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_blockedUsersButton setTitle:[[Language sharedInstance] stringWithKey:@"settings_blocked_users"] forState:UIControlStateNormal];
}

@end
