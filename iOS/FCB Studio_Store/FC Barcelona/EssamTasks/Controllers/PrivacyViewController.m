//
//  PrivacyViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/18/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "PrivacyViewController.h"
#import "MSPickerViewController.h"
#import "CurrentUser.h"
#import "User.h"
#import "UsersManager.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "StudioManager.h"

@interface PrivacyViewController ()<MSPickerViewControllerDelegate>

@end

@implementation PrivacyViewController
{
    NSArray *friendRequestsValues;
    NSArray *personalInfoValues;
    NSArray *contactsInfoValues;
    MSPickerViewController *pickerView;
    
    int friendRequests;
    int personalInfo;
    int contactsInfo;
    BOOL receivePush;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    friendRequestsValues = @[[[Language sharedInstance] stringWithKey:@"privacy_option1"],
                             [[Language sharedInstance] stringWithKey:@"privacy_option2"]];
    personalInfoValues   = @[[[Language sharedInstance] stringWithKey:@"privacy_option1"],
                             [[Language sharedInstance] stringWithKey:@"Messages_Friends"],
                             [[Language sharedInstance] stringWithKey:@"privacy_option2"]];
    contactsInfoValues   = @[[[Language sharedInstance] stringWithKey:@"privacy_option1"],
                             [[Language sharedInstance] stringWithKey:@"Messages_Friends"],
                             [[Language sharedInstance] stringWithKey:@"privacy_option2"]];
    
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"Settings_Privacy"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_friendRequestsLabel setText:[[Language sharedInstance] stringWithKey:@"privacy_item1"]];
    [_personalInfoLabel setText:[[Language sharedInstance] stringWithKey:@"privacy_personal_info"]];
    [_contactsInfoLabel setText:[[Language sharedInstance] stringWithKey:@"privacy_contact_info"]];
    [_pushStateLabel setText:[[Language sharedInstance] stringWithKey:@"privacy_item4"]];
    
    [self initPrivacyValues];
    
}

-(void)initPrivacyValues{
    friendRequests = [[CurrentUser getObject] getUser].receiveFriendRequest;
    personalInfo   = [[CurrentUser getObject] getUser].viewPersonalInfo;
    contactsInfo   = [[CurrentUser getObject] getUser].viewContactsInfo;
    receivePush    = [[CurrentUser getObject] getUser].receivePushNotification;
    
    _friendRequestsValue.text = friendRequests == 0 ?[[Language sharedInstance] stringWithKey:@"Select"] :
    friendRequests==1? friendRequestsValues[0] :
    friendRequestsValues[1] ;
    
    _personalInfoValue.text   = personalInfo   ==0 ?[[Language sharedInstance] stringWithKey:@"Select"] : personalInfoValues[personalInfo-1];
    _contactsInfoValue.text   = contactsInfo   ==0 ?[[Language sharedInstance] stringWithKey:@"Select"] : contactsInfoValues[contactsInfo-1];
    _pushStateButton.on = [[CurrentUser getObject] getUser].receivePushNotification;
}

- (void)pickerView:(MSPickerViewController *)PickerView didSelectItem:(int)row {
    switch (PickerView.tag) {
        case 1:
            _friendRequestsValue.text = friendRequestsValues[row];
            friendRequests =row==0?1:2;
            [self savePrivacyValues];
            break;
        case 2:
            _personalInfoValue.text   = personalInfoValues[row];
            personalInfo = row+1;
            [self savePrivacyValues];
            break;
        case 3:
            _contactsInfoValue.text   = contactsInfoValues[row];
            contactsInfo = row+1;
            [self savePrivacyValues];
            break;
        default:
            break;
    }
}

- (void)pickerViewDidDismiss {
    pickerView = nil;
}

- (IBAction)friendRequests_Action:(id)sender{
    pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                    forItems:friendRequestsValues
                                                selectedItem:friendRequests!=0?friendRequests==1?0:1: -1];
    pickerView.delegate = self;
    pickerView.tag = 1 ;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
}

- (IBAction)personalInfo_Action:(id)sender{
    pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                    forItems:personalInfoValues
                                                selectedItem:personalInfo!=0?personalInfo-1:-1];
    pickerView.delegate = self;
    pickerView.tag = 2 ;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
}

- (IBAction)contactsInfo_Action:(id)sender{
    pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                    forItems:contactsInfoValues
                                                selectedItem:contactsInfo!=0?contactsInfo-1:-1];
    pickerView.delegate = self;
    pickerView.tag = 3 ;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
    
}




- (void)savePrivacyValues{
    User* user = [[User alloc] init];

    [user setUserId:[[CurrentUser getObject] getUser].userId];
    [user setNoOfPics:[[CurrentUser getObject] getUser].noOfPics];
    [user setUserType:[[CurrentUser getObject] getUser].userType];
    [user setGender:[[CurrentUser getObject] getUser].gender];
    [user setCredit:[[CurrentUser getObject] getUser].credit];
    [user setCountryId:[[CurrentUser getObject] getUser].countryId];
    [user setCityId:[[CurrentUser getObject] getUser].cityId];
    [user setFollowersCount:[[CurrentUser getObject] getUser].followersCount];
    [user setFollowingCount:[[CurrentUser getObject] getUser].followingCount];
    [user setIncome:[[CurrentUser getObject] getUser].income];
    [user setRelationship:[[CurrentUser getObject] getUser].relationship];
    [user setDeviceType:[[CurrentUser getObject] getUser].deviceType];
    [user setDateOfBirth:[[CurrentUser getObject] getUser].dateOfBirth];
    [user setAllowLocation:[[CurrentUser getObject] getUser].allowLocation];
    [user setIsOnline:[[CurrentUser getObject] getUser].isOnline];
    [user setIsPremium:[[CurrentUser getObject] getUser].isPremium];
    [user setEmailVerified:[[CurrentUser getObject] getUser].emailVerified];
    [user setPhoneVerified:[[CurrentUser getObject] getUser].phoneVerified];
    [user setProfilePic:[[CurrentUser getObject] getUser].profilePic];
    [user setFullName:[[CurrentUser getObject] getUser].fullName];
    [user setPassword:[[CurrentUser getObject] getUser].password];
    [user setEmail:[[CurrentUser getObject] getUser].email];
    [user setLatitude:[[CurrentUser getObject] getUser].latitude];
    [user setLongitude:[[CurrentUser getObject] getUser].longitude];
    [user setRegistrationDate:[[CurrentUser getObject] getUser].registrationDate];
    [user setLastLoginDate:[[CurrentUser getObject] getUser].lastLoginDate];
    [user setSocialMediaId:[[CurrentUser getObject] getUser].socialMediaId];
    [user setBrief:[[CurrentUser getObject] getUser].brief];
    [user setCountryName:[[CurrentUser getObject] getUser].countryName];
    [user setCityName:[[CurrentUser getObject] getUser].cityName];
    [user setDistrict:[[CurrentUser getObject] getUser].district];
    [user setPhone:[[CurrentUser getObject] getUser].phone];
    [user setStatus:[[CurrentUser getObject] getUser].status];
    [user setAboutMe:[[CurrentUser getObject] getUser].aboutMe];
    [user setReligion:[[CurrentUser getObject] getUser].religion];
    [user setEducation:[[CurrentUser getObject] getUser].education];
    [user setJob:[[CurrentUser getObject] getUser].job];
    [user setCompany:[[CurrentUser getObject] getUser].company];
    [user setDeviceToken:[[CurrentUser getObject] getUser].deviceToken];
    [user setCountryCode:[[CurrentUser getObject] getUser].countryCode];
    [user setReceiveFriendRequest:friendRequests];
    [user setViewPersonalInfo:personalInfo];
    [user setViewContactsInfo:contactsInfo];
    [user setReceivePushNotification:receivePush];
    [user setJobRole:[[CurrentUser getObject] getUser].jobRole];

    
    @try {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if([RestServiceAgent internetAvailable])
        {
            [[UsersManager getInstance] updateUserProfile:user witImage:nil and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (currentClient) {
                        [SVProgressHUD dismiss];
                        if([currentClient isKindOfClass:[User class]])
                        {
                            [[CurrentUser getObject] setUser:(User*)currentClient];
                            [[CurrentUser getObject] saveCurrentUser];
                            [self initPrivacyValues];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [self initPrivacyValues];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    
                });
            }];
        }
        else
        {
            [SVProgressHUD dismiss];
            [self initPrivacyValues];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        [self initPrivacyValues];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
}

- (IBAction)pushState_Action:(UISwitch*)sender{
    receivePush = sender.on;
    [self savePrivacyValues];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}


@end
