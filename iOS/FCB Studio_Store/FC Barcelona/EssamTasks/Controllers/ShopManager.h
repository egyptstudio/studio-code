//
//  ShopManager.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessManager.h"

@interface ShopManager : BusinessManager

+(ShopManager*)getInstance;

-(void)subscripeInPremium:(int)userID subscriptionDuration:(int)subscriptionDuration transactionId:(NSString* )transactionId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)LoadInAppProductIDs:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)getStoreLink:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;
-(void)myPremiumStatus:(int)userID  and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure;


@end


