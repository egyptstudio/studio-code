//
//  SettingsViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/18/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UILabel *languageTitle;
@property (weak, nonatomic) IBOutlet UILabel *languageCurrent;
@property (weak, nonatomic) IBOutlet UIButton *privacyButton;
@property (weak, nonatomic) IBOutlet UIButton *blockedUsersButton;
@property (weak, nonatomic) IBOutlet UIButton *messageButton;
@property (weak, nonatomic) IBOutlet UIButton *contactButton;
@property (weak, nonatomic) IBOutlet UIButton *changPassButton;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;


- (IBAction)language_Action:(id)sender;
- (IBAction)privacy_Action:(id)sender;
- (IBAction)blockedUsers_Action:(id)sender;

- (IBAction)messgas_Action:(id)sender;
- (IBAction)contact_Action:(id)sender;
- (IBAction)changePass_Action:(id)sender;
- (IBAction)logout_Action:(id)sender;

- (IBAction)openStudio:(id)sender;
- (IBAction)back_Action:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

@end
