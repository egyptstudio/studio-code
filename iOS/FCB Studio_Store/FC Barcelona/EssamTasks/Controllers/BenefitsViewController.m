//
//  BenefitsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/2/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "BenefitsViewController.h"

@interface BenefitsViewController ()

@end

@implementation BenefitsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
