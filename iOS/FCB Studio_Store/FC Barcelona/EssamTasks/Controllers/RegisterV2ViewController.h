//
//  RegisterV2ViewController.h
//  FC Barcelona
//
//  Created by Eissa on 9/24/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImageView.h"
#import <CoreLocation/CoreLocation.h>

@protocol RegisterDelegate <NSObject>
@required
- (void)didRegSuccessfully:(CLPlacemark*)place;
@end

@interface RegisterV2ViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,assign) id <RegisterDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *profilePicView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *addPhoto_Caption;
- (IBAction)addPhotot_Action:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addImageLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UITextField *fullName;
@property (weak, nonatomic) IBOutlet UIView *fullNameView;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UITextField *passWord;
@property (weak, nonatomic) IBOutlet UIView *passWordView;
@property (weak, nonatomic) IBOutlet UITextField *confirmPass;
@property (weak, nonatomic) IBOutlet UIView *confirmPassView;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;

- (IBAction)registerButton_Action:(id)sender;
- (IBAction)back_Action:(id)sender;

@property id socialProfile;

@end
