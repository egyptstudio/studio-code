//
//  BlockedUsersViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 3/8/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "BlockedUsersViewController.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
#import "CurrentUser.h"

@interface BlockedUsersViewController ()<UITableViewDataSource,UITableViewDelegate>
@end
@implementation BlockedUsersViewController
{
    NSMutableArray *blockedUsersArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    blockedUsersArray = [[NSMutableArray alloc] init];

    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    self.tableView.tableFooterView = footerView;
    [self.tableView setSeparatorStyle:(UITableViewCellSeparatorStyleNone)];
    
    CALayer *l = [_noBlockedUsersIcon layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:50];
    [l setBorderWidth:1];
    [l setBorderColor:[UIColor blackColor].CGColor];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
}
-(void)viewWillAppear:(BOOL)animated{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"settings_blocked_users"]];
    [_noBlockedUsersLabel1 setText:[[Language sharedInstance] stringWithKey:@"settings_no_blocked_user"]];
    [_noBlockedUsersLabel2 setText:[[Language sharedInstance] stringWithKey:@"settings_no_blocked_user_description"]];

    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [self getBlockedUsers];
}
-(void)getBlockedUsers{

    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[UsersManager getInstance] getBlockedUsers:[[CurrentUser getObject] getUser].userId and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        [blockedUsersArray addObjectsFromArray:currentClient[@"data"]];
                        [_tableView reloadData];
                        
                        if ([blockedUsersArray count]<=0)
                            [self showNoBlockedUsersMessage];
                        else
                            [self hideNoBlockedUsersMessage];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [blockedUsersArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myCell"];
    }
    UILabel *userName = (UILabel*)[cell viewWithTag:1];
    userName.text = blockedUsersArray[indexPath.row][@"username"];
    UIButton *unblock = (UIButton*)[cell viewWithTag:2];
    [unblock setTitle:[[Language sharedInstance] stringWithKey:@"settings_Unblocked_user"] forState:UIControlStateNormal];
    [unblock addTarget:self action:@selector(unblockUser:) forControlEvents:UIControlEventTouchUpInside];
    
    CALayer *l = [unblock layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)unblockUser:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    int fanId = [blockedUsersArray[indexPath.row][@"fanId"] intValue];
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[UsersManager getInstance] unblockUser:[[CurrentUser getObject] getUser].userId fanId:fanId  and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        if ([currentClient[@"status"] intValue]==1) {
                            [blockedUsersArray removeObjectAtIndex:indexPath.row];
                            [_tableView reloadData];
                            if ([blockedUsersArray count]<=0)
                                [self showNoBlockedUsersMessage];
                            else
                                [self hideNoBlockedUsersMessage];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)showNoBlockedUsersMessage{
    [_noBlockedUsersView setHidden:NO];
}
-(void)hideNoBlockedUsersMessage{
    [_noBlockedUsersView setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
