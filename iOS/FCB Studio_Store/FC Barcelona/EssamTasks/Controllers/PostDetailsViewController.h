//
//  PostDetailsViewController.h
//  FC Barcelona
//
//  Created by Eissa on 9/10/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserPhoto.h"
#import "Post.h"

@interface PostDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *imageName;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(assign) Post* userPost;
@property (weak, nonatomic) IBOutlet UILabel *postDate;
-(IBAction)back_Action:(id)sender;
@property(assign) BOOL isEditable;

@end

