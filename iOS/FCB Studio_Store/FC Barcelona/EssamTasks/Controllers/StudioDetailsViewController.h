//
//  StudioDetailsViewController.h
//  FC Barcelona
//
//  Created by Eissa on 9/26/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudioFolder.h"
#import "RGMPagingScrollView.h"
@interface StudioDetailsViewController : UIViewController<UIScrollViewDelegate,RGMPagingScrollViewDelegate,RGMPagingScrollViewDatasource>



- (IBAction)back_Action:(id)sender;
@property(assign) StudioFolder *currentFolder;

@property(assign)NSMutableArray * tempDic;
@property(nonatomic) int purchasedIndex;
@property(weak, nonatomic) IBOutlet UILabel *pageTitle;
@property(weak, nonatomic) IBOutlet UILabel *photoCredit;
@property(weak, nonatomic) IBOutlet UILabel *usersCredit;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

@property (weak, nonatomic) IBOutlet UIView *adBanner;

@property(weak, nonatomic) IBOutlet UICollectionView *tableView;
@property(nonatomic,assign) int folderId;

@property (weak, nonatomic) IBOutlet UIButton *useButton;

@property (weak, nonatomic) IBOutlet RGMPagingScrollView *imagesTableView;
@property (weak, nonatomic) IBOutlet UIView *galleryView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (assign) int filterBy;
@property (assign) NSArray *seasons;


- (IBAction)openFilter:(id)sender;
- (IBAction)useButton_Action:(id)sender;
- (IBAction)nextImage:(id)sender;
- (IBAction)prevImage:(id)sender;
- (IBAction)closePopup:(id)sender;


- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;
@end
