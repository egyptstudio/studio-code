//
//  IAPHelper.h
//  twitterExampleIII
//
//  Created by ousamaMacBook on 6/19/13.
//  Copyright (c) 2013 MacBook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "NSData+Base64.h"

UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotificationn;
UIKIT_EXTERN NSString *const IAPHelperProductRestoreNotificationn;
UIKIT_EXTERN NSString *const IAPHelperProductFailedNotificationn;
UIKIT_EXTERN NSString *const shallIBuy;
BOOL isRealBuy;
typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface ShopIAPHelper : NSObject

@property (nonatomic, copy) RequestProductsCompletionHandler completionHandler;
@property(strong, nonatomic) NSURLConnection* pointsConnection;
@property(strong, nonatomic) NSURLConnection* validateConnection;
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (void)buyProduct:(SKProduct *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;
- (void)restoreCompletedTransactions;
@end
