//
//  FansViewController.m
//  FC Barcelona
//
//  Created by Mohamed Mitwaly on 9/3/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FansViewController.h"
#include "FansManager.h"
#import "Fan.h"
#import "WebImage.h"
#import "FanDetailsViewController.h"
#import "CountryManager.h"
#import "Country.h"
#import "SVProgressHUD.h"
#import "User.h"
#import "RestServiceAgent.h"

@interface FansViewController (){
    NSUserDefaults * defaults;
    User* currentUser;
    id resultsDictionary;
    id citiesDictionary;

    NSMutableArray * premiumUsers;
    NSMutableArray * citiesArray;
    BOOL isFans;
    int pageNumber;
    int FansSortingEnum;
    NSMutableDictionary *FansFiltersEnum;
}

@end

@implementation FansViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    isFans = YES;
    pageNumber = 0 ;
    FansSortingEnum = 0 ;
    premiumUsers = [[NSMutableArray alloc] init];
    citiesArray  = [[NSMutableArray alloc] init];
    defaults = [NSUserDefaults standardUserDefaults];
    currentUser =  (User*)[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"user"]];
    
    FansFiltersEnum = [[NSMutableDictionary alloc] init];
    [FansFiltersEnum setValue:@"0" forKey:@"online"];
    [FansFiltersEnum setValue:@"0" forKey:@"offline"];
    [FansFiltersEnum setValue:@"0" forKey:@"male"];
    [FansFiltersEnum setValue:@"0" forKey:@"female"];
    [FansFiltersEnum setValue:@"0" forKey:@"allFans"];
    [FansFiltersEnum setValue:@"0" forKey:@"following"];
    [FansFiltersEnum setValue:@"0" forKey:@"followers"];
    
    
    CALayer * l0 = [self.applyButton layer];
    [l0 setMasksToBounds:YES];
    [l0 setCornerRadius:10.0];
    
    CALayer * l1 = [self.filterView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:10.0];
    
    CALayer * l2 = [self.citiesView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:10.0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self getFans];
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return isFans ? [resultsDictionary count]:[premiumUsers count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    UILabel* fanName        = (UILabel*)[cell viewWithTag:1];
    UIImageView* fanImage   = (UIImageView*)[cell viewWithTag:2];
    UIButton* followButton  = (UIButton*)[cell viewWithTag:3];
    UIView* fanStatus       = (UIView*)[cell viewWithTag:4];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:5] ;
    UIImageView* premiumImage   = (UIImageView*)[cell viewWithTag:6];

    
    fanName.text = [(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:indexPath.row] userName] ;
    
    
    fanImage.clipsToBounds = YES;
    [self setRoundedView:fanImage toDiameter:fanImage.frame.size.width];
    [WebImage processImageDataWithURLString:[(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:indexPath.row] profilePic] cacheImage:YES andBlock:^(NSData *imageData) {
        fanImage.image = [UIImage imageWithData:imageData];
        [loader stopAnimating];
    }];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewFan:)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    [fanImage addGestureRecognizer:tap1];
    fanImage.userInteractionEnabled = YES;
    
    
    
    
    followButton.tag = indexPath.row;
    [followButton setTitle:[(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:indexPath.row] isFollow]?@"UnFollow":@"Follow"
                  forState:UIControlStateNormal];
    [followButton addTarget:self action:@selector(followFan:) forControlEvents:UIControlEventTouchUpInside];
    CALayer * l = [followButton layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10];

    
    fanStatus.clipsToBounds = YES;
    [self setRoundedView:fanStatus toDiameter:fanStatus.frame.size.width];
    fanStatus.backgroundColor = [(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:indexPath.row] online]?[UIColor greenColor]:[UIColor redColor];
    
    if (![(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:indexPath.row] isPremium])
        premiumImage.hidden = YES;
    
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [citiesDictionary count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    UILabel *cityName = (UILabel*)[cell viewWithTag:1];
    cityName.text = [(Country*)[citiesDictionary objectAtIndex:indexPath.row] countryName] ;

    UIButton *selectCity = (UIButton*)[cell viewWithTag:2];
    [selectCity addTarget:self action:@selector(addCity:) forControlEvents:UIControlEventTouchUpInside];
    selectCity.tag = indexPath.row;

    return cell;
}
-(void)addCity:(UIButton*)sender
{
    NSString *cityId     = [NSString stringWithFormat:@"%d",[(Country*)[citiesDictionary objectAtIndex:[sender tag]] countryId]] ;
    NSData *checked      = UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"]);
    NSData *unchecked    = UIImagePNGRepresentation([UIImage imageNamed:@"unchecked.png"]);
    
    
    
    if ([citiesArray containsObject:cityId])
    {
        [citiesArray removeObject:cityId];
        [sender setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];

    }
    else
    {
        [citiesArray addObject:cityId];
        [sender setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    }

}
-(void)viewFan:(id)sender
{
    [(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:[[sender view] tag]] userId];
    
}
-(void)followFan:(UIButton*)sender
{
    if ([(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:[sender tag]] isFollow]) {
        @try {
            if ([RestServiceAgent internetAvailable]) {
                [[FansManager getInstance] unFollowFan:currentUser.Id
                                               fanId:[(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:[sender tag]] userId]
                                                and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if (currentClient) {
                            if([[currentClient objectForKey:@"status"] intValue] == 1)
                            {
                                [sender setTitle:@"Follow" forState:UIControlStateNormal];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"The Internet connection is not available, please check it and try again" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        @try {
            if ([RestServiceAgent internetAvailable]) {
                [[FansManager getInstance] followFan:currentUser.Id
                                                 fanId:[(Fan*)[isFans?resultsDictionary:premiumUsers objectAtIndex:[sender tag]] userId]
                                                   and:^(id currentClient) {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           [SVProgressHUD dismiss];
                                                           if (currentClient) {
                                                               if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                               {
                                                                   [sender setTitle:@"UnFollow" forState:UIControlStateNormal];
                                                                   
                                                               }
                                                           }
                                                       });
                                                   }onFailure:^(NSError *error) {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           [SVProgressHUD dismiss];
                                                           
                                                           UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                                                           [alert show];
                                                       });
                                                   }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"The Internet connection is not available, please check it and try again" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert show];
        }
    }
}
-(void)getFans
{
    [SVProgressHUD showWithStatus:@"LOADING" maskType:SVProgressHUDMaskTypeClear];
    
    if (FansSortingEnum != 0 || [citiesArray count]>0 || ![_searchText.text isEqualToString:@""] )
    {
        @try {
            if ([RestServiceAgent internetAvailable]) {
                pageNumber++;
                [[FansManager getInstance]getFans:currentUser.userId pageNo:pageNumber searchText:_searchText.text sortBy:FansSortingEnum filters:FansFiltersEnum countriesIds:citiesArray favoriteFans:nil  and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if (currentClient) {
                            resultsDictionary = currentClient;
                            for (int i=0; i < [resultsDictionary count]; i++) {
                                if ([(Fan*)[resultsDictionary objectAtIndex:i] isPremium])
                                    [premiumUsers addObject:(Fan*)[resultsDictionary objectAtIndex:i]];
                            }
                            [_tableView reloadData];
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                        [alert show];
                        pageNumber--;
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"The Internet connection is not available, please check it and try again" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        @try {
            if ([RestServiceAgent internetAvailable]) {
                pageNumber++;
                [[FansManager getInstance]getFans:currentUser.userId pageNo:pageNumber  and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if (currentClient) {
                            resultsDictionary = currentClient;
                            for (int i=0; i < [resultsDictionary count]; i++) {
                                if ([(Fan*)[resultsDictionary objectAtIndex:i] isPremium])
                                    [premiumUsers addObject:(Fan*)[resultsDictionary objectAtIndex:i]];
                            }
                            [_tableView reloadData];
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                        [alert show];
                        pageNumber--;
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"The Internet connection is not available, please check it and try again" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert show];
        }

    }
    
}

-(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height) {
        [self getFans];
        return;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)fans_Action:(id)sender {
    isFans = YES;
    [self.tabImage setImage:[UIImage imageNamed:@"tabright_player.png"]];
    [_tableView reloadData];
}

- (IBAction)premuim_Action:(id)sender {
    isFans = NO;
    [self.tabImage setImage:[UIImage imageNamed:@"tab_left_team.png"]];
    [_tableView reloadData];
}

- (IBAction)filter_Action:(id)sender {
    [_backgroundView setHidden:NO];
    [_filterView setHidden:NO];
}

- (IBAction)closePopup:(id)sender {
    [_backgroundView setHidden:YES];
    [_filterView setHidden:YES];
}
- (IBAction)closeCitiesPopup:(id)sender {
    [_citiesView setHidden:YES];
}


- (IBAction)selectCountry_Action:(id)sender {

    [SVProgressHUD showWithStatus:@"LOADING" maskType:SVProgressHUDMaskTypeClear];
    [[CountryManager getInstance] getCountriesListand:^(id currentClient) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            citiesDictionary = currentClient;
            _citiesView.hidden = NO;
            [_citiesTableView reloadData];
        });
    } onFailure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert show];
        });
    }];
}

- (IBAction)applyButton_Action:(id)sender {
    pageNumber = 0;
    [self closePopup:nil];
    [self getFans];
}


- (IBAction)nearButton_Action:(id)sender
{
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"unchecked.png"]);
    
    [_nearButton   setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    [_latestButton setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    [_mostButton   setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    FansSortingEnum = 1;
}
- (IBAction)latestButton_Action:(id)sender
{
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"unchecked.png"]);
    
    [_nearButton   setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    [_latestButton setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    [_mostButton   setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    FansSortingEnum = 2;
}
- (IBAction)mostButton_Action:(id)sender
{
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"unchecked.png"]);
    
    [_nearButton   setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    [_latestButton setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
    [_mostButton   setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
    FansSortingEnum = 3;
}
- (IBAction)onlineButton_Action:(id)sender
{
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"unchecked.png"]);
    
    
    if ([[FansFiltersEnum objectForKey:@"online"] isEqualToString:@"1"]) {
        [sender setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
        [FansFiltersEnum setValue:@"0" forKey:@"online"];

    }
    else{
        [sender setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
        [FansFiltersEnum setValue:@"1" forKey:@"online"];

    }
}
- (IBAction)offlineButton_Action:(id)sender
{
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"unchecked.png"]);
    
    if ([[FansFiltersEnum objectForKey:@"offline"] isEqualToString:@"1"]) {
        [sender setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
        [FansFiltersEnum setValue:@"0" forKey:@"offline"];
        
    }
    else{
        [sender setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
        [FansFiltersEnum setValue:@"1" forKey:@"offline"];
        
    }
}
- (IBAction)maleButton_Action:(id)sender
{
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"unchecked.png"]);
    
    if ([[FansFiltersEnum objectForKey:@"male"] isEqualToString:@"1"]) {
        [sender setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
        [FansFiltersEnum setValue:@"0" forKey:@"male"];
        
    }
    else{
        [sender setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
        [FansFiltersEnum setValue:@"1" forKey:@"male"];
        
    }
}
- (IBAction)femaleButton_Action:(id)sender
{
    NSData *checked = UIImagePNGRepresentation([UIImage imageNamed:@"checked.png"]);
    NSData *unchecked = UIImagePNGRepresentation([UIImage imageNamed:@"unchecked.png"]);
    
    if ([[FansFiltersEnum objectForKey:@"female"] isEqualToString:@"1"]) {
        [sender setBackgroundImage:[UIImage imageWithData:unchecked] forState:UIControlStateNormal];
        [FansFiltersEnum setValue:@"0" forKey:@"female"];
        
    }
    else{
        [sender setBackgroundImage:[UIImage imageWithData:checked] forState:UIControlStateNormal];
        [FansFiltersEnum setValue:@"1" forKey:@"female"];
        
    }
}
@end
