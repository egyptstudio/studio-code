//
//  MainWallViewController.m
//  FC Barcelona
//
//  Created by Eissa on 3/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//


#import "MainWallViewController.h"
#import "Login/LoginViewController.h"
#import "PostsViewController.h"
#import "MenuViewController.h"
#import "SKBounceAnimation.h"
#import "Top10ViewController.h"
#import "StudioV2ViewController.h"
#import "FansViewController.h"
#import "CurrentUser.h"
#import "CitiesFilterViewController.h"
#import "InviteFriendsViewController.h"
#import "KxMenu.h"
#import "ViewProfileViewController.h"
#import "SettingsViewController.h"
#import "ChangePasswordViewController.h"
#import "PrivacyViewController.h"
#import "ContactUsViewController.h"
#import "InfoViewController.h"
#import "PrivacyAgreementViewController.h"
#import "AboutFCBarcelonaViewController.h"
#import "AboutTawasolViewController.h"
#import "AboutFCBStudioViewController.h"
#import "ChatMainViewController.h"
#import "CurrentUser.h"
#import "FansListView.h"
#import "ShopViewController.h"
#import "FriendsView.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
#import "VerifyPhoneViewController.h"
#import "ContestViewController.h"
#import "GetAndPushCashedData.h"
#import "FMDBHelper.h"
#import "FansManager.h"
#import "Fan.h"
#import "HelpViewController.h"
#import "TutorialViewController.h"
#import "BlockedUsersViewController.h"
#import "SopnsorsViewController.h"
#import "NotificationViewController.h"
#import "MessagingViewController.h"
#import "WallManager.h"
#import "FansDetailView.h"
#import "StudioManager.h"
#import "BarcelonaAppDelegate.h"
#import "SmartPopup.h"
#import <objc/runtime.h>

#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface MainWallViewController ()<PostsListDelegate,LoginDelegate,CityFilterDelegate,Top10Delegate,UIAlertViewDelegate>
{
    LoginViewController * login;
    Top10ViewController * topTen;
    UIDynamicAnimator *animator;
    PostsViewController * latestPosts;
    PostsViewController * myPicsPosts;
    PostsViewController * wallPosts;
    BOOL isNeedDialog;
    BOOL displayFavorate;
    BOOL dialogIsOpend;
    NSMutableArray* countriesList;
    CGRect mainContainerFrame;
    BOOL filterOn;
    BOOL filterForLatest;
    CurrentContent currentContent ;
    CGPoint barCenter;
    UILabel* selectedCountries;
    int premPageNumber;
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *segments;
@end

@implementation MainWallViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)dumpInfo
{
    Class clazz = [UIDatePicker class];
    u_int count;
    
    Ivar* ivars = class_copyIvarList(clazz, &count);
    NSMutableArray* ivarArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++)
    {
        const char* ivarName = ivar_getName(ivars[i]);
        [ivarArray addObject:[NSString  stringWithCString:ivarName encoding:NSUTF8StringEncoding]];
    }
    free(ivars);
    
    objc_property_t* properties = class_copyPropertyList(clazz, &count);
    NSMutableArray* propertyArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++)
    {
        const char* propertyName = property_getName(properties[i]);
        [propertyArray addObject:[NSString  stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
    }
    free(properties);
    
    Method* methods = class_copyMethodList(clazz, &count);
    NSMutableArray* methodArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++)
    {
        SEL selector = method_getName(methods[i]);
        const char* methodName = sel_getName(selector);
        [methodArray addObject:[NSString  stringWithCString:methodName encoding:NSUTF8StringEncoding]];
    }
    free(methods);
    
    NSDictionary* classDump = [NSDictionary dictionaryWithObjectsAndKeys:
                               ivarArray, @"ivars",
                               propertyArray, @"properties",
                               methodArray, @"methods",
                               nil];
    
    NSLog(@"%@", classDump);
}
- (void)viewDidLoad
{
    [self dumpInfo];
    
    [super viewDidLoad];
    

    
    [_segmentsView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:84/255.0f blue:164/255.0f alpha:1]];
    [_segments setFrame:_segmentsView.bounds];
    [_segmentsViewTap setFrame:_segments.bounds];
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    UIFont *fontBold = [UIFont boldSystemFontOfSize:14.0f];
    
    UIColor *color = [UIColor whiteColor];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesBold = [NSDictionary dictionaryWithObjectsAndKeys:
                                    fontBold,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    [_segments setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [_segments setTitleTextAttributes:attributesBold forState:UIControlStateSelected];
    
    
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_normal.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"]
                         forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    UIView *devider = [[UIView alloc] initWithFrame:CGRectMake(0, 0, .5, _segments.frame.size.height)];
    [devider setBackgroundColor:[UIColor whiteColor]];
    UIGraphicsBeginImageContextWithOptions(devider.bounds.size, devider.opaque, 0.0);
    [devider.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * deviderImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateSelected
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateNormal
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    //////////////////
    
    barCenter = _buttonBar.center;
    latestPosts = [self.storyboard instantiateViewControllerWithIdentifier:@"PostsViewController"];
    myPicsPosts = [self.storyboard instantiateViewControllerWithIdentifier:@"PostsViewController"];
    wallPosts   = [self.storyboard instantiateViewControllerWithIdentifier:@"PostsViewController"];
    latestPosts.delegate = self;
    myPicsPosts.delegate = self;
    wallPosts.delegate = self;
    
    mainContainerFrame = _mainContainer.frame;
    login  = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    login.delegate = self;
    topTen = [self.storyboard instantiateViewControllerWithIdentifier:@"Top10ViewController"];
    topTen.delegate = self;
    
    
    
    
    
    [_latest setBackgroundImage:nil forState:UIControlStateNormal];
    [_top10 setBackgroundImage:nil forState:UIControlStateNormal];
    [_wall setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"] forState:UIControlStateNormal];
    [_myPics setBackgroundImage:nil forState:UIControlStateNormal];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    [_segments setSelectedSegmentIndex:2];
    
    if ([[CurrentUser getObject] isUser]) {
        [self loginDone];
    }
    else
        [self performSelector:@selector(wall_Action:) withObject:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openMessaging:) name:@"openMessaging" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postingDone:) name:@"postingDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDone) name:@"userLoggedIn" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openStudio)    name:@"openStudio" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHome)      name:@"openHome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openFans)      name:@"openFans" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openContest)   name:@"openContest" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openFriends)   name:@"openFriends" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openSponsors)  name:@"openSponsers" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openNotifications)  name:@"openNotifications" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myPics_Action:)      name:@"imageDeleted" object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOut)  name:@"logOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshUser)      name:@"refreshUser" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wall_Action:)  name:@"imageAdded" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openLoginScreen)  name:@"openLogin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animationStopped) name:@"ViewFooter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openShop) name:@"openShop" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chat_Action:) name:@"openChat" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openProfile) name:@"openProfile" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openSettings) name:@"openSettings" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openChangePassword) name:@"openChangePassword" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emailVerified) name:@"emailVerified" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(phoneVerified) name:@"phoneVerified" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openPrivacySettings) name:@"openPrivacySettings" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openContactUs) name:@"openContactUs" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openPrivacyAgreement) name:@"openPrivacyAgreement" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAboutFCBStudio) name:@"openAboutFCBStudio" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAboutFCBarcelona) name:@"openAboutFCBarcelona" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAboutTawasolit) name:@"openAboutTawasolit" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openInfo) name:@"openInfo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openTop10) name:@"openTop10" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHelp)      name:@"openHelp" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openBlockedUsers)      name:@"openBlockedUsers" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getBlockedUsersNCompareWith:) name:@"getBlockedUsersNCompareWith" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myPics_Action:)      name:@"imageDeleted" object:nil];
    
    CALayer * l = [_goLoginButton layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    [_goLoginButton addTarget:self action:@selector(openLoginScreen) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[CurrentUser getObject] isUser]) {
        _studioView.hidden=NO;
    }
    else{
        _studioView.hidden=YES;
    }
    
    [_segments setTitle:@"Latest" forSegmentAtIndex:0];
    [_segments setTitle:@"Top 10" forSegmentAtIndex:1];
    [_segments setTitle:@"Wall"   forSegmentAtIndex:2];
    [_segments setTitle:@"My pics"forSegmentAtIndex:3];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollToTop:)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    [_segmentsViewTap addGestureRecognizer:tap1];
    
    NSUserDefaults *defaults= [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    if (![defaults boolForKey:@"tipOpened"]) {
        [_tipView setHidden:NO];
    }
    
    UITapGestureRecognizer *studioTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openStudio:)];
    studioTap.cancelsTouchesInView = YES;
    studioTap.numberOfTapsRequired = 1;
    [_tipView addGestureRecognizer:studioTap];
    _tipView.userInteractionEnabled = YES;
    premPageNumber=0;
}
-(void)viewWillAppear:(BOOL)animated{
    self.screenName = @"Wall Screen";

    if ([[CurrentUser getObject] isUser]) {
        if (![[CurrentUser getObject] getUser].emailVerified) {
            [self closeEmailVerificationDialog];
            [self openEmailVerificationDialog];
        }
        //    else if([[CurrentUser getObject] getUser].emailVerified && ![[CurrentUser getObject] getUser].phoneVerified && [[CurrentUser getObject] getUser].phone){
        //        [self closePhoneVerificationDialog];
        //        [self openPhoneVerificationDialog];
        //    }
        else {
            [self closeEmailVerificationDialog];
            [self closePhoneVerificationDialog];
        }
    }
    
    [self setLocalisables];
}

-(void)viewDidAppear:(BOOL)animated{
    NSUserDefaults *defaults= [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![[GetAndPushCashedData getObjectFromCash:@"tutorialOpend"] boolValue]) {
            TutorialViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (![[GetAndPushCashedData getObjectFromCash:@"menuOpened"] boolValue]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openMenu" object:nil];
            _segments.selectedSegmentIndex = 0;
            isNeedDialog = NO ;
            _filterButton.hidden = NO;
            [_filterButton setImage:[UIImage imageNamed:@"new_filter"] forState:UIControlStateNormal];
            [_latest setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"] forState:UIControlStateNormal];
            [_top10 setBackgroundImage:nil forState:UIControlStateNormal];
            [_wall setBackgroundImage:nil forState:UIControlStateNormal];
            [_myPics setBackgroundImage:nil forState:UIControlStateNormal];
            [[_mainContainer subviews]  makeObjectsPerformSelector:@selector(removeFromSuperview)];
            _needLoginView.hidden = YES;
            latestPosts = [self.storyboard instantiateViewControllerWithIdentifier:@"PostsViewController"];
            latestPosts.delegate = self;
            latestPosts.requestType = 2;
            currentContent = latest;
            [self displayController:latestPosts];
        }
        else if ([defaults objectForKey:@"justReg"]) {
            [defaults removeObjectForKey:@"justReg"];
            [defaults synchronize];
            //InviteFriendsViewController* invite = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteFriendsViewController"];
            //[self.navigationController pushViewController:invite animated:YES];
        }
    });
    
//    [[[SmartPopup alloc] initWithBody:@"Hello there you guys."
//      withShowAgainChanged:^(BOOL flag) {
//        NSLog(@"Should Show Again:%@",flag?@"True":@"False");
//    } withDismissChanged:^(BOOL flag) {
//        NSLog(@"Dismissed With Success:%@",flag?@"True":@"False");
//        
//    }] showPopUpInController:self];
    
    
}

-(void)openLoginScreen{
    [self.navigationController pushViewController:login animated:YES];
}

-(void)scrollToTop:(UITapGestureRecognizer*)tap{
    CGPoint tapPoint = [tap locationInView:tap.view ];
    NSInteger index ;
    if (tapPoint.x > 0 && tapPoint.x < tap.view.frame.size.width/4) {
        index = 0;
    }else if (tapPoint.x > tap.view.frame.size.width/4 && tapPoint.x < (tap.view.frame.size.width/4)*2) {
        index = 1;
    }else if (tapPoint.x > (tap.view.frame.size.width/4)*2 && tapPoint.x < (tap.view.frame.size.width/4)*3) {
        index = 2;
    }else if (tapPoint.x > (tap.view.frame.size.width/4)*3 && tapPoint.x < (tap.view.frame.size.width/4)*4) {
        index = 3;
    }
    if ([_segments selectedSegmentIndex]==index) {
        switch (currentContent) {
            case wall:
                [wallPosts scrollTopTop];
                break;
            case latest:
                [latestPosts scrollTopTop];
                break;
            case myPics:
                [myPicsPosts scrollTopTop];
                break;
            default:
                break;
        }
    }
    else{
        [_segments setSelectedSegmentIndex:index];
        [self segmentChanged:_segments];
    }
    
}
-(void)refreshUser
{[[CurrentUser getObject]refreshUser];}

-(void)loginDone
{
    _studioView.hidden=NO;
    [self performSelector:@selector(wall_Action:) withObject:nil];
    
    if (![[CurrentUser getObject] getUser].emailVerified) {
        [self closeEmailVerificationDialog];
        [self openEmailVerificationDialog];
    }
//    else if([[CurrentUser getObject] getUser].emailVerified && ![[CurrentUser getObject] getUser].phoneVerified && [[CurrentUser getObject] getUser].phone){
//        [self closePhoneVerificationDialog];
//        [self openPhoneVerificationDialog];
//    }
    else {
        [self closeEmailVerificationDialog];
        [self closePhoneVerificationDialog];
    }
}

-(void)postingDone:(NSNotification*)notification
{
    switch ([notification.object intValue]) {
        case 1:
            [_segments setSelectedSegmentIndex:0];
            [self performSelector:@selector(latest_Action:) withObject:nil];
            break;
        case 2:
            [_segments setSelectedSegmentIndex:2];
            [self performSelector:@selector(wall_Action:) withObject:nil];
            break;
        case 3:
            [_segments setSelectedSegmentIndex:3];
            [self performSelector:@selector(myPics_Action:) withObject:nil];
            break;
        default:
            break;
    }
   
    
}

-(void)openTop10
{
    [_segments setSelectedSegmentIndex:1];
    [self performSelector:@selector(top10_Action:) withObject:nil];
    
}

-(void)openEmailVerificationDialog{
    
    UIView *dialogView = [[UIView alloc] initWithFrame:CGRectMake(_mainContainer.frame.origin.x+10,
                                                                  _mainContainer.frame.origin.y,
                                                                  _mainContainer.frame.size.width-20,80)];
    [dialogView setTag:100];
    dialogView.backgroundColor = [UIColor whiteColor];
    CALayer *l = [dialogView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10];
    
    UILabel *dialogTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, dialogView.frame.size.width-20, 20)];
    dialogTitle.textAlignment = NSTextAlignmentLeft;
    dialogTitle.textColor = _goLoginButton.backgroundColor;
    dialogTitle.text = [[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_emailMessage"];
    [dialogView addSubview:dialogTitle];
    
    
    UIButton *resendButton = [[UIButton alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(dialogTitle.frame)+10,150,30)];
    [resendButton setTitle:[[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_ResendEmail"] forState:UIControlStateNormal];
    [resendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [resendButton setBackgroundColor:_goLoginButton.backgroundColor];
    CALayer *l3 = [resendButton layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:5];
    [resendButton addTarget:self action:@selector(reSend_Action) forControlEvents:UIControlEventTouchUpInside];
    [dialogView addSubview:resendButton];
    
    
    UIButton *helpButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(resendButton.frame)+30,
                                                                      CGRectGetMaxY(dialogTitle.frame)+10,30,30)];
    [helpButton addTarget:self action:@selector(openEmailVerificationHelp:) forControlEvents:UIControlEventTouchUpInside];
    [helpButton setTitle:@"?" forState:UIControlStateNormal];
    [helpButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [helpButton setBackgroundColor:[UIColor whiteColor]];
    [helpButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    CALayer *l1 = [helpButton layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:15];
    [l1 setBorderWidth:2.0];
    [l1 setBorderColor:[[UIColor blackColor] CGColor]];
    [dialogView addSubview:helpButton];
    
    
    _mainContainer.frame = CGRectMake(_mainContainer.frame.origin.x,
                                      _mainContainer.frame.origin.y+dialogView.frame.size.height+5,
                                      _mainContainer.frame.size.width,
                                      _mainContainer.frame.size.height-dialogView.frame.size.height-5);
    
    [self.view insertSubview:dialogView belowSubview:_filterButton];
}

-(void)openEmailVerificationHelp:(UIButton*)sender{
    [KxMenu showMenuInView:self.view
                  fromRect:[sender.superview convertRect:sender.frame toView:nil]
                 menuItems:@[[KxMenuItem menuItem:[[Language sharedInstance]stringWithKey:@"HomeUnconfirmedEmailAndPhone_phoneWhy_one"] image:nil target:nil action:nil],
                             [KxMenuItem menuItem:[[Language sharedInstance]stringWithKey:@"HomeUnconfirmedEmailAndPhone_emailWhy_two"] image:nil target:nil action:nil]]];
}

-(void)closeEmailVerificationDialog{
    
    for (UIView* view in self.view.subviews) {
        if (view.tag == 100) {
            _mainContainer.frame = mainContainerFrame;
            [_mainContainer.subviews[0] setFrame:_mainContainer.bounds];
            [view removeFromSuperview];
        }
    }
}

- (void)reSend_Action{
    
    [self checkIFNotificationEnabled];
    @try {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if([RestServiceAgent internetAvailable])
        {
            [[UsersManager getInstance] verifyUserEmail:[[CurrentUser getObject] getUser].userId email:[[CurrentUser getObject] getUser].email and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (currentClient) {
                        [SVProgressHUD dismiss];
                        if([[currentClient objectForKey:@"status"] intValue] == 1)
                        {
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                                            message:[[Language sharedInstance] stringWithKey:@"ForgotPassword_postEmailMessage"]
                                                                           delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                            [alert show];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
}
-(void)checkIFNotificationEnabled{
    if (![self isRegisteredForRemoteNotifications] && [self canOpenSettings]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please, enable notifications" delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:@"Settings", nil];
        [alert show];
    }
    else if(![self isRegisteredForRemoteNotifications]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please, enable notifications" delegate:nil cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:nil];
        [alert show];
    }
    
}

-(BOOL)canOpenSettings{
#ifdef __IPHONE_8_0
    return &UIApplicationOpenSettingsURLString != NULL;
#endif
    return NO;
}

-(BOOL)isRegisteredForRemoteNotifications{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        return     [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
    }
    else{
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types == UIRemoteNotificationTypeNone)
            return NO;
        else
            return YES;
    }
}

-(void)openDeviceSettings{
#ifdef __IPHONE_8_0
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
#endif
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex){
        [self openDeviceSettings];
    }
}


-(void)openVerifyPhoneScreen{
    
    VerifyPhoneViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyPhoneViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(void)openPhoneVerificationDialog{
    
    UIView *dialogView = [[UIView alloc] initWithFrame:CGRectMake(_mainContainer.frame.origin.x+10,
                                                                  _mainContainer.frame.origin.y,
                                                                  _mainContainer.frame.size.width-20,80)];
    [dialogView setTag:101];
    dialogView.backgroundColor = [UIColor whiteColor];
    CALayer *l = [dialogView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10];
    
    UILabel *dialogTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, dialogView.frame.size.width-20, 20)];
    dialogTitle.textAlignment = NSTextAlignmentLeft;
    dialogTitle.textColor = _goLoginButton.backgroundColor;
    dialogTitle.text =[[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_phoneMessage"];
    [dialogView addSubview:dialogTitle];
    
    
    UIButton *resendButton = [[UIButton alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(dialogTitle.frame)+10,60,30)];
    [resendButton setTitle:[[Language sharedInstance] stringWithKey:@"HomeUnconfirmedEmailAndPhone_Verify"] forState:UIControlStateNormal];
    [resendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [resendButton setBackgroundColor:_goLoginButton.backgroundColor];
    CALayer *l3 = [resendButton layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:5];
    [resendButton addTarget:self action:@selector(openVerifyPhoneScreen) forControlEvents:UIControlEventTouchUpInside];
    [dialogView addSubview:resendButton];
    
    
    UIButton *helpButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(resendButton.frame)+30,
                                                                      CGRectGetMaxY(dialogTitle.frame)+10,30,30)];
    [helpButton addTarget:self action:@selector(openPhoneVerificationHelp:) forControlEvents:UIControlEventTouchUpInside];
    [helpButton setTitle:@"?" forState:UIControlStateNormal];
    [helpButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [helpButton setBackgroundColor:[UIColor whiteColor]];
    [helpButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    CALayer *l1 = [helpButton layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:15];
    [l1 setBorderWidth:2.0];
    [l1 setBorderColor:[[UIColor blackColor] CGColor]];
    [dialogView addSubview:helpButton];
    
    
    _mainContainer.frame = CGRectMake(_mainContainer.frame.origin.x,
                                      _mainContainer.frame.origin.y+dialogView.frame.size.height+5,
                                      _mainContainer.frame.size.width,
                                      _mainContainer.frame.size.height-dialogView.frame.size.height-5);
    
    [self.view insertSubview:dialogView belowSubview:_filterButton];
}
-(void)openPhoneVerificationHelp:(UIButton*)sender{
    [KxMenu showMenuInView:self.view
                  fromRect:[sender.superview convertRect:sender.frame toView:nil]
                 menuItems:@[[KxMenuItem menuItem:[[Language sharedInstance]stringWithKey:@"HomeUnconfirmedEmailAndPhone_phoneWhy_one"] image:nil target:nil action:nil],
                             [KxMenuItem menuItem:[[Language sharedInstance]stringWithKey:@"HomeUnconfirmedEmailAndPhone_phoneWhy_two"] image:nil target:nil action:nil]]];
}

-(void)closePhoneVerificationDialog{
    for (UIView* view in self.view.subviews) {
        if (view.tag == 101) {
            _mainContainer.frame = mainContainerFrame;
            [_mainContainer.subviews[0] setFrame:_mainContainer.bounds];
            [view removeFromSuperview];
        }
    }
}

- (void) displayController: (UIViewController*) content;
{
    [self addChildViewController:content];
    content.view.frame = self.mainContainer.bounds;
    [self.mainContainer addSubview:content.view];
    [content didMoveToParentViewController:self];
}

- (void) hideController: (UIViewController*) content
{
    [content willMoveToParentViewController:nil];
    [content.view removeFromSuperview];
    [content removeFromParentViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)latest_Action:(id)sender {
    [_segments setSelectedSegmentIndex:0];
    isNeedDialog = NO ;
    _filterButton.hidden = NO;
    [_filterButton setImage:[UIImage imageNamed:@"new_filter"] forState:UIControlStateNormal];
    [_latest setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"] forState:UIControlStateNormal];
    [_top10 setBackgroundImage:nil forState:UIControlStateNormal];
    [_wall setBackgroundImage:nil forState:UIControlStateNormal];
    [_myPics setBackgroundImage:nil forState:UIControlStateNormal];
    [[_mainContainer subviews]  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _needLoginView.hidden = YES;
    latestPosts = [self.storyboard instantiateViewControllerWithIdentifier:@"PostsViewController"];
    latestPosts.delegate = self;
    latestPosts.requestType = 2;
    currentContent = latest;
    [self displayController:latestPosts];
}

- (IBAction)top10_Action:(id)sender {
    [_segments setSelectedSegmentIndex:1];
    _filterButton.hidden = YES;
    _needLoginView.hidden = YES;
    [_latest setBackgroundImage:nil forState:UIControlStateNormal];
    [_top10 setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"] forState:UIControlStateNormal];
    [_wall setBackgroundImage:nil forState:UIControlStateNormal];
    [_myPics setBackgroundImage:nil forState:UIControlStateNormal];
    [[_mainContainer subviews]  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self displayController:topTen];
}

- (IBAction)wall_Action:(id)sender {
    [_segments setSelectedSegmentIndex:2];
    isNeedDialog = YES ;
    _filterButton.hidden = YES;
    [_latest setBackgroundImage:nil forState:UIControlStateNormal];
    [_top10 setBackgroundImage:nil forState:UIControlStateNormal];
    [_wall setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"] forState:UIControlStateNormal];
    [_myPics setBackgroundImage:nil forState:UIControlStateNormal];
    [[_mainContainer subviews]  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if ([[CurrentUser getObject] isUser]) {
        _filterButton.hidden = NO;
        [_filterButton setImage:[UIImage imageNamed:@"new_filter"] forState:UIControlStateNormal];
        _needLoginView.hidden = YES;
        wallPosts   = [self.storyboard instantiateViewControllerWithIdentifier:@"PostsViewController"];
        wallPosts.delegate = self;
        wallPosts.requestType = 1;
        currentContent = wall;
        [self displayController:wallPosts];
        
    }
    else{
        [self setNeedsLogin];
    }
}
-(void)setNeedsLogin{
    _needLoginLabel.text = [[Language sharedInstance ] stringWithKey:@"HomeInitialState_WallLoginMessage"];
    _needLoginView.hidden = NO;
}
- (IBAction)myPics_Action:(id)sender {
    [_segments setSelectedSegmentIndex:3];
    _filterButton.hidden = YES;
    [_latest setBackgroundImage:nil forState:UIControlStateNormal];
    [_top10 setBackgroundImage:nil forState:UIControlStateNormal];
    [_wall setBackgroundImage:nil forState:UIControlStateNormal];
    [_myPics setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"] forState:UIControlStateNormal];
    [[_mainContainer subviews]  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if ([[CurrentUser getObject] isUser]) {
        _needLoginView.hidden = YES;
        myPicsPosts = [self.storyboard instantiateViewControllerWithIdentifier:@"PostsViewController"];
        myPicsPosts.delegate = self;
        myPicsPosts.requestType = 3;
        currentContent = myPics;
        [self displayController:myPicsPosts];
        
    }
    else{
        [self setNeedsLogin];
    }
}
-(void)logOut{
    
    [GetAndPushCashedData cashObject:@"NO" withAction:@"LoggedWithSocialMedia"];
    [GetAndPushCashedData removeObjectWithAction:@"likeNotification"];
    [GetAndPushCashedData removeObjectWithAction:@"commentNotification"];
    [GetAndPushCashedData removeObjectWithAction:@"favNotification"];
    [GetAndPushCashedData removeObjectWithAction:@"followNotification"];
    [GetAndPushCashedData removeObjectWithAction:@"chatNotification"];
    [GetAndPushCashedData removeObjectWithAction:@"requestNotification"];
    [GetAndPushCashedData removeObjectWithAction:@"recentMsgs"];

    
    [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setObject:nil forKey:@"Notifications"];
    [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"]synchronize];
    [[CurrentUser getObject] removeUser];
    [[CurrentUser getObject] setUser:nil];
    [self performSelector:@selector(latest_Action:) withObject:nil];
    [self.segments setSelectedSegmentIndex:0];
    [self closeEmailVerificationDialog];
     NSUserDefaults *defaults= [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    [defaults setBool:NO forKey:@"inviteOpened"];
    [defaults setBool:NO forKey:@"tipOpened"];
    [defaults removeObjectForKey:@"TwitterToken"];
    [defaults synchronize];
    FBSession *session=[FBSession activeSession];
    [session closeAndClearTokenInformation];
    [session close];
    [[FBSession activeSession] closeAndClearTokenInformation];
    [[FBSession activeSession] close];
    [FBSession setActiveSession:nil];
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
    [self closeEmailVerificationDialog];
    [self closePhoneVerificationDialog];
    BOOL isDeleted = [[FMDBHelper new] deleteAllMessage];
    if (isDeleted) {
        NSLog(@"Deleted");
    }
    else
    {
        NSLog(@"Not Deleted");
    }
    BOOL isDeleted1 = [[FMDBHelper new] deleteAllFriends];
    if (isDeleted1) {
        NSLog(@"Deleted");
    }
    else
    {
        NSLog(@"Not Deleted");
    }
    BOOL isDeleted11 = [[FMDBHelper new] deleteAllFans];
    if (isDeleted11) {
        NSLog(@"Deleted");
    }
    else
    {
        NSLog(@"Not Deleted");
    }
    
    //////////
//    NSUserDefaults * defs = [[NSUserDefaults alloc] initWithSuiteName:@"FCB_Data"];
//    NSDictionary * dict = [defs dictionaryRepresentation];
//    for (id key in dict) {
//        [defs removeObjectForKey:key];
//    }
//    [defs synchronize];
    [self deleteAllObjects:@"Message"];
    [self deleteAllObjects:@"Contact"];
    
    NSUserDefaults* lastTimeStampChecked = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    [lastTimeStampChecked removeObjectForKey:@"lastTimeStampChecked"];
    [lastTimeStampChecked synchronize];
    
}
- (void) deleteAllObjects: (NSString *) entityDescription  {
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];

    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
    }
    if (![context save:&error]) {
    }
    
}
- (void)openInfo{
    InfoViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    
}

- (IBAction)filter_Action:(id)sender {
}

-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)segmentChanged:(id)sender {
    displayFavorate=NO;
    filterOn = NO;
    [countriesList removeAllObjects];
    
    [UIView animateWithDuration:.5f animations:^{
        _buttonBar.center = barCenter;
    }];
    switch (_segments.selectedSegmentIndex) {
        case 0:
            [self latest_Action:nil];
            break;
        case 1:
            [self top10_Action:nil];
            break;
        case 2:
            [self wall_Action:nil];
            break;
        case 3:
            [self myPics_Action:nil];
            break;
        default:
            break;
    }
}

- (IBAction)openMenu:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openMenu" object:nil];
}

- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
}

- (void) openHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void) openStudio
{
    NSUserDefaults *defaults= [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    [defaults setBool:YES forKey:@"tipOpened"];
    [defaults synchronize];
    [_tipView setHidden:YES];
    
    StudioV2ViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"StudioV2ViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    
}
- (void) openFans
{
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    FansListView *viewController = (FansListView *)[sb instantiateViewControllerWithIdentifier:@"FansListView"];
    
    //    FansViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FansViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}
- (void) openSponsors
{
    
    SopnsorsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SopnsorsViewController"];
    
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}
- (void) openFriends
{
    if ([[CurrentUser getObject] isUser]) {
        UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FriendsView *viewController = (FriendsView *)[sb instantiateViewControllerWithIdentifier:@"FriendsView"];
        
        BOOL controllerExist = NO;
        for (UIViewController* controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[viewController class]])
            {
                controllerExist = YES;
                [self.navigationController popToViewController:controller animated:YES];
                
            }
        }
        if (!controllerExist) {
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
}

- (void) openNotifications
{
    if ([[CurrentUser getObject] isUser]) {
        UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
        NotificationViewController *viewController = [sb instantiateViewControllerWithIdentifier:@"NotificationViewController"];
        BOOL controllerExist = NO;
        
        for (UIViewController* controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[viewController class]])
            {
                controllerExist = YES;
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
        if (!controllerExist) {
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
}

-(void)didScrollToDirection:(ScrollDirection)direction{
    if (direction==ScrollDirectionDown) {
        [_tipView setHidden:YES];
        
        if (_buttonBar.frame.origin.y <= [UIScreen mainScreen].bounds.size.height) {
            //[_buttonBar setCenter:CGPointMake(_buttonBar.center.x, _buttonBar.center.y+3)];
            
            [UIView animateWithDuration:.5f animations:^{
                [_buttonBar setCenter:CGPointMake(_buttonBar.center.x, _buttonBar.center.y+(_buttonBar.frame.size.height/2))];
            }];
        }
        
    }
    else if (direction==ScrollDirectionUp){
        if (_buttonBar.center.y > barCenter.y) {
            //[_buttonBar setCenter:CGPointMake(_buttonBar.center.x, _buttonBar.center.y-3)];
            [UIView animateWithDuration:.5f animations:^{
                _buttonBar.center = barCenter;
            }];
        }
        
    }  else if (direction==ScrollDirectionCrazy){
        _buttonBar.center = barCenter;
    }
}

- (void)didLoginSuccessfully:(CLPlacemark*)place{
    
    if (place) {
        User *user = [[CurrentUser getObject] getUser];
        [user setLatitude:[NSString stringWithFormat:@"%f",place.location.coordinate.latitude]];
        [user setLongitude:[NSString stringWithFormat:@"%f",place.location.coordinate.longitude]];
        [user setCountryName:place.country];
        [user setCityName:place.administrativeArea];
        [user setDistrict:place.locality];
        
        if([RestServiceAgent internetAvailable])
        {
            [[UsersManager getInstance] updateUserProfile:user witImage:nil and:^(id currentClient) {
                if([currentClient isKindOfClass:[User class]])
                {
                    [[CurrentUser getObject] setUser:(User*)currentClient];
                    [[CurrentUser getObject] saveCurrentUser];
                }
            }onFailure:^(NSError *error) {
            }];
        }
    }
    [self loginDone];
}

- (IBAction)filterButton_Action:(id)sender {
    if ([[CurrentUser getObject] isUser]) {
        
        if (!filterOn) {
            if (isNeedDialog) {
                [self openFilterDialogForWall];
                filterForLatest = NO;
            }
            else{
                [self openFilterDialogForLatest];
                filterForLatest = YES ;
            }
        }
        else{
            [_filterButton setImage:[UIImage imageNamed:@"new_filter"] forState:UIControlStateNormal];
            
            switch (currentContent) {
                case wall:
                    [wallPosts applyFilerWithIsFavorate:NO andCountries:nil];
                    break;
                case latest:
                    [latestPosts applyFilerWithIsFavorate:NO andCountries:nil];
                    break;
                default:
                    break;
            }
            displayFavorate=NO;
            filterOn = NO;
            [countriesList removeAllObjects];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
}
-(void)openFilterDialogForWall{
    dialogIsOpend = YES ;
    
    UIView *bigView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    bigView.tag = 102;
    bigView.backgroundColor = [UIColor clearColor];
    UIView *maskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    maskView.backgroundColor = [UIColor darkGrayColor];
    maskView.alpha = .75;
    [bigView addSubview:maskView];
    
    
    UIView *dialogView = [[UIView alloc] initWithFrame:CGRectMake(20, 100, [UIScreen mainScreen].bounds.size.width-40,200)];
    dialogView.backgroundColor = [UIColor whiteColor];
    
    UILabel *dialogTitle=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, dialogView.frame.size.width, 35)];
    dialogTitle.textAlignment = NSTextAlignmentCenter;
    dialogTitle.backgroundColor = [UIColor lightGrayColor];
    dialogTitle.textColor = [UIColor blackColor];
    dialogTitle.text = [[Language sharedInstance] stringWithKey:@"Filter1_title"];
    CALayer *l = [dialogView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10];
    [dialogView addSubview:dialogTitle];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 25, 25)];
    //[closeButton setTitle:@"X" forState:UIControlStateNormal];
    [closeButton setImage:[UIImage imageNamed:@"popup-close.png"] forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeDialog) forControlEvents:UIControlEventTouchUpInside];
    [dialogView addSubview:closeButton];
    [dialogView bringSubviewToFront:closeButton];
    
    UIButton *applyButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0,100,30)];
    [applyButton setTitle:[[Language sharedInstance] stringWithKey:@"Filter1_Apply"] forState:UIControlStateNormal];
    [applyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [applyButton setBackgroundColor:_goLoginButton.backgroundColor];
    CALayer *l2 = [applyButton layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    applyButton.center = CGPointMake(dialogView.frame.size.width/2, dialogView.frame.size.height-20);
    [applyButton addTarget:self action:@selector(applyFilter) forControlEvents:UIControlEventTouchUpInside];
    [dialogView addSubview:applyButton];
    
    
    UIButton *checkBox = [[UIButton alloc] initWithFrame:CGRectMake(10, 67, 20, 20)];
    [checkBox setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
    [checkBox addTarget:self action:@selector(setDisplayFavorate:) forControlEvents:UIControlEventTouchUpInside];
    
    if (!displayFavorate) {
        [checkBox setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
    }else{
        [checkBox setBackgroundImage:[UIImage imageNamed:@"popup_checkbox_sc.png"] forState:UIControlStateNormal];
    }
    
    [dialogView addSubview:checkBox];
    
    UILabel *checkBoxTitle=[[UILabel alloc] initWithFrame:CGRectMake(40,50,dialogView.frame.size.width-50,70)];
    checkBoxTitle.numberOfLines = 0 ;
    checkBoxTitle.textAlignment = NSTextAlignmentLeft;
    checkBoxTitle.textColor = [UIColor blackColor];
    checkBoxTitle.text = [[Language sharedInstance] stringWithKey:@"Filter1_checkbox1"];
    [checkBoxTitle setFont:[UIFont systemFontOfSize:14]];

    [dialogView addSubview:checkBoxTitle];
    
    
    UIButton *countryButton = [[UIButton alloc] initWithFrame:CGRectMake(40,CGRectGetMaxY(checkBoxTitle.frame),120, 30)];
    [countryButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [countryButton setTitle:[[Language sharedInstance] stringWithKey:@"Filter1_title2"] forState:UIControlStateNormal];
    [countryButton setTitleColor:_goLoginButton.backgroundColor forState:UIControlStateNormal];
    [countryButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [countryButton addTarget:self action:@selector(openCountriesController) forControlEvents:UIControlEventTouchUpInside];
    [dialogView addSubview:countryButton];

    selectedCountries=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(countryButton.frame)+10,
                                                                CGRectGetMaxY(checkBoxTitle.frame),
                                                                70,30)];
    selectedCountries.font = [UIFont systemFontOfSize:14.f];
    selectedCountries.numberOfLines = 0 ;
    selectedCountries.textAlignment = NSTextAlignmentCenter;
    selectedCountries.textColor = [UIColor blackColor];
    selectedCountries.text = @">";
    [dialogView addSubview:selectedCountries];
    
    [bigView addSubview:dialogView];
    [self.view addSubview:bigView];
}

-(void)openFilterDialogForLatest{
    dialogIsOpend = YES ;
    
    UIView *bigView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    bigView.tag = 102;
    bigView.backgroundColor = [UIColor clearColor];
    UIView *maskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    maskView.backgroundColor = [UIColor darkGrayColor];
    maskView.alpha = .75;
    [bigView addSubview:maskView];
    
    
    UIView *dialogView = [[UIView alloc] initWithFrame:CGRectMake(20, 100, [UIScreen mainScreen].bounds.size.width-40,200)];
    dialogView.backgroundColor = [UIColor whiteColor];
    
    UILabel *dialogTitle=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, dialogView.frame.size.width, 35)];
    dialogTitle.textAlignment = NSTextAlignmentCenter;
    dialogTitle.backgroundColor = [UIColor lightGrayColor];
    dialogTitle.textColor = [UIColor blackColor];
    dialogTitle.text = [[Language sharedInstance]stringWithKey:@"Filter1_title"];
    CALayer *l = [dialogView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10];
    [dialogView addSubview:dialogTitle];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 25, 25)];
    [closeButton setImage:[UIImage imageNamed:@"popup-close.png"] forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeDialog) forControlEvents:UIControlEventTouchUpInside];
    [dialogView addSubview:closeButton];
    [dialogView bringSubviewToFront:closeButton];
    
    UIButton *applyButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0,100,30)];
    [applyButton setTitle:[[Language sharedInstance] stringWithKey:@"Filter1_Apply"] forState:UIControlStateNormal];
    [applyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [applyButton setBackgroundColor:_goLoginButton.backgroundColor];
    CALayer *l2 = [applyButton layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    applyButton.center = CGPointMake(dialogView.frame.size.width/2, dialogView.frame.size.height-20);
    [applyButton addTarget:self action:@selector(applyFilter) forControlEvents:UIControlEventTouchUpInside];
    [dialogView addSubview:applyButton];
    
    
    
    
    UIButton *countryButton = [[UIButton alloc] initWithFrame:CGRectMake(40,100,120, 30)];
    [countryButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [countryButton setTitle:[[Language sharedInstance] stringWithKey:@"Filter1_title2"] forState:UIControlStateNormal];
    [countryButton setTitleColor:_goLoginButton.backgroundColor forState:UIControlStateNormal];
    [countryButton addTarget:self action:@selector(openCountriesController) forControlEvents:UIControlEventTouchUpInside];
    [countryButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [dialogView addSubview:countryButton];
    
    selectedCountries=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(countryButton.frame)+10,
                                                                100,
                                                                70,30)];
    selectedCountries.font = [UIFont systemFontOfSize:14.f];
    selectedCountries.numberOfLines = 0 ;
    selectedCountries.textAlignment = NSTextAlignmentCenter;
    selectedCountries.textColor = [UIColor blackColor];
    [dialogView addSubview:selectedCountries];
    
    [bigView addSubview:dialogView];
    [self.view addSubview:bigView];
}
-(void)openCountriesController{
    CitiesFilterViewController *citiesFilter;
    citiesFilter  = [self.storyboard instantiateViewControllerWithIdentifier:@"CitiesFilterViewController"];
    citiesFilter.delegate = self;
    [self.navigationController pushViewController:citiesFilter animated:YES];
    
}
-(void)setDisplayFavorate:(UIButton*)sender{
    if (displayFavorate) {
        [sender setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
        displayFavorate = NO;
    }else{
        [sender setBackgroundImage:[UIImage imageNamed:@"popup_checkbox_sc.png"] forState:UIControlStateNormal];
        displayFavorate = YES;
    }
}
-(void)closeDialog{
    dialogIsOpend = NO ;
    for (UIView *view in self.view.subviews) {
        if ([view tag]==102) {
            [view removeFromSuperview];
        }
    }
}

-(void)applyFilter{
    if (!displayFavorate && [countriesList count]<=0) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"no_Filter"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else{
        [self closeDialog];
        switch (currentContent) {
            case wall:
                [wallPosts applyFilerWithIsFavorate:displayFavorate andCountries:countriesList];
                break;
            case latest:
                [latestPosts applyFilerWithIsFavorate:displayFavorate andCountries:countriesList];
                break;
            default:
                break;
        }
        [_filterButton setImage:[UIImage imageNamed:@"new_filter_sc"] forState:UIControlStateNormal];
        filterOn = YES;
    }
}

- (void)didSelectCountries:(NSMutableArray*)array{
    if (array) {
        countriesList = [[NSMutableArray alloc] initWithArray:array];
        selectedCountries.text = [NSString stringWithFormat:@"%d %@>",[countriesList count],[[Language sharedInstance] stringWithKey:@"Filter1_country"]];
    }
    [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
}


-(void)animationStopped{
    _buttonBar.center = barCenter;
}

-(void)openProfile{
    ViewProfileViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewProfileViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}
-(void)openSettings{
    SettingsViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    
}
-(void)openChangePassword{
    ChangePasswordViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}
-(void)openPrivacySettings{
    
    PrivacyViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}
-(void)openContactUs{
    
    ContactUsViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}

-(void)openPrivacyAgreement{
    
    PrivacyAgreementViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyAgreementViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}

-(void)openAboutFCBStudio{
    
    AboutFCBStudioViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutFCBStudioViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}

-(void)openAboutFCBarcelona{
    
    AboutFCBarcelonaViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutFCBarcelonaViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)openAboutTawasolit{
    
    AboutTawasolViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutTawasolViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}

-(void)openHelp{
    
    HelpViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}

-(void)openShop{
    if ([[CurrentUser getObject] isUser]) {
        ShopViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopViewController"];
        BOOL controllerExist = NO;
        for (UIViewController* controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[viewController class]])
            {
                controllerExist = YES;
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
        if (!controllerExist) {
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }else{
        [self openLoginScreen];
    }
}

-(void)openChat{
    ChatMainViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatMainViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
            
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void) openContest{
    ContestViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContestViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)openBlockedUsers{
    BlockedUsersViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BlockedUsersViewController"];
    BOOL controllerExist = NO;
    for (UIViewController* controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[viewController class]])
        {
            controllerExist = YES;
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    if (!controllerExist) {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)emailVerified{
    [[CurrentUser getObject] getUser].emailVerified = YES;
    [[CurrentUser getObject] saveCurrentUser];
    [self closeEmailVerificationDialog];
}

-(void)phoneVerified{
    [[CurrentUser getObject] getUser].phoneVerified = YES;
    [[CurrentUser getObject] saveCurrentUser];
    [self closePhoneVerificationDialog];
}

- (IBAction)shop_Action:(id)sender{
    [self openShop];
}

- (IBAction)chat_Action:(id)sender{
    if([[CurrentUser getObject] isUser])
    {
        [self openChat];
    }
    else
    {
        [self openLoginScreen];
    }
}

-(void)setLocalisables{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"app_name"]];
    [_tipLabel setText:[[Language sharedInstance] stringWithKey:@"HomeInitialState_CameraMessage"]];
    [_needLoginLabel setText:[[Language sharedInstance] stringWithKey:@"HomeInitialState_WallLoginMessage"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_goLoginButton setTitle:[[Language sharedInstance] stringWithKey:@"HomeInitialState_WallLogin"] forState:UIControlStateNormal];
    
    
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Home_tab1"] forSegmentAtIndex:0];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Home_tab2"] forSegmentAtIndex:1];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Home_tab3"] forSegmentAtIndex:2];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Home_tab4"] forSegmentAtIndex:3];
    [self adjustSegments];
}

-(void)adjustSegments{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableArray *stringsWidths =[[NSMutableArray alloc] init] ;
        [stringsWidths addObject:@([self widthForString:[[Language sharedInstance] stringWithKey:@"Home_tab1"]])];
        [stringsWidths addObject:@([self widthForString:[[Language sharedInstance] stringWithKey:@"Home_tab2"]])];
        [stringsWidths addObject:@([self widthForString:[[Language sharedInstance] stringWithKey:@"Home_tab3"]])];
        [stringsWidths addObject:@([self widthForString:[[Language sharedInstance] stringWithKey:@"Home_tab4"]])];
        
        float sementsWidth = 0.0;
        for (int i=0; i <_segments.numberOfSegments; i++) {
            sementsWidth += [stringsWidths[i] floatValue]+10;
        }
        
        if ([Language sharedInstance].currentLanguage == English || [Language sharedInstance].currentLanguage == Arabic )
        {
            for (int i=0; i <_segments.numberOfSegments; i++) {
                [_segments setWidth:[UIScreen mainScreen].bounds.size.width/4 forSegmentAtIndex:i];
            }
            _segments.frame = CGRectMake(_segmentsView.bounds.origin.x,
                                         _segmentsView.bounds.origin.y,
                                         _segmentsView.frame.size.width,
                                         _segmentsView.frame.size.height);
            
            _segmentsViewTap.frame = CGRectMake(_segmentsView.bounds.origin.x,
                                                _segmentsView.bounds.origin.y,
                                                _segmentsView.frame.size.width,
                                                _segmentsView.frame.size.height);
            
            [_segmentsView setContentSize:CGSizeMake(0,0)];
            
        }
        else{
            for (int i=0; i <_segments.numberOfSegments; i++) {
                [_segments setWidth:[stringsWidths[i] floatValue]+10 forSegmentAtIndex:i];
            }
            _segments.frame = CGRectMake(_segments.frame.origin.x,
                                         _segments.frame.origin.y,
                                         sementsWidth+30,
                                         _segments.frame.size.height);
            
            _segmentsViewTap.frame = CGRectMake(_segmentsViewTap.frame.origin.x,
                                                _segmentsViewTap.frame.origin.y,
                                                sementsWidth+30,
                                                _segmentsViewTap.frame.size.height);
            
            [_segmentsView setContentSize:CGSizeMake(sementsWidth+30,50)];
        }
        
    });
}

-(CGFloat)widthForString:(NSString*)string{
    UIFont *myFont = [UIFont systemFontOfSize:17.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:myFont, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

- (void)openMessaging:(NSNotification *) notification
{
    
    MessagingViewController * messagingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagingViewController"];
    messagingViewController.profile = [Contact contactFromFan:(Fan*)notification.object];
    [Contact chatWithUser:(Fan*)notification.object];
    [self.navigationController pushViewController:messagingViewController animated:YES];
}



-(void) getBlockedUsersNCompareWith:(NSNotification *) notification{
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[UsersManager getInstance] getBlockedUsers:[[CurrentUser getObject] getUser].userId
                                                    and:^(id currentClient)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     BOOL isFound = YES;
                     NSMutableArray * blockedUsersArray;
                     if (currentClient) {
                         blockedUsersArray = [NSMutableArray array]
                         ;
                         [blockedUsersArray addObjectsFromArray:currentClient[@"data"]];
                         if ([blockedUsersArray count]) {
                             
                             for (NSDictionary * dic in blockedUsersArray) {
                                 if ([[dic objectForKey:@"fanId"] intValue] == [notification.object intValue]) {
                                     BlockedUsersViewController * blockedUsersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BlockedUsersViewController"];
                                     [self.navigationController pushViewController:blockedUsersViewController animated:YES];
                                     isFound = YES;
                                     break;
                                 }
                                 isFound = NO;
                             }
                         }
                         if (!isFound || ![blockedUsersArray count]) {
                             
                             UIStoryboard * sb = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ?
                             [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] :
                             [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
                             FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
                             fansDetailView.fanID = [notification.object intValue];
                             [self.navigationController pushViewController:fansDetailView animated:YES];
                         }
                     }
                 });
             }onFailure:^(NSError *error) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                     [alert show];
                 });
             }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

@end
