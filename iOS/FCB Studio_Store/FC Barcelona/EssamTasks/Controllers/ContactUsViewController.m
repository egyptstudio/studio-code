//
//  ContactUsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/18/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "ContactUsViewController.h"
#import "UsersManager.h"
#import "RestServiceAgent.h"
#import "SVProgressHUD.h"
#import "CurrentUser.h"

@interface ContactUsViewController ()<UITextFieldDelegate,UITextViewDelegate>

@end

@implementation ContactUsViewController
{
    NSString* messagePlaceHolder;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.email.delegate = self;
    self.messagetitle.delegate = self;
    self.messageBody.delegate = self;
    
    
    
    CALayer * l = [self.sendButton layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    CALayer * l1 = [self.emailView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    
    CALayer * l2 = [self.titleView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    
    CALayer * l3 = [self.mesageView layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:5];
    
    if ([[CurrentUser getObject] isUser]) {
        [_emailView setHidden:YES];
        [_email setText:[[CurrentUser getObject] getUser].email];
    }
    messagePlaceHolder = [[Language sharedInstance] stringWithKey:@"ContactUs_MessageBody"];
    _messageBody.text = messagePlaceHolder;
    
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"settings_contact_us"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_sendButton setTitle:[[Language sharedInstance] stringWithKey:@"MessagesWindow_Send"] forState:UIControlStateNormal];

    self.email.placeholder = [[Language sharedInstance] stringWithKey:@"Registration_EmailDefaultValue"] ;
    self.messagetitle.placeholder = [[Language sharedInstance] stringWithKey:@"ContactUs_MessageTitle"] ;
    
    
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:messagePlaceHolder]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = messagePlaceHolder;
        textView.textColor = [UIColor lightGrayColor]; 
    }
    [textView resignFirstResponder];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)send_Action:(id)sender
{
    @try {
        if (![[CurrentUser getObject] isUser]&&[[self.email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Login_emailMissingAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        
        else if ([[self.messagetitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"ContactUs_MessageTitle"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if ([[self.messageBody.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"ContactUs_MessageBody"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            if([RestServiceAgent internetAvailable])
            {
                [[UsersManager getInstance] sendContactUs:_email.text messageTitle:_messagetitle.text messageText:_messageBody.text and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            [SVProgressHUD dismiss];
                            if([[currentClient objectForKey:@"status"] intValue] == 1)
                            {
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                message:[[Language sharedInstance] stringWithKey:@"ContactUs_OnSend"]
                                                                               delegate:nil
                                                                      cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                                _email.text = @"";
                                _messagetitle.text = @"";
                                _messageBody.text  = messagePlaceHolder;
                                _messageBody.textColor = [UIColor lightGrayColor];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else
            {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
    
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}

@end
