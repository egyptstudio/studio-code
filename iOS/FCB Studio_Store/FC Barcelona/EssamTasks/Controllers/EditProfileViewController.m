//
//  EditProfileViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/13/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "EditProfileViewController.h"
#import "User.h"
#import "UsersManager.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "KxMenu.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "FCBPhotosViewController.h"
#import "PECropView.h"
#import "WebImage.h"
#import "CurrentUser.h"
#import "StudioManager.h"
#import "CountryManager.h"
#import "City.h"
#import "MSPickerViewController.h"
#import "MyPicsViewController.h"
#import "EditProfile2ViewController.h"

@interface EditProfileViewController ()<FCBPhotosDelegate,UIActionSheetDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MSPickerViewControllerDelegate,MyPhotosDelegate>

@end
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."

@implementation EditProfileViewController
{
    BOOL sortedCodes;
    UIImagePickerController* cameraPicker;
    UIImage * userImage;
    PECropView *cropView;
    NSString* imageUrl;
    int selectedGender;
    int selectedCountry;
    int selectedCity;
    NSMutableArray* countriesArray;
    NSMutableArray* citiesArray;
    
    UIDatePicker *datePicker;
    UIPopoverController *popoverController;
    MSPickerViewController *pickerView;
    long selectedDate;
    User* user;
    
    __weak IBOutlet UITextField * countryCodeTxtField;
    UIPickerView * countryCodePickerView;
    UIToolbar *toolBar;
    NSString * defaultCountryCode;
    NSMutableArray* countryCodeArray;
}
@synthesize fromPoints;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    countriesArray  = [[NSMutableArray alloc] init];
    citiesArray     = [[NSMutableArray alloc] init];
    _phoneField.keyboardType = UIKeyboardTypeNumberPad;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatePicker)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    [_dateOfBirthPickerView addGestureRecognizer:tap];
    _dateOfBirthPickerView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCountriesPicker)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    [_countryView addGestureRecognizer:tap1];
    _countryView .userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCitiesPicker)];
    tap2.cancelsTouchesInView = YES;
    tap2.numberOfTapsRequired = 1;
    [_cityView addGestureRecognizer:tap2];
    _cityView .userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCountriesCodesPicker)];
    tap3.cancelsTouchesInView = YES;
    tap3.numberOfTapsRequired = 1;
    [_countryCodeView addGestureRecognizer:tap3];
    
    _cityView .userInteractionEnabled = YES;

    
    for (UIView *view in _mainScroll.subviews) {
        [_mainScroll setContentSize:CGSizeMake(0, _mainScroll.contentSize.height + view.frame.size.height+4)];
    }
    
    self.userNameField.delegate = self;
    self.emailField.delegate = self;
    self.districtField.delegate = self;
    self.dateOfBirthField.delegate = self;
    self.phoneField.delegate = self;
    
    //CALayer *mask = [CALayer layer];
    //mask.contents = (id)[[UIImage imageNamed:@"photo_mask.png"] CGImage];
    //mask.frame = CGRectMake(0, 0, _profilePicView.frame.size.width, _profilePicView.frame.size.height);
    //_profilePicView.layer.mask = mask;
    //_profilePicView.layer.masksToBounds = YES;
    
    
    
    //    CALayer * l8 = [_profilePicView layer];
    //    [l8 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
    //    [l8 setBorderWidth:2.0];
    //    [l8 setMasksToBounds:YES];
    //    [l8 setCornerRadius:20.0];
    
    CALayer * l = [self.userNameView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:3];
    
    CALayer * l1 = [self.emailView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:3];
    
    CALayer * l2 = [self.phoneView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:3];
    
    CALayer * l3 = [self.districtView layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:3];
    
    CALayer * l4 = [self.dateOfBirthView layer];
    [l4 setMasksToBounds:YES];
    [l4 setCornerRadius:3];
    
    
    CALayer * l5 = [self.countryCodeView layer];
    [l5 setMasksToBounds:YES];
    [l5 setCornerRadius:3];
    
    [self initView];
    
    userImage = nil;
    
    [self setLocalisables];
}

-(void)initView
{
    
    selectedCountry =[[[CurrentUser getObject] getUser] countryId];
    selectedCity    = [[[CurrentUser getObject] getUser] cityId];
    selectedGender  = [[[CurrentUser getObject] getUser] gender] ;
    
    [_profileImage sd_setImageWithURL:[NSURL URLWithString:[[[CurrentUser getObject] getUser] profilePic]] placeholderImage:[UIImage imageNamed:@"avatar"]];
    
    _userNameField.text = [[[CurrentUser getObject] getUser] fullName];
    _districtField.text = [[[[CurrentUser getObject] getUser] district] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _emailField.text = [[[[CurrentUser getObject] getUser] email] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _phoneField.text = [[[[CurrentUser getObject] getUser] phone] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _countryCodeLabel.text = [[[[CurrentUser getObject] getUser] phoneCode] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    _countryLabel.text     = ![[[[[CurrentUser getObject] getUser] countryName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] ?
    [[[CurrentUser getObject] getUser] countryName]:[[Language sharedInstance] stringWithKey:@"Select"] ;
    
    _cityLabel.text        = [[[CurrentUser getObject] getUser] cityName] ?
    [[[CurrentUser getObject] getUser] cityName]:[[Language sharedInstance] stringWithKey:@"Select"] ;
    
    
    if ([[[CurrentUser getObject] getUser] dateOfBirth]!= 0) {
        NSDate* theDate = [NSDate dateWithTimeIntervalSince1970:[[[CurrentUser getObject] getUser] dateOfBirth]];
        _dateOfBirthField.text = [self relativeDateStringForDate:theDate];
        _dateOfBirthPickerLabel.text = [self relativeDateStringForDate:theDate];
        selectedDate = [[[CurrentUser getObject] getUser] dateOfBirth] ;
    }
    else{
        _dateOfBirthPickerLabel.text = [[Language sharedInstance] stringWithKey:@"Select"];
    }
    
    if ([[[CurrentUser getObject] getUser] gender]== 1) {
        [_femaleButton setImage:[UIImage imageNamed:@"popup_radio_btn.png"] forState:UIControlStateNormal];
        [_maleButton setImage:[UIImage imageNamed:@"popup_radio_btn_sc.png"] forState:UIControlStateNormal];
    }
    else if ([[[CurrentUser getObject] getUser] gender]== 2) {
        [_maleButton setImage:[UIImage imageNamed:@"popup_radio_btn.png"] forState:UIControlStateNormal];
        [_femaleButton setImage:[UIImage imageNamed:@"popup_radio_btn_sc.png"] forState:UIControlStateNormal];
    }
}


-(void)showCountriesPicker
{
    sortedCodes = NO;
    if ([countriesArray count]<1) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
        [[CountryManager getInstance] getCountriesListand:^(id currentClient) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [countriesArray addObjectsFromArray:currentClient];
                NSMutableArray *tmp = [[NSMutableArray alloc] init];
                for (Country *obj in currentClient) {
                    [tmp addObject:[obj countryName]];
                }
                pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"] forItems:tmp selectedItem:0];
                pickerView.delegate = self;
                pickerView.tag = 1;
                [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
                [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
                [pickerView showPickerViewInView:self.view];
            });
        } onFailure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }];
        
    }else{
        NSMutableArray *tmp = [[NSMutableArray alloc] init];
        for (Country *obj in countriesArray) {
            [tmp addObject:[obj countryName]];
        }
        pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"] forItems:tmp selectedItem:0];
        pickerView.delegate = self;
        pickerView.tag = 1;
        [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
        [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
        [pickerView showPickerViewInView:self.view];
        
    }
}

-(void)showCountriesCodesPicker
{
    sortedCodes = YES;
    if ([countriesArray count]<1) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        [[CountryManager getInstance] getCountriesListand:^(id currentClient) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [countriesArray addObjectsFromArray:currentClient];
                NSMutableArray *tmp = [[NSMutableArray alloc] init];
                for (Country *obj in currentClient) {
                    [tmp addObject:[obj phone_code]];
                }
                NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:tmp];
                tmp = [[orderedSet array] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                                forItems:tmp
                                                            selectedItem:0];
                pickerView.delegate = self;
                pickerView.tag = 4;
                [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
                [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
                [pickerView showPickerViewInView:self.view];
            });
        } onFailure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }];
        
    }else{
        NSMutableArray *tmp = [[NSMutableArray alloc] init];
        for (Country *obj in countriesArray) {
            [tmp addObject:[obj phone_code]];
        }
        
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:tmp];
        tmp = [[orderedSet array] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                        forItems:tmp
                                                    selectedItem:0];
        pickerView.delegate = self;
        pickerView.tag = 4;
        [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
        [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
        [pickerView showPickerViewInView:self.view];
        

        
    }
    
    
}

-(void)showCitiesPicker
{
    if (selectedCountry != 0) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
        [[CountryManager getInstance] getCitiesListForCountry:selectedCountry and:^(id currentClient) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                citiesArray = [NSMutableArray array];
                [citiesArray addObjectsFromArray:currentClient];
                NSMutableArray *tmp = [[NSMutableArray alloc] init];
                for (City *obj in currentClient) {
                    [tmp addObject:[obj cityName]];
                }
                
                
                selectedCity = [(City*)citiesArray[0] cityId];
                _cityLabel.text = [(City*)citiesArray[0] cityName];
                
                pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"] forItems:tmp selectedItem:0];
                
                pickerView.delegate = self;
                pickerView.tag = 2;
                [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
                [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
                [pickerView showPickerViewInView:self.view];
            });
        } onFailure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }];
        
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Filter1_title2"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
        
    }
    
    
}
-(void)addDefaultCity{

    if (selectedCountry != 0) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
        [[CountryManager getInstance] getCitiesListForCountry:selectedCountry and:^(id currentClient) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                citiesArray = [NSMutableArray array];
                [citiesArray addObjectsFromArray:currentClient];
                selectedCity = [(City*)citiesArray[0] cityId];
                _cityLabel.text = [(City*)citiesArray[0] cityName];
            });
        } onFailure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }];
        
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Filter1_title2"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
        
    }

    
}
- (void)pickerView:(MSPickerViewController *)PickerView didSelectItem:(int)row {
    if (pickerView.tag == 1)
    {
        NSString * prevCountry = _countryLabel.text;
        selectedCountry = [(Country*)countriesArray[row] countryId];
        _countryLabel.text = [(Country*)countriesArray[row] countryName];
        _countryCodeLabel.text = [(Country*)countriesArray[row] phone_code];
        if (![prevCountry isEqualToString:_countryLabel.text]) {
            _cityLabel.text = @"";
        }
        [self  addDefaultCity];
    }
    else if (pickerView.tag == 2)
    {
        selectedCity = [(City*)citiesArray[row] cityId];
        _cityLabel.text = [(City*)citiesArray[row] cityName];
    }else if (pickerView.tag == 4)
    {
        
        NSMutableArray *tmp = [[NSMutableArray alloc] init];
        for (Country *obj in countriesArray) {
            [tmp addObject:[obj phone_code]];
        }
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:tmp];
        tmp = [[orderedSet array] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        _countryCodeLabel.text = tmp[row];
    }
}

-(void)showDatePicker{
    pickerView = [MSPickerViewController datePickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]];
    pickerView.delegate = self;
    pickerView.tag = 3;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex != actionSheet.destructiveButtonIndex)
    {
        switch (actionSheet.tag) {
            case 1: {
                imageUrl = nil;
                switch (buttonIndex) {
                    case 0:
                        [self takePhoto_myPics:nil];
                        break;
                    case 1:
                        [self takePhoto_Cam:nil];
                        break;
                    case 2:
                        [self takePhoto_gal:nil];
                        break;
                    case 3:
                        [self takePhoto_fcb:nil];
                        break;
                    default:
                        break;
                }
                break;
            }
            default:
                break;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    
    if (textField==_userNameField) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return newLength <= MAXLENGTH || returnKey || [string isEqualToString:filtered];
    }
    return newLength <= MAXLENGTH || returnKey;
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addPhoto_Action:(id)sender {
    
    [self selectImageSource:sender];
}
-(void) selectImageSource:(UIButton*)sender
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:[[Language sharedInstance] stringWithKey:@"Select"] delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] destructiveButtonTitle:nil otherButtonTitles:
                            [[Language sharedInstance] stringWithKey:@"MyPictures"],
                            [[Language sharedInstance] stringWithKey:@"Camera"],
                            [[Language sharedInstance] stringWithKey:@"PhotoAlbum"],
                            [[Language sharedInstance] stringWithKey:@"FCBPhotos"],
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)takePhoto_Cam:(id)sender
{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"]
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        cameraPicker.delegate = self;
        cameraPicker.showsCameraControls = YES;
        cameraPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
        [self.view addSubview:cameraPicker.view];
    }
}

- (void)takePhoto_gal:(id)sender
{
    cameraPicker = [[UIImagePickerController alloc] init];
    cameraPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    cameraPicker.delegate = self;
    cameraPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
    [self.view addSubview:cameraPicker.view];
    
}
- (void)takePhoto_fcb:(id)sender
{
    FCBPhotosViewController * fcb = [self.storyboard instantiateViewControllerWithIdentifier:@"FCBPhotosViewController"];
    fcb.delegate = self ;
    [self.navigationController pushViewController:fcb animated:YES];
}
- (void)takePhoto_myPics:(id)sender
{
    MyPicsViewController * fcb = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPicsViewController"];
    fcb.delegate = self ;
    [self.navigationController pushViewController:fcb animated:YES];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [cameraPicker.view removeFromSuperview];
    [self openCropperWithImage:image];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [cameraPicker.view removeFromSuperview];
    
}

- (IBAction)moreButton_Action:(id)sender
{
    user = [[CurrentUser getObject] getUser];
    [user setFullName:_userNameField.text];
    [user setEmail:_emailField.text];
    [user setDistrict:_districtField.text];
    [user setPhone:_phoneField.text];
    [user setDateOfBirth:selectedDate];
    [user setCountryId:selectedCountry];
    [user setCountryName:selectedCountry==0?@"":_countryLabel.text];
    [user setCityId:selectedCity];
    [user setCityName:selectedCity==0?@"":_cityLabel.text];
    [user setGender:selectedGender];
    [user setProfilePic:imageUrl?imageUrl:user.profilePic];
    [user setPhoneCode:_countryCodeLabel.text];

    EditProfile2ViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfile2ViewController"];
    viewController.user = user;
    viewController.fromPoints = fromPoints;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)maleButton_Action:(id)sender
{
    selectedGender = 1;
    [_femaleButton setImage:[UIImage imageNamed:@"popup_radio_btn.png"] forState:UIControlStateNormal];
    [_maleButton setImage:[UIImage imageNamed:@"popup_radio_btn_sc.png"] forState:UIControlStateNormal];
    
}
- (IBAction)femaleButton_Action:(id)sender
{
    selectedGender = 2;
    [_maleButton setImage:[UIImage imageNamed:@"popup_radio_btn.png"] forState:UIControlStateNormal];
    [_femaleButton setImage:[UIImage imageNamed:@"popup_radio_btn_sc.png"] forState:UIControlStateNormal];
}

-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)didSelectImage:(UIImage *)image withUrl:(NSString *)url{
    imageUrl = url;
    _profileImage.image = image;
    userImage = nil;
}

-(void)openCropperWithImage:(UIImage*)image{
    cropView = [[PECropView alloc] initWithFrame:self.view.bounds];
    cropView.image = image;
    
    UIView* mask = [[UIView alloc] initWithFrame:self.view.bounds];
    [mask setBackgroundColor:[UIColor blackColor]];
    UIButton *close = [[UIButton alloc] initWithFrame:CGRectMake(10,20,70, 30)];
    [close setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [close setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [close setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [close addTarget:self action:@selector(closeCropper) forControlEvents:UIControlEventTouchUpInside];
    CALayer *l2 = [close layer];
    [l2 setMasksToBounds:YES];
    [l2 setBorderColor:[[UIColor whiteColor] CGColor]];
    [l2 setBorderWidth:2];
    [l2 setCornerRadius:5];
    
    
    [mask addSubview:cropView];
    [mask addSubview:close];
    [self.view addSubview:mask];
    
}
-(void)closeCropper{
    self.profileImage.image = cropView.croppedImage ;
    userImage = cropView.croppedImage;
    imageUrl = nil;
    [[self.view.subviews objectAtIndex:[self.view.subviews count]-1] removeFromSuperview];
}
- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd"];
    return [dateFormat stringFromDate:date];
    
}



- (void)pickerView:(MSPickerViewController *)pickerView didSelectDate:(NSDate*)date{
    _dateOfBirthPickerLabel.text = [self relativeDateStringForDate:date];
    selectedDate = [date timeIntervalSince1970];
}

- (void)pickerViewDidDismiss {
    pickerView = nil;
}

- (IBAction)saveButton_Action:(id)sender{
    user = [[CurrentUser getObject] getUser];
    [user setFullName:_userNameField.text];
    [user setEmail:_emailField.text];
    [user setDistrict:_districtField.text];
    [user setPhone:_phoneField.text];
    [user setDateOfBirth:selectedDate];
    [user setCountryId:selectedCountry];
    [user setCountryName:selectedCountry==0?@"":_countryLabel.text];
    [user setCityId:selectedCity];
    [user setCityName:selectedCity==0?@"":_cityLabel.text];
    [user setGender:selectedGender];
    [user setProfilePic:imageUrl?imageUrl:user.profilePic];
    [user setPhoneCode:_countryCodeLabel.text];


    @try {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if([RestServiceAgent internetAvailable])
        {
            [[UsersManager getInstance] updateUserProfile:user witImage:userImage and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (currentClient) {
                        [SVProgressHUD dismiss];
                        if([currentClient isKindOfClass:[User class]])
                        {
                            [[CurrentUser getObject] setUser:(User*)currentClient];
                            [[CurrentUser getObject] saveCurrentUser];
                            [self initView];
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"DoneEdit"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                            [alert show];
                            if (!fromPoints) {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
                            }
                        }
                        else{
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Server Error." delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                            [alert show];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
}
-(void)setLocalisables{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"EditProfile_title"]];
    [_addPhoto_Caption setText:[[Language sharedInstance] stringWithKey:@"EditMyPhoto_title"]];
    
    [_countryTitle setText:[[Language sharedInstance] stringWithKey:@"Filter1_country"]];
    [_cityTitle setText:[[Language sharedInstance] stringWithKey:@"MyProfile_City"]];
    [_birthDateTitle setText:[[Language sharedInstance] stringWithKey:@"MyProfile_Birthdate"]];
    
    
    self.userNameField.placeholder = [[Language sharedInstance] stringWithKey:@"chat_user_name"];
    self.emailField.placeholder = [[Language sharedInstance] stringWithKey:@"Registration_EmailDefaultValue"];
    self.districtField.placeholder = [[Language sharedInstance] stringWithKey:@"MyProfile_District"];
    self.dateOfBirthField.placeholder = [[Language sharedInstance] stringWithKey:@"MyProfile_Birthdate"];
    self.phoneField.placeholder = [[Language sharedInstance] stringWithKey:@"MyProfile_Phone"];
    
    [_maleButton setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_Male"] forState:UIControlStateNormal];
    [_femaleButton setTitle:[[Language sharedInstance] stringWithKey:@"FansFilter_Female"] forState:UIControlStateNormal];
    [_moreButton setTitle:[[Language sharedInstance] stringWithKey:@"user_profile_more"] forState:UIControlStateNormal];
}

@end
