//
//  ContestManager.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/7/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "ContestManager.h"

@implementation ContestManager

static ContestManager* instance;
+(ContestManager *)getInstance{
    if (!instance) {
        instance=[[ContestManager alloc] init];
    }
    return instance;
}

-(void)getContests:(int)userId and:(void (^)(id currentClient))handler onFailure:(void (^)(NSError* error))failure
{
    NSArray* vars = [NSArray arrayWithObjects:[[Language sharedInstance] langAbbreviation],@(userId),nil];
    NSArray* keys = [NSArray arrayWithObjects:@"lang",@"userId",nil];
    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
    [self submitWithAction:@"contest/getContest" andParams:dic andIam:nil and:^(id currentClient) {
        handler(currentClient);
    }onFailure:^(NSError *error) {
        failure(error);
    }];
}

@end
