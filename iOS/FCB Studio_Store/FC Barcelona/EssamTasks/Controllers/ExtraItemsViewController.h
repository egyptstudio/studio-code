//
//  ExtraItemsViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/12/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExtraItemsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *itemQuantity;
@property (weak, nonatomic) IBOutlet UIImageView *itemImage;
@property (weak, nonatomic) IBOutlet UIView *itemView;
@property (nonatomic , strong) UIImage* editedImage;
@property (nonatomic , strong) UIImage* originalImage;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;


- (IBAction)continueButton_Action:(id)sender;
- (IBAction)back_Action:(id)sender;

@end
