//
//  TutorialViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 3/5/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RGMPagingScrollView.h"

@interface TutorialViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pageNumber;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet RGMPagingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (nonatomic) BOOL helpFlag;

- (IBAction)skip_Action:(id)sender;
@end
