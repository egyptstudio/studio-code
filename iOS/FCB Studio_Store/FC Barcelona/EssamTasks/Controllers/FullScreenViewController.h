//
//  FullScreenViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 11/30/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImageView.h"
#import "FullScreenCell.h"
#import "Post.h"
#import "RGMPagingScrollView.h"

@interface FullScreenViewController : UIViewController<UIScrollViewDelegate,RGMPagingScrollViewDelegate,RGMPagingScrollViewDatasource>

@property (assign) Post* currentPost;
@property (assign) int wallType;
@property (assign) NSMutableArray * allPosts;

@property (assign) NSString *lastRequestTime;
@property (assign) NSString *timeFilter;
@property (assign) NSArray* countriesArray;
@property (assign) BOOL favoritePostsFlag;

@property (assign) int selectedPostIndex;


- (IBAction)back_Action:(id)sender;
@property (weak, nonatomic) IBOutlet RGMPagingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITableView *doomsTabel;


@end
