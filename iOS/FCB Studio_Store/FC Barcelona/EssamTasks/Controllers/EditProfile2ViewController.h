//
//  EditProfile2ViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/14/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "User.h"
@interface EditProfile2ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *status;
@property (weak, nonatomic) IBOutlet UITextView *aboutMe;
@property (weak, nonatomic) IBOutlet UILabel *relationShip;
@property (weak, nonatomic) IBOutlet UITextField *religion;
@property (weak, nonatomic) IBOutlet UILabel *education;
@property (weak, nonatomic) IBOutlet UILabel *jop;
@property (weak, nonatomic) IBOutlet UILabel *jopRole;
@property (weak, nonatomic) IBOutlet UITextField *company;
@property (weak, nonatomic) IBOutlet UILabel *income;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UIView *aboutMeView;
@property (weak, nonatomic) IBOutlet UIView *relationShipView;
@property (weak, nonatomic) IBOutlet UIView *religionView;
@property (weak, nonatomic) IBOutlet UIView *educationView;
@property (weak, nonatomic) IBOutlet UIView *jopView;
@property (weak, nonatomic) IBOutlet UIView *jopRoleView;
@property (weak, nonatomic) IBOutlet UIView *companyView;
@property (weak, nonatomic) IBOutlet UIView *incomeView;

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UILabel *statusTitle;
@property (weak, nonatomic) IBOutlet UILabel *aboutTitle;
@property (weak, nonatomic) IBOutlet UILabel *relationShipTitle;
@property (weak, nonatomic) IBOutlet UILabel *educationTitle;
@property (weak, nonatomic) IBOutlet UILabel *jobTitle;
@property (weak, nonatomic) IBOutlet UILabel *jobRoleTitle;

@property (weak, nonatomic) IBOutlet UILabel *companyTitle;
@property (weak, nonatomic) IBOutlet UILabel *incomeTitle;

@property (nonatomic)  BOOL fromPoints;

@property (assign) User *user;

- (IBAction)back_Action:(id)sender;
- (IBAction)saveButton_Action:(id)sender;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *mainScroll;
@end
