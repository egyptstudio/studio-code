//
//  AboutViewController.h
//  FC Barcelona
//
//  Created by Eissa on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactUs;
@property (weak, nonatomic) IBOutlet UILabel *privacyPolicy;
@property (weak, nonatomic) IBOutlet UILabel *faq;
@property (weak, nonatomic) IBOutlet UILabel *tc;



@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *webView;

- (IBAction)back_Action:(id)sender;
- (IBAction)refresh_Action:(id)sender;

@end
