//
//  AboutViewController.m
//  FC Barcelona
//
//  Created by Eissa on 8/27/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    // Do any additional setup after loading the view.
}

-(void)initView
{
 
    CALayer * l = [self.containerView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10.0];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goContactUs)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    
    [self.contactUs addGestureRecognizer:tap];
    self.contactUs.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goPrivacyPolicy)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    
    [self.privacyPolicy addGestureRecognizer:tap1];
    self.privacyPolicy.userInteractionEnabled = YES;

    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goFAQ)];
    tap2.cancelsTouchesInView = YES;
    tap2.numberOfTapsRequired = 1;
    
    [self.faq addGestureRecognizer:tap2];
    self.faq.userInteractionEnabled = YES;

    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goTC)];
    tap3.cancelsTouchesInView = YES;
    tap3.numberOfTapsRequired = 1;
    
    [self.tc addGestureRecognizer:tap3];
    self.tc.userInteractionEnabled = YES;

}
-(void) goContactUs
{
    
}
-(void) goPrivacyPolicy
{
    
}
-(void) goFAQ
{
    
}
-(void) goTC
{
    
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)refresh_Action:(id)sender
{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
