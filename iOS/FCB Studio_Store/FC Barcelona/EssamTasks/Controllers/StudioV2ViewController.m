//
//  StudioV2ViewController.m
//  FC Barcelona
//
//  Created by Eissa on 9/25/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "StudioV2ViewController.h"
#import "SVProgressHUD.h"
#import "StudioManager.h"
#import "RestServiceAgent.h"
#import "StudioFolder.h"
#import "WebImage.h"
#import "StudioDetailsViewController.h"
#import "AvatarImageView.h"
#import "MenuViewController.h"
#import "SKBounceAnimation.h"
#import "StudioPhoto.h"
#import "CurrentUser.h"
#import "DatabaseEngine.h"
#import "StudioFilterViewController.h"
#import "GetAndPushCashedData.h"
#import <GoogleMobileAds/DFPBannerView.h>
#import <GoogleMobileAds/DFPRequest.h>

@interface StudioV2ViewController ()<UIAlertViewDelegate,GADBannerViewDelegate>

@end


@implementation StudioV2ViewController
{
    tapType tap;
    id resultsDictionary;
    NSMutableArray* playersFolders;
    NSMutableArray* teamFolders;
    NSMutableArray* purchasedFolder;
    int pageNo;
    UILabel *errorlabel;
    BOOL adsOpendOnce;
    BOOL adsBannerOpendOnce;


}




- (void)viewDidLoad
{
    [super viewDidLoad];
    /////////////////
    [_segmentsView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:84/255.0f blue:164/255.0f alpha:1]];
    [_segments setFrame:_segmentsView.bounds];
    [_segmentsViewTap setFrame:_segments.bounds];
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    UIFont *fontBold = [UIFont boldSystemFontOfSize:14.0f];
    
    UIColor *color = [UIColor whiteColor];
    UIColor *colorDisabled = [UIColor darkGrayColor];

    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesBold = [NSDictionary dictionaryWithObjectsAndKeys:
                                    fontBold,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesDisabled = [NSDictionary dictionaryWithObjectsAndKeys:
                                        font,NSFontAttributeName,colorDisabled,NSForegroundColorAttributeName,nil];

    [_segments setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [_segments setTitleTextAttributes:attributesBold forState:UIControlStateSelected];
    [_segments setTitleTextAttributes:attributesDisabled forState:UIControlStateDisabled];
    
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_normal.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"]
                         forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    UIView *devider = [[UIView alloc] initWithFrame:CGRectMake(0, 0, .5, _segments.frame.size.height)];
    [devider setBackgroundColor:[UIColor whiteColor]];
    UIGraphicsBeginImageContextWithOptions(devider.bounds.size, devider.opaque, 0.0);
    [devider.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * deviderImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateSelected
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateNormal
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    //////////////////
    
    playersFolders  = [[NSMutableArray alloc] init];
    teamFolders     = [[NSMutableArray alloc] init];
    purchasedFolder = [[NSMutableArray alloc] init];

    [_segments setTitle:@"Players" forSegmentAtIndex:0];
    [_segments setTitle:@"Team" forSegmentAtIndex:1];
    [_segments setTitle:@"Purchased"   forSegmentAtIndex:2];
    [_segments setSelectedSegmentIndex:0];

    errorlabel = [[UILabel alloc] initWithFrame:_tableView.frame];
    errorlabel.textAlignment = NSTextAlignmentCenter;
    [self loadFolders];
    [self setLocalisables];
    
}

-(void)viewWillAppear:(BOOL)animated{
    self.screenName = @"Shop Screen";
    [[CurrentUser getObject] updateUserPoints];
    [self checkIFNotificationEnabled];
    
    /////////////////
    if ([GetAndPushCashedData getObjectFromCash:@"OpenPurchasedTab"]) {
        [GetAndPushCashedData removeObjectWithAction:@"OpenPurchasedTab"];
        _segments.selectedSegmentIndex = 2;
        [self segmentChanged:_segments];
    }else{
        //_segments.selectedSegmentIndex = 0;
        [self segmentChanged:_segments];
    }
    [self addAdBanner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger numOfRows ;
    switch (tap) {
        case playersTap:
            numOfRows = [playersFolders count];
            break;
        case teamTap:
            numOfRows = [teamFolders count];
            break;
        case purchasedTap:
            numOfRows = [purchasedFolder count];
            break;
        default:
            break;
    }
    return numOfRows;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [[UICollectionViewCell alloc] init];
    switch (tap) {
        case playersTap:
            cell = [self cellForPlayersAtIndexPath:indexPath];
            break;
        case teamTap:
            cell = [self cellForTeamAtIndexPath:indexPath];
            break;
        case purchasedTap:
            cell = [self cellForPurchasedAtIndexPath:indexPath];
            break;
        default:
            break;
    }
    return cell;
}

-(UICollectionViewCell *)cellForPurchasedAtIndexPath:(NSIndexPath *)indexPath
{
    StudioPhoto *currentPhoto = (StudioPhoto*)[purchasedFolder objectAtIndex:indexPath.row];
    UICollectionViewCell *cell = [_tableView dequeueReusableCellWithReuseIdentifier:@"purchasedCell" forIndexPath:indexPath];
    UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:1];
    UIImageView* premuimImage = (UIImageView*)[cell viewWithTag:2];
    UIImageView* newImage     = (UIImageView*)[cell viewWithTag:3];
    UILabel* rank         = (UILabel*)[cell viewWithTag:4];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:9] ;
    
    mainImage.image = nil;
    
    [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentPhoto photoUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    premuimImage.hidden = ![currentPhoto isPremium];
    rank.text = [NSString stringWithFormat:@"%i",[currentPhoto useCount]];
    
    newImage.hidden = ![self isNotOlderThanOneWeekFromDate:[NSDate dateWithTimeIntervalSince1970:[currentPhoto uploadDate]]];
    
    CALayer * l = [cell layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:2];
    return cell;
}

-(BOOL)isNotOlderThanOneWeekFromDate:(NSDate*)date{
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *todaysComponents = [gregorian components:NSWeekCalendarUnit fromDate:[NSDate date]];
    NSUInteger todaysWeek = [todaysComponents week];
    
    NSDateComponents *otherComponents =[gregorian components:NSWeekCalendarUnit fromDate:date];
    NSUInteger anotherWeek = [otherComponents week];
    
    if ( todaysWeek == anotherWeek || todaysWeek-1 == anotherWeek ) {
        return YES;
    }
    else{
        return NO;
    }
}

-(UICollectionViewCell *)cellForPlayersAtIndexPath:(NSIndexPath *)indexPath
{
    StudioFolder *currentFolder = (StudioFolder*)[playersFolders objectAtIndex:indexPath.row];
    UICollectionViewCell *cell  = [_tableView dequeueReusableCellWithReuseIdentifier:@"playerCell" forIndexPath:indexPath];
    UIImageView* mainImage      = (UIImageView*)[cell viewWithTag:1];
    UILabel* numOfPics          = (UILabel*)[cell viewWithTag:2];
    UILabel* playerNumber       = (UILabel*)[cell viewWithTag:3];
    UILabel* playerName         = (UILabel*)[cell viewWithTag:4];

    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:9] ;
    
    mainImage.image = nil;
    [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentFolder folderPicUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    numOfPics.text = [NSString stringWithFormat:@"%i",[currentFolder noOfPics]];
    playerNumber.text = [[currentFolder rank] isEqualToString:@"0"]?@"":[currentFolder rank];
    playerName.text = [currentFolder folderName];
    CALayer * l = [cell layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:2];
    return cell;

}

-(UICollectionViewCell *)cellForTeamAtIndexPath:(NSIndexPath *)indexPath
{
    StudioFolder *currentFolder = (StudioFolder*)[teamFolders objectAtIndex:indexPath.row];
    UICollectionViewCell *cell = [_tableView dequeueReusableCellWithReuseIdentifier:@"teamCell" forIndexPath:indexPath];
    
    UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:1];
    UILabel* folderName       = (UILabel*)[cell viewWithTag:2];
    UILabel* numOfPics        = (UILabel*)[cell viewWithTag:3];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:9] ;
    
    mainImage.image = nil;
    [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentFolder folderPicUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    folderName.text = [currentFolder folderName];
    numOfPics.text = [NSString stringWithFormat:@"%i",[currentFolder noOfPics]];
    
    
    CALayer * l = [cell layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    return cell;

}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    StudioDetailsViewController* details = [self.storyboard
                                            instantiateViewControllerWithIdentifier:@"StudioDetailsViewController"];
    switch (tap) {
        case playersTap:
            details.currentFolder = (StudioFolder*)[playersFolders objectAtIndex:indexPath.row]  ;
            break;
        case teamTap:
            details.currentFolder = (StudioFolder*)[teamFolders objectAtIndex:indexPath.row]  ;
            break;
        case purchasedTap:
            details.currentFolder = (StudioFolder*)[purchasedFolder objectAtIndex:indexPath.row]  ;
            [details setTempDic:purchasedFolder];
            [details setPurchasedIndex:indexPath.row];

            break;
        default:
            break;
    }
     [self.navigationController pushViewController:details animated:YES];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize;
    switch (tap) {
        case playersTap:
        {
            cellSize = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? CGSizeMake(150, 190) : CGSizeMake(76, 92) ;
            break;
        }
        case teamTap:
        {
            cellSize = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? CGSizeMake(740, 220): [[UIImage imageNamed:@"team_frame.png"] size];
            break;
        }
        case purchasedTap:
        {
            cellSize = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? CGSizeMake(150, 160) :  CGSizeMake(75, 75);
            break;
        }
        default:
            break;
    }
    return cellSize;
}


-(void)loadFolders
{
    tap = playersTap;
    if ([GetAndPushCashedData getObjectFromCash:@"StudioFolders"]) {
        [self prepareFoldersFromCache:[GetAndPushCashedData getObjectFromCash:@"StudioFolders"]];
    }
    else{
        [self getStudioFolders];
    }
}


-(void)getStudioFolders
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[StudioManager getInstance]getStudioFolders:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        if (![[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] objectForKey:@"CurrentLibVer"]) {
                            [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setObject:@(1.0) forKey:@"CurrentLibVer"];
                        }
                        resultsDictionary = currentClient;
                        [self prepareFoldersForFirstTime];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)prepareFoldersForFirstTime{
    for (StudioFolder *folder in resultsDictionary) {
        if ([folder folderType]==1)
            [playersFolders addObject:folder];
        else
            [teamFolders addObject:folder];
    }
    [GetAndPushCashedData cashObject:resultsDictionary withAction:@"StudioFolders"];
    [_tableView reloadData];
}

-(void)prepareFoldersFromCache:(NSArray*)folders{
    for (StudioFolder *folder in folders) {
        if ([folder folderType]==1)
            [playersFolders addObject:folder];
        else
            [teamFolders addObject:folder];
    }
    [_tableView reloadData];
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)openStudio:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];

}
- (void)team_Action
{
    [_needLoginView setHidden:YES];
    [errorlabel removeFromSuperview];
    pageNo = 0;
    tap = teamTap;
    [_tableView reloadData];
}
- (void)players_Action
{
    [_needLoginView setHidden:YES];
    [errorlabel removeFromSuperview];
    pageNo = 0;
    tap= playersTap;
    [_tableView reloadData];
}
- (void)purchased_Action
{
    [errorlabel removeFromSuperview];
    if ([[CurrentUser getObject] isUser]) {
        [_needLoginView setHidden:YES];
    }else{
        [_needLoginView setHidden:NO];
    }

    pageNo = 0;
    tap=purchasedTap;
    [_tableView reloadData];
    if ([[CurrentUser getObject] isUser]) {
        [self getUserPurchasedPhotos];
    }
}
- (void)getUserPurchasedPhotos{
    pageNo++;
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[StudioManager getInstance]getUserPurchasedPhotos:[[CurrentUser getObject] getUser].userId pageNo:pageNo and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([currentClient count]>0) {
                            purchasedFolder = currentClient;
                            [_tableView reloadData];
                    }else{
                        if ([purchasedFolder count]>0) {
                            errorlabel.text = @"You have not any purchased photos yes.";
                            [self.view addSubview:errorlabel];
                        }
                    }
                    
                    
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                    pageNo--;
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            pageNo--;

        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
        pageNo--;
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height + 6 && tap==purchasedTap)
    {
        if ([purchasedFolder count] >= 24)
            [self getUserPurchasedPhotos];
        return;
    }
}

- (IBAction)segmentChanged:(id)sender {
    switch (_segments.selectedSegmentIndex) {
        case 0:
            [self players_Action];
            break;
        case 1:
            [self team_Action];
            break;
        case 2:
            [self purchased_Action];
            break;
        default:
            break;
    }
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];

}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];

}
- (IBAction)filterButton_Action:(id)sender{
    StudioFilterViewController* filter = [self.storyboard
                                            instantiateViewControllerWithIdentifier:@"StudioFilterViewController"];
    [self.navigationController pushViewController:filter animated:YES];

}
-(void)setLocalisables{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"Gallary"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Studio_Players"] forSegmentAtIndex:0];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Studio_Team"] forSegmentAtIndex:1];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Studio_Purchased"] forSegmentAtIndex:2];
    [_needLoginLabel setText:[[Language sharedInstance] stringWithKey:@"Login_subtitle"]];
    [_goLoginButton setTitle:[[Language sharedInstance] stringWithKey:@"HomeInitialState_WallLogin"] forState:UIControlStateNormal];
    CALayer * l = [_goLoginButton layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    [_goLoginButton addTarget:self action:@selector(openLoginScreen) forControlEvents:UIControlEventTouchUpInside];
}

-(void)openLoginScreen{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
}

-(void)checkIFNotificationEnabled{
    if (![self isRegisteredForRemoteNotifications] && [self canOpenSettings]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please, enable notifications" delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:@"Settings", nil];
        [alert show];
    }
    else if(![self isRegisteredForRemoteNotifications]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please, enable notifications" delegate:nil cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:nil];
        [alert show];
    }
    
}
-(BOOL)canOpenSettings{
#ifdef __IPHONE_8_0
    return &UIApplicationOpenSettingsURLString != NULL;
#endif
    return NO;
}
-(BOOL)isRegisteredForRemoteNotifications{
     if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        return     [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
    }
    else{
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types == UIRemoteNotificationTypeNone)
            return NO;
        else
            return YES;
    }
}

-(void)openSettings{
#ifdef __IPHONE_8_0
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
#endif
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex!=alertView.cancelButtonIndex) {
        [self openSettings];
    }
}
-(void)addAdBanner{
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    bannerView.rootViewController = self;
    bannerView.adUnitID = @"ca-app-pub-2924334778141972/3060492441";
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"abc7f676a40f3514a5a8c71ad28dfd27"];
    [bannerView loadRequest:request];
    
    [_adBanner addSubview:bannerView];
}


@end
