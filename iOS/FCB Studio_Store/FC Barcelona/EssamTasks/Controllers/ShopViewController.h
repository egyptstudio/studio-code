//
//  ShopViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/2/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "GAITrackedViewController.h"

@interface ShopViewController : GAITrackedViewController



@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (weak, nonatomic) IBOutlet UILabel *comingSoon;
@property (nonatomic) int tapIndex;

@property (weak, nonatomic) IBOutlet UILabel *pack1Label;
@property (weak, nonatomic) IBOutlet UILabel *pack2Label;
@property (weak, nonatomic) IBOutlet UILabel *pack3Label;
@property (weak, nonatomic) IBOutlet UILabel *pack4Label;

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

@property (weak, nonatomic) IBOutlet UILabel *premuimPac;
@property (weak, nonatomic) IBOutlet UILabel *pointsPac;

@property (weak, nonatomic) IBOutlet UIView *premuimPacView;
@property (weak, nonatomic) IBOutlet UIView *pointsPacView;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segments;
@property (weak, nonatomic) IBOutlet UIScrollView *segmentsView;
@property (weak, nonatomic) IBOutlet UIView *segmentsViewTap;

@property (weak, nonatomic) IBOutlet UIButton *freePointsButton;
- (IBAction)freePointsButton_Action:(id)sender;

- (IBAction)openStudio:(id)sender;
- (IBAction)back_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;
- (IBAction)segmentChanged:(id)sender;

@end
