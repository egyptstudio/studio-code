//
//  ImageToZoomViewController.h
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 10/11/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageToZoomViewController : UIViewController<UIScrollViewDelegate>
{
    UIImageView* imageV;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainZoomView;
@property (weak, nonatomic) IBOutlet UILabel *photoName;
@property (nonatomic, strong) UIImage *image;
- (IBAction)back_Action:(id)sender;
@end
