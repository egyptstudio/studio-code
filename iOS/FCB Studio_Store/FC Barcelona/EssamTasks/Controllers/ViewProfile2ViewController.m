//
//  ViewProfile2ViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/13/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "ViewProfile2ViewController.h"
#import "CurrentUser.h"
@interface ViewProfile2ViewController ()
{

}
@end

@implementation ViewProfile2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareView];

}

-(void)prepareView
{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"MyProfile_MoreAboutMe"]];

    
    User * user = [[CurrentUser getObject] getUser];
    
    
    
    _status.text      = ![user status]  || [[user status] isEqualToString:@" "]?
                       [[Language sharedInstance] stringWithKey:@"MyProfile_Status"]:[[NSString stringWithFormat:@"%@\n",[[Language sharedInstance] stringWithKey:@"edit_more_status"]] stringByAppendingString:[user status]];
    
    _aboutMe.text      = ![user aboutMe]  || [[user aboutMe] isEqualToString:@" "]?
                        [[Language sharedInstance] stringWithKey:@"MyProfile_AboutMe"]:[[NSString stringWithFormat:@"%@\n",[[Language sharedInstance] stringWithKey:@"edit_more_about_me"]] stringByAppendingString:[user aboutMe]];
    
    
    NSString* relationShipState;
    
    switch ([user relationship]) {
        case 1:
            relationShipState = [[Language sharedInstance] stringWithKey:@"relationship_list_item1"];
            break;
        case 2:
            relationShipState = [[Language sharedInstance] stringWithKey:@"relationship_list_item2"];
            break;
        case 3:
            relationShipState = [[Language sharedInstance] stringWithKey:@"relationship_list_item3"];
            break;
        case 4:
            relationShipState = [[Language sharedInstance] stringWithKey:@"relationship_list_item4"];
            break;
        default:
            relationShipState = @"NONE";
            break;
    }
    
    if ([[Language sharedInstance] currentLanguage]==Arabic) {
        [_status setTextAlignment:NSTextAlignmentRight];
        [_aboutMe setTextAlignment:NSTextAlignmentRight];
        [_relationShip setTextAlignment:NSTextAlignmentRight];
        [_religion setTextAlignment:NSTextAlignmentRight];
        [_education setTextAlignment:NSTextAlignmentRight];
        [_jop setTextAlignment:NSTextAlignmentRight];
        [_jopRole setTextAlignment:NSTextAlignmentRight];
        [_company setTextAlignment:NSTextAlignmentRight];
        [_income setTextAlignment:NSTextAlignmentRight];
    }
    
    _relationShip.text = [user relationship] == 0?
                        [[Language sharedInstance] stringWithKey:@"MyProfile_Relationship"]:[[NSString stringWithFormat:@"%@\n",[[Language sharedInstance] stringWithKey:@"edit_more_relationship"]] stringByAppendingString:relationShipState];
    
    
    NSString* educationState = [[[Language sharedInstance] arrayWithKey:@"Educations"] objectAtIndex:[user education]];
    _education.text      = [user education] == 0?
                        [[Language sharedInstance] stringWithKey:@"MyProfile_Education"]:[[NSString stringWithFormat:@"%@\n",[[Language sharedInstance] stringWithKey:@"edit_more_education"]] stringByAppendingString:educationState];

    NSString* jobState = [[[Language sharedInstance] arrayWithKey:@"Jobs"] objectAtIndex:[user job]];
    _jop.text            = [user job] == 0?
                        [[Language sharedInstance] stringWithKey:@"MyProfile_Job"]:[[NSString stringWithFormat:@"%@\n",[[Language sharedInstance] stringWithKey:@"edit_more_job"]] stringByAppendingString:jobState];
    
    NSString* jobRoleState = [[[Language sharedInstance] arrayWithKey:@"JobRoles"] objectAtIndex:[user jobRole]];
    _jopRole.text            = [user jobRole] == 0?
    [[Language sharedInstance] stringWithKey:@"MyProfile_JobRole"]:[[NSString stringWithFormat:@"%@\n",[[Language sharedInstance] stringWithKey:@"JobRole"]] stringByAppendingString:jobRoleState];
    
    
    _company.text        = ![user company]  || [[user company] isEqualToString:@" "]?
                        [[Language sharedInstance] stringWithKey:@"MyProfile_Company"]:[[NSString stringWithFormat:@"%@\n",[[Language sharedInstance] stringWithKey:@"edit_more_company"]] stringByAppendingString:[user company]];
    
    NSString* incomeState;
    switch ([user income]) {
        case 1:
            incomeState = [[Language sharedInstance] stringWithKey:@"income_list_item1"];
            break;
        case 2:
            incomeState = [[Language sharedInstance] stringWithKey:@"income_list_item2"];
            break;
        case 3:
            incomeState = [[Language sharedInstance] stringWithKey:@"income_list_item3"];
            break;
        default:
            incomeState = @"NONE";
            break;
    }
    _income.text = [user income] == 0?
                   [[Language sharedInstance] stringWithKey:@"initial_state_income"]:[[NSString stringWithFormat:@"%@\n",[[Language sharedInstance] stringWithKey:@"edit_more_income"]] stringByAppendingString:incomeState];
    
    
    
    CGSize statusSize = [_status.text sizeWithFont:[UIFont systemFontOfSize:16.0f] constrainedToSize:CGSizeMake(_status.frame.size.width, 20000) lineBreakMode: NSLineBreakByWordWrapping];
    
    CGSize aboutSize = [_aboutMe.text sizeWithFont:[UIFont systemFontOfSize:16.0f] constrainedToSize:CGSizeMake(_aboutMe.frame.size.width, 20000) lineBreakMode:NSLineBreakByWordWrapping];

    
    if ( statusSize.height > 40 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self reDrawBelowState];
    }
    if ( aboutSize.height > 40 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self reDrawBelowAbout];
    }
    
    [self initScrollContentSize];
}
-(void)reDrawBelowState
{
    CGSize textSize = [_status.text sizeWithFont:[UIFont systemFontOfSize:16.0f] constrainedToSize:CGSizeMake(_aboutMe.frame.size.width, 20000) lineBreakMode:NSLineBreakByWordWrapping];
    
    _status.frame = CGRectMake(_status.frame.origin.x,
                                _status.frame.origin.y,
                                _mainScroll.frame.size.width,
                                textSize.height);
    _aboutMe.frame = CGRectMake(0,CGRectGetMaxY(_status.frame)+10, _mainScroll.frame.size.width, 40);
    _relationShip.frame = CGRectMake(0,CGRectGetMaxY(_aboutMe.frame)+10, _mainScroll.frame.size.width, 40);
    _education.frame     = CGRectMake(0,CGRectGetMaxY(_relationShip.frame)+10, _mainScroll.frame.size.width, 40);
    _jop.frame    = CGRectMake(0,CGRectGetMaxY(_education.frame)+10, _mainScroll.frame.size.width, 40);
    _company.frame          = CGRectMake(0,CGRectGetMaxY(_jop.frame)+10, _mainScroll.frame.size.width, 40);
    _income.frame      = CGRectMake(0,CGRectGetMaxY(_company.frame)+10, _mainScroll.frame.size.width, 40);
    

}
-(void)reDrawBelowAbout
{
    CGSize textSize = [_aboutMe.text sizeWithFont:[UIFont systemFontOfSize:16.0f] constrainedToSize:CGSizeMake(_aboutMe.frame.size.width, 20000) lineBreakMode:NSLineBreakByWordWrapping];
    
    _aboutMe.frame = CGRectMake(_aboutMe.frame.origin.x,
                                _aboutMe.frame.origin.y,
                                _mainScroll.frame.size.width,
                                textSize.height);
    
    _relationShip.frame = CGRectMake(0,CGRectGetMaxY(_aboutMe.frame)+10, _mainScroll.frame.size.width, 40);
    _education.frame     = CGRectMake(0,CGRectGetMaxY(_relationShip.frame)+10, _mainScroll.frame.size.width, 40);
    _jop.frame    = CGRectMake(0,CGRectGetMaxY(_education.frame)+10, _mainScroll.frame.size.width, 40);
    _company.frame          = CGRectMake(0,CGRectGetMaxY(_jop.frame)+10, _mainScroll.frame.size.width, 40);
    _income.frame      = CGRectMake(0,CGRectGetMaxY(_company.frame)+10, _mainScroll.frame.size.width, 40);
    
}

-(void)initScrollContentSize
{
    for (UIView *view in _mainScroll.subviews) {
        [_mainScroll setContentSize:CGSizeMake(0, _mainScroll.contentSize.height + view.frame.size.height+10)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)back_Action:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
