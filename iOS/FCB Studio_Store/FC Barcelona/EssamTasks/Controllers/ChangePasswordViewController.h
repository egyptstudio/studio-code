//
//  ChangePasswordViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/18/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

@property (weak, nonatomic) IBOutlet UIView *passOldView;
@property (weak, nonatomic) IBOutlet UITextField *passOld;

@property (weak, nonatomic) IBOutlet UIView *passNewView;
@property (weak, nonatomic) IBOutlet UITextField *passNew;

@property (weak, nonatomic) IBOutlet UIView *confirmView;
@property (weak, nonatomic) IBOutlet UITextField *confirm;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;

- (IBAction)send_Action:(id)sender;
- (IBAction)back_Action:(id)sender;
- (IBAction)openStudio:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

@end
