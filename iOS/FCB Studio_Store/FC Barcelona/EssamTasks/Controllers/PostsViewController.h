//
//  PostsViewController.h
//  FC Barcelona
//
//  Created by Eissa on 10/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RGMPagingScrollView.h"

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;

@protocol PostsListDelegate <NSObject>
@required
- (void)didScrollToDirection:(ScrollDirection)direction;
@end
@interface PostsViewController : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet RGMPagingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *noPhotosLabel;
@property (weak, nonatomic) IBOutlet UIView *noPhotosView;
@property (nonatomic,assign) id <PostsListDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(assign) int requestType;
-(void)scrollTopTop;
-(void)applyFilerWithIsFavorate:(BOOL)favorate andCountries:(NSArray*)array;

@end
