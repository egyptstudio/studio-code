//
//  StudioDetailsViewController.m
//  FC Barcelona
//
//  Created by Eissa on 9/26/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "StudioDetailsViewController.h"
#import "SVProgressHUD.h"
#import "StudioManager.h"
#import "RestServiceAgent.h"
#import "StudioFolder.h"
#import "WebImage.h"
#import "StudioPhoto.h"
#import "CaptureViewController.h"
#import "MenuViewController.h"
#import "SKBounceAnimation.h"
#import "CurrentUser.h"
#import <KeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "GetAndPushCashedData.h"
#import <GoogleMobileAds/DFPBannerView.h>
#import <GoogleMobileAds/DFPRequest.h>

@interface StudioDetailsViewController ()<UIAlertViewDelegate>
{
    int currentScrollIndex;
    int pageNumber;
    BOOL loadedBefore;
    BOOL isFiltring;
    UILabel *errorLabel;
    NSMutableArray *resultsDictionary;
    StudioFolder *folderModel;
}
@end

@implementation StudioDetailsViewController
@synthesize folderId;
@synthesize pageTitle;
@synthesize currentFolder;
@synthesize seasons;
@synthesize filterBy;
@synthesize tempDic;
@synthesize  purchasedIndex;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    pageTitle.text = @"";
    
    errorLabel = [[UILabel alloc] initWithFrame:_tableView.frame];
    resultsDictionary = [[NSMutableArray alloc] init];
    CALayer * l = [self.useButton layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10.0];
    
    CALayer * l1 = [self.galleryView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:10.0];
    [l1 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
    [l1 setBorderWidth:2.0];
    
    currentScrollIndex=0;
    pageNumber = 0 ;
    
    CALayer * l4 = [self.photoCredit layer];
    [l4 setMasksToBounds:YES];
    [l4 setCornerRadius:10.0];
    self.usersCredit.text = [NSString stringWithFormat:@"%@ %d XP",[[Language sharedInstance] stringWithKey:@"photoUse_unock_credit"],[[CurrentUser getObject] getUser].credit];
    [self setLocalisables];
}

-(void)viewWillAppear:(BOOL)animated{
    [[CurrentUser getObject] updateUserPoints];
    [self addAdBanner];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (!loadedBefore) {
        folderId        = [currentFolder folderId];
        if (filterBy!=0 || seasons)
        {
            isFiltring = YES;
            pageTitle.text = [[Language sharedInstance] stringWithKey:@"FansFilter_Filter"];
            [self getStudioFolderPhotosWithFilter];
        }
        else if ([tempDic count]<= 0)
        {
            
            //            if ([GetAndPushCashedData getObjectFromCash:[NSString stringWithFormat:@"StudioFolder_%i",folderId]]) {
            //                [self preparePhotosFromCache:[GetAndPushCashedData getObjectFromCash:[NSString stringWithFormat:@"StudioFolder_%i",folderId]]];
            //            }
            //else{
            [self getStudioFolderPhotos];
            //}
            pageTitle.text = [currentFolder folderName];
            pageTitle.numberOfLines = 2;
            [pageTitle setFrame:CGRectMake(pageTitle.frame.origin.x+10, pageTitle.frame.origin.y-5, pageTitle.frame.size.width-20, pageTitle.frame.size.height+20)];
        }else{
            pageTitle.text = [[Language sharedInstance] stringWithKey:@"Studio_Purchased"];
            [resultsDictionary addObjectsFromArray:tempDic];
            [_tableView reloadData];
            [_imagesTableView reloadData];
            [self collectionView:_tableView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:purchasedIndex inSection:0]];
        }
        
        loadedBefore=YES;
    }
}


///Collection View Delegates//////////////////////////
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [resultsDictionary count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    StudioPhoto *currentPhoto = (StudioPhoto*)[resultsDictionary objectAtIndex:indexPath.row];
    cell = [_tableView dequeueReusableCellWithReuseIdentifier:@"purchasedCell" forIndexPath:indexPath];
    UIImageView* mainImage    = (UIImageView*)[cell viewWithTag:1];
    UIImageView* premuimImage = (UIImageView*)[cell viewWithTag:2];
    UIImageView* newImage     = (UIImageView*)[cell viewWithTag:3];
    UILabel* rank         = (UILabel*)[cell viewWithTag:4];
    UIActivityIndicatorView *loader = (UIActivityIndicatorView* )[cell viewWithTag:9] ;
    
    
    [mainImage sd_setImageWithURL:[NSURL URLWithString:[currentPhoto photoUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader setHidden:YES];
    }];
    
    premuimImage.hidden = ![currentPhoto isPremium];
    rank.text = [NSString stringWithFormat:@"%i",[currentPhoto useCount]];
    newImage.hidden = ![self isNotOlderThanOneWeekFromDate:[NSDate dateWithTimeIntervalSince1970:[currentPhoto uploadDate]]];
    CALayer * l = [cell layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:2];
    
    return cell;
}
-(BOOL)isNotOlderThanOneWeekFromDate:(NSDate*)date{
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *todaysComponents = [gregorian components:NSWeekCalendarUnit fromDate:[NSDate date]];
    NSUInteger todaysWeek = [todaysComponents week];
    
    NSDateComponents *otherComponents =[gregorian components:NSWeekCalendarUnit fromDate:date];
    NSUInteger anotherWeek = [otherComponents week];
    
    if ( todaysWeek == anotherWeek || todaysWeek-1 == anotherWeek ) {
        return YES;
    }
    else{
        return NO;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[CurrentUser getObject] isUser]) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        @try {
            if([RestServiceAgent internetAvailable])
            {
                [[StudioManager getInstance] getUserCredit:[[CurrentUser getObject] getUser].userId and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if (currentClient) {
                            if([currentClient[@"status"] intValue]==2)
                            {
                                [[[CurrentUser getObject] getUser] setCredit:[currentClient[@"data"][@"credit"] intValue]];
                                [[CurrentUser getObject] saveCurrentUser];
                                
                                _backgroundView.hidden = NO;
                                _galleryView.hidden = NO;
                                currentScrollIndex = indexPath.row;
                                StudioPhoto* currentPhoto= (StudioPhoto*)[resultsDictionary objectAtIndex:currentScrollIndex];
                                self.usersCredit.text = [NSString stringWithFormat:@"%@ %d XP",[[Language sharedInstance] stringWithKey:@"photoUse_unock_credit"],[[CurrentUser getObject] getUser].credit];
                                [_photoCredit setText:[NSString stringWithFormat:@"%d XP",[currentPhoto requiredPoints]]];
                                [_imagesTableView setCurrentPage:currentScrollIndex animated:NO];
                                
                                StudioPhoto *selectedPhoto = (StudioPhoto*)[resultsDictionary objectAtIndex:currentScrollIndex];
                                
                                if ([selectedPhoto isPurchased] || [currentPhoto requiredPoints] <= [[CurrentUser getObject] getUser].credit) {
                                    [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Use"] forState:UIControlStateNormal];
                                    [_useButton setBackgroundColor:_photoCredit.backgroundColor];
                                    
                                }
                                else if ([selectedPhoto isPremium] && ![[CurrentUser getObject] getUser].isPremium) {
                                    [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Unlock"] forState:UIControlStateNormal];
                                    [_useButton setBackgroundColor:[UIColor grayColor]];
                                }
                                else if ([selectedPhoto requiredPoints] > [[CurrentUser getObject] getUser].credit){
                                    [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Unlock"] forState:UIControlStateNormal];
                                    [_useButton setBackgroundColor:[UIColor grayColor]];
                                }
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }
        }
        @catch (NSException *exception) {
            NSLog(@"getUserCredit Error");
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
    
}

///Table View Delegates//////////////////////////
-(NSInteger)pagingScrollViewNumberOfPages:(RGMPagingScrollView *)pagingScrollView{
    return [resultsDictionary count];
}

- (UIView *)pagingScrollView:(RGMPagingScrollView *)pagingScrollView viewForIndex:(NSInteger)idx
{
    UIImageView *image = [[UIImageView alloc] initWithFrame:pagingScrollView.bounds];
    [image setContentMode:UIViewContentModeScaleAspectFit];
    [_useButton setEnabled:NO];
    
    [image sd_setImageWithURL:[NSURL URLWithString:[(StudioPhoto*)[resultsDictionary objectAtIndex:idx] photoUrl]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_useButton setEnabled:YES];

    }];
    
    StudioPhoto* currentPhoto= (StudioPhoto*)[resultsDictionary objectAtIndex:idx];
    [_photoCredit setText:[NSString stringWithFormat:@"%d XP",[currentPhoto requiredPoints]]];
    if ([currentPhoto isPurchased] || [currentPhoto requiredPoints] <= [[CurrentUser getObject] getUser].credit) {
        [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Use"] forState:UIControlStateNormal];
        [_useButton setBackgroundColor:_photoCredit.backgroundColor];
        
    }
    else if ([currentPhoto isPremium] && ![[CurrentUser getObject] getUser].isPremium) {
        [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Unlock"] forState:UIControlStateNormal];
        [_useButton setBackgroundColor:[UIColor grayColor]];
    }
    else if ([currentPhoto requiredPoints] > [[CurrentUser getObject] getUser].credit){
        [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Unlock"] forState:UIControlStateNormal];
        [_useButton setBackgroundColor:[UIColor grayColor]];
    }
    return image;
}

-(void)preparepreparePhotosForFirstTime{
    for (int i = 0 ; i < [resultsDictionary count]; i++) {
        NSMutableArray * allPhotosTags = [[NSMutableArray alloc] init];
        for (int j = 0 ; j < [[(StudioPhoto*)resultsDictionary[i] tags] count]; j++) {
            NSDictionary* dic = [[NSDictionary alloc] initWithDictionary:[(StudioPhoto*)resultsDictionary[i] tags][j]];
            StudioPhotoTag* tag = [[StudioPhotoTag alloc] init];
            tag.tagName =  dic[@"tagName"];
            tag.tagId   = [dic[@"tagId"] intValue];
            tag.tagType = [dic[@"tagType"]intValue];
            [allPhotosTags addObject:tag];
        }
        [(StudioPhoto*)resultsDictionary[i] setTags:allPhotosTags];
    }
    
    //////Add StudioPhotos////////
    [GetAndPushCashedData cashObject:resultsDictionary withAction:[NSString stringWithFormat:@"StudioFolder_%i",folderId]];
    [_tableView reloadData];
    [_imagesTableView reloadData];
}

-(void)preparePhotosFromCache:(StudioPhoto*)photos{
    [resultsDictionary addObjectsFromArray:(NSArray*)photos];
    [_tableView reloadData];
    [_imagesTableView reloadData];
}

////////////////////////////////////////////////////////////
-(void)getStudioFolderPhotos
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[StudioManager getInstance]getStudioFolderPhotos:[[CurrentUser getObject] getUser].userId folderId:folderId pageNo:1 and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        resultsDictionary = currentClient;
                        [self preparepreparePhotosForFirstTime];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
-(void)getStudioFolderPhotosWithFilter
{
    pageNumber++;
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[StudioManager getInstance]getStudioFolderPhotos:[[CurrentUser getObject] getUser].userId
                                                     folderId:folderId
                                                       pageNo:pageNumber
                                                     filterBy:filterBy
                                                      seasons:seasons and:^(id currentClient) {
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [SVProgressHUD dismiss];
                                                              if ([currentClient count]>0) {
                                                                  [resultsDictionary addObjectsFromArray:currentClient];
                                                                  [_tableView reloadData];
                                                                  [_imagesTableView reloadData];
                                                              }
                                                              else{
                                                                  if ([resultsDictionary count]==0)
                                                                      [self showErrorLabelWithText:[[Language sharedInstance] stringWithKey:@"filterResult_NoResults"]];
                                                              }
                                                          });
                                                      }onFailure:^(NSError *error) {
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [SVProgressHUD dismiss];
                                                              
                                                              UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                              [alert show];
                                                              pageNumber--;
                                                          });
                                                      }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    //    }
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height + 6) {
        if (isFiltring) {
            if ([resultsDictionary count] >= 24)
                [self getStudioFolderPhotosWithFilter];
            
        }
        return;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)useButton_Action:(id)sender
{
//    [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPremTab"];

    if ([[CurrentUser getObject] isUser]) {
        NSInteger index = _imagesTableView.currentPage;
        StudioPhoto *selectedPhoto = (StudioPhoto*)[resultsDictionary objectAtIndex:index];
        if ([selectedPhoto isPurchased]) {
            [self closePopup:nil];
            
            CaptureViewController* details = [self.storyboard instantiateViewControllerWithIdentifier:@"CaptureViewController"];
            
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            [WebImage processImageDataWithURLString:[selectedPhoto photoUrl] cacheImage:YES andBlock:^(NSData *imageData) {
                [SVProgressHUD dismiss];
                BOOL controllerExist = NO;
                for (UIViewController* controller in self.navigationController.viewControllers) {
                    if ([controller isKindOfClass:[details class]])
                    {
                        controllerExist = YES;
                        [(CaptureViewController*)controller setStudioPhoto:selectedPhoto];
                        [self.navigationController popToViewController:controller animated:YES];
                        
                    }
                }
                if (!controllerExist) {
                    details.studioPhoto =  selectedPhoto;
                    [self.navigationController pushViewController:details animated:YES];
                }
            }];
            
        }else if ([selectedPhoto isPremium] && ![[CurrentUser getObject] getUser].isPremium ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[Language sharedInstance] stringWithKey:@"PremiumList_title"]
                                                            message:[[Language sharedInstance] stringWithKey:@"PremuimRequired"]
                                                           delegate:self
                                                  cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Cancel"]
                                                  otherButtonTitles:[[Language sharedInstance] stringWithKey:@"Subscribe"]
                                  , nil];
            [alert setTag:1];
            [alert show];
        }
        else if ([selectedPhoto requiredPoints] > [[CurrentUser getObject] getUser].credit){
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Insufficient Points"
//                                                            message:@"This photo requires more points, Do you want to purchase points now?"
//                                                           delegate:self
//                                                  cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"]
//                                                  otherButtonTitles:@"Purchase", nil];
//            [alert setTag:2];
//            [alert show];
            [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPointsTab"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];

        }
        else{
            
            @try {
                [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
                if([RestServiceAgent internetAvailable])
                {
                    [[StudioManager getInstance] purchaseStudioPhoto:[[[CurrentUser getObject] getUser] userId]  studioID:[selectedPhoto studioPhotoId] and:^(id currentClient) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                            if (currentClient) {
                                if([currentClient[@"status"] intValue]==1)
                                {
                                    
                                    [[[CurrentUser getObject] getUser] setCredit:[CurrentUser getObject].getUser.credit - [selectedPhoto requiredPoints]];
                                    [[CurrentUser getObject] saveCurrentUser];
                                    
                                    [selectedPhoto setIsPurchased:YES];
                                    //[self preparepreparePhotosForFirstTime];
                                    CaptureViewController* details = [self.storyboard instantiateViewControllerWithIdentifier:@"CaptureViewController"];
                                    
                                    //details.studioPhoto =  selectedPhoto;
                                    //[self.navigationController pushViewController:details animated:YES];
                                
                                    
                                    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
                                    [WebImage processImageDataWithURLString:[selectedPhoto photoUrl] cacheImage:YES andBlock:^(NSData *imageData) {
                                        [self closePopup:nil];
                                        [SVProgressHUD dismiss];
                                        BOOL controllerExist = NO;
                                        for (UIViewController* controller in self.navigationController.viewControllers) {
                                            if ([controller isKindOfClass:[details class]])
                                            {
                                                controllerExist = YES;
                                                [(CaptureViewController*)controller setStudioPhoto:selectedPhoto];
                                                [self.navigationController popToViewController:controller animated:YES];
                                                
                                            }
                                        }
                                        if (!controllerExist) {
                                            details.studioPhoto =  selectedPhoto;
                                            [self.navigationController pushViewController:details animated:YES];
                                        }
                                    }];
                                    
                                    
                                }
                            }
                        });
                    }onFailure:^(NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                            [alert show];
                        });
                    }];
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }
            }
            @catch (NSException *exception) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });
                
            }
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        switch (alertView.tag) {
            case 1:
            {
                [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPremTab"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
                break;
            }
            case 2:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
                break;
            }
            default:
                break;
        }
    }
}


- (IBAction)nextImage:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        currentScrollIndex = _imagesTableView.currentPage;
        if (currentScrollIndex < [resultsDictionary count]-1) {
            currentScrollIndex++;
            [_imagesTableView setCurrentPage:currentScrollIndex animated:YES];
            StudioPhoto* currentPhoto= (StudioPhoto*)[resultsDictionary objectAtIndex:currentScrollIndex];
            [_photoCredit setText:[NSString stringWithFormat:@"%d XP",[currentPhoto requiredPoints]]];
            if ([currentPhoto isPurchased] || [currentPhoto requiredPoints] <= [[CurrentUser getObject] getUser].credit) {
                [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Use"] forState:UIControlStateNormal];
                [_useButton setBackgroundColor:_photoCredit.backgroundColor];
                
            }
            else if ([currentPhoto isPremium] && ![[CurrentUser getObject] getUser].isPremium) {
                [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Unlock"] forState:UIControlStateNormal];
                [_useButton setBackgroundColor:[UIColor grayColor]];
            }
            else if ([currentPhoto requiredPoints] > [[CurrentUser getObject] getUser].credit){
                [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Unlock"] forState:UIControlStateNormal];
                [_useButton setBackgroundColor:[UIColor grayColor]];
            }
        }
        else{
            if ([resultsDictionary count] >= 24) {
                [self getStudioFolderPhotos];
            }
        }
    });
}

- (IBAction)prevImage:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        currentScrollIndex = _imagesTableView.currentPage;
        if (currentScrollIndex!= 0 ) {
            currentScrollIndex--;
            [_imagesTableView setCurrentPage:currentScrollIndex animated:YES];
            StudioPhoto* currentPhoto= (StudioPhoto*)[resultsDictionary objectAtIndex:currentScrollIndex];
            [_photoCredit setText:[NSString stringWithFormat:@"%d XP",[currentPhoto requiredPoints]]];
            
            if ([currentPhoto isPurchased] || [currentPhoto requiredPoints] <= [[CurrentUser getObject] getUser].credit) {
                [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Use"] forState:UIControlStateNormal];
                [_useButton setBackgroundColor:_photoCredit.backgroundColor];
            }
            else if ([currentPhoto isPremium] && ![[CurrentUser getObject] getUser].isPremium) {
                [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Unlock"] forState:UIControlStateNormal];
                [_useButton setBackgroundColor:[UIColor grayColor]];
            }
            else if ([currentPhoto requiredPoints] > [[CurrentUser getObject] getUser].credit){
                [_useButton setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Unlock"] forState:UIControlStateNormal];
                [_useButton setBackgroundColor:[UIColor grayColor]];
            }
        }
    });
}
- (IBAction)closePopup:(id)sender
{
    _backgroundView.hidden = YES;
    _galleryView.hidden = YES;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    objc_msgSend([UIDevice currentDevice], @selector(setOrientation:),UIInterfaceOrientationPortrait ); //cportrait
}

- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
}

- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
}

- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
}

-(void)showErrorLabelWithText:(NSString*)error{
    errorLabel.text = error;
    [self.view addSubview:errorLabel];
}

-(void)setLocalisables{
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    
}

-(void)addAdBanner{
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    bannerView.rootViewController = self;
    bannerView.adUnitID = @"ca-app-pub-2924334778141972/3060492441";
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"abc7f676a40f3514a5a8c71ad28dfd27"];
    [bannerView loadRequest:request];
    
    [_adBanner addSubview:bannerView];
}

@end
