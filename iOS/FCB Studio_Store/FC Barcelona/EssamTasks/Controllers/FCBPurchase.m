//
//  FollowersExchangePurchase.m
//  twitterExampleIII
//
//  Created by ousamaMacBook on 6/19/13.
//  Copyright (c) 2013 MacBook. All rights reserved.
//

#import "FCBPurchase.h"

@implementation FCBPurchase


+(FCBPurchase *)sharedInstance:(NSMutableArray*)identiferes {
    static dispatch_once_t once;
    static FCBPurchase * sharedInstance;

    dispatch_once(&once, ^{
        NSMutableSet * productIdentifiers;
    
        productIdentifiers = [NSMutableSet setWithArray:identiferes];
        
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}



@end
