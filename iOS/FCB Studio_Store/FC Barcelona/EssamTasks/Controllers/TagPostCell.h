//
//  VineListCell.h
//  PublicMinistry
//
//  Created by Shabir Jan on 19/12/2014.
//  Copyright (c) 2014 Shabir Jan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImageView.h"
#import "Post.h"
@interface TagPostCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;
@property (weak, nonatomic) IBOutlet UIImageView *postImage;
@property (weak, nonatomic) IBOutlet AvatarImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIView  *rankView;
@property (weak, nonatomic) IBOutlet UILabel *likesCount;
@property (weak, nonatomic) IBOutlet UILabel *postDate;

@property (weak, nonatomic) IBOutlet UILabel *commentsCount;
@property (weak, nonatomic) IBOutlet UILabel *useCount;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *repostButton;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *bigLoader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *smallLoader;
@property (weak, nonatomic) IBOutlet UILabel *ranks;

@property (weak, nonatomic) IBOutlet UIScrollView *tagsScroll;

- (IBAction)like:(id)sender;
- (IBAction)comment:(id)sender;
- (IBAction)use:(id)sender;
- (IBAction)share:(id)sender;

@property (assign)  int walltype;

@property (assign) NSString *lastRequestTime;
@property (assign) NSString *timeFilter;
@property (assign) NSArray* countriesArray;
@property (assign) BOOL favoritePostsFlag;


@property (strong) NSMutableArray * allPosts;

@property(assign) BOOL isEditable;

-(void)initWithPost:(Post*)post andController:(UIViewController*)mainView;
-(void)initImages;

@end
