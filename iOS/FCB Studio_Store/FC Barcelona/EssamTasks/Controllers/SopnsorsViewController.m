//
//  SopnsorsViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 5/3/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "SopnsorsViewController.h"

@interface SopnsorsViewController ()

@end

@implementation SopnsorsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"menu_Sponsors"]];
    [_comingSoon setText:[[Language sharedInstance] stringWithKey:@"coming_soon"]];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
}

- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}

- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}

- (IBAction)studio_Action:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
