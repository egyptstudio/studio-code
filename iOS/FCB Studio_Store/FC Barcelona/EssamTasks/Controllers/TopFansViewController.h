//
//  TopFansViewController.h
//  FC Barcelona
//
//  Created by Eissa on 10/6/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopFansViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)followAll:(id)sender;

@end
