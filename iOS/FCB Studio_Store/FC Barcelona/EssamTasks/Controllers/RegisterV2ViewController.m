//
//  RegisterV2ViewController.m
//  FC Barcelona
//
//  Created by Eissa on 9/24/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "RegisterV2ViewController.h"
#import "User.h"
#import "UsersManager.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "KxMenu.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "FCBPhotosViewController.h"
#import "PECropView.h"
#import "WebImage.h"
#import "CurrentUser.h"
#import "StudioManager.h"

@interface RegisterV2ViewController ()<FCBPhotosDelegate,UIActionSheetDelegate,CLLocationManagerDelegate>
{
    UIImagePickerController* cameraPicker;
    UIImage * userImage;
    PECropView *cropView;
    NSString* imageUrl;
    double lati;
    double longi;
    BOOL locationFound;
    CLPlacemark *placemark;
    
}
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."

@end

@implementation RegisterV2ViewController
{
    CLLocationManager *locationManager;
    CLGeocoder*geocoder;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    
    userImage = nil;
    //CALayer *mask = [CALayer layer];
    //mask.contents = (id)[[UIImage imageNamed:@"photo_mask.png"] CGImage];
    //mask.frame = CGRectMake(0, 0, _profilePicView.frame.size.width, _profilePicView.frame.size.height);
    //_profilePicView.layer.mask = mask;
    //_profilePicView.layer.masksToBounds = YES;
    
    geocoder = [[CLGeocoder alloc] init];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    
    CALayer * l8 = [_profilePicView layer];
    [l8 setBorderColor:[[UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1] CGColor]];
    [l8 setBorderWidth:2.0];
    [l8 setMasksToBounds:YES];
    [l8 setCornerRadius:20.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:@"UIKeyboardWillHideNotification"
                                               object:nil];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (locations == nil)
        return;

    [geocoder reverseGeocodeLocation:[locations objectAtIndex:0] completionHandler:^(NSArray *placemarks, NSError *error)
     {
         locationFound = YES;
         placemark = [placemarks lastObject];
         if (placemarks == nil)
             return;
     }];
}

-(void)setLocalisabels{
    _titleLabel.text = [[Language sharedInstance]stringWithKey:@"Registration_title"];
    self.fullName.placeholder = [[Language sharedInstance]stringWithKey:@"Registration_nameDefaultValue"];
    self.email.placeholder = [[Language sharedInstance]stringWithKey:@"Registration_EmailDefaultValue"];
    self.passWord.placeholder = [[Language sharedInstance]stringWithKey:@"Login_passwordDefaultMessage"];
    self.confirmPass.placeholder =  [[Language sharedInstance]stringWithKey:@"Registration_ConfirmPasswordDefaultValue"];
    [self.registerButton setTitle:[[Language sharedInstance]stringWithKey:@"Registration_title"] forState:UIControlStateNormal];
    [_addPhoto_Caption setText:[[Language sharedInstance] stringWithKey:@"menu_add_photo"]];
}

-(void)viewWillAppear:(BOOL)animated{
    if(_socialProfile){
        
        [_profileImage sd_setImageWithURL:[NSURL URLWithString:_socialProfile[@"picUrl"]] placeholderImage:[UIImage imageNamed:@"avatar"]];
        
        _fullName.text = _socialProfile[@"fullName"];
        
        _passWordView.hidden = YES;
        _confirmPassView.hidden = YES;
    }
    [self setLocalisabels];

}
- (void) keyboardWillShow:(NSNotification *)note {
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.mainScroll.contentSize = CGSizeMake(0, self.mainScroll.frame.size.height + kbSize.height+10);
    if ([UIScreen mainScreen].bounds.size.height < 568) {
        CGPoint bottomOffset = CGPointMake(0, self.mainScroll.contentSize.height - self.mainScroll.bounds.size.height);
        [self.mainScroll setContentOffset:bottomOffset animated:YES];
    }
}

- (void) keyboardWillHide:(NSNotification *)note {
    [UIView animateWithDuration:0.2 animations:^{
        self.mainScroll.contentSize = CGSizeMake(0,0);
    }];
}

-(void) selectImageSource:(UIButton*)sender
{
//    
//    CGRect frame;
//    frame = sender.superview.frame;
//    
//    [KxMenu showMenuInView:self.view
//                  fromRect:frame
//                 menuItems:@[
//                             [KxMenuItem menuItem:@"Camera"
//                                            image:nil
//                                           target:self
//                                           action:@selector(takePhoto_Cam:)],
//                             [KxMenuItem menuItem:@"Photo Album"
//                                            image:nil
//                                           target:self
//                                           action:@selector(takePhoto_gal:)],
//                             [KxMenuItem menuItem:@"FCB Photos"
//                                            image:nil
//                                           target:self
//                                           action:@selector(takePhoto_fcb:)]
//                             ]];

    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select photo source:" delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]destructiveButtonTitle:nil otherButtonTitles:
                            [[Language sharedInstance] stringWithKey:@"Camera"],
                            [[Language sharedInstance] stringWithKey:@"PhotoAlbum"],
                            [[Language sharedInstance] stringWithKey:@"FCBPhotos"],
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            imageUrl = nil;
            switch (buttonIndex) {
                case 0:
                    [self takePhoto_Cam:nil];
                    break;
                case 1:
                    [self takePhoto_gal:nil];
                    break;
                case 2:
                    [self takePhoto_fcb:nil];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}
- (void)takePhoto_Cam:(id)sender
{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"]
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        cameraPicker.delegate = self;
        cameraPicker.showsCameraControls = YES;
        cameraPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
        [self.view addSubview:cameraPicker.view];
    }
}

- (void)takePhoto_gal:(id)sender
{
    cameraPicker = [[UIImagePickerController alloc] init];
    cameraPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    cameraPicker.delegate = self;
    cameraPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
    [self.view addSubview:cameraPicker.view];
    
}
- (void)takePhoto_fcb:(id)sender
{
    FCBPhotosViewController * fcb = [self.storyboard instantiateViewControllerWithIdentifier:@"FCBPhotosViewController"];
    fcb.delegate = self ;
    [self.navigationController pushViewController:fcb animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [cameraPicker.view removeFromSuperview];
    [self openCropperWithImage:[self scaleImage:image]];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [cameraPicker.view removeFromSuperview];
}


-(void)initView
{

    self.fullName.delegate = self;
    self.email.delegate = self;
    self.passWord.delegate = self;
    self.confirmPass.delegate = self;
    
    
    CALayer * l = [self.fullNameView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:5];
    
    CALayer * l1 = [self.emailView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:5];
    
    CALayer * l2 = [self.passWordView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:5];
    
    CALayer * l3 = [self.confirmPassView layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:5];
    
    CALayer * l4 = [self.registerButton layer];
    [l4 setMasksToBounds:YES];
    [l4 setCornerRadius:5];
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addPhotot_Action:(id)sender {
    [self selectImageSource:sender];
}

- (IBAction)registerButton_Action:(id)sender {
    if (_socialProfile) {
        [self registerSocial];
    }
    else{
    
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
        if ([[self.fullName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"username_required"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if ([[self.email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Login_emailMissingAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if (![self isValidEmail:self.email.text])
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Registration_IncorrectEmailFormatAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if ([[self.passWord.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Login_PasswordMissingMlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        else if (![self.passWord.text isEqualToString:self.confirmPass.text]) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"ChangePassword_Password_confirmDontMatchAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }else if ([self.passWord.text length]<=4) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Registration_passwordLengthAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }else if (!userImage) {
            
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[StudioManager getInstance] getFCBPhotos:^(id currentClient) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                            if ([currentClient count]>0) {
                                int randomIndex = arc4random_uniform([currentClient count]);
                                
                                [_profileImage sd_setImageWithURL:[NSURL URLWithString:currentClient[randomIndex][@"url"]] placeholderImage:[UIImage imageNamed:@"avatar"]  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    userImage = image;
                                    imageUrl = currentClient[randomIndex][@"url"] ;
                                    [self registerButton_Action:nil];
                                }];
                                
                                

                            }
                        });
                    }onFailure:^(NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                            [alert show];
                        });
                    }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }

        }
        else
        {
            if ([RestServiceAgent internetAvailable]) {
                @try {
                    __block User* userObject = [[User alloc] init];
                    [[UsersManager getInstance] registerUser:self.fullName.text
                                                       email:self.email.text
                                                    password:self.passWord.text
                                                    photoURL:imageUrl
                                                       photo:userImage and:^(id currentClient) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           
                           [SVProgressHUD dismiss];
                           if(currentClient && [currentClient isKindOfClass:[User class]])
                           {
                               UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Registration_Congratulations"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                               [alert show];
                               userObject = (User*)currentClient;
                               if(userObject)
                               {
                                   
                                   [[CurrentUser getObject] setUser:userObject];
                                   [[CurrentUser getObject] saveCurrentUser];
                                   NSUserDefaults *defaults= [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
                                   [defaults setObject:@"justReg" forKey:@"justReg"];
                                   [defaults synchronize];
                                   

                                   if (locationFound) {
                                       [_delegate didRegSuccessfully:placemark];

                                   }
                                   else{
                                       [_delegate didRegSuccessfully:nil];

                                   }
                                   
                                   //[self.navigationController popViewControllerAnimated:YES];

                                   
                               }
                           }
                           
                       });
                   } onFailure:^(NSError *error)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [SVProgressHUD dismiss];
                             
                             UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                             [alert show];
                         });
                     }];
                }
                @catch (NSException *exception)
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.description delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });            }
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }
        }
    }
    
    
}
-(void)registerSocial
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    if ([[self.fullName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
    {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"username_required"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else if ([[self.email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
    {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Login_emailMissingAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else if (![self isValidEmail:self.email.text])
    {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Registration_IncorrectEmailFormatAlert"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else if (!userImage) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"register_photo_mandatory"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        if ([RestServiceAgent internetAvailable]) {
            @try {
                __block User* userObject = [[User alloc] init];
                [[UsersManager getInstance] loginSocialMedia:_socialProfile[@"userId"]
                                                       email:self.email.text
                                                    fullName:self.fullName.text
                                                       phone:@""
                                                     country:@""
                                                        city:@""
                                                    district:@""
                                                    birthday:@""
                                                      gender:_socialProfile[@"gender"]
                                               profilePicURL:_socialProfile[@"picUrl"]
                                                         and:^(id currentClient) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [SVProgressHUD dismiss];
                                         if(currentClient && [currentClient isKindOfClass:[User class]])
                                         {
                                             userObject = (User*)currentClient;
                                             if(userObject)
                                             {
                                                 
                                                 [[CurrentUser getObject] setUser:userObject];
                                                 [[CurrentUser getObject] saveCurrentUser];
                                                 NSUserDefaults *defaults= [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
                                                 [defaults setObject:@"justReg" forKey:@"justReg"];
                                                 [defaults synchronize];
                                                 
                                                 if (locationFound) {
                                                     [_delegate didRegSuccessfully:placemark];
                                                     
                                                 }
                                                 else{
                                                     [_delegate didRegSuccessfully:nil];
                                                     
                                                 }
                                                 //[self.navigationController popViewControllerAnimated:YES];
                                                 
                                             }
                                             
                                         }
                                         
                                     });
                                 } onFailure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                         
                         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                         [alert show];
                     });
                 }];
            }
            @catch (NSException *exception)
            {
                NSLog(@"nothing");
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }

}
-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate{
    return NO;
}

//-(NSUInteger)supportedInterfaceOrientations{
//    return UIInterfaceOrientationMaskPortrait;
//}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationMaskPortrait;
//}


-(void)didSelectImage:(UIImage *)image withUrl:(NSString *)url{
    imageUrl = url;
    _profileImage.image = image;
    userImage = image;
}

-(void)openCropperWithImage:(UIImage*)image{
    cropView = [[PECropView alloc] initWithFrame:self.view.bounds];
    cropView.image = image;
    
    UIView* mask = [[UIView alloc] initWithFrame:self.view.bounds];
    [mask setBackgroundColor:[UIColor blackColor]];
    UIButton *close = [[UIButton alloc] initWithFrame:CGRectMake(10,20,70, 30)];
    [close setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [close setTitle:@"Done" forState:UIControlStateNormal];
    [close setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [close addTarget:self action:@selector(closeCropper) forControlEvents:UIControlEventTouchUpInside];
    CALayer *l2 = [close layer];
    [l2 setMasksToBounds:YES];
    [l2 setBorderColor:[[UIColor whiteColor] CGColor]];
    [l2 setBorderWidth:2];
    [l2 setCornerRadius:5];
    
    
    [mask addSubview:cropView];
    [mask addSubview:close];
    [self.view addSubview:mask];
    
}
-(void)closeCropper{
    self.profileImage.image = cropView.croppedImage ;
    userImage = cropView.croppedImage;
    imageUrl = nil;
    [[self.view.subviews objectAtIndex:[self.view.subviews count]-1] removeFromSuperview];
}

-(void)uploadProfilePic:(UIImage*)profilePic toUser:(NSString*)userId
{
    if ([RestServiceAgent internetAvailable]) {
        @try {
            [[UsersManager getInstance] uploadUserPhoto:userId andImage:profilePic and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if(currentClient)
                    {
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Registration_Congratulations"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    
                });
            } onFailure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                     
                     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                     [alert show];
                 });
             }];
        }
        @catch (NSException *exception)
        {
            NSLog(@"nothing");
        }
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        });
    }
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    
    if (textField==_fullName) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
        return newLength <= MAXLENGTH || returnKey || [string isEqualToString:filtered];
    }
    return newLength <= MAXLENGTH || returnKey;
}
-(UIImage*) scaleImage:(UIImage*)image {
    
    CGSize scaledSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    CGFloat scaleFactor;
    
    if (image.size.width > image.size.height) {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.width = scaledSize.width;
        scaledSize.height = scaledSize.width * scaleFactor;
    } else {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.height = scaledSize.height;
        scaledSize.width = scaledSize.height * scaleFactor;
    }
    
    UIGraphicsBeginImageContext(scaledSize);
    [image drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    UIImage * scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}


@end
