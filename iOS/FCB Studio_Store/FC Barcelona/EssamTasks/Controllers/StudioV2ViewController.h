//
//  StudioV2ViewController.h
//  FC Barcelona
//
//  Created by Eissa on 9/25/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

typedef enum {
    playersTap = 1,
    teamTap,
    purchasedTap,
} tapType;

@interface StudioV2ViewController : GAITrackedViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segments;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIScrollView *segmentsView;
@property (weak, nonatomic) IBOutlet UIView *segmentsViewTap;

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

@property (weak, nonatomic) IBOutlet UIView *adBanner;

@property (weak, nonatomic) IBOutlet UILabel *needLoginLabel;
@property (weak, nonatomic) IBOutlet UIView *needLoginView;
@property (weak, nonatomic) IBOutlet UIButton *goLoginButton;


- (IBAction)openStudio:(id)sender;
- (IBAction)segmentChanged:(id)sender;
- (IBAction)back_Action:(id)sender;

- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

- (IBAction)filterButton_Action:(id)sender;

@end
