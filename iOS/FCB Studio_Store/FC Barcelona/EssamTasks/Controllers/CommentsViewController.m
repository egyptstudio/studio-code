//
//  CommentsViewController.m
//  FC Barcelona
//
//  Created by Eissa on 9/9/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "CommentsViewController.h"
#import "UserComment.h"
#import "WallManager.h"
#import "User.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "AvatarImageView.h"
#import "WebImage.h"
#import "GetAndPushCashedData.h"
#import "FXBlurView.h"
#import "CurrentUser.h"
@interface CommentsViewController () {
    id gSender;
}

@end

@implementation CommentsViewController
{
    NSMutableArray * comments;
    int pageNumber;
    BOOL kbOpendBefore;
    CGRect orgFrame;
}
@synthesize post;
@synthesize userID;
@synthesize userName;
@synthesize close;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    comments = [[NSMutableArray alloc] init];
    orgFrame = _tableView.frame;
    pageNumber=0;
    CALayer * l = [self.containerView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:6.0];
    
    CALayer * l1 = [self.tableView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:6.0];
    
    CALayer * l2 = [self.commentText layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:6.0];
    [_commentText setPlaceholder:[[Language sharedInstance] stringWithKey:@"Comments_fieldInitialValue"]];
    
    CALayer * l3 = [self.postButton layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:6.0];
    
    [self.postButton setTitle:[[Language sharedInstance] stringWithKey:@"Comments_post"] forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:@"UIKeyboardWillHideNotification"
                                               object:nil];
    self.commentText.delegate = self;
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEdit)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    [self.tableView addGestureRecognizer:tap];
    self.tableView.userInteractionEnabled = YES;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [_backGround drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}
- (void) keyboardWillShow:(NSNotification *)note {
    NSDictionary *userInfo = [note userInfo];
    CGSize  kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if(!kbOpendBefore)
    {
        [UIView animateWithDuration:0.2 animations:^{
            self.tableView.frame = CGRectMake(_tableView.frame.origin.x
                                              , _tableView.frame.origin.y
                                              , _tableView.frame.size.width
                                              ,_tableView.frame.size.height-kbSize.height+(_commentText.frame.size.height/2)-5);
            
        }];
        
        [_tableView reloadData];
        NSInteger numberOfRows = [_tableView numberOfRowsInSection:0];
        if (numberOfRows!=0) {
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        kbOpendBefore = YES;
    }
}
- (void) keyboardWillHide:(NSNotification *)note {
    [UIView animateWithDuration:0.2 animations:^{
        self.tableView.frame = orgFrame;
    }];
    [_tableView reloadData];
    NSInteger numberOfRows = [_tableView numberOfRowsInSection:0];
    if (numberOfRows!=0) {
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    kbOpendBefore = NO;
}

-(void)endEdit
{
    [self.view endEditing:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self getComments:[post postId]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getComments:(NSInteger)postid
{
    [self showLoadingindicator];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            pageNumber = [comments count] <= 20 ? ([comments count]/20) + 1 : 1;
            [[WallManager getInstance] getComments:postid pageNo:pageNumber and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _commentsCountLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)[currentClient count],[[Language sharedInstance] stringWithKey:@"notifications_comments"]];
                    if ([currentClient count]>0) {
                        comments = currentClient ;
                        [self prepareAndCacheComments];
                        [_tableView setHidden:NO];
                    }
                    else{
                        [self noCommentsFound];
                        [_tableView setHidden:YES];
                        pageNumber--;
                    }
                    [self dismissLoadingIndicator];
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismissLoadingIndicator];
                    [comments addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:[NSString stringWithFormat:@"Comments_%i",[post postId]]]];
                    
                    if ([comments count] > 0) {
                        [_tableView reloadData];
                    }
                    
                    pageNumber--;
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissLoadingIndicator];
                pageNumber--;
                [comments addObjectsFromArray:[GetAndPushCashedData getObjectFromCash:[NSString stringWithFormat:@"Comments_%i",[post postId]]]];
                
                if ([comments count] > 0) {
                    [_tableView reloadData];
                }
                
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        [self dismissLoadingIndicator];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
}
-(void)prepareAndCacheComments{
    [_tableView reloadData];
    NSInteger numberOfRows = [_tableView numberOfRowsInSection:0];
    if (numberOfRows!=0) {
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    _noCommentsView.hidden = YES;
    [GetAndPushCashedData cashObject:comments withAction:[NSString stringWithFormat:@"Comments_%i",[post postId]]];
}
-(void)comment
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateStyle:NSDateFormatterMediumStyle];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* selectedDate = [dateFormat stringFromDate:date];
    
    UserComment * comment = [[UserComment alloc] init];
    comment.userId = userID;
    comment.postId = [post postId];
    comment.commentText = self.commentText.text;
    comment.commentDate = [selectedDate longLongValue];
    
    
    [self showLoadingindicator];
    
    @try {
        
        if ([RestServiceAgent internetAvailable]) {
            if ([[self.commentText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
            {
                [self dismissLoadingIndicator];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"Comments_fieldInitialValue"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            else{
                [[WallManager getInstance] addComment:comment and:^(id currentClient) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            self.commentText.text = @"";
                            [self.delegate performSelector:@selector(commentAdded)];
                            [self getComments:[post postId]];
                        }
                    });
                } onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self dismissLoadingIndicator];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissLoadingIndicator];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        [self dismissLoadingIndicator];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
}

-(void)deleteComment:(id)sender{
    gSender = sender;
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"deleteComment"] delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:[[Language sharedInstance] stringWithKey:@"Cancel"]]
                                           otherButtonTitles: [[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"], nil];
    alert.tag = 2;
    [alert show];
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (alertView.tag == 2) {
            
            CGPoint buttonPosition = [gSender convertPoint:CGPointZero toView:self.tableView];
            NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
            UserComment *currentComment = (UserComment*)[comments objectAtIndex:indexPath.row];
            @try {
                [self showLoadingindicator];
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] deleteComment:currentComment.commentId postId:currentComment.postId  and:^(id currentClient) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (currentClient) {
                                self.commentText.text = @"";
                                [self.delegate performSelector:@selector(commentAdded)];
                                [self getComments:[post postId]];
                            }
                        });
                    } onFailure:^(NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self dismissLoadingIndicator];
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                            [alert show];
                        });
                    }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self dismissLoadingIndicator];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
            }
            @catch (NSException *exception) {
                [self dismissLoadingIndicator];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
    }
}




- (IBAction)post:(id)sender {
    [self comment];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [comments count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    UILabel * userNameLabel = (UILabel*)[cell viewWithTag:1];
    UILabel * userCommentLable    = (UILabel*)[cell viewWithTag:2];
    AvatarImageView * userImage    = (AvatarImageView*)[cell viewWithTag:3];
    UILabel * commentDateLabel = (UILabel*)[cell viewWithTag:5];
    UIButton  * commentDelete = (UIButton*)[cell viewWithTag:11];
    
    userNameLabel.text = [(UserComment*)[comments objectAtIndex:indexPath.row] fullName] ;
    userCommentLable.text = [(UserComment*)[comments objectAtIndex:indexPath.row] commentText];
    
    userImage.borderColor = [UIColor colorWithRed:(234/255.0) green:(200/255.0) blue:(92/255.0) alpha:1];
    userImage.borderWidth = 3.0;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* theDate = [NSDate dateWithTimeIntervalSince1970:[(UserComment*)[comments objectAtIndex:indexPath.row] commentDate]];
    
    commentDateLabel.text = [self relativeDateStringForDate:theDate];
    

    [userImage sd_setImageWithURL:[NSURL URLWithString:[(UserComment*)[comments objectAtIndex:indexPath.row] profilePicURL]] placeholderImage:[UIImage imageNamed:@"avatar"]];
    
    
//    dispatch_async(dispatch_get_main_queue(), ^{
        if([post userId ] == [[CurrentUser getObject] getUser].userId || [(UserComment*)[comments objectAtIndex:indexPath.row] userId] == [[CurrentUser getObject] getUser].userId )
        {
            [commentDelete setHidden:NO];
            [commentDelete addTarget:self action:@selector(deleteComment:) forControlEvents:UIControlEventTouchUpInside];
        }
//    });
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CALayer * l1 = [cell layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:6.0];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize textSize = [[(UserComment*)[comments objectAtIndex:indexPath.row] commentText] sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(tableView.frame.size.width-6, 20000) lineBreakMode: NSLineBreakByWordWrapping];
    return textSize.height+100;
}

-(void)dismissLoadingIndicator
{
    [SVProgressHUD dismiss];
}

-(void)showLoadingindicator
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([scrollView contentOffset].y + scrollView.frame.size.height) == [scrollView contentSize].height+6) {
        NSLog(@"scrolled to bottom");
        [self getComments:[post postId]];
        return;
    }
    
    
}
- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    return [dateFormat stringFromDate:date];
    
    //    NSCalendarUnit units = NSDayCalendarUnit | NSWeekOfYearCalendarUnit |
    //    NSMonthCalendarUnit | NSYearCalendarUnit;
    //
    //    // if `date` is before "now" (i.e. in the past) then the components will be positive
    //    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
    //                                                                   fromDate:date
    //                                                                     toDate:[NSDate date]
    //                                                                    options:0];
    //
    //    if (components.year > 0) {
    //        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
    //    } else if (components.month > 0) {
    //        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
    //    } else if (components.weekOfYear > 0) {
    //        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
    //    } else if (components.day > 0) {
    //        if (components.day > 1) {
    //            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
    //        } else {
    //            return @"Yesterday";
    //        }
    //    } else {
    //        return @"Today";
    //    }
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)noCommentsFound{
    _noCommentsLabel.text = [[Language sharedInstance] stringWithKey:@"FirstCommenter"];
    _noCommentsView.hidden = NO;
}
@end

