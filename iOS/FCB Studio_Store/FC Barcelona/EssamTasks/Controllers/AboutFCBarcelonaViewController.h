//
//  AboutFCBarcelonaViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/19/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutFCBarcelonaViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

- (IBAction)openStudio:(id)sender;
- (IBAction)back_Action:(id)sender;
- (IBAction)shop_Action:(id)sender;
- (IBAction)chat_Action:(id)sender;

@end
