//
//  EditProfile2ViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/14/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "EditProfile2ViewController.h"
#import "CurrentUser.h"
#import "MSPickerViewController.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "UsersManager.h"
@interface EditProfile2ViewController ()<UITextFieldDelegate,UITextViewDelegate,MSPickerViewControllerDelegate>

@end

@implementation EditProfile2ViewController
{
    int selectedRelationship;
    int selectedIncome;
    int selectedEducation;
    int selectedJob;
    int selectedJobRole;

    MSPickerViewController *pickerView;
    NSArray *relationships;
    NSArray *incomes;
    NSArray *educations;
    NSArray *jobs;
    NSArray *jobsRole;

    NSString *messagePlaceHolder;
}
@synthesize user;
@synthesize fromPoints;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _status.delegate = self;
    _aboutMe.delegate = self;
    _company.delegate = self;
    
    for (UIView *view in _mainScroll.subviews) {
        [_mainScroll setContentSize:CGSizeMake(0, _mainScroll.contentSize.height + view.frame.size.height+4)];
    }
    
    CALayer * l = [self.statusView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:3];
    
    CALayer * l1 = [self.aboutMeView layer];
    [l1 setMasksToBounds:YES];
    [l1 setCornerRadius:3];
    
    CALayer * l2 = [self.relationShipView layer];
    [l2 setMasksToBounds:YES];
    [l2 setCornerRadius:3];
    
    CALayer * l3 = [self.religionView layer];
    [l3 setMasksToBounds:YES];
    [l3 setCornerRadius:3];
    
    CALayer * l4 = [self.educationView layer];
    [l4 setMasksToBounds:YES];
    [l4 setCornerRadius:3];
    
    CALayer * l5 = [self.jopView layer];
    [l5 setMasksToBounds:YES];
    [l5 setCornerRadius:3];
    
    CALayer * l6 = [self.companyView layer];
    [l6 setMasksToBounds:YES];
    [l6 setCornerRadius:3];
    
    CALayer * l7 = [self.incomeView layer];
    [l7 setMasksToBounds:YES];
    [l7 setCornerRadius:3];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showRelationsPicker)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    [_relationShipView addGestureRecognizer:tap];
    _relationShipView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showIncomesPicker)];
    tap1.cancelsTouchesInView = YES;
    tap1.numberOfTapsRequired = 1;
    [_incomeView addGestureRecognizer:tap1];
    _incomeView .userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showEducationPicker)];
    tap2.cancelsTouchesInView = YES;
    tap2.numberOfTapsRequired = 1;
    [_educationView addGestureRecognizer:tap2];
    _educationView.userInteractionEnabled = YES;

    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showJobPicker)];
    tap3.cancelsTouchesInView = YES;
    tap3.numberOfTapsRequired = 1;
    [_jopView addGestureRecognizer:tap3];
    _jopView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showJobRolePicker)];
    tap4.cancelsTouchesInView = YES;
    tap4.numberOfTapsRequired = 1;
    [_jopRoleView addGestureRecognizer:tap4];
    _jopRoleView.userInteractionEnabled = YES;
    
    
    [self setLocalisables];
    [self initView];
}
- (void) keyboardWillShow:(NSNotification *)note {
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.mainScroll.contentSize = CGSizeMake(0, self.mainScroll.frame.size.height + kbSize.height);
}

- (void) keyboardWillHide:(NSNotification *)note {
    [UIView animateWithDuration:0.2 animations:^{
        self.mainScroll.contentSize = CGSizeMake(0,0);
    }];
}

-(void)initView
{
    
    selectedRelationship= [[[CurrentUser getObject] getUser] relationship];
    selectedIncome = [[[CurrentUser getObject] getUser] income];
    selectedEducation =[[[CurrentUser getObject] getUser] education];
    selectedJob =[[[CurrentUser getObject] getUser] job];
    selectedJobRole =[[[CurrentUser getObject] getUser] jobRole];

    
    relationships =@[[[Language sharedInstance] stringWithKey:@"relationship_list_item1"],
                     [[Language sharedInstance] stringWithKey:@"relationship_list_item2"],
                     [[Language sharedInstance] stringWithKey:@"relationship_list_item3"],
                     [[Language sharedInstance] stringWithKey:@"relationship_list_item4"]];
    
    incomes = @[[[Language sharedInstance] stringWithKey:@"income_list_item1"],
                [[Language sharedInstance] stringWithKey:@"income_list_item2"],
                [[Language sharedInstance] stringWithKey:@"income_list_item3"]];
    
    //////Education////////
    educations = [[Language sharedInstance] arrayWithKey:@"Educations"];
    _education.text = educations[selectedEducation];
    ////////////////////////
    //////Job////////
    jobs = [[Language sharedInstance] arrayWithKey:@"Jobs"];
    _jop.text = jobs[selectedJob];
    ////////////////////////
    //////Job////////
    jobsRole = [[Language sharedInstance] arrayWithKey:@"JobRoles"];
    _jopRole.text = jobsRole[selectedJobRole];
    ////////////////////////
    
    
    _status.text      = [[CurrentUser getObject]getUser].status;
   _aboutMe.text      = [[CurrentUser getObject]getUser].aboutMe;
    NSString *relationshipState;
    switch ([[CurrentUser getObject]getUser].relationship) {
        case 1:
            relationshipState = [[Language sharedInstance] stringWithKey:@"relationship_list_item1"];
            break;
        case 2:
            relationshipState = [[Language sharedInstance] stringWithKey:@"relationship_list_item2"];
            break;
        case 3:
            relationshipState = [[Language sharedInstance] stringWithKey:@"relationship_list_item3"];
            break;
        case 4:
            relationshipState = [[Language sharedInstance] stringWithKey:@"relationship_list_item4"];
            break;
        case 0:
            relationshipState = [[[Language sharedInstance] arrayWithKey:@"Educations"] objectAtIndex:0];
            break;
        default:
            break;
    }
   _relationShip.text = relationshipState;
   _company.text      = [[CurrentUser getObject]getUser].company;
    
    NSString *incomeState;
    switch ([[CurrentUser getObject]getUser].relationship) {
        case 1:
            incomeState = [[Language sharedInstance] stringWithKey:@"income_list_item1"];
            break;
        case 2:
            incomeState = [[Language sharedInstance] stringWithKey:@"income_list_item2"];
            break;
        case 3:
            incomeState = [[Language sharedInstance] stringWithKey:@"income_list_item3"];
            break;
        case 0:
            incomeState = [[[Language sharedInstance] arrayWithKey:@"Educations"] objectAtIndex:0] ;
            break;
        default:
            break;
    }
   _income.text       = incomeState;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)showRelationsPicker{
    [self.view endEditing:YES];
    pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                    forItems:relationships
                                                selectedItem:0];
    pickerView.delegate = self;
    pickerView.tag = 1;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];

}

-(void)showIncomesPicker{
    [self.view endEditing:YES];
    pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                    forItems:incomes
                                                selectedItem:0];
    pickerView.delegate = self;
    pickerView.tag =2;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
}

-(void)showEducationPicker{
    [self.view endEditing:YES];
    pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                    forItems:educations
                                                selectedItem:0];
    pickerView.delegate = self;
    pickerView.tag =3;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
}
-(void)showJobPicker{
    [self.view endEditing:YES];
    pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                    forItems:jobs
                                                selectedItem:0];
    pickerView.delegate = self;
    pickerView.tag =4;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
}
-(void)showJobRolePicker{
    [self.view endEditing:YES];
    pickerView = [MSPickerViewController pickerViewWithTitle:[[Language sharedInstance] stringWithKey:@"Select"]
                                                    forItems:jobsRole
                                                selectedItem:0];
    pickerView.delegate = self;
    pickerView.tag =5;
    [pickerView.doneButton setTitle:[[Language sharedInstance] stringWithKey:@"Done"] forState:UIControlStateNormal];
    [pickerView.cancelButton setTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] forState:UIControlStateNormal];
    [pickerView showPickerViewInView:self.view];
}
- (void)pickerView:(MSPickerViewController *)PickerView didSelectItem:(int)row {
    if (pickerView.tag == 1) {
        selectedRelationship = row+1;
        _relationShip.text = relationships[row];
    }
    else if (pickerView.tag == 2) {
        selectedIncome = row+1;
        _income.text = incomes[row];
    }else if (pickerView.tag == 3) {
        selectedEducation = row;
        _education.text = educations[row];
    }
    else if (pickerView.tag == 4) {
        selectedJob = row;
        _jop.text = jobs[row];
    }
    else if (pickerView.tag == 5) {
        selectedJobRole = row;
        _jopRole.text = jobsRole[row];
    }
}
- (void)pickerViewDidDismiss {
    pickerView = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)saveButton_Action:(id)sender{    
    [user setStatus:_status.text];
    [user setAboutMe:_aboutMe.text];
    [user setRelationship:selectedRelationship];
    [user setEducation:selectedEducation];
    [user setJob:selectedJob];
    [user setJobRole:selectedJobRole];
    [user setCompany:_company.text];
    [user setIncome:selectedIncome];
    
    @try {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if([RestServiceAgent internetAvailable])
        {
            [[UsersManager getInstance] updateUserProfile:user witImage:nil and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (currentClient) {
                        [SVProgressHUD dismiss];
                        if([currentClient isKindOfClass:[User class]])
                        {
                            [[CurrentUser getObject] setUser:(User*)currentClient];
                            [[CurrentUser getObject] saveCurrentUser];
                            [self initView];
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                                            message:[[Language sharedInstance] stringWithKey:@"DoneEdit"]
                                                                           delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                            [alert show];
                            if (!fromPoints) {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
                            }                            
                        }
                        else{
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Server Error." delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                            [alert show];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
        
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    

}

-(void)setLocalisables{
    [_pageTitle setText:[[Language sharedInstance] stringWithKey:@"User_Profile"]];
    [_statusTitle setText:[[Language sharedInstance] stringWithKey:@"edit_more_status"]];
    [_aboutTitle setText:[[Language sharedInstance] stringWithKey:@"edit_more_about_me"]];
    [_relationShipTitle setText:[[Language sharedInstance] stringWithKey:@"edit_more_relationship"]];
    [_educationTitle setText:[[Language sharedInstance] stringWithKey:@"edit_more_education"]];
    [_jobTitle setText:[[Language sharedInstance] stringWithKey:@"edit_more_job"]];
    [_jobRoleTitle setText:[[Language sharedInstance] stringWithKey:@"JobRole"]];
    [_companyTitle setText:[[Language sharedInstance] stringWithKey:@"edit_more_company"]];
    [_incomeTitle setText:[[Language sharedInstance] stringWithKey:@"edit_more_income"]];
    
    _status.placeholder = @"";
    _education.text = @"";
    _jop.text = @"";
    _company.placeholder = @"";
    _aboutMe.text = @"";
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
@end
