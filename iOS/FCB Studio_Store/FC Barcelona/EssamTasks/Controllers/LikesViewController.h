//
//  LikesViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/14/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
@interface LikesViewController : UIViewController

@property(assign)Post* currentPost;
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *likesCount;
- (IBAction)back_Action:(id)sender;
@end
