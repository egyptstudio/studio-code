//
//  FullScreenPageViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/28/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FullScreenPageViewController.h"
#import "FullScreenCell.h"
#import "WebImage.h"
#import "RestServiceAgent.h"
#import "WallManager.h"
#import "CurrentUser.h"
#import "User.h"
#import "SVProgressHUD.h"


@interface FullScreenPageViewController ()

@end

@implementation FullScreenPageViewController
{
    Post* currentPost;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close_Action:(id)sender {
}
- (IBAction)likeButton_Action:(id)sender {
}

- (IBAction)info_Action:(id)sender {
}
-(void)viewPost:(Post*)post{
    currentPost = post;
    
    _userPhoto.borderColor = [UIColor whiteColor];
    
    [_userPhoto sd_setImageWithURL:[NSURL URLWithString:[currentPost userProfilePicUrl]] placeholderImage:[UIImage imageNamed:@"avatar"]];
    
    [_postPhoto sd_setImageWithURL:[NSURL URLWithString:[currentPost postPhotoUrl]]];


    _userName.text = [currentPost userName];
    
    _likesNumber.text =[currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
    

    if ([currentPost  liked]) {
        [_likeButton setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
    }
    else {
        [_likeButton setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
    }

}

@end
