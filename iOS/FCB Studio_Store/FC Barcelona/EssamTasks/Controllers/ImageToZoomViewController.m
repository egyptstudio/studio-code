//
//  ImageToZoomViewController.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 10/11/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "ImageToZoomViewController.h"

@interface ImageToZoomViewController ()

@end

@implementation ImageToZoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+64, self.view.frame.size.width, self.view.frame.size.height-64)];
    self.mainZoomView.backgroundColor = [UIColor blackColor];
    self.mainZoomView.delegate = self;
    float tempRatio=self.image.size.width/self.image.size.height;
    self.image=[self imageWithImage:self.image scaledToSize:CGSizeMake(320, 320*(1/tempRatio))];
    imageV = [[UIImageView alloc] initWithImage:self.image];
    [imageV setContentMode:UIViewContentModeScaleAspectFit];
    self.mainZoomView.contentSize = imageV.frame.size;
    [self.mainZoomView addSubview:imageV];
    
    //scroll.minimumZoomScale = scroll.frame.size.width / image.frame.size.width;
    self.mainZoomView.maximumZoomScale = 3.0;
    self.mainZoomView.minimumZoomScale = 1.0;
    
    [self.mainZoomView setZoomScale:self.mainZoomView.minimumZoomScale];
    [self.mainZoomView setBackgroundColor:[UIColor clearColor]];
    // [self.view addSubview:scroll];
}
-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
//- (void)loadView {
//
//    //[self.view bringSubviewToFront:scroll];
//
//
//}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+64, self.view.frame.size.width, self.view.frame.size.height-64)];
    //    scroll.backgroundColor = [UIColor blackColor];
    //    scroll.delegate = self;
    //    imageV = [[UIImageView alloc] initWithImage:self.image];
    //    scroll.contentSize = imageV.frame.size;
    //    [scroll addSubview:imageV];
    //
    //    //scroll.minimumZoomScale = scroll.frame.size.width / image.frame.size.width;
    //    scroll.maximumZoomScale = 2.0;
    //    [scroll setZoomScale:scroll.minimumZoomScale];
    //    [scroll setBackgroundColor:[UIColor clearColor]];
    //    [self.view addSubview:scroll];
}
- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
    CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
    
    return frameToCenter;
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    imageV.frame = [self centeredFrameForScrollView:scrollView andUIView:imageV];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageV;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(BOOL)shouldAutorotate{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
    
}


- (IBAction)back_Action:(id)sender {
    objc_msgSend([UIDevice currentDevice], @selector(setOrientation:),UIInterfaceOrientationPortrait ); //cportrait
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
