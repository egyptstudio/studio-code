//
//  StudioFilterViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 1/12/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudioFilterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *latestButton;
@property (weak, nonatomic) IBOutlet UIButton *mostButton;
@property (weak, nonatomic) IBOutlet UIButton *applyButton;
@property (weak, nonatomic) IBOutlet UICollectionView *seasonTableView;

@property(weak, nonatomic) IBOutlet UILabel *pageTitle;
@property(weak, nonatomic) IBOutlet UILabel *latestLabel;
@property(weak, nonatomic) IBOutlet UILabel *mostLabel;
@property(weak, nonatomic) IBOutlet UILabel *sortBy;

- (IBAction)latestButton_Action:(id)sender;
- (IBAction)mostButton_Action:(id)sender;
- (IBAction)applyButton_Action:(id)sender;

- (IBAction)back_Action:(id)sender;

@end
