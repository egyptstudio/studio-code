//
//  FCBPhotosViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/15/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FCBPhotosDelegate <NSObject>
@required
- (void)didSelectImage:(UIImage*)image withUrl:(NSString*)url;
@end
@interface FCBPhotosViewController : UIViewController
@property (nonatomic,assign) id <FCBPhotosDelegate> delegate;

- (IBAction)back_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collection;
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@end
