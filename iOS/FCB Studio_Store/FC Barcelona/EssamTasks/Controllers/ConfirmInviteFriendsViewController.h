//
//  ConfirmInviteFriendsViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/24/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmInviteFriendsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *siteUrl;
@property (weak, nonatomic) IBOutlet UILabel *smsMessage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIView *messageView;
@property (assign, nonatomic) NSArray* phoneNumbers;

- (IBAction)back_Action:(id)sender;
- (IBAction)done_Action:(id)sender;
@end
