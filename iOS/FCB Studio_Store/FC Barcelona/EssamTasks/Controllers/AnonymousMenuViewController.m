//
//  AnonymousMenuViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/1/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "AnonymousMenuViewController.h"
#import "GetAndPushCashedData.h"
@interface AnonymousMenuViewController ()

@end

@implementation AnonymousMenuViewController
{
    UIDynamicAnimator* animator;
    UIGravityBehavior *gravityBehaviour;
    UIPushBehavior *pushBehavior;
    UICollisionBehavior *collision;
    UIPanGestureRecognizer * dragGesture;
    UIAttachmentBehavior *panAttachmentBehaviour;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(animationStopped)
                                                name:@"animationStopped" object:nil];
    
    UITapGestureRecognizer *fcbTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openHome:)];
    fcbTap.cancelsTouchesInView = YES;
    fcbTap.numberOfTapsRequired = 1;
    [self.fcbView addGestureRecognizer:fcbTap];
    self.fcbView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *fansTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFans:)];
    fansTap.cancelsTouchesInView = YES;
    fansTap.numberOfTapsRequired = 1;
    [self.fansView addGestureRecognizer:fansTap];
    self.fansView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *contestTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openContest:)];
    contestTap.cancelsTouchesInView = YES;
    contestTap.numberOfTapsRequired = 1;
    [self.contestView addGestureRecognizer:contestTap];
    self.contestView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *friendsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFriends:)];
    friendsTap.cancelsTouchesInView = YES;
    friendsTap.numberOfTapsRequired = 1;
    [self.friendsView addGestureRecognizer:friendsTap];
    self.friendsView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *sponsorsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSponsers:)];
    sponsorsTap.cancelsTouchesInView = YES;
    sponsorsTap.numberOfTapsRequired = 1;
    [self.sponsorsView addGestureRecognizer:sponsorsTap];
    self.sponsorsView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *profileTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProfile:)];
    profileTap.cancelsTouchesInView = YES;
    profileTap.numberOfTapsRequired = 1;
    [self.profileView addGestureRecognizer:profileTap];
    self.profileView.userInteractionEnabled = YES;
    
    [self setLocalisables];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)closeMenu:(id)sender {
    [UIView animateWithDuration:0.6f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}
- (void)dragMenu:(UIPanGestureRecognizer *)sender
{
    
    
    CGPoint location = [sender locationInView:sender.view];
    location.x = CGRectGetMidX(self.view.bounds);
    CGPoint velocity = [sender velocityInView:self.view];
    
    if (sender.state == UIGestureRecognizerStateBegan) {
    }
    else if (sender.state == UIGestureRecognizerStateChanged) {
        if (velocity.y > 0) { // down
            CGPoint newCenter = sender.view.center;
            newCenter.y += [sender translationInView:self.view].y;
            if (newCenter.y + (self.view.frame.size.height/2) <= [UIScreen mainScreen].bounds.size.height) {
                self.view.center = newCenter;
            }
            [dragGesture setTranslation:CGPointZero inView:self.view];
        } else if (velocity.y < 0) { //up
            CGPoint newCenter = sender.view.center;
            newCenter.y += [sender translationInView:self.view].y;
            self.view.center = newCenter;
            [dragGesture setTranslation:CGPointZero inView:self.view];
        }
        
    }
    else if (sender.state == UIGestureRecognizerStateEnded) {
        
        if (velocity.y > 0) {
            CGFloat yPoints = [UIScreen mainScreen].bounds.size.width;
            NSTimeInterval duration = yPoints / velocity.y;
            [UIView animateWithDuration:duration animations:^{
                self.view.frame =CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
            } completion:^(BOOL finished) {
            }];
        }
        else {
            [UIView animateWithDuration:0.5f animations:^{
                self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
            } completion:^(BOOL finished) {
                [self willMoveToParentViewController:nil];
                [self.view removeFromSuperview];
                [self removeFromParentViewController];
            }];
        }
        
    }
}

-(void)animationStopped{
    dragGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragMenu:)];
    dragGesture.minimumNumberOfTouches = 1;
    dragGesture.maximumNumberOfTouches = 1;
    [self.view addGestureRecognizer:dragGesture];
    
    if (![[GetAndPushCashedData getObjectFromCash:@"menuOpened"] boolValue]) {
        [self closeMenu:nil];
        [GetAndPushCashedData cashObject:@(YES) withAction:@"menuOpened"];
    }
}

-(void)hideview
{
    self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (IBAction)openProfile:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

- (IBAction)openFans:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openFans" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
- (IBAction)openContest:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openContest" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
- (IBAction)openFriends:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openFriends" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        
    }];
}
- (IBAction)openSponsers:(id)sender{}
- (IBAction)openHome:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openHome" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
-(void)setLocalisables{
    [_pageTitle     setText:[[Language sharedInstance] stringWithKey:@"app_name"]];
    [_profileLabel setText:[[Language sharedInstance] stringWithKey:@"User_Profile"]];
    [_fcbLabel      setText:[[Language sharedInstance] stringWithKey:@"app_name"]];
    [_fansLabel     setText:[[Language sharedInstance] stringWithKey:@"menu_Fans"]];
    [_contestLabel  setText:[[Language sharedInstance] stringWithKey:@"menu_Contest"]];
    [_friendsLabel  setText:[[Language sharedInstance] stringWithKey:@"menu_Friends"]];
    [_sponsorsLabel setText:[[Language sharedInstance] stringWithKey:@"menu_Sponsors"]];
    [(UILabel*)[self.view viewWithTag:1] setText:[[Language sharedInstance] stringWithKey:@"menu_registerMotive"]];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    [UIView animateWithDuration:0.2f animations:^{
        self.view.frame =CGRectMake(0, 0-self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
@end
