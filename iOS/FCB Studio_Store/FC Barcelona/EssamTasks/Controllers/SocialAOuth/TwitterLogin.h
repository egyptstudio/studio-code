//
//  TwitterLogin.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAConsumer.h"
#import "OAMutableURLRequest.h"
#import "OADataFetcher.h"
@protocol TwitterLoginDelegate <NSObject>
@required
- (void)twitterFinishedWithAuth:(NSDictionary *)user error: (NSError *)error;
- (void)twitterFinishedTweeting:(NSDictionary *)user error: (NSError *)error;
@end

@interface TwitterLogin : NSObject
@property (nonatomic,assign) id <TwitterLoginDelegate> delegate;
+(TwitterLogin *)getInstance;
-(BOOL)handleURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation: (id)annotation;
-(void)openLogger;
-(BOOL)hasOpenSession;
-(void)sendTweetWithStatus:(NSString*)status;
@end
