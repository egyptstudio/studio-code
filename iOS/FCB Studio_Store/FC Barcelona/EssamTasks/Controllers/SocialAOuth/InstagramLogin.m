//
//  InstagramLogin.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "InstagramLogin.h"
#import "OAuthCore.h"
#import "OAuth+Additions.h"
#import "NSData+Base64.h"
#import <CommonCrypto/CommonHMAC.h>
#import "JSONKit.h"

static InstagramLogin* instance;
@implementation InstagramLogin
{
    OAConsumer* consumer;
    OAToken* requestToken;
    OAToken* accessToken;
    NSString *isLogin;
    UIActivityIndicatorView *indicator;
    UIButton *close ;
    
    NSString *client_id ;
    NSString *secret ;
    NSString *callback;
    NSString *scope;
    NSString *visibleactions;


}
+(InstagramLogin *)getInstance{
    if (!instance) {
        instance=[[InstagramLogin alloc] init];
    }
    return instance;
}

-(id)init
{
    self = [super init];
    if (self) {
        client_id = @"fc576ff321034bcea10bb2b90b177b52";
        secret = @"9dae7cc39a124505af8eb68c0372cc73";
        callback = @"FCBS-Schema://";
        scope = @"basic";

       // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(urlRecived:) name:@"urlRecived" object:nil];

    }
    return self;
}
-(void)openLogger{
    
    [self openView];
}

- (void)openView {
//initWithFormat:@"%@/?client_id=%@&redirect_uri=%@&scope=%@&response_type=token&display=touch",

    NSURL* authorizeUrl = [NSURL URLWithString:@"https://api.instagram.com/oauth/authorize/"];
    OAMutableURLRequest* authorizeRequest = [[OAMutableURLRequest alloc] initWithURL:authorizeUrl
                                                                            consumer:nil
                                                                               token:nil
                                                                               realm:nil
                                                                   signatureProvider:nil];
    
    
    OARequestParameter* clientid = [[OARequestParameter alloc] initWithName:@"client_id" value:client_id];
    OARequestParameter* response_type = [[OARequestParameter alloc] initWithName:@"response_type" value:@"code"];
    OARequestParameter* _scope = [[OARequestParameter alloc] initWithName:@"scope" value:scope];
    OARequestParameter* redirect_uri = [[OARequestParameter alloc] initWithName:@"redirect_uri" value:callback];
    OARequestParameter* display = [[OARequestParameter alloc] initWithName:@"display" value:@"display"];
    
    [authorizeRequest setHTTPMethod:@"GET"];
    [authorizeRequest setParameters:[NSArray arrayWithObjects:
                                     clientid,
                                     response_type,
                                     _scope,
                                     redirect_uri,
                                     display,nil]];
    
     [[UIApplication sharedApplication] openURL:[authorizeRequest URL]];

}


- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    [indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles:nil] show];
}

- (void)urlRecived:(NSURL*)url
{
    NSString* verifier = nil;
    NSArray* urlParams = [[url resourceSpecifier] componentsSeparatedByString:@"="];
    verifier = [urlParams objectAtIndex:1];

    
    if (verifier) {
        
        NSURL* accessTokenUrl = [NSURL URLWithString:@"https://api.instagram.com/oauth/access_token/"];
        OAMutableURLRequest* accessTokenRequest = [[OAMutableURLRequest alloc] initWithURL:accessTokenUrl
                                                                                  consumer:nil
                                                                                     token:nil
                                                                                     realm:nil
                                                                         signatureProvider:nil];
        
        
        OARequestParameter* code          = [[OARequestParameter alloc] initWithName:@"code" value:verifier];
        OARequestParameter* clientid     = [[OARequestParameter alloc] initWithName:@"client_id" value:client_id];
        OARequestParameter* client_secret = [[OARequestParameter alloc] initWithName:@"client_secret" value:secret];
        OARequestParameter* grant_type  = [[OARequestParameter alloc] initWithName:@"grant_type" value:@"authorization_code"];
        OARequestParameter* _redirect = [[OARequestParameter alloc] initWithName:@"redirect_uri" value:callback];

        [accessTokenRequest setHTTPMethod:@"POST"];
        [accessTokenRequest setParameters:[NSArray arrayWithObjects:code,
                                           clientid,
                                           client_secret,
                                           grant_type,
                                           _redirect,nil]];
        
        OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
        [dataFetcher fetchDataWithRequest:accessTokenRequest
                                 delegate:self
                        didFinishSelector:@selector(didReceiveAccessToken:data:)
                          didFailSelector:@selector(didFailOAuth:error:)];
    }
    
}

- (void)didReceiveAccessToken:(OAServiceTicket*)ticket data:(NSData*)data {
    JSONDecoder *decoder = [JSONDecoder decoder];
    NSUserDefaults* defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    [defaults setObject:[decoder objectWithData:data] forKey:@"iObject"];
    [defaults synchronize];
    [_delegate instagramFinishedWithAuth:[decoder objectWithData:data] error:nil];
    
}

- (void)didFailOAuth:(OAServiceTicket*)ticket error:(NSError*)error {
    [_delegate instagramFinishedWithAuth:nil error:error];

}
-(BOOL)handleURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation: (id)annotation{
    if ([[url scheme] isEqualToString:@"fcbs-schema"]) {
        
        [self urlRecived:url];
        return YES;
    }
    return NO;
}


@end
