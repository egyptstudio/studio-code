//
//  TwitterLogin.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAConsumer.h"
#import "OAMutableURLRequest.h"
#import "OADataFetcher.h"
@protocol GooglePlusLoginDelegate <NSObject>
@required
- (void)didReceiveGoogleUser:(NSDictionary*)user;
- (void)didReceiveError:(NSError*)error;

@end

@interface GooglePlusLogin : NSObject<UIApplicationDelegate>
@property (nonatomic,assign) id <GooglePlusLoginDelegate> delegate;

-(void)openLogger;
@end
