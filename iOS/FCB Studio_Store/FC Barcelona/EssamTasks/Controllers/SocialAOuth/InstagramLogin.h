//
//  InstagramLogin.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAConsumer.h"
#import "OAMutableURLRequest.h"
#import "OADataFetcher.h"
@protocol InstagramLoginDelegate <NSObject>
@required
- (void)instagramFinishedWithAuth:(NSDictionary *)user error: (NSError *)error;
@end

@interface InstagramLogin : NSObject<UIApplicationDelegate>
@property (nonatomic,assign) id <InstagramLoginDelegate> delegate;
+(InstagramLogin *)getInstance;
-(void)openLogger;
-(BOOL)handleURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation: (id)annotation;
@end
