//
//  TwitterLogin.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "TwitterLogin.h"
#import "OAuthCore.h"
#import "OAuth+Additions.h"
#import "NSData+Base64.h"
#import <CommonCrypto/CommonHMAC.h>
#import "JSONKit.h"
#import "SVProgressHUD.h"

static TwitterLogin* instance;

@implementation TwitterLogin
{

    OAConsumer* consumer;
    OAToken* requestToken;
    OAToken* accessToken;
    NSString *isLogin;
    UIActivityIndicatorView *indicator;
    UIButton *close ;
    NSString *client_id ;
    NSString *secret ;
    NSString *callback;
    
    
}
+(TwitterLogin *)getInstance{
    if (!instance) {
        instance=[[TwitterLogin alloc] init];
    }
    return instance;
}

- (id)init
{
    self = [super init];
    if (self) {
        client_id = @"Rh1z5ZIN38sDNlUlQ37aR3mSn";
        secret = @"NV0h2zafUyyrpkIFZMsOEVfBEoQroJgRjB7jzUq19D0HHcoWnN";
        callback = @"com.tawasol.FC-Barcelonaa:/oauth2Callback";
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(urlRecived:) name:@"urlRecived" object:nil];

    }
    return self;
}
-(void)openLogger{
    
    [self openView];
}

- (void)openView {

    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];

    consumer = [[OAConsumer alloc] initWithKey:client_id secret:secret realm:nil];
    NSURL* requestTokenUrl = [NSURL URLWithString:@"https://api.twitter.com/oauth/request_token"];
    OAMutableURLRequest* requestTokenRequest = [[OAMutableURLRequest alloc] initWithURL:requestTokenUrl
                                                                               consumer:consumer
                                                                                  token:nil
                                                                                  realm:nil
                                                                      signatureProvider:nil];
    OARequestParameter* callbackParam = [[OARequestParameter alloc] initWithName:@"oauth_callback" value:callback];
    [requestTokenRequest setHTTPMethod:@"POST"];
    [requestTokenRequest setParameters:[NSArray arrayWithObject:callbackParam]];
    OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
    [dataFetcher fetchDataWithRequest:requestTokenRequest
                             delegate:self
                    didFinishSelector:@selector(didReceiveRequestToken:data:)
                      didFailSelector:@selector(didFailOAuth:error:)];
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    [indicator startAnimating];
    NSString *temp = [NSString stringWithFormat:@"%@",request];
    NSRange textRange = [[temp lowercaseString] rangeOfString:[@"com.tawasol.FC-Barcelonaa:/oauth2Callback" lowercaseString]];
    if(textRange.location != NSNotFound){
        NSString* verifier = nil;
        NSArray* urlParams = [[[request URL] query] componentsSeparatedByString:@"&"];
        for (NSString* param in urlParams) {
            NSArray* keyValue = [param componentsSeparatedByString:@"="];
            NSString* key = [keyValue objectAtIndex:0];
            if ([key isEqualToString:@"oauth_verifier"]) {
                verifier = [keyValue objectAtIndex:1];
                break;
            }
        }
        
        if (verifier) {
            NSURL* accessTokenUrl = [NSURL URLWithString:@"https://api.twitter.com/oauth/access_token"];
            OAMutableURLRequest* accessTokenRequest = [[OAMutableURLRequest alloc] initWithURL:accessTokenUrl consumer:consumer token:requestToken realm:nil signatureProvider:nil];
            OARequestParameter* verifierParam = [[OARequestParameter alloc] initWithName:@"oauth_verifier" value:verifier];
            [accessTokenRequest setHTTPMethod:@"POST"];
            [accessTokenRequest setParameters:[NSArray arrayWithObject:verifierParam]];
            OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
            [dataFetcher fetchDataWithRequest:accessTokenRequest
                                     delegate:self
                            didFinishSelector:@selector(didReceiveAccessToken:data:)
                              didFailSelector:@selector(didFailOAuth:error:)];
        } else {
            // ERROR!
        }
        
        return NO;
    }
    
    return YES;
}


- (void)didReceiveRequestToken:(OAServiceTicket*)ticket data:(NSData*)data {
    NSString* httpBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    requestToken = [[OAToken alloc] initWithHTTPResponseBody:httpBody];
    
    NSURL* authorizeUrl = [NSURL URLWithString:@"https://api.twitter.com/oauth/authorize"];
    OAMutableURLRequest* authorizeRequest = [[OAMutableURLRequest alloc] initWithURL:authorizeUrl
                                                                            consumer:nil
                                                                               token:nil
                                                                               realm:nil
                                                                   signatureProvider:nil];
    NSString* oauthToken = requestToken.key;
    OARequestParameter* oauthTokenParam = [[OARequestParameter alloc] initWithName:@"oauth_token" value:oauthToken];
    [authorizeRequest setParameters:[NSArray arrayWithObject:oauthTokenParam]];
    //[SVProgressHUD dismiss];
    [[UIApplication sharedApplication] openURL:[authorizeRequest URL]];

}

- (void)urlRecived:(NSURL*)url
{
        NSString* verifier = nil;
        NSArray* urlParams = [[url query] componentsSeparatedByString:@"&"];
        for (NSString* param in urlParams) {
            NSArray* keyValue = [param componentsSeparatedByString:@"="];
            NSString* key = [keyValue objectAtIndex:0];
            if ([key isEqualToString:@"oauth_verifier"]) {
                verifier = [keyValue objectAtIndex:1];
                break;
            }
        }
        
        if (verifier) {
            NSURL* accessTokenUrl = [NSURL URLWithString:@"https://api.twitter.com/oauth/access_token"];
            OAMutableURLRequest* accessTokenRequest = [[OAMutableURLRequest alloc] initWithURL:accessTokenUrl consumer:consumer token:requestToken realm:nil signatureProvider:nil];
            OARequestParameter* verifierParam = [[OARequestParameter alloc] initWithName:@"oauth_verifier" value:verifier];
            [accessTokenRequest setHTTPMethod:@"POST"];
            [accessTokenRequest setParameters:[NSArray arrayWithObject:verifierParam]];
            OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
            [dataFetcher fetchDataWithRequest:accessTokenRequest
                                     delegate:self
                            didFinishSelector:@selector(didReceiveAccessToken:data:)
                              didFailSelector:@selector(didFailOAuth:error:)];
        } else {
            [_delegate twitterFinishedWithAuth:nil error:[NSError errorWithDomain:@"Access Denied" code:1 userInfo:nil]];
        }
}
- (void)didReceiveAccessToken:(OAServiceTicket*)ticket data:(NSData*)data {
    NSString* httpBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    accessToken = [[OAToken alloc] initWithHTTPResponseBody:httpBody];
    NSURL *url = [[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
    NSString *_oAuthNonce = [NSString ab_GUID];
    NSString *_oAuthTimestamp = [NSString stringWithFormat:@"%d", (int)[[NSDate date] timeIntervalSince1970]];
    
    OAMutableURLRequest* userDataRequest = [[OAMutableURLRequest alloc] initWithURL:url
                                                                           consumer:consumer token:accessToken realm:nil signatureProvider:nil
                                                                              nonce:_oAuthNonce
                                                                          timestamp:_oAuthTimestamp];
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    [defaults setObject:httpBody forKey:@"TwitterToken"];
    [defaults synchronize];
    
    OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
    [dataFetcher fetchDataWithRequest:userDataRequest
                             delegate:self
                    didFinishSelector:@selector(didReceiveUserData:data:)
                      didFailSelector:@selector(didFailOAuth:error:)];
    
    
}


- (void)didReceiveUserData:(OAServiceTicket*)ticket data:(NSData*)data {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    JSONDecoder *decoder = [JSONDecoder decoder];
    [_delegate twitterFinishedWithAuth:[decoder objectWithData:data] error:nil];
    
}

- (void)didFailOAuth:(OAServiceTicket*)ticket error:(NSError*)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    
    [_delegate twitterFinishedWithAuth:nil error:error];
}

-(BOOL)handleURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation: (id)annotation{
    if ([[url scheme] isEqualToString:@"com.tawasol.fc-barcelonaa"]) {
        
        [self urlRecived:url];
        return YES;
    }
    return NO;
}

-(BOOL)hasOpenSession{
    return NO;
}

-(void)sendTweetWithStatus:(NSString*)status{
        NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
        requestToken = [[OAToken alloc] initWithHTTPResponseBody:[defaults objectForKey:@"TwitterToken"]];
        NSURL *url = [[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
        NSString *_oAuthNonce = [NSString ab_GUID];
        NSString *_oAuthTimestamp = [NSString stringWithFormat:@"%d", (int)[[NSDate date] timeIntervalSince1970]];
        
        OAMutableURLRequest* userDataRequest = [[OAMutableURLRequest alloc] initWithURL:url
                                                                               consumer:consumer token:accessToken realm:nil signatureProvider:nil
                                                                                  nonce:_oAuthNonce
                                                                              timestamp:_oAuthTimestamp];
        OARequestParameter* statusParam = [[OARequestParameter alloc] initWithName:@"status" value:status];
        [userDataRequest setHTTPMethod:@"POST"];
        [userDataRequest setParameters:[NSArray arrayWithObject:statusParam]];
        
        OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
        [dataFetcher fetchDataWithRequest:userDataRequest
                                 delegate:self
                        didFinishSelector:@selector(didTweet:data:)
                          didFailSelector:@selector(didFailTweet:error:)];
    
}
- (void)didTweet:(OAServiceTicket*)ticket data:(NSData*)data {
    JSONDecoder *decoder = [JSONDecoder decoder];
    NSLog(@"Status===>%@",[decoder objectWithData:data]);
    //[_delegate twitterFinishedTweeting:[decoder objectWithData:data] error:nil];
}

- (void)didFailTweet:(OAServiceTicket*)ticket error:(NSError*)error {
    [_delegate twitterFinishedTweeting:nil error:error];
}

@end
