//
//  FacebookLogin.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FacebookLogin.h"
#import "OAuthCore.h"
#import "OAuth+Additions.h"
#import "NSData+Base64.h"
#import <CommonCrypto/CommonHMAC.h>
#import "JSONKit.h"

@implementation FacebookLogin
{
    OAConsumer* consumer;
    OAToken* requestToken;
    OAToken* accessToken;
    NSString *isLogin;
    UIActivityIndicatorView *indicator;
    UIButton *close ;
    
    NSString *client_id ;
    NSString *secret ;
    NSString *callback;
    NSString *scope;
    NSString *visibleactions;
    NSString *input_token;
}

-(id)init
{
    self = [super init];
    if (self) {
        client_id = @"277335509139670";
        secret = @"9f0f381f265dffeca06491101d7c729b";
        callback = @"http://api.tawasoldev.com/barca/index.php/api/barca/fb_log";
        scope = @" public_profile";
        input_token = @"277335509139670|9f0f381f265dffeca06491101d7c729b";

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(urlRecived:) name:@"urlRecived" object:nil];

    }
    return self;
}
-(void)openLogger{
    
    [self openView];
}

- (void)openView {
    NSURL* authorizeUrl = [NSURL URLWithString:@"https://www.facebook.com/dialog/oauth"];
    OAMutableURLRequest* authorizeRequest = [[OAMutableURLRequest alloc] initWithURL:authorizeUrl
                                                                            consumer:nil
                                                                               token:nil
                                                                               realm:nil
                                                                   signatureProvider:nil];
    
    NSString *_oAuthNonce = [NSString ab_GUID];

    OARequestParameter* clientid = [[OARequestParameter alloc] initWithName:@"app_id" value:client_id];
    OARequestParameter* esponse_type = [[OARequestParameter alloc] initWithName:@"response_type" value:@"code"];
    OARequestParameter* _scope = [[OARequestParameter alloc] initWithName:@"scope" value:scope];
    OARequestParameter* redirect_uri = [[OARequestParameter alloc] initWithName:@"redirect_uri" value:callback];
    OARequestParameter* auth_type = [[OARequestParameter alloc] initWithName:@"auth_type" value:@"reauthenticate"];
    OARequestParameter* auth_nonce = [[OARequestParameter alloc] initWithName:@"auth_nonce" value:_oAuthNonce];

    
    [authorizeRequest setHTTPMethod:@"GET"];
    [authorizeRequest setParameters:[NSArray arrayWithObjects:
                                     clientid,
                                     esponse_type,
                                     _scope,
                                     auth_type,
                                     redirect_uri,
                                     auth_nonce,
                                     nil]];
    
     [[UIApplication sharedApplication] openURL:[authorizeRequest URL]];

}


- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    [indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil] show];
}

- (void)urlRecived:(NSNotification*)notification
{
    NSURL *url = (NSURL*)notification.object;
    NSString* verifier = nil;
    NSArray* urlParams = [[url resourceSpecifier] componentsSeparatedByString:@"="];
    verifier = [urlParams objectAtIndex:1];
    
  
    if (verifier) {
        NSURL* accessTokenUrl = [NSURL URLWithString:@"https://graph.facebook.com/oauth/access_token"];
        OAMutableURLRequest* accessTokenRequest = [[OAMutableURLRequest alloc] initWithURL:accessTokenUrl
                                                                                  consumer:nil
                                                                                     token:nil
                                                                                     realm:nil
                                                                         signatureProvider:nil];
        
        OARequestParameter* code          = [[OARequestParameter alloc] initWithName:@"code" value:verifier];
        OARequestParameter* clientid     = [[OARequestParameter alloc] initWithName:@"client_id" value:client_id];
        OARequestParameter* client_secret = [[OARequestParameter alloc] initWithName:@"client_secret" value:secret];
        OARequestParameter* redirect_uri  = [[OARequestParameter alloc] initWithName:@"redirect_uri" value:callback];
        
        [accessTokenRequest setHTTPMethod:@"POST"];
        [accessTokenRequest setParameters:[NSArray arrayWithObjects:code,
                                           clientid,
                                           client_secret,
                                           redirect_uri,nil]];
        
        OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
        [dataFetcher fetchDataWithRequest:accessTokenRequest
                                 delegate:self
                        didFinishSelector:@selector(didReceiveAccessToken:data:)
                          didFailSelector:@selector(didFailOAuth:error:)];
    }
    
}

- (void)didReceiveAccessToken:(OAServiceTicket*)ticket data:(NSData*)data {
    NSString* access_token;
    NSString* url = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSArray* urlParams = [url componentsSeparatedByString:@"&"];
    for (NSString* obj in urlParams) {
        NSArray*tmp = [obj componentsSeparatedByString:@"="];
        if ([[tmp objectAtIndex:0] isEqualToString:@"access_token"]) {
            access_token =[tmp objectAtIndex:1];
        }
    }
    
    if (access_token) {
        
        NSURL *url = [[NSURL alloc] initWithString:@"https://graph.facebook.com/debug_token"];
        OAMutableURLRequest* userDataRequest = [[OAMutableURLRequest alloc] initWithURL:url
                                                                               consumer:consumer token:accessToken realm:nil signatureProvider:nil
                                                                                  nonce:nil
                                                                              timestamp:nil];
        OARequestParameter* _input_token  = [[OARequestParameter alloc] initWithName:@"input_token" value:access_token];
        OARequestParameter* _access_token  = [[OARequestParameter alloc] initWithName:@"access_token" value:input_token];
        
        [userDataRequest setHTTPMethod:@"GET"];
        [userDataRequest setParameters:[NSArray arrayWithObjects:_input_token,_access_token,nil]];
        OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
        [dataFetcher fetchDataWithRequest:userDataRequest
                                 delegate:self
                        didFinishSelector:@selector(didReceiveUserData:data:)
                          didFailSelector:@selector(didFailOAuth:error:)];
        
        

    }
}

- (void)didFailOAuth:(OAServiceTicket*)ticket error:(NSError*)error {
    [_delegate didReceiveError:error];
}

- (void)didReceiveUserData:(OAServiceTicket*)ticket data:(NSData*)data {
    JSONDecoder *decoder = [JSONDecoder decoder];
    [_delegate didReceiveFacebookUser:[decoder objectWithData:data]];
}

@end
