//
//  TwitterLogin.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/8/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "GooglePlusLogin.h"
#import "OAuthCore.h"
#import "OAuth+Additions.h"
#import "NSData+Base64.h"
#import <CommonCrypto/CommonHMAC.h>
#import "JSONKit.h"

@implementation GooglePlusLogin
{
    OAConsumer* consumer;
    OAToken* requestToken;
    OAToken* accessToken;
    NSString *isLogin;
    UIActivityIndicatorView *indicator;
    UIButton *close ;
    
    NSString *client_id ;
    NSString *secret ;
    NSString *callback;
    NSString *scope;
    NSString *visibleactions;


}

-(id)init
{
    self = [super init];
    if (self) {
        client_id = @"65048645003-gjrm6hopnvbtr1pfjf9d7sl4qh2m8li2.apps.googleusercontent.com";
        secret = @"rfkwj0GLVGIlC0oBfeWbdDpA";
        callback = @"com.tawasol.FC-Barcelona:/oauth2Callback";
        scope = @"openid";
        visibleactions = @"http://schemas.google.com/AddActivity";
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(urlRecived:) name:@"urlRecived" object:nil];

    }
    return self;
}
-(void)openLogger{
    
    [self openView];
}

- (void)openView {
    NSURL* authorizeUrl = [NSURL URLWithString:@"https://accounts.google.com/o/oauth2/auth"];
    OAMutableURLRequest* authorizeRequest = [[OAMutableURLRequest alloc] initWithURL:authorizeUrl
                                                                            consumer:nil
                                                                               token:nil
                                                                               realm:nil
                                                                   signatureProvider:nil];
    
    
    OARequestParameter* clientid = [[OARequestParameter alloc] initWithName:@"client_id" value:client_id];
    OARequestParameter* response_type = [[OARequestParameter alloc] initWithName:@"response_type" value:@"code"];
    OARequestParameter* _scope = [[OARequestParameter alloc] initWithName:@"scope" value:@"openid"];
    OARequestParameter* redirect_uri = [[OARequestParameter alloc] initWithName:@"redirect_uri" value:callback];
    OARequestParameter* requestvisibleactions = [[OARequestParameter alloc] initWithName:@"data-requestvisibleactions" value:visibleactions];
    
    [authorizeRequest setHTTPMethod:@"GET"];
    [authorizeRequest setParameters:[NSArray arrayWithObjects:
                                     clientid,
                                     response_type,
                                     _scope,
                                     redirect_uri,
                                     requestvisibleactions,nil]];
    
     [[UIApplication sharedApplication] openURL:[authorizeRequest URL]];

}


- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    [indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil] show];
}

- (void)urlRecived:(NSNotification*)notification
{
    NSURL *url = (NSURL*)notification.object;
    NSString* verifier = nil;
    NSArray* urlParams = [[url query] componentsSeparatedByString:@"&"];
    for (NSString* param in urlParams) {
        NSArray* keyValue = [param componentsSeparatedByString:@"="];
        NSString* key = [keyValue objectAtIndex:0];
        if ([key isEqualToString:@"code"]) {
            verifier = [keyValue objectAtIndex:1];
            NSLog(@"verifier %@",verifier);
            break;
        }
    }
    
    if (verifier) {
        NSURL* accessTokenUrl = [NSURL URLWithString:@"https://www.googleapis.com/oauth2/v3/token"];
        OAMutableURLRequest* accessTokenRequest = [[OAMutableURLRequest alloc] initWithURL:accessTokenUrl
                                                                                  consumer:nil
                                                                                     token:nil
                                                                                     realm:nil
                                                                         signatureProvider:nil];
        
        
        OARequestParameter* code          = [[OARequestParameter alloc] initWithName:@"code" value:verifier];
        OARequestParameter* clientid     = [[OARequestParameter alloc] initWithName:@"client_id" value:client_id];
        OARequestParameter* client_secret = [[OARequestParameter alloc] initWithName:@"client_secret" value:secret];
        OARequestParameter* redirect_uri  = [[OARequestParameter alloc] initWithName:@"redirect_uri" value:callback];
        OARequestParameter* grant_type  = [[OARequestParameter alloc] initWithName:@"grant_type" value:@"authorization_code"];
        
        [accessTokenRequest setHTTPMethod:@"POST"];
        [accessTokenRequest setParameters:[NSArray arrayWithObjects:code,
                                           clientid,
                                           client_secret,
                                           redirect_uri,
                                           grant_type,nil]];
        
        OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
        [dataFetcher fetchDataWithRequest:accessTokenRequest
                                 delegate:self
                        didFinishSelector:@selector(didReceiveAccessToken:data:)
                          didFailSelector:@selector(didFailOAuth:error:)];
    }
    
}

- (void)didReceiveAccessToken:(OAServiceTicket*)ticket data:(NSData*)data {
    JSONDecoder *decoder = [JSONDecoder decoder];
    NSString* token = [decoder objectWithData:data][@"id_token"]?[decoder objectWithData:data][@"id_token"]:@"";
    NSURL *url = [[NSURL alloc] initWithString:@"https://www.googleapis.com/oauth2/v1/tokeninfo"];
    OAMutableURLRequest* userDataRequest = [[OAMutableURLRequest alloc] initWithURL:url
                                                                           consumer:consumer token:accessToken realm:nil signatureProvider:nil
                                                                              nonce:nil
                                                                          timestamp:nil];
    OARequestParameter* id_token  = [[OARequestParameter alloc] initWithName:@"id_token" value:token];
    [userDataRequest setHTTPMethod:@"GET"];
    [userDataRequest setParameters:[NSArray arrayWithObject:id_token]];
    OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
    [dataFetcher fetchDataWithRequest:userDataRequest
                             delegate:self
                    didFinishSelector:@selector(didReceiveUserData:data:)
                      didFailSelector:@selector(didFailOAuth:error:)];

    
}

- (void)didFailOAuth:(OAServiceTicket*)ticket error:(NSError*)error {
    [_delegate didReceiveError:error];

}

- (void)didReceiveUserData:(OAServiceTicket*)ticket data:(NSData*)data {
    JSONDecoder *decoder = [JSONDecoder decoder];
    [decoder objectWithData:data];
    NSLog(@"didReceiveUserData======>%@",[decoder objectWithData:data]);
    [_delegate didReceiveGoogleUser:[decoder objectWithData:data]];
}

@end
