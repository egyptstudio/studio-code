//
//  CNavigationController.m
//  FCB
//
//  Created by Eissa on 4/16/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "CNavigationController.h"
#import "MenuViewController.h"
#import "AnonymousMenuViewController.h"
#import <Accounts/Accounts.h>
#import "CurrentUser.h"
#import "MainWallViewController.h"
#import "StudioV2ViewController.h"
#import "StudioDetailsViewController.h"
#import "DatabaseEngine.h"
#import "ViewProfileViewController.h"
#import "SettingsViewController.h"
#import "ChangePasswordViewController.h"
#import "PrivacyViewController.h"
#import "ContactUsViewController.h"
#import "ContestViewController.h"
#import "InfoViewController.h"
#import "AboutFCBarcelonaViewController.h"
#import "AboutFCBStudioViewController.h"
#import "AboutTawasolViewController.h"
#import "PrivacyAgreementViewController.h"
#import "CachedImagesCleaner.h"
#import "ChatMainViewController.h"
#import "FansListView.h"
#import "FansDetailView.h"
#import "FansCollectionView.h"
#import "FriendsView.h"
#import "ShopViewController.h"
#import "HelpViewController.h"
#import "NotificationViewController.h"
#import "SopnsorsViewController.h"
#import "BlockedUsersViewController.h"
#import "FreePointsDetailsViewController.h"
@interface CNavigationController ()

@end

@implementation CNavigationController
{
    UIDynamicAnimator* animator;
    UIAttachmentBehavior *panAttachmentBehaviour;
    UIGravityBehavior *gravityBehaviour;
    UIPushBehavior *pushBehavior;
    UICollisionBehavior *collision;
    NSTimer * elapser;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openMenu) name:@"openMenu" object:nil];
    self.delegate = self ;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotate
{
    return self.topViewController.shouldAutorotate;
}

- (NSUInteger)supportedInterfaceOrientations
{
    UIInterfaceOrientation statusBarOrientation =[UIApplication sharedApplication].statusBarOrientation;
    
    if (self.topViewController.supportedInterfaceOrientations == UIInterfaceOrientationPortrait) {
        if (statusBarOrientation != UIInterfaceOrientationPortrait) {
            [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
        }
    }
    return  self.topViewController.supportedInterfaceOrientations;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    if ([viewController isKindOfClass:[MainWallViewController class]]
        || [viewController isKindOfClass:[StudioDetailsViewController class]]
        || [viewController isKindOfClass:[StudioV2ViewController class]]
        || [viewController isKindOfClass:[ViewProfileViewController class]]
        || [viewController isKindOfClass:[SettingsViewController class]]
        || [viewController isKindOfClass:[ChangePasswordViewController class]]
        || [viewController isKindOfClass:[PrivacyViewController class]]
        || [viewController isKindOfClass:[ContactUsViewController class]]
        || [viewController isKindOfClass:[InfoViewController class]]
        || [viewController isKindOfClass:[AboutFCBarcelonaViewController class]]
        || [viewController isKindOfClass:[AboutFCBStudioViewController class]]
        || [viewController isKindOfClass:[AboutTawasolViewController class]]
        || [viewController isKindOfClass:[PrivacyAgreementViewController class]]
        || [viewController isKindOfClass:[FansListView class]]
        || [viewController isKindOfClass:[FansDetailView class]]
        || [viewController isKindOfClass:[FansCollectionView class]]
        || [viewController isKindOfClass:[FriendsView class]]
        || [viewController isKindOfClass:[ChatMainViewController class]]
        || [viewController isKindOfClass:[ContestViewController class]]
        || [viewController isKindOfClass:[ShopViewController class]]
        || [viewController isKindOfClass:[HelpViewController class]]
        || [viewController isKindOfClass:[NotificationViewController class]]
        || [viewController isKindOfClass:[SopnsorsViewController class]]
        || [viewController isKindOfClass:[BlockedUsersViewController class]]
        || [viewController isKindOfClass:[FreePointsDetailsViewController class]])
    {
        float screenX = [UIScreen mainScreen].bounds.size.width;
        UIView * dragbbleErea = [[UIView alloc] initWithFrame:CGRectMake(screenX/3-40, 20, (screenX/3)+80, 67)];
        [dragbbleErea setBackgroundColor:[UIColor clearColor]];
        
        UIPanGestureRecognizer * dragGesture= [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragMenu:)];
        dragGesture.minimumNumberOfTouches = 1;
        dragGesture.maximumNumberOfTouches = 1;
        [dragbbleErea addGestureRecognizer:dragGesture];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openMenu)];
        [dragbbleErea addGestureRecognizer:gestureRecognizer];
        gestureRecognizer.cancelsTouchesInView = NO;
        gestureRecognizer.numberOfTapsRequired = 1 ;
        dragbbleErea.userInteractionEnabled = YES;
        
        [viewController.view addSubview:dragbbleErea];
        [viewController.view insertSubview:dragbbleErea atIndex:[viewController.view.subviews count]];
        animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.topViewController.view];
    }
}


- (void)dragMenu:(UIPanGestureRecognizer *)sender
{
    [self.topViewController.view endEditing:YES];
    
    MenuViewController *menu ;
    BOOL menuAddedBefore = NO;
    @try {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ViewFooter" object:nil];

        for (NSObject *obj in self.topViewController.childViewControllers) {
            if ([obj isKindOfClass:[MenuViewController class]]||[obj isKindOfClass:[AnonymousMenuViewController class]]) {
                menuAddedBefore = YES;
                menu =(MenuViewController*)obj;
                break;
            }
        }
        
        if (!menuAddedBefore) {
            NSString * identifier = [[CurrentUser getObject] isUser] ? @"MenuViewController" : @"AnonymousMenuViewController" ;
            menu = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
            [self.topViewController addChildViewController:menu];
            menu.view.frame =CGRectMake(0, 0-self.view.bounds.size.height+130, self.view.bounds.size.width, self.view.bounds.size.height) ;
            [self.topViewController.view insertSubview:menu.view atIndex:[self.topViewController.view.subviews count]];
            [menu didMoveToParentViewController:self];
        }
        CGPoint location = [sender locationInView:sender.view.superview];
        location.x = CGRectGetMidX(menu.view.bounds);
        
        if (sender.state == UIGestureRecognizerStateBegan) {
            [animator removeBehavior:gravityBehaviour];
            panAttachmentBehaviour = [[UIAttachmentBehavior alloc] initWithItem:menu.view attachedToAnchor:location];
            [animator addBehavior:panAttachmentBehaviour];
        }
        else if (sender.state == UIGestureRecognizerStateChanged) {
            if (location.y < 50) {
                [menu.view setHidden:YES];
            }
            else{
                [menu.view setHidden:NO];
            }
            panAttachmentBehaviour.anchorPoint = location;

            NSLog(@"Location:%@",NSStringFromCGPoint(location));
        }
        else if (sender.state == UIGestureRecognizerStateEnded) {
            pushBehavior = [[UIPushBehavior alloc] initWithItems:@[menu.view] mode:UIPushBehaviorModeInstantaneous];
            gravityBehaviour = [[UIGravityBehavior alloc] initWithItems:@[menu.view]];
            CGPoint velocity = [sender velocityInView:self.topViewController.view];
            if (velocity.y > 0) {
                gravityBehaviour.gravityDirection = CGVectorMake(0.0f, 1.0f);
                collision = [[UICollisionBehavior alloc] initWithItems:@[menu.view]];
                collision.translatesReferenceBoundsIntoBoundary = NO;
                [collision addBoundaryWithIdentifier:@"tabbar"
                                           fromPoint:CGPointMake(0,[UIScreen mainScreen].bounds.size.height)
                                             toPoint:CGPointMake([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
                [animator addBehavior:collision];
                [animator removeBehavior:panAttachmentBehaviour];
                [animator addBehavior:gravityBehaviour];
                pushBehavior.pushDirection =  CGVectorMake( 0,velocity.y / 10.0f);
                pushBehavior.active = YES;
                [animator addBehavior:pushBehavior];
                
                [animator setDelegate:self];
            }
            else {
                [animator removeAllBehaviors];
                [UIView animateWithDuration:0.5f animations:^{
                    menu.view.frame =CGRectMake(0, 0-menu.view.bounds.size.height, menu.view.bounds.size.width, menu.view.bounds.size.height);
                } completion:^(BOOL finished) {
                    [menu willMoveToParentViewController:nil];
                    [menu.view removeFromSuperview];
                    [menu removeFromParentViewController];
                }];
            }
            
        }
    
    }
    @catch (NSException *exception) {
        NSLog(@"NSException %@",exception.description);
        [self removeMenu:menu];
        
        float screenX = [UIScreen mainScreen].bounds.size.width;
        UIView * dragbbleErea = [[UIView alloc] initWithFrame:CGRectMake(screenX/3-40, 20, (screenX/3)+80, 67)];
        [dragbbleErea setBackgroundColor:[UIColor clearColor]];
        
        UIPanGestureRecognizer * dragGesture= [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragMenu:)];
        dragGesture.minimumNumberOfTouches = 1;
        dragGesture.maximumNumberOfTouches = 1;
        [dragbbleErea addGestureRecognizer:dragGesture];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openMenu)];
        [dragbbleErea addGestureRecognizer:gestureRecognizer];
        gestureRecognizer.cancelsTouchesInView = NO;
        gestureRecognizer.numberOfTapsRequired = 1 ;
        dragbbleErea.userInteractionEnabled = YES;
        
        [self.topViewController.view addSubview:dragbbleErea];
        [self.topViewController.view insertSubview:dragbbleErea atIndex:[self.topViewController.view.subviews count]];
        animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.topViewController.view];
        [self openMenu];
    }
}

-(void)removeMenu:(UIViewController*)menu{
    [menu willMoveToParentViewController:nil];
    [menu.view removeFromSuperview];
    [menu removeFromParentViewController];
}

- (void) openMenu
{

    @try {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ViewFooter" object:nil];
        [self.topViewController.view endEditing:YES];
        
        BOOL menuOpendBefore = NO;
        MenuViewController *menu;
        NSString * identifier = [[CurrentUser getObject] isUser] ? @"MenuViewController" : @"AnonymousMenuViewController" ;
        menu = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
        
        for (NSObject *obj in self.topViewController.childViewControllers) {
            if ([obj isKindOfClass:[MenuViewController class]]||[obj isKindOfClass:[AnonymousMenuViewController class]]) {
                menuOpendBefore = YES;
                return;
            }
        }
        if (!menuOpendBefore ) {
            
            [self.topViewController addChildViewController:menu];
            menu.view.frame =CGRectMake(0, 0-self.view.bounds.size.height+130, self.view.bounds.size.width, self.view.bounds.size.height) ;
            [self.topViewController.view insertSubview:menu.view atIndex:[self.topViewController.view.subviews count]-1];
            [menu didMoveToParentViewController:self];
            
            collision = [[UICollisionBehavior alloc] initWithItems:@[menu.view]];
            collision.translatesReferenceBoundsIntoBoundary = NO;
            [collision addBoundaryWithIdentifier:@"tabbar"
                                       fromPoint:CGPointMake(0,[UIScreen mainScreen].bounds.size.height)
                                         toPoint:CGPointMake([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
            UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[menu.view]];
            [animator addBehavior:collision];
            [animator addBehavior:gravity];
            [animator setDelegate:self];
            
        }
    }
    @catch (NSException *exception) {
    }
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animatorX{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"animationStopped" object:nil];
    [animator setDelegate:nil];
}

@end

@implementation UIAlertView(Orientation)

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return  (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
