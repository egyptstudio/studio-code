//
//  CitiesFilterViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 12/14/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CityFilterDelegate <NSObject>
@required
- (void)didSelectCountries:(NSMutableArray*)array;
@end
@interface CitiesFilterViewController : UIViewController
@property (nonatomic,assign) id <CityFilterDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)done_Action:(id)sender;
- (IBAction)back_Action:(id)sender;

@end
