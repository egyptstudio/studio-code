//
//  CNavigationController.h
//  MOHE
//
//  Created by Eissa on 4/16/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNavigationController : UINavigationController<UINavigationControllerDelegate,UIDynamicAnimatorDelegate>

@end
