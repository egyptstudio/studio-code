//
//  Season.m
//  FC Barcelona
//
//  Created by Eissa on 10/2/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "Season.h"

@implementation Season


-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:@(self.seasonId) forKey:@"seasonId"];
    [encoder encodeObject:self.seasonName forKey:@"seasonName"];
    [encoder encodeObject:self.posts forKey:@"posts"];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self.seasonId     =  [[decoder decodeObjectForKey:@"seasonId"] intValue];
    self.seasonName =  [decoder decodeObjectForKey:@"seasonName"];
    self.posts         =  [decoder decodeObjectForKey:@"posts"];
    return self;
}

@end
