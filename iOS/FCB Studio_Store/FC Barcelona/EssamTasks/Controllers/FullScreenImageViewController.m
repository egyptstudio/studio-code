//
//  FullScreenImageViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 12/7/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "FullScreenImageViewController.h"
#import "WebImage.h"

@interface FullScreenImageViewController ()

@end

@implementation FullScreenImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    self.fullScreenImage.image =[UIImage imageWithCGImage:[_image CGImage]
                                                    scale:1.0
                                              orientation: _orienaiton];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationChanged:)
                                                name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
    [self performSelector:@selector(checkOrientation) withObject:nil afterDelay:.7];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)checkOrientation{
    
    if([UIDevice currentDevice].orientation == UIDeviceOrientationPortrait )
    {
        [self.navigationController popViewControllerAnimated:NO];

        
    }
    
}

-(void) orientationChanged:(NSNotification *)note{
    
    UIDevice * device = note.object;
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait :
        {
            [self.navigationController popViewControllerAnimated:NO];
            break;
            
        }
            break;
        default:
            break;
    };
}

/////////////////////////////
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
