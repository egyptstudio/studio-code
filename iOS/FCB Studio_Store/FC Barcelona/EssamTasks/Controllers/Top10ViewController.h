//
//  Top10ViewController.h
//  FC Barcelona
//
//  Created by Eissa on 9/10/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostsViewController.h"
@protocol Top10Delegate <NSObject>
@required
- (void)didScrollToDirection:(ScrollDirection)direction;
@end

@interface Top10ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,assign) id <Top10Delegate> delegate;

@end
