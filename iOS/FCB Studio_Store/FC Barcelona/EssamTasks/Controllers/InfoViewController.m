//
//  InfoViewController.m
//  FC Barcelona
//
//  Created by Essam Eissa on 1/19/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "InfoViewController.h"
#import "RestServiceAgent.h"
#import "UsersManager.h"
#import "FreePointsDetailsViewController.h"
#import "SVProgressHUD.h"
@interface InfoViewController ()

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [self setLocalisables];
}

-(void)setLocalisables{
    _pageTitle.text = [[Language sharedInstance] stringWithKey:@"i_title"];
    [_helpButton setTitle:[[Language sharedInstance] stringWithKey:@"i_Help"] forState:UIControlStateNormal];
    [_privacyButton setTitle:[[Language sharedInstance] stringWithKey:@"infoElement_item3"] forState:UIControlStateNormal];
    [_aboutFCBStudioButton setTitle:[[Language sharedInstance] stringWithKey:@"infoElement_item2"] forState:UIControlStateNormal];
    [_aboutFCBarcelonaButton setTitle:[[Language sharedInstance] stringWithKey:@"i_AboutFC"] forState:UIControlStateNormal];
    [_aboutTawasolitButton setTitle:[[Language sharedInstance] stringWithKey:@"infoElement_item4"] forState:UIControlStateNormal];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    [_freePointsButton setTitle:[[Language sharedInstance] stringWithKey:@"RewardingPoints"] forState:UIControlStateNormal];
    [_faqButton setTitle:[[Language sharedInstance] stringWithKey:@"FAQ"] forState:UIControlStateNormal];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
}

- (IBAction)faqButton_Action:(id)sender{
    [self getInfoUrlForSection:@"faq"];
}

-(void)getInfoUrlForSection:(NSString*)section{
    @try {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if([RestServiceAgent internetAvailable])
        {
            [[UsersManager getInstance] getLinks:section and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        if([[currentClient objectForKey:@"status"] intValue] == 2)
                        {
                            FreePointsDetailsViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"FreePointsDetailsViewController"];
                            view.webUrl = currentClient[@"data"][0][@"link"];
                            view.webTitle =[section isEqualToString:@"faq"] ? [[Language sharedInstance] stringWithKey:@"FAQ"] : [[Language sharedInstance] stringWithKey:@"FreePoints"];
                            [self.navigationController pushViewController:view animated:YES];
                        }
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
}
- (IBAction)helpButton_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openHelp" object:nil];

}
- (IBAction)privacyButton_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openPrivacyAgreement" object:nil];

}
- (IBAction)aboutFCBStudioButton_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openAboutFCBStudio" object:nil];

}
- (IBAction)aboutFCBarcelonaButton_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openAboutFCBarcelona" object:nil];

}
- (IBAction)aboutTawasolitButton_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openAboutTawasolit" object:nil];

}
- (IBAction)freePointsButton_Action:(id)sender{
    [self getInfoUrlForSection:@"completepoints"];
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
