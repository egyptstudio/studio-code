//
//  Language.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//


#import "Language.h"
Language *LanguageObject = nil;

@interface Language ()
@end

@implementation Language
{
    supportedLanguages currentLanguage;
    NSString *langAbbreviation;
    NSDictionary *stringsDictionary;
}
@synthesize currentLanguage, langAbbreviation,stringsDictionary;


- (id)init{
	self = [super init];
	if (self) {
		if (![[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] objectForKey:@"language"]) {
            NSString * langPref = [[ NSLocale preferredLanguages ] objectAtIndex:0];
            if (![langPref isEqualToString:@"en"]&&
                ![langPref isEqualToString:@"es"]&&
                ![langPref isEqualToString:@"ca"]&&
                ![langPref isEqualToString:@"ar"]&&
                ![langPref isEqualToString:@"fr"]&&
                ![langPref isEqualToString:@"pt"]) {
                [self setLanguage:English];
            }
            else if([langPref isEqualToString:@"en"]){
                [self setLanguage:English];
            }else if ([langPref isEqualToString:@"es"]) {
                [self setLanguage:Spanish];
            }else if ([langPref isEqualToString:@"ca"]) {
                [self setLanguage:Catalan];
            }else if ([langPref isEqualToString:@"ar"]) {
                [self setLanguage:Arabic];
            }else if ([langPref isEqualToString:@"fr"]) {
                [self setLanguage:French];
            }else if ([langPref isEqualToString:@"pt"]) {
                [self setLanguage:Portuguese];
            }
		} else {
            [self setLanguage:[[[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] objectForKey:@"language"] intValue]];
		}
	}
	return self;
}

+ (Language *)sharedInstance{
	if (LanguageObject == nil) {
		LanguageObject = [[Language alloc] init];
	}
	return LanguageObject;
}

- (void)setLanguage:(supportedLanguages)lang{
    [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setObject:[NSNumber numberWithInt:lang] forKey:@"language"];
    [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] synchronize];
	self.currentLanguage = lang;
	switch (lang) {
		case English:
        {
			langAbbreviation = @"en";
			self.stringsDictionary = [NSDictionary dictionaryWithContentsOfFile:
                                      [[NSBundle mainBundle] pathForResource:@"English" ofType:@"plist"]];
			break;
        }
        case Spanish:
        {
            langAbbreviation = @"es";
            self.stringsDictionary = [NSDictionary dictionaryWithContentsOfFile:
                                      [[NSBundle mainBundle] pathForResource:@"Spanish" ofType:@"plist"]];
            break;
        }
        case Catalan:
        {
            langAbbreviation = @"ca";
            self.stringsDictionary = [NSDictionary dictionaryWithContentsOfFile:
                                      [[NSBundle mainBundle] pathForResource:@"Catalan" ofType:@"plist"]];
            break;
        }
        case Arabic:
        {
            langAbbreviation = @"ar";
            self.stringsDictionary = [NSDictionary dictionaryWithContentsOfFile:
                                      [[NSBundle mainBundle] pathForResource:@"Arabic" ofType:@"plist"]];
            break;
        }
        case French:
        {
            langAbbreviation = @"fr";
            self.stringsDictionary = [NSDictionary dictionaryWithContentsOfFile:
                                      [[NSBundle mainBundle] pathForResource:@"French" ofType:@"plist"]];
            break;
        }
        case Portuguese:
        {
            langAbbreviation = @"pt";
            self.stringsDictionary = [NSDictionary dictionaryWithContentsOfFile:
                                      [[NSBundle mainBundle] pathForResource:@"Portuguese" ofType:@"plist"]];
            break;
        }
	}
}

- (supportedLanguages)getLanguage{
	return self.currentLanguage;
}

- (NSString *)stringWithKey:(NSString *)stringKey{
    return [[[self.stringsDictionary objectForKey:stringKey] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"] stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
}

- (NSArray *)arrayWithKey:(NSString *)arrayKey
{
	return [self.stringsDictionary objectForKey:arrayKey];
}


@end
