//
//  Language.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//


#import <Foundation/Foundation.h>
typedef enum {
    English=0,
    Catalan,
    Spanish,
    Arabic,
    French,
    Portuguese,
} supportedLanguages;

@interface Language : NSObject

@property (nonatomic, readonly) NSString *langAbbreviation;
@property (nonatomic) supportedLanguages currentLanguage;
@property (nonatomic, retain) NSDictionary *imagesDictionary;
@property (nonatomic, retain) NSDictionary *stringsDictionary;

+ (Language *)sharedInstance;
- (void)setLanguage:(supportedLanguages)lang;
- (supportedLanguages)getLanguage;
- (NSString *)stringWithKey:(NSString *)stringKey;
- (NSArray *)arrayWithKey:(NSString *)arrayKey;
@end
