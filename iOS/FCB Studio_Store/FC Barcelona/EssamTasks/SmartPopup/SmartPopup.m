//
//  SmartPopup.m
//  FC Barcelona
//
//  Created by Essam Eissa on 7/30/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "SmartPopup.h"

@interface SmartPopup ()

@end

@implementation SmartPopup
#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (id)initWithBody:(NSString *)body withShowAgainChanged:(void (^)(BOOL flag))showAgainBlock withDismissChanged:(void (^)(BOOL flag))dismissBlock{
    self = [super initWithNibName:@"SmartPopup" bundle:nil];
    self.view.frame = [[UIScreen mainScreen] bounds];
    if (self) {
        self.bodyLabel.text =  body;
        
        [self.laterButton setTitle:[[Language sharedInstance] stringWithKey:@"Later"] forState:UIControlStateNormal];
        [self.laterButton addTarget:self action:@selector(laterButton_Action) forControlEvents:UIControlEventTouchUpInside];

        [self.agreeButton setTitle:[[Language sharedInstance] stringWithKey:@"Agree"] forState:UIControlStateNormal];
        [self.agreeButton addTarget:self action:@selector(agreeButton_Action) forControlEvents:UIControlEventTouchUpInside];

        [self.showAgainButton setTitle:[[Language sharedInstance] stringWithKey:@"DonAskAgain"] forState:UIControlStateNormal];
        [self.showAgainButton addTarget:self action:@selector(showAgain) forControlEvents:UIControlEventTouchUpInside];

        self.dismissBlock = dismissBlock;
        self.showAgainBlock = showAgainBlock;
        
        _mainWindow = [[[UIApplication sharedApplication] delegate] window];
        [self.view setTag:1000];
    }
    return self;
    
}

- (void)showPopUpInController:(UIViewController *)controller{
    [controller addChildViewController:self];
    [self didMoveToParentViewController:controller];
    [_popupView setTransform:CGAffineTransformMakeScale(0, 0)];
    [controller.view addSubview:self.view];
    [UIView animateWithDuration:0.4 animations:^{
        [_popupView setTransform:CGAffineTransformIdentity];
    }completion:nil];
}

- (void)dismissPopUp{
    [[_mainWindow viewWithTag:1000] removeFromSuperview];
}

- (void)showAgain{
    if (self.dismissBlock != nil) {
        self.dismissBlock(YES);
    }
}

- (void)agreeButton_Action{
    [self dismissPopUp];
    if (self.dismissBlock != nil) {
        self.dismissBlock(YES);
    }
}

- (void)laterButton_Action{
    if (self.dismissBlock != nil) {
        self.dismissBlock(NO);
    }
    [self dismissPopUp];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
@end
