//
//  SmartPopup.h
//  FC Barcelona
//
//  Created by Essam Eissa on 7/30/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DismissBlock)(BOOL flag);
typedef void(^ShowAgainBlock)(BOOL flag);

@interface SmartPopup : UIViewController

@property (weak, nonatomic) UIWindow *mainWindow;

@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;

@property (strong, nonatomic) IBOutlet UIButton *showAgainButton;
@property (strong, nonatomic) IBOutlet UIButton *laterButton;
@property (strong, nonatomic) IBOutlet UIButton *agreeButton;

@property (weak, nonatomic) IBOutlet UIView *popupView;

@property (nonatomic, copy) DismissBlock dismissBlock;
@property (nonatomic, copy) ShowAgainBlock showAgainBlock;

- (id)initWithBody:(NSString *)body withShowAgainChanged:(void (^)(BOOL flag))showAgainBlock withDismissChanged:(void (^)(BOOL flag))dismissBlock;

- (void)showPopUpInController:(UIViewController *)controller;
- (void)dismissPopUp;

@end
