//
//  Post.m
//  FC Barcelona
//
//  Created by Eissa on 9/10/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "Post.h"

@implementation Post

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:@(self.originalPostId) forKey:@"originalPostId"];
    [encoder encodeObject:@(self.studioPhotoId) forKey:@"studioPhotoId"];
    [encoder encodeObject:@(self.postId) forKey:@"postId"];
    [encoder encodeObject:@(self.commentCount) forKey:@"commentCount"];
    [encoder encodeObject:@(self.likeCount) forKey:@"likeCount"];
    [encoder encodeObject:@(self.repostCount) forKey:@"repostCount"];
    [encoder encodeObject:@(self.shareCount) forKey:@"shareCount"];
    [encoder encodeObject:@(self.contestRank) forKey:@"contestRank"];
    [encoder encodeObject:@(self.userId) forKey:@"userId"];
    [encoder encodeObject:@(self.rePosterId) forKey:@"rePosterId"];
    [encoder encodeObject:@(self.photoOrientation) forKey:@"photoOrientation"];
    [encoder encodeObject:@(self.creationTime) forKey:@"creationTime"];
//    [encoder encodeObject:@(self.lastUpdateTime) forKey:@"lastUpdateTime"];
    
    [encoder encodeObject:self.postPhotoUrl forKey:@"postPhotoUrl"];
    [encoder encodeObject:self.postPhotoCaption forKey:@"postPhotoCaption"];
    [encoder encodeObject:self.userName forKey:@"userName"];
    [encoder encodeObject:self.userCountry forKey:@"userCountry"];
    [encoder encodeObject:self.userProfilePicUrl forKey:@"userProfilePicUrl"];
    [encoder encodeObject:self.rePostedBy forKey:@"rePostedBy"];
    [encoder encodeObject:self.rePosterProfilePic forKey:@"rePosterProfilePic"];

    
    [encoder encodeObject:@(self.reported) forKey:@"reported"];
    [encoder encodeObject:@(self.lastUpdateTime) forKey:@"lastUpdateTime"];
    [encoder encodeObject:@(self.following) forKey:@"following"];
    [encoder encodeObject:@(self.liked) forKey:@"liked"];
    [encoder encodeObject:@(self.rePosted) forKey:@"rePosted"];
    [encoder encodeObject:@(self.rePosterIsFollowing) forKey:@"rePosterIsFollowing"];
    
    [encoder encodeObject:self.tags forKey:@"tags"];
    [encoder encodeObject:self.postComments forKey:@"postComments"];
    [encoder encodeObject:self.postLikes forKey:@"postLikes"];

    

}

-(id)initWithCoder:(NSCoder *)decoder
{
    self.originalPostId     =[[decoder decodeObjectForKey:@"originalPostId"] intValue];
    self.studioPhotoId      =[[decoder decodeObjectForKey:@"studioPhotoId"] intValue];
    self.postId             =[[decoder decodeObjectForKey:@"postId"] intValue];
    self.commentCount       =[[decoder decodeObjectForKey:@"commentCount"] intValue];
    self.likeCount          =[[decoder decodeObjectForKey:@"likeCount"] intValue];
    self.repostCount          =[[decoder decodeObjectForKey:@"repostCount"] intValue];
    self.shareCount         =[[decoder decodeObjectForKey:@"shareCount"] intValue];
    self.contestRank        =[[decoder decodeObjectForKey:@"contestRank"] intValue];
    self.userId             =[[decoder decodeObjectForKey:@"userId"] intValue];
    self.rePosterId             =[[decoder decodeObjectForKey:@"rePosterId"] intValue];
    self.photoOrientation   =[[decoder decodeObjectForKey:@"photoOrientation"] intValue];
    self.privacy             =[[decoder decodeObjectForKey:@"privacy"] intValue];
    self.creationTime       =[[decoder decodeObjectForKey:@"creationTime"] longValue];
    self.lastUpdateTime     =[[decoder decodeObjectForKey:@"lastUpdateTime"] longValue];
    self.postPhotoUrl       =[decoder decodeObjectForKey:@"postPhotoUrl"];
    self.postPhotoCaption   =[decoder decodeObjectForKey:@"postPhotoCaption"];
    self.userName           =[decoder decodeObjectForKey:@"userName"];
    self.userCountry        =[decoder decodeObjectForKey:@"userCountry"];
    self.userProfilePicUrl  =[decoder decodeObjectForKey:@"userProfilePicUrl"];
    self.rePostedBy         =[decoder decodeObjectForKey:@"rePostedBy"];
    self.rePosterProfilePic =[decoder decodeObjectForKey:@"rePosterProfilePic"];
    self.reported           =[[decoder decodeObjectForKey:@"reported"] boolValue];
    self.following          =[[decoder decodeObjectForKey:@"following"] boolValue];
    self.liked              =[[decoder decodeObjectForKey:@"liked"] boolValue];
    self.rePosted           =[[decoder decodeObjectForKey:@"rePosted"] boolValue];
    self.rePosterIsFollowing =[[decoder decodeObjectForKey:@"rePosterIsFollowing"] boolValue];
    self.tags                =[decoder decodeObjectForKey:@"tags"];
    self.postComments        =[decoder decodeObjectForKey:@"postComments"] ;
    self.postLikes           =[decoder decodeObjectForKey:@"postLikes"] ;
    return self;
}

@end

