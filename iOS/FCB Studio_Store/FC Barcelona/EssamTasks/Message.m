//
//  Message.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//


#import "Message.h"
#import "Contact.h"
#import "BarcelonaAppDelegate.h"
#import "Fan.h"
#import "NSDate+TimeZone.h"


@implementation Message

@dynamic date;
@dynamic message;
@dynamic messageId;
@dynamic reciever;
@dynamic sender;


+ (NSDate *)dateFromString:(NSString *)dateString
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate * date = [formatter dateFromString:dateString];
    
    return date;
}

+ (NSDate *)timeFromString:(NSString *)timeString
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSDate * time = [formatter dateFromString:timeString];
    
    return time;
}

+ (NSDate *)dateFromDate:(NSDate *)datePart time:(NSDate *)timePart
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    // Extract date components into components1
    NSDateComponents *components1 = [gregorianCalendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                                         fromDate:datePart];
    
    // Extract time components into components2
    NSDateComponents *components2 = [gregorianCalendar components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit
                                                         fromDate:timePart];
    
    // Combine date and time into components3
    NSDateComponents *components3 = [[NSDateComponents alloc] init];
    
    [components3 setYear:components1.year];
    [components3 setMonth:components1.month];
    [components3 setDay:components1.day];
    
    [components3 setHour:components2.hour];
    [components3 setMinute:components2.minute];
    [components3 setSecond:components2.second];
    
    // Generate a new NSDate from components3.
    return [gregorianCalendar dateFromComponents:components3];
}

+ (Message *)messageWithId:(int)messageId message:(NSString *)messageText from:(Fan *)sender to:(User *)destination atTime:(NSDate*)date seen:(BOOL)seen {
    
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
    
    
    //////////////////////////////////////////////////////////
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId == %i", messageId];
    [fetch setPredicate:predicate];
    
    NSArray* messagesExist = [context executeFetchRequest:fetch error:nil] ;
    Message *message;
    
    if([messagesExist count] == 0) {
        
        message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
        message.messageId = @(messageId);
        message.message = messageText;
        message.sender = [Contact contactFromFan:sender];
        message.reciever = [Contact contactFromUser:destination];
        
        if (!seen) {
            [message.sender newMessageRecieved];
        }
        
        message.date = [date localTime];
        
        NSError *error = nil;
        
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    else{
        message = (Message*)[messagesExist objectAtIndex:0];
    }

    return message;
    
}

+ (Message *)messageFromObject:(NSDictionary *)object {
    
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
    
    Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
    
    message.messageId = (object[@"id"] != (id)[NSNull null])? @([object[@"id"] intValue]):@0;
    message.message = (object[@"message"] != (id)[NSNull null])? object[@"message"]:@"";
    
    Fan *sender = [Fan alloc];
    sender.userId = (object[@"user_id"] !=(id)[NSNull null])? [object[@"user_id"] intValue]:0;
    sender.fanName = (object[@"sender_name"] !=(id)[NSNull null])? object[@"sender_name"]:@"";
    
    Fan *reciever = [Fan alloc];
    reciever.userId = (object[@"destination_id"] !=(id)[NSNull null])? [object[@"destination_id"] intValue]:0;
    reciever.fanName = (object[@"destination_name"] !=(id)[NSNull null])? object[@"destination_name"]:@"";
    
    message.sender = [Contact contactFromFan:sender];
    message.reciever = [Contact contactFromFan:reciever];
    
    
    NSString *dateS = (object[@"date"] !=(id)[NSNull null])? object[@"date"]:@"";
    NSString *timeS = (object[@"time"] !=(id)[NSNull null])? object[@"time"]:@"";
    
    NSDate *date = [self dateFromString:dateS];
    NSDate *time = [self timeFromString:timeS];
    
    message.date = [self dateFromDate:date time:time];
    
    NSError *error = nil;
    
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return message;
    
}

- (JSMessage *)JSMessage {
    return [[JSMessage alloc] initWithText:self.message sender:self.sender.userName date:self.date messageId:self.messageId];
}

@end
