//
//  CachedImagesCleaner.h
//  FCB Studio
//
//  Created by Essam Eissa on 6/1/15.
//  Copyright (c) 2015 Essam Eissa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CachedImagesCleaner : NSObject
@property(nonatomic,strong) NSTimer *timer;
+ (CachedImagesCleaner *)sharedInstance;
-(void)clearImagesAfterTimeInterval:(NSTimeInterval)interval;
@end
