//
//  Post.h
//  FC Barcelona
//
//  Created by Eissa on 9/10/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostLike : NSObject
@property(nonatomic,assign) int  likeId;
@property(nonatomic,assign) int  userId;
@property(strong,nonatomic) NSString * fullName;
@property(strong,nonatomic) NSString * profilePicURL;
@property(nonatomic) BOOL  isFollowing;

@end
