//
//  DatabaseEngine.h
//  FCB Studio
//
//  Created by Essam Eissa on 6/1/15.
//  Copyright (c) 2015 Essam Eissa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseEngine : NSObject
{
    sqlite3  * dataBase;
    const char *dbpath;
}

+ (DatabaseEngine *)getObject;
-(void)insertObjects:(NSArray*)objectsArray where:(NSString*)whereStmt;
-(NSMutableArray*)getObjectsFor:(id)object where:(NSString*)whereStmt;
-(void)doNothing;

@end
