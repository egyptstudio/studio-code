//
//  DatabaseEngine.m
//  FCB Studio
//
//  Created by Essam Eissa on 6/1/15.
//  Copyright (c) 2015 Essam Eissa. All rights reserved.
//

#import "DatabaseEngine.h"
#import <objC/runtime.h>

DatabaseEngine *DatabaseObject = nil;

#pragma mark -
#pragma mark Initialization

@implementation DatabaseEngine


+ (DatabaseEngine *)getObject{
	if (DatabaseObject == nil) {
		DatabaseObject = [[DatabaseEngine alloc] init];
	}
	
	return DatabaseObject;
}


-(id)init
{
    self = [super init];
    if (self != nil) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"fcb.db"];
        
        
        NSFileManager *filemgr = [NSFileManager defaultManager];
        if ([filemgr fileExistsAtPath: databasePath ] == NO)
        {
            dbpath = [databasePath UTF8String];
            NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"fcb" ofType:@"db"];
            [fileManager copyItemAtPath:resourcePath toPath:databasePath error:&error];
            sqlite3_open(dbpath, &dataBase);
        }
        else
        {
            dbpath = [databasePath UTF8String];
            sqlite3_open(dbpath, &dataBase);
        }
    }
    return self;
   
}

-(void)doNothing{
}

-(void)insertObjects:(NSArray*)objectsArray where:(NSString*)whereStmt{
    
    NSString *tableName = NSStringFromClass ([objectsArray[0] class]);
    NSArray *tableColumns = [[NSArray alloc] initWithArray:[self tableColumns:tableName]];
    NSDictionary *entity = [[NSDictionary alloc] init];
    
    for (int x = 0; x < [objectsArray count]; x++) {
        entity = [self dictionaryForObject:objectsArray[x]];
        NSString *countSQL = [NSString stringWithFormat:
                              @"SELECT COUNT(*) FROM %@ WHERE %@ = '%@'"
                              ,tableName
                              ,tableColumns[0]
                              ,entity[tableColumns[0]]];
        
        const char* sqlStatement = [countSQL UTF8String];
        sqlite3_stmt *statement;
        
        if( sqlite3_prepare_v2(dataBase, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            if( sqlite3_step(statement) == SQLITE_ROW )
            {
                if( sqlite3_column_int(statement, 0) == 0)
                {
                    NSString *insertSQL =
                    [NSString stringWithFormat:
                              @"INSERT INTO %@ %@"
                               ,tableName,[self tableColumnsValuesStringFromArray:tableColumns andDictionary:entity ]];
                    const char *insert_stmt = [insertSQL UTF8String];
                    sqlite3_exec(dataBase, insert_stmt,
                                 NULL, NULL, NULL);
                }
                
            }
        }
        sqlite3_finalize(statement);
    }
}
-(NSMutableArray*)getObjectsFor:(id)object where:(NSString*)whereStmt{
    NSString *tableName = NSStringFromClass ([object class]);
    NSMutableArray * _results = [[NSMutableArray alloc] init] ;
    sqlite3_stmt *sqlStatement;
    NSString *querySQL = [NSString stringWithFormat:
                          @"SELECT * from %@",tableName];
    const char *query_stmt = [querySQL UTF8String];
    
    if (sqlite3_prepare_v2(dataBase,query_stmt, -1, &sqlStatement, NULL) == SQLITE_OK)
    {
        while (sqlite3_step(sqlStatement) == SQLITE_ROW)
        {
            int count = sqlite3_data_count(sqlStatement);
            NSMutableDictionary * row = [NSMutableDictionary dictionaryWithCapacity:count];
            for ( int i=0; i<count; i++ )
            {
                NSString * columnName = [NSString stringWithUTF8String:(char *)sqlite3_column_name(sqlStatement, i)];
                [row setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStatement, i)] forKey:columnName];
            }
            [_results addObject:row];
        }
        sqlite3_finalize(sqlStatement);
        
    }
    return _results;

}

-(NSMutableArray*)tableColumns:(NSString *)tableName
{
    sqlite3_stmt *sqlStatement;
    NSMutableArray *result = [[NSMutableArray alloc] init];
    const char *sql = [[NSString stringWithFormat:@"pragma table_info('%s')",[tableName UTF8String]] UTF8String];
    if(sqlite3_prepare(dataBase, sql, -1, &sqlStatement, NULL) != SQLITE_OK)
    {
        NSLog(@"Problem with prepare statement tableInfo %@",[NSString stringWithUTF8String:(const char *)sqlite3_errmsg(dataBase)]);
        
    }
    while (sqlite3_step(sqlStatement)==SQLITE_ROW)
    {
        [result addObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(sqlStatement, 1)]];
        
    }
    return result;
}



-(NSMutableDictionary*)dictionaryForObject:(id)object
{
    // somewhere to store the results
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    // we'll grab properties for this class and every superclass
    // other than NSObject
    Class classOfObject = [object class];
    while(![classOfObject isEqual:[NSObject class]])
    {
        // ask the runtime to give us a C array of the properties defined
        // for this class (which doesn't include those for the superclass)
        unsigned int numberOfProperties;
        objc_property_t  *properties =
        class_copyPropertyList(classOfObject, &numberOfProperties);
        
        // go through each property in turn...
        for(
            int propertyNumber = 0;
            propertyNumber < numberOfProperties;
            propertyNumber++)
        {
            // get the name and convert it to an NSString
            NSString *nameOfProperty = [NSString stringWithUTF8String:
                                        property_getName(properties[propertyNumber])];
            
            // use key-value coding to get the property value
            id propertyValue = [object valueForKey:nameOfProperty];
            
            // add the value to the dictionary —
            // we'll want to transmit NULLs, even though an NSDictionary
            // can't store nils
            [result
             setObject:propertyValue ? propertyValue : [NSNull null]
             forKey:nameOfProperty];
        }
        
        // we took a copy of the property list, so...
        free(properties);
        
        // we'll want to consider the superclass too
        classOfObject = [classOfObject superclass];
    }
    
    // return the dictionary
    return result;
}

-(NSString*)tableColumnsValuesStringFromArray:(NSArray*)columnsArray andDictionary:(NSDictionary*)values
{
    NSData *columnsData = [NSJSONSerialization dataWithJSONObject:columnsArray options:NSJSONWritingPrettyPrinted error:nil];
    NSString * columnsString;
    columnsString = [[NSString alloc] initWithData:columnsData encoding:NSUTF8StringEncoding];
    columnsString = [columnsString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    columnsString = [columnsString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    columnsString = [columnsString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
     columnsString = [columnsString stringByReplacingOccurrencesOfString:@"[" withString:@"("];
     columnsString = [columnsString stringByReplacingOccurrencesOfString:@"]" withString:@")"];
    
    NSMutableArray *valuesArray = [[NSMutableArray alloc] init];
    for (NSString* columnName in columnsArray) {
        [valuesArray addObject:values[columnName]];
    }
    
    NSData *valuesData = [NSJSONSerialization dataWithJSONObject:valuesArray options:NSJSONWritingPrettyPrinted error:nil];
    NSString * valuesString;
    valuesString = [[NSString alloc] initWithData:valuesData encoding:NSUTF8StringEncoding];
    valuesString = [valuesString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    valuesString = [valuesString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    valuesString = [valuesString stringByReplacingOccurrencesOfString:@"[" withString:@"("];
    valuesString = [valuesString stringByReplacingOccurrencesOfString:@"]" withString:@")"];
    
    return [NSString stringWithFormat:@"%@ Values %@",columnsString,valuesString];
}

- (void)close
{
    if (dataBase)
    {
        NSLog(@"closing");
        int rc = sqlite3_close(dataBase);
        NSLog(@"close rc=%d", rc);
        
        if (rc == SQLITE_BUSY)
        {
            NSLog(@"SQLITE_BUSY: not all statements cleanly finalized");
            
            sqlite3_stmt *stmt;
            while ((stmt = sqlite3_next_stmt(dataBase, 0x00)) != 0)
            {
                NSLog(@"finalizing stmt");
                sqlite3_finalize(stmt);
            }
            rc = sqlite3_close(dataBase);
        }
        if (rc != SQLITE_OK)
        {
            NSLog(@"close not OK.  rc=%d", rc);
        }
        dataBase = NULL;
    }
}
-(void)clear{
    sqlite3_stmt *stmt;
    while ((stmt = sqlite3_next_stmt(dataBase, 0x00)) != 0)
    {
        NSLog(@"finalizing stmt");
        sqlite3_finalize(stmt);
    }

}
@end

