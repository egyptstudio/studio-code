//
//  Message.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "JSMessage.h"

@class Contact, User,Fan;

@interface Message : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSNumber * messageId;
@property (nonatomic, retain) Contact *reciever;
@property (nonatomic, retain) Contact *sender;

+ (Message *)messageWithId:(int)messageId message:(NSString *)messageText from:(Fan *)sender to:(User *)destination atTime:(NSDate*)date seen:(BOOL)seen;

+ (Message *)messageFromObject:(NSDictionary *)object;
- (JSMessage *)JSMessage;

@end
