//
//  Contact.m
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//


#import "Contact.h"
#import "Message.h"
#import "BarcelonaAppDelegate.h"
#import "User.h"
#import "Fan.h"

@implementation Contact

@dynamic firstTimeChat;
@dynamic imageURL;
@dynamic userId;
@dynamic userName;
@dynamic contacts;
@dynamic recievedMessages;
@dynamic sentMessages;
@dynamic user;


- (int)newMessageRecieved {
    
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageCountChanged" object:self];
    
    NSError *error = nil;
    
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return 1;
}

- (void)allMessagesRead {
    
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageCountChanged" object:self];
    
    NSError *error = nil;
    
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
}

+ (Contact *)chatWithUser:(Fan *)user {
    
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %d", [user userId]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSUInteger count = [context countForFetchRequest:request
                                               error:&error];
    
    Contact *contact;
    
    if (count == 0) {
        
        contact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
        contact.imageURL = [user fanPicture];
        contact.userId = @([user userId]);
        contact.userName = [user fanName];
        
    }
    else {
        
        NSArray *array = [context executeFetchRequest:request error:&error];
        contact = [array lastObject];
        
    }
    
    
    Contact *loginUser = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate userCD];
    
    [loginUser addContactsObject:contact];
    
    
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return contact;
    
}


+ (Contact *)contactFromFan:(Fan *)user {
    
    Contact *contact;
    
    if (user) {
        
        NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
        
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %d", [user userId]];
        [request setPredicate:predicate];
        
        NSError *error = nil;
        NSUInteger count = [context countForFetchRequest:request
                                                   error:&error];
        
        
        
        if (count == 0) {
            
            
            contact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
            
            contact.imageURL = [user fanPicture];
            contact.userId = @([user userId]);
            contact.userName = [user fanName];
            
            
            // Save the context.
            if (![context save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
            
        }
        else {
            
            NSArray *array = [context executeFetchRequest:request error:&error];
            contact = [array lastObject];
            
        }
        
    }
    
    return contact;
    
}
+ (Contact *)contactFromUser:(User *)user {
    
    Contact *contact;
    
    if (user) {
        
        NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
        
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %d", [user userId]];
        [request setPredicate:predicate];
        
        NSError *error = nil;
        NSUInteger count = [context countForFetchRequest:request
                                                   error:&error];
        
        
        
        if (count == 0) {
            
            
            contact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
            
            contact.imageURL = [user profilePic];
            contact.userId = @([user userId]);
            contact.userName = [user fullName];
            
            
            // Save the context.
            if (![context save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
            
        }
        else {
            
            NSArray *array = [context executeFetchRequest:request error:&error];
            contact = [array lastObject];
            
        }
        
    }
    
    return contact;
    
}


@end
