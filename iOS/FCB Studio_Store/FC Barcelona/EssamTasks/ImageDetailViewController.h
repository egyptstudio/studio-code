//
//  ImageDetailViewController.h
//  FC Barcelona
//
//  Created by Saad Ansari on 12/14/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"
#import "Post.h"
#import "WebImage.h"
#import "AvatarImageView.h"
#import "WallManager.h"
#import "CommentsViewController.h"

@interface ImageDetailViewController : UIViewController <DWTagListDelegate,UIDocumentInteractionControllerDelegate,commentDelegate>
{
    
}
@property (nonatomic, weak) IBOutlet UIView          *tagView;
@property (nonatomic, weak) IBOutlet UIView          *bottomView;
@property (nonatomic, weak) IBOutlet UIView          *editView;
@property (nonatomic, weak) IBOutlet UIButton        *reportButton;
@property (nonatomic, weak) IBOutlet UIButton        *btnRepost;
@property (nonatomic, weak) IBOutlet UILabel         *lblLikeCount;
@property (nonatomic, weak) IBOutlet UILabel         *lblCommentCount;
@property (nonatomic, weak) IBOutlet UITextView      *txtVwCaption;
@property (nonatomic, weak) IBOutlet UILabel         *lblTitleUser;
@property (nonatomic, weak) IBOutlet UILabel         *lblUserName;
@property (nonatomic, weak) IBOutlet UILabel         *lblRepost;
@property (nonatomic, weak) IBOutlet UILabel         *lblDate;
@property (nonatomic, weak) IBOutlet UILabel         *lblRate;

@property (nonatomic, weak) IBOutlet UIImageView         *imgPost;
@property (nonatomic, weak) IBOutlet AvatarImageView         *imgTitle;
@property (nonatomic, weak) IBOutlet AvatarImageView         *imgUser;
@property (nonatomic, weak) IBOutlet UIImageView         *imgFollow;
@property (nonatomic, weak) IBOutlet UIImageView         *imgRate;
@property (nonatomic, weak) IBOutlet UIImageView         *imgBg;

@property (nonatomic, weak) IBOutlet UIButton        *btnFacebook;
@property (nonatomic, weak) IBOutlet UIButton        *btnTwitter;
@property (nonatomic, weak) IBOutlet UIButton        *btnGoogle;
@property (nonatomic, weak) IBOutlet UIButton        *btnInsta;
@property (nonatomic, weak) IBOutlet UIButton        *btnLike;

@property (nonatomic, weak) IBOutlet UIButton        *btnUse;
@property (nonatomic, weak) IBOutlet UIButton        *btnDelete;
@property (nonatomic, weak) IBOutlet UIButton        *btnEdit;


@property(nonatomic, strong)     UIDocumentInteractionController* docController;
@property(nonatomic, assign)     BOOL isWall;



@property (nonatomic, weak) IBOutlet UIScrollView          *mainScrollView;
@property (nonatomic, strong) Post *currentPost;
-(IBAction)infoBtnAction:(id)sender;
-(IBAction)cancelBtnAction:(id)sender;
-(IBAction)repostBtnAction:(id)sender;
-(IBAction)shareBtnAction:(id)sender;
-(IBAction)useBtnAction:(id)sender;
-(IBAction)reportBtnAction:(id)sender;
-(IBAction)editBtnAction:(id)sender;
-(IBAction)deleteBtnAction:(id)sender;
@end
