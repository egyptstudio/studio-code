//
//  CurrentUser.m
//  FC Barcelona
//
//  Created by Eissa on 10/11/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "CurrentUser.h"
#import "StudioManager.h"
#import "RestServiceAgent.h"
#import "JSONKit.h"
#import "UsersManager.h"
#import "StudioManager.h"
#import "GetAndPushCashedData.h"
#import "MPNotificationView.h"
#import "DataHelper.h"

CurrentUser *currentUser = nil;
User* user;
NSUserDefaults* defaults;
@implementation CurrentUser
{
    int notificationId;
    NSMutableArray *mainNotificationArray;
    
    NSMutableArray *likeArray;
    NSMutableArray *likeGroupArray;
    NSMutableArray *commentArray;
    NSMutableArray *commentGroupArray;
    NSMutableArray *chatArray;
    NSMutableArray *followArray;
    NSMutableArray *followGroupArray;
    NSMutableArray *favArray;
    NSMutableArray *favGroupArray;
    NSMutableArray *requestArray;
    
    int pageNo;
}
- (id)init{
	self = [super init];
    pageNo=0;
    defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    user =  (User*)[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"user"]];
    [self updateNotifications];
	return self;
}

+ (CurrentUser *)getObject{
	if (currentUser == nil) {
		currentUser = [[CurrentUser alloc] init];
	}
	return currentUser;
}

-(void)refreshUser
{
    user =  (User*)[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"user"]];
}

-(User*)getUser
{
    return user;
}

-(BOOL)isUser
{
    return [defaults objectForKey:@"user"]?YES:NO;
}

-(void)saveCurrentUser
{
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"user"];
    [defaults synchronize];
}

-(void)setUser:(User*)newUser
{
    user = newUser;
}

-(void)removeUser{
    [defaults removeObjectForKey:@"user"];
}
-(void)updateUserPoints{
    if ([self isUser]) {
        @try {
            if([RestServiceAgent internetAvailable])
            {
                [[StudioManager getInstance] getUserCredit:user.userId and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (currentClient) {
                            if([currentClient[@"status"] intValue]==2)
                            {
                                user.credit = [currentClient[@"data"][@"credit"] intValue];
                                [self saveCurrentUser];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                }];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"getUserCredit Error");
        }
    }
}
-(void)updateNotifications
{
    notificationId = [[defaults objectForKey:@"notificationId"] intValue];
    @try {
        if ([RestServiceAgent internetAvailable]&&[self isUser]) {
            pageNo++;
            NSString *post = [NSString stringWithFormat:@"data={\"notificationId\" : \"%d\",      \"pageNo\": %i,      \"userId\": %i }",notificationId,1,user.userId];
            
            NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
            NSURL *url = [NSURL URLWithString:@"http://barca.tawasoldev.com/barca/index.php/api/notification/getNotificationsIOS"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90.0];
            [request setHTTPMethod:@"POST"];
            
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:postData];
            
            __block NSMutableData* responseData = [[NSMutableData alloc] init];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc]init];
            [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                if (!connectionError) {
                    [responseData appendData:data];
                    JSONDecoder *decoder = [JSONDecoder decoder];
                    NSDictionary* dict = [decoder objectWithData:responseData];
                    if([[dict objectForKey:@"status"] intValue] == 2)
                    {
                        //                        if (![[dict objectForKey:@"data"] isKindOfClass:[NSNull class]]) {

                            if ([[dict objectForKey:@"data"] count]>0) {

                                
                                mainNotificationArray = [NSMutableArray arrayWithArray:[dict objectForKey:@"data"]];
                                [defaults setObject:@([mainNotificationArray count]) forKey:@"NotificationsNumber"];
                                NSString * timestamp = [NSString stringWithFormat:@"%@",[[mainNotificationArray lastObject] valueForKey:@"notificationId"]];
                                [defaults setObject:timestamp forKey:@"notificationId"];
                                [defaults synchronize];
                                [self populateData];
                            }
//                        }
                       
                    }else
                    {
                        pageNo--;
                    }
                }
            }];
            
        }else{
            pageNo--;
        }
    }
    @catch (NSException *exception) {
            pageNo--;
    }
}

-(void)populateData
{
    
    
    

    
    [likeArray removeAllObjects];
    [likeGroupArray removeAllObjects];
    [commentGroupArray removeAllObjects];
    [commentArray removeAllObjects];
    [chatArray removeAllObjects];
    [requestArray removeAllObjects];
    [followArray removeAllObjects];
    [followGroupArray removeAllObjects];
    [favGroupArray removeAllObjects];
    [favArray removeAllObjects];
    
  
    
    switch ([[[mainNotificationArray firstObject] objectForKey:@"notificationType"] intValue])
    {
        case 1:
            [self showLocalNotificaitonWithTitle:@"Check who Liked your posts" andType:1];
            break;
            
        case 2:
            [self showLocalNotificaitonWithTitle:@"Check who comment on your posts" andType:2];
            break;
            
        case 3://
            [self showLocalNotificaitonWithTitle:@"Check new friend requests" andType:3];
            break;
            
        case 4:
            [self showLocalNotificaitonWithTitle:@"Check who start following you" andType:4];
            break;
            
        case 5:
            [self showLocalNotificaitonWithTitle:@"check new stories of your favorite friends" andType:4];
            break;
        case 6:
            [self showLocalNotificaitonWithTitle:@"check new stories of your favorite friends" andType:4];
            break;
            
        case 7:
                [self showLocalNotificaitonWithTitle:@"check new stories of your favorite friends" andType:4];
            break;
            
        default:
            break;
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    
    
    for (int i=0; i < [mainNotificationArray count]; i++)
    {
        
        NSMutableDictionary *dict  = [[mainNotificationArray objectAtIndex:i] mutableCopy];
        
        NSDate* notificationDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[dict objectForKey:@"time"]]longLongValue]];
        
        NSString* occurenceDate = [formatter stringFromDate:notificationDate];
        [dict setObject:occurenceDate forKey:@"occurenceDateString"];
        
        int type = [dict[@"notificationType"] intValue];
        switch (type)
        {
            case 1:
                [likeArray addObject:dict];
                break;
                
            case 2:
                [commentArray addObject:dict];
                break;
                
            case 3:
                [requestArray addObject:dict];
                break;
                
            case 4:
                if ([dict[@"receiverId"] intValue]==[[CurrentUser getObject] getUser].userId)
                {
                    [followArray addObject:dict];
                }
                else
                {
                    [favArray addObject:dict];
                }
                break;
                
            case 5:
                [favArray addObject:dict];
                break;
                
            case 6:
                [favArray addObject:dict];
                break;
                
            case 7:
                [favArray addObject:dict];
                break;
                
            default:
                break;
        }
    }
    
    [likeArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"likeNotification"]];
    [commentArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"commentNotification"]];
    [favArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"favNotification"]];
    [followArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"followNotification"]];
    [chatArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"chatNotification"]];
    [requestArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"requestNotification"]];
    
    
    
    // SORT ARRAYS
    likeArray = [[NSMutableArray alloc]initWithArray:[likeArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    commentArray = [[NSMutableArray alloc]initWithArray:[commentArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    requestArray = [[NSMutableArray alloc]initWithArray:[requestArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    followArray = [[NSMutableArray alloc]initWithArray:[followArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    favArray= [[NSMutableArray alloc]initWithArray:[favArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    [GetAndPushCashedData cashObject:likeArray withAction:@"likeNotification"];
    [GetAndPushCashedData cashObject:followArray withAction:@"followNotification"];
    [GetAndPushCashedData cashObject:favArray withAction:@"favNotification"];
    [GetAndPushCashedData cashObject:commentArray withAction:@"commentNotification"];
    [GetAndPushCashedData cashObject:requestArray withAction:@"requestNotification"];
    [GetAndPushCashedData cashObject:chatArray withAction:@"chatNotification"];
    [self clearNotification];
    
}
-(void)showLocalNotificaitonWithTitle:(NSString*)title andType:(int)type{
    
    [MPNotificationView notifyWithText:@"FCB Studio"
                                detail:title
                         andTouchBlock:^(MPNotificationView *notificationView) {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"openNotifications" object:nil];
                                 [GetAndPushCashedData cashObject:@(type) withAction:@"setSelectedTap"];
                             });
                         }];

}
-(void)clearNotification{
    notificationId = [[defaults objectForKey:@"notificationId"] intValue];
    [[UsersManager getInstance] clearNotification:user.userId notificationId:notificationId and:^(id currentClient) {
    } onFailure:^(NSError *error) {}];
}

-(NSMutableArray *)addUnReadValueToArray:(NSMutableArray*)array
{
    for (int i=0; i < [array count]; i++)
    {
        [[array objectAtIndex:i] setValue:@"unread" forKey:@"isRead"];
        //        NSMutableDictionary *temp1 =[array objectAtIndex:i];
        //        NSMutableDictionary *temp =[array objectAtIndex:i];
        //        [temp setValue:@"unread" forKey:@"isRead"];
        //        [array removeObject:temp1];
        //        [array addObject:temp];
        
    }
    return array;
}

-(void)updateUser{
    if ([self isUser]) {
        @try {
            if([RestServiceAgent internetAvailable])
            {
                [[UsersManager getInstance] getUserByID:user.userId and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        User* userObject = (User*)currentClient;
                        if(userObject)
                        {
                            
                            [self setUser:userObject];
                            [self saveCurrentUser];
                            [self refreshUser];
                        }
                    });
                }onFailure:^(NSError *error) {
                }];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"getUserCredit Error");
        }
    }

}

-(void)getLibVer{
    if ([self isUser]) {
        @try {
            if([RestServiceAgent internetAvailable])
            {
                [[StudioManager getInstance] getLibVer:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(currentClient)
                        {
                            if ([[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] objectForKey:@"CurrentLibVer"]) {
                                float currentVer =[[[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] objectForKey:@"CurrentLibVer"] floatValue];
                                float newVer =[currentClient[@"data"] floatValue];
                                if (newVer > currentVer) {
                                    [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setObject:@(newVer) forKey:@"CurrentLibVer"];
                                    [[SDImageCache sharedImageCache] cleanDisk];
                                    [[SDImageCache sharedImageCache] clearDisk];
                                    [GetAndPushCashedData removeObjectWithAction:@"StudioFolders"];
                                    [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setObject:@(newVer) forKey:@"CurrentLibVer"];
                                    [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] synchronize];
                                }
                            }
                            else{
                                float newVer =[currentClient[@"data"] floatValue];
                                [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setObject:@(newVer) forKey:@"CurrentLibVer"];
                                [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] synchronize];
                            }
                        }
                    });
                }onFailure:^(NSError *error) {
                }];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"getUserCredit Error");
        }
    }
}

@end
