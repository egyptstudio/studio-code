//
//  Post.m
//  FC Barcelona
//
//  Created by Eissa on 9/10/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "PostLike.h"

@implementation PostLike


-(void)encodeWithCoder:(NSCoder *)encoder
{
    
    [encoder encodeObject:@(self.likeId) forKey:@"likeId"];
    [encoder encodeObject:@(self.userId) forKey:@"userId"];
    [encoder encodeObject:self.fullName forKey:@"fullName"];
    [encoder encodeObject:self.profilePicURL forKey:@"profilePicURL"];
    [encoder encodeObject:@(self.isFollowing) forKey:@"isFollowing"];
}
-(id)initWithCoder:(NSCoder *)decoder
{
    self.likeId        = [[decoder decodeObjectForKey:@"likeId"] intValue];
    self.userId        = [[decoder decodeObjectForKey:@"userId"] intValue];
    self.fullName      = [decoder decodeObjectForKey:@"fullName"];
    self.profilePicURL = [decoder decodeObjectForKey:@"profilePicURL"];
    self.isFollowing   = [[decoder decodeObjectForKey:@"userProfilePicUrl"] boolValue];
    return self;
}


@end

