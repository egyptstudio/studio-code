//
//  DatabaseEngine.m
//  FCB Studio
//
//  Created by Essam Eissa on 6/1/15.
//  Copyright (c) 2015 Essam Eissa. All rights reserved.
//

#import "CachedImagesCleaner.h"
CachedImagesCleaner *cacheCleaner = nil;


@implementation CachedImagesCleaner
@synthesize timer;

+ (CachedImagesCleaner *)sharedInstance{
	if (cacheCleaner == nil) {
		cacheCleaner = [[CachedImagesCleaner alloc] init];
	}
	return cacheCleaner;
}

-(void)clearImagesAfterTimeInterval:(NSTimeInterval)interval{
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(checkCaches) userInfo:nil repeats:YES];
    [timer fire];
}

-(void)checkCaches{
    [[SDImageCache sharedImageCache] calculateSizeWithCompletionBlock:^(NSUInteger fileCount, NSUInteger totalSize) {
        if ((totalSize/1024.0f/1024.0f)>=100) {
            [[SDImageCache sharedImageCache] cleanDisk];
            [[SDImageCache sharedImageCache] calculateSizeWithCompletionBlock:^(NSUInteger fileCount, NSUInteger totalSize) {
                if ((totalSize/1024.0f/1024.0f)>=100) {
                    [[SDImageCache sharedImageCache] clearDisk]; 
                };
            }];
        };
    }];
}

@end

