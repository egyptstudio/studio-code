//
 //  Share.h
 //  FCB
 //
 //  Created by Eissa on 8/26/14.
 //  Copyright (c) 2014 Tawasolit. All rights reserved.
 //
 
 #import <Foundation/Foundation.h>

 typedef enum{
 shareFacebook = 1,
 shareTwitter  ,
 shareAll ,
 }shareMedia;
 
 @interface Share : NSObject
 
 -(void)initFrom:(UIViewController*)mainController shareContentToMedia:(shareMedia)media withText:(NSString*)text andImage:(UIImage*)image;
 
 @end
 