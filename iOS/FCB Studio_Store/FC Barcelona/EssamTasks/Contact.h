//
//  Contact.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Contact, Message, Fan,User;


@interface Contact : NSManagedObject

@property (nonatomic, retain) NSNumber * firstTimeChat;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSSet *contacts;
@property (nonatomic, retain) NSSet *recievedMessages;
@property (nonatomic, retain) NSSet *sentMessages;

@property (nonatomic, retain) Contact *user;

- (int)newMessageRecieved;
- (void)allMessagesRead;

+ (Contact *)contactFromFan:(Fan *)user;
+ (Contact *)contactFromUser:(User *)user;

+ (Contact *)chatWithUser:(Fan *)user;

@end

@interface Contact (CoreDataGeneratedAccessors)

- (void)addContactsObject:(Contact *)value;
- (void)removeContactsObject:(Contact *)value;
- (void)addContacts:(NSSet *)values;
- (void)removeContacts:(NSSet *)values;

- (void)addRecievedMessagesObject:(Message *)value;
- (void)removeRecievedMessagesObject:(Message *)value;
- (void)addRecievedMessages:(NSSet *)values;
- (void)removeRecievedMessages:(NSSet *)values;

- (void)addSentMessagesObject:(Message *)value;
- (void)removeSentMessagesObject:(Message *)value;
- (void)addSentMessages:(NSSet *)values;
- (void)removeSentMessages:(NSSet *)values;

@end
