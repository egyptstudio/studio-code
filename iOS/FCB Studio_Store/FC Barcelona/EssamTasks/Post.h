//
//  Post.h
//  FC Barcelona
//
//  Created by Eissa on 9/10/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserComment.h"
#import "StudioPhotoTag.h"
#import "PostLike.h"

@interface Post : NSObject

@property(nonatomic,assign) int  originalPostId;
@property(nonatomic,assign) int  studioPhotoId;
@property(nonatomic,assign) int  postId;
@property(nonatomic,assign) int  commentCount;
@property(nonatomic,assign) int  likeCount;
@property(nonatomic,assign) int  repostCount;
@property(nonatomic,assign) int  shareCount;
@property(nonatomic,assign) int  contestRank;
@property(nonatomic,assign) int  userId;
@property(nonatomic,assign) int  rePosterId;
@property(nonatomic,assign) int photoOrientation;
@property(nonatomic,assign) int privacy;

@property(nonatomic,assign) long creationTime;
@property(nonatomic,assign) long  lastUpdateTime;
@property(strong,nonatomic) NSString * postPhotoUrl;
@property(strong,nonatomic) NSString * postPhotoCaption;
@property(strong,nonatomic) NSString * userName;
@property(strong,nonatomic) NSString * userCountry;
@property(strong,nonatomic) NSString * userProfilePicUrl;
@property(strong,nonatomic) NSString * rePostedBy;
@property(strong,nonatomic) NSString * rePosterProfilePic;
@property(nonatomic) BOOL  reported;
@property(nonatomic) BOOL  following;
@property(nonatomic) BOOL  liked;
@property(nonatomic) BOOL  rePosted;
@property(nonatomic) BOOL  rePosterIsFollowing;
@property(nonatomic ,strong) NSMutableArray* tags;
@property(nonatomic ,strong) NSMutableArray* postComments;
@property(nonatomic ,strong) NSMutableArray* postLikes;
@end

