//
//  NSDate+TimeZone.m
//  Morasel
//
//  Created by yehia  on ٣١‏/٣‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Tawasol. All rights reserved.
//

#import "NSDate+TimeZone.h"

@implementation NSDate (TimeZone)

-(NSDate *)localTime
{
    NSTimeZone *tz = [NSTimeZone timeZoneForSecondsFromGMT:3600*2];
    NSInteger seconds = [tz secondsFromGMTForDate:self];
    return [NSDate dateWithTimeInterval: seconds sinceDate:self];
}

-(NSDate *)globalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate:self];
    return [NSDate dateWithTimeInterval: seconds sinceDate:self];
}

@end
