//
//  NSDate+TimeZone.h
//  Morasel
//
//  Created by yehia  on ٣١‏/٣‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TimeZone)

-(NSDate *) localTime;

-(NSDate *) globalTime;

@end
