//
//  JSMessagesViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "JSMessagesViewController.h"
#import "User.h"
#import "Contact.h"

@interface MessagingViewController : JSMessagesViewController <JSMessagesViewDelegate, JSMessagesViewDataSource,UITextViewDelegate>

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) NSDictionary *avatars;

@property (strong, nonatomic) Contact *profile;
@property (strong, nonatomic) UIImage *profileImage;

- (void)didSelectContact:(Contact *)contact;

@end
