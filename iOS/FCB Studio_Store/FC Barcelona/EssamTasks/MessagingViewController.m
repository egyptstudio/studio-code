//
//  JSMessagesViewController.h
//  FC Barcelona
//
//  Created by Essam Eissa on 2/4/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "MessagingViewController.h"
#import "JSMessage.h"
#import "Language.h"
#import "Contact.h"
#import "Message.h"
#import "BarcelonaAppDelegate.h"
#import "NSString+JSMessagesView.h"
#import "NSDate+TimeZone.h"
#import "CurrentUser.h"
#define kSubtitleJobs @"Jobs"
#define kSubtitleWoz @"Steve Wozniak"
#import "MPNotificationView.h"
#import "WebImage.h"
#import "WallManager.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "Fan.h"
#import "JSAvatarImageFactory.h"
#import "UsersManager.h"
@interface MessagingViewController ()<UIAlertViewDelegate> {
    Contact *currentProfile;
    int minChatId;
    NSTimer *timer;
    
    NSMutableArray* allMessagesIDs;
    BOOL canText;
}

@end

@implementation MessagingViewController

- (void)newMessage:(NSNotification *)notification
{
    
    Message *message = [notification.userInfo objectForKey:@"Message"];
    
    if ([message.sender.userId integerValue] == [_profile.userId integerValue] ) {
        
        [currentProfile allMessagesRead];
        
        [self.delegate didSendText:[message.message js_stringByTrimingWhitespace]
                            withID:message.messageId
                        fromSender:message.sender.userName
                            onDate:message.date];
    }
    else {
        
        
        [MPNotificationView notifyWithText:message.sender.userName
                                    detail:message.message
                             andTouchBlock:^(MPNotificationView *notificationView) {
                                 
                                 [self didSelectContact:message.sender];
                                 [self.delegate didSendText:[message.message js_stringByTrimingWhitespace]
                                                     withID:message.messageId
                                                 fromSender:message.sender.userName
                                                     onDate:message.date];
                                 
                             }];

    }
    
}


- (void)sendPressed:(UIButton *)sender
{
    [sender setEnabled:NO];
    Fan * from = [[Fan alloc] init];
    from.fanID = [CurrentUser getObject].getUser.userId ;
    from.fanName = [CurrentUser getObject].getUser.fullName;
    from.fanPicture = [CurrentUser getObject].getUser.profilePic;
    
    User * to = [[User alloc] init];
    to.userId = [_profile.userId intValue] ;
    to.fullName = _profile.userName;
    to.profilePic = _profile.imageURL;
    
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    if([RestServiceAgent internetAvailable])
    {
        canText = NO;
        [[WallManager getInstance] sendMessagesUserID:[CurrentUser getObject].getUser.userId
                                          andFriendID:[self.profile.userId intValue]
                                           andMessage:self.messageInputView.textView.text
                                                  and:^(id currentClient) {
              dispatch_sync(dispatch_get_main_queue(), ^{
                  canText = YES;
                  [SVProgressHUD dismiss];
              });
                                                      
                if (currentClient) {
                    if([[currentClient objectForKey:@"status"] intValue] == 2)
                    {
                        Fan * from = [[Fan alloc] init];
                        from.fanID = [CurrentUser getObject].getUser.userId ;
                        from.fanName = [CurrentUser getObject].getUser.fullName;
                        from.fanPicture = [CurrentUser getObject].getUser.profilePic;
                        
                        User * to = [[User alloc] init];
                        to.userId = [_profile.userId intValue] ;
                        to.fullName = _profile.userName;
                        to.profilePic = _profile.imageURL;
                        
                        
                        
                        NSDate *sentAt =[NSDate dateWithTimeIntervalSince1970:[currentClient[@"data"][@"storedTime"] longLongValue]];
                        NSLog(@"storedTime%@",[self relativeDateStringForDate:sentAt]);
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{

                            [Message messageWithId:[currentClient[@"data"][@"storedMessageID"] intValue]
                                           message:self.messageInputView.textView.text
                                              from:from
                                                to:to
                                            atTime:sentAt seen:YES];
                            
                            [self.delegate didSendText:[self.messageInputView.textView.text js_stringByTrimingWhitespace]
                                                withID:[NSNumber numberWithInt:[currentClient[@"data"][@"storedMessageID"] intValue]]
                                            fromSender:self.sender
                                                onDate:sentAt];
                        });
                    }
                }
        }onFailure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                canText = YES;
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }];
    }
    else
    {
        [SVProgressHUD dismiss];
        canText = YES;
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    return [dateFormat stringFromDate:date];
}
- (void)didSelectContact:(Contact *)contact {
    
    self.title = contact.userName;
    
    currentProfile = contact;
    
    self.profile = contact ;
    
    [contact allMessagesRead];
    
    [self fetchMessagesFromCoreData];
    
    [self scrollToBottomAnimated:NO];
    
}

- (void)fetchMessagesFromCoreData {
    
    self.messages = [NSMutableArray array];

    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                   ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sender.userId == %i OR reciever.userId == %i", [self.profile.userId intValue], [self.profile.userId intValue]];
    
    [request setPredicate:predicate];
    
    NSError *error = nil;
    
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        [self.messages addObject:[(Message *)array[idx] JSMessage]];
        
    }];
    
    NSArray *arrayOfIds = [array valueForKey:@"messageId"];
    allMessagesIDs = [array valueForKey:@"messageId"];
    minChatId = [[arrayOfIds valueForKeyPath:@"@min.doubleValue"] intValue];
    
    [self.tableView reloadData];
    [self scrollToBottomAnimated:YES];
}

- (void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setBackButton
{
    UIButton *backButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setFrame:CGRectMake(0, 0, 24, 32)];
    
    
    //    self.navigationItem.leftItemsSupplementBackButton = YES;
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:[[UIBarButtonItem alloc] initWithCustomView:backButton], nil];
    
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

#pragma mark - Messages view delegate: REQUIRED

- (void)didSendText:(NSString *)text withID:(NSNumber*)messageId fromSender:(NSString *)sender onDate:(NSDate *)date
{
    [self.messages addObject:[[JSMessage alloc] initWithText:text sender:sender date:date messageId:messageId]];
    
    if ([[[CurrentUser getObject] getUser].fullName isEqualToString:sender]) {
        [JSMessageSoundEffect playMessageSentSound];
        [self finishSend];

    }else{
        [JSMessageSoundEffect playMessageReceivedSound];
        [self.tableView reloadData];
    }
    
    [self scrollToBottomAnimated:YES];
}

- (JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *fullname = [(JSMessage *)self.messages[indexPath.row] sender];
    if ([fullname isEqualToString:[[CurrentUser getObject].getUser fullName]]) {
        return JSBubbleMessageTypeOutgoing;
    }
    else {
        return JSBubbleMessageTypeIncoming;
    }
}

- (UIImageView *)bubbleImageViewWithType:(JSBubbleMessageType)type
                       forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *fullname = [(JSMessage *)self.messages[indexPath.row] sender];
    
    if ([fullname isEqualToString:[[CurrentUser getObject].getUser fullName]]) {
        return [JSBubbleImageViewFactory bubbleImageViewForType:type
                                                          color:[UIColor colorWithRed:0/255.0f green:84/255.0f blue:164/255.0f alpha:1]];
    }
    else {
        return [JSBubbleImageViewFactory bubbleImageViewForType:type
                                                          color:[UIColor colorWithWhite:.95 alpha:1.0]];
    }
    
}

- (JSMessageInputViewStyle)inputViewStyle
{
    return JSMessageInputViewStyleFlat;
}

#pragma mark - Messages view delegate: OPTIONAL

- (BOOL)shouldDisplayTimestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 3 == 0) {
        return YES;
    }
    return NO;
}

//
//  *** Implement to customize cell further
//////////////////////////////////////////////
- (void)configureCell:(JSBubbleMessageCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([cell messageType] == JSBubbleMessageTypeOutgoing) {
        cell.bubbleView.textView.textColor = [UIColor whiteColor];
        cell.bubbleView.textView.textColor = [UIColor blackColor];
        
        
        if ([cell.bubbleView.textView respondsToSelector:@selector(linkTextAttributes)]) {
            NSMutableDictionary *attrs = [cell.bubbleView.textView.linkTextAttributes mutableCopy];
            [attrs setValue:[UIColor blueColor] forKey:UITextAttributeTextColor];
            
            cell.bubbleView.textView.linkTextAttributes = attrs;
            
            //
            
        }
    }
    

    
    if (cell.timestampLabel) {
        cell.timestampLabel.textColor = [UIColor lightGrayColor];
        cell.timestampLabel.shadowOffset = CGSizeZero;
    }
    
    if (cell.subtitleLabel) {
        cell.subtitleLabel.textColor = [UIColor whiteColor];
    }
    
#if TARGET_IPHONE_SIMULATOR
    cell.bubbleView.textView.dataDetectorTypes = UIDataDetectorTypeNone;
#else
    cell.bubbleView.textView.dataDetectorTypes = UIDataDetectorTypeAll;
#endif
    
    cell.backgroundColor = [UIColor clearColor];
}

//  *** Implement to use a custom send button
//
//  The button's frame is set automatically for you
//
- (UIButton *)sendButtonForInputView {
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[Language sharedInstance] currentLanguage] !=English ) {
        [sendButton.titleLabel setFont:[UIFont systemFontOfSize:10]];
    }
    [sendButton setTitle:[[Language sharedInstance] stringWithKey:@"MessagesWindow_Send"] forState:UIControlStateNormal];
    [sendButton.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [sendButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [sendButton setBackgroundColor:[UIColor colorWithRed:0/255.0f green:84/255.0f blue:164/255.0f alpha:1]];
    
    sendButton.frame = CGRectMake(self.messageInputView.textView.frame.origin.x + self.messageInputView.textView.frame.size.width,
                           8,
                           150.0f,
                           self.messageInputView.textView.frame.size.height - 8);
    return sendButton;
}

//  *** Implement to prevent auto-scrolling when message is added
//
- (BOOL)shouldPreventScrollToBottomWhileUserScrolling
{
    return YES;
}

// *** Implemnt to enable/disable pan/tap todismiss keyboard
//
- (BOOL)allowsPanToDismissKeyboard
{
    return NO;
}

#pragma mark - Messages view data source: REQUIRED

- (JSMessage *)messageForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messages objectAtIndex:indexPath.row];
}

- (UIImageView *)avatarImageViewForRowAtIndexPath:(NSIndexPath *)indexPath sender:(NSString *)sender
{
    NSString *fullname = [(JSMessage *)self.messages[indexPath.row] sender];
    
    __block UIImageView * imageVIew = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    if ([fullname isEqualToString:[[CurrentUser getObject].getUser fullName]]) {
        [imageVIew sd_setImageWithURL:[NSURL URLWithString:[CurrentUser getObject].getUser.profilePic] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            imageVIew.image =   [JSAvatarImageFactory avatarImage:image croppedToCircle:YES];
        }];
        
    }
    else {

        [imageVIew sd_setImageWithURL:[NSURL URLWithString:_profile.imageURL] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            imageVIew.image =   [JSAvatarImageFactory avatarImage:image croppedToCircle:YES];
        }];
        
    }
    return imageVIew;
}

- (void)initViews {
    float messageInputViewWidth = self.view.frame.size.width - 80;
    self.messageInputView.sendButton.frame = CGRectMake((messageInputViewWidth+8*2), 8, 60, 28);
    self.messageInputView.textView.textAlignment = NSTextAlignmentLeft;
    self.messageInputView.textView.frame = CGRectMake(8, 4.5, messageInputViewWidth, 36);
    
    self.messageInputView.textView.placeHolder = [[Language sharedInstance] stringWithKey:@"MessagesWindow_chatDefaultValue"];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.delegate = self;
    self.dataSource = self;
    
    canText = YES;
    
    [super viewDidLoad];
    
    self.messageInputView.textView.returnKeyType = UIReturnKeyDefault;
    self.messageInputView.textView.delegate = self;


        [self setBackButton];
    
    [self initViews];

    
    [[JSBubbleView appearance] setFont:[UIFont systemFontOfSize:16.0f]];
    
    self.title = self.profile.userName;
    self.sender = [[CurrentUser getObject].getUser fullName];
    
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"app_bg.png"]];
    [tempImageView setFrame:self.tableView.frame];
    self.tableView.backgroundView = tempImageView;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    //[self performSelectorInBackground:@selector(fetchMessagesFromCoreData) withObject:nil];

    [self fetchMessagesFromCoreData];
    [self scrollToBottomAnimated:NO];
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
//    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
//    refreshControl.tintColor = [UIColor whiteColor];
//    [refreshControl addTarget:self action:@selector(fetchMoreMessagesFromCoreData) forControlEvents:UIControlEventValueChanged];
//    self.refreshControl = refreshControl;
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    tableViewController.refreshControl = self.refreshControl;
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [self scrollToBottomAnimated:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessage:) name:@"NewMessageNotification" object:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setBool:YES forKey:@"Chatting"];
    
    [defaults synchronize];
    [self.navigationController setNavigationBarHidden:NO];
    
    
    if (![self isRegisteredForRemoteNotifications]){
        timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(pullNewMessages) userInfo:nil repeats:YES];
        [timer fire];
        NSLog(@"Not Reg====|");
    }
    [self checkIFNotificationEnabled];

}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return  canText;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"Chatting"];
    [timer invalidate];
    [defaults synchronize];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)pullNewMessages{

    if([RestServiceAgent internetAvailable] && [[CurrentUser getObject] isUser])
    {
        [[UsersManager getInstance] pullMessages:[[CurrentUser getObject] getUser].userId
                                           fanID:[_profile.userId intValue]
                                       messageID:[_messages count] > 0?  [[(JSMessage *)[_messages lastObject] messageId] intValue] : 0
     and:^(id currentClient) {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (currentClient)
             {
                 if ([currentClient[@"data"] count]>0)
                 {
                     for (NSDictionary* dic in currentClient[@"data"]) {
                         int messageId            = [dic[@"messageID"] intValue];
                         NSString *messageText    = dic[@"messageText"];
                                                     //[[NSString alloc] initWithCString:[dic[@"messageText"] cString] encoding:NSNonLossyASCIIStringEncoding];// The Magic Line //


                         int senderId             = [dic[@"senderUserID"] intValue];
                         NSString *senderName     = [dic[@"senderName"] isKindOfClass:[NSNull class]] ? @"Empty" : dic[@"senderName"];
                         NSString *senderPic      = dic[@"senderPicture"];
                         NSDate *sentAt           = [NSDate dateWithTimeIntervalSince1970:[dic[@"sentAt"] longLongValue]];
                         NSLog(@"storedTime%@",[self relativeDateStringForDate:sentAt]);

                         Fan *sender = [[Fan alloc] init];
                         sender.userId = senderId;
                         sender.fanName = senderName;
                         sender.fanPicture = senderPic;
                         Message *message = [Message messageWithId:messageId message:messageText from:sender to:[[CurrentUser getObject] getUser] atTime:sentAt seen:YES];
                         
                         //[[NSNotificationCenter defaultCenter] postNotificationName:@"NewMessageNotification" object:self userInfo:@{@"Message":message}];
                         
                         [self.messages addObject:[message JSMessage]];
                         NSIndexPath * newMessagePath= [NSIndexPath indexPathForRow:[self.messages count]-1 inSection:0];
                         [self.tableView beginUpdates];
                         [self.tableView insertRowsAtIndexPaths:@[newMessagePath] withRowAnimation:UITableViewRowAnimationBottom];
                         [self.tableView endUpdates];
                         [self scrollToBottomAnimated:YES];
                         [JSMessageSoundEffect playMessageReceivedSound];
                     }
                 }
             }
         });
          }onFailure:^(NSError *error) {
              
          }];
    }
}
-(void)checkIFNotificationEnabled{
    if (![self isRegisteredForRemoteNotifications] && [self canOpenSettings]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please, enable notifications" delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:@"Settings", nil];
        [alert show];
    }
    else if(![self isRegisteredForRemoteNotifications]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please, enable notifications" delegate:nil cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:nil];
        [alert show];
    }
    
}
-(BOOL)canOpenSettings{
#ifdef __IPHONE_8_0
    return &UIApplicationOpenSettingsURLString != NULL;
#endif
    return NO;
}
-(BOOL)isRegisteredForRemoteNotifications{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        return     [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
    }
    else{
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types == UIRemoteNotificationTypeNone)
            return NO;
        else
            return YES;
    }
}

-(void)openSettings{
#ifdef __IPHONE_8_0
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
#endif

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex!=alertView.cancelButtonIndex) {
        [self openSettings];
    }
}

@end
