//
//  StudioFolder.m
//  FC Barcelona
//
//  Created by Eissa on 9/26/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "StudioFolder.h"

@implementation StudioFolder
-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:@(self.folderId) forKey:@"folderId"];
    [encoder encodeObject:@(self.noOfPics) forKey:@"noOfPics"];
    [encoder encodeObject:@(self.folderType) forKey:@"folderType"];
    [encoder encodeObject:self.folderName forKey:@"folderName"];
    [encoder encodeObject:self.folderPicUrl forKey:@"folderPicUrl"];
    [encoder encodeObject:self.studioPhotos forKey:@"studioPhotos"];
    [encoder encodeObject:self.rank forKey:@"rank"];
}
-(id)initWithCoder:(NSCoder *)decoder
{
    self.folderId     =  [[decoder decodeObjectForKey:@"folderId"] intValue];
    self.noOfPics     =  [[decoder decodeObjectForKey:@"noOfPics"] intValue];
    self.folderType   =  [[decoder decodeObjectForKey:@"folderType"] intValue];
    self.folderName   =  [decoder decodeObjectForKey:@"folderName"];
    self.folderPicUrl =  [decoder decodeObjectForKey:@"folderPicUrl"];
    self.studioPhotos =  [decoder decodeObjectForKey:@"studioPhotos"];
    self.rank         =  [decoder decodeObjectForKey:@"rank"];
    return self;
}
@end
