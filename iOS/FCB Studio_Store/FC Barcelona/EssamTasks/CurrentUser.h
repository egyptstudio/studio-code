//
//  CurrentUser.h
//  FC Barcelona
//
//  Created by Eissa on 10/11/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface CurrentUser : NSObject
typedef void(^CurrentUserBlock)(int notificationType,NSString* title);
@property (nonatomic, copy) CurrentUserBlock completionBlock;

+ (CurrentUser *)getObject;
-(void)refreshUser;
-(User*)getUser;
-(BOOL)isUser;
-(void)saveCurrentUser;
-(void)setUser:(User*)newUser;
-(void)removeUser;
-(void)updateUserPoints;
-(void)updateNotifications;
-(void)updateUser;
-(void)getLibVer;
@end
