//
//  StudioFolder.h
//  FC Barcelona
//
//  Created by Eissa on 9/26/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StudioPhoto.h"

@interface StudioFolder : NSObject
@property int  folderId;
@property int  noOfPics;
@property int  folderType;
@property NSString * rank;
@property NSString * folderName;
@property NSString * folderPicUrl;
@property NSMutableArray *studioPhotos;

@end
