//
//  ImageDetailViewController.m
//  FC Barcelona
//
//  Created by Saad Ansari on 12/14/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "ImageDetailViewController.h"
#import "CurrentUser.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "ShareViewController.h"
#import <Social/Social.h>
#import "StudioPhotoTag.h"
#import "TagPostViewController.h"
#import "FansDetailView.h"
#import "StudioManager.h"
#import "CaptureViewController.h"
#import <KeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "GetAndPushCashedData.h"
@interface ImageDetailViewController () <UIAlertViewDelegate,EditViewDelegate>
{
    NSMutableArray        *array;
    DWTagList             *tagList;
    BOOL isMyPhoto;
    BOOL isExpand;
    BOOL isShareAnimated;
    NSMutableArray        *arrayOriginal;
    StudioPhoto *selectedPhoto ;
    int reported;
}

@end

@implementation ImageDetailViewController
@synthesize currentPost,isWall;

-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self populateView];
}

-(void)populateView
{
    [(CALayer*)[_reportButton layer] setCornerRadius:3];
    [(CALayer*)[_reportButton layer] setMasksToBounds:YES];

    if ([[CurrentUser getObject] isUser])
    {
        if([[CurrentUser getObject] getUser].userId == currentPost.userId || currentPost.rePosterId == [[CurrentUser getObject] getUser].userId )
        {
            isMyPhoto = YES;
        }
        else
        {
            isMyPhoto = NO;
        }
    }
    else
    {
        isMyPhoto = NO;
    }
    
    if (isMyPhoto)
    {
        _imgFollow.hidden=YES;
        _reportButton.hidden=YES;
        _editView.hidden=NO;
    }
    else
    {
        _imgFollow.hidden=NO;
        _reportButton.hidden=NO;
        _editView.hidden=YES;

//        [_reportButton setTitle:[[Language sharedInstance]stringWithKey:@"PhotoDetails_report"] forState:UIControlStateNormal];
        
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            CGRect frameRect = _bottomView.frame;
            frameRect.origin.y = 589;
            _bottomView.frame = frameRect;
        }
        else{
            CGRect frameRect = _bottomView.frame;
            frameRect.origin.y = 1050;
            _bottomView.frame = frameRect;
        
        }
    }
    
    
    _imgTitle.borderColor = [UIColor whiteColor];
    _imgTitle.borderWidth = 1.0;
    
    _imgUser.borderColor = [UIColor whiteColor];
    _imgUser.borderWidth = 1.0;
    
    [_imgTitle sd_setImageWithURL:[NSURL URLWithString:[currentPost userProfilePicUrl]] placeholderImage:[UIImage imageNamed:@"avatar"]];
    
    _lblTitleUser.text= currentPost.userName;
    _lblUserName.text= currentPost.userName;
    _txtVwCaption.text = currentPost.postPhotoCaption;
    
    NSDate* theDate = [NSDate dateWithTimeIntervalSince1970:[currentPost creationTime] ];
    _lblDate.text = [self relativeDateStringForDate:theDate];
    _lblLikeCount.text = [NSString stringWithFormat:@"%d",currentPost.likeCount];
    _lblCommentCount.text = [NSString stringWithFormat:@"%d",currentPost.commentCount];
    
    if (currentPost.following)
    {
        _imgFollow.hidden=YES;
    }
    else
    {
        [_imgFollow setImage:[UIImage imageNamed:@"unfollowed.png"]];
    }
    
    if (currentPost.contestRank >1 && currentPost.contestRank<=10) {
        _lblRate.text = [NSString stringWithFormat:@"%d",currentPost.contestRank];
    }
    else
    {
        _lblRate.hidden=YES;
        _imgRate.hidden=YES;
    }
    
    //    if (currentPost.reported)
    //    {
    //        _reportButton.enabled = NO;
    //    }
    
    [_imgPost sd_setImageWithURL:[NSURL URLWithString:[currentPost postPhotoUrl]]];

    array = [NSMutableArray new];
    arrayOriginal = [NSMutableArray new];
    
    [self getUserPosts];
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        self.mainScrollView.contentSize = CGSizeMake(self.tagView.frame.size.width , 1050);
    }
    else{
        self.mainScrollView.contentSize = CGSizeMake(self.tagView.frame.size.width , 1500);

    }
    
    _imgBg.frame = CGRectMake(0, 0, self.view.frame.size.width, self.mainScrollView.contentSize.height);
    
    isExpand = YES;
    
    isShareAnimated = NO;
    
    if ([currentPost originalPostId]!=0)
    {
        _lblRepost.text = [currentPost rePostedBy];
        [_btnRepost setImage:[UIImage imageNamed:@"repost_sc.png"] forState:UIControlStateNormal];
    
        [_imgUser sd_setImageWithURL:[NSURL URLWithString:[currentPost rePosterProfilePic]] placeholderImage:[UIImage imageNamed:@"avatar"]];
        
    }
    else
    {
        _lblRepost.text = currentPost.userName;
        [_imgUser sd_setImageWithURL:[NSURL URLWithString:[currentPost userProfilePicUrl]] placeholderImage:[UIImage imageNamed:@"avatar"]];

    }
    
    if ([currentPost studioPhotoId]==0)
    {
        _btnUse.hidden=YES;
    }
    
    /// Manar
    if ([currentPost liked]) {
        [_btnLike setImage:[UIImage imageNamed:@"wall-like_sc"] forState:UIControlStateNormal];
    }
    else {
        [_btnLike setImage:[UIImage imageNamed:@"wall-like"] forState:UIControlStateNormal];
    }
    
    //[self generateStudioPhoto];
}

- (void)selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex
{
    //NSLog(@"[array objectAtIndex:tagIndex] %@",[arrayOriginal objectAtIndex:tagIndex]);
    if ([[arrayOriginal objectAtIndex:tagIndex] isKindOfClass:[StudioPhotoTag class]])
    {
        StudioPhotoTag *tag = [arrayOriginal objectAtIndex:tagIndex];
        TagPostViewController * commentsView = [self.storyboard instantiateViewControllerWithIdentifier:@"TagPostViewController"];
        commentsView.tagID = [NSString stringWithFormat:@"%d",tag.tagId];
        commentsView.tagName = tag.tagName;
        [self.navigationController pushViewController:commentsView animated:YES];
        
    }
    else
    {
        NSDictionary *tag = [arrayOriginal objectAtIndex:tagIndex];
        TagPostViewController * commentsView = [self.storyboard instantiateViewControllerWithIdentifier:@"TagPostViewController"];
        commentsView.tagID = [NSString stringWithFormat:@"%@",tag[@"tagId"]];
        commentsView.tagName = tag[@"tagName"];
        [self.navigationController pushViewController:commentsView animated:YES];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [self infoBtnAction:nil];
}

-(IBAction)infoBtnAction:(id)sender
{
    CGPoint bottomOffset = CGPointMake(0, self.mainScrollView.contentSize.height - self.mainScrollView.bounds.size.height);
    [self.mainScrollView setContentOffset:bottomOffset animated:YES];
    _imgBg.frame = CGRectMake(0, 0, self.view.frame.size.width, self.mainScrollView.contentSize.height);
}

-(IBAction)openTopProfile
{
    if ([[CurrentUser getObject] getUser].userId == [currentPost userId]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
    }
    else
    {
        UIStoryboard *sb =  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ?
        [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] :
        [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
        fansDetailView.fanID = [currentPost userId] ;
        [self.navigationController pushViewController:fansDetailView animated:YES];
    }
    
}

-(IBAction)openBottomProfile
{
    if ([[CurrentUser getObject] getUser].userId == [currentPost rePosterId]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openProfile" object:nil];
    }
    else
    {
        UIStoryboard *sb =  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ?
        [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] :
        [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
        if ([currentPost rePosterId]==0) {
            fansDetailView.fanID= [currentPost userId] ;
        }
        else{
            fansDetailView.fanID = [currentPost rePosterId] ;
        }
        
        [self.navigationController pushViewController:fansDetailView animated:YES];
    }
}

-(IBAction)cancelBtnAction:(id)sender
{
    //    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getUserPosts
{
    
    @try {
        if ([[CurrentUser getObject] isUser])
        {
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            
            if ([RestServiceAgent internetAvailable]) {
                [[WallManager getInstance] getPostTag:[currentPost studioPhotoId] postId:[currentPost postId] and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if ([currentClient count]>0)
                        {
                            //NSLog(@"currentClient %@",currentClient);
                            reported = [[currentClient objectForKey:@"reported"] intValue];
                            if (reported==0)
                            {
                                _reportButton.enabled = YES;
                            }
                            
                            tagList = [[DWTagList alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tagView.frame.size.width, self.tagView.frame.size.height)];
                            [tagList setAutomaticResize:NO];
                            
                            [tagList setTagDelegate:self];
                            arrayOriginal = [[currentClient objectForKey:@"tags"] mutableCopy];
                            [currentPost setTags:arrayOriginal];
                            // Customisation
                            [tagList setCornerRadius:2.0f];
                            [tagList setTextColor:[UIColor whiteColor]];
                            for (int i = 0 ; i < arrayOriginal.count; i++)
                            {
                                if ([[arrayOriginal objectAtIndex:i] isKindOfClass:[StudioPhotoTag class]])
                                {
                                    StudioPhotoTag *tag = [arrayOriginal objectAtIndex:i];
                                    NSString *temp = @"#";
                                    temp = [temp stringByAppendingString:tag.tagName];
                                    [array addObject:temp];
                                }
                                else
                                {
                                    @try {
                                        NSDictionary *tag = [arrayOriginal objectAtIndex:i];
                                        NSString *temp = @"#";
                                        temp = [temp stringByAppendingString:tag[@"tagName"]];
                                        [array addObject:temp];
                                    }
                                    @catch (NSException *exception) {
                                        
                                    }


                                }
                                
                            }
                            [tagList setTags:array];
                            tagList.contentSize = CGSizeMake(self.tagView.bounds.size.width ,tagList.frameHeight );
                            
                            [self.tagView addSubview:tagList];
                            
                        }
                        
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
                
            }
            
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

-(IBAction)repostBtnAction:(id)sender
{
    if([[CurrentUser getObject] isUser]){
        if ([GetAndPushCashedData getObjectFromCash:@"RememberCostAlert"]) {
            [self promotePost];
        }
        else{
            
            NSString *title = [[Language sharedInstance] stringWithKey:@"RepostMessage"];
            NSString *message = [[Language sharedInstance] stringWithKey:@"DonAskAgain"];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Cancel"] otherButtonTitles:[[Language sharedInstance] stringWithKey:@"Confirm"],nil];
            
            UIButton *checkBox = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30.0)];
            [checkBox setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
            [checkBox addTarget:self action:@selector(setDonOpenAgain:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 270, 30)];
            [view addSubview:checkBox];
            [checkBox setCenter:CGPointMake(view.frame.size.width/2, view.frame.size.height/2)];
            
            [alert setValue:view  forKey:@"accessoryView"];
            [alert setTag:1];
            [alert show];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
}
-(void)setDonOpenAgain:(UIButton*)sender{
    
    if (![GetAndPushCashedData getObjectFromCash:@"RememberCostAlert"]) {
        [sender setBackgroundImage:[UIImage imageNamed:@"popup_checkbox_sc.png"] forState:UIControlStateNormal];
        [GetAndPushCashedData cashObject:@(YES) withAction:@"RememberCostAlert"];
    }
    else {
        [sender setBackgroundImage:[UIImage imageNamed:@"popup_checkbox.png"] forState:UIControlStateNormal];
        [GetAndPushCashedData removeObjectWithAction:@"RememberCostAlert"];
    }
}
-(void)promotePost{
    int requiredPoints = 50 ;
    
    if (requiredPoints > [[CurrentUser getObject] getUser].credit){
        [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPointsTab"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    }
    else{
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *date = [dateFormat stringFromDate:[NSDate date] ];
        
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        @try {
            if ([RestServiceAgent internetAvailable]) {
                [[WallManager getInstance] rePost:[currentPost postId] userId:[[CurrentUser getObject] getUser].userId date:date and:^(id currentClient) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        if([[currentClient objectForKey:@"status"] intValue] == 1)
                        {
                            [currentPost setRePosted:YES];
                            [_btnRepost setImage:[UIImage imageNamed:@"repost_sc.png"] forState:UIControlStateNormal];
                            
                            [[[CurrentUser getObject] getUser] setCredit:[CurrentUser getObject].getUser.credit - requiredPoints];
                            [[CurrentUser getObject] saveCurrentUser];

                            
                            [[[UIAlertView alloc] initWithTitle:nil
                                                        message:[[Language sharedInstance] stringWithKey:@"Home_repostAlert"]
                                                       delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"] otherButtonTitles:nil] show];
                        }
                    });
                }onFailure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                }];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }
        }
        @catch (NSException *exception) {
            [SVProgressHUD dismiss];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
}

-(IBAction)shareBtnAction:(id)sender
{
    NSString *sharedLink = [NSString stringWithFormat:@"http://fcbstudio.mobi/share.php?postid=%i",currentPost.postId];
    sharedLink = [sharedLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSArray *itemsToShare = @[@"",[NSURL URLWithString:sharedLink]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes =@[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact]; //or whichever you don't need
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}


-(IBAction)useBtnAction:(id)sender
{
    __block StudioPhoto *usedPhoto;
    
    if ([[CurrentUser getObject] isUser]) {
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[StudioManager getInstance] getStudioPhoto:[currentPost studioPhotoId] andUerId:[CurrentUser getObject].getUser.userId
                                                    and:^(id currentClient)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                     DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[StudioPhoto class]];
                     usedPhoto = [parser parseDictionary:(currentClient[@"data"][0])];
                     
                     if ([usedPhoto isPurchased]) {
                         
                         CaptureViewController* details = [self.storyboard instantiateViewControllerWithIdentifier:@"CaptureViewController"];
//                         details.studioPhoto =  usedPhoto;
//                         [self.navigationController pushViewController:details animated:YES];
                         
                         
                         [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
                         [WebImage processImageDataWithURLString:[usedPhoto photoUrl] cacheImage:YES andBlock:^(NSData *imageData) {
                             [SVProgressHUD dismiss];
                             BOOL controllerExist = NO;
                             for (UIViewController* controller in self.navigationController.viewControllers) {
                                 if ([controller isKindOfClass:[details class]])
                                 {
                                     controllerExist = YES;
                                     [(CaptureViewController*)controller setStudioPhoto:usedPhoto];
                                     [self.navigationController popToViewController:controller animated:YES];
                                     
                                 }
                             }
                             if (!controllerExist) {
                                 details.studioPhoto =  usedPhoto;
                                 [self.navigationController pushViewController:details animated:YES];
                             }
                         }];

                         
                     }else if ([usedPhoto isPremium] && ![[CurrentUser getObject] getUser].isPremium) {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[Language sharedInstance] stringWithKey:@"PremiumList_title"]
                                                                         message:[[Language sharedInstance] stringWithKey:@"PremuimRequired"]
                                                                        delegate:self
                                                               cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Cancel"]
                                                               otherButtonTitles:[[Language sharedInstance] stringWithKey:@"Subscribe"]
                                               , nil];
                         [alert setTag:10];
                         [alert show];
                     }
                     else if ([usedPhoto requiredPoints] > [[CurrentUser getObject] getUser].credit){
//                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Insufficient Points"
//                                                                         message:@"This photo requires more points, Do you want to purchase points now?"
//                                                                        delegate:self
//                                                               cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"]
//                                                               otherButtonTitles:@"Purchase", nil];
//                         [alert setTag:11];
//                         [alert show];
                         [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPointsTab"];
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
                         
                     }
                     else{
                         
                         @try {
                             [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
                             if([RestServiceAgent internetAvailable])
                             {
                                 [[StudioManager getInstance] purchaseStudioPhoto:[[[CurrentUser getObject] getUser] userId]  studioID:[usedPhoto studioPhotoId] and:^(id currentClient) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [SVProgressHUD dismiss];
                                         if (currentClient) {
                                             if([currentClient[@"status"] intValue]==1)
                                             {
                                                 [[[CurrentUser getObject] getUser] setCredit:[CurrentUser getObject].getUser.credit - [usedPhoto requiredPoints]];
                                                 [[CurrentUser getObject] saveCurrentUser];
                                                 
                                                 CaptureViewController* details = [self.storyboard instantiateViewControllerWithIdentifier:@"CaptureViewController"];
                                                 
                                                 [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
                                                 [WebImage processImageDataWithURLString:[usedPhoto photoUrl] cacheImage:YES andBlock:^(NSData *imageData) {
                                                     [SVProgressHUD dismiss];
                                                     BOOL controllerExist = NO;
                                                     for (UIViewController* controller in self.navigationController.viewControllers) {
                                                         if ([controller isKindOfClass:[details class]])
                                                         {
                                                             controllerExist = YES;
                                                             [(CaptureViewController*)controller setStudioPhoto:usedPhoto];
                                                             [self.navigationController popToViewController:controller animated:YES];
                                                             
                                                         }
                                                     }
                                                     if (!controllerExist) {
                                                         details.studioPhoto =  usedPhoto;
                                                         [self.navigationController pushViewController:details animated:YES];
                                                     }
                                                 }];
                                                 
                                               
                                             }
                                         }
                                     });
                                 }onFailure:^(NSError *error) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [SVProgressHUD dismiss];
                                         UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                         [alert show];
                                     });
                                 }];
                             }
                             else
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [SVProgressHUD dismiss];
                                     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                     [alert show];
                                 });
                             }
                         }
                         @catch (NSException *exception) {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [SVProgressHUD dismiss];
                             });
                             
                         }
                     }

                 });
             } onFailure:^(NSError *error) {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                     [alert show];
                 });
             }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}


-(IBAction)reportBtnAction:(id)sender
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:[[Language sharedInstance]stringWithKey:@"PhotoDetails_report_alert"]
                           delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]
                                          otherButtonTitles:[[Language sharedInstance]stringWithKey:@"Report"],nil];
    alert.tag = 2;
    [alert show];
}
-(IBAction)editBtnAction:(id)sender
{
    ShareViewController* share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
    [share setEditedImage:_imgPost.image];
    [share setOriginalImage:_imgPost.image];
    [share setStudioPhoto:selectedPhoto];
    [share setIsEditImage:YES];
    [share setCurrentPost:currentPost];
    [share setDelegate:self];
    [self.navigationController pushViewController:share animated:YES];
    
}
- (void)didViewEditedWith:(Post*)object;
{
    currentPost = object;
    [self populateView];
}

-(IBAction)deleteBtnAction:(id)sender
{
    
    
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:[[Language sharedInstance] stringWithKey:@"PhotoDetails_delete_alert"]
                                                   delegate:self cancelButtonTitle:[[Language sharedInstance]stringWithKey:@"Cancel"]otherButtonTitles:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"],nil];
    alert.tag = 101;
    [alert show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        switch (alertView.tag) {
            case 10:
            {
                [GetAndPushCashedData cashObject:@(YES) withAction:@"OpenPremTab"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
                return;
                break;
            }
            case 11:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
                return;
                
                break;
            }
            case 1:
            {
                [self promotePost];
                break;
            }
            default:
                break;
        }
    }
    
    if (buttonIndex == 0)
    {
        //Cancel
        if (alertView.tag==1) {
            //delete
        }
        else
        {
            //report
        }
    }
    else if (buttonIndex == 1)
    {
        //OK
        if (alertView.tag==101) {
            //delete
            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] deletePost:[currentPost postId] and:^(id currentClient) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                            if([[currentClient objectForKey:@"status"] intValue] == 1)
                            {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"imageDeleted" object:nil];
                                
                                //[[[UIAlertView alloc] initWithTitle:nil message:@"Image Deleted Successfully" delegate:self cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles:nil] show];
                                //                                if (self.isWall) {
                                //                                    [self.navigationController popViewControllerAnimated:YES];
                                //
                                //                                }
                                //                                else
                                //                                {
                                //                                    NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
                                //                                    if( currentIndex-2 >= 0 ) {
                                //                                        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-2] animated:YES];
                                //                                    }
                                //                                }
                                [self.navigationController popToRootViewControllerAnimated:YES];
                            }
                        });
                        }onFailure:^(NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [SVProgressHUD dismiss];
                                
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            });
                        }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
            
            
            
        }
 
    }
}

-(IBAction)fbBtnAction:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        // Initialize Compose View Controller
        SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        // Configure Compose View Controller
        // [vc setInitialText:self.captionTextField.text];
        [vc addImage:_imgPost.image];
        // Present Compose View Controller
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        NSString *message = @"It seems that we cannot talk to Facebook at the moment or you have not yet added your Facebook account to this device. Go to the Settings application to add your Facebook account to this device.";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:message delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"] otherButtonTitles:nil];
        [alertView show];
    }
}
-(IBAction)twBtnAction:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        // Initialize Compose View Controller
        SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        // Configure Compose View Controller
        // [vc setInitialText:self.captionTextField.text];
        [vc addImage:_imgPost.image];
        // Present Compose View Controller
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        NSString *message = @"It seems that we cannot talk to Twitter at the moment or you have not yet added your Twitter account to this device. Go to the Settings application to add your Twitter account to this device.";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:message delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_OK"] otherButtonTitles:nil];
        [alertView show];
    }
    
}
-(IBAction)ggBtnAction:(id)sender
{
    
}
-(IBAction)igBtnAction:(id)sender
{
    UIImage* instaImage = _imgPost.image;
    NSString* imagePath = [NSString stringWithFormat:@"%@/image.igo", [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
    [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
    [UIImagePNGRepresentation(instaImage) writeToFile:imagePath atomically:YES];
    //NSLog(@"image size: %@", NSStringFromCGSize(instaImage.size));
    _docController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:imagePath]];
    _docController.delegate=self;
    _docController.UTI = @"com.instagram.exclusivegram";
    [_docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
}


- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormat stringFromDate:date];
}

- (IBAction)like:(id)sender {
    
    if ([[CurrentUser getObject] isUser]) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        if (![currentPost liked]) {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] likePost:[[CurrentUser getObject] getUser].userId postId:[currentPost postId]
                                                    and:^(id currentClient) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [SVProgressHUD dismiss];
                                                            if (currentClient) {
                                                                if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                                {
                                                                    [sender setImage:[UIImage imageNamed:@"wall-like_sc.png"] forState:UIControlStateNormal];
                                                                    
                                                                    currentPost.liked = YES;
                                                                    currentPost.likeCount++;
                                                                    _lblLikeCount.text =[currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
                                                                }
                                                            }
                                                        });
                                                    }onFailure:^(NSError *error) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [SVProgressHUD dismiss];
                                                            
                                                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                            [alert show];
                                                        });
                                                    }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
        else
        {
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance] dislikePost:[[CurrentUser getObject] getUser].userId postId:[currentPost postId]
                                                       and:^(id currentClient) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [SVProgressHUD dismiss];
                                                               if (currentClient) {
                                                                   if([[currentClient objectForKey:@"status"] intValue] == 1)
                                                                   {
                                                                       [sender setImage:[UIImage imageNamed:@"wall-like.png"] forState:UIControlStateNormal];
                                                                       
                                                                       currentPost.liked = NO;
                                                                       currentPost.likeCount--;
                                                                       _lblLikeCount.text =[currentPost likeCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost likeCount]];
                                                                       
                                                                   }
                                                               }
                                                           });
                                                       }onFailure:^(NSError *error) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [SVProgressHUD dismiss];
                                                               
                                                               UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                                               [alert show];
                                                           });
                                                       }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
    }
}
- (IBAction)comment:(id)sender {
    if ([[CurrentUser getObject] isUser]) {
        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
        UIGraphicsBeginImageContext(window.bounds.size);
        [window.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CommentsViewController * commentsView = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
        commentsView.post = currentPost;
        commentsView.userID  = [[[CurrentUser getObject] getUser] userId];
        commentsView.userName = [currentPost userName];
        commentsView.delegate = self;
        commentsView.backGround = image;
        [self.navigationController pushViewController:commentsView animated:YES];
        
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
    
}
- (void)commentAdded{
    currentPost.commentCount++;
    _lblCommentCount.text = [currentPost commentCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost commentCount]];
}
- (void)commentDeleted{
    currentPost.commentCount--;
    _lblCommentCount.text = [currentPost commentCount]>999? [NSString stringWithFormat:@"%d k",[currentPost likeCount]/1000]:[NSString stringWithFormat:@"%d",[currentPost commentCount]];
}
-(void)viewWillAppear:(BOOL)animated{
    [_btnUse    setTitle:[[Language sharedInstance] stringWithKey:@"PhotoDetails_Use"] forState:UIControlStateNormal];
    [_btnDelete setTitle:[[Language sharedInstance] stringWithKey:@"DeleteMsg_Delete"] forState:UIControlStateNormal];
    [_btnEdit   setTitle:[[Language sharedInstance] stringWithKey:@"Edit2_Edit"] forState:UIControlStateNormal];

}
@end
