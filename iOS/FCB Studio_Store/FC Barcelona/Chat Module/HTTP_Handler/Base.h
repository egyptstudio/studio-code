////
////  Base.h
////  Cisco
////
////  Created by msaad on 7/7/13.
////  Copyright (c) 2013 UCS. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import <Foundation/Foundation.h>
//
//@protocol RequestDelegate <NSObject>
//
//- (void) didReceiveResponse:(id) data;
//- (void) didFailWithError:(NSError *)error;
//
//@end
//
//@interface Base : NSObject
//{
//    id <RequestDelegate>delegate;
//}
//
//- (void )tLoadFriendsWithDelegate:(id)_delegate andUserID:(int)userid andFilter:(NSString*)filter andLimit:(int)limit;
//- (void )tLoadMessageWithDelegate:(id)_delegate andUserID:(int)userid;
//- (void )tSendMessageWithDelegate:(id)_delegate andUserID:(int)userid andFriendID:(int)fanid andMessage:(NSString *)message;
//- (void )tDeleteMessageWithDelegate:(id)_delegate andMessageID:(int)msgid;
//- (void )tChangeStatusWithDelegate:(id)_delegate andUserID:(int)userid andStatus:(BOOL)status;
//
//@end
