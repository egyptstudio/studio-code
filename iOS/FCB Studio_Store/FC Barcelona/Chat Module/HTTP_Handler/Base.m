////
////  Base.m
////  Cisco
////
////  Created by msaad on 7/7/13.
////  Copyright (c) 2013 UCS. All rights reserved.
////
//
//#import "HttpClient.h"
//#import "AFNetworking.h"
//#import "Base.h"
//#import "DataHelper.h"
//
//static NSString *baseURL= @"http://barca.tawasoldev.com/barca/index.php/api/chat/";
//////Alrassas
////static NSString *baseURL= @"http://barca.tawasoldev.com/barca/index.php/api/chat/";
//
//@implementation Base
//
//- (void) setServerLink:(NSString *)link
//{
//    baseURL = link;
//}
//
//-(NSString *)convertDateToString:(NSDate *)date
//{
//    NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
//                                                          dateStyle:NSDateFormatterShortStyle
//                                                          timeStyle:NSDateFormatterFullStyle];
//    
//    return dateString;
//}
//
////loadFriends
//- (void )tLoadFriendsWithDelegate:(id)_delegate andUserID:(int)userid andFilter:(NSString*)filter andLimit:(int)limit
//{
//    NSString *url = [NSString stringWithFormat:@"%@loadFriends",baseURL];
//    NSLog(@"URL %@",url);
//    
//    //NSDictionary *dic = @{@"userID":[@"" stringByAppendingFormat:@"%ld",(long)userid], @"filter": filter , @"startingLimit":[@"" stringByAppendingFormat:@"%ld",(long)limit]};
//    
//    NSArray* vars = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",userid],filter,[NSString stringWithFormat:@"%d",limit], nil];
//    NSArray* keys = [NSArray arrayWithObjects:@"userID",@"filter",@"startingLimit", nil];
//
//    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
//
//    NSString* jsonString;
//    NSArray* tempKeysArray;
//    NSArray* tempValuesArray;
//    jsonString =[[DataHelper getInstance] convertToJSONString:dic];
//    tempKeysArray =[NSArray arrayWithObject:@"data"];
//    tempValuesArray = [NSArray arrayWithObject:jsonString];
//    
//    NSLog(@"Parameter %@",tempValuesArray);
//    
//    [[HttpClient sharedManager] postPath:url parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject)
//                                {
//                                    if([_delegate respondsToSelector:@selector(didReceiveResponse:)]) {
//                                        NSLog(@"response Object %@",responseObject);
//                                        [_delegate didReceiveResponse:responseObject];
//                                    }
//                                }
//                                failure:^(AFHTTPRequestOperation *operation, NSError *error)
//                                {
//                                    if([_delegate respondsToSelector:@selector(didFailWithError:)]) {
//                                        [_delegate didFailWithError:error];
//                                    }
//                                }];
//}
//
////loadMessage
//- (void )tLoadMessageWithDelegate:(id)_delegate andUserID:(int)userid
//{
//    
//    NSString *url = [NSString stringWithFormat:@"%@loadMessage",baseURL];
//    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],[self convertDateToString:[NSDate date]],nil];
//    
//    NSArray* keys = [NSArray arrayWithObjects:@"myID",@"lastTimeStampChecked", nil];
//    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
//    
//    [[HttpClient sharedManager] postPath:url parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         if([_delegate respondsToSelector:@selector(didReceiveResponse:)]) {
//             [_delegate didReceiveResponse:responseObject[@"News"]];
//         }
//     }
//                                failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {
//         if([_delegate respondsToSelector:@selector(didFailWithError:)]) {
//             [_delegate didFailWithError:error];
//         }
//     }];
//}
////sendMessage
//- (void )tSendMessageWithDelegate:(id)_delegate andUserID:(int)userid andFriendID:(int)fanid andMessage:(NSString *)message
//{
//    
//    NSString *url = [NSString stringWithFormat:@"%@sendMessage",baseURL];
//    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],[@"" stringByAppendingFormat:@"%ld",(long)fanid], message,nil];
//    
//    NSArray* keys = [NSArray arrayWithObjects:@"myID",@"fanD",@"messageText", nil];
//    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
//    
//    [[HttpClient sharedManager] postPath:url parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         if([_delegate respondsToSelector:@selector(didReceiveResponse:)]) {
//             [_delegate didReceiveResponse:responseObject[@"News"]];
//         }
//     }
//                                failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {
//         if([_delegate respondsToSelector:@selector(didFailWithError:)]) {
//             [_delegate didFailWithError:error];
//         }
//     }];
//}
////deleteMessage
//- (void )tDeleteMessageWithDelegate:(id)_delegate andMessageID:(int)msgid
//{
//    NSString *url = [NSString stringWithFormat:@"%@deleteMessage",baseURL];
//    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)msgid],nil];
//    
//    NSArray* keys = [NSArray arrayWithObjects:@"messageID",nil];
//    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
//    
//    [[HttpClient sharedManager] postPath:url parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         if([_delegate respondsToSelector:@selector(didReceiveResponse:)]) {
//             [_delegate didReceiveResponse:responseObject[@"News"]];
//         }
//     }
//                                failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {
//         if([_delegate respondsToSelector:@selector(didFailWithError:)]) {
//             [_delegate didFailWithError:error];
//         }
//     }];
//}
////changeStatus
//- (void )tChangeStatusWithDelegate:(id)_delegate andUserID:(int)userid andStatus:(BOOL)status
//{
//    NSString *url = [NSString stringWithFormat:@"%@changeStatus",baseURL];
//    NSArray* vars = [NSArray arrayWithObjects:[@"" stringByAppendingFormat:@"%ld",(long)userid],@"true",nil];
//    
//    NSArray* keys = [NSArray arrayWithObjects:@"myID",@"online",nil];
//    NSDictionary* dic = [NSDictionary dictionaryWithObjects:vars forKeys:keys];
//    
//    [[HttpClient sharedManager] postPath:url parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         if([_delegate respondsToSelector:@selector(didReceiveResponse:)]) {
//             [_delegate didReceiveResponse:responseObject[@"News"]];
//         }
//     }
//                                failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {
//         if([_delegate respondsToSelector:@selector(didFailWithError:)]) {
//             [_delegate didFailWithError:error];
//         }
//     }];
//}
//
//@end
