//
//  ChatMainViewController.h
//  FC Barcelona
//
//  Created by Saad Ansari on 12/30/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ChatMainViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@property (weak, nonatomic) IBOutlet UILabel *comingSoon;


@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UIView *adBanner;


@end
