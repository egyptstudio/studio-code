//
//  AWRMenuCell.h
//  AWR-CMS
//
//  Created by Saad Ansari on 2/2/14.
//  Copyright (c) 2014 Saad Ansari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeNotificationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@end
