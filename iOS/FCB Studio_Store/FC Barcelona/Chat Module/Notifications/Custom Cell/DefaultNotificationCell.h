//
//  AWRMenuCell.h
//  AWR-CMS
//
//  Created by Saad Ansari on 2/2/14.
//  Copyright (c) 2014 Saad Ansari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DefaultNotificationCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *cellName;
@property (nonatomic, weak) IBOutlet UILabel *cellPrice;


@end
