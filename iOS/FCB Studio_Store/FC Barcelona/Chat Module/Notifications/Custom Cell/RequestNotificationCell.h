//
//  SimpleTableCell.h
//  SimpleTable
//
//  Created by Simon Ng on 28/4/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImageView.h"
@interface RequestNotificationCell : UITableViewCell

@property (nonatomic,weak)IBOutlet UILabel *titleLabel;
@property (nonatomic,weak)IBOutlet UIImageView *imgProfile;
@property (nonatomic,weak)IBOutlet UILabel *lblCount;
@property (nonatomic,weak)IBOutlet UIImageView *imgFavorite;
@property (nonatomic,weak)IBOutlet UIImageView *imgFollow;
@property (nonatomic,weak)IBOutlet UIButton *accpetBtns;
@property (nonatomic,weak)IBOutlet UIButton *cancelBtns;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@end
