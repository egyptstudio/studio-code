//
//  NotificationViewController.m
//  FC Barcelona
//
//  Created by Saad Ansari on 2/19/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "NotificationViewController.h"
#import "SVProgressHUD.h"
#import "RestServiceAgent.h"
#import "WallManager.h"
#import "CurrentUser.h"
#import "RequestNotificationCell.h"
#import "DefaultNotificationCell.h"
#import "LikeNotificationCell.h"
#import <QuartzCore/QuartzCore.h>
#import "NotificationHeaderCellTableViewCell.h"
#import "UsersManager.h"
#import "ImageDetailViewController.h"
#import "Post.h"
#import "WebImage.h"
#import "FansManager.h"
#import "Fan.h"
#import "RequestNotificationCell.h"
#import "FansDetailView.h"
#import "GetAndPushCashedData.h"
#import "SystemMessage.h"
#define kBoarderWidth 2.0
#define kCornerRadius 1.0


@interface NotificationViewController ()<NSURLConnectionDataDelegate,NSURLConnectionDelegate>
{
    int pageNo;
    NSString *lastRequestTime;
    NSMutableArray *mainNotificationArray;

    NSMutableArray *likeArray;
    NSMutableArray *likeGroupArray;
    NSMutableArray *commentArray;
    NSMutableArray *commentGroupArray;
    NSMutableArray *chatArray;
    NSMutableArray *followArray;
    NSMutableArray *followGroupArray;
    NSMutableArray *favArray;
    NSMutableArray *favGroupArray;
    NSMutableArray *requestArray;
    NSURLConnection* getNotificationConnection;
    NSMutableData* notificatoinData;
    int selectedView;
    
    BOOL isBtnLike;
    BOOL isBtnComment;
    BOOL isBtnChat;
    BOOL isBtnFollow;
    BOOL isBtnFav;
    BOOL isBtnRequest;
    NSArray *coreDataFavArray;
}
@property(nonatomic,weak)IBOutlet UIButton *btnLike;
@property(nonatomic,weak)IBOutlet UIButton *btnComment;
@property(nonatomic,weak)IBOutlet UIButton *btnChat;
@property(nonatomic,weak)IBOutlet UIButton *btnFollow;
@property(nonatomic,weak)IBOutlet UIButton *btnFav;
@property(nonatomic,weak)IBOutlet UIButton *btnRequest;

@property(nonatomic,weak)IBOutlet UILabel *lbl_Like;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Comment;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Chat;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Follow;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Fav;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Request;
@property(nonatomic,weak)IBOutlet UITableView *tbl_main;
@property(nonatomic,weak)IBOutlet UIView *view_Initial;

@property(nonatomic,weak)IBOutlet UILabel *lbl_Title;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Like_Bottom;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Comment_Bottom;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Chat_Bottom;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Follow_Bottom;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Fav_Bottom;
@property(nonatomic,weak)IBOutlet UILabel *lbl_Request_Bottom;
@property(nonatomic,weak)IBOutlet UILabel *lbl_noNotifications;

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;

@end

@implementation NotificationViewController
#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedView = 1;
    [self setBorderToLabels];
    pageNo=0;
    mainNotificationArray = [NSMutableArray new];
    likeArray= [NSMutableArray new];
    likeGroupArray = [NSMutableArray new];
    commentArray= [NSMutableArray new];
    commentGroupArray = [NSMutableArray new];

    chatArray= [NSMutableArray new];
    followArray= [NSMutableArray new];
    followGroupArray = [NSMutableArray new];
    favArray= [NSMutableArray new];
    requestArray= [NSMutableArray new];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    mainNotificationArray = [[NSMutableArray alloc] initWithArray:[defaults objectForKey:@"Notifications"]];
    lastRequestTime = [mainNotificationArray count]==0 ?@"" : [defaults objectForKey:@"currentRequestTimeForNotification"];
    [self.tbl_main setBackgroundColor:[UIColor clearColor]];
    [self.tbl_main setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tbl_main setSeparatorColor:[UIColor blackColor]];



    self.lbl_Chat.hidden=YES;
    self.lbl_Comment.hidden=YES;
    self.lbl_Follow.hidden=YES;
    self.lbl_Fav.hidden=YES;
    self.lbl_Request.hidden=YES;
    self.lbl_Like.hidden=YES;

    
    [self populateCacheData];
    [self getNotifications];
     isBtnLike=NO;
     isBtnComment=NO;
     isBtnChat=NO;
     isBtnFollow=NO;
     isBtnFav=NO;
     isBtnRequest=NO;
    [self like_Action:nil];
    [self setLocalisabels];
    

}
-(void)setLocalisabels{
    _lbl_Title.text = [[Language sharedInstance] stringWithKey:@"notifications"];
    _lbl_Like_Bottom.text = [[Language sharedInstance] stringWithKey:@"notifications_likes"];
    _lbl_Comment_Bottom.text = [[Language sharedInstance] stringWithKey:@"notifications_comments"];
    _lbl_Chat_Bottom.text = [[Language sharedInstance] stringWithKey:@"notifications_messages"];
    _lbl_Follow_Bottom.text = [[Language sharedInstance] stringWithKey:@"notifications_likes_Following"];
    _lbl_Fav_Bottom.text = [[Language sharedInstance] stringWithKey:@"SystemNotifications"];
    _lbl_Request_Bottom.text = [[Language sharedInstance] stringWithKey:@"notifications_requests"];
    _lbl_noNotifications.text = [[Language sharedInstance] stringWithKey:@"no_notfications_initial_state"];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
//
}
-(void)setSelectedTap:(int)type{

    switch (type) {
        case 1:
            [self like_Action:_btnLike];
            break;
        case 2:
            [self comment_Action:_btnComment];
            break;
        case 3:
            [self request_Action:_btnRequest];
            break;
        case 4:
            [self follow_Action:_btnFollow];
            break;
        default:
            [self fav_Action:_btnFav];
            break;
    }
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openStudio:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}

- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}

- (IBAction)openMenu:(id)sender
{
    if (!isPad) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openMenu" object:nil];
    }
}



- (void)getNotifications
{
    pageNo++;
    NSUserDefaults * defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    int notificationId = [[defaults objectForKey:@"notificationId"] intValue];
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            NSString *post = [NSString stringWithFormat:@"data={\"notificationId\" : \"%d\",      \"pageNo\": %i,      \"userId\": %i }",notificationId,pageNo,[[CurrentUser getObject] getUser].userId];
            
            NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
            NSURL *url = [NSURL URLWithString:@"http://barca.tawasoldev.com/barca/index.php/api/notification/getNotificationsIOS"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90.0];
            [request setHTTPMethod:@"POST"];
            
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:postData];
            
            getNotificationConnection = [[NSURLConnection alloc]initWithRequest:request delegate:self    startImmediately:NO];
            
            notificatoinData = [[NSMutableData alloc]init];
            
            [getNotificationConnection scheduleInRunLoop:[NSRunLoop mainRunLoop]
                                             forMode:NSDefaultRunLoopMode];
            [getNotificationConnection start];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            pageNo--;
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
        pageNo--;
    }
}

-(NSMutableArray*)getGroupedBasedOnDate:(NSMutableArray*)scatteredArray
{
    NSMutableArray* groupedArray = [[NSMutableArray alloc]init];
    
    NSString* previousDateString = [[scatteredArray objectAtIndex:0] objectForKey:@"occurenceDateString"];
    NSMutableArray* currentGroupArray = [[NSMutableArray alloc]init];
    [currentGroupArray addObject:[scatteredArray objectAtIndex:0]];
    
    for(int i = 1 ; i < scatteredArray.count ; i++)
    {
        if([[[scatteredArray objectAtIndex:i] objectForKey:@"occurenceDateString"] isEqualToString:previousDateString])
        {
            [currentGroupArray addObject:[scatteredArray objectAtIndex:i]];
        }else
        {
            NSDictionary* groupDict = [[NSDictionary alloc]initWithObjects:@[previousDateString,currentGroupArray] forKeys:@[@"groupTitle",@"groupMembers"]];
            [groupedArray addObject:groupDict];
            previousDateString = [[scatteredArray objectAtIndex:i] objectForKey:@"occurenceDateString"];
            currentGroupArray = [[NSMutableArray alloc]init];
            [currentGroupArray addObject:[scatteredArray objectAtIndex:i]];
        }
    }
    
    NSDictionary* groupDict = [[NSDictionary alloc]initWithObjects:@[previousDateString,currentGroupArray] forKeys:@[@"groupTitle",@"groupMembers"]];
    [groupedArray addObject:groupDict];
    
    return groupedArray;
}



- (IBAction)like_Action:(id)sender
{
    self.lbl_Like.hidden = YES;
    
    self.btnChat.selected = NO;
    self.btnComment.selected = NO;
    self.btnFav.selected = NO;
    self.btnFollow.selected = NO;
    self.btnLike.selected = YES;
    self.btnRequest.selected = NO;
    
    self.lbl_Like_Bottom.textColor = [UIColor colorWithRed:247.0/255.0 green:211.0/255.0 blue:9.0/255.0 alpha:1];
    self.lbl_Comment_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Chat_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Follow_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Request_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Fav_Bottom.textColor = [UIColor whiteColor];

    if(selectedView != 1)
    {
        selectedView = 1;
        [self.tbl_main reloadData];
        [self.tbl_main setNeedsDisplay];
    }
    
    if(likeArray.count == 0)
    {
        self.tbl_main.hidden = YES;
        self.view_Initial.hidden = NO;
    }else
    {
        self.tbl_main.hidden = NO;
        self.view_Initial.hidden = YES;
    }
    if (isBtnLike)
    {
        likeArray =[self setReadValueToArray:likeArray];
    }
    else
    {
        isBtnLike = YES;
    }
}
- (IBAction)comment_Action:(id)sender
{
    self.lbl_Comment.hidden = YES;
    self.btnChat.selected = NO;
    self.btnComment.selected = YES;
    self.btnFav.selected = NO;
    self.btnFollow.selected = NO;
    self.btnLike.selected = NO;
    self.btnRequest.selected = NO;
    
    self.lbl_Like_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Comment_Bottom.textColor = [UIColor colorWithRed:247.0/255.0 green:211.0/255.0 blue:9.0/255.0 alpha:1];
    self.lbl_Chat_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Follow_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Request_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Fav_Bottom.textColor = [UIColor whiteColor];

    if(selectedView != 2)
    {
        selectedView = 2;
        [self.tbl_main reloadData];
        [self.tbl_main setNeedsDisplay];
    }
    if(commentArray.count == 0)
    {
        self.tbl_main.hidden = YES;
        self.view_Initial.hidden = NO;
    }else
    {
        self.tbl_main.hidden = NO;
        self.view_Initial.hidden = YES;
    }
    if (isBtnComment)
    {
        commentArray =[self setReadValueToArray:commentArray];
    }
    else
    {
        isBtnComment = YES;
    }
}
- (IBAction)messages_Action:(id)sender
{
    self.lbl_Chat.hidden = YES;
    self.btnChat.selected = YES;
    self.btnComment.selected = NO;
    self.btnFav.selected = NO;
    self.btnFollow.selected = NO;
    self.btnLike.selected = NO;
    self.btnRequest.selected = NO;
    
    self.lbl_Like_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Comment_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Chat_Bottom.textColor = [UIColor colorWithRed:247.0/255.0 green:211.0/255.0 blue:9.0/255.0 alpha:1];
    self.lbl_Follow_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Request_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Fav_Bottom.textColor = [UIColor whiteColor];
    selectedView = 3;
    

    if(chatArray.count == 0)
    {
        self.tbl_main.hidden = YES;
        self.view_Initial.hidden = NO;
    }else
    {
        self.tbl_main.hidden = NO;
        self.view_Initial.hidden = YES;
    }
    [self.tbl_main reloadData];
    if (isBtnChat)
    {
        chatArray =[self setReadValueToArray:chatArray];
    }
    else
    {
        isBtnChat = YES;
    }
    
}
- (IBAction)request_Action:(id)sender
{
    self.lbl_Request.hidden = YES;
    self.btnChat.selected = NO;
    self.btnComment.selected = NO;
    self.btnFav.selected = NO;
    self.btnFollow.selected = NO;
    self.btnLike.selected = NO;
    self.btnRequest.selected = YES;
    
    self.lbl_Like_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Comment_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Chat_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Follow_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Request_Bottom.textColor = [UIColor colorWithRed:247.0/255.0 green:211.0/255.0 blue:9.0/255.0 alpha:1];
    self.lbl_Fav_Bottom.textColor = [UIColor whiteColor];
    selectedView = 4;
    

    if(requestArray.count == 0)
    {
        self.tbl_main.hidden = YES;
        self.view_Initial.hidden = NO;
    }else
    {
        self.tbl_main.hidden = NO;
        self.view_Initial.hidden = YES;
    }
    [self.tbl_main reloadData];
    if (isBtnRequest)
    {
        requestArray =[self setReadValueToArray:requestArray];
    }
    else
    {
        isBtnRequest = YES;
    }

}
- (IBAction)follow_Action:(id)sender
{
    self.lbl_Follow.hidden = YES;
    self.btnChat.selected = NO;
    self.btnComment.selected = NO;
    self.btnFav.selected = NO;
    self.btnFollow.selected = YES;
    self.btnLike.selected = NO;
    self.btnRequest.selected = NO;
    
    self.lbl_Like_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Comment_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Chat_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Follow_Bottom.textColor = [UIColor colorWithRed:247.0/255.0 green:211.0/255.0 blue:9.0/255.0 alpha:1];
    self.lbl_Request_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Fav_Bottom.textColor = [UIColor whiteColor];
    selectedView = 5;
    
    
    if(followArray.count == 0)
    {
        self.tbl_main.hidden = YES;
        self.view_Initial.hidden = NO;
    }else
    {
        self.tbl_main.hidden = NO;
        self.view_Initial.hidden = YES;
    }
    [self.tbl_main reloadData];
    if (isBtnFollow)
    {
        followArray =[self setReadValueToArray:followArray];
    }
    else
    {
        isBtnFollow = YES;
    }
}

- (IBAction)fav_Action:(id)sender
{
    coreDataFavArray = [[NSArray alloc] initWithArray:[SystemMessage allMessages]];

    self.lbl_Fav.hidden = YES;
    self.btnChat.selected = NO;
    self.btnComment.selected = NO;
    self.btnFav.selected = YES;
    self.btnFollow.selected = NO;
    self.btnLike.selected = NO;
    self.btnRequest.selected = NO;
    
    self.lbl_Like_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Comment_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Chat_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Follow_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Request_Bottom.textColor = [UIColor whiteColor];
    self.lbl_Fav_Bottom.textColor = [UIColor colorWithRed:247.0/255.0 green:211.0/255.0 blue:9.0/255.0 alpha:1];

    selectedView = 6;
    

    if(coreDataFavArray.count == 0)
    {
        self.tbl_main.hidden = YES;
        self.view_Initial.hidden = NO;
    }else
    {
        self.tbl_main.hidden = NO;
        self.view_Initial.hidden = YES;
    }
    [self.tbl_main reloadData];
    //[SystemMessage markAllSeen];

}
-(void)setBorderToLabels
{
    self.lbl_Chat.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lbl_Chat.layer.borderWidth = 1.0;
    
    self.lbl_Comment.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lbl_Comment.layer.borderWidth = 1.0;
    
    self.lbl_Fav.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lbl_Fav.layer.borderWidth = 1.0;
    
    self.lbl_Follow.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lbl_Follow.layer.borderWidth = 1.0;
    
    self.lbl_Like.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lbl_Like.layer.borderWidth = 1.0;
    
    self.lbl_Request.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lbl_Request.layer.borderWidth = 1.0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    switch (selectedView)
    {
        case 1:
            return [likeGroupArray count];
            break;
        case 2:
            return [commentGroupArray count];
            break;
        case 3:
            return 1;
            break;
        case 4:
            return 1;
            break;
        case 5:
            return [followGroupArray count];
            break;
        case 6:
            return [coreDataFavArray count];
            break;
        default:
            return 1;
            break;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    
    switch (selectedView)
    {
        case 1:
            return [[[likeGroupArray objectAtIndex:section] objectForKey:@"groupMembers"] count];
            break;
        case 2:
            return [[[commentGroupArray objectAtIndex:section] objectForKey:@"groupMembers"] count];
            break;
        case 3:
            return [chatArray count];
            break;
        case 4:
            return [requestArray count];
            break;
        case 5:
            return [[[followGroupArray objectAtIndex:section] objectForKey:@"groupMembers"] count];
            break;
        case 6:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (selectedView)
    {
        case 1:
            return 74;
            break;
        case 2:
            return 74;
            break;
        case 3:
            return 74;
            break;
        case 4:
            return 110;
            break;
        case 5:
            return 74;
            break;
        case 6:
            return 74;
            break;
            
        default:
            return 0;
            break;
    }

  
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (selectedView)
    {
        case 1:
            return 30;
            break;
        case 2:
            return 30;
            break;
        case 3:
            return 30;
            break;
        case 4:
            return 0;
            break;
        case 5:
            return 30;
            break;
        case 6:
            return 0;
            break;
            
        default:
            return 0;
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"MyFeedCell";
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        /*NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DefaultNotificationCell" owner:self options:nil];
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (DefaultNotificationCell *)currentObject;
                break;
            }
        }*/
        
        cell  = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary* content;
    switch (selectedView)
    {
        case 1:
            content = [[[likeGroupArray objectAtIndex:indexPath.section] objectForKey:@"groupMembers"] objectAtIndex:indexPath.row];
            cell = [self configureLikeCell:content descriptionString:@"liked a photo of you"];
            break;
        case 2:
            content = [[[commentGroupArray objectAtIndex:indexPath.section] objectForKey:@"groupMembers"] objectAtIndex:indexPath.row];
            cell = [self configureLikeCell:content descriptionString:@"commented on a photo of you"];
            break;
        case 4:
            cell = [self configureRequestCell:indexPath];
            break;
        case 5:
            content = [[[followGroupArray objectAtIndex:indexPath.section] objectForKey:@"groupMembers"] objectAtIndex:indexPath.row];
            cell = [self configureFollowCell:content descriptionString:@"started following you"];
            break;
        case 6:
            cell = [self configureFavCell:(SystemMessage*)[coreDataFavArray objectAtIndex:indexPath.row] descriptionString:@""];
            break;
    }

    //cell.cellName.text = @"Hello Saad";
    return cell;
}

-(LikeNotificationCell*)configureLikeCell:(NSDictionary*)content descriptionString:(NSString*)descriptionString{

    LikeNotificationCell *cell;
    
    NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LikeNotificationCell" owner:self options:nil];
    for (id currentObject in topLevelObjects)
    {
        if ([currentObject isKindOfClass:[UITableViewCell class]])
        {
            cell = (LikeNotificationCell *)currentObject;
            break;
        }
    }
   
    [[cell descriptionLabel]setText:[NSString stringWithFormat:@"%@ %@",[content objectForKey:@"senderFullName"],descriptionString]];
    
    if ([[content valueForKey:@"isRead"] isEqualToString:@"unread"])
    {
        [cell.contentView setBackgroundColor:[UIColor lightGrayColor]];
        cell.timeLabel.textColor = [UIColor whiteColor];
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate* notificationDate = [NSDate dateWithTimeIntervalSince1970:[[content objectForKey:@"time"]longLongValue]];
    
    
    [[cell timeLabel]setText:[formatter stringFromDate:notificationDate]];
    
    
    return cell;
}

-(RequestNotificationCell*)configureRequestCell:(NSIndexPath*)indexPath
{
    NSDictionary* content = [requestArray objectAtIndex:indexPath.row];
    
    NSLog(@"content %@",content);
    
    RequestNotificationCell *cell;
    
    NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RequestNotificationCell" owner:self options:nil];
    for (id currentObject in topLevelObjects)
    {
        if ([currentObject isKindOfClass:[UITableViewCell class]])
        {
            cell = (RequestNotificationCell *)currentObject;
            break;
        }
    }
    
//    if (![[content valueForKey:@"isRead"] isEqualToString:@"read"])
//    {
//        [cell.contentView setBackgroundColor:[UIColor lightGrayColor]];
//    }
    [[cell titleLabel]setText:[content objectForKey:@"senderFullName"]];
    [[cell loader]setAlpha:1.0];
    [[cell loader]startAnimating];
    
    [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[content objectForKey:@"senderProfilePicUrl"]] placeholderImage:[UIImage imageNamed:@"avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [cell.loader stopAnimating];
        [cell.loader setHidden:YES];
    }];
    
    [[cell imgFollow]setAlpha:0.0];
    [[cell imgFavorite]setAlpha:0.0];
    [[cell lblCount]setAlpha:0.0];
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, (cell.imgProfile.frame.size.width), (cell.imgProfile.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:kCornerRadius];
    [borderLayer setBorderWidth:kBoarderWidth];

    
    cell.accpetBtns.tag = indexPath.row;
    [cell.accpetBtns addTarget:self action:@selector(accteptButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.cancelBtns.tag = indexPath.row;
    [cell.cancelBtns addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
-(void)accteptButtonAction:(UIButton*)sender
{
    NSDictionary *dict = [requestArray objectAtIndex:sender.tag];
    NSLog(@"dict %@",dict);
    [self acceptFriendRequestForFan:dict];
}
-(void)cancelButtonAction:(UIButton*)sender
{
    NSDictionary *dict = [requestArray objectAtIndex:sender.tag];
    NSLog(@"dict %@",dict);
    [self declineFriendRequestForFan:dict];
}

- (void)declineFriendRequestForFan:(NSDictionary *)fan
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] declineFriendRequestsWithUserID:[[[CurrentUser getObject] getUser] userId] fanID:[[fan objectForKey:@"senderId"]intValue] and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        for(int i = 0 ; i < requestArray.count ; i++)
                        {
                            if([[[requestArray objectAtIndex:i] objectForKey:@"notificationId"] intValue] == [[fan objectForKey:@"notificationId"] intValue])
                            {
                                [requestArray removeObjectAtIndex:i];
                                break;
                            }
                        }

                        [self.tbl_main reloadData];
                        [self.tbl_main setNeedsDisplay];

                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}
- (void)acceptFriendRequestForFan:(NSDictionary *)fan
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[FansManager getInstance] changeFriendForUserID:[[[CurrentUser getObject] getUser] userId] fanID:[[fan objectForKey:@"senderId"]intValue] friend:YES and:^(id currentClient) {
                
                NSLog(@"%@",currentClient);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient) {
                        for(int i = 0 ; i < requestArray.count ; i++)
                        {
                            if([[[requestArray objectAtIndex:i] objectForKey:@"notificationId"] intValue] == [[fan objectForKey:@"notificationId"] intValue])
                            {
                                [requestArray removeObjectAtIndex:i];
                                break;
                            }
                        }
                        
                        [self.tbl_main reloadData];
                        [self.tbl_main setNeedsDisplay];
                    }
                });
            }onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}


-(LikeNotificationCell*)configureFollowCell:(NSDictionary*)content descriptionString:(NSString*)descriptionString
{
    LikeNotificationCell *cell;
    
    NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LikeNotificationCell" owner:self options:nil];
    for (id currentObject in topLevelObjects)
    {
        if ([currentObject isKindOfClass:[UITableViewCell class]])
        {
            cell = (LikeNotificationCell *)currentObject;
            break;
        }
    }
 
    [[cell descriptionLabel]setText:[NSString stringWithFormat:@"%@ %@",[content objectForKey:@"senderFullName"],descriptionString]];
    
    if ([[content valueForKey:@"isRead"] isEqualToString:@"unread"])
    {
        [cell.contentView setBackgroundColor:[UIColor lightGrayColor]];
        cell.timeLabel.textColor = [UIColor whiteColor];
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate* notificationDate = [NSDate dateWithTimeIntervalSince1970:[[content objectForKey:@"time"]longLongValue]];
    
    
    [[cell timeLabel]setText:[formatter stringFromDate:notificationDate]];
    
    return cell;
}

-(LikeNotificationCell*)configureFavCell:(SystemMessage*)message descriptionString:(NSString*)descriptionString{
    
    LikeNotificationCell *cell;
    
    NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LikeNotificationCell" owner:self options:nil];
    for (id currentObject in topLevelObjects)
    {
        if ([currentObject isKindOfClass:[UITableViewCell class]])
        {
            cell = (LikeNotificationCell *)currentObject;
            break;
        }
    }


    [[cell descriptionLabel]setText:[NSString stringWithFormat:@"%@ %@",[message message],@""]];
    if (![message seen])
    {
        [cell.contentView setBackgroundColor:[UIColor lightGrayColor]];
        cell.timeLabel.textColor = [UIColor whiteColor];
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate* notificationDate = [message date];
    
    [[cell timeLabel]setText:[formatter stringFromDate:notificationDate]];
    
    return cell;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. Dequeue the custom header cell
    NotificationHeaderCellTableViewCell* headerCell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    if(!headerCell)
    {
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationHeaderCellTableViewCell" owner:self options:nil];
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                headerCell = (NotificationHeaderCellTableViewCell *)currentObject;
                break;
            }
        }

    }
    
    // 2. Set the various properties
    if(selectedView == 1)
    {
        [[headerCell textLabel]setText:[[likeGroupArray objectAtIndex:section] objectForKey:@"groupTitle"]];
    }else if(selectedView == 2)
    {
        [[headerCell textLabel]setText:[[commentGroupArray objectAtIndex:section] objectForKey:@"groupTitle"]];
    }
    else if(selectedView == 5)
    {
        [[headerCell textLabel]setText:[[followGroupArray objectAtIndex:section] objectForKey:@"groupTitle"]];
    }
    else if(selectedView == 6)
    {
        [[headerCell textLabel] setText:@""];
    }
    else
    {
        [[headerCell textLabel]setText:@""];
    }
    [[headerCell textLabel] sizeToFit];
    
    return headerCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedView == 1 || selectedView == 2)
    {
        NSDictionary* clickedNotification;
        
        if(selectedView == 1)
        {
            clickedNotification = [[[likeGroupArray objectAtIndex:indexPath.section] objectForKey:@"groupMembers"] objectAtIndex:indexPath.row];
        }else
        {
            clickedNotification = [[[commentGroupArray objectAtIndex:indexPath.section] objectForKey:@"groupMembers"] objectAtIndex:indexPath.row];
        }
    
        NSLog(@"%@",[clickedNotification objectForKey:@"postId"]);
        
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
            @try {
                if ([RestServiceAgent internetAvailable]) {
                    [[WallManager getInstance]getPost:[[clickedNotification objectForKey:@"postId"] integerValue] userId:[[[CurrentUser getObject]getUser] userId] and:^(id currentClient) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                            if ([currentClient count]>0) {
                                NSLog(@"%@",currentClient);
                                if([currentClient count] >0)
                                {
                                    ImageDetailViewController * commentsView = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageDetailViewController"];
                                    commentsView.currentPost = (Post*)[currentClient objectAtIndex:0];
                                    commentsView.isWall = NO;
                                    [self.navigationController pushViewController:commentsView animated:YES];
                                }else
                                {
                                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:
                                                          @"Post not found"
                                                                                   delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                    [alert show];
                                }
                            }else{
                                
                            }
                        });
                    }onFailure:^(NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                            NSLog(@"%@",[error debugDescription]);
                        });
                    }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:
                                              [[Language sharedInstance] stringWithKey:@"connection_noConnection"]
                                                                       delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                        [alert show];
                    });
                    
                }
                
            }
            @catch (NSException *exception) {
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            }
    }else if(selectedView == 4)
    {
        NSDictionary* dict = [requestArray objectAtIndex:indexPath.row];
        UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
        fansDetailView.fanID = [[dict objectForKey:@"senderId"] intValue];
        [self.navigationController pushViewController:fansDetailView animated:YES];
    }
    else if(selectedView == 5)
    {
        NSDictionary* dict = [[[followGroupArray objectAtIndex:indexPath.section] objectForKey:@"groupMembers"] objectAtIndex:indexPath.row];
        
        
        UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
        FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
        fansDetailView.fanID = [[dict objectForKey:@"senderId"] intValue];
        [self.navigationController pushViewController:fansDetailView animated:YES];
    }
    else if(selectedView == 6)
    {
//        NSDictionary* dict = [[[favGroupArray objectAtIndex:indexPath.section] objectForKey:@"groupMembers"] objectAtIndex:indexPath.row];
//
//        int notiID = [[dict objectForKey:@"notificationType"] intValue];
//        if (notiID==4 || notiID==5 )
//        {
//            UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
//            FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
//            fansDetailView.fanID = [[dict objectForKey:@"senderId"] intValue];
//            [self.navigationController pushViewController:fansDetailView animated:YES];
//        }
//        else if (notiID==7)
//        {
//            UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
//            FansDetailView *fansDetailView = (FansDetailView *)[sb instantiateViewControllerWithIdentifier:@"FansDetailView"];
//            fansDetailView.fanID = [[dict objectForKey:@"senderId"] intValue];
//            [self.navigationController pushViewController:fansDetailView animated:YES];
//        }
//        else if (notiID==6)
//        {
//            [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
//            @try {
//                if ([RestServiceAgent internetAvailable]) {
//                    [[WallManager getInstance]getPost:[[dict objectForKey:@"postId"] integerValue] userId:[[[CurrentUser getObject]getUser] userId] and:^(id currentClient) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [SVProgressHUD dismiss];
//                            if ([currentClient count]>0) {
//                                NSLog(@"%@",currentClient);
//                                NSArray* postsArray = [currentClient objectForKey:@"posts"];
//                                if(postsArray.count >0)
//                                {
//                                    ImageDetailViewController * commentsView = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageDetailViewController"];
//                                    commentsView.currentPost = (Post*)[postsArray objectAtIndex:0];
//                                    commentsView.isWall = NO;
//                                    [self.navigationController pushViewController:commentsView animated:YES];
//                                }else
//                                {
//                                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:
//                                                          @"Post not found"
//                                                                                   delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                                    [alert show];
//                                }
//                            }else{
//                                
//                            }
//                        });
//                    }onFailure:^(NSError *error) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [SVProgressHUD dismiss];
//                            NSLog(@"%@",[error debugDescription]);
//                        });
//                    }];
//                }
//                else{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [SVProgressHUD dismiss];
//                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:
//                                              [[Language sharedInstance] stringWithKey:@"connection_noConnection"]
//                                                                       delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                        [alert show];
//                    });
//                    
//                }
//                
//            }
//            @catch (NSException *exception) {
//                [SVProgressHUD dismiss];
//                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
//                [alert show];
//            }
//        }
        
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(connection == getNotificationConnection)
    {
        [notificatoinData appendData:data];
    }
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(connection == getNotificationConnection)
    {
        [SVProgressHUD dismiss];
        NSError* error2;
        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:notificatoinData options:NSJSONReadingAllowFragments error:&error2];
    
        if([[dict objectForKey:@"status"] intValue] == 2)
        {
            if([[dict objectForKey:@"data"]count] == 0)
            {
                if (mainNotificationArray.count ==0) {
                    self.tbl_main.hidden = YES;
                    self.view_Initial.hidden = NO;
                }
            }else
            {
                //1424950370
                self.tbl_main.hidden = NO;
                self.view_Initial.hidden = YES;
                [mainNotificationArray removeAllObjects];
                mainNotificationArray = [NSMutableArray arrayWithArray:[dict objectForKey:@"data"]];
                [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] setObject:mainNotificationArray forKey:@"Notifications"];
                [[[NSUserDefaults alloc] initWithSuiteName:@"FCB"]synchronize];
                NSString * timestamp = [NSString stringWithFormat:@"%@",[[mainNotificationArray objectAtIndex:mainNotificationArray.count -1] valueForKey:@"notificationId"]];
                NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
                [defaults setObject:timestamp forKey:@"notificationId"];
                [self populateData];
                [self clearNotification];
            }
        }else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"error"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
            pageNo--;

        }
        
    }
}
-(void)clearNotification{
    int notificationId = [[[[NSUserDefaults alloc] initWithSuiteName:@"FCB"] objectForKey:@"notificationId"] intValue];
    [[UsersManager getInstance] clearNotification:[[CurrentUser getObject] getUser].userId notificationId:notificationId and:^(id currentClient) {
    } onFailure:^(NSError *error) {}];
    
}

-(void)populateCacheData
{
    
    [likeArray removeAllObjects];
    [likeGroupArray removeAllObjects];
    [commentGroupArray removeAllObjects];
    [commentArray removeAllObjects];
    [chatArray removeAllObjects];
    [requestArray removeAllObjects];
    [followArray removeAllObjects];
    [followGroupArray removeAllObjects];
    [favGroupArray removeAllObjects];
    [favArray removeAllObjects];

    
    [likeArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"likeNotification"]];
    [commentArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"commentNotification"]];
    [favArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"favNotification"]];
    [followArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"followNotification"]];
    [chatArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"chatNotification"]];
    [requestArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"requestNotification"]];
    
    // SORT ARRAYS
    likeArray = [[NSMutableArray alloc]initWithArray:[likeArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    commentArray = [[NSMutableArray alloc]initWithArray:[commentArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    requestArray = [[NSMutableArray alloc]initWithArray:[requestArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    followArray = [[NSMutableArray alloc]initWithArray:[followArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    favArray= [[NSMutableArray alloc]initWithArray:[favArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        return [bDate compare:aDate];
    }]];
    
    
    
    
    // Group Them Based On Day
    if(likeArray.count > 0)
    {
        likeGroupArray = [self getGroupedBasedOnDate:likeArray];
    }else
    {
        likeGroupArray = [[NSMutableArray alloc]init];
    }
    
    if(commentArray.count > 0)
    {
        commentGroupArray = [self getGroupedBasedOnDate:commentArray];
    }else
    {
        commentGroupArray = [[NSMutableArray alloc]init];
    }
    
    if(followArray.count > 0)
    {
        followGroupArray = [self getGroupedBasedOnDate:followArray];
    }else
    {
        followGroupArray = [[NSMutableArray alloc]init];
    }
    
    if(favArray.count > 0)
    {
        favGroupArray = [self getGroupedBasedOnDate:favArray];
    }else
    {
        favGroupArray = [[NSMutableArray alloc]init];
    }
    [self populateNotificationOverButton];
    
    
    [self.tbl_main reloadData];
    [self.tbl_main setNeedsDisplay];
    
}


-(void)populateData
{

    
    [likeArray removeAllObjects];
    [likeGroupArray removeAllObjects];
    [commentGroupArray removeAllObjects];
    [commentArray removeAllObjects];
    [chatArray removeAllObjects];
    [requestArray removeAllObjects];
    [followArray removeAllObjects];
    [followGroupArray removeAllObjects];
    [favGroupArray removeAllObjects];
    [favArray removeAllObjects];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    for (int i=0; i < [mainNotificationArray count]; i++)
    {
        
        NSMutableDictionary *dict  = [[mainNotificationArray objectAtIndex:i] mutableCopy];
        
        NSDate* notificationDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[dict objectForKey:@"time"]]longLongValue]];
        
        NSString* occurenceDate = [formatter stringFromDate:notificationDate];
        [dict setObject:occurenceDate forKey:@"occurenceDateString"];
        
        int type = [dict[@"notificationType"] intValue];
        switch (type)
        {
            case 1:
                [likeArray addObject:dict];
                break;
                
            case 2:
                [commentArray addObject:dict];
                break;
                
            case 3:
                [requestArray addObject:dict];
                break;
                
            case 4:
                if ([dict[@"receiverId"] intValue]==[[CurrentUser getObject] getUser].userId)
                {
                    [followArray addObject:dict];
                }
                else
                {
                    [favArray addObject:dict];
                }
                break;
                
            case 5:
                [favArray addObject:dict];
                break;
                
            case 6:
                [favArray addObject:dict];
                break;
                
            case 7:
                [favArray addObject:dict];
                break;
                
            default:
                break;
        }
    }
    
    likeArray       = [self addUnReadValueToArray:likeArray];
    followArray     = [self addUnReadValueToArray:followArray];
    favArray        = [self addUnReadValueToArray:favArray];
    requestArray    = [self addUnReadValueToArray:requestArray];
    commentArray    = [self addUnReadValueToArray:commentArray];
    chatArray       = [self addUnReadValueToArray:chatArray];
    
    [likeArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"likeNotification"]];
    [commentArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"commentNotification"]];
    [favArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"favNotification"]];
    [followArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"followNotification"]];
    [chatArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"chatNotification"]];
    [requestArray addObjectsFromArray:(NSMutableArray*)[GetAndPushCashedData getObjectFromCash:@"requestNotification"]];
    

    
    // SORT ARRAYS
    likeArray = [[NSMutableArray alloc]initWithArray:[likeArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    commentArray = [[NSMutableArray alloc]initWithArray:[commentArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    requestArray = [[NSMutableArray alloc]initWithArray:[requestArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    followArray = [[NSMutableArray alloc]initWithArray:[followArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    favArray= [[NSMutableArray alloc]initWithArray:[favArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary* a, NSDictionary* b) {
        
        NSDate* aDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[a objectForKey:@"time"]]longLongValue]];
        
        NSDate *bDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithFormat:@"%@",[b objectForKey:@"time"]]longLongValue]];
        
        return [bDate compare:aDate];
    }]];
    
    
    
    
    // Group Them Based On Day
    if(likeArray.count > 0)
    {
        likeGroupArray = [self getGroupedBasedOnDate:likeArray];
    }else
    {
        likeGroupArray = [[NSMutableArray alloc]init];
    }
    
    if(commentArray.count > 0)
    {
        commentGroupArray = [self getGroupedBasedOnDate:commentArray];
    }else
    {
        commentGroupArray = [[NSMutableArray alloc]init];
    }
    
    if(followArray.count > 0)
    {
        followGroupArray = [self getGroupedBasedOnDate:followArray];
    }else
    {
        followGroupArray = [[NSMutableArray alloc]init];
    }
    
    if(favArray.count > 0)
    {
        favGroupArray = [self getGroupedBasedOnDate:favArray];
    }else
    {
        favGroupArray = [[NSMutableArray alloc]init];
    }
    [self populateNotificationOverButton];
    
    
    [self.tbl_main reloadData];
    [self.tbl_main setNeedsDisplay];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [GetAndPushCashedData cashObject:likeArray withAction:@"likeNotification"];
    [GetAndPushCashedData cashObject:followArray withAction:@"followNotification"];
    [GetAndPushCashedData cashObject:favArray withAction:@"favNotification"];
    [GetAndPushCashedData cashObject:commentArray withAction:@"commentNotification"];
    [GetAndPushCashedData cashObject:requestArray withAction:@"requestNotification"];
    [GetAndPushCashedData cashObject:chatArray withAction:@"chatNotification"];
}

-(NSMutableArray *)addUnReadValueToArray:(NSMutableArray*)array
{
    for (int i=0; i < [array count]; i++)
    {
        [[array objectAtIndex:i] setValue:@"unread" forKey:@"isRead"];
//        NSMutableDictionary *temp1 =[array objectAtIndex:i];
//        NSMutableDictionary *temp =[array objectAtIndex:i];
//        [temp setValue:@"unread" forKey:@"isRead"];
//        [array removeObject:temp1];
//        [array addObject:temp];
        
    }
    return array;
}

-(NSMutableArray *)setReadValueToArray:(NSMutableArray*)array
{
    for (int i=0; i < [array count]; i++)
    {
        [[array objectAtIndex:i] setValue:@"read" forKey:@"isRead"];

//        NSMutableDictionary *temp1 =[array objectAtIndex:i];
//
//        NSMutableDictionary *temp =[array objectAtIndex:i];
//        [temp setValue:@"read" forKey:@"isRead"];
//        [array removeObject:temp1];
//        [array addObject:temp];
    }
    return array;
}

-(int)getCountOfUnReadValueOfArray:(NSMutableArray*)array
{
    int count = 0;
    for (int i=0; i < [array count]; i++)
    {
        NSMutableDictionary *temp =[array objectAtIndex:i];
        NSString *isRead = [temp valueForKey:@"isRead"];
        if ([isRead isEqualToString:@"unread"])
        {
            count ++;
        }
    }
    return count;
}
-(void)populateNotificationOverButton
{
    self.lbl_Chat.text = [NSString stringWithFormat:@"%lu",(unsigned long)[self getCountOfUnReadValueOfArray:chatArray]];
    self.lbl_Comment.text = [NSString stringWithFormat:@"%lu",(unsigned long)[self getCountOfUnReadValueOfArray:commentArray]];
    self.lbl_Fav.text = [NSString stringWithFormat:@"%lu",(unsigned long)[SystemMessage unseenMessagesNumber]];
    self.lbl_Follow.text = [NSString stringWithFormat:@"%lu",(unsigned long)[self getCountOfUnReadValueOfArray:followArray]];
    self.lbl_Like.text = [NSString stringWithFormat:@"%lu",(unsigned long)[self getCountOfUnReadValueOfArray:likeArray]];
    self.lbl_Request.text = [NSString stringWithFormat:@"%lu",(unsigned long)[self getCountOfUnReadValueOfArray:requestArray]];
    
    if ([self.lbl_Chat.text isEqualToString:@"0"])
    {
        self.lbl_Chat.hidden=YES;
    }
    else{
        self.lbl_Chat.hidden=NO;
    }
    
    if ([self.lbl_Comment.text isEqualToString:@"0"])
    {
        self.lbl_Comment.hidden=YES;
    }
    else{
        self.lbl_Comment.hidden=NO;
    }
    
    if ([self.lbl_Fav.text isEqualToString:@"0"])
    {
        self.lbl_Fav.hidden=YES;
    }
    else{
        self.lbl_Fav.hidden=NO;
    }
    
    if ([self.lbl_Follow.text isEqualToString:@"0"])
    {
        self.lbl_Follow.hidden=YES;
    }
    else{
        self.lbl_Follow.hidden=NO;
    }
    
    if ([self.lbl_Like.text isEqualToString:@"0"])
    {
        self.lbl_Like.hidden=YES;
    }
    else{
        self.lbl_Like.hidden=NO;
    }
    
    if ([self.lbl_Request.text isEqualToString:@"0"])
    {
        self.lbl_Request.hidden=YES;
    }
    else{
        self.lbl_Request.hidden=NO;
    }

}
-(void)viewWillAppear:(BOOL)animated{

    if ([GetAndPushCashedData getObjectFromCash:@"setSelectedTap"]) {
        [self setSelectedTap:[[GetAndPushCashedData getObjectFromCash:@"setSelectedTap"] intValue]];
        [GetAndPushCashedData removeObjectWithAction:@"setSelectedTap"];
    }
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"FCB"];
    [defaults setObject:@(0) forKey:@"NotificationsNumber"];
    [defaults synchronize];
}
@end
