//
//  NotificationHeaderCellTableViewCell.m
//  FC Barcelona
//
//  Created by Osama Rabie on 2/26/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "NotificationHeaderCellTableViewCell.h"

@implementation NotificationHeaderCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
