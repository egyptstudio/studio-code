//
//  ChatMainViewController.m
//  FC Barcelona
//
//  Created by Saad Ansari on 12/30/14.
//  Copyright (c) 2014 Tawasol. All rights reserved.
//

#import "ChatMainViewController.h"
#import "RecentCell.h"
#import "FriendsCell.h"
#import "ContactsCell.h"
#import "RestServiceAgent.h"
#import "WallManager.h"
#import "SVProgressHUD.h"
#import "CurrentUser.h"
#import "Base.h"
#import "UsersManager.h"
#import "WebImage.h"
#import "FMDBHelper.h"
#import <AddressBook/AddressBook.h>
#import "CJSONSerializer.h"
#import "InviteFriendsView.h"
#import "FansManager.h"
#import "NBPhoneNumberUtil.h"
#import "GetAndPushCashedData.h"
#import "MessagingViewController.h"
#import "Fan.h"
#import "Message.h"
#import "Contact.h"
#import "BarcelonaAppDelegate.h"
#import <GoogleMobileAds/DFPBannerView.h>
#import <GoogleMobileAds/DFPRequest.h>
@interface ChatMainViewController ()
{
    NSMutableArray *friendsArray;
    NSMutableArray *contactArray;
    NSMutableArray *saveFriendArray;
    NSMutableArray *saveContactArray;
    
    NSMutableDictionary * recentChatDic;
    NSMutableArray * filteredMsgs;
    NSComparator sortingComparator;
    
    BOOL isMessageLoaded;
    BOOL isSearch;
    
}
@property (nonatomic,weak)IBOutlet UITableView *recentTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segments;
@property (weak, nonatomic) IBOutlet UICollectionView *friendsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *contactsCollectionView;
@property (weak, nonatomic) IBOutlet UIView *recentView;
@property (weak, nonatomic) IBOutlet UIView *friendsView;
@property (weak, nonatomic) IBOutlet UIView *contactsView;
@property (weak, nonatomic) IBOutlet UILabel *lblInitRecent;
@property (weak, nonatomic) IBOutlet UILabel *lblInitFriend;
@property (weak, nonatomic) IBOutlet UILabel *lblInitContact;
@property (weak, nonatomic) IBOutlet UIButton *btnInitInvite;
@property (weak, nonatomic) IBOutlet UIScrollView *segmentsView;


@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property IBOutlet UISearchBar *theSearchBar;

@end

@implementation ChatMainViewController
#define isPad     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

-(IBAction)inviteActive:(id)sender {
    UIStoryboard *sb =  (isPad) ? [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]] : [UIStoryboard storyboardWithName:@"Fans_iPhone" bundle:[NSBundle mainBundle]];
    InviteFriendsView* invite = [sb instantiateViewControllerWithIdentifier:@"InviteFriendsView"];
    [self.navigationController pushViewController:invite animated:YES];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)openStudio:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openStudio" object:nil];
    
}

- (IBAction)shop_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openShop" object:nil];
    
}
- (IBAction)chat_Action:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openChat" object:nil];
    
}

- (IBAction)searchAction:(id)sender
{
    if (isSearch)
    {
        isSearch = NO;
        self.searchView.hidden=YES;
    }
    else
    {
        isSearch = YES;
        self.searchView.hidden=NO;
    }
    [self.theSearchBar becomeFirstResponder];
}



- (void) viewDidLoad {
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessage) name:@"NewMessageNotification" object:nil];

    UIRefreshControl * refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [_recentTableView addSubview:refreshControl];
    //    [_searchButton setHidden:YES];
    
    //////////////////////
    [_segmentsView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:84/255.0f blue:164/255.0f alpha:1]];
    [_segments setFrame:_segmentsView.bounds];
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    UIFont *fontBold = [UIFont boldSystemFontOfSize:14.0f];
    
    UIColor *color = [UIColor whiteColor];
    UIColor *colorDisabled = [UIColor darkGrayColor];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesBold = [NSDictionary dictionaryWithObjectsAndKeys:
                                    fontBold,NSFontAttributeName,color,NSForegroundColorAttributeName,nil];
    
    NSDictionary *attributesDisabled = [NSDictionary dictionaryWithObjectsAndKeys:
                                        font,NSFontAttributeName,colorDisabled,NSForegroundColorAttributeName,nil];
    
    [_segments setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [_segments setTitleTextAttributes:attributesBold forState:UIControlStateSelected];
    [_segments setTitleTextAttributes:attributesDisabled forState:UIControlStateDisabled];
    
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_normal.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setBackgroundImage:[UIImage imageNamed:@"tab_selected.png"]
                         forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    UIView *devider = [[UIView alloc] initWithFrame:CGRectMake(0, 0, .5, _segments.frame.size.height)];
    [devider setBackgroundColor:[UIColor whiteColor]];
    UIGraphicsBeginImageContextWithOptions(devider.bounds.size, devider.opaque, 0.0);
    [devider.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * deviderImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateSelected
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segments setDividerImage:deviderImg forLeftSegmentState:UIControlStateNormal
             rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [_segments addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
    //////////////////
    
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Messages_Recent"] forSegmentAtIndex:0];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"Messages_Friends"]  forSegmentAtIndex:1];
    [_segments setTitle:[[Language sharedInstance] stringWithKey:@"contacts"]    forSegmentAtIndex:2];
    [_segments setSelectedSegmentIndex:0];
    
    [_btnInitInvite setTitle:[[Language sharedInstance] stringWithKey:@"initialMessages_Invite"] forState:UIControlStateNormal];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_shopButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Shop"] forState:UIControlStateNormal];
    
    [_lblInitRecent setText:[[Language sharedInstance] stringWithKey:@"initialMessages_NoRecentMessage"]];
    [_lblInitFriend setText:[[Language sharedInstance] stringWithKey:@"initialFriends_noFriendsYet"]];
    [_lblInitContact setText:[[Language sharedInstance] stringWithKey:@"no_contacts"]];
    
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_chatButton setTitle:[[Language sharedInstance] stringWithKey:@"Home_Message"] forState:UIControlStateNormal];
    [_pageTitle  setText:[[Language sharedInstance] stringWithKey:@"notifications_messages"]];
    [_comingSoon setText:[[Language sharedInstance] stringWithKey:@"coming_soon"]];
    
    isSearch = NO;
    saveFriendArray= [NSMutableArray new];
    saveContactArray= [NSMutableArray new];
    contactArray = [NSMutableArray new];
    
    _recentView.hidden=NO;
    _friendsView.hidden=YES;
    _contactsView.hidden=YES;
    _segments.selectedSegmentIndex = 0;
    
    
    [self loadContactWebService];
    
}
-(void)newMessage{
    if ([self.navigationController.topViewController isKindOfClass:[self class]]) {
        [self chatLoadMessages];
    }
}
- (void) dismissKeyboard {
    //[self.theSearchBar resignFirstResponder];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.screenName = @"Chat Screen";
    if (isSearch)
    {
        isSearch = NO;
        self.searchView.hidden=YES;
    }
    
    [self sortUsingComparator];
    
    isMessageLoaded = NO;
    if ([GetAndPushCashedData getObjectFromCash:@"recentMsgs"]) {
        _lblInitRecent.hidden = YES;
        _recentTableView.hidden = NO;
        recentChatDic = [GetAndPushCashedData getObjectFromCash:@"recentMsgs"];
        isMessageLoaded = YES;
        [_recentTableView reloadData];
    } else {
        recentChatDic = [[NSMutableDictionary alloc] init];
    }
    
    NSArray *tempFrnds = [[FMDBHelper new] getAllFriends];
    if (tempFrnds.count >0)
    {
        _lblInitFriend.hidden = YES;
        _btnInitInvite.hidden = YES;
        _friendsCollectionView.hidden = NO;
        friendsArray = [[NSMutableArray alloc]initWithArray:tempFrnds];
    }
    else
    {
        friendsArray = [NSMutableArray new];
    }
    
    if ([[CurrentUser getObject] isUser])
    {
        
        if ([RestServiceAgent internetAvailable])
        {
            [self chatLoadFriends];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
    
    [self addAdBanner];

}
-(void)addAdBanner{
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
    bannerView.rootViewController = self;
    bannerView.adUnitID = @"ca-app-pub-2924334778141972/3060492441";
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[@"abc7f676a40f3514a5a8c71ad28dfd27"];
    [bannerView loadRequest:request];
    
    [_adBanner addSubview:bannerView];
}



-(void)loadContactWebService
{
    if ([[CurrentUser getObject] isUser])
    {
        
        if ([RestServiceAgent internetAvailable])
        {
            [self chatLoadContacts];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
            [alert show];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
        
        //        UIAlertView* error = [[UIAlertView alloc] initWithTitle:nil message:@"Please login to use this feature" delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        //        [error show];
    }
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(BOOL)shouldAutorotate{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)chatLoadFriends
{
    FMDBHelper *helper = [FMDBHelper new];
    
    if (!isMessageLoaded) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
    }
    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance] getChatFriends:@"city" userId:[[CurrentUser getObject] getUser].userId limit:0 and:^(id currentClient){
                
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   
                                   if (currentClient>0)
                                   {
                                       
                                       NSMutableDictionary *dict =[currentClient objectForKey:@"data"];
                                       NSMutableArray *array = [dict objectForKey:@"data"];
                                       NSLog(@"chat/loadFriends %@",currentClient);
                                       if (array.count > 0)
                                       {
                                           _lblInitFriend.hidden = YES;
                                           _btnInitInvite.hidden = YES;
                                           _friendsCollectionView.hidden = NO;
                                           
                                           for (int i = 0 ; i<array.count; i++)
                                           {
                                               NSLog(@"[array objectAtIndex:i] %@",[array objectAtIndex:i]);
                                               BOOL isSuccess = [helper insertFriends:[array objectAtIndex:i]];
                                               if (isSuccess)
                                               {
                                                   //NSLog(@"Insert Successfully");
                                               }
                                               else
                                               {
                                                   //NSLog(@"Insert NOT Successfully");
                                               }
                                               
                                               NSMutableDictionary *dict = [array objectAtIndex:i];
                                               NSString *fanID = [NSString stringWithFormat:@"%d",[dict[@"friendID"] intValue]];
                                               NSString *fanName = [NSString stringWithFormat:@"%@",dict[@"friendName"]];
                                               NSString *fanPicture = [NSString stringWithFormat:@"%@",dict[@"friendPicture"]];
                                               
                                               NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithObjectsAndKeys:fanID,@"fanID",fanName,@"fanName",fanPicture,@"fanPicture", nil];
                                               
                                               BOOL isSuccess1 = [helper insertFan:temp];
                                               if (isSuccess1)
                                               {
                                                   //NSLog(@"Insert Successfully");
                                               }
                                               else
                                               {
                                                   //NSLog(@"Insert NOT Successfully");
                                               }
                                               
                                               
                                           }
                                           
                                       }
                                       //NSLog(@"Array Currinr %@",[currentClient objectForKey:@"data"]);
                                       friendsArray = [helper getAllFriends];
                                       [saveFriendArray addObjectsFromArray:friendsArray];
                                       [_friendsCollectionView reloadData];
                                       [self chatLoadMessages];
                                       //[SVProgressHUD dismiss];
                                   }
                               });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
}

-(void)chatLoadContacts
{
    FMDBHelper *helper = [FMDBHelper new];
    
    if (!isMessageLoaded)
    {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
    }
    @try {
        if ([RestServiceAgent internetAvailable])
        {
            [[WallManager getInstance] getChatContacts:[[CurrentUser getObject] getUser].userId Contacts:[self LoadDeviceContacts] Language:[[Language sharedInstance] langAbbreviation] and:^(id currentClient){
                
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   
                                   if (currentClient>0)
                                   {
                                       NSMutableArray *array = [currentClient objectForKey:@"data"];
                                       contactArray = [NSMutableArray arrayWithArray:array];
                                       saveContactArray = [NSMutableArray arrayWithArray:array];
                                       
                                       if (array.count > 0)
                                       {
                                           for (int i = 0 ; i<array.count; i++)
                                           {
                                               
                                               BOOL isSuccess = [helper insertFan:[array objectAtIndex:i]];
                                               if (isSuccess)
                                               {
                                                   //NSLog(@"Insert Successfully");
                                               }
                                               else
                                               {
                                                   //NSLog(@"Insert NOT Successfully");
                                               }
                                           }
                                           
                                       }
                                       
                                       //NSLog(@"Friends %@",array);
                                       if (array.count > 0)
                                       {
                                           _lblInitContact.hidden = YES;
                                           _btnInitInvite.hidden = YES;
                                           _contactsCollectionView.hidden = NO;
                                       }
                                       
                                       [_contactsCollectionView reloadData];
                                       [SVProgressHUD dismiss];
                                   }
                               });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}

-(NSMutableArray *)LoadDeviceContacts
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    __block BOOL accessGranted = NO;
    
    if (ABAddressBookRequestAccessWithCompletion != NULL){
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    else{
        accessGranted = YES;
    }
    if (accessGranted)
    {
        NSArray *allPeople = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
        NSInteger numberOfPeople = [allPeople count];
        
        for (NSInteger i = 0; i < numberOfPeople; i++) {
            ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
            
            NSString *firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
            NSString *lastName  = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
            NSString *email,*phoneNumber;
            if (lastName.length==0) {
                lastName = @"";
            }
            if (firstName.length==0) {
                firstName = @"";
            }
            NSString *fullName = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
            
            ABMultiValueRef emailNumbers = ABRecordCopyValue(person, kABPersonEmailProperty);
            CFIndex numberOfEmailNumbers = ABMultiValueGetCount(emailNumbers);
            for (CFIndex i = 0; i < numberOfEmailNumbers; i++) {
                email = CFBridgingRelease(ABMultiValueCopyValueAtIndex(emailNumbers, i));
            }
            
            ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
            CFIndex numberOfPhoneNumbers = ABMultiValueGetCount(phoneNumbers);
            for (CFIndex i = 0; i < numberOfPhoneNumbers; i++) {
                phoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phoneNumbers, i));
            }
            
            if (phoneNumber.length>0)
            {
                if (email.length==0) {
                    email = @"";
                }
                
                NSString* phones = @"";
                
                //                NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil sharedInstance];
                //
                //
                //                for (NSString *countryCode in [NSLocale ISOCountryCodes])
                //                {
                //                    NSError *error = nil;
                //                    NBPhoneNumber *phoneNumberUS = [phoneUtil parse:phoneNumber defaultRegion:countryCode error:&error];
                //                    if (error) {
                //                        //NSLog(@"err [%@]", [error localizedDescription]);
                //                    }
                //
                //                    if([phoneUtil isValidNumber:phoneNumberUS] && [phoneUtil getRegionCodeForNumber:phoneNumberUS])
                //
                //                    {
                //                        if([phoneUtil getNumberType:phoneNumberUS] == NBEPhoneNumberTypeMOBILE)
                //                        {
                //                            phones = [phones stringByAppendingFormat:@"%@,",[phoneUtil getNationalSignificantNumber:phoneNumberUS]];
                //                            // NSLog(@"%@%@",[phoneUtil getCountryCodeForRegion:countryCode],[phoneUtil getNationalSignificantNumber:phoneNumberUS]);
                //                        }
                //                    }
                //
                //                }
                
                if(phones.length>0)
                {
                    
                    phones = [phones substringToIndex:phones.length-1];
                    NSSet * uniqueNumbers = [[NSSet alloc]initWithArray:[phones componentsSeparatedByString:@","]];
                    phones = [[uniqueNumbers allObjects] componentsJoinedByString:@","];
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%@",fullName],@"name",[NSString stringWithFormat:@"%@",phones],@"phones",[NSString stringWithFormat:@"%@",email],@"emails", nil];
                    [result addObject:dict];
                }
                
            }
            CFRelease(phoneNumbers);
            
        }
    }
    //sort array
    NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"name"ascending:YES]; // 1
    NSMutableArray * sortedArray = [[result sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]] mutableCopy];
    NSError *error1;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sortedArray options:0 error:&error1];
    NSString *columnsString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //    columnsString = [columnsString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    //    columnsString = [columnsString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    //    columnsString = [columnsString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    //    columnsString = [columnsString stringByReplacingOccurrencesOfString:@"[" withString:@"("];
    //    columnsString = [columnsString stringByReplacingOccurrencesOfString:@"]" withString:@")"];
    
    
    return sortedArray ;
}

- (IBAction)segmentChanged:(id)sender
{
    switch (_segments.selectedSegmentIndex) {
        case 0:
            //            [self.searchButton setHidden:YES];
            _recentView.hidden=NO;
            _friendsView.hidden=YES;
            _adBanner.hidden=YES;
            _contactsView.hidden=YES;
            [_recentTableView reloadData];
            
            break;
        case 1:
            //            [self.searchButton setHidden:NO];
            _recentView.hidden=YES;
            _friendsView.hidden=NO;
            _adBanner.hidden=NO;
            _contactsView.hidden=YES;
            [_friendsCollectionView reloadData];
            break;
        case 2:
            //            [self.searchButton setHidden:NO];
            _recentView.hidden=YES;
            _friendsView.hidden=YES;
            _adBanner.hidden=YES;
            _contactsView.hidden=NO;
            [_contactsCollectionView reloadData];
            
            break;
        default:
            break;
    }
    
    _theSearchBar.text = @"";
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _friendsCollectionView)
    {
        return [friendsArray count];
    }
    else
    {
        return [contactArray count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _friendsCollectionView)
    {
        FriendsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"friendsCell" forIndexPath:indexPath];
        CGFloat borderWidth = 0.5f;
        
        cell.imgProfile.layer.borderColor = [UIColor yellowColor].CGColor;
        cell.imgProfile.layer.borderWidth = borderWidth;
        cell.imgProfile.layer.cornerRadius = 5.0;
        cell.imgProfile.layer.masksToBounds = YES;
        
        NSDictionary *dit = [friendsArray objectAtIndex:indexPath.row];
        
        cell.lblTitle.text = [dit valueForKey:@"userName"];
        
        //Count
        cell.lblCount.text= [dit valueForKey:@"posts"];
        
        //Favorite
        NSString *favBool = [dit valueForKey:@"isFavorite"];
        if ([favBool isEqualToString:@"true"])
        {
            cell.imgFavorite.image = [UIImage imageNamed:@"favourate.png"];
        }
        else
        {
            cell.imgFavorite.image = [UIImage imageNamed:@"unfavourate.png"];
        }
        
        //Status
        NSString *statusBool = [dit valueForKey:@"online"];
        if ([statusBool isEqualToString:@"true"])
        {
            cell.imgStatus.hidden = NO;
            cell.imgStatus.image = [UIImage imageNamed:@"online.png"];
        }
        else
        {
            cell.imgStatus.hidden = YES;
        }
        
        //Follow
        NSString *followBool = [dit objectForKey:@"isFollowing"];
        if ([followBool isEqualToString:@"true"])
        {
            cell.imgFollow.hidden = NO;
            cell.imgFollow.image = [UIImage imageNamed:@"followed.png"];
        }
        else
        {
            cell.imgFollow.hidden = YES;
        }
        NSString * picURL = [dit valueForKey:@"profilePic"];
        //Profile
        if (picURL)
        {
            [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:picURL] placeholderImage:[UIImage imageNamed:@"avatar"]];
        }
        else
        {
            [cell.imgProfile setImage:[UIImage imageNamed:@"avatar.png"]];
        }
        
        return cell;
    }
    else
    {
        ContactsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"contactsCell" forIndexPath:indexPath];
        CGFloat borderWidth = 0.5f;
        cell.imgProfile.layer.borderColor = [UIColor yellowColor].CGColor;
        cell.imgProfile.layer.borderWidth = borderWidth;
        cell.imgProfile.layer.cornerRadius = 5.0;
        cell.imgProfile.layer.masksToBounds = YES;
        
        NSDictionary *dit = [contactArray objectAtIndex:indexPath.row];
        
        //Name
        cell.lblTitle.text = [dit valueForKey:@"fanName"];
        
        //Count
        cell.lblCount.text= [NSString stringWithFormat:@"%d",[[dit valueForKey:@"fanPicNumber"] intValue]]; ;
        
        //Favorite
        int favBool = [[dit valueForKey:@"isFavorite"] intValue];
        if (favBool ==1)
        {
            cell.imgFavorite.image = [UIImage imageNamed:@"favourate.png"];
        }
        else
        {
            cell.imgFavorite.image = [UIImage imageNamed:@"unfavourate.png"];
        }
        
        //Status
        int statusBool = [[dit valueForKey:@"fanOnline"] intValue];
        if (statusBool == 1)
        {
            cell.imgStatus.hidden = NO;
            cell.imgStatus.image = [UIImage imageNamed:@"online.png"];
        }
        else
        {
            cell.imgStatus.hidden = YES;
        }
        
        //Follow
        int followBool = [[dit objectForKey:@"isFollowed"] intValue];
        if (followBool==1)
        {
            cell.imgFollow.hidden = NO;
            cell.imgFollow.image = [UIImage imageNamed:@"followed.png"];
        }
        else
        {
            cell.imgFollow.hidden = YES;
        }
        
        //Profile
        NSString * picURL = [dit valueForKey:@"fanPicture"];
        if (picURL)
        {
            [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:picURL] placeholderImage:[UIImage imageNamed:@"avatar"]];
        }
        else
        {
            [cell.imgProfile setImage:[UIImage imageNamed:@"avatar.png"]];
        }
        
        cell.btn_Add.tag = indexPath.row;
        [cell.btn_Add addTarget:self action:@selector(add_Friend_Action:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
    }
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    Fan *sender = [[Fan alloc] init];
    
    if (collectionView==_friendsCollectionView)
    {
        sender.userId = [[friendsArray objectAtIndex:indexPath.row][@"userId"] intValue] ;
        sender.fanName = [friendsArray objectAtIndex:indexPath.row][@"userName"];
        sender.fanPicture = [friendsArray objectAtIndex:indexPath.row][@"fanPicture"];
    }
    else
    {
        sender.userId = [[contactArray objectAtIndex:indexPath.row][@"fanID"] intValue] ;
        sender.fanName = [contactArray objectAtIndex:indexPath.row][@"fanName"];
        sender.fanPicture = [contactArray objectAtIndex:indexPath.row][@"fanPicture"];
    }
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openMessaging" object:sender userInfo:nil];
}

- (void)add_Friend_Action:(id)sender
{
    
    if ([[CurrentUser getObject] isUser])
    {
        UIButton *btn = ((UIButton *)sender);
        NSDictionary *dic = [contactArray objectAtIndex:btn.tag];
        [self changeFriend:YES andFanID:dic[@"fanID"]];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openLogin" object:nil];
    }
}

- (void)changeFriend:(BOOL)isFriend andFanID:(NSString*)fanID
{
    [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
    
    @try {
        if ([RestServiceAgent internetAvailable]) {
            
            [[FansManager getInstance] changeFriendForUserID:[[[CurrentUser getObject] getUser] userId] fanID:[fanID intValue] friend:isFriend and:^(id currentClient) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if (currentClient)
                    {
                        BOOL success;
                        if (currentClient[@"status"])
                        {
                            success = ([currentClient[@"status"] intValue] == 1) ? YES: NO;
                            if (success)
                            {
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Friend request has been sent." delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                                [alert show];
                            }
                        }
                    }
                });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
}




-(void)searchBarCancelButtonClicked:(UISearchBar *)aSearchBar
{
    isSearch = NO;
    self.searchView.hidden=YES;
    aSearchBar.text = @"";
    if (_segments.selectedSegmentIndex == 0)
    {
        [filteredMsgs removeAllObjects];
        [_recentTableView reloadData];
    }
    else if(_segments.selectedSegmentIndex == 1)
    {
        if (saveFriendArray.count>0)
        {
            [friendsArray removeAllObjects];
            [friendsArray addObjectsFromArray:saveFriendArray];
            [_friendsCollectionView reloadData];
        }
        else
        {
            [_friendsCollectionView reloadData];
            
        }
    }
    else if(_segments.selectedSegmentIndex == 2)
    {
        if (saveContactArray.count>0)
        {
            [contactArray removeAllObjects];
            [contactArray addObjectsFromArray:saveContactArray];
            [_contactsCollectionView reloadData];
        }
        else
        {
            [_contactsCollectionView reloadData];
        }
        
    }
    [aSearchBar resignFirstResponder];
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)aSearchBar
{
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)aSearchBar
{
    return YES;
}


-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    
    
    if (![searchText isEqualToString:@""]) {
        NSMutableArray * recentData  = [NSMutableArray array];
        for (NSString * key in [recentChatDic allKeys]) {
            [recentData addObject:[recentChatDic objectForKey:key]];
        }
        [filteredMsgs removeAllObjects];
        
        // Filter the array using NSPredicate
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.senderName contains[c] %@ ",searchText];
        filteredMsgs = [NSMutableArray arrayWithArray:[recentData filteredArrayUsingPredicate:predicate]];
        NSLog(@"%@", filteredMsgs);
        [self.recentTableView reloadData];
    }
    else
    {
        [filteredMsgs removeAllObjects];
        [self.recentTableView reloadData];
    }
    
}


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if (_segments.selectedSegmentIndex == 0)
    {
        searchBar.showsCancelButton=YES;
        [self filterContentForSearchText:text scope:0];
        
        //        }
        //        if (saveFriendArray.count>0)
        //        {
        //            [friendsArray removeAllObjects];
        //            [friendsArray addObjectsFromArray:saveFriendArray];
        //            [_friendsCollectionView reloadData];
        //        }
        //        if (saveContactArray.count>0)
        //        {
        //            [contactArray removeAllObjects];
        //            [contactArray addObjectsFromArray:saveContactArray];
        //            [_contactsCollectionView reloadData];
        //        }
        //
        //
        //        NSArray *tempMsg = [[FMDBHelper new] getAllMessageseHavingText:text];
        //        [recentMsgArray removeAllObjects];
        //        [recentMsgArray addObjectsFromArray:tempMsg];
        //        [_recentTableView reloadData];
    }
    else if(_segments.selectedSegmentIndex == 1)
    {
        [_recentTableView reloadData];
        if (saveContactArray.count>0)
        {
            [contactArray removeAllObjects];
            [contactArray addObjectsFromArray:saveContactArray];
            [_contactsCollectionView reloadData];
        }
        NSArray *tempMsg = [[FMDBHelper new] getAllFriendsHavingText:text];
        [friendsArray removeAllObjects];
        [friendsArray addObjectsFromArray:tempMsg];
        [_friendsCollectionView reloadData];
    }
    else if(_segments.selectedSegmentIndex == 2)
    {
        if (saveFriendArray.count>0)
        {
            [friendsArray removeAllObjects];
            [friendsArray addObjectsFromArray:saveFriendArray];
            [_friendsCollectionView reloadData];
        }
        [_recentTableView reloadData];
        
        
        NSArray * tempArray = [NSArray arrayWithArray:saveContactArray];
        NSArray *newArray = [tempArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(fanName beginswith[c] %@)", text]];
        [contactArray removeAllObjects];
        [contactArray addObjectsFromArray:newArray];
        [_contactsCollectionView reloadData];
    }
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
}


#pragma mark - Load Messages
-(void) chatLoadMessages {
    
    if (!isMessageLoaded) {
        [SVProgressHUD showWithStatus:[[Language sharedInstance]stringWithKey:@"chat_loading"] maskType:SVProgressHUDMaskTypeGradient];
        
    }    @try {
        if ([RestServiceAgent internetAvailable]) {
            [[WallManager getInstance] getLoadMessagesUserID:[[CurrentUser getObject] getUser].userId and:^(id currentClient){
                
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   if (currentClient)
                                   {
                                       NSArray * dummyArr = [currentClient objectForKey:@"data"];
                                       _lblInitRecent.hidden = YES;
                                       _recentTableView.hidden = NO;
                                       [self handleRecentChatDataWithArray:dummyArr];
                                   }
                                   [SVProgressHUD dismiss];
                               });
                
            } onFailure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:error.domain delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                    [alert show];
                });
            }];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:[[Language sharedInstance] stringWithKey:@"connection_noConnection"] delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
                [alert show];
            });
            
        }
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:exception.reason delegate:nil cancelButtonTitle:[[Language sharedInstance] stringWithKey:@"Close"] otherButtonTitles: nil];
        [alert show];
    }
    
}



-(void) handleRecentChatDataWithArray:(NSArray *) recentMsgs {
    
    NSMutableDictionary * userMsg;
    
    
    for (NSDictionary * dic in recentMsgs) {
        
        userMsg = [[NSMutableDictionary alloc] init];
        
        [userMsg setObject:[dic objectForKey:@"senderName"] forKey:@"senderName"];
        [userMsg setObject:[dic objectForKey:@"senderPicture"] forKey:@"senderPicture"];
        [userMsg setObject:[dic objectForKey:@"messageText"] forKey:@"messageText"];
        [userMsg setObject:[dic objectForKey:@"sentAt"] forKey:@"sentAt"];
        [userMsg setObject:[dic objectForKey:@"type"] forKey:@"type"];
        [userMsg setObject:[dic objectForKey:@"senderUserID"] forKey:@"senderUserID"];
        
        [recentChatDic setObject:userMsg forKey:[dic objectForKey:@"senderUserID"]];
        
        
        //////////////////// Caching Messages/User Into CoreData/////////////////////
        int messageId            = [dic[@"messageRemoteID"] intValue];
        NSString *messageText    = dic[@"messageText"];
        int senderId             = [dic[@"senderUserID"] intValue];
        NSString *senderName     = [dic[@"senderName"] isKindOfClass:[NSNull class]] ? @"Empty" : dic[@"senderName"]
        ;
        NSString *senderPic      = dic[@"senderPicture"];
        NSDate *sentAt           = [NSDate dateWithTimeIntervalSince1970:[dic[@"sentAt"] longLongValue]];
        Fan *sender = [[Fan alloc] init];
        sender.userId = senderId;
        sender.fanName = senderName;
        sender.fanPicture = senderPic;
        [Message messageWithId:messageId message:messageText from:sender to:[[CurrentUser getObject] getUser] atTime:sentAt seen:NO];
        [Contact chatWithUser:sender];
        ///////////////////////////////////////////////////////////////////////////////
        
        
    }
    if ([recentMsgs count] < 1 && [[recentChatDic allKeys] count] < 1) {
        [_lblInitRecent setHidden:NO];
    }
    [GetAndPushCashedData cashObject:recentChatDic withAction:@"recentMsgs"];
    [_recentTableView reloadData];
}



- (void) sortUsingComparator {
    sortingComparator = ^(NSDictionary * dic1, NSDictionary * dic2)
    {
        return (NSComparisonResult) [[dic2 objectForKey:@"sentAt"] compare:[dic1 objectForKey:@"sentAt"]];
    };
}


-(NSDictionary *) getUserMsgsByIndex:(NSIndexPath *)indexPath
{
    NSArray *values= [([filteredMsgs count]) ? filteredMsgs : [recentChatDic allValues] sortedArrayUsingComparator:sortingComparator];
    return [values objectAtIndex:indexPath.row];
}


#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ([filteredMsgs count]) ? [filteredMsgs count] : recentChatDic.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"recentCell";
    RecentCell *cell=[self.recentTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSDictionary *dict = [self getUserMsgsByIndex:indexPath];
    
    if (dict[@"senderPicture"])
    {
        [cell.cellLogo sd_setImageWithURL:[NSURL URLWithString:dict[@"senderPicture"]] placeholderImage:[UIImage imageNamed:@"avatar"]];

    }
    else
    {
        [cell.cellLogo sd_setImageWithURL:[NSURL URLWithString:dict[@"fanPicture"]] placeholderImage:[UIImage imageNamed:@"avatar"]];
    }
    
    NSDate* date = [NSDate dateWithTimeIntervalSince1970: [dict[@"sentAt"] longValue]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy HH:mm"];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *dateString = [formatter stringFromDate:date];
    
    if (dict[@"senderName"])
    {
        cell.lblTitle.text = ([dict[@"senderName"] isKindOfClass:[NSNull class]]) ? @"" : dict[@"senderName"];
    }
    else
    {
        cell.lblTitle.text = dict[@"fanName"];
    }
    cell.lblMessage.text = dict[@"messageText"];
    cell.lblDate.text=dateString;
    
    return cell;
}


-(void) refresh:(UIRefreshControl *) refreshControl {
    [self chatLoadMessages];
    [refreshControl endRefreshing];
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    NSDictionary *dict =  [self getUserMsgsByIndex:indexPath];
    
    Fan *sender = [[Fan alloc] init];
    sender.userId = [dict[@"senderUserID"] intValue];
    sender.fanName = dict[@"senderName"];
    sender.fanPicture = dict[@"senderPicture"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openMessaging" object:sender userInfo:nil];
}

@end
