//
//  FMDBHelper.h
//  SmartTracker
//
//  Created by Priyanka Bhatia on 7/25/13.
//  Copyright (c) 2013 UCS. All rights reserved.
//*

#import <Foundation/Foundation.h>

@class NewsItem;
@class EventItem;
@class HealthStats;

@interface FMDBHelper : NSObject

//----------------------------------
//NEWS ITEM
- (BOOL)                insertMessages                          :(NSMutableDictionary *)msg;
- (NSMutableArray *)    getAllMessagese;
- (NSMutableArray *)    getAllMessageseOfFriend                 : (NSString *)userID;
- (BOOL)                deleteMessage                           :(NSString *)msg;
- (NSMutableArray *)    getAllMessageseHavingText               :(NSString *)text;
- (BOOL)                deleteAllMessage;

- (BOOL)                insertFriends                           :(NSMutableDictionary *)msg;
- (BOOL)                insertOrReplaceFriends                  :(NSMutableDictionary *)msg;
- (NSMutableArray *)    getFriendWithID                         :(int)friendID;
- (NSMutableArray *)    getAllFriends;
- (NSMutableArray *)    getAllFriendsHavingText                 :(NSString *)text;
- (BOOL)                deleteAllFriends;

- (BOOL)                insertFan                               :(NSMutableDictionary *)msg;
- (BOOL)                deleteAllFans;
- (NSMutableArray *)    getAllMessageseOfFans;


//----------------------------------
@end
