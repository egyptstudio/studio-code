//
//  FMDBHelper.m
//  SmartTracker
//
//  Created by Priyanka Bhatia on 7/25/13.
//  Copyright (c) 2013 UCS. All rights reserved.
//

#import "FMDatabase.h"
#import "FMResultSet.h"
#import "FMDBHelper.h"


@implementation FMDBHelper

- (NSString *)createGuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    NSString *uuid = [NSString stringWithString:(__bridge NSString *)uuidStringRef];
    CFRelease(uuidStringRef);
    return uuid;
}
- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    return [dateFormat stringFromDate:date];
}

- (FMDatabase *)getDatabase
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"fcb-chat.sqlite"]];
    
    return db;
}



#pragma mark - NEWS ITEM

- (BOOL)insertMessages:(NSMutableDictionary *)msg
{
    NSString *msgID = [self createGuid];
    NSDate* theDate = [NSDate dateWithTimeIntervalSince1970:[msg[@"sentAt"] integerValue]];
    //NSString *dateString = [self relativeDateStringForDate:theDate];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *dateString = [formatter stringFromDate:theDate];

    int senderID = [msg[@"senderUserID"] intValue];
    NSString *strDenderID  = [NSString stringWithFormat:@"%d",senderID];
    
    int messageRemoteID = [msg[@"messageRemoteID"] intValue];
    NSString *strRemoteID  = [NSString stringWithFormat:@"%d",messageRemoteID];
    
    NSString *temp = [NSString stringWithFormat:@"INSERT INTO Messages (localMessageID, messageText, senderID, sent, messageTime, remoteMessageTime, received , messageRemoteID , receiverID , threadID) VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}','{7}' ,'{8}' , '{9}' , '{10}')"];
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:msgID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:msg[@"messageText"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:strDenderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:msg[@"sent"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:@"true"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:msg[@"received"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:strRemoteID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:msg[@"receiverID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:msg[@"threadID"]];
    NSLog(@"temp %@",temp);

    return [self executeNonQuery:temp];
}

- (NSMutableArray *)getAllMessagese
{
    NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.*,B.userName,B.profilePic FROM Messages As A, Friends As B  Where A.senderID=B.userId OR A.receiverID = B.userId Group By threadID"]] mutableCopy];
    //NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.*,B.userName,B.profilePic FROM Messages As A, Friends As B  Where A.senderID=B.userId Group By senderID"]] mutableCopy];

    return temp;
}
- (NSMutableArray *)getAllMessageseOfFans
{
    NSLog(@"Query Fan %@",[NSString stringWithFormat:@"SELECT A.*,B.fanName,B.fanPicture FROM Messages As A, Fan As B  Where A.senderID=B.fanID OR A.receiverID = B.fanID Group By threadID"]);
     NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.*,B.fanName,B.fanPicture FROM Messages As A, Fan As B  Where A.senderID=B.fanID OR A.receiverID = B.fanID Group By threadID"]] mutableCopy];
    return temp;

}
- (NSMutableArray *)getAllDefaultMessagese
{
    NSLog(@"Query Fan %@",[NSString stringWithFormat:@"SELECT A.*,fanName,B.fanPicture FROM Messages As A, Fan As B  Where A.senderID=B.fanID OR A.receiverID = B.fanID Group By threadID"]);
    NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.*,B.fanName,B.fanPicture FROM Messages As A, Fan As B  Where A.senderID=B.fanID OR A.receiverID = B.fanID Group By threadID"]] mutableCopy];
    return temp;
    
}

- (NSMutableArray *)getAllMessageseHavingText:(NSString *)text
{
    NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.*,B.userName,B.profilePic FROM Messages As A, Friends As B  Where (A.senderID=B.userId OR A.receiverID = B.userId) AND messageText LIKE '%%%@%%' Group By threadID",text]] mutableCopy];
    return temp;
}

- (NSMutableArray *)getAllMessageseOfFriend : (NSString *)userID
{
    NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM Messages Where senderID=%@ OR receiverID = %@ Order By messageTime",userID,userID]] mutableCopy];
    return temp;
}


- (BOOL)insertFriends:(NSMutableDictionary *)msg
{
    /*friendID = 722;
     friendName = "Ali Hachem";
     friendOnline = 0;
     friendPicNumber = 0;
     friendPicture = "http://barca.s3.amazonaws.com/third_party/uploads/profile/AliHachem.png";
     isFavorite = 0;
     isFollowed = 1;
     isPremium = 0;*/
    int onlineID = [msg[@"friendOnline"] intValue];
    NSString *online;
    if (onlineID==1) {online = @"true";}
    else{online=@"false";}
    
    int premium = [msg[@"isPremium"] intValue];
    NSString *isPremium;
    if (premium==1) {isPremium = @"true";}
    else{isPremium=@"false";}
    
    int favorite = [msg[@"isFavorite"] intValue];
    NSString *isFavorite;
    if (favorite==1) {isFavorite = @"true";}
    else{isFavorite=@"false";}
    
    int follow = [msg[@"isFollowed"] intValue];
    NSString *isFollowing;
    if (follow==1) {isFollowing = @"true";}
    else{isFollowing=@"false";}
    
    int senderID = [msg[@"friendID"] intValue];
    NSString *strDenderID  = [NSString stringWithFormat:@"%d",senderID];
    
    int picNo = [msg[@"friendPicNumber"] intValue];
    NSString *strPicNo  = [NSString stringWithFormat:@"%d",picNo];
    
    NSString *temp = [NSString stringWithFormat:@"INSERT INTO Friends (userId, userName, profilePic, online ,premium, isFavorite, posts, isFollowing, followers, following, bio, country, city, district) VALUES ('{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', '{8}','{9}', '{10}', '{11}', '{12}', '{13}', '{14}')"];
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:strDenderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:msg[@"friendName"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:msg[@"friendPicture"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:online];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:isPremium];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:isFavorite];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:strPicNo];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:isFollowing];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:@""];

    return [self executeNonQuery:temp];
}

- (BOOL)insertOrReplaceFriends:(NSMutableDictionary *)msg
{
    /*friendID = 722;
     friendName = "Ali Hachem";
     friendOnline = 0;
     friendPicNumber = 0;
     friendPicture = "http://barca.s3.amazonaws.com/third_party/uploads/profile/AliHachem.png";
     isFavorite = 0;
     isFollowed = 1;
     isPremium = 0;*/
    int onlineID = [msg[@"friendOnline"] intValue];
    NSString *online;
    if (onlineID==1) {online = @"true";}
    else{online=@"false";}
    
    int premium = [msg[@"isPremium"] intValue];
    NSString *isPremium;
    if (premium==1) {isPremium = @"true";}
    else{isPremium=@"false";}
    
    int favorite = [msg[@"isFavorite"] intValue];
    NSString *isFavorite;
    if (favorite==1) {isFavorite = @"true";}
    else{isFavorite=@"false";}
    
    int follow = [msg[@"isFollowed"] intValue];
    NSString *isFollowing;
    if (follow==1) {isFollowing = @"true";}
    else{isFollowing=@"false";}
    
    int senderID = [msg[@"friendID"] intValue];
    NSString *strDenderID  = [NSString stringWithFormat:@"%d",senderID];
    
    int picNo = [msg[@"friendPicNumber"] intValue];
    NSString *strPicNo  = [NSString stringWithFormat:@"%d",picNo];
    
    NSString *temp = [NSString stringWithFormat:@"INSERT OR REPLACE INTO Friends (userId, userName, profilePic, online ,premium, isFavorite, posts, isFollowing, followers, following, bio, country, city, district) VALUES ('{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', '{8}','{9}', '{10}', '{11}', '{12}', '{13}', '{14}')"];
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:strDenderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:msg[@"friendName"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:msg[@"friendPicture"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:online];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:isPremium];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:isFavorite];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:strPicNo];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:isFollowing];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:@""];
    
    return [self executeNonQuery:temp];
}

- (NSMutableArray *)getFriendWithID:(int)friendID
{
    NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM Friends WHERE userId == %d",friendID]] mutableCopy];
    return temp;
}

- (NSMutableArray *)getAllFriends
{
    NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM Friends"]] mutableCopy];
    return temp;
}

- (NSMutableArray *)getAllFriendsHavingText:(NSString *)text
{
    NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM Friends WHERE userName LIKE '%%%@%%'",text]] mutableCopy];
    return temp;
}

- (BOOL)deleteMessage:(NSString *)msg
{
    NSString *temp = @"DELETE FROM Messages WHERE localMessageID='{0}'";
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:msg];
    return [self executeNonQuery:temp];

}

- (BOOL)deleteAllMessage
{
    NSString *temp = @"DELETE FROM Messages";
    return [self executeNonQuery:temp];
}
- (BOOL)deleteAllFriends
{
    NSString *temp = @"DELETE FROM Friends";
    return [self executeNonQuery:temp];
}
- (BOOL)deleteAllFans
{
    NSString *temp = @"DELETE FROM Fan";
    return [self executeNonQuery:temp];
}

- (BOOL)                insertFan                               :(NSMutableDictionary *)msg
{
    NSString *temp = [NSString stringWithFormat:@"INSERT INTO Fan (fanID, fanName, fanPicture) VALUES ('{1}', '{2}', '{3}')"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@",msg[@"fanID"]]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@",msg[@"fanName"]]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[NSString stringWithFormat:@"%@",msg[@"fanPicture"]]];

    return [self executeNonQuery:temp];
}

- (BOOL)executeNonQuery:(NSString *)sql {
    //[self doQuery:sql];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:sql];
    }
    @catch (NSException *exception)
    {
        //NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return success;
    
    
}
- (NSArray *)fetchDataForQuery:(NSString *)sql
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSMutableArray *newsItems = [NSMutableArray new];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            [newsItems addObject:[results resultDictionary]];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        //NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        
        [db close];
    }
    
    return newsItems;
    
}

@end
 
 
