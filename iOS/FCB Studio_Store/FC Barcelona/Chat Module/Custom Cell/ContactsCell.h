//
//  RecentCell.h
//  PublicMinistry
//
//  Created by Shabir Jan on 19/12/2014.
//  Copyright (c) 2014 Shabir Jan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImageView.h"
@interface ContactsCell : UICollectionViewCell

@property (nonatomic,strong)IBOutlet UILabel *lblTitle;
@property (nonatomic,strong)IBOutlet AvatarImageView *imgProfile;
@property (nonatomic,strong)IBOutlet UILabel *lblCount;
@property (nonatomic,strong)IBOutlet UIImageView *imgFavorite;
@property (nonatomic,strong)IBOutlet UIImageView *imgFollow;
@property (nonatomic,strong)IBOutlet UIImageView *imgStatus;
@property (nonatomic,strong)IBOutlet UIImageView *imgBg;
@property (nonatomic,strong)IBOutlet UIButton *btn_Add;

@end
