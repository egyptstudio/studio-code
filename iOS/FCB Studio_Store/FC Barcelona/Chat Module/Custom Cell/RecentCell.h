//
//  RecentCell.h
//  PublicMinistry
//
//  Created by Shabir Jan on 19/12/2014.
//  Copyright (c) 2014 Shabir Jan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarImageView.h"
@interface RecentCell : UITableViewCell

@property (nonatomic,strong)IBOutlet UILabel *lblTitle;
@property (nonatomic,strong)IBOutlet AvatarImageView *cellLogo;
@property (nonatomic,strong)IBOutlet UILabel *lblDate;
@property (nonatomic,strong)IBOutlet UILabel *lblMessage;

@end
