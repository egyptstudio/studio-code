//
//  BarcelonaViewController.m
//  FC Barcelona
//
//  Created by Mohammed Alrassas on 8/26/14.
//  Copyright (c) 2016 Tawasol. All rights reserved.
//

#import "BarcelonaViewController.h"
#import "UsersManager.h"

#import "FansListView.h"
#import "FriendsView.h"

@interface BarcelonaViewController ()

@end

@implementation BarcelonaViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    User* user = [[User alloc] init];
//    user.userName = @"Mohammed";
//    user.email = @"";
//    @try {
//        [[UsersManager getInstance] registerUser:user];
//    }
//    @catch (NSException *exception) {
//        NSLog(@"%@",exception);
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openFans:(id)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]];
    FansListView *viewController = (FansListView *)[sb instantiateViewControllerWithIdentifier:@"FansListView"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)openFriends:(id)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Fans_iPad" bundle:[NSBundle mainBundle]];
    FriendsView *viewController = (FriendsView *)[sb instantiateViewControllerWithIdentifier:@"FriendsView"];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
