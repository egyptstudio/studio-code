//
//  SystemMessage.m
//  FC Barcelona
//
//  Created by Essam Eissa on 8/27/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import "SystemMessage.h"
#import "BarcelonaAppDelegate.h"


@implementation SystemMessage

@dynamic date;
@dynamic message;
@dynamic seen;


+(NSArray*)allMessages{
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"SystemMessage" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                   ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"SystemMessage: Unable to Fetch SystemMessage.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    return array ;
}

+(void)markAllSeen{
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SystemMessage" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"seen == %i", 0];
    [fetchRequest setPredicate:predicate];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject * message in result) {
        [message setValue:@1 forKey:@"seen"];

        NSError *saveError = nil;
        
        if (![message.managedObjectContext save:&saveError]) {
            NSLog(@"SystemMessage: Unable to save SystemMessage.");
            NSLog(@"%@, %@", saveError, saveError.localizedDescription);
        }
    }
    

}

+(NSInteger)unseenMessagesNumber{
    NSManagedObjectContext *context = [(BarcelonaAppDelegate *)UIApplication.sharedApplication.delegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SystemMessage" inManagedObjectContext:context];
    [request setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"seen == %@", 0];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *array = [context executeFetchRequest:request error:&error];
    return [array count];
}
@end
