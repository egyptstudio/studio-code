//
//  SystemMessage.h
//  FC Barcelona
//
//  Created by Essam Eissa on 8/27/15.
//  Copyright (c) 2015 Tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SystemMessage : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSNumber * seen;

+(NSArray*)allMessages;
+(void)markAllSeen;
+(NSInteger)unseenMessagesNumber;

@end
